//
//  EmbeddedExample_main.c
//  PetriLab
//
//  Created by Rémi Saurel on 2016-04-24.
//

// This file serves as an example of what to do to use the code generated from
// the EmbeddedPetriNet.petri example.
// You will have to remove the "generic_embedded_main.h" header from the document,
// so that no duplicate error will be triggered by the 2 "main" functions.
// Then, all you have to do is to compile this file along with EmbC.c, and then
// execute the resulting program.

#define PETRI_GENERATED_HEADER // Required to use the generated code as a header.
#include "EmbC.c"

#include <stdio.h>

int main(void) {
    int status = Petri_runPetriNet(&EmbC_petriNet);
    if(status != 0) {
        switch(status) {
            case PETRI_ERROR_COULD_NOT_ENABLE_STATE:
                fprintf(stderr,
                        "The runtime could not enable a state! This means the max "
                        "parallelism level set for the petri net is too low.\n");
                break;

            default:
                fprintf(stderr, "Unexpected error %d!\n", status);
                break;
        }

        return 1;
    }

    return 0;
}
