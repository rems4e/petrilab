//
//  generic_embedded_main.h
//  PetriLab
//
//  Created by Rémi Saurel on 2016-04-17.
//

// This file serves as an example of what to do to use the code generated from
// an embedded petri net.
// All you have to do is to #define GENERATED_CODE_PATH to the petri net's generated
// and the name of the petri net in PETRI_NET_TO_RUN.

#ifndef PETRI_GENERIC_EMBEDDED_MAIN
#define PETRI_GENERIC_EMBEDDED_MAIN

#define PETRI_GENERATED_HEADER // Required to use the generated code as a header.
#include GENERATED_CODE_PATH

#define GET_PETRI_FROM_NAME2(NAME) NAME##_petriNet
#define GET_PETRI_FROM_NAME(NAME) GET_PETRI_FROM_NAME2(NAME)

#include <stdio.h>

int main(void) {
    // You have to #define PETRI_NET_TO_RUN as the name of the petri net to run.
    int status = Petri_runPetriNet(&GET_PETRI_FROM_NAME(PETRI_NET_TO_RUN));
    if(status != 0) {
        switch(status) {
            case PETRI_ERROR_COULD_NOT_ENABLE_STATE:
                fprintf(stderr,
                        "The runtime could not enable a state! This means the max "
                        "parallelism level set for the petri net is too low.\n");
                break;

            default:
                fprintf(stderr, "Unexpected error %d!\n", status);
                break;
        }

        return 1;
    }

    return 0;
}

#endif
