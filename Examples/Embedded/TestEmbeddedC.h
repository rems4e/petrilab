//
//  TestEmbeddedC.h
//  PetriLab
//
//  Created by Rémi Saurel on 2016-04-18.
//

#include <petrilab/Embedded/PetriNet.h>

#include <inttypes.h>
#include <stdio.h>

static Petri_actionResult_t printVariable(char const *var, int64_t value) {
    printf("%s: %ld\n", var, value);
    return 0;
}

static Petri_actionResult_t action1() {
    printf("Action1!\n");
    return 0;
}

static Petri_actionResult_t action2() {
    printf("Action2!\n");
    return 0;
}

static bool condition1(Petri_actionResult_t result) {
    printf("Condition1!\n");
    return true;
}

static bool condition2(Petri_actionResult_t result) {
    printf("Condition2!\n");
    return true;
}
