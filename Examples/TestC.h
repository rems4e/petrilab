//
//  TestC.h
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-25.
//

#include <petrilab/C/PetriLab.h>

#include <inttypes.h>
#include <stdio.h>

typedef enum ActionResult ActionResult;

static ActionResult action1() {
    printf("Action1!\n");
    return 0;
}

// @PetriLab@ Action2&3

static ActionResult action2() {
    printf("Action2!\n");
    return 0;
}

// clang-format off
static double action3()

 {
    printf(
        "Action3!\n");
    return
     0.0;}
// clang-format on

static ActionResult action4(int param, char const *param2) {
    printf("Action2!\n");
    return 0;
}


// @PetriLab@ Conditions

static bool condition1(Petri_actionResult_t result) {
    printf("Condition1!\n");
    return true;
}

/*
static ActionResult actionInComment() {
    printf("Action2!\n");
    return 0;
}

 */

static char const *var = "hey!"
                         "static ActionResult actionInString() {"
                         "    printf(\"Action2!\n\");"
                         "    return 0;"
                         "}";

static bool condition2(Petri_actionResult_t result);
