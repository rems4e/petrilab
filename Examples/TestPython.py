# coding: utf-8

#
#  TestPython.py
#  PetriLab
#
#  Created by Rémi Saurel on 2017-11-20.
#

# comment

import sys


def action1():
    print("Action1!")
    sys.stdout.flush()
    return 0

# @PetriLab@ Test section


def action2():
    print("Action2!")
    sys.stdout.flush()
    return 0


def action3(arg1, arg2):
    print("Action3!")
    sys.stdout.flush()
    return 0


def condition1(action_result):
    print("Condition1!")
    sys.stdout.flush()
    return True


# @PetriLab@ Test section 2

class MyClass(object):
    pass


class MyClass2(object):
    def my_method1(self):
        return 0

    def my_method2():
        return 0

    class MyClass3():
            def my_method3(   myself    , arg):
                        if False:
                            return 4

    def my_method4():
        pass
