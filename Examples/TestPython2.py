# coding: utf-8

#
#  TestPython2.py
#  PetriLab
#
#  Created by Rémi Saurel on 2017-12-03.
#

# comment

import sys


def action1():
    print "{0} == 0".format(1 / 2)
    sys.stdout.flush()
    return 0


def action2():
    print "{0} == 2".format(sys.version_info.major)
    sys.stdout.flush()
    return 0
