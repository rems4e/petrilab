#!/bin/bash

#
#  upgrade.sh
#  PetriLab
#
#  Created by Rémi Saurel on 2017-01-31.
#


dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

find "$dir" -name "*.petri" -a \( \! -name "*api*.petri" \) -exec "$dir/../petri" --upgrade-doc {} +
