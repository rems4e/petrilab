//
//  TestCSharp.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-17.
//

using System;
using System.Collections.Generic;
using ActionResult = Petri.Runtime.ActionResult;

class TestNS {
    public static ActionResult Action1() {
        Console.WriteLine("Action1!");
        return 0;
    }
    // @PetriLab@ class
    public static ActionResult Action2() {
            Console.WriteLine("Action2!");
            return 0;
    }

    public static bool Condition1(Int32 result) {
            Console.WriteLine("Condition1!");
            return true;
    }
}

namespace TestNS2 {// @PetriLab@ class + NS
    class TestClass {
        public static List<Tuple<string, char>> Action3(string value) {
            Console.WriteLine("Action3!");
            return null;
        }


        /*public static ActionResult Action4() {
                Console.WriteLine("Action2!");
                return 0;
        }*/

        public static bool Condition2(Int32 result) {
                Console.WriteLine("Condition1!");
                return true;
        }
    }

}
