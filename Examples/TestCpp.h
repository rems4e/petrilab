//
//  TestCpp.h
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-25.
//

#include <petrilab/Cpp/PetriLab.h>

#include <list>
#include <vector>
using Petri::ActionResult;


namespace TestNS {
    static ActionResult action1() {
        std::cout << "Action1!" << std::endl;
        return {};
    }
    // @PetriLab@ action2
    // clang-format off
    static ActionResult action2()

 { std::cout << "Action2!" << std::endl; return {};}

auto str = ""
    "static ActionResult action3() {"
        "std::cout << \"Action3!\" << std::endl;"
        "return {};"
    "}";
    // clang-format on


    static bool condition1(ActionResult result) {
        std::cout << "Condition1!" << std::endl;
        return true;
    }
}

// @PetriLab@ class

class TestClass {
public:
    // clang-format off
    static std::size_t action4(std::vector<std::list<int >> const &&) { std::cout << "Action4!" << std::endl; return 4;
}

static ActionResult action5(std::vector<std::list<int >> const &&) { std::cout << "Action5!" << std::endl; return {};
}
    // clang-format on
    /*
        static ActionResult action6() {
            std::cout << "Action6!" << std::endl;
            return {};
        }
    */
    static bool condition2(ActionResult result, int param) {
        std::cout << "Condition2!" << std::endl;
        return true;
    }
};


static ActionResult action7(std::vector<std::list<int>> const &param)

    ;

// clang-format off

namespace      NS2

{
    class MyClass {
        public   :
        // clang-format on
        static int action8(double) {
            std::cout << "Action8!" << std::endl;
            return {};
        }

        int action9(double) {
            std::cout << "Action9!" << std::endl;
            return {};
        }

        ActionResult action10(double) {
            std::cout << "Action10!" << std::endl;
            return {};
        }
    };
}
