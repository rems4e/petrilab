#!/bin/bash

#
#  petri_cpp_to_c_cs_python.sh
#  PetriLab
#
#  Created by Rémi Saurel on 2017-01-30.
#


if [[ $# == 0 ]]; then
    dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
    find "$dir" -name "*Cpp*.petri" -a \( \! -name "CppBasic.petri" \) -a \( \! -name "*api*.petri" \) -exec echo {} \; -exec "$0" {} \;
else
    dir="$(cd "$(dirname "$1")" && pwd)"

    filename=$(basename "$1")
    echo "$(sed 's/Cpp/C/g ; s/c++/cc/g ; s/Petri::Utility::/PetriUtility_/g' <"$1" \
        | perl -pe 's#(\d+)ms#($1/1000)#e'
    )" > "${1/Cpp/C}"

    echo "$(sed 's/Cpp/CSharp/g ; s/\.h/.cs/g ; s/c++/csc/g ; s/Petri::Utility::\(.\)/Petri.Runtime.Utility.\U\1/g' <"$1" \
        | perl -pe 's#(\d+)ms#($1/1000)#e'
    )" > "${1/Cpp/CSharp}"

    echo "$(sed 's/Cpp/Python/g ; s/\.h/.py/g ; s/c++/cc/g ; s/Petri::Utility::/PetriUtility_/g ; s/Condition="true"/Condition="True"/g ; s/Condition="false"/Condition="False"/g ; s/Condition="c/Condition="TestPython.c/g ; s/Action="a/Action="TestPython.a/g ; s/++\$\([a-zA-Z0-9]\+\)/\$\1 = \$\1 + 1/g' <"$1" \
        | perl -pe 's#(\d+)ms#($1/1000)#e'
    )" > "${1/Cpp/Python}"
fi
