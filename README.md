# PetriLab

A C# Petri Net editor, code generator and debugger with C, C++, C# and Python runtimes. The editor/code/generator/debugger is built against the Mono framework, see below for compiling and running the editor.

[![Build Status](https://jenkins.remi-saurel.com/buildStatus/icon?job=PetriLab)](https://jenkins.remi-saurel.com/job/PetriLab/)

The code's documentation can be found here: https://jenkins.remi-saurel.com/job/PetriLab/ws/Documentation/html/index.html

An amd64 .deb file is available here: https://jenkins.remi-saurel.com/job/PetriLab/ws/petrilab.deb

If you get a 404 trying to get there it probably means the CI is running a build and has cleaned the workspace. Just wait for it to complete.

## Compilation of the petri net editor/debugger
Although you can get the running executables at https://git.remi-saurel.com/sigilence/petrilab/releases, you may want to compile the source code.

### Tools
#### Linux

Here is the whole package list that is needed to build the app on Debian-based distributions:
``` bash
apt install mono-complete g++ gcc clang python3-dev python-dev cmake git
```

The mono distribution that comes with Debian (tested on Debian 8) or Ubuntu (tested on 15.10) is somewhat outdated and may fail to compile the source code.

I recommend that you follow the instructions found at http://www.mono-project.com/docs/getting-started/install/linux/, and then install the `mono-devel` and `gtk-sharp2` packages (at least on Debian based distros, the actual package may be different for others).

Alternatively, you can install the `monodevelop` package. This will give you a complete IDE.

#### OS X
There are two simple methods to install mono on OS X:
* By following the instructions at http://www.mono-project.com/docs/getting-started/install/mac/.
* By first installing Homebrew, a great package manager. For that, just follow the instructions at http://brew.sh, and then run the command `brew install mono`.

Alternatively, you can install Xamarin Studio for OS X from here: http://www.monodevelop.com/download/. This will give you a complete IDE.

### The compilation process
This projects uses the CMake build system. To build, put yourself in a dedicated directory, and run the following:

``` bash
cmake "path/to/the/repository"
make
```
These commands will compile the editor and runtime.

By default, CMake generates a set of makefiles. I will use them as example, but you are free to use the ability of CMake to use `ninja` or `Xcode` or any of the supported generators.

``` bash
make check
```
This command will run the unit tests. It requires the `nunit-console` package.

## Running the editor/compiler
Once compiled, the editor is available in the Editor/bin directory, for command-line and GUI invocation.
You are encouraged to use the `petri` wrapper script at the root of the repo.

### Editor
``` bash
path_to_repo/ $ ./petrilab
```

This command will spawn the GUI editor.

### Compiler
The compiler is the same executable as before, simply invoked with additional arguments.
```
path_to_repo/ $ ./petrilab --help
Usage: petrilab [--version] [--player] [--profile <profile-name>] [--generate|-g] [--compile|-c] [--deploy] [--update|-u] [--run|-r [--args '$arg0:value1 $arg1:value2'] [--no-return-values]] [--clean|-k] [--verbose|-v] [--debug|-d] [--exportPDF file.pdf] [--list-params] [--list-profiles] [--] <path-to-document.petri>

Possible use cases:
  --version                           ~ Prints the application's version with some additional info.
  [--player] <path-to-doc.petri> [<path-to-other-doc-1.petri> …] ~ Opens the specified documents list in the petri net editor. If the --player option is specified, then an execute/debug only version of the app is opened.
  --exportPDF <path-to-new-PDF-doc> <path-to-doc.petri>          ~ Creates a PDF representation of the petri net at the specified path.

  --generate|-g <path-to-doc.petri>   ~ Generates the petri net's source code.
  --compile|-c <path-to-doc.petri>    ~ Compiles the petri net's source code. If the source code is outdated, it is generated as if the --generate option was passed as well.
  --deploy    <path-to-doc.petri>     ~ Deploys the dynamic library.
  --update|-c <path-to-doc.petri>     ~ Recompiles and deploys the dynamic library if it is outdated only. If the source code is outdated, it is generated as if the --generate option was passed as well.
  --clean|-k <path-to-doc.petri>      ~ Cleans the artifacts of a petri net, i.e removes the generated source code files and compiled libraries.

  --list-params <path-to-doc.petri>   ~ Displays the list of parameters the petri net accepts.
  --list-profiles <path-to-doc.petri> ~ Displays the list of profiles the document declares.

  --run|-r [--args '$arg0:value $arg1:value'] [--no-return-values] <path-to-doc.petri>      ~ Runs the petri net with the provided arguements, if any. Prints the return values of the petri net, if any, unless the --no-return-values option is specified. The petri net must be able to run embedded in the editor, i.e. not require hosting from an external application. If the dynamic library is outdated or nonexistent, it is generated as if the --compile option was passed.
  --debug|-d <path-to-doc.petri>      ~ Load the command-line debugger and starts a debugging session in it.

The --profile option is to be followed by a profile name, which can be obtained from the --list-profiles option. If present, it will be used by the following options: --generate --compile --update --deploy --run.

Options that can be combined:
  --generate --compile --update --run --clean, --deploy (or equivalently -gcrku, -g -c -r -k, -u, or any mix of short and long options format).
--verbose|-v can always be specified with every other option and causes some information output to be generated.

Specifying the --lang option and giving it a 2 letter ISO country name code argument will cause the application to load the appropriate locale. Currently supported locales are:
  en - English
  fr - Français
```

## Compilation of the runtime
The compilation of the C++ runtime requires a C++14-compliant compiler (g++, tested on version 5.2 and 4.9, and clang++ from version 3.4 and over).

Run the following commands:
``` bash
make PetriRuntime
```
They will give you a shared library, libPetriRuntime.so (or .dylib on OS X), that contains both the C and C++ runtimes, and a DLL for the C# runtime.
