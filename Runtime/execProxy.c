//
//  execProxy.c
//  PetriLab
//
//  Created by Rémi Saurel on 2016-03-23.
//

#include <errno.h>
#include <libgen.h>
#include <limits.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define ERROR_USAGE 1
#define ERROR_EXEC_NOT_FOUND 2
#define ERROR_DIR_NOT_FOUND 3
#define ERROR_UNKNOWN 64

int main(int argc, char *argv[]) {
    if(argc < 2) {
        return ERROR_USAGE;
    }

    sigset_t sigs;

    sigemptyset(&sigs);
    sigaddset(&sigs, SIGINT);
    sigprocmask(SIG_BLOCK, &sigs, NULL);

    char buf[PATH_MAX + 2];
    strncpy(buf + 2, argv[1], PATH_MAX);
    buf[PATH_MAX + 2 - 1] = '\0';
    char *dir = dirname(buf + 2);
    if(chdir(dir) < 0) {
        fprintf(stderr, "Petri execProxy: could not cd to '%s': %s (%d)!\n", dir, strerror(errno), errno);
        return ERROR_DIR_NOT_FOUND;
    }

    strncpy(buf + 2, argv[1], PATH_MAX);
    buf[PATH_MAX + 2 - 1] = '\0';

    char *name = buf + 2;
    if(argv[1][0] != '/' && strchr(argv[1] + 1, '/') != NULL) {
        name = basename(buf + 2);
        name = name - 2;
        name[0] = '.';
        name[1] = '/';
    }

    execvp(name, argv + 1);

    fprintf(stderr, "Petri execProxy: could not launch '%s': %s (%d)!\n", name, strerror(errno), errno);

    if(errno == ENOENT) {
        return ERROR_EXEC_NOT_FOUND;
    }

    return ERROR_UNKNOWN;
}
