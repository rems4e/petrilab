//
//  PetriNet.h
//  PetriLab
//
//  Created by Rémi Saurel on 2016-04-16.
//

#ifndef PETRI_EMBEDDED_PetriNet_h
#define PETRI_EMBEDDED_PetriNet_h

#define PETRI_ERROR_COULD_NOT_ENABLE_STATE 1

// This is the number of times the transitions of a state are tested when none of them can be
// crossed.
// When the specified number of retries is exhausted, the state is rescheduled for later execution.
#ifndef PETRI_RETRY_COUNT_BEFORE_RESCHEDULE
#define PETRI_RETRY_COUNT_BEFORE_RESCHEDULE 100
#endif

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

typedef int16_t Petri_actionResult_t;
typedef uint16_t Petri_token_t;
typedef uint16_t Petri_size_t;
typedef int16_t Petri_ssize_t;

struct Petri_Action;
struct Petri_Transition;

struct Petri_ActionCircularQueue {
    Petri_size_t _states[PETRI_PETRI_NET_MAX_ACTIVE_STATES];
    Petri_size_t _first;
    Petri_size_t _size;
};

struct Petri_Action {
    Petri_actionResult_t (*_action)(Petri_ssize_t *);
    Petri_size_t _firstTransition;
    Petri_size_t _transitionsCount;

    Petri_token_t _requiredTokens;
    Petri_token_t _currentTokens;

    bool _executed;
};

struct Petri_Transition {
    bool (*_condition)(Petri_actionResult_t, Petri_ssize_t *);
    Petri_size_t _next;
};

struct Petri_PetriNet {
    struct Petri_Action _states[PETRI_PETRI_NET_STATES_COUNT];
    struct Petri_Transition _transitions[PETRI_PETRI_NET_TRANSITIONS_COUNT];

    struct Petri_ActionCircularQueue _activeStates;

    Petri_ssize_t _variables[PETRI_PETRI_NET_VARIABLES_COUNT];
};

#ifdef __cplusplus
extern "C" {
#endif

static Petri_actionResult_t Petri_doNothing();
static int Petri_enableState(struct Petri_PetriNet *petriNet, Petri_size_t state);
static int Petri_runPetriNet(struct Petri_PetriNet *petriNet);

static inline int Petri_runPetriNet(struct Petri_PetriNet *petriNet) {
    while(petriNet->_activeStates._size > 0) {
        Petri_size_t currentIndex =
        petriNet->_activeStates._states[(petriNet->_activeStates._first + petriNet->_activeStates._size-- - 1) % PETRI_PETRI_NET_MAX_ACTIVE_STATES];
        struct Petri_Action *current = &petriNet->_states[currentIndex];

        Petri_actionResult_t result = 0;
        if(!current->_executed && current->_action) {
            result = current->_action(petriNet->_variables);
            current->_executed = true;
        }

        Petri_size_t tries = 0;

        bool isFulfilled = current->_transitionsCount == 0;
        while(!isFulfilled && tries++ < PETRI_RETRY_COUNT_BEFORE_RESCHEDULE) {
            for(Petri_size_t i = 0; i != current->_transitionsCount; ++i) {
                if(petriNet->_transitions[current->_firstTransition + i]._condition(result, petriNet->_variables)) {
                    Petri_size_t next = petriNet->_transitions[current->_firstTransition + i]._next;
                    ++petriNet->_states[next]._currentTokens;

                    if(petriNet->_states[next]._currentTokens >= petriNet->_states[next]._requiredTokens) {
                        petriNet->_states[next]._currentTokens -= petriNet->_states[next]._requiredTokens;
                        int status = Petri_enableState(petriNet, next);
                        if(status != 0) {
                            return status;
                        }
                    }

                    isFulfilled = true;
                }
            }
        }

        // This means we have to reschedule the action for later.
        if(!isFulfilled) {
            // Cannot fail as we have freed a slot at the beginning of the iteration and not
            // activated a new state.
            Petri_enableState(petriNet, currentIndex);
            current->_executed = true;
        }
    }

    return 0;
}

static inline int Petri_enableState(struct Petri_PetriNet *petriNet, Petri_size_t state) {
    if(petriNet->_activeStates._size < PETRI_PETRI_NET_MAX_ACTIVE_STATES) {
        if(petriNet->_activeStates._first == 0) {
            petriNet->_activeStates._first = PETRI_PETRI_NET_MAX_ACTIVE_STATES - 1;
        } else {
            --petriNet->_activeStates._first;
        }
        petriNet->_activeStates._states[petriNet->_activeStates._first] = state;
        petriNet->_states[state]._executed = false;
        petriNet->_states[state]._currentTokens = 0;
        ++petriNet->_activeStates._size;

        return 0;
    }

    return PETRI_ERROR_COULD_NOT_ENABLE_STATE;
}

static inline Petri_actionResult_t Petri_doNothing() {
    return 0;
}


#ifdef __cplusplus
}
#endif

#endif /* PetriNet_h */
