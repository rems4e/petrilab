# coding: utf-8

#
#  PetriRuntime.py
#  PetriLab
#
#  Created by Rémi Saurel on 2017-11-17.
#

import sys
import traceback


class _PetriRuntime_Interop(object):
    def __init__(self):
        from ctypes import cdll, c_size_t, c_char_p, c_void_p, c_int32, c_int64, c_uint32, c_uint64, c_double, c_bool
        petri_runtime = cdll.LoadLibrary('libPetriRuntime.so')

        self.varslot_get_size = petri_runtime.PetriVarSlot_getSize
        self.varslot_get_size.restype = c_size_t
        self.varslot_get_size.argtypes = [c_void_p]

        self.var_get_value = petri_runtime.PetriVarSlot_getVariableValue
        self.var_get_value.restype = c_int64
        self.var_get_value.argtypes = [c_void_p, c_uint32]

        self.var_set_value = petri_runtime.PetriVarSlot_setVariableValue
        self.var_set_value.argtypes = [c_void_p, c_uint32, c_int64]

        self.var_get_name = petri_runtime.PetriVarSlot_getVariableName
        self.var_get_name.restype = c_char_p
        self.var_get_name.argtypes = [c_void_p, c_uint32]

        self.var_is_return_values = petri_runtime.PetriVarSlot_isReturnValues
        self.var_is_return_values.restype = c_bool
        self.var_is_return_values.argtypes = [c_void_p]

        self.utility_print_all_vars = petri_runtime.PetriUtility_printAllVars
        self.utility_print_all_vars.restype = c_int32
        self.utility_print_all_vars.argtypes = [c_void_p]

        self.utility_print_text = petri_runtime.PetriUtility_printText
        self.utility_print_text.restype = c_int32
        self.utility_print_text.argtypes = [c_char_p]

        self.utility_print_action = petri_runtime.PetriUtility_printAction
        self.utility_print_action.restype = c_int32
        self.utility_print_action.argtypes = [c_char_p, c_uint64]

        self.utility_pause = petri_runtime.PetriUtility_pause
        self.utility_pause.restype = c_int32
        self.utility_pause.argtypes = [c_double]

        self.utility_do_nothing = petri_runtime.PetriUtility_doNothing
        self.utility_do_nothing.restype = c_int32
        self.utility_do_nothing.argtypes = []

        self.utility_random = petri_runtime.PetriUtility_random
        self.utility_random.restype = c_int64
        self.utility_random.argtypes = [c_int64, c_int64]

_PetriRuntime_Interop_Instance = _PetriRuntime_Interop()


class PetriRuntime_VarSlot(object):
    def __init__(self, slot):
        self._slot = slot

    def __getitem__(self, key):
        if key < 0 or key >= len(self):
            raise IndexError()
        return PetriRuntime_Var(self, key)

    def __len__(self):
        return _PetriRuntime_Interop_Instance.varslot_get_size(self._slot)

    def __iter__(self):
        return PetriRuntime_Var(self, -1)

    def is_return_values(self):
        return _PetriRuntime_Interop_Instance.var_is_return_values(self._slot)


class PetriRuntime_Var(object):
    def __init__(self, slot, index):
        self._slot = slot
        self._index = index

    def __iter__(self):
        return self

    def __next__(self):
        self._index = self._index + 1
        if self._index >= len(self._slot):
            raise StopIteration()
        return self

    def next(self):
        return self.__next__()

    def get_value(self):
        return _PetriRuntime_Interop_Instance.var_get_value(self._slot._slot, self._index)

    def set_value(self, value):
        _PetriRuntime_Interop_Instance.var_set_value(self._slot._slot, self._index, value)

    def get_name(self):
        return _PetriRuntime_Interop_Instance.var_get_name(self._slot._slot, self._index).decode("utf-8")

    value = property(get_value, set_value)


def PetriRuntime_invoke(func, slotptr, _PETRI_PRIVATE_GET_ACTION_RESULT_, Petri_Var_Enum):
    try:
        _PETRI_PRIVATE_GET_VARIABLES_ = PetriRuntime_VarSlot(slotptr)
        return eval(func)
    except:
        sys.stderr.write("Exception when executing {0}:\n{1}\n".format(func, traceback.format_exc()))
        sys.stdout.flush()
        sys.stderr.flush()
        return None


def PetriUtility_printAction(name, id):
    return _PetriRuntime_Interop_Instance.utility_print_action(name.encode("utf-8"), id)


def PetriUtility_pause(delay_in_seconds):
    return _PetriRuntime_Interop_Instance.utility_pause(delay_in_seconds)


def PetriUtility_doNothing():
    return _PetriRuntime_Interop_Instance.utility_do_nothing()


def PetriUtility_printAllVars(_PETRI_PRIVATE_GET_VARIABLES_):
    return _PetriRuntime_Interop_Instance.utility_print_all_vars(_PETRI_PRIVATE_GET_VARIABLES_._slot)


def PetriUtility_printText(text):
    return _PetriRuntime_Interop_Instance.utility_print_text(text.encode("utf-8"))


def PetriUtility_random(lower_bound, upper_bound):
    return _PetriRuntime_Interop_Instance.utility_random(lower_bound, upper_bound)
