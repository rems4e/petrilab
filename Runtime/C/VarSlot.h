//
//  VarSlot.h
//  PetriLab
//
//  Created by Rémi Saurel on 2017-03-18.
//

#ifndef Petri_CVarSlot_H
#define Petri_CVarSlot_H

#include <stdint.h>
#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif
struct PetriVarSlot;

struct PetriVarSlot *PetriVarSlot_create(size_t capacity);
void PetriVarSlot_destroy(struct PetriVarSlot *slot);

size_t PetriVarSlot_getSize(struct PetriVarSlot *slot);

/**
 * Gets the variable designated by the specified id.
 * @param slot The slot that contains the variable.
 * @param id The id of the variable.
 * @return The value of the variable.
 */
volatile int64_t *PetriVarSlot_getVariable(struct PetriVarSlot *slot, uint32_t id);

/**
 * Gets the value of the variable designated by the specified id.
 * @param slot The slot that contains the variable.
 * @param id The id of the variable.
 * @return The value of the variable.
 */
int64_t PetriVarSlot_getVariableValue(struct PetriVarSlot *slot, uint32_t id);

/**
 * Sets the value of the variable designated by the specified id to the given value.
 * @param slot The slot that contains the variable.
 * @param id The id of the variable.
 * @param value The value to assign to the variable.
*/
void PetriVarSlot_setVariableValue(struct PetriVarSlot *slot, uint32_t id, int64_t value);

void PetriVarSlot_setVariableDefaultValue(struct PetriVarSlot *slot, uint32_t id, int64_t value);

char const *PetriVarSlot_getVariableName(struct PetriVarSlot *slot, uint32_t id);

void PetriVarSlot_setVariableName(struct PetriVarSlot *slot, uint32_t id, char const *value);


void PetriVarSlot_pushVariables(struct PetriVarSlot *slot, size_t count);
void PetriVarSlot_pushReturnValues(struct PetriVarSlot *slot, size_t count);

void PetriVarSlot_pop(struct PetriVarSlot *slot);

bool PetriVarSlot_isFirstSlot(struct PetriVarSlot *slot);

bool PetriVarSlot_isReturnValues(struct PetriVarSlot *slot);

#ifdef __cplusplus
}
#endif

#endif /* end of include guard: Petri_CVarSlot_H */
