//
//  DebugServer.h
//  PetriLab
//
//  Created by Rémi Saurel on 2015-07-04.
//

#ifndef Petri_CDebugServer_H
#define Petri_CDebugServer_H

#include "PetriDynamicLib.h"
#include <stdbool.h>
#include <time.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Returns the DebugServer API's version
 * @return The current version of the API.
 */
char const *PetriDebugServer_getVersion();

/**
 * Creates the DebugServer and binds it to the provided dynamic library.
 * @param petri The dynamic lib from which the debug server operates.
 */
struct PetriDebugServer *PetriDebugServer_create(struct PetriDynamicLib *petri);

/**
 * Creates the DebugServer and binds it to the provided dynamic library.
 * The debug server will act as a client and not output log messages to stdout.
 * @param petri The dynamic lib from which the debug server operates.
 */
struct PetriDebugServer *PetriDebugServer_createClient(struct PetriDynamicLib *petri);

/**
* Destroys the debug server. If the server is running, this call will wait for the connected client
* to end the debug session to continue the program exectution.
* @param server The debug server to operate on.
*/
void PetriDebugServer_destroy(struct PetriDebugServer *server);

/**
 * Starts the debug server by listening on the debug port of the bound dynamic library, making it
 * ready to receive a debugger connection.
 * @param server The debug server to operate on.
 */
void PetriDebugServer_start(struct PetriDebugServer *server);

/**
 * Stops the debug server. After that, the debugging port is unbound.
 * @param server The debug server to operate on.
 */
void PetriDebugServer_stop(struct PetriDebugServer *server);

/**
 * Checks whether the debug server is running or not.
 * @param server The debug server to operate on.
 * @return true if the server is running, false otherwise.
 */
bool PetriDebugServer_isRunning(struct PetriDebugServer *server);

/**
 * Waits for the debug server session to end.
 * @param server The debug server to operate on.
 */
void PetriDebugServer_join(struct PetriDebugServer *server);

#ifdef __cplusplus
}
#endif

#endif /* Petri_CDebugServer_H */
