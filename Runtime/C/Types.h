//
//  Types.h
//  PetriLab
//
//  Created by Rémi Saurel on 2015-06-25.
//

#ifndef Petri_CTypes_H
#define Petri_CTypes_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

typedef int32_t Petri_actionResult_t;

#ifdef __cplusplus
}
#endif

#endif /* Petri_CTypes_H */
