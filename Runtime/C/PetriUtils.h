//
//  PetriUtils.h
//  PetriLab
//
//  Created by Rémi Saurel on 2015-07-01.
//

#ifndef Petri_CUtils_H
#define Petri_CUtils_H

#include "Types.h"
#include <stdbool.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

struct PetriDynamicLib;
struct PetriVarSlot;

enum ActionResult { OK, NOK };

/**
 * Pauses the execution of the calling thread for the specified amount of time.
 * @param seconds The delay to wait, in seconds.
 * @return A defaulted action result.
 */
Petri_actionResult_t PetriUtility_pause(double seconds);

/**
 * Prints the specified action name and id to stdout.
 * @param name The name of the action.
 * @param id The id of the action.
 * @return A defaulted action result.
 */
Petri_actionResult_t PetriUtility_printAction(char const *name, uint64_t id);

/**
 * A function that does not perform any action, and returns a defaulted action result.
 * @return A defaulted action result.
 */
Petri_actionResult_t PetriUtility_doNothing();

/*
 * Prints all variables of an entity
 * @param variables The slot containing the variables to print.
 * @return A defaulted action result.
 */
Petri_actionResult_t PetriUtility_printAllVars(struct PetriVarSlot *variables);

/**
 * Prints the given text.
 * @param value The text to print.
 * @return A defaulted action result.
 */
Petri_actionResult_t PetriUtility_printText(char const *value);

/**
* A function that returns a random number between the specified bounds.
* @param lowerBound The lower bound of the range from which the random number is retrieved.
* @param upperBound The upper bound of the range from which the random number is retrieved.
* @return A random number such as lowerBound <= result <= upperBound, iff lowerBound <=
* upperBound.
*/
int64_t PetriUtility_random(int64_t lowerBound, int64_t upperBound);

char *PetriUtility_loadEvaluateAndInvoke(struct PetriVarSlot *vars, char const *lib, char const *prefix);

struct PetriDynamicLib *Petri_loadPetriDynamicLib(char const *path, char const *name, bool nodelete);

#ifdef __cplusplus
}
#endif

#endif /* Petri_CUtils_H */
