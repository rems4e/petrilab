//
//  DebugServer.c
//  PetriLab
//
//  Created by Rémi Saurel on 2015-07-04.
//

#include "../../Cpp/DebugServer.h"
#include "../../Cpp/PetriDebug.h"
#include "../../Cpp/PetriDynamicLib.h"
#include "../DebugServer.h"
#include "Types.hpp"
#include <cstdbool>

char const *PetriDebugServer_getVersion() {
    return Petri::DebugServer::getVersion().c_str();
}

PetriDebugServer *PetriDebugServer_create(PetriDynamicLib *petri) {
    return new PetriDebugServer{std::make_unique<Petri::DebugServer>(*petri->lib, false)};
}

PetriDebugServer *PetriDebugServer_createClient(PetriDynamicLib *petri) {
    return new PetriDebugServer{std::make_unique<Petri::DebugServer>(*petri->lib, true)};
}

void PetriDebugServer_destroy(PetriDebugServer *server) {
    delete server;
}

void PetriDebugServer_start(PetriDebugServer *server) {
    server->server->start();
}

void PetriDebugServer_stop(PetriDebugServer *server) {
    server->server->stop();
}

bool PetriDebugServer_isRunning(PetriDebugServer *server) {
    return server->server->running();
}

void PetriDebugServer_join(PetriDebugServer *server) {
    server->server->join();
}
