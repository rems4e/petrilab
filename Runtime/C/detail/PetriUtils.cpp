//
//  PetriUtils.c
//  PetriLab
//
//  Created by Rémi Saurel on 2015-07-01.
//

#include "../../Cpp/PetriUtils.h"
#include "../../Cpp/DynamicLib.h"
#include "../../Cpp/PetriDynamicLib.h"
#include "../../Cpp/VarSlot.h"
#include "../../Cpp/Variable.h"
#include "../PetriDynamicLib.h"
#include "../PetriUtils.h"
#include "Types.hpp"
#include <iostream>

extern "C" {
void **_get_Petri_Pyinvoke();
void **_get_Petri_Pyinvoke() {
    static void *_petri_Pyinvoke = NULL;
    return &_petri_Pyinvoke;
}
}

Petri_actionResult_t PetriUtility_pause(double seconds) {
    return Petri::Utility::pause(std::chrono::milliseconds(uint64_t(seconds * 1000)));
}

Petri_actionResult_t PetriUtility_printAction(char const *name, uint64_t id) {
    return Petri::Utility::printAction(name, id);
}

Petri_actionResult_t PetriUtility_doNothing() {
    return Petri::Utility::doNothing();
}

Petri_actionResult_t PetriUtility_printAllVars(struct PetriVarSlot *slot) {
    return Petri::Utility::printAllVars(*slot->slot);
}

Petri_actionResult_t PetriUtility_printText(char const *value) {
    return Petri::Utility::printText(value);
}

int64_t PetriUtility_random(int64_t lowerBound, int64_t upperBound) {
    return Petri::Utility::random(lowerBound, upperBound);
}

PetriDynamicLib *Petri_loadPetriDynamicLib(char const *path, char const *name, bool nodelete) {
    // The lib must be dlopen()ed with the RTLD_NODELETE flag on OS X, otherwise a segfault occurs after it is unloaded.
    Petri::DynamicLib lib(nodelete, path);

    try {
        lib.load();
        auto createPtr = lib.loadSymbol<PetriDynamicLib *()>(std::string{name} + "_createLibForEditor");

        return createPtr();
    } catch(std::exception const &e) {
        std::cerr << e.what() << std::endl;
    }

    return nullptr;
}


char *PetriUtility_loadEvaluateAndInvoke(struct PetriVarSlot *vars, char const *lib, char const *prefix) {
    return Petri::Utility::loadEvaluateAndInvoke(vars, lib, prefix);
}
