//
//  Transition.cpp
//  PetriLab
//
//  Created by Rémi Saurel on 2015-06-25.
//

#define PETRI_NEEDS_GET_TRANSITION

#include "../Transition.h"
#include "../../Cpp/Variable.h"
#include "Transition.hpp"
#include "Types.hpp"
#include <chrono>

void PetriTransition_destroy(PetriTransition *transition) {
    delete transition;
}

PetriAction *PetriTransition_getPrevious(struct PetriTransition *transition) {
    return new PetriAction{nullptr, &getTransition(transition).previous()};
}

PetriAction *PetriTransition_getNext(struct PetriTransition *transition) {
    return new PetriAction{nullptr, &getTransition(transition).next()};
}

uint64_t PetriTransition_getID(PetriTransition *transition) {
    return getTransition(transition).ID();
}

void PetriTransition_setID(PetriTransition *transition, uint64_t id) {
    return getTransition(transition).setID(id);
}

bool PetriTransition_isFulfilled(PetriTransition *transition, Petri_actionResult_t actionResult, PetriVarSlot *vars) {
    return getTransition(transition).isFulfilled(actionResult, *vars->slot);
}

void PetriTransition_setCondition(PetriTransition *transition, transitionCallable_t test) {
    getTransition(transition).setCondition(Petri::make_transition_callable(test));
}

void PetriTransition_setConditionWithParam(PetriTransition *transition, parametrizedTransitionCallable_t test) {
    getTransition(transition).setCondition(getParametrizedTransitionCallable(test));
}

char const *PetriTransition_getName(PetriTransition *transition) {
    return getTransition(transition).name().c_str();
}

void PetriTransition_setName(PetriTransition *transition, char const *name) {
    getTransition(transition).setName(name);
}

uint64_t PetriTransition_getDelayBetweenEvaluation(PetriTransition *transition) {
    return std::chrono::duration_cast<std::chrono::microseconds>(getTransition(transition).delayBetweenEvaluation()).count();
}

void PetriTransition_setDelayBetweenEvaluation(PetriTransition *transition, uint64_t usDelay) {
    getTransition(transition).setDelayBetweenEvaluation(std::chrono::microseconds(usDelay));
}

void PetriTransition_addVariable(PetriTransition *transition, uint32_t id) {
    getTransition(transition).addVariable(id);
}
