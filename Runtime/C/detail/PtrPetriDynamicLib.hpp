//
//  PtrDynamicLib.h
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-15.
//

#ifndef Petri_PtrPetriDynamicLib_h
#define Petri_PtrPetriDynamicLib_h

#include "../../Cpp/MemberPetriDynamicLib.h"
#include "Types.hpp"

namespace Petri {

    class PtrPetriDynamicLib : public MemberPetriDynamicLib {
    public:
        PtrPetriDynamicLib(void *(*createPtr)(), void *(*createDebugPtr)(), char *(*evaluatePtr)(void *, char const *), char const *(*hashPtr)(), char const *name, std::uint16_t port, char const *path = "")
                : MemberPetriDynamicLib(false, true, name, port, path) {
            _createPtr = createPtr;
            _createDebugPtr = createDebugPtr;
            _evaluatePtr = evaluatePtr;
            _hashPtr = hashPtr;
        }

        PtrPetriDynamicLib(PtrPetriDynamicLib const &) = delete;
        PtrPetriDynamicLib &operator=(PtrPetriDynamicLib const &) = delete;

        PtrPetriDynamicLib(PtrPetriDynamicLib &&) = default;
        PtrPetriDynamicLib &operator=(PtrPetriDynamicLib &&) = default;
        virtual ~PtrPetriDynamicLib() = default;

        virtual void load() override {}
        virtual void unload() override {}

        virtual bool loaded() const override {
            return true;
        }
    };
}

#endif /* PtrDynamicLib_h */
