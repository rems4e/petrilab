//
//  Transition.hpp
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-30.
//

#ifndef PETRI_Transition_hpp
#define PETRI_Transition_hpp

#include "../Transition.h"
#include "Types.hpp"

namespace {
    auto getParametrizedTransitionCallable(parametrizedTransitionCallable_t transition) {
        return Petri::make_param_transition_callable([transition](Petri::VarSlot const &s, Petri_actionResult_t a) {
            PetriVarSlot slot{const_cast<Petri::VarSlot *>(&s)};
            return transition(&slot, a);
        });
    }
}


#endif /* Transition_h */
