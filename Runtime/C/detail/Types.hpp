//
//  Types.h
//  PetriLab
//
//  Created by Rémi Saurel on 2015-06-25.
//

#ifndef Petri_Types_hpp
#define Petri_Types_hpp

#include "../../Cpp/Action.h"
#include "../../Cpp/DebugServer.h"
#include "../../Cpp/PetriNet.h"
#include "../../Cpp/Transition.h"
#include "../../Cpp/VarSlot.h"
#include <memory>

class CPetriDynamicLib;

#ifndef NO_C_PETRI_NET

#include "../PetriNet.h"

struct PetriNet {
    std::unique_ptr<Petri::PetriNet> owned;
    Petri::PetriNet *notOwned;
};

#endif

struct PetriAction {
    std::unique_ptr<Petri::Action> owned;
    Petri::Action *notOwned;
};

struct PetriTransition {
    std::unique_ptr<Petri::Transition> owned;
    Petri::Transition *notOwned;
};

struct PetriDynamicLib {
    std::unique_ptr<Petri::PetriDynamicLib> lib;
};

struct PetriDebugServer {
    std::unique_ptr<Petri::DebugServer> server;
    ::PetriNet cHandle;
};

struct PetriVarSlot {
    Petri::VarSlot *slot;
};

namespace {
// The following #ifdef prevent unused functions warning.
#ifdef PETRI_NEEDS_GET_ACTION
    Petri::Action &getAction(PetriAction *action) {
        if(action->owned) {
            return *action->owned;
        } else {
            return *action->notOwned;
        }
    }
#endif
#ifdef PETRI_NEEDS_GET_TRANSITION
    Petri::Transition &getTransition(PetriTransition *transition) {
        if(transition->owned) {
            return *transition->owned;
        } else {
            return *transition->notOwned;
        }
    }
#endif
#ifdef PETRI_NEEDS_GET_PETRINET
    Petri::PetriNet &getPetriNet(PetriNet *pn) {
        if(pn->owned) {
            return *pn->owned;
        } else {
            return *pn->notOwned;
        }
    }
#endif
}

#endif /* Types_h */
