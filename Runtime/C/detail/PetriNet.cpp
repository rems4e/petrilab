//
//  PetriNet.c
//  PetriLab
//
//  Created by Rémi Saurel on 2015-06-25.
//

#include "../../Cpp/PetriNet.h"
#include "../../Cpp/Action.h"
#include "../../Cpp/PetriDebug.h"
#include "../../Cpp/Variable.h"
#include "../Action.h"
#include "../PetriNet.h"

#define PETRI_NEEDS_GET_PETRINET

#include "Types.hpp"

PetriNet *PetriNet_create(char const *name) {
    return new PetriNet{std::make_unique<Petri::PetriNet>(name ? name : "")};
}

PetriNet *PetriNet_createDebug(char const *name) {
    return new PetriNet{std::make_unique<Petri::PetriDebug>(name ? name : "")};
}

PetriNet *PetriNet_createVariablesCount(char const *name, size_t variablesCount) {
    return new PetriNet{std::make_unique<Petri::PetriNet>(name ? name : "", variablesCount)};
}

PetriNet *PetriNet_createDebugVariablesCount(char const *name, size_t variablesCount) {
    return new PetriNet{std::make_unique<Petri::PetriDebug>(name ? name : "", variablesCount)};
}

void PetriNet_destroy(PetriNet *pn) {
    delete pn;
}

void PetriNet_addAction(PetriNet *pn, PetriAction *action, bool active) {
    if(!action->owned) {
        std::cerr << "The action has already been added to a petri net!" << std::endl;
    } else {
        auto &a = getPetriNet(pn).addAction(std::move(*action->owned), active);
        action->owned.reset();
        action->notOwned = &a;
    }
}

bool PetriNet_isRunning(PetriNet *pn) {
    return getPetriNet(pn).running();
}

void PetriNet_run(PetriNet *pn) {
    getPetriNet(pn).run();
}

void PetriNet_stop(PetriNet *pn) {
    getPetriNet(pn).stop();
}

void PetriNet_join(PetriNet *pn) {
    getPetriNet(pn).join();
}

char const *PetriNet_getName(PetriNet *pn) {
    return getPetriNet(pn).name().c_str();
}

size_t PetriNet_getMaxConcurrency(PetriNet *pn) {
    return getPetriNet(pn).maxConcurrency();
}

void PetriNet_setMaxConcurrency(PetriNet *pn, size_t maxConcurrency) {
    getPetriNet(pn).setMaxConcurrency(maxConcurrency);
}

size_t PetriNet_getCurrentConcurrency(PetriNet *pn) {
    return getPetriNet(pn).currentConcurrency();
}

PetriVarSlot *PetriNet_getVariables(struct PetriNet *pn) {
    return getPetriNet(pn).cvariables();
}

char const *const PetriNet_LogOutput_Console = NULL;

void PetriNet_setLogOutputPath(struct PetriNet *pn, char const *outputPath) {
    if(outputPath == PetriNet_LogOutput_Console) {
        outputPath = Petri::PetriNet::LogOutput_Console;
    }

    getPetriNet(pn).setLogOutputPath(outputPath);
}

void PetriNet_setLogVerbosity(struct PetriNet *pn, int verbosity) {
    getPetriNet(pn).setLogVerbosity(verbosity);
}
