//
//  PetriDynamicLib.c
//  PetriLab
//
//  Created by Rémi Saurel on 2015-07-02.
//

#include "../PetriDynamicLib.h"
#include "../../Cpp/MemberPetriDynamicLib.h"
#include "PtrPetriDynamicLib.hpp"
#include "Types.hpp"

PetriDynamicLib *PetriDynamicLib_create(char const *name, uint16_t port, char const *path) {
    return new PetriDynamicLib{std::make_unique<Petri::MemberPetriDynamicLib>(true, name, port, path)};
}

PetriDynamicLib *PetriDynamicLib_createWithPtr(void *(*createPtr)(), void *(*createDebugPtr)(), char *(*evaluatePtr)(void *, char const *), char const *(*hashPtr)(), char const *name, uint16_t port, char const *path) {
    return new PetriDynamicLib{std::make_unique<Petri::PtrPetriDynamicLib>(createPtr, createDebugPtr, evaluatePtr, hashPtr, name, port, path)};
}

void PetriDynamicLib_destroy(PetriDynamicLib *lib) {
    delete lib;
}

PetriNet *PetriDynamicLib_createPetriNet(PetriDynamicLib *lib) {
    try {
        return new PetriNet{lib->lib->createPetriNet()};
    } catch(std::exception const &e) {
        std::cerr << e.what() << std::endl;
        return nullptr;
    }
}

PetriNet *PetriDynamicLib_createDebugPetriNet(PetriDynamicLib *lib) {
    try {
        return new PetriNet{lib->lib->createDebugPetriNet()};
    } catch(std::exception const &e) {
        std::cerr << e.what() << std::endl;
        return nullptr;
    }
}

char const *PetriDynamicLib_getHash(PetriDynamicLib *lib) {
    return lib->lib->hash().c_str();
}

uint16_t PetriDynamicLib_getPort(PetriDynamicLib *lib) {
    return lib->lib->port();
}

bool PetriDynamicLib_load(PetriDynamicLib *lib) {
    try {
        lib->lib->load();

        return true;
    } catch(std::exception &e) {
        std::cerr << e.what() << std::endl;
        return false;
    }
}

void PetriDynamicLib_unload(PetriDynamicLib *lib) {
    lib->lib->unload();
}

char const *PetriDynamicLib_getPath(PetriDynamicLib *lib) {
    return lib->lib->path().c_str();
}

char const *PetriDynamicLib_getName(PetriDynamicLib *lib) {
    return lib->lib->name();
}
