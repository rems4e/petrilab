//
//  VarSlot.cpp
//  PetriLab
//
//  Created by Rémi Saurel on 2017-03-18.
//

#include "../VarSlot.h"
#include "../../Cpp/Variable.h"
#include "../../Cpp/detail/multiheadstack/MultiHeadStack.h"
#include "Types.hpp"


PetriVarSlot *PetriVarSlot_create(size_t capacity) {
    auto stack = MultiHeadStackNS::MultiHeadStack<Petri::VarSlot::Var>{};
    auto ptr = stack.push(Petri::VarSlot::Var{0, std::vector<Petri::Variable>(capacity)}).shared_from_this();
    return new PetriVarSlot{new Petri::VarSlot{ptr}};
}

void PetriVarSlot_destroy(PetriVarSlot *slot) {
    delete slot->slot;
    delete slot;
}

size_t PetriVarSlot_getSize(struct PetriVarSlot *slot) {
    return slot->slot->size();
}

volatile int64_t *PetriVarSlot_getVariable(struct PetriVarSlot *slot, uint32_t id) {
    return &slot->slot->operator[](id).value();
}

int64_t PetriVarSlot_getVariableValue(struct PetriVarSlot *slot, uint32_t id) {
    return slot->slot->operator[](id).value();
}

void PetriVarSlot_setVariableValue(struct PetriVarSlot *slot, uint32_t id, int64_t value) {
    slot->slot->operator[](id).value() = value;
}

void PetriVarSlot_setVariableDefaultValue(struct PetriVarSlot *slot, uint32_t id, int64_t value) {
    slot->slot->operator[](id).setDefaultValue(value);
}

char const *PetriVarSlot_getVariableName(struct PetriVarSlot *slot, uint32_t id) {
    return slot->slot->operator[](id).name().c_str();
}

void PetriVarSlot_setVariableName(struct PetriVarSlot *slot, uint32_t id, char const *value) {
    slot->slot->operator[](id).setName(value);
}

void PetriVarSlot_pushVariables(struct PetriVarSlot *slot, size_t count) {
    slot->slot->pushVariables(count);
}

void PetriVarSlot_pushReturnValues(struct PetriVarSlot *slot, size_t count) {
    slot->slot->pushReturnValues(count);
}

void PetriVarSlot_pop(struct PetriVarSlot *slot) {
    slot->slot->pop();
}

bool PetriVarSlot_isFirstSlot(struct PetriVarSlot *slot) {
    return slot->slot->isFirstSlot();
}

bool PetriVarSlot_isReturnValues(struct PetriVarSlot *slot) {
    return slot->slot->isReturnValues();
}
