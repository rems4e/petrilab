//
//  Transition.h
//  PetriLab
//
//  Created by Rémi Saurel on 2015-06-25.
//

#ifndef Petri_CTransition_H
#define Petri_CTransition_H

#include "Types.h"
#include <stdbool.h>
#include <time.h>

#ifdef __cplusplus
extern "C" {
#endif
struct PetriTransition;
struct PetriAction;
struct PetriVarSlot;

typedef bool (*transitionCallable_t)(Petri_actionResult_t);
typedef bool (*parametrizedTransitionCallable_t)(struct PetriVarSlot *, Petri_actionResult_t);

/**
 * Destroys a PetriAction instance created by one of the PetriAction_create functions.
 * @param transition The PetriTransition instance to destroy.
 */
void PetriTransition_destroy(struct PetriTransition *transition);

struct PetriAction *PetriTransition_getPrevious(struct PetriTransition *transition);
struct PetriAction *PetriTransition_getNext(struct PetriTransition *transition);

/**
 * Returns the ID of the PetriTransition.
 * @param transition The PetriTransition to query.
 */
uint64_t PetriTransition_getID(struct PetriTransition *transition);

/**
 * Changes the ID of the PetriTransition.
 * @param transition The PetriTransition to change.
 * @param id The new ID.
 */
void PetriTransition_setID(struct PetriTransition *transition, uint64_t id);

/**
 * Checks whether the PetriTransition can be crossed
 * @param transition The PetriTransition instance to test against.
 * @param actionResult The result of the Action 'previous'. This is useful when the
 * PetriTransition's test uses this value.
 * @param vars The slot giving access to the transition's variables.
 * @return The result of the test, true meaning that the PetriTransition can be crossed to enable
 * the action 'next'
 */
bool PetriTransition_isFulfilled(struct PetriTransition *transition, Petri_actionResult_t actionResult, struct PetriVarSlot *vars);

/**
 * Changes the condition associated to the PetriTransition
 * @param transition The PetriTransition instance to change.
 * @param test The new condition to associate to the PetriTransition
 */
void PetriTransition_setCondition(struct PetriTransition *transition, transitionCallable_t test);
void PetriTransition_setConditionWithParam(struct PetriTransition *transition, parametrizedTransitionCallable_t test);

/**
 * Gets the name of the PetriTransition.
 * @param transition The PetriTransition instance to query.
 * @return The name of the PetriTransition.
 */
char const *PetriTransition_getName(struct PetriTransition *transition);

/**
 * Changes the name of the PetriTransition.
 * @param transition The PetriTransition instance to change.
 * @param name The new name of the PetriTransition.
 */
void PetriTransition_setName(struct PetriTransition *transition, char const *name);

/**
 * The delay in microseconds between successive evaluations of the PetriTransition. The runtime will
 * not try to evaluate
 * the PetriTransition with a delay smaller than this delay after a previous evaluation, but only
 * for one execution of PetriAction 'previous'
 * @param transition The PetriTransition instance to query.
 * @return The minimal delay between two evaluations of the PetriTransition.
 */
uint64_t PetriTransition_getDelayBetweenEvaluation(struct PetriTransition *transition);

/**
 * Changes the delay between successive evaluations of the PetriTransition.
 * @param transition The PetriTransition instance to change.
 * @param usDelay The new minimal delay in microseconds between two evaluations of the
 * PetriTransition.
 */
void PetriTransition_setDelayBetweenEvaluation(struct PetriTransition *transition, uint64_t usDelay);

/**
 * References the variable in the transition
 * @param transition The transition
 * @param id The identifier of the variable
 */
void PetriTransition_addVariable(struct PetriTransition *transition, uint32_t id);

#ifdef __cplusplus
}
#endif

#endif /* Petri_CTransition_H */
