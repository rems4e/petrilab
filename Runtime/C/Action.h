//
//  Action.h
//  PetriLab
//
//  Created by Rémi Saurel on 2015-06-25.
//

#ifndef Petri_CAction_H
#define Petri_CAction_H

#include "Transition.h"
#include "Types.h"
#include <stdint.h>
#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

struct PetriNet;
struct PetriVarSlot;

typedef Petri_actionResult_t (*callable_t)();
typedef Petri_actionResult_t (*parametrizedCallable_t)(struct PetriVarSlot *);

// typedef struct PetriAction PetriAction;

/**
 * Creates an empty action, associated to a null CallablePtr.
 */
struct PetriAction *PetriAction_createEmpty();

/**
 * Creates an empty action, associated to the specified Callable.
 * @param id The ID of the new action.
 * @param name The name of the new action.
 * @param action The Callable which will be called when the action is run.
 * @param requiredTokens The number of tokens that must be inside the active action for it to
 * execute.
 * @return The newly created action.
 */
struct PetriAction *PetriAction_create(uint64_t id, char const *name, callable_t action, uint32_t requiredTokens);

/**
 * Creates an empty action, associated to the specified Callable.
 * @param id The ID of the new action.
 * @param name The name of the new action.
 * @param action The Callable which will be called when the action is run.
 * @param requiredTokens The number of tokens that must be inside the active action for it to
 * execute.
 * @return The newly created action.
 */
struct PetriAction *PetriAction_createWithParam(uint64_t id, char const *name, parametrizedCallable_t action, uint32_t requiredTokens);

/**
 * Destroys a PetriAction instance created by one of the PetriAction_create functions.
 * @param action The PetriAction instance to destroy.
 */
void PetriAction_destroy(struct PetriAction *action);

/**
 * Returns the ID of the PetriAction.
 * @param action The PetriAction to query.
 */
uint64_t PetriAction_getID(struct PetriAction *action);

/**
 * Changes the ID of the PetriAction.
 * @param action The PetriAction to change.
 * @param id The new ID.
 */
void PetriAction_setID(struct PetriAction *action, uint64_t id);

/**
 * Adds a PetriTransition to the PetriAction.
 * @param action The PetriAction instance to add the PetriTransition to.
 * @param id The id of the Transition
 * @param name The name of the transition to be added
 * @param next The Action following the transition to be added
 * @param cond The condition of the Transition to be added
 * @return The transition newly created.
 */
struct PetriTransition *PetriAction_addTransition(struct PetriAction *action, uint64_t id, char const *name, struct PetriAction *next, transitionCallable_t cond);
struct PetriTransition *PetriAction_addTransitionWithParam(struct PetriAction *action, uint64_t id, char const *name, struct PetriAction *next, parametrizedTransitionCallable_t cond);

/**
 * Adds a PetriTransition to the PetriAction.
 * @param action The PetriAction instance to add the PetriTransition to.
 * @param next The Action following the transition to be added
 * @return The transition newly created.
 */
struct PetriTransition *PetriAction_addEmptyTransition(struct PetriAction *action, struct PetriAction *next);

/**
 * Changes the action associated to the PetriAction
 * @param action The PetriAction instance of which the action will be changed.
 * @param a The Callable which will be copied and put in the Action
 */
void PetriAction_setAction(struct PetriAction *action, callable_t a);

/**
 * Changes the action associated to the PetriAction
 * @param action The PetriAction instance of which the action will be changed.
 * @param a The Callable which will be copied and put in the Action
 */
void PetriAction_setActionParam(struct PetriAction *action, parametrizedCallable_t a);

/**
 * Returns the required tokens of the Action to be activated, i.e. the count of Actions which must
 * lead to *this and terminate for *this to activate.
 * @return The required tokens of the Action
 */
uint32_t PetriAction_getRequiredTokens(struct PetriAction *action);

/**
 * Changes the required tokens of the Action to be activated.
 * @param requiredTokens The new required tokens count
 */
void PetriAction_setRequiredTokens(struct PetriAction *action, uint32_t requiredTokens);

/**
 * Gets the current tokens count given to the Action by its preceding Actions.
 * @return The current tokens count of the Action
 */
uint32_t PetriAction_getCurrentTokens(struct PetriAction *action);

/**
 * Returns the name of the Action.
 * @return The name of the Action
 */
char const *PetriAction_getName(struct PetriAction *action);

/**
 * Sets the name of the Action
 * @param name The name of the Action
 */
void PetriAction_setName(struct PetriAction *action, char const *name);

/**
 * References the variable in the action
 * @param action The action
 * @param id The identifier of the variable
 */
void PetriAction_addVariable(struct PetriAction *action, uint32_t id);

/**
 * Gets the value of variable designated by the specified id.
 * @param a The action that contains the variable.
 * @param id The id of the new variable.
 * @return The value of the variable.
 */
volatile int64_t *PetriAction_getVariable(struct PetriAction *a, uint32_t id);


#ifdef __cplusplus
}
#endif

#endif /* Petri_CAction_H */
