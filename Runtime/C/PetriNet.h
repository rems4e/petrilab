//
//  PetriNet.h
//  PetriLab
//
//  Created by Rémi Saurel on 2015-06-25.
//

#ifndef Petri_CPetriNet_H
#define Petri_CPetriNet_H

#include <stdbool.h>
#include <stdint.h>

#include "Action.h"

#ifdef __cplusplus
extern "C" {
#endif

// typedef struct PetriNet PetriNet;

/**
 * Creates the PetriNet, assigning it a name which serves debug purposes
 * @param name The name to assign to the PetriNet, or a designated one if empty or NULL
 * @return The PetriNet instance, or NULL if an error occurred.
 */
struct PetriNet *PetriNet_create(char const *name);

/**
 * Creates the PetriNet, along with some debugging facilities.
 * @param name The name to assign to the PetriNet, or a designated one if empty or NULL
 * @return The PetriNet instance, or NULL if an error occurred.
 */
struct PetriNet *PetriNet_createDebug(char const *name);

/**
 * Creates the PetriNet, assigning it a name which serves debug purposes, and providing a number of variables.
 * @param name The name to assign to the PetriNet, or a designated one if empty or NULL.
 * @param variablesCount The count of variables of the petri net.
 * @return The PetriNet instance, or NULL if an error occurred.
 */
struct PetriNet *PetriNet_createVariablesCount(char const *name, size_t variablesCount);

/**
 * Creates the PetriNet, along with some debugging facilities, and providing a number of variables.
 * @param name The name to assign to the PetriNet, or a designated one if empty or NULL.
 * @param variablesCount The count of variables of the petri net.
 * @return The PetriNet instance, or NULL if an error occurred.
 */
struct PetriNet *PetriNet_createDebugVariablesCount(char const *name, size_t variablesCount);

/**
 * Destroys a PetriNet instance created by the PetriNet_create* functions.
 * @param pn The PetriNet instance to destroy.
 */
void PetriNet_destroy(struct PetriNet *pn);

/**
 * Adds a PetriAction to the PetriNet. The net must not be running yet.
 * Once this function has been called, the handle to the action may not
 * be added to a petri net again, but is still needs to be free()d.
 * @param pn The Petri Net to add add the action to
 * @param action The action to add
 * @param active Controls whether the action is active as soon as the petri net is started or not.
 */
void PetriNet_addAction(struct PetriNet *pn, struct PetriAction *action, bool active);

/**
 * Checks whether the net is running.
 * @param pn The Petri Net on which the test will be performed
 * @return true means that the net has been started, and we can not add any more action to it now.
 */
bool PetriNet_isRunning(struct PetriNet *pn);

/**
 * Starts the Petri net. It must not be already running. If no states are initially active, this is
 * a no-op.
 * @param pn The Petri Net to start
 */
void PetriNet_run(struct PetriNet *pn);

/**
 * Stops the Petri net. It blocks the calling thread until all running states are finished,
 * but do not allows new states to be enabled. If the net is not running, this is a no-op.
 * @param pn The Petri Net to stop.
 */
void PetriNet_stop(struct PetriNet *pn);

/**
 * Blocks the calling thread until the Petri net has completed its whole execution.
 * @param pn The Petri Net to join.
 */
void PetriNet_join(struct PetriNet *pn);

/**
 * Returns a string representing the name of the petri net, and whose lifetime is the same as the PetriNet's instance.
 * @param pn The Petri Net.
 * @return The name of the petri net.
 */
char const *PetriNet_getName(struct PetriNet *pn);

/**
 * Returns the max number of threads executing the petri net.
 * @param pn The Petri Net.
 * @return The max concurrency of the petri net.
 */
size_t PetriNet_getMaxConcurrency(struct PetriNet *pn);

/**
 * Sets the max number of threads executing the petri net.
 * @param pn The Petri Net.
 * @param maxConcurrency The max number of threads executing the petri net.
 */
void PetriNet_setMaxConcurrency(struct PetriNet *pn, size_t maxConcurrency);

/**
 * Returns the current number of threads executing the petri net.
 * @param pn The Petri Net.
 * @return The current number of threads executing the petri net.
 */
size_t PetriNet_getCurrentConcurrency(struct PetriNet *pn);

/**
 * Gets the initial variables slot of the petri net.
 * Its lifetime is bound to the petri net's.
 * @param pn The Petri Net.
 */
struct PetriVarSlot *PetriNet_getVariables(struct PetriNet *pn);

/*
* The flags enum that configures how much is logged during a petri net execution.
* Its members can be OR'ed to achieve the desired log output.
*/
enum PetriNet_LogVerbosity {
    PetriNet_VerbosityNothing = 0x0,      /**< Do not log anything. */
    PetriNet_VerbosityStates = 0x1,       /**< Log states with their result. */
    PetriNet_VerbosityTransitions = 0x2,  /**< Log transitions with their result. */
    PetriNet_VerbosityReturnValues = 0x4, /**< Log return values name and values. */
};

/**
* A special value meant to be used with the setLogOutputPath method, which signals the log to be output to the
* console.
* @see setLogOutputPath
*/
extern char const *const PetriNet_LogOutput_Console;

/**
 * Sets the petri net's log output to the specified filename, or PetriNet_LogOutput_Console to set the output to stdout.
 * @param outputPath The path of the log file to append to, or PetriNet_LogOutput_Console if the logs are to output to
 * stdout.
 * @param pn The Petri Net.
 * @see PetriNet_LogOutput_Console
 */
void PetriNet_setLogOutputPath(struct PetriNet *pn, char const *outputPath);

/**
 * Changes the verbosity of the log output. Default is PetriNet_VerbosityNone.
 * @param verbosity The new verbosity level.
 * @param pn The Petri Net.
 * @see PetriNet_LogVerbosity
 */
void PetriNet_setLogVerbosity(struct PetriNet *pn, int verbosity);


#ifdef __cplusplus
}
#endif

#endif /* Petri_CPetriNet_H */
