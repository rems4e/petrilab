//
//  PetriDynamicLib.h
//  PetriLab
//
//  Created by Rémi Saurel on 2015-07-02.
//

#ifndef Petri_CPetriDynamicLib_H
#define Petri_CPetriDynamicLib_H

#include <stdbool.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

// typedef struct PetriDynamicLib PetriDynamicLib;

/**
 * Creates a dynamic library handle to the specified name, port and path.
 * The wrapped library must contain the externally visible symbols <name>_create(), <name>_createDebug() and <name>_getHash() symbols,
 * returning respectively a PetriNet *, a PetriNet * and a char const *.
 * @return The newly created handle.
 */
struct PetriDynamicLib *PetriDynamicLib_create(char const *name, uint16_t port, char const *path);

/**
 * Create a dynamic library handle with the specified function ptr.
 * No dynamic library is actually loaded, but this provides a convenient interface for being used by the DebugServer API.
 */
struct PetriDynamicLib *PetriDynamicLib_createWithPtr(void *(*createPtr)(), void *(*createDebugPtr)(), char *(*evaluatePtr)(void *, char const *), char const *(*hashPtr)(), char const *name, uint16_t port, char const *path);

/**
* Destroys the specified dynamic library handle.
* @param lib The dynamic library handle to destroy.
*/
void PetriDynamicLib_destroy(struct PetriDynamicLib *lib);

/**
 * Creates the PetriNet as contained in the dynamic library.
 * @param lib The dynamic library handle to extract the PetriNet from.
 * @return The newly created PetriNet, or NULL if the lib is not load()ed.
 */
struct PetriNet *PetriDynamicLib_createPetriNet(struct PetriDynamicLib *lib);

/**
 * Creates the PetriNet as contained in the dynamic library, along with debugging facilities.
 * @param lib The dynamic library handle to extract the PetriNet from.
 * @return The newly created PetriNet, or NULL if the lib is not load()ed.
 */
struct PetriNet *PetriDynamicLib_createDebugPetriNet(struct PetriDynamicLib *lib);

/**
 * Returns the SHA256 hash string that identifies the PetriNet contained in the library.
 * @param lib The dynamic library handle containing the PetriNet.
 * @return The PetriNet's SHA256 hash string.
 */
char const *PetriDynamicLib_getHash(struct PetriDynamicLib *lib);

/**
 * Returns the name of the PetriNet contained in the library.
 * @param lib The dynamic library handle containing the PetriNet.
 * @return The PetriNet's name.
 */
char const *PetriDynamicLib_getName(struct PetriDynamicLib *lib);

/**
 * Returns the TCP port on which the debugger will try to attach to.
 * @param lib The dynamic library handle containing the PetriNet.
 * @return The PetriNet's debugging TCP port.
 */
uint16_t PetriDynamicLib_getPort(struct PetriDynamicLib *lib);

/**
 * Loads the symbols contained in the dynamic library, resulting in an error if they are not
 * available
 * @param lib The dynamic library handle containing the PetriNet.
 * @return Whether the load succeeded.
 */
bool PetriDynamicLib_load(struct PetriDynamicLib *lib);

/**
 * Unloads the dynamic library.
 * @param lib The dynamic library handle containing the PetriNet.
 */
void PetriDynamicLib_unload(struct PetriDynamicLib *lib);

/**
 * Returns the path of the dynamic library, relative to the main executable.
 * @param lib The dynamic library handle containing the PetriNet.
 * @return The dynamic library's relative path.
 */
char const *PetriDynamicLib_getPath(struct PetriDynamicLib *lib);

#ifdef __cplusplus
}
#endif

#endif /* Petri_CPetriDynamicLib_H */
