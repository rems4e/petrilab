# coding: utf-8

#
#  PetriDynamicLib.py
#  PetriLab
#
#  Created by Rémi Saurel on 2017-11-30.
#

from . import PetriNet


class _PetriDynamicLib_Interop(object):
    def __init__(self):
        from ctypes import cdll, c_char_p, c_void_p, c_bool, c_uint16
        petri_runtime = cdll.LoadLibrary('libPetriRuntime.so')

        self.petridynamiclib_create = petri_runtime.PetriDynamicLib_create
        self.petridynamiclib_create.restype = c_void_p
        self.petridynamiclib_create.argtypes = [c_char_p, c_uint16, c_char_p]

        self.petridynamiclib_delete = petri_runtime.PetriDynamicLib_destroy
        self.petridynamiclib_delete.restype = None
        self.petridynamiclib_delete.argtypes = [c_void_p]

        self.petridynamiclib_create_petri_net = petri_runtime.PetriDynamicLib_createPetriNet
        self.petridynamiclib_create_petri_net.restype = c_void_p
        self.petridynamiclib_create_petri_net.argtypes = [c_void_p]

        self.petridynamiclib_create_debug_petri_net = petri_runtime.PetriDynamicLib_createDebugPetriNet
        self.petridynamiclib_create_debug_petri_net.restype = c_void_p
        self.petridynamiclib_create_debug_petri_net.argtypes = [c_void_p]

        self.petridynamiclib_get_hash = petri_runtime.PetriDynamicLib_getHash
        self.petridynamiclib_get_hash.restype = c_char_p
        self.petridynamiclib_get_hash.argtypes = [c_void_p]

        self.petridynamiclib_get_name = petri_runtime.PetriDynamicLib_getName
        self.petridynamiclib_get_name.restype = c_char_p
        self.petridynamiclib_get_name.argtypes = [c_void_p]

        self.petridynamiclib_get_port = petri_runtime.PetriDynamicLib_getPort
        self.petridynamiclib_get_port.restype = c_uint16
        self.petridynamiclib_get_port.argtypes = [c_void_p]

        self.petridynamiclib_load = petri_runtime.PetriDynamicLib_load
        self.petridynamiclib_load.restype = c_bool
        self.petridynamiclib_load.argtypes = [c_void_p]

        self.petridynamiclib_unload = petri_runtime.PetriDynamicLib_unload
        self.petridynamiclib_unload.restype = None
        self.petridynamiclib_unload.argtypes = [c_void_p]

        self.petridynamiclib_get_path = petri_runtime.PetriDynamicLib_getPath
        self.petridynamiclib_get_path.restype = c_char_p
        self.petridynamiclib_get_path.argtypes = [c_void_p]

_PetriDynamicLib_Interop_Instance = _PetriDynamicLib_Interop()


class PetriDynamicLib(object):
    def __init__(self, name, port, path=""):
        self._handle = _PetriDynamicLib_Interop_Instance.petridynamiclib_create(name.encode("utf-8"), port, path.encode("utf-8"))

    def __del__(self):
        _PetriDynamicLib_Interop_Instance.petridynamiclib_delete(self._handle)

    def create_petri_net(self):
        return PetriNet.PetriNet(_handle=_PetriDynamicLib_Interop_Instance.petridynamiclib_create_petri_net(self._handle))

    def create_debug_petri_net(self):
        return PetriNet.PetriNet(_handle=_PetriDynamicLib_Interop_Instance.petridynamiclib_create_debug_petri_net(self._handle))

    def get_hash(self):
        return _PetriDynamicLib_Interop_Instance.petridynamiclib_get_hash(self._handle)

    def get_name(self):
        return _PetriDynamicLib_Interop_Instance.petridynamiclib_get_name(self._handle).decode("utf-8")

    def get_port(self):
        return _PetriDynamicLib_Interop_Instance.petridynamiclib_get_port(self._handle)

    def load(self):
        return _PetriDynamicLib_Interop_Instance.petridynamiclib_load(self._handle)

    def unload(self):
        _PetriDynamicLib_Interop_Instance.petridynamiclib_unload(self._handle)

    def get_path(self):
        return _PetriDynamicLib_Interop_Instance.petridynamiclib_get_path(self._handle).decode("utf-8")
