//
//  PetriInvoke.h
//  PetriLab
//
//  Created by Rémi Saurel on 2017-11-19.
//

#ifndef Petri_PythonPetriInvoke_H
#define Petri_PythonPetriInvoke_H

static char PetriRuntime_py[] = {
#include "PetriRuntime.xxd"
,
0x00};

static void Petri_initPython() {
    extern void **_get_Petri_Pyinvoke();

    if(*_get_Petri_Pyinvoke() == NULL) {
        Py_InitializeEx(0);
        bool release = false;
        bool needsInit = !PyEval_ThreadsInitialized();
        PyGILState_STATE gstate;
        if(needsInit) { // Setup of the GIL
            PyEval_InitThreads();
            release = true;
        } else {
            gstate = PyGILState_Ensure();
        }

#if PY_MAJOR_VERSION == 2
        char
#else
        wchar_t
#endif
        argv[1] = {'\0'},
        *ptr;
        ptr = argv;
        PySys_SetArgv(0, &ptr); // Required for relative imports

        PyObject *builtins = PyEval_GetBuiltins();
        PyObject *compile = PyDict_GetItemString(builtins, "compile");
        PyObject *code = PyObject_CallFunction(compile, "sss", PetriRuntime_py, "PetriRuntime.py", "exec");
        PyObject *petriRuntimeModule = PyImport_ExecCodeModule("PetriRuntime", code);
        PyObject *petriRuntimeDict = PyModule_GetDict(petriRuntimeModule);

        for(int i = 0; i < sizeof(_PETRI_PYTHON_MODULES_) / sizeof(*_PETRI_PYTHON_MODULES_); ++i) {
            PyObject *module = PyImport_ImportModule(_PETRI_PYTHON_MODULES_[i]);
            if(module == NULL) {
                fprintf(stderr, "Could not import module %s!\n", _PETRI_PYTHON_MODULES_[i]);
                fflush(stderr);
                if(PyErr_Occurred()) {
                    PyErr_PrintEx(0);
                    fflush(stderr);
                }
            }
            PyObject_SetAttrString(petriRuntimeModule, _PETRI_PYTHON_MODULES_[i], module);
        }

        *_get_Petri_Pyinvoke() = PyDict_GetItemString(petriRuntimeDict, "PetriRuntime_invoke");

        if(needsInit) {
            PyEval_SaveThread();
        } else {
            PyGILState_Release(gstate);
        }
    }
}

static PyObject *_Petri_invokePythonToObject(char const *invocation, Petri_actionResult_t actionResult, struct PetriVarSlot *slot) {
    static PyObject *varEnum;
    extern void **_get_Petri_Pyinvoke();

    if(varEnum == NULL) {
        varEnum = _PETRI_VAR_ENUM_INITIALIZER_;
    }

    return PyObject_CallFunction((PyObject *)*_get_Petri_Pyinvoke(), "sNlO", invocation, PyLong_FromVoidPtr(slot), (long)actionResult, varEnum);
}

static char *Petri_invokePythonToString(char const *invocation, Petri_actionResult_t actionResult, struct PetriVarSlot *slot) {
    char *result;

    PyGILState_STATE gstate = PyGILState_Ensure();
    PyObject *pyresult = _Petri_invokePythonToObject(invocation, actionResult, slot);
    PyObject *as_str = PyObject_Str(pyresult);
    Py_XDECREF(pyresult);
#if PY_MAJOR_VERSION == 2
    char const *as_string = PyString_AsString(as_str);
    result = malloc(strlen(as_string) + 1);
    strcpy(result, as_string);
#else
    PyObject *as_unicode = PyUnicode_AsUTF8String(as_str);
    const char *as_string = PyBytes_AS_STRING(as_unicode);
    result = malloc(strlen(as_string) + 1);
    strcpy(result, as_string);
    Py_XDECREF(as_unicode);
#endif
    Py_DECREF(as_str);
    PyGILState_Release(gstate);

    return result;
}

static Petri_actionResult_t Petri_invokePython(char const *invocation, Petri_actionResult_t actionResult, struct PetriVarSlot *slot) {
    long result = 0;

    PyGILState_STATE gstate = PyGILState_Ensure();
    PyObject *pyresult = _Petri_invokePythonToObject(invocation, actionResult, slot);
    if(PyLong_Check(pyresult)) {
        result = PyLong_AsLong(pyresult);
#if PY_MAJOR_VERSION == 2
    } else if(PyInt_Check(pyresult)) {
        result = PyInt_AsLong(pyresult);
#endif
    } else {
        fprintf(stderr, "The result of %s was not representable as a long!\n", invocation);
        fflush(stderr);
    }
    Py_XDECREF(pyresult);
    PyGILState_Release(gstate);

    return result;
}

#endif // Petri_PythonPetriInvoke_H
