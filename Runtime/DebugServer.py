# coding: utf-8

#
#  DebugServer.py
#  PetriLab
#
#  Created by Rémi Saurel on 2017-11-30.
#


class _DebugServer_Interop(object):
    def __init__(self):
        from ctypes import cdll, c_char_p, c_void_p, c_bool
        petri_runtime = cdll.LoadLibrary('libPetriRuntime.so')

        self.debugserver_get_version = petri_runtime.PetriDebugServer_getVersion
        self.debugserver_get_version.restype = c_char_p
        self.debugserver_get_version.argtypes = []

        self.debugserver_create = petri_runtime.PetriDebugServer_create
        self.debugserver_create.restype = c_void_p
        self.debugserver_create.argtypes = [c_void_p]

        self.debugserver_create_client = petri_runtime.PetriDebugServer_createClient
        self.debugserver_create_client.restype = c_void_p
        self.debugserver_create_client.argtypes = [c_void_p]

        self.debugserver_delete = petri_runtime.PetriDebugServer_destroy
        self.debugserver_delete.restype = None
        self.debugserver_delete.argtypes = [c_void_p]

        self.debugserver_is_running = petri_runtime.PetriDebugServer_isRunning
        self.debugserver_is_running.restype = c_bool
        self.debugserver_is_running.argtypes = [c_void_p]

        self.debugserver_start = petri_runtime.PetriDebugServer_start
        self.debugserver_start.restype = None
        self.debugserver_start.argtypes = [c_void_p]

        self.debugserver_stop = petri_runtime.PetriDebugServer_stop
        self.debugserver_stop.restype = None
        self.debugserver_stop.argtypes = [c_void_p]

        self.debugserver_join = petri_runtime.PetriDebugServer_join
        self.debugserver_join.restype = None
        self.debugserver_join.argtypes = [c_void_p]

_DebugServer_Interop_Instance = _DebugServer_Interop()


class DebugServer(object):
    @staticmethod
    def get_version(self):
        return _DebugServer_Interop_Instance.debugserver_get_version()

    def __init__(self, lib, is_client=False):
        self._lib = lib  # refcount
        if is_client:
            self._handle = _DebugServer_Interop_Instance.debugserver_create_client(self._lib._handle)
        else:
            self._handle = _DebugServer_Interop_Instance.debugserver_create(self._lib._handle)

    def __del__(self):
        _DebugServer_Interop_Instance.debugserver_delete(self._handle)

    def is_running(self):
        return _DebugServer_Interop_Instance.debugserver_is_running(self._handle)

    def start(self):
        _DebugServer_Interop_Instance.debugserver_start(self._handle)

    def stop(self):
        _DebugServer_Interop_Instance.debugserver_stop(self._handle)

    def join(self):
        _DebugServer_Interop_Instance.debugserver_join(self._handle)
