//
//  DebugServer.h
//  PetriLab
//
//  Created by Rémi Saurel on 2014-11-23.
//

#ifndef Petri_DebugServer_h
#define Petri_DebugServer_h

#include "Callable.h"
#include "Common.h"
#include <chrono>
#include <string>

struct PetriDynamicLib;

namespace Petri {

    using namespace std::string_literals;
    using namespace std::chrono_literals;

    class PetriDynamicLib;
    class PetriDebug;
    class Action;

    class DebugServer {
        friend class PetriDebug;

    public:
        /**
         * Returns the DebugServer API's version
         * @return The current version of the API.
         */
        static std::string const &getVersion();

        /**
         * Creates the DebugServer and binds it to the provided dynamic library.
         * @param petri The dynamic lib from which the debug server operates.
         * @param isClient If true, the debug server will act as a client and will not output log
         * messages to stdout.
         */
        DebugServer(PetriDynamicLib &petri, bool isClient = false);

        /**
         * Destroys the debug server. If the server is running, a deleted or out of scope
         * DebugServer
         * object will wait for the connected client to end the debug session to continue the
         * program exectution.
         */
        ~DebugServer();

        DebugServer(DebugServer const &) = delete;
        DebugServer &operator=(DebugServer const &) = delete;

        /**
         * Starts the debug server by listening on the debug port of the bound dynamic library,
         * making it ready to receive a debugger connection.
         */
        void start();

        /**
         * Stops the debug server. After that, the debugging port is unbound.
         */
        void stop();

        /**
         * Checks whether the debug server is running or not.
         * @return true if the server is running, false otherwise.
         */
        bool running() const;

        /**
         * Waits for the debug server session to end.
         */
        void join() const;

    protected:
        void addActiveState(Action &a);
        void removeActiveState(Action &a);
        void notifyStop();

    private:
        struct Internals;
        std::unique_ptr<Internals> _internals;
    };
}

#endif
