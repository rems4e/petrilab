//
//  PetriDynamicLibCommon.h
//  PetriLab
//
//  Created by Rémi Saurel on 2014-11-22.
//

#ifndef Petri_PetriDynamicLib_h
#define Petri_PetriDynamicLib_h

#include "DynamicLib.h"
#include "PetriDebug.h"
#include "PetriUtils.h"
#include <memory>

#include <iostream>

namespace Petri {
    class VarSlot;

    class PetriDynamicLib : public DynamicLib {
    public:
        /**
         * Creates the dynamic library wrapper. It still needs to be loaded to make it possible to
         * create the PetriNet objects.
         */
        PetriDynamicLib(bool c_dynamicLib, std::string const &path)
                : DynamicLib(false, path)
                , _c_dynamicLib(c_dynamicLib) {}
        PetriDynamicLib(PetriDynamicLib const &pn) = delete;
        PetriDynamicLib &operator=(PetriDynamicLib const &pn) = delete;

        PetriDynamicLib(PetriDynamicLib &&pn) = default;
        PetriDynamicLib &operator=(PetriDynamicLib &&pn) = default;
        virtual ~PetriDynamicLib();

        /**
         * Creates the PetriNet object according to the code contained in the dynamic library.
         * @return The PetriNet object wrapped in a std::unique_ptr
         */
        std::unique_ptr<PetriNet> createPetriNet();

        /**
         * Creates the PetriDebug object according to the code contained in the dynamic library.
         * @return The PetriDebug object wrapped in a std::unique_ptr
         */
        std::unique_ptr<PetriDebug> createDebugPetriNet();

        std::string evaluate(VarSlot &varSlot, char const *lib);

        /**
         * Returns the SHA256 hash of the dynamic library. It uniquely identifies the code of the
         * PetriNet,
         * so that a different or modified petri net has a different hash print
         * @return The dynamic library hash
         */
        std::string hash() const {
            if(!this->loaded()) {
                throw std::runtime_error("PetriDynamicLib::hash: Dynamic library not loaded!");
            }
            return std::string(_hashPtr());
        }

        /**
         * Returns the TCP port on which a DebugSession initialized with this wrapper will listen to
         * debugger connection.
         * @return The TCP port which will be used by DebugSession
         */
        virtual uint16_t port() const = 0;

        /**
         * Loads the dynamic library associated to this wrapper.
         * @throws std::runtime_error on two occasions: when the dylib could not be found (wrong
         * path, missing file, wrong architecture or other error), or when the debug server's code
         * has been changed (impliying the dylib has to be recompiled).
         */
        virtual void load() override {
            if(this->loaded()) {
                return;
            }

            this->DynamicLib::load();

            std::string const name = this->name();

            // Accesses the newly loaded symbols
            _createPtr = this->loadSymbol<void *()>((name + "_create").c_str());
            _createDebugPtr = this->loadSymbol<void *()>((name + "_createDebug").c_str());
            _evaluatePtr = this->loadSymbol<char *(void *, char const *)>((name + "_evaluate").c_str());
            _hashPtr = this->loadSymbol<char const *()>((name + "_getHash").c_str());
        }

        /**
         * Gives access to the path of the dynamic library archive, relative to the executable path.
         * @return The relative path of the dylib
         */
        virtual std::string path() const override {
            auto base = this->DynamicLib::path();
            if(base.empty()) {
                base = ".";
            }
            if(base.back() != '/') {
                base.append(1, '/');
            }
            return base + this->name() + ".so";
        }

        /**
         * Gets the name of the petri net.
         * @return The name of the petri net.
         */
        virtual char const *name() const = 0;

    protected:
        void *(*_createPtr)() = nullptr;
        void *(*_createDebugPtr)() = nullptr;
        char *(*_evaluatePtr)(void *, char const *) = nullptr;
        char const *(*_hashPtr)() = nullptr;

        bool _c_dynamicLib;
    };
}

#endif
