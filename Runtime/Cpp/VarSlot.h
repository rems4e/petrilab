//
//  VarSlot.h
//  PetriLab
//
//  Created by Rémi Saurel on 2017-03-05.
//


#ifndef Petri_VarSlot_h
#define Petri_VarSlot_h

#include "Variable.h"
#include <memory>
#include <vector>

namespace MultiHeadStackNS {
    template <typename T>
    class StackNode;
}

namespace Petri {
    struct Entity;

    class VarSlot {
    public:
        struct Var {
            std::uint64_t _entityID;
            std::vector<Variable> _variables;
        };
        using VarsPtr = std::shared_ptr<const MultiHeadStackNS::StackNode<Var>>;

        VarSlot(Entity &entity, VarsPtr vars)
                : _ptr(std::move(vars))
                , _entity(&entity) {}

        VarSlot(VarsPtr vars)
                : _ptr(std::move(vars))
                , _entity(nullptr) {}

        std::vector<Variable>::iterator begin();
        std::vector<Variable>::iterator end();

        std::vector<Variable>::const_iterator cbegin();
        std::vector<Variable>::const_iterator cend();

        std::vector<Variable>::reverse_iterator rbegin();
        std::vector<Variable>::reverse_iterator rend();

        std::vector<Variable>::const_reverse_iterator crbegin();
        std::vector<Variable>::const_reverse_iterator crend();

        Variable &operator[](std::uint_fast32_t index) const;

        size_t size() const;

        void pushVariables(std::size_t count);
        void pushReturnValues(std::size_t count);
        void pop();

        bool isFirstSlot() const;

        bool isReturnValues() const;

        // private:
        Entity &entity() {
            return *_entity;
        }

        VarsPtr _ptr;

    private:
        Entity *_entity;
        void pushSlot(std::size_t count, std::uint64_t id);
    };
}

#endif /* end of include guard: Petri_VarSlot_h */
