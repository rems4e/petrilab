//
//  Action.h
//  PetriLab
//
//  Created by Rémi Saurel on 2014-11-22.
//

#ifndef Petri_Action_h
#define Petri_Action_h

#include "Callable.h"
#include "Transition.h"
#include <list>
#include <mutex>

namespace Petri {

    using namespace std::chrono_literals;

    class PetriNet;
    class VarSlot;

    using ActionCallableBase = CallableBase<actionResult_t>;
    using ParametrizedActionCallableBase = CallableBase<actionResult_t, VarSlot &>;

    template <typename CallableType>
    auto make_action_callable(CallableType &&c) {
        return Callable<CallableType, actionResult_t>(c);
    }

    template <typename CallableType>
    auto make_param_action_callable(CallableType &&c) {
        return Callable<CallableType, actionResult_t, VarSlot &>(c);
    }

    /**
     * A state composing a PetriNet.
     */
    class Action : public Entity {
        friend class PetriNet;

    public:
        friend class Petri::Transition;
        /**
         * Creates an empty action, associated to a null CallablePtr.
         */
        Action();

        /**
         * Creates an empty action, associated to a copy of the specified Callable.
         * @param id The ID of the new action.
         * @param name The name of the new action.
         * @param action The Callable which will be called when the action is run.
         * @param requiredTokens The number of tokens that must be inside the active action for it
         * to execute.
         */
        Action(uint64_t id, std::string const &name, ActionCallableBase const &action, uint32_t requiredTokens);
        Action(uint64_t id, std::string const &name, actionResult_t (*action)(), uint32_t requiredTokens);

        /**
         * Creates an empty action, associated to a copy of the specified Callable.
         * @param id The ID of the new action.
         * @param name The name of the new action.
         * @param action The Callable which will be called when the action is run.
         * @param requiredTokens The number of tokens that must be inside the active action for it
         * to execute.
         */
        Action(uint64_t id, std::string const &name, ParametrizedActionCallableBase const &action, uint32_t requiredTokens);
        Action(uint64_t id, std::string const &name, actionResult_t (*action)(VarSlot &), uint32_t requiredTokens);

        Action(Action &&) noexcept;
        Action(Action const &) = delete;

        ~Action();

        /**
         * Adds a Transition to the Action.
         * @param id the id of the Transition
         * @param name the name of the transition to be added
         * @param next the Action following the transition to be added
         * @param cond the condition of the Transition to be added
         * @return The newly created transition.
         */
        Transition &addTransition(uint64_t id, std::string const &name, Action &next, ParametrizedTransitionCallableBase const &cond);
        Transition &addTransition(uint64_t id, std::string const &name, Action &next, TransitionCallableBase const &cond);
        Transition &addTransition(uint64_t id, std::string const &name, Action &next, bool (*cond)(actionResult_t));
        Transition &addTransition(uint64_t id, std::string const &name, Action &next, bool (*cond)(VarSlot const &, actionResult_t));

        /**
         * Adds a Transition to the Action.
         * @param next the Action following the transition to be added
         * @return The newly created transition.
         */
        Transition &addTransition(Action &next);

        /**
         * Returns the Callable asociated to the action. An Action with a null Callable must not
         * invoke this method!
         * @return The Callable of the Action
         */
        ParametrizedActionCallableBase &action() noexcept;

        /**
         * Changes the Callable associated to the Action
         * @param action The Callable which will be copied and put in the Action
         */
        void setAction(ActionCallableBase const &action);
        void setAction(actionResult_t (*action)());

        /**
         * Changes the Callable associated to the Action
         * @param action The Callable which will be copied and put in the Action
         */
        void setAction(ParametrizedActionCallableBase const &action);
        void setAction(actionResult_t (*action)(VarSlot &));

        /**
         * Returns the required tokens of the Action to be activated, i.e. the count of Actions
         * which must lead to *this and terminate for *this to activate.
         * @return The required tokens of the Action
         */
        std::uint32_t requiredTokens() const noexcept;

        /**
         * Changes the required tokens of the Action to be activated.
         * @param requiredTokens The new required tokens count
         */
        void setRequiredTokens(std::uint32_t requiredTokens) noexcept;

        /**
         * Gets the current tokens count given to the Action by its preceding Actions.
         * @return The current tokens count of the Action
         */
        std::uint32_t currentTokens() const noexcept;

        /**
         * Returns the name of the Action.
         * @return The name of the Action
         */
        std::string const &name() const noexcept;

        /**
         * Sets the name of the Action
         * @param name The name of the Action
         */
        void setName(std::string const &name) noexcept(std::is_nothrow_move_constructible<std::string>::value);

        /**
         * Returns the transitions exiting the Action.
         */
        std::list<Transition> const &transitions() const noexcept;

        /**
         * Returns the timeout of the action.
         * A timeout of zero length means no timeout.
         * @return The current timeout value of the action.
         */
        std::chrono::milliseconds timeout() const;

        /**
         * Sets the timeout of the action. No timeout is active by default.
         * A timeout of zero length means no timeout.
         * @param timeout The timeout of the state.
         */
        void setTimeout(std::chrono::milliseconds timeout);

    private:
        std::uint32_t &currentTokensRef() noexcept;
        std::mutex &tokensMutex() noexcept;

        Transition &addTransition(Transition t);

        struct Internals;
        std::unique_ptr<Internals> _internals;
    };
}

#endif
