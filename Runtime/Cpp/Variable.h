//
//  Variable.h
//  PetriLab
//
//  Created by Rémi Saurel on 2015-04-28.
//

#ifndef Petri_Variable_h
#define Petri_Variable_h

#include <mutex>
#include <string>

namespace Petri {

    class Variable {
    public:
        Variable()
                : _value(0) {}

        auto &value() noexcept {
            _isDefault = false;
            return _value;
        }

        auto value() const noexcept {
            return _value;
        }

        void setDefaultValue(std::int64_t value) noexcept {
            if(_isDefault) {
                _value = value;
            }
        }

        auto const &name() const noexcept {
            return _name;
        }

        void setName(std::string value) noexcept {
            _name = std::move(value);
        }

        auto getLock() noexcept(std::is_nothrow_constructible<std::unique_lock<std::mutex>, std::mutex &, std::defer_lock_t>::value) {
            return std::unique_lock<std::mutex>{_mutex, std::defer_lock};
        }

        auto &getMutex() noexcept {
            return _mutex;
        }

    private:
        std::int64_t _value;
        std::string _name;
        std::mutex _mutex;
        bool _isDefault = true;
    };
}


#endif
