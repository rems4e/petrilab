//
//  PetriNet.h
//  PetriLab
//
//  Created by Rémi Saurel on 2014-06-27.
//

#ifndef Petri_PetriNet_h
#define Petri_PetriNet_h

#include "Common.h"
#include <limits>
#include <memory>
#include <string>

struct PetriVarSlot;

namespace Petri {

    class Variable;
    class Action;
    class VarSlot;

    class PetriNet {
    public:
        /**
         * The flags enum that configures how much is logged during a petri net execution.
         * Its members can be OR'ed to achieve the desired log output.
         */
        enum LogVerbosity {
            VerbosityNothing = 0x0,      /**< Do not log anything. */
            VerbosityStates = 0x1,       /**< Log actions with their result. */
            VerbosityTransitions = 0x2,  /**< Log transitions with their result. */
            VerbosityReturnValues = 0x4, /**< Log return values name and values. */
        };

        /**
         * A special value meant to be used with the setLogOutputPath method, which signals the log to be output to the
         * console.
         * @see setLogOutputPath
         */
        static char const *LogOutput_Console;

        /**
         * Creates the PetriNet, assigning it a name which serves debug purposes (see ThreadPool
         * constructor), and an argument count
         * @param name The name to assign to the PetriNet or a designated one if left empty.
         * @param variablesCount The count of arguments of the petri net.
         */
        PetriNet(std::string const &name = "", std::size_t variablesCount = 0);

        virtual ~PetriNet();

        /**
         * Adds an Action to the PetriNet. The net must not be running yet.
         * @param action The action to add
         * @param active Controls whether the action is active as soon as the net is started or not
         */
        virtual Action &addAction(Action action, bool active = false);

        /**
         * Checks whether the net is running.
         * @return true means that the net has been started, and we can not add any more action to
         * it now.
         */
        bool running() const;

        /**
         * Starts the Petri net. It must not be already running. If no states are initially active,
         * this is a no-op.
         */
        virtual void run();

        /**
         * Stops the Petri net. It blocks the calling thread until all running states are finished,
         * but do not allows new states to be enabled. If the net is not running, this is a no-op.
         */
        virtual void stop();

        /**
         * Blocks the calling thread until the Petri net has completed its whole execution.
         */
        virtual void join();

        /**
         * Gets the name given to the petri net at creation.
         * @return The name of the petri net
         */
        std::string const &name() const;

        /**
         * Returns the max number of threads executing the petri net.
         * @return The max concurrency of the petri net.
         */
        size_t maxConcurrency() const;

        /**
         * Sets the max number of threads executing the petri net.
         * @param maxConcurrency The max number of threads executing the petri net.
         */
        void setMaxConcurrency(std::size_t maxConcurrency);

        /**
         * Returns the current number of threads executing the petri net.
         * @return The current number of threads executing the petri net.
         */
        size_t currentConcurrency() const;

        /**
         * Gets the initial variables slot of the petri net.
         * Its lifetime is bound to the petri net's.
         */
        VarSlot &variables();

        /**
         * Internal usage only, used by the C runtime.
         * Gets the initial variables slot of the petri net.
         * Its lifetime is bound to the petri net's.
         */
        PetriVarSlot *cvariables();

        /**
         * Sets the petri net's log output to the specified filename, or LogOutput_Console to set the output to stdout.
         * @param outputPath The path of the log file to append to, or LogOutput_Console if the logs are to output to
         * stdout.
         * @see LogOutput_Console
         */
        void setLogOutputPath(char const *outputPath);

        /**
         * Changes the verbosity of the log output. Default is LogVerbosity::VerbosityNone.
         * @param verbosity The new verbosity level.
         * @see LogVerbosity
         */
        void setLogVerbosity(int verbosity);

        actionResult_t resultWhenTimeout() const;

        void setResultWhenTimeout(actionResult_t result);

    protected:
        struct Internals;
        PetriNet(std::unique_ptr<Internals> internals);
        std::unique_ptr<Internals> _internals;
    };
}

#endif
