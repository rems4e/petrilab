//
//  PetriDynamicLib.hpp
//  PetriLab
//
//  Created by Rémi Saurel on 2015-07-02.
//

#ifndef PetriDynamicLib_hpp
#define PetriDynamicLib_hpp

#include "PetriDynamicLib.h"

namespace Petri {

    class MemberPetriDynamicLib : public Petri::PetriDynamicLib {
    public:
        MemberPetriDynamicLib(bool c_petriDynamicLib, std::string const &name, uint16_t port, std::string const &path = "")
                : MemberPetriDynamicLib(true, c_petriDynamicLib, name, port, path) {}

        MemberPetriDynamicLib(MemberPetriDynamicLib const &pn) = delete;
        MemberPetriDynamicLib &operator=(MemberPetriDynamicLib const &pn) = delete;

        MemberPetriDynamicLib(MemberPetriDynamicLib &&pn) = delete;
        MemberPetriDynamicLib &operator=(MemberPetriDynamicLib &&pn) = delete;

        virtual ~MemberPetriDynamicLib() = default;

        virtual uint16_t port() const override {
            return _port;
        }

        virtual char const *name() const override {
            return _name.c_str();
        }

        static void *libForEditor(char const *prefix, std::uint16_t port);

    protected:
        MemberPetriDynamicLib(bool load, bool c_petriDynamicLib, std::string const &name, uint16_t port, std::string const &path = "")
                : PetriDynamicLib(c_petriDynamicLib, path)
                , _name(name)
                , _port(port) {
            if(load) {
                this->load();
            }
        }

    private:
        std::string _name;
        uint16_t _port;
    };
}


#endif /* PetriDynamicLib_hpp */
