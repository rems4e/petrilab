//
//  Common.h
//  PetriLab
//
//  Created by Rémi Saurel on 2015-04-15.
//

#ifndef Petri_Common_h
#define Petri_Common_h

#include "../C/Types.h"
#include <cstdint>
#include <list>
#include <string>

namespace Petri {

    class PetriNet;
    class Variable;

    void setThreadName(char const *name);
    void setThreadName(std::string const &name);

    using actionResult_t = Petri_actionResult_t;

    struct Entity {
    public:
        Entity(uint64_t id)
                : _id(id) {}

        auto ID() const {
            return _id;
        }

        void setID(uint64_t id) {
            _id = id;
        }

        /**
         * Adds a variable to the entity's associated ones.
         * @param id The new variable to add.
         */
        void addVariable(std::uint_fast32_t id) {
            _vars.push_back(id);
        }

        /**
         * Returns a list of the associated variables' IDs.
         * @return The list of variabels of the entity.
         */
        std::list<uint_fast32_t> const &getVariables() const {
            return _vars;
        }

    private:
        std::uint64_t _id;
        std::list<std::uint_fast32_t> _vars;
    };
}


#endif
