//
//  Logger.h
//  PetriLab
//
//  Created by Rémi Saurel on 2016-03-10.
//

#ifndef PETRI_Logger_h
#define PETRI_Logger_h

#include <iostream>

namespace Petri {
    struct Logger;

    Logger &endl(Logger &log);

    struct Logger {
        Logger(std::ostream *out)
                : _out(out) {}

        template <typename T>
        friend Logger &operator<<(Logger &out, T const &t) {
            if(out._out) {
                (*out._out) << t;
            }

            return out;
        }

        friend Logger &operator<<(Logger &out, Logger &(*func)(Logger &)) {
            if(out._out) {
                func(out);
            }

            return out;
        }

        friend Logger &endl(Logger &log) {
            if(log._out) {
                (*log._out) << std::endl;
            }

            return log;
        }

        std::ostream *_out;
    };
}

#endif /* Logger_h */
