//
//  DynamicLib.cpp
//  PetriLab
//
//  Created by Rémi Saurel on 2015-05-04.
//

#include "../DynamicLib.h"
#include <dlfcn.h>
#include <fcntl.h>
#include <iostream>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

namespace Petri {

    DynamicLib::DynamicLib(bool nodelete, std::string const &path)
            : _nodelete(nodelete)
            , _path(path) {
        // Keeping the working directory to cd into it when load() is invoked.
        // This allows libs specified with a relative path to be loaded when the working directory
        // has been changed.
        _wd = open(".", O_RDONLY);
        if(_wd < 0) {
            std::cerr << "DynamicLib::DynamicLib(): Could not open the current directory (" << strerror(errno) << ")!" << std::endl;
        }
    }

    DynamicLib::~DynamicLib() {
        this->unload();
        if(_wd >= 0) {
            close(_wd);
        }
    }

    void DynamicLib::load() {
        if(this->loaded()) {
            return;
        }

        auto changeDirectory = [](int descriptor) {
            if(fchdir(descriptor) != 0) {
                std::cerr << "DynamicLib::load(): Could not change the current directory (" << strerror(errno) << ")!" << std::endl;
            }
        };

        // Keeping the previous working directory…
        int oldwd = open(".", O_RDONLY);
        try {
            if(oldwd < 0) {
                std::cerr << "DynamicLib::load(): Could not open the current directory (" << strerror(errno) << ")!" << std::endl;
            }

            // … changing to the saved one…
            changeDirectory(_wd);

            int nodeleteFlag = _nodelete ? RTLD_NODELETE : 0;

            auto const &path = this->path();

            // RTLD_GLOBAL is required for Python language
            _libHandle = dlopen(path.c_str(), RTLD_NOW | RTLD_GLOBAL | nodeleteFlag);

            if(_libHandle == nullptr) {
                std::string err = dlerror();
                std::cerr << "Unable to load the dynamic library at path \"" << path << "\"!\n"
                          << "Reason: " << err << std::endl;

                throw std::runtime_error("Unable to load the dynamic library at path \"" + path + "\"! Reason: " + err);
            }

            // … and restoring the previous working directory.
            changeDirectory(oldwd);
            if(oldwd >= 0) {
                close(oldwd);
            }
        } catch(...) {
            changeDirectory(oldwd);
            if(oldwd >= 0) {
                close(oldwd);
            }

            throw;
        }
    }

    /**
     * Removes the dynamic library associated to this wrapper from memory.
     */
    void DynamicLib::unload() {
        if(this->loaded()) {
            if(dlclose(_libHandle) != 0) {
                std::cerr << "Unable to unload the dynamic library!\n"
                          << "Reason: " << dlerror() << std::endl;
            }
        }

        _libHandle = nullptr;
    }

    void *DynamicLib::_loadSymbol(const std::string &name) {
        return dlsym(_libHandle, name.c_str());
    }
}
