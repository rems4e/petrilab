//
//  PetriNet.cpp
//  PetriLab
//
//  Created by Rémi Saurel on 2014-11-29.
//

#include "../PetriNet.h"
#include "PetriNetImpl.h"
#include "lock.h"
#include "multiheadstack/MultiHeadStack.h"
#include <cassert>
#include <cstring>

namespace Petri {

    char const *PetriNet::LogOutput_Console = nullptr;

    PetriNet::PetriNet(std::string const &name, size_t variablesCount)
            : PetriNet(std::make_unique<Internals>(*this, name, variablesCount)) {}
    PetriNet::PetriNet(std::unique_ptr<Internals> internals)
            : _internals(std::move(internals)) {}

    PetriNet::~PetriNet() {
        this->stop();
    }

    Action &PetriNet::addAction(Action action, bool active) {
        if(this->running()) {
            throw std::runtime_error("Cannot modify a running petri net!");
        }

        _internals->_states.emplace_back(std::move(action), active);

        return _internals->_states.back().first;
    }

    std::string const &PetriNet::name() const {
        return _internals->_name;
    }

    size_t PetriNet::maxConcurrency() const {
        return _internals->_maxConcurrency;
    }

    void PetriNet::setMaxConcurrency(std::size_t maxConcurrency) {
        if(this->running()) {
            throw std::runtime_error("Cannot modify a running petri net!");
        }

        _internals->_maxConcurrency = std::max<size_t>(1, maxConcurrency);
    }

    size_t PetriNet::currentConcurrency() const {
        return _internals->_actionsPool.threadCount();
    }

    actionResult_t PetriNet::resultWhenTimeout() const {
        return _internals->_returnWhenTimeout;
    }

    void PetriNet::setResultWhenTimeout(actionResult_t result) {
        _internals->_returnWhenTimeout = result;
    }


    bool PetriNet::running() const {
        return _internals->_running;
    }

    void PetriNet::run() {
        if(this->running()) {
            throw std::runtime_error("Already running!");
        }

        auto ptr = _internals->_firstSlot._ptr;
        for(auto &p : _internals->_states) {
            if(p.second) {
                _internals->_running = true;
                _internals->enableState(p.first, ptr);
            }
        }
    }

    void PetriNet::stop() {
        // Must notify that we are not running anymore before stopping (and joining) the thread pool.
        _internals->_running = false;
        _internals->_actionsPool.stop();

        _internals->_activationCondition.notify_all();
    }

    void PetriNet::join() {
        _internals->_actionsPool.join();
    }

    VarSlot &PetriNet::variables() {
        return _internals->_firstSlot;
    }

    PetriVarSlot *PetriNet::cvariables() {
        return &_internals->_cvariables;
    }

    void PetriNet::setLogOutputPath(char const *outputPath) {
        std::lock_guard<std::mutex> lock{_internals->_logMutex};
        if(outputPath != LogOutput_Console) {
            auto newLog = std::make_unique<std::ofstream>(outputPath, std::ios_base::app);
            if(!newLog->is_open()) {
                std::cerr << "Could not write to log file '" << outputPath << "': " << strerror(errno) << std::endl;
            } else {
                _internals->_logOutput = std::move(newLog);
            }
        } else {
            _internals->_logOutput = nullptr;
        }
    }

    void PetriNet::setLogVerbosity(int verbosity) {
        std::lock_guard<std::mutex> lock{_internals->_logMutex};
        _internals->_logVerbosity = verbosity;
    }

    void PetriNet::Internals::executeState(Action &stateParam, VarSlot::VarsPtr ptr) {
        Action *state = nullptr;
        Action *nextState = &stateParam;
        do {
            if(state != nullptr) {
                this->stateDisabled(*state);
                this->stateEnabled(*nextState);
            }

            state = nextState;
            nextState = nullptr;
            actionResult_t res;

            {
                VarSlot slot{*state, std::move(ptr)};

                std::vector<std::unique_lock<std::mutex>> locks;
                locks.reserve(state->getVariables().size());
                for(auto &var : state->getVariables()) {
                    locks.emplace_back(slot[var].getLock());
                }

                lock(locks.begin(), locks.end());

                // FIXME: re-enable
                if(true || state->timeout().count() == 0) {
                    res = state->action()(slot);
                } else {
                    // Add a new thread if all of the pool is working.
                    if(_actionsPool.threadCount() <= _activeCount.fetch_add(1)) {
                        _actionsPool.addThreadWithinMaxValue(_maxConcurrency);
                    }
                    auto taskResult = _actionsPool.addTask(
                        make_callable([&state, &res, &slot]() mutable { res = state->action()(slot); }));

                    try {
                        taskResult.waitForCompletion(state->timeout());
                    } catch(TimeoutException const &except) {
                        res = _returnWhenTimeout;
                        // TODO: pop slot?
                    }
                }

                // Runs the Callable
                logAction(*state, res);
                bool isReturn = slot.isReturnValues();

                ptr = std::move(slot._ptr);

                if(isReturn) {
                    _firstSlot._ptr = _firstSlot._ptr->push(ptr).shared_from_this();
                }
            }

            if(!state->transitions().empty()) {
                std::list<Transition *> transitionsToTest;
                for(auto &t : state->transitions()) {
                    transitionsToTest.push_back(const_cast<Transition *>(&t));
                }

                auto lastTest = ClockType::time_point();

                while(_running && transitionsToTest.size()) {
                    auto now = ClockType::now();
                    auto minDelay = ClockType::duration::max() / 2;

                    auto lastTransitionsCount = transitionsToTest.size();

                    for(auto it = transitionsToTest.begin(); it != transitionsToTest.end();) {
                        bool isFulfilled = false;

                        if((now - lastTest) >= (*it)->delayBetweenEvaluation()) {
                            {
                                VarSlot slot{**it, ptr};
                                std::vector<std::unique_lock<std::mutex>> locks;
                                locks.reserve((*it)->getVariables().size());
                                for(auto &var : (*it)->getVariables()) {
                                    locks.emplace_back(slot[var].getLock());
                                }

                                lock(locks.begin(), locks.end());

                                // Testing the transition
                                isFulfilled = (*it)->isFulfilled(res, slot);
                                logTransition(**it, isFulfilled);

                                // Here, we do not take in account a potential push or pop operation on the var slot,
                                // as it would mean different result depending on the testing order among other
                                // transitions.
                            }

                            minDelay = std::min(minDelay, (*it)->delayBetweenEvaluation());
                        } else {
                            minDelay = std::min(minDelay, (*it)->delayBetweenEvaluation() - (now - lastTest));
                        }

                        if(isFulfilled) {
                            Action &a = (*it)->next();
                            std::lock_guard<std::mutex> tokensLock(a.tokensMutex());
                            if(++a.currentTokensRef() >= a.requiredTokens()) {
                                a.currentTokensRef() -= a.requiredTokens();

                                if(nextState == nullptr) {
                                    nextState = &a;
                                } else {
                                    // Purposely not moving
                                    this->enableState(a, ptr);
                                }
                            }

                            it = transitionsToTest.erase(it);
                        } else {
                            ++it;
                        }
                    }

                    if(transitionsToTest.size() != lastTransitionsCount) {
                        // We have passed at least one transition, so we stop transitions testing
                        break;
                    } else if(!transitionsToTest.empty()) {
                        lastTest = now;
                        while(ClockType::now() - lastTest <= minDelay) {
                            std::this_thread::sleep_for(std::min(100000ns, minDelay));
                        }
                    }
                }
            }
        } while(nextState != nullptr);

        this->disableState(*state);
    }

    void PetriNet::Internals::enableState(Action &newAction, VarSlot::VarsPtr ptr) {
        // Add a new thread if all of the pool is working.
        if(_actionsPool.threadCount() <= _activeCount.fetch_add(1)) {
            _actionsPool.addThreadWithinMaxValue(_maxConcurrency);
        }

        this->stateEnabled(newAction);

        _actionsPool.addTask(make_callable(
            [this, &newAction, ptr{std::move(ptr)}]() mutable { this->executeState(newAction, std::move(ptr)); }));
    }

    void PetriNet::Internals::disableState(Action &oldAction) {
        this->stateDisabled(oldAction);
        if(_activeCount.fetch_sub(1) == 1 && _running) {
            _this.stop();
            this->logReturnValues();
        }
    }

    void PetriNet::Internals::logMessage(std::string const &message) {
        std::lock_guard<std::mutex> lock{_logMutex};
        if(_logOutput) {
            (*_logOutput) << message << std::endl;
        } else {
            std::cout << message << std::endl;
        }
    }

    void PetriNet::Internals::logAction(Action const &a, actionResult_t result) {
        if(_logVerbosity & LogVerbosity::VerbosityStates) {
            logMessage("Executed state {" + a.name() + ", " + std::to_string(a.ID()) + "}, result is " + std::to_string(result));
        }
    }

    void PetriNet::Internals::logTransition(Transition const &t, bool result) {
        if(_logVerbosity & LogVerbosity::VerbosityTransitions) {
            logMessage("Executed transition {" + t.name() + ", " + std::to_string(t.ID()) + "}, result is " +
                       (result ? "true" : "false"));
        }
    }

    void PetriNet::Internals::logReturnValues() {
        if(_logVerbosity & LogVerbosity::VerbosityReturnValues) {
            if(_this.variables().isReturnValues() && _this.variables().size() > 0) {
                std::string message = "Return values:";
                for(auto const &v : _this.variables()) {
                    message += "\n\t" + v.name() + ": " + std::to_string(v.value());
                }
                logMessage(message);
            }
        }
    }
}
