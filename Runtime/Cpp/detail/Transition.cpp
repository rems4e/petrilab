//
//  Transition.cpp
//  PetriLab
//
//  Created by Rémi Saurel on 2015-05-09.
//

#include "../Transition.h"
#include "../Action.h"

namespace Petri {

    struct Transition::Internals {
        Internals(Action &previous, Action &next)
                : _previous(&previous)
                , _next(&next) {}

        Internals(std::string const &name, Action &previous, Action &next, ParametrizedTransitionCallableBase const &cond)
                : _name(name)
                , _previous(&previous)
                , _next(&next)
                , _test(cond.copy_ptr()) {}

        std::string _name;
        Action *_previous;
        Action *_next;
        std::unique_ptr<ParametrizedTransitionCallableBase> _test;

        // Default delay between evaluation
        std::chrono::nanoseconds _delayBetweenEvaluation = 10ms;
    };

    Transition::Transition(Action &previous, Action &next)
            : Entity(0)
            , _internals(std::make_unique<Internals>(previous, next)) {}

    Transition::Transition(uint64_t id, std::string const &name, Action &previous, Action &next, ParametrizedTransitionCallableBase const &cond)
            : Entity(id)
            , _internals(std::make_unique<Internals>(name, previous, next, cond)) {}

    Transition::~Transition() = default;
    Transition::Transition(Transition &&) noexcept = default;

    void Transition::setPrevious(Action &previous) noexcept {
        _internals->_previous = &previous;
    }
    void Transition::setNext(Action &next) noexcept {
        _internals->_next = &next;
    }

    bool Transition::isFulfilled(actionResult_t actionResult, VarSlot const &vars) {
        return (*_internals->_test)(vars, actionResult);
    }

    ParametrizedTransitionCallableBase const &Transition::condition() const noexcept {
        return *_internals->_test;
    }

    void Transition::setCondition(TransitionCallableBase const &test) {
        auto copy = test.copy_ptr();
        auto shared_copy = std::shared_ptr<TransitionCallableBase>(copy.release());
        this->setCondition(make_param_transition_callable(
            [shared_copy](VarSlot const &, actionResult_t a) { return shared_copy->operator()(a); }));
    }

    void Transition::setCondition(ParametrizedTransitionCallableBase const &test) {
        _internals->_test = test.copy_ptr();
    }

    Action &Transition::previous() noexcept {
        return *_internals->_previous;
    }

    Action &Transition::next() noexcept {
        return *_internals->_next;
    }

    std::string const &Transition::name() const noexcept {
        return _internals->_name;
    }

    void Transition::setName(std::string const &name) {
        _internals->_name = name;
    }

    std::chrono::nanoseconds Transition::delayBetweenEvaluation() const {
        return _internals->_delayBetweenEvaluation;
    }

    void Transition::setDelayBetweenEvaluation(std::chrono::nanoseconds delay) {
        _internals->_delayBetweenEvaluation = delay;
    }
}
