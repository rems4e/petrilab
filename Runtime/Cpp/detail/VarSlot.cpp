//
//  VarSlot.h
//  PetriLab
//
//  Created by Rémi Saurel on 2017-03-05.
//

#include "../VarSlot.h"
#include "../Common.h"
#include "multiheadstack/MultiHeadStack.h"
#include <limits>

namespace Petri {

    Variable &VarSlot::operator[](std::uint_fast32_t index) const {
        return const_cast<std::vector<Variable> &>(_ptr->value()._variables)[index];
    }

    size_t VarSlot::size() const {
        return _ptr->value()._variables.size();
    }

    void VarSlot::pushVariables(std::size_t count) {
        this->pushSlot(count, _entity ? _entity->ID() : 0);
    }

    void VarSlot::pushReturnValues(std::size_t count) {
        this->pushSlot(count, std::numeric_limits<std::uint64_t>::max());
    }

    void VarSlot::pushSlot(std::size_t count, std::uint64_t id) {
        _ptr = _ptr->push(Var{id, std::vector<Variable>(count)}).shared_from_this();
    }

    void VarSlot::pop() {
        auto old = _ptr.get();
        _ptr.reset();
        _ptr = old->pop()->shared_from_this();
    }

    bool VarSlot::isFirstSlot() const {
        return _entity == nullptr || _entity->ID() == 0;
    }

    bool VarSlot::isReturnValues() const {
        return _ptr->value()._entityID == std::numeric_limits<std::uint64_t>::max();
    }

    std::vector<Variable>::iterator VarSlot::begin() {
        return std::const_pointer_cast<std::remove_const_t<VarsPtr::element_type>>(_ptr)->value()._variables.begin();
    }
    std::vector<Variable>::iterator VarSlot::end() {
        return std::const_pointer_cast<std::remove_const_t<VarsPtr::element_type>>(_ptr)->value()._variables.end();
    }

    std::vector<Variable>::const_iterator VarSlot::cbegin() {
        return std::const_pointer_cast<std::remove_const_t<VarsPtr::element_type>>(_ptr)->value()._variables.cbegin();
    }
    std::vector<Variable>::const_iterator VarSlot::cend() {
        return std::const_pointer_cast<std::remove_const_t<VarsPtr::element_type>>(_ptr)->value()._variables.cend();
    }

    std::vector<Variable>::reverse_iterator VarSlot::rbegin() {
        return std::const_pointer_cast<std::remove_const_t<VarsPtr::element_type>>(_ptr)->value()._variables.rbegin();
    }
    std::vector<Variable>::reverse_iterator VarSlot::rend() {
        return std::const_pointer_cast<std::remove_const_t<VarsPtr::element_type>>(_ptr)->value()._variables.rend();
    }

    std::vector<Variable>::const_reverse_iterator VarSlot::crbegin() {
        return std::const_pointer_cast<std::remove_const_t<VarsPtr::element_type>>(_ptr)->value()._variables.crbegin();
    }
    std::vector<Variable>::const_reverse_iterator VarSlot::crend() {
        return std::const_pointer_cast<std::remove_const_t<VarsPtr::element_type>>(_ptr)->value()._variables.crend();
    }
}
