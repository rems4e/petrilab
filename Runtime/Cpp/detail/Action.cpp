//
//  Action.cpp
//  PetriLab
//
//  Created by Rémi Saurel on 2015-05-04.
//

#include "../Action.h"
#include "../PetriNet.h"
#include <list>
#include <mutex>

namespace Petri {

    struct Action::Internals {
        Internals() = default;
        Internals(std::string const &name, uint32_t requiredTokens)
                : _name(name)
                , _requiredTokens(requiredTokens) {}
        std::list<Transition> _transitions;
        std::list<std::reference_wrapper<Transition>> _transitionsLeadingToMe;
        std::unique_ptr<ParametrizedActionCallableBase> _action;
        std::string _name;
        std::uint32_t _requiredTokens = 1;

        std::uint32_t _currentTokens = 0;
        std::mutex _tokensMutex;

        std::chrono::milliseconds _timeout = {};
    };

    Action::Action()
            : Entity(0)
            , _internals(std::make_unique<Internals>()) {}

    /**
     * Creates an empty action, associated to a copy of the specified Callable.
     * @param action The Callable which will be copied
     */
    Action::Action(uint64_t id, std::string const &name, ActionCallableBase const &action, uint32_t requiredTokens)
            : Entity(id)
            , _internals(std::make_unique<Internals>(name, requiredTokens)) {
        this->setAction(action);
    }
    Action::Action(uint64_t id, std::string const &name, actionResult_t (*action)(), uint32_t requiredTokens)
            : Action(id, name, make_action_callable(action), requiredTokens) {}

    /**
     * Creates an empty action, associated to a copy of the specified Callable.
     * @param action The Callable which will be copied
     */
    Action::Action(uint64_t id, std::string const &name, ParametrizedActionCallableBase const &action, uint32_t requiredTokens)
            : Entity(id)
            , _internals(std::make_unique<Internals>(name, requiredTokens)) {
        this->setAction(action);
    }
    Action::Action(uint64_t id, std::string const &name, actionResult_t (*action)(VarSlot &), uint32_t requiredTokens)
            : Action(id, name, make_param_action_callable(action), requiredTokens) {}

    Action::Action(Action &&a) noexcept
            : Entity(a.ID())
            , _internals(std::move(a._internals)) {
        for(auto &t : _internals->_transitions) {
            t.setPrevious(*this);
        }
        for(Transition &t : _internals->_transitionsLeadingToMe) {
            t.setNext(*this);
        }
    }

    Action::~Action() = default;

    Transition &Action::addTransition(Transition t) {
        _internals->_transitions.push_back(std::move(t));

        Transition &returnValue = _internals->_transitions.back();
        returnValue.next()._internals->_transitionsLeadingToMe.push_back(returnValue);

        return returnValue;
    }

    Transition &Action::addTransition(Action &next) {
        return this->addTransition(Transition(*this, next));
    }

    Transition &Action::addTransition(uint64_t id, std::string const &name, Action &next, ParametrizedTransitionCallableBase const &cond) {
        return this->addTransition(Transition(id, name, *this, next, cond));
    }
    Transition &Action::addTransition(uint64_t id, std::string const &name, Action &next, TransitionCallableBase const &cond) {
        auto copy = cond.copy_ptr();
        auto shared_copy = std::shared_ptr<TransitionCallableBase>(copy.release());
        return this->addTransition(id, name, next, make_param_transition_callable([shared_copy](VarSlot const &, actionResult_t a) {
                                       return shared_copy->operator()(a);
                                   }));
    }
    Transition &Action::addTransition(uint64_t id, std::string const &name, Action &next, bool (*cond)(actionResult_t)) {
        return addTransition(id, name, next, make_transition_callable(cond));
    }
    Transition &Action::addTransition(uint64_t id, std::string const &name, Action &next, bool (*cond)(VarSlot const &, actionResult_t)) {
        return addTransition(id, name, next, make_param_transition_callable(cond));
    }

    /**
     * Returns the Callable asociated to the action. An Action with a null Callable must not invoke
     * this method!
     * @return The Callable of the Action
     */
    ParametrizedActionCallableBase &Action::action() noexcept {
        return *_internals->_action;
    }

    /**
     * Changes the Callable associated to the Action
     * @param action The Callable which will be copied and put in the Action
     */
    void Action::setAction(ActionCallableBase const &action) {
        auto copy = action.copy_ptr();
        auto shared_copy = std::shared_ptr<ActionCallableBase>(copy.release());
        this->setAction(make_param_action_callable([shared_copy](VarSlot &) { return shared_copy->operator()(); }));
    }
    void Action::setAction(actionResult_t (*action)()) {
        this->setAction(make_action_callable(action));
    }

    /**
     * Changes the Callable associated to the Action
     * @param action The Callable which will be copied and put in the Action
     */
    void Action::setAction(ParametrizedActionCallableBase const &action) {
        _internals->_action = action.copy_ptr();
    }
    void Action::setAction(actionResult_t (*action)(VarSlot &)) {
        this->setAction(make_param_action_callable(action));
    }

    /**
     * Returns the required tokens of the Action to be activated, i.e. the count of Actions which
     * must lead to *this and terminate for *this to activate.
     * @return The required tokens of the Action
     */
    std::uint32_t Action::requiredTokens() const noexcept {
        return _internals->_requiredTokens;
    }

    /**
     * Changes the required tokens of the Action to be activated.
     * @param requiredTokens The new required tokens count
     */
    void Action::setRequiredTokens(uint32_t requiredTokens) noexcept {
        _internals->_requiredTokens = requiredTokens;
    }

    /**
     * Gets the current tokens count given to the Action by its preceding Actions.
     * @return The current tokens count of the Action
     */
    std::uint32_t Action::currentTokens() const noexcept {
        return _internals->_currentTokens;
    }

    std::uint32_t &Action::currentTokensRef() noexcept {
        return _internals->_currentTokens;
    }

    std::mutex &Action::tokensMutex() noexcept {
        return _internals->_tokensMutex;
    }

    /**
     * Returns the name of the Action.
     * @return The name of the Action
     */
    std::string const &Action::name() const noexcept {
        return _internals->_name;
    }

    /**
     * Sets the name of the Action
     * @param name The name of the Action
     */
    void Action::setName(std::string const &name) noexcept(std::is_nothrow_move_constructible<std::string>::value) {
        _internals->_name = name;
    }

    /**
     * Returns the transitions exiting the Action.
     */
    std::list<Transition> const &Action::transitions() const noexcept {
        return _internals->_transitions;
    }

    /**
     * Returns the timeout of the action.
     * A timeout of zero length means no timeout.
     * @return The current timeout value of the action.
     */
    std::chrono::milliseconds Action::timeout() const {
        return _internals->_timeout;
    }

    /**
     * Sets the timeout of the action. No timeout is active by default.
     * A timeout of zero length means no timeout.
     * @param timeout The timeout of the state.
     */
    void Action::setTimeout(std::chrono::milliseconds timeout) {
        _internals->_timeout = timeout;
    }
}
