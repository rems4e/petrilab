//
//  PetriNetImpl.h
//  PetriLab
//
//  Created by Rémi Saurel on 2015-05-04.
//

#ifndef IA_Pe_tri_PetriNetImpl_h
#define IA_Pe_tri_PetriNetImpl_h

#include "../../C/detail/Types.hpp"
#include "../Action.h"
#include "../Common.h"
#include "../PetriNet.h"
#include "../Transition.h"
#include "../VarSlot.h"
#include "../Variable.h"
#include "ThreadPool.h"
#include "multiheadstack/MultiHeadStack.h"
#include <atomic>
#include <fstream>
#include <list>
#include <mutex>
#include <set>

namespace Petri {
    enum { InitialThreadsActions = 1 };

    // We want a steady clock (no adjustments, only ticking forward in time), but it would
    // be better if we got an high resolution clock.
    using ClockType =
        std::conditional_t<std::chrono::high_resolution_clock::is_steady, std::chrono::high_resolution_clock, std::chrono::steady_clock>;

    struct PetriNet::Internals {
        Internals(PetriNet &pn, std::string const &name, size_t variablesCount)
                : _actionsPool(InitialThreadsActions, name.empty() ? "Anonymous petri net" : name)
                , _maxConcurrency(std::numeric_limits<size_t>::max())
                , _name(name.empty() ? "Anonymous petri net" : name)
                , _variablesStack(std::make_shared<MultiHeadStackNS::MultiHeadStack<VarSlot::Var>>())
                , _firstSlot(_variablesStack)
                , _this(pn) {
            _firstSlot.pushVariables(variablesCount);
            _cvariables = PetriVarSlot{&_firstSlot};
        }
        virtual ~Internals() = default;

        // This method is executed concurrently on the thread pool.
        virtual void executeState(Action &a, VarSlot::VarsPtr ptr);

        virtual void stateEnabled(Action &) {}
        virtual void stateDisabled(Action &) {}

        void enableState(Action &a, VarSlot::VarsPtr ptr);
        void disableState(Action &a);

        void logMessage(std::string const &message);
        void logAction(Action const &a, actionResult_t result);
        void logTransition(Transition const &t, bool result);
        void logReturnValues();

        std::condition_variable _activationCondition;
        std::atomic<std::size_t> _activeCount = {0};

        std::atomic_bool _running = {false};
        ThreadPool<void> _actionsPool;
        size_t _maxConcurrency;

        actionResult_t _returnWhenTimeout;

        std::unique_ptr<std::ofstream> _logOutput;
        int _logVerbosity = PetriNet::LogVerbosity::VerbosityNothing;
        std::mutex _logMutex;

        std::string const _name;
        std::list<std::pair<Action, bool>> _states;
        std::list<Transition> _transitions;

        std::shared_ptr<MultiHeadStackNS::MultiHeadStack<VarSlot::Var>> _variablesStack;
        VarSlot _firstSlot;
        PetriVarSlot _cvariables;
        PetriNet &_this;
    };
}


#endif
