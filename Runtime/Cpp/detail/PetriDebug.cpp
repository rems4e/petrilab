//
//  PetriDebug.cpp
//  PetriLab
//
//  Created by Rémi Saurel on 2014-11-27.
//

#include "../PetriDebug.h"
#include "../Action.h"
#include "../DebugServer.h"
#include "PetriNetImpl.h"
#include <limits>
#include <unordered_map>

namespace Petri {

    struct PetriDebug::Internals : PetriNet::Internals {
        Internals(PetriDebug &pn, std::string const &name, size_t maxConcurrency)
                : PetriNet::Internals(pn, name, maxConcurrency) {}

        void stateEnabled(Action &a) override;
        void stateDisabled(Action &a) override;

        DebugServer *_observer = nullptr;
        std::unordered_map<uint64_t, Action *> _statesMap;
    };

    void PetriDebug::Internals::stateEnabled(Action &a) {
        if(_observer) {
            _observer->addActiveState(a);
        }
    }

    void PetriDebug::Internals::stateDisabled(Action &a) {
        if(_observer) {
            _observer->removeActiveState(a);
        }
    }

    PetriDebug::PetriDebug(std::string const &name, size_t maxConcurrency)
            : PetriNet(std::make_unique<PetriDebug::Internals>(*this, name, maxConcurrency)) {}

    PetriDebug::~PetriDebug() = default;


    void PetriDebug::setObserver(DebugServer *session) {
        static_cast<Internals &>(*_internals)._observer = session;
    }
    Action &PetriDebug::addAction(Action action, bool active) {
        auto &a = this->PetriNet::addAction(std::move(action), active);
        static_cast<Internals &>(*_internals)._statesMap[a.ID()] = &a;

        return a;
    }

    void PetriDebug::stop() {
        this->PetriNet::stop();

        // Be careful to notify *after* the petri net has been stopped, or a race condition may
        // crash the debug server.
        if(static_cast<Internals &>(*_internals)._observer) {
            static_cast<Internals &>(*_internals)._observer->notifyStop();
        }
    }

    Action *PetriDebug::stateWithID(uint64_t id) const {
        auto it = static_cast<Internals &>(*_internals)._statesMap.find(id);
        if(it != static_cast<Internals &>(*_internals)._statesMap.end()) {
            return it->second;
        } else {
            return nullptr;
        }
    }

    std::list<std::pair<Action, bool>> const &PetriDebug::states() const {
        return _internals->_states;
    }

    ThreadPool<void> &PetriDebug::actionsPool() {
        return _internals->_actionsPool;
    }

    std::map<std::uint64_t, std::list<std::shared_ptr<std::vector<Variable>>>> PetriDebug::getVariables() const {
        std::map<std::uint64_t, std::list<std::shared_ptr<std::vector<Variable>>>> result;
        for(auto ptr : _internals->_variablesStack->getNodes()) {
            if(ptr->value()._entityID != std::numeric_limits<std::uint64_t>::max()) {
                auto &list = result[ptr->value()._entityID];
                // we alias the node's vector of variables to the node's shared_ptr
                list.push_back(std::shared_ptr<std::vector<Variable>>(ptr, &ptr->value()._variables));
            }
        }

        return result;
    }

    Variable *PetriDebug::getVariable(std::uint64_t id, std::string const &name) const {
        for(auto ptr : _internals->_variablesStack->getNodes()) {
            if(ptr->value()._entityID == id) {
                for(auto &a : ptr->value()._variables) {
                    if(a.name() == name) {
                        return &a;
                    }
                }
                break;
            }
        }

        return nullptr;
    }

    VarSlot::VarsPtr PetriDebug::getSlot(std::uint64_t id) const {
        // TODO: how to handle with multiple concurrent executions of the same petri net?
        for(auto ptr : _internals->_variablesStack->getNodes()) {
            if(ptr->value()._entityID == id) {
                return ptr;
            }
        }

        throw std::runtime_error("Could not find the right variables slot (" + std::to_string(id) + ")!");
    }
}
