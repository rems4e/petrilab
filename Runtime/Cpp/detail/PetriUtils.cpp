//
//  PetriUtils.cpp
//  PetriLab
//
//  Created by Rémi Saurel on 2015-05-04.
//

#include "../PetriUtils.h"
#include "../Action.h"
#include "../Common.h"
#include "../DynamicLib.h"
#include "../PetriNet.h"
#include "../Variable.h"
#include <iostream>
#include <mutex>
#include <random>
#include <string>
#include <thread>

using namespace std::literals;

namespace Petri {
    void setThreadName(char const *name) {
#if __linux__
        pthread_setname_np(pthread_self(), name);
#elif __APPLE__
        pthread_setname_np(name);
#endif
    }

    void setThreadName(std::string const &name) {
        setThreadName(name.c_str());
    }

    namespace Utility {
        namespace {
            std::random_device _rd;
            std::default_random_engine _engine{_rd()};
            std::mutex _printMutex;
        }

        actionResult_t pause(std::chrono::milliseconds const &delay) {
            std::this_thread::sleep_for(delay);
            return {};
        }

        actionResult_t printAction(std::string const &name, std::uint64_t id) {
            std::lock_guard<std::mutex> lk(_printMutex);
            std::cout << "Action " << name << ", ID " << id << " completed." << std::endl;
            return {};
        }

        actionResult_t doNothing() {
            return {};
        }

        actionResult_t printAllVars(VarSlot &slot) {
            std::lock_guard<std::mutex> lock(_printMutex);
            auto const &vars = slot.entity().getVariables();
            auto const &beg = vars.begin(), &end = vars.end();
            for(auto it = beg; it != end; ++it) {
                if(it != beg) {
                    std::cout << ' ';
                }

                auto &var = slot[*it];
                std::cout << var.name() << ": " << var.value();
            }
            std::cout << std::endl;
            return {};
        }

        actionResult_t printText(std::string const &value) {
            std::lock_guard<std::mutex> lock(_printMutex);
            std::cout << value << std::endl;
            return {};
        }

        int64_t random(int64_t lowerBound, int64_t upperBound) {
            // These sanitizations are intended to avoid UB.
            static_assert(std::is_same<int64_t, int>::value || std::is_same<int64_t, long>::value ||
                              std::is_same<int64_t, long long>::value,
                          "int64_t has an inadequate underlying type for being used as a parameter "
                          "to the RNG distribution.");
            if(upperBound < lowerBound) {
                upperBound = lowerBound;
            }

            return std::uniform_int_distribution<int64_t>{lowerBound, upperBound}(_engine);
        }

        char *loadEvaluateAndInvoke(void *vars, char const *lib, char const *prefix) {
            DynamicLib dl(false, lib);
            dl.load();
            auto eval = dl.loadSymbol<char *(void *)>(prefix + "_evaluate"s);
            return eval(vars);
        }
    }
}
