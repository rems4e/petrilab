//
//  DebugServer.cpp
//  PetriLab
//
//  Created by Rémi Saurel on 2014-11-23.
//

#include "../DebugServer.h"
#include "../../C/detail/Types.hpp"
#include "../Action.h"
#include "../PetriDynamicLib.h"
#include "../Variable.h"
#include "Logger.h"
#include "Socket.h"
#include "ThreadPool.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdocumentation"
#endif
#include "jsoncpp/include/json/json.h"
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#include "multiheadstack/MultiHeadStack.h"
#include <atomic>
#include <condition_variable>
#include <cstring>
#include <mutex>
#include <set>
#include <string>
#include <thread>
using namespace std::string_literals;


namespace Petri {

    class VarSlot;

    struct DebugServer::Internals {
        Internals(DebugServer &that, PetriDynamicLib &lib, bool isClient)
                : _that(that)
                , _client()
                , _petriNetFactory(lib)
                , _isClient(isClient)
                , _logger(isClient ? nullptr : &std::cout) {}
        DebugServer &_that;
        bool _quiet;
        void addActiveState(Action &a);
        void removeActiveState(Action &a);
        void notifyStop();

        void serverCommunication();
        void heartBeat();

        void startPetri(Json::Value const &paylod);
        void evaluate(Json::Value const &payload);
        void listVars();
        void clearPetri();

        void setPause(bool pause);

        void updateBreakpoints(Json::Value const &breakpoints);

        Json::Value receiveObject();
        void sendObject(Json::Value const &o);

        Json::Value json(std::string const &type, Json::Value const &payload);
        Json::Value error(std::string const &error);

        std::map<Action *, std::size_t> _activeStates;
        bool _stateChange = false;
        std::condition_variable _stateChangeCondition;
        std::mutex _stateChangeMutex;

        std::thread _receptionThread;
        std::thread _heartBeat;
        Petri::Socket _client;
        std::atomic_bool _running = {false};

        PetriDynamicLib &_petriNetFactory;
        std::unique_ptr<PetriDebug> _petriNet;
        std::mutex _sendMutex;
        std::mutex _breakpointsMutex;
        std::set<Action *> _breakpoints;

        bool _isClient;
        Logger _logger;
    };

    std::string const &DebugServer::getVersion() {
        static auto const version = "106"s;
        return version;
    }

    DebugServer::DebugServer(PetriDynamicLib &petri, bool isClient)
            : _internals(std::make_unique<DebugServer::Internals>(*this, petri, isClient)) {}

    DebugServer::~DebugServer() {
        this->join();
    }

    void DebugServer::start() {
        _internals->_running = true;
        _internals->_receptionThread = std::thread(&DebugServer::Internals::serverCommunication, &*_internals);
    }

    void DebugServer::stop() {
        _internals->_running = false;
        _internals->_client.shutdown();
        this->join();
    }

    void DebugServer::join() const {
        if(_internals->_receptionThread.joinable()) {
            _internals->_receptionThread.join();
        }
        if(_internals->_heartBeat.joinable()) {
            _internals->_heartBeat.join();
        }
    }

    bool DebugServer::running() const {
        return _internals->_running;
    }

    void DebugServer::addActiveState(Action &a) {
        _internals->addActiveState(a);
    }

    void DebugServer::removeActiveState(Action &a) {
        _internals->removeActiveState(a);
    }

    void DebugServer::notifyStop() {
        _internals->notifyStop();
    }

    void DebugServer::Internals::addActiveState(Action &a) {
        {
            std::lock_guard<std::mutex> lk(_breakpointsMutex);
            if(_breakpoints.count(&a) > 0) {
                this->setPause(true);
                this->sendObject(this->json("ack", "pause"));
            }
        }
        std::unique_lock<std::mutex> lk(_stateChangeMutex);
        ++_activeStates[&a];

        _stateChange = true;
        _stateChangeCondition.notify_all();

        if(_petriNet->actionsPool().is_paused()) {
            _stateChangeCondition.wait(lk, [this]() { return !_petriNet->actionsPool().is_paused(); });
        }
    }

    void DebugServer::Internals::removeActiveState(Action &a) {
        std::lock_guard<std::mutex> lk(_stateChangeMutex);
        auto it = _activeStates.find(&a);
        if(it == _activeStates.end() || it->second == 0) {
            throw std::runtime_error("Trying to remove an inactive state!");
        }
        --it->second;

        _stateChange = true;
        _stateChangeCondition.notify_all();
    }

    void DebugServer::Internals::notifyStop() {
        fflush(stdout);
        fflush(stderr);
        this->sendObject(this->json("ack", "stopped"));
    }

    void DebugServer::Internals::serverCommunication() {
        setThreadName("DebugServer "s + _petriNetFactory.name());

        _logger << "Debug session for Petri net " << _petriNetFactory.name() << " started." << endl;

        bool hasConnected = false;
        while(_running) {
            if(_isClient) {
                _client.connect("localhost", _petriNetFactory.port());
            } else {
                Socket serverSocket;
                if(!serverSocket.listen(_petriNetFactory.port()) || !serverSocket.setBlocking(false)) {
                    _running = false;
                }

                _logger << "Waiting for the debugger to attach…" << endl;
                while(_running && !serverSocket.accept(_client)) {
                    std::this_thread::sleep_for(20ms);
                }
            }
            _client.setBlocking(true);

            if(!_running) {
                break;
            }

            _logger << "Debugger connected!" << endl;

            try {
                while(_running && _client.getState() == Socket::SOCK_CONNECTED) {
                    hasConnected = true;
                    auto const root = this->receiveObject();
                    auto const &type = root["type"];

                    if(type == "hello") {
                        if(root["payload"]["version"] != DebugServer::getVersion()) {
                            auto error = "The server (version " + DebugServer::getVersion() +
                                         ") is incompatible with your client (version " +
                                         root["payload"]["version"].asString() + ")!";
                            this->sendObject(this->error(error));
                            throw std::runtime_error(error);
                        } else {
                            Json::Value ehlo;
                            ehlo["type"] = "ehlo";
                            ehlo["version"] = DebugServer::getVersion();
                            this->sendObject(ehlo);
                            _heartBeat = std::thread(&DebugServer::Internals::heartBeat, this);
                        }
                    } else if(type == "start") {
                        this->startPetri(root["payload"]);
                    } else if(type == "detach") {
                        this->clearPetri();
                        this->sendObject(this->json("detach", "kbye"));

                        break;
                    } else if(type == "detachAndExit") {
                        this->clearPetri();
                        this->sendObject(this->json("detachAndExit", "kbye"));
                        _running = false;

                        break;
                    } else if(type == "stop") {
                        if(_petriNet) {
                            this->setPause(false);
                            _petriNet->stop();
                            _petriNet->join();
                        }
                        this->clearPetri();
                        this->sendObject(this->json("ack", "stop"));
                    } else if(type == "pause") {
                        this->setPause(true);
                        this->sendObject(this->json("ack", "pause"));
                    } else if(type == "resume") {
                        this->sendObject(this->json("ack", "resume"));
                        this->setPause(false);
                    } else if(type == "reload") {
                        this->clearPetri();
                        _petriNetFactory.reload();
                        _petriNet = _petriNetFactory.createDebugPetriNet();
                        _petriNet->setObserver(&_that);
                        _logger << "Reloaded Petri Net." << endl;
                        _logger << "New hash: " << _petriNetFactory.hash() << endl;
                        this->sendObject(this->json("ack", "reload"));
                    } else if(type == "breakpoints") {
                        this->updateBreakpoints(root["payload"]);
                    } else if(type == "evaluate") {
                        this->evaluate(root["payload"]);
                    } else if(type == "vars") {
                        this->listVars();
                    } else if(type == "setVar") {
                        auto var = _petriNet->getVariable(root["payload"]["id"].asUInt64(), root["payload"]["name"].asString());
                        if(var) {
                            var->value() = root["payload"]["value"].asInt64();
                        } else {
                            this->sendObject(this->error("Could not find the variable (" + root["payload"]["id"].asString() +
                                                         ", " + root["payload"]["name"].asString() + ")!"));
                            std::cerr << "Could not find the variable (" << root["payload"]["id"].asString() << ", "
                                      << root["payload"]["name"].asString() << ")!" << std::endl;
                        }
                    } else if(type == "flush") {
                        fflush(stdout);
                        fflush(stderr);
                        this->sendObject(this->json("ack", "flush"));
                    }
                }
            } catch(std::exception const &e) {
                this->sendObject(this->json("detach", e.what()));
                std::cerr << "Caught exception, detaching from client: " << e.what() << std::endl;
            }
            _client.shutdown();
            _stateChangeCondition.notify_all();
            if(_heartBeat.joinable()) {
                _heartBeat.join();
            }

            this->clearPetri();

            _logger << "Disconnected!" << endl;

            if(_isClient && hasConnected) {
                break;
            }
        }

        _running = false;
        _stateChangeCondition.notify_all();

        if(_petriNet) {
            _petriNet->stop();
        }
    }

    void DebugServer::Internals::updateBreakpoints(Json::Value const &breakpoints) {
        if(breakpoints.type() != Json::arrayValue) {
            throw std::runtime_error("Invalid breakpoints format!");
        }

        std::lock_guard<std::mutex> lk(_breakpointsMutex);
        _breakpoints.clear();
        for(Json::ArrayIndex i = Json::ArrayIndex(0); i != breakpoints.size(); ++i) {
            auto id = breakpoints[i].asUInt64();
            _breakpoints.insert(_petriNet->stateWithID(id));
        }
    }

    void DebugServer::Internals::heartBeat() {
        setThreadName("DebugServer "s + _petriNetFactory.name() + " heart beat");
        auto lastSendDate = std::chrono::system_clock::now();
        auto const minDelayBetweenSend = 100ms;

        while(_running && _client.getState() == Petri::Socket::SOCK_CONNECTED) {
            std::unique_lock<std::mutex> lk(_stateChangeMutex);
            _stateChangeCondition.wait(lk, [this]() {
                return _stateChange || !_running || _client.getState() != Petri::Socket::SOCK_CONNECTED;
            });

            if(!_running || _client.getState() != Petri::Socket::SOCK_CONNECTED) {
                break;
            }

            auto delaySinceLastSend = std::chrono::system_clock::now() - lastSendDate;
            if(delaySinceLastSend < minDelayBetweenSend) {
                std::this_thread::sleep_until(lastSendDate + minDelayBetweenSend);
            }

            Json::Value states(Json::arrayValue);

            for(auto &s : _petriNet->states()) {
                Json::Value state;
                state["id"] = Json::Value(Json::UInt64(s.first.ID()));
                state["tokens"] = Json::Value(Json::UInt(s.first.currentTokens()));

                auto it = _activeStates.find(const_cast<Action *>(&s.first));
                std::uint64_t count = 0;
                if(it != _activeStates.end() && it->second > 0) {
                    count = it->second;
                }
                state["count"] = Json::Value(Json::UInt64(count));

                states[states.size()] = state;
            }

            fflush(stdout);
            fflush(stderr);
            this->sendObject(this->json("states", states));
            _stateChange = false;
        }
    }

    void DebugServer::Internals::startPetri(Json::Value const &payload) {
        if(!_petriNetFactory.loaded()) {
            try {
                _petriNetFactory.load();
            } catch(std::exception const &e) {
                this->sendObject(this->error("An exception occurred upon dynamic lib loading ("s + e.what() + ")!"));
                std::cerr << "An exception occurred upon dynamic lib loading (" << e.what() << ")!" << std::endl;
            }
        }
        if(_petriNetFactory.loaded()) {
            if(payload["hash"].asString() != _petriNetFactory.hash()) {
                this->sendObject(this->error("You are trying to run a Petri net that is different "
                                             "from the one which is compiled!"));
                std::cerr << "You are trying to run a Petri net that is different "
                             "from the one which is compiled!"
                          << std::endl;
                _logger << "Expected hash: " << payload["hash"].asString() << "; got " << _petriNetFactory.hash() << endl;
                _petriNetFactory.unload();
            } else {
                if(!_petriNet) {
                    _petriNet = _petriNetFactory.createDebugPetriNet();
                    _petriNet->setObserver(&_that);

                    auto const &verbosity = payload["loglevel"];
                    auto finalLevel = PetriNet::VerbosityNothing;
                    for(auto const &level : verbosity) {
                        if(level.asString() == "nothing") {
                            finalLevel = PetriNet::VerbosityNothing;
                        } else if(level.asString() == "states") {
                            finalLevel = (PetriNet::LogVerbosity)(finalLevel | PetriNet::VerbosityStates);
                        } else if(level.asString() == "transitions") {
                            finalLevel = (PetriNet::LogVerbosity)(finalLevel | PetriNet::VerbosityTransitions);
                        } else if(level.asString() == "returnvalues") {
                            finalLevel = (PetriNet::LogVerbosity)(finalLevel | PetriNet::VerbosityReturnValues);
                        }
                    }
                    if(!verbosity.empty()) {
                        _petriNet->setLogVerbosity(finalLevel);
                    }

                    auto const &args = payload["args"];
                    for(auto it = args.begin(); it != args.end(); ++it) {
                        std::uint32_t key = std::stol(it.key().asString());
                        std::int64_t value = it->asInt64();
                        if(key < _petriNet->variables().size()) {
                            _petriNet->variables()[key].value() = value;
                        } else {
                            this->sendObject(this->error("Invalid argument index (" + std::to_string(key) + ")!"));
                            std::cerr << "Invalid argument index (" << std::to_string(key) << ")!" << std::endl;
                        }
                    }
                } else if(_petriNet->running()) {
                    throw std::runtime_error("The petri net is already running!");
                }

                this->sendObject(this->json("ack", "start"));

                auto const obj = this->receiveObject();
                this->updateBreakpoints(obj["payload"]);

                _petriNet->run();
            }
        }
    }

    void DebugServer::Internals::evaluate(Json::Value const &payload) {
        std::string result, lib;
        try {
            lib = payload["lib"].asString();
            auto action = Entity{0};
            auto slot = VarSlot{action, nullptr};
            if(_petriNet) {
                slot._ptr = _petriNet->getSlot(payload["id"].asUInt64());
            }
            result = _petriNetFactory.evaluate(slot, lib.c_str());
        } catch(std::exception const &e) {
            result = "Could not evaluate the symbol, reason: "s + e.what();
        }
        Json::Value answer;
        answer["eval"] = result;
        answer["lib"] = lib;
        answer["userInfo"] = payload["userInfo"].asString();

        this->sendObject(this->json("evaluation", answer));
    }

    void DebugServer::Internals::listVars() {
        Json::Value result;
        for(auto const &p : _petriNet->getVariables()) {
            Json::Value slots(Json::arrayValue);

            for(auto &ptr : p.second) {
                Json::Value vars(Json::arrayValue);
                for(auto &a : *ptr) {
                    Json::Value result;
                    result["name"] = a.name();
                    result["value"] = std::to_string(a.value());

                    vars.append(std::move(result));
                }
                slots.append(std::move(vars));
            }

            result[std::to_string(p.first)] = std::move(slots);
        }

        this->sendObject(this->json("vars", result));
    }

    void DebugServer::Internals::clearPetri() {
        if(_petriNet != nullptr) {
            _petriNet = nullptr;
        }
        _activeStates.clear();
    }

    void DebugServer::Internals::setPause(bool pause) {
        if(!_petriNet) {
            throw std::runtime_error("Petri net is not running!");
        }

        if(pause) {
            _petriNet->actionsPool().pause();
        } else {
            _petriNet->actionsPool().resume();
            _stateChangeCondition.notify_all();
        }
    }

    Json::Value DebugServer::Internals::receiveObject() {
        std::vector<uint8_t> vect = _client.receiveNewMsg();

        std::string msg(vect.begin(), vect.end());

        Json::Value root;
        Json::Reader reader;
        if(!reader.parse(&msg.data()[0], &msg.data()[msg.length()], root)) {
            std::cerr << "Invalid debug message received from client: \"" << msg << "\"" << std::endl;
            throw std::runtime_error("Invalid debug message received!");
        }

        return root;
    }

    void DebugServer::Internals::sendObject(Json::Value const &o) {
        std::lock_guard<std::mutex> lk(_sendMutex);

        Json::FastWriter writer;
        writer.omitEndingLineFeed();

        std::string s = writer.write(o);

        _client.sendMsg(s.c_str(), s.size());
    }

    Json::Value DebugServer::Internals::json(std::string const &type, Json::Value const &payload) {
        Json::Value err;
        err["type"] = type;
        err["payload"] = payload;

        return err;
    }

    Json::Value DebugServer::Internals::error(std::string const &error) {
        return this->json("error", error);
    }
}
