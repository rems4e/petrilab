//
//  PetriDynamicLib.cpp
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-27.
//

#include "../PetriDynamicLib.h"
#include "../../C/PetriNet.h"
#include "../../C/detail/Types.hpp"
#include "../MemberPetriDynamicLib.h"

namespace Petri {
    void *MemberPetriDynamicLib::libForEditor(char const *prefix, std::uint16_t port) {
        return new ::PetriDynamicLib{std::make_unique<::Petri::MemberPetriDynamicLib>(false, prefix, port)};
    }


    PetriDynamicLib::~PetriDynamicLib() = default;

    std::unique_ptr<PetriNet> PetriDynamicLib::createPetriNet() {
        if(!this->loaded()) {
            throw std::runtime_error("PetriDynamicLib::createPetriNet: Dynamic library not loaded!");
        }

        void *ptr = _createPtr();

        if(_c_dynamicLib) {
            ::PetriNet *cPetriNet = static_cast<::PetriNet *>(ptr);
            ptr = cPetriNet->owned.release();
        }

        return std::unique_ptr<PetriNet>(static_cast<PetriNet *>(ptr));
    }

    std::unique_ptr<PetriDebug> PetriDynamicLib::createDebugPetriNet() {
        if(!this->loaded()) {
            throw std::runtime_error("PetriDynamicLib::createDebugPetriNet: Dynamic library not loaded!");
        }

        void *ptr = _createDebugPtr();

        if(_c_dynamicLib) {
            ::PetriNet *cPetriNet = static_cast<::PetriNet *>(ptr);
            ptr = cPetriNet->owned.release();
        }

        return std::unique_ptr<PetriDebug>(static_cast<PetriDebug *>(ptr));
    }

    std::string PetriDynamicLib::evaluate(VarSlot &varSlot, char const *lib) {
        if(!this->loaded()) {
            throw std::runtime_error("PetriDynamicLib::evaluate: Dynamic library not loaded!");
        }

        void *vars;
        PetriVarSlot cvars{&varSlot};
        if(_c_dynamicLib) {
            vars = &cvars;
        } else {
            vars = &varSlot;
        }
        char *res = _evaluatePtr(vars, lib);
        if(res == nullptr) {
            throw std::runtime_error("Invalid evaluation result");
        }

        auto str = std::string{res};
        std::free(res);
        return str;
    }
}
