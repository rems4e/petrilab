//
//  StateChartUtils.h
//  PetriLab
//
//  Created by Rémi Saurel on 2014-11-12.
//

#ifndef Petri_PetriUtils_h
#define Petri_PetriUtils_h

#include "Common.h"
#include "VarSlot.h"
#include <chrono>

namespace Petri {

    using namespace std::chrono_literals;

    enum class ActionResult { OK, NOK };

    namespace Utility {
        /**
         * Pauses the execution of the calling thread for the specified amount of time.
         * @param delay The delay to wait.
         * @return A defaulted action result.
         */
        actionResult_t pause(std::chrono::milliseconds const &delay);

        /**
         * Prints the specified action name and id to stdout.
         * @param name The name of the action.
         * @param id The id of the action.
         * @return A defaulted action result.
         */
        actionResult_t printAction(std::string const &name, std::uint64_t id);

        /**
         * A function that do not perform any action, and returns a defaulted action result.
         * @return A defaulted action result.
         */
        actionResult_t doNothing();

        /*
         * Prints all variables of an entity.
         * @param vars The slot containing the variables to print.
         * @return A defaulted action result.
         */
        actionResult_t printAllVars(VarSlot &vars);

        /**
         * Prints the given text.
         * @param value The text to print.
         * @return A defaulted action result.
         */
        actionResult_t printText(std::string const &value);

        /**
         * A function that returns a random number between the specified bounds.
         * @param lowerBound The lower bound of the range from which the random number is retrieved.
         * @param upperBound The upper bound of the range from which the random number is retrieved.
         * @return A random number such as lowerBound <= result <= upperBound, iff lowerBound <=
         * upperBound.
         */
        int64_t random(int64_t lowerBound, int64_t upperBound);

        char *loadEvaluateAndInvoke(void *vars, char const *lib, char const *prefix);
    }
}

#endif
