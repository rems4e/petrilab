//
//  PetriDebug.h
//  PetriLab
//
//  Created by Rémi Saurel on 2014-11-22.
//

#ifndef Petri_PetriDebug_h
#define Petri_PetriDebug_h

#include "PetriNet.h"
#include "VarSlot.h"
#include <list>
#include <map>
#include <vector>

namespace Petri {

    class DebugServer;
    template <typename _ReturnType>
    class ThreadPool;

    class PetriDebug : public PetriNet {
    public:
        PetriDebug(std::string const &name, size_t argumentCount = 0);

        virtual ~PetriDebug();

        /**
         * Adds an Action to the PetriNet. The net must not be running yet.
         * @param action The action to add
         * @param active Controls whether the action is active as soon as the net is started or not
         */
        virtual Action &addAction(Action action, bool active = false) override;

        /**
         * Sets the observer of the PetriDebug object. The observer will be notified by some of the
         * Petri net events, such as when a state is activated or disabled.
         * @param session The observer which will be notified of the events
         */
        void setObserver(DebugServer *session);

        /**
         * Retrieves the underlying ThreadPool object.
         * @return The underlying ThreadPool
         */
        ThreadPool<void> &actionsPool();

        /**
         * Finds the state associated to the specified ID, or nullptr if not found.
         * @param id The ID to match with a state.
         * @return The state matching ID
         */
        Action *stateWithID(uint64_t id) const;

        /**
         * Gets the states list.
         * @return The states list.
         */
        std::list<std::pair<Action, bool>> const &states() const;

        void stop() override;

        /**
         * Gets the variables of the petri net. An entity ID is mapped to the list of VarSlots
         * content it has access to.
         */
        std::map<std::uint64_t, std::list<std::shared_ptr<std::vector<Variable>>>> getVariables() const;

        Variable *getVariable(std::uint64_t id, std::string const &name) const;

        VarSlot::VarsPtr getSlot(std::uint64_t id) const;

    protected:
        struct Internals;
    };
}

#endif
