//
//  Transition.h
//  PetriLab
//
//  Created by Rémi Saurel on 2014-11-22.
//

#ifndef Petri_Transition_h
#define Petri_Transition_h

#include "Callable.h"
#include "Common.h"
#include <chrono>

namespace Petri {

    using namespace std::chrono_literals;

    class Action;
    class VarSlot;

    using TransitionCallableBase = CallableBase<bool, actionResult_t>;
    using ParametrizedTransitionCallableBase = CallableBase<bool, VarSlot const &, actionResult_t>;

    template <typename CallableType>
    auto make_transition_callable(CallableType &&c) {
        return Callable<CallableType, std::result_of_t<CallableType(actionResult_t)>, actionResult_t>(c);
    }

    template <typename CallableType>
    auto make_param_transition_callable(CallableType &&c) {
        return Callable<CallableType, std::result_of_t<CallableType(VarSlot const &, actionResult_t)>, VarSlot const &, actionResult_t>(c);
    }

    /**
     * A transition linking 2 Action, composing a PetriNet.
     */
    class Transition : public Entity {
        friend class Petri::Action;

    public:
        Transition(Transition &&) noexcept;
        ~Transition();
        /**
         * Checks whether the Transition can be crossed
         * @param actionResult The result of the Action 'previous'. This is useful when the
         * Transition's test uses this value.
         * @return The result of the test, true meaning that the Transition can be crossed to enable
         * the action 'next'
         */
        bool isFulfilled(actionResult_t actionResult, VarSlot const &vars);

        /**
         * Returns the condition associated to the Transition
         * @return The condition associated to the Transition
         */
        ParametrizedTransitionCallableBase const &condition() const noexcept;

        /**
         * Changes the condition associated to the Transition
         * @param test The new condition to associate to the Transition
         */
        void setCondition(TransitionCallableBase const &test);
        void setCondition(ParametrizedTransitionCallableBase const &test);

        /**
         * Gets the Action 'previous', the starting point of the Transition.
         * @return The Action 'previous', the starting point of the Transition.
         */
        Action &previous() noexcept;

        /**
         * Gets the Action 'next', the arrival point of the Transition.
         * @return The Action 'next', the arrival point of the Transition.
         */
        Action &next() noexcept;

        /**
         * Gets the name of the Transition.
         * @return The name of the Transition.
         */
        std::string const &name() const noexcept;

        /**
         * Changes the name of the Transition.
         * @param name The new name of the Transition.
         */
        void setName(std::string const &name);

        /**
         * The delay between successive evaluations of the Transition. The runtime will not try to
         * evaluate
         * the Transition with a delay smaller than this delay after a previous evaluation, but only
         * for one execution of Action 'previous'
         * @return The minimal delay between two evaluations of the Transition.
         */
        std::chrono::nanoseconds delayBetweenEvaluation() const;

        /**
         * Changes the delay between successive evaluations of the Transition.
         * @param delay The new minimal delay between two evaluations of the Transition.
         */
        void setDelayBetweenEvaluation(std::chrono::nanoseconds delay);

    private:
        Transition(Action &previous, Action &next);
        Transition(uint64_t id, std::string const &name, Action &previous, Action &next, ParametrizedTransitionCallableBase const &cond);

        void setPrevious(Action &previous) noexcept;
        void setNext(Action &next) noexcept;

        struct Internals;
        std::unique_ptr<Internals> _internals;
    };
}

#endif
