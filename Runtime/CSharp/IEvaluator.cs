//
//  IEvaluator.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2014-12-09.
//

﻿namespace Petri.Runtime
{
    /// <summary>
    /// The interface C# Expression evaluators conform to.
    /// </summary>
    public interface IEvaluator
    {
        /// <summary>
        /// Returns the evaluation result
        /// </summary>
        /// <returns>The result.</returns>
        /// <param name="vars">The slot that gives access to variables.</param>
        string Evaluate(VarSlot vars);
    }
}
