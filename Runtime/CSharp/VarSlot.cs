//
//  VarSlot.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-01.
//

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Petri.Runtime
{
    /// <summary>
    /// A slot containing petri net's variables.
    /// </summary>
    public class VarSlot : MarshalByRefObject, IEnumerable<Variable>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Runtime.VarSlot"/> class.
        /// </summary>
        /// <param name="handle">Handle.</param>
        public VarSlot(IntPtr handle)
        {
            Handle = handle;
        }

        /// <summary>
        /// Gets the variable matching the provided id.
        /// </summary>
        /// <returns>The variable.</returns>
        /// <param name="id">The identifier.</param>
        public Variable this[UInt32 id] {
            get {
                if(id > Size) {
                    throw new IndexOutOfRangeException();
                }

                return new Variable(this, id);
            }
        }

        /// <summary>
        /// Gets the variable matching the provided id.
        /// </summary>
        /// <returns>The variable.</returns>
        /// <param name="id">The identifier.</param>
        public Variable this[Enum id] {
            get {
                return this[Convert.ToUInt32(id)];
            }
        }

        /// <summary>
        /// Gets the size of the slot.
        /// </summary>
        /// <value>The size.</value>
        public UInt64 Size {
            get {
                return Interop.VarSlot.PetriVarSlot_getSize(Handle);
            }
        }

        /// <summary>
        /// Pushes a new variable slot with the given capacity.
        /// </summary>
        /// <param name="count">The capacity of the new variables slot.</param>
        public void PushVariables(UInt64 count)
        {
            Interop.VarSlot.PetriVarSlot_pushVariables(Handle, count);
        }

        /// <summary>
        /// Pushes a new variable slot with the given capacity and intended to store return values.
        /// </summary>
        /// <param name="count">The capacity of the new return values slot.</param>
        public void PushReturnValues(UInt64 count)
        {
            Interop.VarSlot.PetriVarSlot_pushReturnValues(Handle, count);
        }

        /// <summary>
        /// Pops the variables slot.
        /// </summary>
        public void Pop()
        {
            Interop.VarSlot.PetriVarSlot_pop(Handle);
        }

        /// <summary>
        /// Gets the enumerator.
        /// </summary>
        /// <returns>The enumerator.</returns>
        public IEnumerator<Variable> GetEnumerator()
        {
            return new VarEnumerator(this);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// Gets a value indicating whether this <see cref="T:Petri.Runtime.VarSlot"/> is the petri net's first variables slot.
        /// </summary>
        /// <value><c>true</c> if first slot; otherwise, <c>false</c>.</value>
        public bool IsFirstSlot {
            get {
                return Interop.VarSlot.PetriVarSlot_isFirstSlot(Handle);
            }
        }

        /// <summary>
        /// Gets a value indicating whether this <see cref="T:Petri.Runtime.VarSlot"/> is a return values slot.
        /// </summary>
        /// <value><c>true</c> if return values slot; otherwise, <c>false</c>.</value>
        public bool IsReturnValues {
            get {
                return Interop.VarSlot.PetriVarSlot_isReturnValues(Handle);
            }
        }

        internal IntPtr Handle {
            get;
            private set;
        }

        [Serializable]
        class VarEnumerator : IEnumerator<Variable>
        {
            public VarEnumerator(VarSlot slot)
            {
                _slot = slot;
                _current = 0;
            }

            public Variable Current {
                get {
                    if(_current == 0 || _current > _slot.Size) {
                        throw new IndexOutOfRangeException();
                    }
                    return _slot[_current - 1];
                }
            }

            object IEnumerator.Current => Current;

            public void Dispose()
            {
            }

            public bool MoveNext()
            {
                return _current++ < _slot.Size;
            }

            public void Reset()
            {
                _current = 0;
            }

            VarSlot _slot;
            uint _current;
        }
    }
}
