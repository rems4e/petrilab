//
//  PetriDebug.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-01.
//

﻿using System;

namespace Petri.Runtime
{
    /// <summary>
    /// A debug version of a petri net.
    /// </summary>
    public class PetriDebug : PetriNet
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Runtime.PetriDebug"/> class.
        /// </summary>
        /// <param name="handle">The native handle.</param>
        /// <param name="owning">If set to <c>true</c> this instance owns the native object.</param>
        internal PetriDebug(IntPtr handle, bool owning = true) : base(handle, owning)
        {
        }

        /// <summary>
        /// Creates the PetriNet, assigning it a name which serves debug purposes.
        /// </summary>
        /// <param name="name">The name to assign to the PetriNet or a designated one if left empty.</param>
        public PetriDebug(string name) : base()
        {
            Handle = Interop.PetriNet.PetriNet_createDebug(name);
        }

        /// <summary>
        /// Creates the PetriNet, assigning it a name which serves debug purposes.
        /// </summary>
        /// <param name="name">The name to assign to the PetriNet or a designated one if left empty.</param>
        /// <param name="variablesCount">The count of variables of the petri net.</param>
        public PetriDebug(string name, UInt64 variablesCount) : base()
        {
            Handle = Interop.PetriNet.PetriNet_createDebugVariablesCount(name, variablesCount);
        }
    }
}
