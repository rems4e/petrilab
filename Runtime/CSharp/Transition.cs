//
//  Transition.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-01.
//

﻿using System;

namespace Petri.Runtime
{
    /// <summary>
    /// A transition linking 2 Action, composing a PetriNet.
    /// </summary>
    public class Transition : Entity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Runtime.Transition"/> class.
        /// </summary>
        /// <param name="handle">The native handle.</param>
        /// <param name="owning">If set to <c>true</c> this instance owns the native object.</param>
        public Transition(IntPtr handle, bool owning = true) : base(handle, owning)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Runtime.Transition"/> class.
        /// </summary>
        /// <param name="handle">The native handle.</param>
        /// <param name="del">The delegate to call to check the transition.</param>
        internal Transition(IntPtr handle, TransitionCallableDel del)
        {
            Handle = handle;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Runtime.Transition"/> class.
        /// </summary>
        /// <param name="handle">The native handle.</param>
        /// <param name="del">The delegate to call to check the transition.</param>
        internal Transition(IntPtr handle, ParametrizedTransitionCallableDel del)
        {
            Handle = handle;
        }

        /// <summary>
        /// Release the native handle.
        /// </summary>
        protected override void Clean()
        {
            Interop.Transition.PetriTransition_destroy(Handle);
        }

        /// <summary>
        /// Checks whether the Transition can be crossed.
        /// </summary>
        /// <returns>The result of the test, <c>true</c> meaning that the Transition can be crossed to enable the action 'next'.</returns>
        /// <param name="actionResult">The result of the Action 'previous'. This is useful when the Transition's test uses this value.</param>
        /// <param name="vars">The slot containing the petri net's variables.</param>
        public bool IsFulfilled(Int32 actionResult, VarSlot vars)
        {
            return Interop.Transition.PetriTransition_isFulfilled(Handle, actionResult, vars.Handle);
        }

        /// <summary>
        /// Changes the condition of the transition.
        /// </summary>
        /// <param name="condition">Condition.</param>
        public void SetCondition(TransitionCallableDel condition)
        {
            var c = WrapForNative.Wrap(condition, Name);
            Interop.Transition.PetriTransition_setCondition(Handle, c);
        }

        /// <summary>
        /// Changes the condition of the transition.
        /// </summary>
        /// <param name="condition">Condition.</param>
        public void SetCondition(ParametrizedTransitionCallableDel condition)
        {
            var c = WrapForNative.Wrap(condition, Name);
            Interop.Transition.PetriTransition_setConditionWithParam(Handle, c);
        }

        /**
         * Gets the Action 'previous', the starting point of the Transition.
         * @return The Action 'previous', the starting point of the Transition.
         */
        public Action Previous()
        {
            return new Action(Interop.Transition.PetriTransition_getPrevious(Handle));
        }

        /**
         * Gets the Action 'next', the arrival point of the Transition.
         * @return The Action 'next', the arrival point of the Transition.
         */
        public Action Next()
        {
            return new Action(Interop.Transition.PetriTransition_getNext(Handle));
        }

        /// <summary>
        /// Gets or sets the transition's name.
        /// </summary>
        /// <value>The name.</value>
        public string Name {
            get {
                return System.Runtime.InteropServices.Marshal.PtrToStringAuto(Interop.Transition.PetriTransition_getName(Handle));
            }
            set {
                Interop.Transition.PetriTransition_setName(Handle, value);
            }
        }

        /// <summary>
        /// Gets or sets the identifier of the transition.
        /// </summary>
        /// <value>The identifier.</value>
        public UInt64 ID {
            get {
                return Interop.Transition.PetriTransition_getID(Handle);
            }
            set {
                Interop.Transition.PetriTransition_setID(Handle, value);
            }
        }

        /// <summary>
        /// Gets or sets the delay between evaluation of the Transition. The runtime will not try to evaluate
        /// the Transition with a delay smaller than this delay after a previous evaluation, but only for one execution of Action 'previous'
        /// </summary>
        /// <value>The minimal delay between evaluation of the transition's condition.</value>
        public double DelayBetweenEvaluation {
            get {
                return Interop.Transition.PetriTransition_getDelayBetweenEvaluation(Handle) / 1.0e6;
            }
            set {
                Interop.Transition.PetriTransition_setDelayBetweenEvaluation(Handle,
                                                                             (UInt64)(value * 1.0e6));
            }
        }

        /// <summary>
        /// Adds a variable's identifier to the transition.
        /// </summary>
        /// <param name="id">Identifier.</param>
        public void AddVariable(Enum id)
        {
            Interop.Transition.PetriTransition_addVariable(Handle, Convert.ToUInt32(id));
        }
        /// <summary>
        /// Adds a variable's identifier to the transition.
        /// </summary>
        /// <param name="id">Identifier.</param>

        public void AddVariable(UInt32 id)
        {
            Interop.Transition.PetriTransition_addVariable(Handle, id);
        }
    }
}
