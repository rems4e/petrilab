//
//  CInterop.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-01.
//

﻿using System;

namespace Petri.Runtime
{
    /// <summary>
    /// The base class of all classes that map to their C equivalent.
    /// </summary>
    public abstract class CInterop : MarshalByRefObject
    {
        internal CInterop()
        {
            Owning = true;
        }

        internal IntPtr Handle {
            get;
            set;
        }

        /// <summary>
        /// Releases unmanaged resources and performs other cleanup operations before the
        /// <see cref="T:Petri.Runtime.CInterop"/> is reclaimed by garbage collection.
        /// </summary>
        ~CInterop()
        {
            if(Owning) {
                Clean();
            }
        }

        /// <summary>
        /// Release the native handle.
        /// </summary>
        protected abstract void Clean();

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="Petri.Runtime.CInterop"/> is owning the native handle.
        /// </summary>
        /// <value><c>true</c> if owning; otherwise, <c>false</c>.</value>
        public bool Owning {
            get;
            protected set;
        }

        /// <summary>
        /// Release this instance. The instance can still be used afterwards, but the destructor will not clean the native handle upon call.
        /// </summary>
        public IntPtr Release()
        {
            Owning = false;
            return Handle;
        }
    }
}
