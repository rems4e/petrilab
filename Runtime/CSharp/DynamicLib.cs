//
//  DynamicLib.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-15.
//

﻿using System;

namespace Petri.Runtime
{
    /// <summary>
    /// Dynamic lib.
    /// </summary>
    public class DynamicLib : CInterop
    {
        internal DynamicLib(IntPtr handle)
        {
            Handle = handle;
            Interop.PetriDynamicLib.PetriDynamicLib_load(Handle);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Runtime.DynamicLib"/> class.
        /// </summary>
        /// <param name="create">The native function's wrapper that creates the the petri net.</param>
        /// <param name="createDebug">The native function's wrapper that creates the debug version of the petri net.</param>
        /// <param name="evaluate">The native function that evaluates an expression.</param>
        /// <param name="hash">The native function's wrapper that returns the hash of the petri net.</param>
        /// <param name="name">The name of the library.</param>
        /// <param name="port">The port to listen to.</param>
        /// <param name="path">The path where to search the library.</param>
        public DynamicLib(PetriNetCallableDel create,
                          PetriDebugCallableDel createDebug,
                          EvalCallableDel evaluate,
                          StringCallableDel hash,
                          string name,
                          UInt16 port,
                          string path = ".")
        {
            _create = create;
            _createDebug = createDebug;
            _evaluatePtr = evaluate;

            _createPtr = () => {
                return _create().Release();
            };
            _createDebugPtr = () => {
                return _createDebug().Release();
            };
            Handle = Interop.PetriDynamicLib.PetriDynamicLib_createWithPtr(_createPtr,
                                                                           _createDebugPtr,
                                                                           _evaluatePtr,
                                                                           hash,
                                                                           name,
                                                                           port,
                                                                           path);
            Interop.PetriDynamicLib.PetriDynamicLib_load(Handle);
        }

        /// <summary>
        /// Release the native handle.
        /// </summary>
        protected override void Clean()
        {
            Interop.PetriDynamicLib.PetriDynamicLib_destroy(Handle);
        }

        /// <summary>
        /// Creates the PetriNet object according to the code contained in the dynamic library.
        /// </summary>
        /// <returns>The create.</returns>
        public PetriNet CreatePetriNet()
        {
            return new PetriNet(Interop.PetriDynamicLib.PetriDynamicLib_createPetriNet(Handle));
        }

        /// <summary>
        /// Creates the PetriDebug object according to the code contained in the dynamic library.
        /// </summary>
        /// <returns>The create.</returns>
        public PetriDebug CreateDebugPetriNet()
        {
            return new PetriDebug(Interop.PetriDynamicLib.PetriDynamicLib_createDebugPetriNet(Handle));
        }

        /// <summary>
        /// Returns the SHA256 hash of the dynamic library. Its purpose is to uniquely identifies the code of the petri net,
        /// so that a different or modified petir net is identified.
        /// </summary>
        /// <value>The hash.</value>
        public string Hash {
            get {
                return System.Runtime.InteropServices.Marshal.PtrToStringAuto(Interop.PetriDynamicLib.PetriDynamicLib_getHash(Handle));
            }
        }

        /// <summary>
        /// Gets the name of the petri net.
        /// </summary>
        /// <value>The name.</value>
        public string Name {
            get {
                return System.Runtime.InteropServices.Marshal.PtrToStringAuto(Interop.PetriDynamicLib.PetriDynamicLib_getName(Handle));
            }
        }

        /// <summary>
        /// Gets the TCP port on which a DebugSession initialized with this wrapper will listen to.
        /// </summary>
        /// <value>The port.</value>
        public UInt16 Port {
            get {
                return Interop.PetriDynamicLib.PetriDynamicLib_getPort(Handle);
            }
        }

        PetriNetCallableDel _create;
        PetriDebugCallableDel _createDebug;
        PtrCallableDel _createPtr;
        PtrCallableDel _createDebugPtr;
        EvalCallableDel _evaluatePtr;
    };
}
