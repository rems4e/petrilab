//
//  Action.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-01.
//

﻿using System;
using System.Collections.Generic;

namespace Petri.Runtime
{
    /// <summary>
    /// A state composing a PetriNet
    /// </summary>
    public class Action : Entity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Runtime.Action"/> class.
        /// </summary>
        /// <param name="handle">The native handle.</param>
        /// <param name="owning">If set to <c>true</c> this instance owns the native object.</param>
        public Action(IntPtr handle, bool owning = true) : base(handle, owning)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Runtime.Action"/> class, associated to a null CallablePtr.
        /// </summary>
        public Action()
        {
            Handle = Interop.Action.PetriAction_createEmpty();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Runtime.Action"/> class.
        /// </summary>
        /// <param name="id">Identifier.</param>
        /// <param name="name">Name.</param>
        /// <param name="action">The callable which will be run when the action is activated.</param>
        /// <param name="requiredTokens">The number of tokens that must be inside the active action for it to execute.</param>
        public Action(UInt64 id, string name, ActionCallableDel action, UInt32 requiredTokens)
        {
            var c = WrapForNative.Wrap(action, name);
            Handle = Interop.Action.PetriAction_create(id, name, c, requiredTokens);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Runtime.Action"/> class.
        /// </summary>
        /// <param name="id">Identifier.</param>
        /// <param name="name">Name.</param>
        /// <param name="action">The callable which will be run when the action is activated.</param>
        /// <param name="requiredTokens">The number of tokens that must be inside the active action for it to execute.</param>
        public Action(UInt64 id,
                      string name,
                      ParametrizedActionCallableDel action,
                      UInt32 requiredTokens)
        {
            var c = WrapForNative.Wrap(action, name);
            Handle = Interop.Action.PetriAction_createWithParam(id, name, c, requiredTokens);
        }

        /// <summary>
        /// Release the native handle.
        /// </summary>
        protected override void Clean()
        {
            Interop.Action.PetriAction_destroy(Handle);
        }

        /// <summary>
        /// Adds a transition going from this instance.
        /// </summary>
        /// <returns>The newly create transition.</returns>
        /// <param name="id">Identifier.</param>
        /// <param name="name">Name.</param>
        /// <param name="next">The Action following the transition to be added.</param>
        /// <param name="cond">The condition of the new transition.</param>
        public Transition AddTransition(UInt64 id,
                                        string name,
                                        Action next,
                                        TransitionCallableDel cond)
        {
            var c = WrapForNative.Wrap(cond, name);

            var handle = Interop.Action.PetriAction_addTransition(Handle,
                                                                  id,
                                                                  name,
                                                                  next.Handle,
                                                                  c);
            var t = new Transition(handle, c);
            t.Release();
            _transitions.Add(t);
            return t;
        }

        /// <summary>
        /// Adds a transition going from this instance.
        /// </summary>
        /// <returns>The newly create transition.</returns>
        /// <param name="id">Identifier.</param>
        /// <param name="name">Name.</param>
        /// <param name="next">The Action following the transition to be added.</param>
        /// <param name="cond">The condition of the new transition.</param>
        public Transition AddTransition(UInt64 id,
                                        string name,
                                        Action next,
                                        ParametrizedTransitionCallableDel cond)
        {
            var c = WrapForNative.Wrap(cond, name);

            var handle = Interop.Action.PetriAction_addTransitionWithParam(Handle,
                                                                           id,
                                                                           name,
                                                                           next.Handle,
                                                                           c);
            var t = new Transition(handle, c);
            t.Release();
            _transitions.Add(t);
            return t;
        }

        /// <summary>
        /// Changes the state's action.
        /// </summary>
        /// <param name="action">The new action.</param>
        public void SetAction(ActionCallableDel action)
        {
            var c = WrapForNative.Wrap(action, Name);
            Interop.Action.PetriAction_setAction(Handle, c);
        }

        /// <summary>
        /// Changes the state's action.
        /// </summary>
        /// <param name="action">The new action.</param>
        public void SetAction(ParametrizedActionCallableDel action)
        {
            var c = WrapForNative.Wrap(action, Name);
            Interop.Action.PetriAction_setActionParam(Handle, c);
        }

        /// <summary>
        /// Gets or sets the required tokens for the Action to be activated, i.e.the count of Actions which must lead to this instance and terminate for the instance to activate.
        /// </summary>
        /// <value>The required tokens.</value>
        public UInt32 RequiredTokens {
            get {
                return Interop.Action.PetriAction_getRequiredTokens(Handle);
            }
            set {
                Interop.Action.PetriAction_setRequiredTokens(Handle, value);
            }
        }

        /// <summary>
        /// Gets the current tokens count given to the Action by its preceding Actions.
        /// </summary>
        /// <value>The current tokens.</value>
        public UInt64 CurrentTokens {
            get {
                return Interop.Action.PetriAction_getCurrentTokens(Handle);
            }
        }

        /// <summary>
        /// Gets or sets the name of the action.
        /// </summary>
        /// <value>The name.</value>
        public string Name {
            get {
                return System.Runtime.InteropServices.Marshal.PtrToStringAuto(Interop.Action.PetriAction_getName(Handle));
            }
            set {
                Interop.Action.PetriAction_setName(Handle, value);
            }
        }

        /// <summary>
        /// Gets or sets the action's identifier.
        /// </summary>
        /// <value>The identifier.</value>
        public UInt64 ID {
            get {
                return Interop.Action.PetriAction_getID(Handle);
            }
            set {
                Interop.Action.PetriAction_setID(Handle, value);
            }
        }

        /// <summary>
        /// Adds a variable's identifier to the action.
        /// </summary>
        /// <param name="id">Identifier.</param>
        public void AddVariable(Enum id)
        {
            Interop.Action.PetriAction_addVariable(Handle, Convert.ToUInt32(id));
        }
        /// <summary>
        /// Adds a variable's identifier to the action.
        /// </summary>
        /// <param name="id">Identifier.</param>

        public void AddVariable(UInt32 id)
        {
            Interop.Action.PetriAction_addVariable(Handle, id);
        }

        List<Transition> _transitions = new List<Transition>();
    }
}
