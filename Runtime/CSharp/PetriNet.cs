//
//  PetriNet.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-01.
//

using System;
using System.Collections.Generic;

namespace Petri.Runtime
{
    /// <summary>
    /// Petri net.
    /// </summary>
    public class PetriNet : CInterop
    {
        /// <summary>
        /// The flags enum that configures how much is logged during a petri net execution.
        /// Its members can be OR'ed to achieve the desired log output.
        /// </summary>
        [Flags]
        public enum LogVerbosity
        {
            /// <summary>
            /// Do not log anything.
            /// </summary>
            Nothing = 0x0,

            /// <summary>
            /// Log states with their result.
            /// </summary>
            States = 0x1,

            /// <summary>
            /// Log transitions with their result.
            /// </summary>
            Transitions = 0x2,

            /// <summary>
            /// Log return values with their name and value.
            /// </summary>
            ReturnValues = 0x4,
        };

        /// <summary>
        /// A special value meant to be used with the setLogOutputPath method, which signals the log to be output to the
        /// console.
        /// <see cref="SetLogOutputPath"/>.
        /// </summary>
        public static readonly string LogOutput_Console = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Runtime.PetriNet"/> class.
        /// </summary>
        protected PetriNet()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Runtime.PetriNet"/> class.
        /// </summary>
        /// <param name="handle">Handle.</param>
        /// <param name="owning">If set to <c>true</c> this instance owns the native object.</param>
        public PetriNet(IntPtr handle, bool owning = true) : this()
        {
            Handle = handle;
            if(!owning) {
                Release();
            }
            if(Handle == IntPtr.Zero) {
                throw new Exception("The petri net could not be loaded!");
            }
        }

        /// <summary>
        /// Creates the PetriNet, assigning it a name which serves debug purposes.
        /// </summary>
        /// <param name="name">The name to assign to the PetriNet or a designated one if left empty.</param>
        public PetriNet(string name) : this()
        {
            Handle = Interop.PetriNet.PetriNet_create(name);
        }

        /// <summary>
        /// Creates the PetriNet, assigning it a name which serves debug purposes, and a number of arguments.
        /// </summary>
        /// <param name="name">The name to assign to the PetriNet or a designated one if left empty.</param>
        /// <param name="variablesCount">The count of arguments of the petri net.</param>
        public PetriNet(string name, int variablesCount) : this()
        {
            Handle = Interop.PetriNet.PetriNet_createVariablesCount(name, (UInt64)variablesCount);
        }

        /// <summary>
        /// Release the native handle.
        /// </summary>
        protected override void Clean()
        {
            Interop.PetriNet.PetriNet_destroy(Handle);
        }

        /// <summary>
        /// Adds the action to the petri net, which must not be running.
        /// </summary>
        /// <param name="action">Action.</param>
        /// <param name="active">Controls whether the action is active as soon as the net is started or not.</param>
        public virtual void AddAction(Action action, bool active = false)
        {
            Interop.PetriNet.PetriNet_addAction(Handle, action.Handle, active);
            action.Release();
            _actions.Add(action);
        }

        /// <summary>
        /// Gets a value indicating whether this <see cref="T:Petri.Runtime.PetriNet"/> is running.
        /// </summary>
        /// <value><c>true</c> means that the net has been started, and we can not add any more action to it now.</value>
        public bool IsRunning {
            get {
                return Interop.PetriNet.PetriNet_isRunning(Handle);
            }
        }

        /// <summary>
        /// Starts the Petri net.It must not be already running.If no states are initially active, this is a no-op.
        /// </summary>
        public virtual void Run()
        {
            Interop.PetriNet.PetriNet_run(Handle);
        }

        /// <summary>
        /// Stops the Petri net.It blocks the calling thread until all running states are finished,
        /// but does not allows new states to be enabled. If the petri net is not running, this is a no-op.
        /// </summary>
        public virtual void Stop()
        {
            Interop.PetriNet.PetriNet_stop(Handle);
        }

        /// <summary>
        /// Blocks the calling thread until the Petri net has completed its whole execution.
        /// </summary>
        public virtual void Join()
        {
            Interop.PetriNet.PetriNet_join(Handle);
        }

        /// <summary>
        /// Gets the name of the petri net.
        /// </summary>
        /// <value>The name.</value>
        public string Name {
            get {
                return System.Runtime.InteropServices.Marshal.PtrToStringAuto(Interop.PetriNet.PetriNet_getName(Handle));
            }
        }

        /// <summary>
        /// Gets or sets the max number of states that may be running in parallel at a given moment.
        /// </summary>
        /// <value>The max concurrency.</value>
        public UInt64 MaxConcurrency {
            get {
                return Interop.PetriNet.PetriNet_getMaxConcurrency(Handle);
            }
            set {
                Interop.PetriNet.PetriNet_setMaxConcurrency(Handle, value);
            }
        }

        /// <summary>
        /// Gets the current number of threads executing the petri net.
        /// </summary>
        /// <value>The current concurrency.</value>
        public UInt64 CurrentConcurrency {
            get {
                return Interop.PetriNet.PetriNet_getCurrentConcurrency(Handle);
            }
        }

        /// <summary>
        /// Gets the initial variables slot of the petri net.
        /// Its lifetime is bound to the petri net's.
        /// </summary>
        public VarSlot Variables {
            get {
                return new VarSlot(Interop.PetriNet.PetriNet_getVariables(Handle));
            }
        }

        /// <summary>
        /// Sets the petri net's log output to the specified filename, or PetriNet_LogOutput_Console to set the output to stdout.
        /// </summary>
        /// <param name="outputPath">The path of the log file to append to, or PetriNet_LogOutput_Console if the logs are to output to stdout.</param>
        /// <see cref="LogOutput_Console"/>
        public void SetLogOutputPath(string outputPath)
        {
            Interop.PetriNet.PetriNet_setLogOutputPath(Handle, outputPath);
        }

        /// <summary>
        /// Changes the verbosity of the log output.Default is <code>LogVerbosity.None</code>.
        /// </summary>
        /// <param name="verbosity">The new verbosity level.</param>
        /// <see cref="LogVerbosity"/>.
        public void SetLogVerbosity(LogVerbosity verbosity)
        {
            Interop.PetriNet.PetriNet_setLogVerbosity(Handle, (int)verbosity);
        }

        List<Action> _actions = new List<Action>();
    }
}
