//
//  DebugServer.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-15.
//

﻿namespace Petri.Runtime
{
    /// <summary>
    /// Debug server.
    /// </summary>
    public class DebugServer : CInterop
    {
        /// <summary>
        /// Gets the DebugServer API's version.
        /// </summary>
        /// <value>The current version of the API.</value>
        public static string Version {
            get {
                return System.Runtime.InteropServices.Marshal.PtrToStringAuto(Interop.DebugServer.PetriDebugServer_getVersion());
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Runtime.DebugServer"/> class.
        /// </summary>
        /// <param name="lib">The dynamic lib from which the debug server operates.</param>
        /// <param name="isClient">If <c>true</c>, the debug server will act as a client and
        /// not output log messages to stdout</param>
        public DebugServer(DynamicLib lib, bool isClient = false)
        {
            if(isClient) {
                Handle = Interop.DebugServer.PetriDebugServer_createClient(lib.Handle);
            } else {
                Handle = Interop.DebugServer.PetriDebugServer_create(lib.Handle);
            }
        }

        /// <summary>
        /// Release the native handle. This will <see cref="Join"/> this instance if needed.
        /// </summary>
        protected override void Clean()
        {
            Interop.DebugServer.PetriDebugServer_destroy(Handle);
        }

        /// <summary>
        /// Starts the debug server by listening on the debug port of the bound dynamic library,
        /// making it ready to receive a debugger connection.
        /// </summary>
        public void Start()
        {
            Interop.DebugServer.PetriDebugServer_start(Handle);
        }

        /// <summary>
        /// Stops the debug server.After that, the debugging port is unbound.
        /// </summary>
        public void Stop()
        {
            Interop.DebugServer.PetriDebugServer_stop(Handle);
        }

        /// <summary>
        /// Checks whether the debug server is running or no.
        /// </summary>
        /// <returns><c>true</c>, if the server is running, <c>false</c> otherwise.</returns>
        public bool IsRunning()
        {
            return Interop.DebugServer.PetriDebugServer_isRunning(Handle);
        }

        /// <summary>
        /// Waits for the debug server to finish its run loop.
        /// </summary>
        public void Join()
        {
            Interop.DebugServer.PetriDebugServer_join(Handle);
        }
    }
}
