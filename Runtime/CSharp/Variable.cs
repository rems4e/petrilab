//
//  Variable.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-01.
//

﻿using System;

namespace Petri.Runtime
{
    /// <summary>
    /// A wrapper to a petri net variable.
    /// </summary>
    public class Variable : MarshalByRefObject
    {
        internal Variable(VarSlot slot, UInt32 id)
        {
            _slot = slot;
            _id = id;
        }

        /// <summary>
        /// Gets or sets the value of the variable
        /// </summary>
        /// <value>The value.</value>
        public Int64 Value {
            get {
                return Interop.VarSlot.PetriVarSlot_getVariableValue(_slot.Handle, _id);
            }
            set {
                Interop.VarSlot.PetriVarSlot_setVariableValue(_slot.Handle, _id, value);
            }
        }

        /// <summary>
        /// Sets the default value of the variable.
        /// </summary>
        /// <param name="value">The default value.</param>
        public Int64 DefaultValue {
            set {
                Interop.VarSlot.PetriVarSlot_setVariableDefaultValue(_slot.Handle, _id, value);
            }
        }

        /// <summary>
        /// Gets or sets the name of the variable.
        /// </summary>
        /// <value>The name.</value>
        public string Name {
            get {
                return System.Runtime.InteropServices.Marshal.PtrToStringAuto(Interop.VarSlot.PetriVarSlot_getVariableName(_slot.Handle, _id));
            }
            set {
                Interop.VarSlot.PetriVarSlot_setVariableName(_slot.Handle, _id, value);
            }
        }


        VarSlot _slot;
        UInt32 _id;
    }
}
