//
//  Utility.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-01.
//

﻿using System;

namespace Petri.Runtime
{
    /// <summary>
    /// The result of a petri net's action.
    /// </summary>
    public enum ActionResult
    {
        /// <summary>
        /// OK.
        /// </summary>
        OK,

        /// <summary>
        /// Not ok.
        /// </summary>
        NOK
    }

    /// <summary>
    /// A collection of utility functions for creating petri nets.
    /// </summary>
    public static class Utility
    {
        /// <summary>
        /// Pauses the execution of the calling thread for the specified amount of time.
        /// </summary>
        /// <returns>A defaulted action result.</returns>
        /// <param name="delaySeconds">The delay to wait, in seconds.</param>
        public static Int32 Pause(double delaySeconds)
        {
            return Interop.PetriUtils.PetriUtility_pause(delaySeconds);
        }

        /// <summary>
        /// Prints the specified action name and id to stdout.
        /// </summary>
        /// <returns>A defaulted action result.</returns>
        /// <param name="name">The name of the action.</param>
        /// <param name="id">The id of the action.</param>
        public static Int32 PrintAction(string name, UInt64 id)
        {
            return Interop.PetriUtils.PetriUtility_printAction(name, id);
        }

        /// <summary>
        /// A function that does not perform any action, and returns a defaulted action result.
        /// </summary>
        /// <returns>A defaulted action result.</returns>
        public static Int32 DoNothing()
        {
            return Interop.PetriUtils.PetriUtility_doNothing();
        }

        /// <summary>
        /// Prints all variables of an action.
        /// </summary>
        /// <returns>The all variables.</returns>
        /// <param name="e">The entity.</param>
        /// <param name="args">Arguments.</param>
        public static Int32 PrintAllVars(VarSlot e, params object[] args)
        {
            return Interop.PetriUtils.PetriUtility_printAllVars(e.Handle);
        }

        /// <summary>
        /// Prints the specified text.
        /// </summary>
        /// <returns>A defaulted action result.</returns>
        /// <param name="value">The text to output.</param>
        public static Int32 PrintText(string value)
        {
            return Interop.PetriUtils.PetriUtility_printText(value);
        }

        /// <summary>
        /// A function that returns a random number between the specified bounds.
        /// </summary>
        /// <returns>A random number such as lowerBound &lt;= result &lt;= upperBound, iff lowerBound &lt;= upperBound.</returns>
        /// <param name="lowerBound">The lower bound of the range from which the random number is retrieved.</param>
        /// <param name="upperBound">The upper bound of the range from which the random number is retrieved</param>
        public static Int64 Random(Int64 lowerBound, Int64 upperBound)
        {
            return Interop.PetriUtils.PetriUtility_random(lowerBound, upperBound);
        }

        /// <summary>
        /// Loads the petri dynamic lib.
        /// </summary>
        /// <returns>The petri dynamic lib.</returns>
        /// <param name="path">Path.</param>
        /// <param name="name">Name.</param>
        /// <param name="nodelete">If set to <c>true</c> nodelete.</param>
        public static IntPtr LoadPetriDynamicLib(string path, string name, bool nodelete)
        {
            return Interop.PetriUtils.Petri_loadPetriDynamicLib(path, name, nodelete);
        }
    }
}
