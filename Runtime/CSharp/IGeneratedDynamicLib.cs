//
//  IGeneratedDynamicLib.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-19.
//

﻿using System;

namespace Petri.Runtime
{
    /// <summary>
    /// Generated dynamic lib.
    /// </summary>
    public interface IGeneratedDynamicLib
    {
        /// <summary>
        /// Accesses the DynamicLib instance embedded into the library
        /// </summary>
        /// <value>The lib.</value>
        DynamicLib Lib {
            get;
        }
    }

    /// <summary>
    /// The base class of a generated petri net dynamic library
    /// </summary>
    public abstract class CSharpGeneratedDynamicLib : MarshalByRefObject, IGeneratedDynamicLib
    {
        /// <summary>
        /// Accesses the DynamicLib instance embedded into the library
        /// </summary>
        /// <value>The lib.</value>
        public DynamicLib Lib {
            get {
                return _lib;
            }
        }

        /// <summary>
        /// The lib.
        /// </summary>
        protected DynamicLib _lib;
    }

    /// <summary>
    /// C Generated dynamic lib.
    /// </summary>
    public class CGeneratedDynamicLib : IGeneratedDynamicLib
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Runtime.CGeneratedDynamicLib"/> class.
        /// </summary>
        /// <param name="handle">Handle.</param>
        public CGeneratedDynamicLib(IntPtr handle)
        {
            _lib = new DynamicLib(handle);
        }

        /// <summary>
        /// Accesses the DynamicLib instance embedded into the library
        /// </summary>
        /// <value>The lib.</value>
        public DynamicLib Lib {
            get {
                return _lib;
            }
        }

        /// <summary>
        /// The lib.
        /// </summary>
        protected DynamicLib _lib;
    }
}
