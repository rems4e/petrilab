//
//  GeneratedDynamicLibProxy.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-03-11.
//

using System;

namespace Petri.Runtime
{
    /// <summary>
    /// Generated dynamic lib proxy.
    /// </summary>
    public abstract class GeneratedDynamicLibProxy : MarshalByRefObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Runtime.GeneratedDynamicLibProxy"/> class.
        /// </summary>
        /// <param name="libPath">Lib path.</param>
        /// <param name="libName">Lib name.</param>
        /// <param name="className">Class name.</param>
        protected GeneratedDynamicLibProxy(string libPath,
                                           string libName,
                                           string className)
        {
            LibPath = libPath;
            LibName = libName;
            ClassName = className;
        }

        /// <summary>
        /// Gets or sets the lib path.
        /// </summary>
        /// <value>The lib path.</value>
        public string LibPath {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the name of the lib.
        /// </summary>
        /// <value>The name of the lib.</value>
        public string LibName {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the name of the class.
        /// </summary>
        /// <value>The name of the class.</value>
        public string ClassName {
            get;
            set;
        }

        /// <summary>
        /// Loads the assembly and extracts the <see cref="Petri.Runtime.IGeneratedDynamicLib"/> instance it encloses.
        /// </summary>
        /// <typeparam name="LibType">The type of the proxy to create. Must be a subclass of <see cref="Petri.Runtime.CGeneratedDynamicLib"/>.</typeparam>
        public abstract LibType Load<LibType>() where LibType : class;

        /// <summary>
        /// Unloads the assembly.
        /// </summary>
        public abstract void Unload();
    }

    /// <summary>
    /// C Sharp generated dynamic lib proxy to load the information on the creation of the petri net.
    /// </summary>
    public class CSharpGeneratedDynamicLibProxy : GeneratedDynamicLibProxy
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Runtime.CSharpGeneratedDynamicLibProxy"/> class.
        /// </summary>
        /// <param name="libPath">Lib path.</param>
        /// <param name="libName">Lib name.</param>
        /// <param name="className">Class name.</param>
        public CSharpGeneratedDynamicLibProxy(string libPath,
                                              string libName,
                                              string className) : base(libPath, libName, className)
        {
        }

        /// <summary>
        /// Load the C# assembly containing the petri net creation information.
        /// </summary>
        /// <returns>The load.</returns>
        /// <typeparam name="LibType">The 1st type parameter.</typeparam>
        public override LibType Load<LibType>()
        {
            var filePath = System.IO.Path.Combine(LibPath, LibName);

            LibType dylib = null;

            if(_domain == null) {
                try {
                    _domain = AppDomain.CreateDomain("PetriDynamicLib" + filePath);
                } catch(System.IO.FileNotFoundException e) {
                    if(Environment.GetEnvironmentVariables().Contains("PETRILAB_AM_I_IN_BUNDLE") && e.Message.Contains("Petri.exe")) {
                        // Here we handle the case where we are in a bundle.
                        // Creating the domain requires for some reason the presence of the original exe file.
                        System.IO.File.WriteAllText("Petri.exe", "");
                        _domain = AppDomain.CreateDomain("PetriDynamicLib" + filePath);
                        System.IO.File.Delete("Petri.exe");
                    } else {
                        throw;
                    }
                }
            }

            try {
                dylib = (LibType)_domain.CreateInstanceFromAndUnwrap(filePath, "Petri.Generated." + ClassName);
            } catch(Exception e) {
                Console.Error.WriteLine("Exception when loading assembly {0}: {1}", filePath, e);
            }

            return dylib;
        }

        /// <summary>
        /// Unloads the assembly.
        /// </summary>
        public override void Unload()
        {
            if(_domain != null) {
                AppDomain.Unload(_domain);
                _domain = null;
            }
        }

        AppDomain _domain;
    }
}
