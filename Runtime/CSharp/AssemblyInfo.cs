//
//  AssemblyInfo.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2014-12-09.
//

﻿using System.Reflection;

[assembly: AssemblyTitle("PetriLab C# Runtime")]
[assembly: AssemblyDescription("PetriLab's C# Runtime")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Sigilence Technologies")]
[assembly: AssemblyProduct("PetriLab")]
[assembly: AssemblyCopyright("Rémi Saurel")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: AssemblyVersion("3.0.0")]
