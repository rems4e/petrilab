//
//  Types.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-01.
//

﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Petri.Runtime
{
    /// <summary>
    /// A callable that takes the information to load an evaluator's dynamic library and returns the enclosed expression result.
    /// </summary>
    [return: MarshalAs(UnmanagedType.LPTStr)]
    public delegate string EvalCallableDel(IntPtr vars, [MarshalAs(UnmanagedType.LPTStr)] string libName);

    /// <summary>
    /// A callable that returns a native string.
    /// </summary>
    [return: MarshalAs(UnmanagedType.LPTStr)]
    public delegate string StringCallableDel();

    /// <summary>
    /// A callable that returns a native pointer.
    /// </summary>
    public delegate IntPtr PtrCallableDel();

    /// <summary>
    /// A callable that creates a petri net.
    /// </summary>
    public delegate PetriNet PetriNetCallableDel();

    /// <summary>
    /// A callable that creates the debug version of a petri net.
    /// </summary>
    public delegate PetriDebug PetriDebugCallableDel();

    /// <summary>
    /// The type of function of a petri net's action.
    /// </summary>
    public delegate Int32 ActionCallableDel();

    /// <summary>
    /// The type of function of a petri net's action, that takes an entity's reference.
    /// </summary>
    public delegate Int32 ParametrizedActionCallableDel(IntPtr vars);


    /// <summary>
    /// The type of function of a petri net's transition.
    /// </summary>
    public delegate bool TransitionCallableDel(Int32 result);

    /// <summary>
    /// The type of function of a petri net's transition, that takes an entity's reference.
    /// </summary>
    public delegate bool ParametrizedTransitionCallableDel(IntPtr vars, Int32 result);

    /// <summary>
    /// A utility class that creates closures to be passed to the native runtime.
    /// </summary>
    public static class WrapForNative
    {
        /// <summary>
        /// Wrap the specified callable and actionName.
        /// </summary>
        /// <returns>The wrap.</returns>
        /// <param name="callable">Callable.</param>
        /// <param name="actionName">Action name.</param>
        public static ActionCallableDel Wrap(ActionCallableDel callable, string actionName)
        {
            var c = new ActionCallableDel(() => {
                try {
                    return callable();
                } catch(Exception e) {
                    Console.Error.WriteLine("The execution of the action {0} failed with the exception \"{1}\"",
                                            actionName,
                                            e.Message);
                    return default(Int32);
                }
            });

            _callbacks.Add(c);
            return c;
        }

        /// <summary>
        /// Wrap the specified callable and actionName.
        /// </summary>
        /// <returns>The wrap.</returns>
        /// <param name="callable">Callable.</param>
        /// <param name="actionName">Action name.</param>
        public static ParametrizedActionCallableDel Wrap(ParametrizedActionCallableDel callable,
                                                         string actionName)
        {
            var c = new ParametrizedActionCallableDel((IntPtr vars) => {
                try {
                    return callable(vars);
                } catch(Exception e) {
                    Console.Error.WriteLine("The execution of the action {0} failed with the exception \"{1}\"",
                                            actionName,
                                            e.Message);
                    return default(Int32);
                }
            });

            _callbacks.Add(c);
            return c;
        }

        /// <summary>
        /// Wrap the specified callable and transitionName.
        /// </summary>
        /// <returns>The wrap.</returns>
        /// <param name="callable">Callable.</param>
        /// <param name="transitionName">Transition name.</param>
        public static TransitionCallableDel Wrap(TransitionCallableDel callable,
                                                 string transitionName)
        {
            var c = new TransitionCallableDel((Int32 result) => {
                try {
                    return callable(result);
                } catch(Exception e) {
                    Console.Error.WriteLine("The condition testing of the condition {0} failed with the exception \"{1}\"",
                                            transitionName,
                                            e.Message);
                    return default(bool);
                }
            });

            _callbacks.Add(c);
            return c;
        }

        /// <summary>
        /// Wrap the specified callable and actionName.
        /// </summary>
        /// <returns>The wrap.</returns>
        /// <param name="callable">Callable.</param>
        /// <param name="actionName">Action name.</param>
        public static ParametrizedTransitionCallableDel Wrap(ParametrizedTransitionCallableDel callable,
                                                             string actionName)
        {
            var c = new ParametrizedTransitionCallableDel((IntPtr vars, Int32 result) => {
                try {
                    return callable(vars, result);
                } catch(Exception e) {
                    Console.Error.WriteLine("The execution of the action {0} failed with the exception \"{1}\"",
                                            actionName,
                                            e.Message);
                    return default(bool);
                }
            });

            _callbacks.Add(c);
            return c;
        }

        static List<object> _callbacks = new List<object>();
    }
}
