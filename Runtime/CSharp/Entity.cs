//
//  Entity.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-01.
//

﻿using System;

namespace Petri.Runtime
{
    /// <summary>
    /// A petri net's entity. The base class for an action or a transition.
    /// </summary>
    public class Entity : CInterop
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Runtime.Entity"/> class.
        /// </summary>
        protected Entity()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Runtime.Entity"/> class.
        /// </summary>
        /// <param name="handle">The native handle.</param>
        /// <param name="owning">If set to <c>true</c> this instance owns the native object.</param>
        public Entity(IntPtr handle, bool owning = true)
        {
            Handle = handle;

            if(!owning) {
                Release();
            }
        }

        /// <summary>
        /// Frees the native handle.
        /// This should never be called, as only an owning entity is cleaned, and only a transition or an action
        /// can be owning, and they both override this method.
        /// </summary>
        protected override void Clean()
        {
            // This should never be called, as only an owning entity is cleaned, and only a transition or an action
            throw new NotImplementedException();
        }
    }
}
