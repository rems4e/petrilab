# coding: utf-8

#
#  PetriNet.py
#  PetriLab
#
#  Created by Rémi Saurel on 2017-11-30.
#

from . import PetriRuntime


class _PetriNet_Interop(object):
    def __init__(self):
        from ctypes import cdll, c_size_t, c_char_p, c_void_p, c_bool, c_int
        petri_runtime = cdll.LoadLibrary('libPetriRuntime.so')

        self.petrinet_create = petri_runtime.PetriNet_create
        self.petrinet_create.restype = c_void_p
        self.petrinet_create.argtypes = [c_char_p]

        self.petrinet_create_debug = petri_runtime.PetriNet_createDebug
        self.petrinet_create_debug.restype = c_void_p
        self.petrinet_create_debug.argtypes = [c_char_p]

        self.petrinet_create_variables_count = petri_runtime.PetriNet_createVariablesCount
        self.petrinet_create_variables_count.restype = c_void_p
        self.petrinet_create_variables_count.argtypes = [c_char_p, c_size_t]

        self.petrinet_create_debug_variables_count = petri_runtime.PetriNet_createDebugVariablesCount
        self.petrinet_create_debug_variables_count.restype = c_void_p
        self.petrinet_create_debug_variables_count.argtypes = [c_char_p, c_size_t]

        self.petrinet_delete = petri_runtime.PetriNet_destroy
        self.petrinet_delete.restype = None
        self.petrinet_delete.argtypes = [c_void_p]

        self.petrinet_is_running = petri_runtime.PetriNet_isRunning
        self.petrinet_is_running.restype = c_bool
        self.petrinet_is_running.argtypes = [c_void_p]

        self.petrinet_run = petri_runtime.PetriNet_run
        self.petrinet_run.restype = None
        self.petrinet_run.argtypes = [c_void_p]

        self.petrinet_stop = petri_runtime.PetriNet_stop
        self.petrinet_stop.restype = None
        self.petrinet_stop.argtypes = [c_void_p]

        self.petrinet_join = petri_runtime.PetriNet_join
        self.petrinet_join.restype = None
        self.petrinet_join.argtypes = [c_void_p]

        self.petrinet_get_name = petri_runtime.PetriNet_getName
        self.petrinet_get_name.restype = c_char_p
        self.petrinet_get_name.argtypes = [c_void_p]

        self.petrinet_get_max_concurrency = petri_runtime.PetriNet_getMaxConcurrency
        self.petrinet_get_max_concurrency.restype = c_size_t
        self.petrinet_get_max_concurrency.argtypes = [c_void_p]

        self.petrinet_set_max_concurrency = petri_runtime.PetriNet_setMaxConcurrency
        self.petrinet_set_max_concurrency.restype = None
        self.petrinet_set_max_concurrency.argtypes = [c_void_p, c_size_t]

        self.petrinet_get_variables = petri_runtime.PetriNet_getVariables
        self.petrinet_get_variables.restype = c_void_p
        self.petrinet_get_variables.argtypes = [c_void_p]

        self.petrinet_set_log_output_path = petri_runtime.PetriNet_setLogOutputPath
        self.petrinet_set_log_output_path.restype = None
        self.petrinet_set_log_output_path.argtypes = [c_void_p, c_char_p]

        self.petrinet_set_log_verbosity = petri_runtime.PetriNet_setLogVerbosity
        self.petrinet_set_log_verbosity.restype = None
        self.petrinet_set_log_verbosity.argtypes = [c_void_p, c_int]

_PetriNet_Interop_Instance = _PetriNet_Interop()


class PetriNet(object):
    LogOutput_Console = 0

    class LogVerbosity(object):
        Nothing = 0x0
        States = 0x1
        Transitions = 0x2
        ReturnValues = 0x4

    def __init__(self, *args, **kwargs):
        if '_handle' in kwargs:
            self._handle = kwargs['_handle']
        else:
            is_debug = kwargs.get('is_debug', False)
            variables_count = kwargs.get('variables_count', None)
            name = kwargs.get('name', "MyPetriNet").encode("utf-8")
            if is_debug:
                if variables_count is not None:
                    self._handle = _PetriNet_Interop_Instance.petrinet_create_debug_variables_count(name, variables_count)
                else:
                    self._handle = _PetriNet_Interop_Instance.petrinet_create_debug(name)
            else:
                if variables_count is not None:
                    self._handle = _PetriNet_Interop_Instance.petrinet_create_variables_count(name, variables_count)
                else:
                    self._handle = _PetriNet_Interop_Instance.petrinet_create(name)

    def __del__(self):
        _PetriNet_Interop_Instance.petrinet_delete(self._handle)

    def is_running(self):
        return _PetriNet_Interop_Instance.petrinet_is_running(self._handle)

    def run(self):
        _PetriNet_Interop_Instance.petrinet_run(self._handle)

    def stop(self):
        _PetriNet_Interop_Instance.petrinet_stop(self._handle)

    def join(self):
        _PetriNet_Interop_Instance.petrinet_join(self._handle)

    def get_name(self):
        return _PetriNet_Interop_Instance.petrinet_get_name(self._handle).decode("utf-8")

    def set_max_concurrency(self, value):
        _PetriNet_Interop_Instance.petrinet_set_max_concurrency(self._handle, self._index, value)

    def get_max_concurrency(self):
        return _PetriNet_Interop_Instance.petrinet_get_max_concurrency(self._handle, self._index)

    max_concurrency = property(get_max_concurrency, set_max_concurrency)

    def get_variables(self):
        return PetriRuntime.PetriRuntime_VarSlot(_PetriNet_Interop_Instance.petrinet_get_variables(self._handle))

    def set_log_output_path(self, path):
        if path == PetriNet.LogOutput_Console:
            _PetriNet_Interop_Instance.petrinet_set_log_output_path(0)
        else:
            _PetriNet_Interop_Instance.petrinet_set_log_output_path(path.encode('utf-8'))

    def set_log_verbosity(self, verbosity):
        _PetriNet_Interop_Instance.petrinet_set_log_verbosity(verbosity)
