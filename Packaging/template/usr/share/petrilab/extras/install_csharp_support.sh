#!/bin/sh

gacutil -i /usr/share/petrilab/PetriRuntime.dll -package PetriRuntime

(cd /usr/lib/mono/PetriRuntime/ && chmod go+r "$(readlink /usr/lib/mono/PetriRuntime/PetriRuntime.dll)")
