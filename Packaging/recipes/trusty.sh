#!/bin/bash

#
#  trusty.sh
#  PetriLab
#
#  Created by Rémi Saurel on 2018-07-22.
#

function env_create_chroot {
    export ARCHIVE_URL='http://fr.archive.ubuntu.com/ubuntu/'
    export BEFORE_APT_CMD=$'echo "deb http://download.mono-project.com/repo/ubuntu stable-trusty main" > /etc/apt/sources.list.d/mono-official-stable.list
echo "deb http://ubuntu.mirrors.ovh.net/ubuntu trusty main universe
deb http://archive.ubuntu.com/ubuntu trusty-backports main restricted universe multiverse" >> /etc/apt/sources.list
'

    export APT_DEPENDENCIES="cmake3"
}

function env_use_chroot {
    export CC="/usr/bin/gcc-5"
    export CXX="/usr/bin/g++-5"

    export BUNDLE_DEPENDENCIES="--library /lib/x86_64-linux-gnu/libglib-2.0.so.0"
}
