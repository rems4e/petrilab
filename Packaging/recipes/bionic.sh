#!/bin/bash

#
#  stretch.sh
#  PetriLab
#
#  Created by Rémi Saurel on 2018-07-22.
#

function env_create_chroot {
    export ARCHIVE_URL='http://fr.archive.ubuntu.com/ubuntu/'
    export BEFORE_APT_CMD=$'echo "deb http://download.mono-project.com/repo/ubuntu stable-bionic main" > /etc/apt/sources.list.d/mono-official-stable.list
echo "deb http://ubuntu.mirrors.ovh.net/ubuntu bionic main universe" >> /etc/apt/sources.list
'

    export APT_DEPENDENCIES="cmake libpng16-16"
}

function env_use_chroot {
    export BUNDLE_DEPENDENCIES="--library /usr/lib/x86_64-linux-gnu/libpng16.so.16 --library /usr/lib/x86_64-linux-gnu/libglib-2.0.so.0"
}
