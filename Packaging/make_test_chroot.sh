#!/bin/bash

#
#  make_test_chroot.sh
#  PetriLab
#
#  Created by Rémi Saurel on 2017-11-28.
#

#
# This script takes the package /tmp/petrilab.deb which must be built for Debian stretch,
# then creates a stretch chroot from scratch, installs the package in it and run a series of execution
# of the petrilab executable as a form of validation tests.
#

set -e
set -u

if [[ $# == 0 || $1 != "okrunnow" ]]; then
    exec "$0" "okrunnow" "$0" "$@" 2> >(while read line; do
        if [[ $line == +* ]]; then
            echo -e "\e[01;33m$line\e[0m" >&2
        else
            echo -e "\e[01;31m$line\e[0m" >&2
        fi
    done)
fi

shift
cmdname=$1
shift

dir=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)

[ -f /tmp/petrilab.deb ] || { echo $'The PetriLab package is not available at /tmp/petrilab.deb.'; exit 1; }

set -x

tmp=$(mktemp -d)/chroot

sudo debootstrap --arch amd64 stretch "$tmp" http://httpredir.debian.org/debian

sudo mkdir -p "$tmp"/{host_tmp,proc}

sudo mount --bind /proc "$tmp"/proc/
sudo mount --bind /tmp "$tmp"/host_tmp/

sudo chroot "$tmp" << EOF
apt-get update

dpkg -i /host_tmp/petrilab.deb

printf "\n\n\n\n\n\n\n\n"

apt-get install -fy

printf "\n\n\n\n\n\n\n\n"

petrilab --version

printf "\n\n\n\n\n\n\n\n"

apt-get install -y build-essential gcc g++ python-dev python3-dev

printf "\n\n\n\n\n\n\n\n"

petrilab -kr /usr/share/petrilab/Examples/CBasic.petri
petrilab -kr /usr/share/petrilab/Examples/CppBasic.petri
petrilab -kr /usr/share/petrilab/Examples/PythonBasic.petri

petrilab -kr /usr/share/petrilab/Examples/CCountTo10.petri
petrilab -kr /usr/share/petrilab/Examples/CppCountTo10.petri
petrilab -kr /usr/share/petrilab/Examples/PythonCountTo10.petri

petrilab -kr /usr/share/petrilab/Examples/ExternalPetriNet/CConcurrent.petri
petrilab -kr /usr/share/petrilab/Examples/ExternalPetriNet/CppConcurrent.petri
petrilab -kr /usr/share/petrilab/Examples/ExternalPetriNet/PythonConcurrent.petri

petrilab -kr /usr/share/petrilab/Examples/Embedded/EmbeddedCCountTo10.petri

printf "\n\n\n\n\n\n\n\n"

cd /usr/share/petrilab/Examples/Integration
make

./runCCountTo10.out
./runCCountTo10plusN.out

./runCppCountTo10.out
./runCppCountTo10plusN.out

./runPython3CountTo10.py
./runPython3CountTo10plusN.py
./runPython2CountTo10plusN.py

apt-get install -y dirmngr
apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF
echo "deb http://download.mono-project.com/repo/debian stretch main" | tee /etc/apt/sources.list.d/mono-official.list
apt-get update
apt-get install mono-devel

/usr/share/petrilab/extras/install_csharp_support.sh

cd /usr/share/petrilab/Examples/Integration
make

petrilab -kr /usr/share/petrilab/Examples/CSharpBasic.petri
petrilab -kr /usr/share/petrilab/Examples/CSharpCountTo10.petri
petrilab -kr /usr/share/petrilab/Examples/ExternalPetriNet/CSharpConcurrent.petri

mono ./runCSharpCountTo10.exe
mono ./runCSharpCountTo10plusN.exe

EOF

read -p "Remove chroot $(realpath "$tmp"/..)? (y/N)? " result
case "$result" in
    "y" | "Y")
        sudo umount "$tmp"/proc
        sudo umount "$tmp"/host_tmp
        sudo rm -rf --one-file-system "$(realpath "$tmp"/..)";
        ;;
    *)
        ;;
esac
