#!/bin/bash

#
#  make_deb.sh
#  PetriLab
#
#  Created by Rémi Saurel on 2017-11-26.
#

#
# This script creates a deb package for the current host's arch (be it a chroot).
# Use --help as its first argument to see its usage.
#

set -e
set -u

if [[ $# == 0 || $1 != "okrunnow" ]]; then
    exec "$0" "okrunnow" "$0" "$@" 2> >(while read line; do
        if [[ $line == +* ]]; then
            echo -e "\e[01;33m$line\e[0m" >&2
        else
            echo -e "\e[01;31m$line\e[0m" >&2
        fi
    done)
fi

shift
cmdname=$1
shift

dir=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)

if [[ $# > 0 && $1 == "--help" ]]; then
    echo "Usage: $cmdname [--trial YYYY MM DD] [output.deb]"
    exit 0
fi

trial=false
if [[ $# -gt 4 && $1 == "--trial" ]]; then
    trial=true
    year=$2
    month=$3
    day=$4
    shift 4
fi
if [[ $# -le 1 ]]; then
    output=${1:-/tmp/petrilab.deb}
    echo "No output specified, will use $output."
else
    echo "Usage: $cmdname [--trial YYYY MM DD] output.deb" >&2
    exit 1
fi

if "$trial"; then
    "$dir"/make_trial.sh $year $month $day || { echo "make_trial.sh failed ($?)"; exit 2; }
fi

set -x

tmp=$(mktemp -d)/petrilab
mkdir -p "$tmp"

rm -r "$dir"/../build
mkdir "$dir"/../build
(cd "$dir"/../build; cmake ..)

mkdir "$tmp"/../build
(cd "$tmp"/../build; cmake "$dir"/../ -DCMAKE_BUILD_TYPE=Release && make -j"$(nproc)") || exit 3
(cd "$dir/"../Examples; git clean -fxd)
(cd "$dir/"../build; make examples) || exit 4
"$dir"/make_bundle.sh

cp -r "$dir"/DEBIAN "$tmp"/
chmod -R 755 "$tmp"/DEBIAN
cp -r "$dir"/template/* "$tmp"/ && echo "Done copying template files."

mkdir -p "$tmp"/usr/include/petrilab/{C,Cpp,Embedded,Python}
cp "$dir"/../Runtime/C/*.h "$tmp"/usr/include/petrilab/C/ && echo "Done copying C headers."
cp "$dir"/../Runtime/Cpp/*.h "$tmp"/usr/include/petrilab/Cpp/ && echo "Done copying C++ headers."
cp "$dir"/../Runtime/Embedded/*.h "$tmp"/usr/include/petrilab/Embedded/ && echo "Done copying EmbeddedC headers."
cp "$dir"/../Runtime/Python/{PetriInvoke.h,PetriRuntime.xxd} "$tmp"/usr/include/petrilab/Python/
cp "$dir"/../Runtime/*.py "$tmp"/usr/include/petrilab/Python/ && echo "Done copying Python files."
cp "$tmp"/../build/Runtime/libPetriRuntime.{so,a} "$tmp"/usr/lib/
cp "$tmp"/../build/Editor/execProxy "$tmp"/usr/share/petrilab/
cp "$dir"/../Editor/bin/PetriRuntime.dll "$tmp"/usr/share/petrilab/
cp "$dir"/../build/petrilab "$tmp"/usr/share/petrilab/

cp -r "$dir"/../Examples "$tmp"/usr/share/petrilab/
find "$tmp"/usr/share/petrilab/Examples -name "*.petri" -exec sed -i '/^[[:blank:]]*<LibPath Path="\(..\/\)\+Editor\/bin\/\?".*\/>$/d' {} \;

cp -r "$dir"/../Editor/Resources/licences "$tmp"/usr/share/petrilab/Licences
sed -i 's#../../petri#petrilab#g ; /.* # Remove in archive/d ; /.*-L.*/d ; s#-lib:.*#-lib:/usr/share/petrilab \\#g' \
    "$tmp"/usr/share/petrilab/Examples/Integration/Makefile
rm -r "$tmp"/usr/share/petrilab/Examples/{petri_cpp_to_c_cs_python.sh,upgrade.sh,old_api}
sn -R "$tmp"/usr/share/petrilab/PetriRuntime.dll "$dir"/../Editor/Projects/petrilab2048.key

find "$tmp" -name ".placeholder" -delete

chmod -R ugo=rX "$tmp"/usr/{bin,include,lib,share}
chmod -R go=rx \
    "$tmp"/usr/bin/petrilab \
    "$tmp"/usr/share/petrilab/{petrilab,execProxy} \
    "$tmp"/usr/share/petrilab/Examples/Integration/*.py \
    "$tmp"/usr/share/petrilab/extras/*.sh
chmod -R ugo+w "$tmp"/usr/share/petrilab/Examples/

(cd "$tmp"/.. && dpkg-deb --build petrilab) || exit 6

mv "$tmp"/../petrilab.deb "$output"

chmod -R +w "$(realpath "$tmp"/..)"
rm -rf "$(realpath "$tmp"/..)"

echo "Package generated successfully!"
