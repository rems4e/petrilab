#!/bin/bash

#
#  make_chroot.sh
#  PetriLab
#
#  Created by Rémi Saurel on 2018-07-22.
#

#
# TODO: usage
#

set -e
set -u

if [[ $# == 0 || $1 != "okrunnow" ]]; then
    exec "$0" "okrunnow" "$0" "$@" 2> >(while read line; do
        if [[ $line == +* ]]; then
            echo -e "\e[01;33m$line\e[0m" >&2
        else
            echo -e "\e[01;31m$line\e[0m" >&2
        fi
    done)
fi

shift
cmdname=$1
shift

dir=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)

if [[ $# > 0 && $1 == "--help" ]]; then
    echo "Usage: $cmdname <dest_dir> <chroot_recipe>"
    exit 0
fi

if [[ $# != 2 ]]; then
    echo "Usage: $cmdname <dest_dir> <chroot_recipe>" >&2
    exit 1
fi

if [[ $(id -u) != 0 ]]; then
    echo "This script must be run as root." >&2
    exit 2
fi

if [[ ! -d "$1" ]]; then
    echo "The chroot parent directory doesn't exist." >&2
    exit 3
fi

if [[ -a "$1"/"${2}_chroot" ]]; then
    echo "The destination '$1/${2}_chroot' already exists." >&2
    exit 4
fi

set -x

. "$dir"/recipes/"${2}".sh
env_create_chroot

chroot_path=$1/${2}_chroot

debootstrap --arch amd64 $2 "$chroot_path" "$ARCHIVE_URL"

mkdir -p "$chroot_path"/proc

mount --bind /proc "$chroot_path"/proc/

chroot "$chroot_path" << EOF
apt-get update

apt-get install -y dirmngr
apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF

bash -c '$BEFORE_APT_CMD'

apt-get update
apt-get install build-essential git gcc g++ python-dev python3-dev mono-devel gtk-sharp2 $APT_DEPENDENCIES

EOF

umount "$chroot_path"/proc/sys/fs/binfmt_misc || true
umount "$chroot_path"/proc/
