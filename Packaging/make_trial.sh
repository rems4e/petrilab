#!/bin/bash

#
#  make_trial.sh
#  PetriLab
#
#  Created by Rémi Saurel on 2017-11-27.
#

#
# This scripts changes the source code of the project to make a trial version that expires at the given date.
# **WARNING** This is a destructive operation, meaning that the source code is **MODIFIED**. Granted,
# this is not a big deal in a project using VCS but beware not commiting any unwanted changes.
# Use --help as its first argument to see its usage.
#

set -e
set -u

if [[ $# == 0 || $1 != "okrunnow" ]]; then
    exec "$0" "okrunnow" "$0" "$@" 2> >(while read line; do
        if [[ $line == +* ]]; then
            echo -e "\e[01;33m$line\e[0m" >&2
        else
            echo -e "\e[01;31m$line\e[0m" >&2
        fi
    done)
fi

shift
cmdname=$1
shift

dir=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)

if [[ $# > 0 && $1 == "--help" ]]; then
    echo "Usage: $0 YYYY MM DD"
    exit 0
fi

[[ $# == 3 ]] || { echo "Usage: $0 YYYY MM DD" >&2; exit 1; }

set -x

read -p "Do you want to create a trial period valid until $1-$2-$3 (y/N)? " result

case "$result" in
    "y" | "Y")
        ;;
    *)
        echo "Abort."
        exit 2;
        ;;
esac

sed -Ei "s/TrialPeriodValidUntil => .*/TrialPeriodValidUntil => new DateTime($1, $2, $3);/g" \
    "$dir"/../Editor/Sources/Application/Common/Configuration.cs || exit 3

echo "Done, you may now build the application"
