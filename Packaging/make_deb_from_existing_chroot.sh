#!/bin/bash

#
#  make_deb_from_existing_chroot.sh
#  PetriLab
#
#  Created by Rémi Saurel on 2018-03-13.
#

#
# This script cross compiles the package into the chroot pointed to by the first argument.
#

set -e
set -u

if [[ $# == 0 || $1 != "okrunnow" ]]; then
    exec "$0" "okrunnow" "$0" "$@" 2> >(while read line; do
        if [[ $line == +* ]]; then
            echo -e "\e[01;33m$line\e[0m" >&2
        else
            echo -e "\e[01;31m$line\e[0m" >&2
        fi
    done)
fi

shift
cmdname=$1
shift

dir=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)

if [[ $# > 0 && $1 == "--help" ]]; then
    echo "Usage: $cmdname <chroot_root_dir>"
    exit 0
fi

if [[ $# < 1 ]]; then
    echo "Usage: $cmdname <chroot_root_dir>" >&2
    exit 1
fi

if [[ $(id -u) != 0 ]]; then
    echo "This script must be run as root." >&2
    exit 2
fi

set -x

name=$(basename "$1")
name=${name%_chroot}

# Simply check that we are actually in a chroot
chroot "$1" ls > /dev/null

mkdir -p "$1"/{proc,host_tmp}

mount --bind /proc/ "$1"/proc/
mount --bind /tmp "$1"/host_tmp/

rm -rf "$1"/host_git
cp -r "$dir"/.. "$1"/host_git/
sed -i "s/\(Version: .*\)/\1+$name/g" "$1"/host_git/Packaging/DEBIAN/control


. "$dir"/recipes/"${name}".sh
env_use_chroot

chroot "$1"/ << EOF
apt-get update
apt-get upgrade -y

cd /host_git/Packaging
./make_deb.sh /host_tmp/petrilab_"$name".deb
EOF


umount "$1"/proc/sys/fs/binfmt_misc || true
umount "$1"/proc/
umount "$1"/host_tmp/
