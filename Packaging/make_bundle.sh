#!/bin/bash

dir=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)

mkbundle -o "$dir"/../build/petrilab \
    --simple \
    "$dir"/../Editor/bin/Petri.exe \
    --deps \
    --static \
    --env PETRILAB_AM_I_IN_BUNDLE=YESIAM \
    -L /usr/lib/mono/4.5/ \
    -L /usr/lib/cli/gtk-sharp-2.0 \
    -L /usr/lib/cli/glib-sharp-2.0 \
    -L /usr/lib/cli/gdk-sharp-2.0 \
    -L /usr/lib/cli/pango-sharp-2.0  \
    -L /usr/lib/cli/atk-sharp-2.0 \
    -L "$dir"/../Editor/bin/ \
	--library /usr/lib/x86_64-linux-gnu/libffi.so.6 \
	--library /usr/lib/x86_64-linux-gnu/libgobject-2.0.so.0 \
	--library /usr/lib/x86_64-linux-gnu/libdatrie.so.1 \
	--library /usr/lib/x86_64-linux-gnu/libthai.so.0 \
	--library /usr/lib/x86_64-linux-gnu/libpango-1.0.so.0 \
	--library /usr/lib/x86_64-linux-gnu/libatk-1.0.so.0 \
	--library /usr/lib/x86_64-linux-gnu/libpixman-1.so.0 \
	--library /lib/x86_64-linux-gnu/libz.so.1 \
	--library /usr/lib/x86_64-linux-gnu/libfreetype.so.6 \
	--library /lib/x86_64-linux-gnu/libexpat.so.1 \
	--library /usr/lib/x86_64-linux-gnu/libfontconfig.so.1 \
	--library /usr/lib/x86_64-linux-gnu/libXau.so.6 \
	--library /lib/x86_64-linux-gnu/libbsd.so.0 \
	--library /usr/lib/x86_64-linux-gnu/libXdmcp.so.6 \
	--library /usr/lib/x86_64-linux-gnu/libxcb.so.1 \
	--library /usr/lib/x86_64-linux-gnu/libxcb-shm.so.0 \
	--library /usr/lib/x86_64-linux-gnu/libxcb-render.so.0 \
	--library /usr/lib/x86_64-linux-gnu/libX11.so.6 \
	--library /usr/lib/x86_64-linux-gnu/libXrender.so.1 \
	--library /usr/lib/x86_64-linux-gnu/libXext.so.6 \
	--library /usr/lib/x86_64-linux-gnu/libcairo.so.2 \
	--library /usr/lib/x86_64-linux-gnu/libgraphite2.so.3 \
	--library /usr/lib/x86_64-linux-gnu/libharfbuzz.so.0 \
    --library /usr/lib/x86_64-linux-gnu/libpango-1.0.so.0 \
	--library /usr/lib/x86_64-linux-gnu/libpangoft2-1.0.so.0 \
	--library /usr/lib/x86_64-linux-gnu/libpangocairo-1.0.so.0 \
	--library /usr/lib/x86_64-linux-gnu/libgmodule-2.0.so.0 \
	--library /usr/lib/x86_64-linux-gnu/libgio-2.0.so.0 \
	--library /usr/lib/x86_64-linux-gnu/libgdk_pixbuf-2.0.so.0 \
	--library /usr/lib/x86_64-linux-gnu/libXinerama.so.1 \
	--library /usr/lib/x86_64-linux-gnu/libXi.so.6 \
	--library /usr/lib/x86_64-linux-gnu/libXrandr.so.2 \
	--library /usr/lib/x86_64-linux-gnu/libXfixes.so.3 \
	--library /usr/lib/x86_64-linux-gnu/libXcursor.so.1 \
	--library /usr/lib/x86_64-linux-gnu/libXcomposite.so.1 \
	--library /usr/lib/x86_64-linux-gnu/libXdamage.so.1 \
	--library /usr/lib/x86_64-linux-gnu/libgdk-x11-2.0.so.0 \
	--library /usr/lib/x86_64-linux-gnu/libgtk-x11-2.0.so.0 \
	--library /usr/lib/cli/gtk-sharp-2.0/libgtksharpglue-2.so  \
	--library /usr/lib/cli/glib-sharp-2.0/libglibsharpglue-2.so \
    --library /usr/lib/cli/gdk-sharp-2.0/libgdksharpglue-2.so \
    --library /usr/lib/cli/pango-sharp-2.0/libpangosharpglue-2.so \
	--library /usr/lib/libMonoPosixHelper.so \
    --machine-config /etc/mono/4.5/machine.config \
    --config "$dir"/bundle_config.config \
    $BUNDLE_DEPENDENCIES
