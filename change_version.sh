#!/bin/bash

#
#  change_version.sh
#  PetriLab
#
#  Created by Rémi Saurel on 2017-02-18.
#

set -e
set -u

dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

if [[ $# < 1 || $# > 2 ]]; then
    echo "Invalid usage!" 1>&2
    echo "Usage: $0 <version>" 1>&2
    exit 1
fi

version=${1%%-*}
suffix=
hyphen=

if [[ "$version" != "$1" ]]; then
    suffix=${1#*-}
    hyphen=-
fi

echo -n "Changing version to ${version}${hyphen}${suffix}… "

sed -Ei "s#Version: .*+#Version: ${version}${hyphen}${suffix}#g" \
    "$dir/Packaging/deb/DEBIAN/control" || (echo "Failed to change the .deb version!" 1>&2; exit 2)

sed -Ei "s#<ReleaseVersion>[^<]+</ReleaseVersion>#<ReleaseVersion>${version}${hyphen}${suffix}</ReleaseVersion>#g" \
    "$dir/Editor/Projects/PetriRuntime.csproj" \
    "$dir/Editor/Projects/Petri.csproj" \
    "$dir/Editor/Projects/PetriMac.csproj" \
    "$dir/Editor/Test/Test.csproj" || (echo "Failed to change the .csproj versions!" 1>&2; exit 2)

sed -Ei "s#version = ([0-9]+\.?)+[-a-zA-Z0-9]*#version = ${version}${hyphen}${suffix}#g" \
    "$dir/Editor/Petri.sln" || (echo "Failed to change the .sln version!" 1>&2; exit 2)

perl -0777 -i -pe "s#(<key>CFBundleShortVersionString</key>\n\s+<string>)([0-9]+\.?)+[-a-zA-Z0-9]*(</string>)#\${1}${version}${hyphen}${suffix}\${3}#igs" \
    "$dir/Editor/Resources/Info.plist" || (echo "Failed to change the .plist version!" 1>&2; exit 2)

sed -Ei "s#\[assembly: AssemblyVersion\(\"([0-9.]+)+\"\)\]#[assembly: AssemblyVersion(\"${version}\")]#g" \
    "$dir/Editor/Sources/AssemblyInfo.cs" || (echo "Failed to change the PetriLab AssemblyInfo.cs version!" 1>&2; exit 2)

sed -Ei "s#\[assembly: AssemblyVersion\(\"([0-9.]+)+\"\)\]#[assembly: AssemblyVersion(\"${version}\")]#g" \
    "$dir/Runtime/CSharp/AssemblyInfo.cs" || (echo "Failed to change the PetriRuntime AssemblyInfo.cs version!" 1>&2; exit 2)

perl -0777 -i -pe "s#(public static string VersionSuffix \{\n\s+get \{\n\s+return \")([^\n]+)(\n\s+\}\n\s+\})#\$1${hyphen}${suffix}\";\$3#igs" \
    "$dir/Editor/Sources/Application/Common/Application.cs" || (echo "Failed to change the Application.cs version!" 1>&2; exit 2)

echo "Done."
