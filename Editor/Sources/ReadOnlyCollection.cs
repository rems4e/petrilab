//
//  ReadOnlyCollection.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-19.
//

using System.Collections;
using System.Collections.Generic;

namespace Petri
{
    /// <summary>
    /// A IReadOnlyCollection, but not a dumb one such as System.Collections.Generic one.
    /// At least contains a Contains method.
    /// </summary>
    public class ReadOnlyCollection<T> : IReadOnlyCollection<T>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.ReadOnlyCollection`1"/> class.
        /// </summary>
        /// <param name="coll">Coll.</param>
        public ReadOnlyCollection(ICollection<T> coll)
        {
            _collection = coll;
        }

        /// <summary>
        /// Gets the count.
        /// </summary>
        /// <value>The count.</value>
        public int Count => _collection.Count;

        /// <summary>
        /// Gets the enumerator.
        /// </summary>
        /// <returns>The enumerator.</returns>
        public IEnumerator<T> GetEnumerator()
        {
            return _collection.GetEnumerator();
        }

        /// <summary>
        /// Checks whether the given object belongs to the collection or not.
        /// </summary>
        /// <returns><c>true</c> if the object is in the collection, otherwise <c>false</c>.</returns>
        /// <param name="obj">Object.</param>
        public bool Contains(T obj)
        {
            return _collection.Contains(obj);
        }

        /// <summary>
        /// Gets the enumerator.
        /// </summary>
        /// <returns>The enumerator.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return _collection.GetEnumerator();
        }

        ICollection<T> _collection;
    }
}
