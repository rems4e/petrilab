//
//  StringExtensions.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2017-11-19.
//

using System;

namespace Petri
{
    /// <summary>
    /// String extension class.
    /// </summary>
    public static class StringExtensions
    {
        /// <summary>
        /// Replaces tabs by spaces, and merges remaining whitespaces sequence into one whitespace.
        /// </summary>
        /// <returns>The trimmed string.</returns>
        /// <param name="s">The string to clean.</param>
        public static string TrimSpaces(this String s)
        {
            s = s.Replace('\t', ' ');
            while(s.Contains("  ")) {
                s = s.Replace("  ", " ");
            }

            s = s.Replace("\n ", "\n");

            while(s.Contains("\n\n")) {
                s = s.Replace("\n\n", "\n");
            }

            return s;
        }

        /// <summary>
        /// Wrapper of the string.EndsWith method, with culture invariant string comparison.
        /// </summary>
        /// <returns><c>true</c>, if the current instance ends with the provided value, <c>false</c> otherwise.</returns>
        /// <param name="s">The current instance.</param>
        /// <param name="value">The value to test.</param>
        public static bool EndsWithInv(this String s, string value)
        {
            return s.EndsWith(value, StringComparison.InvariantCulture);
        }

        /// <summary>
        /// Wrapper of the string.StartsWith method, with culture invariant string comparison.
        /// </summary>
        /// <returns><c>true</c>, if the current instance starts with the provided value, <c>false</c> otherwise.</returns>
        /// <param name="s">The current instance.</param>
        /// <param name="value">The value to test.</param>
        public static bool StartsWithInv(this String s, string value)
        {
            return s.StartsWith(value, StringComparison.InvariantCulture);
        }

        /// <summary>
        /// Wrapper of the string.IndexOf method, with culture invariant string comparison.
        /// </summary>
        /// <returns>The index of the provided substring in the current instance.</returns>
        /// <param name="s">The current instance.</param>
        /// <param name="value">The value to test.</param>
        public static int IndexOfInv(this String s, string value)
        {
            return s.IndexOf(value, StringComparison.InvariantCulture);
        }

        /// <summary>
        /// Wrapper of the string.IndexOf method, with culture invariant string comparison.
        /// </summary>
        /// <returns>The index of the provided substring in the current instance.</returns>
        /// <param name="s">The current instance.</param>
        /// <param name="value">The value to test.</param>
        /// <param name="startIndex">The index to start searching from</param>
        public static int IndexOfInv(this String s, string value, int startIndex)
        {
            return s.IndexOf(value, startIndex, StringComparison.InvariantCulture);
        }

        /// <summary>
        /// Wrapper of the string.LastIndexOf method, with culture invariant string comparison.
        /// </summary>
        /// <returns>The last index of the provided substring in the current instance.</returns>
        /// <param name="s">The current instance.</param>
        /// <param name="value">The value to test.</param>
        public static int LastIndexOfInv(this String s, string value)
        {
            return s.LastIndexOf(value, StringComparison.InvariantCulture);
        }

        /// <summary>
        /// Escapes a string for it to be valid C/C++/C# code.
        /// </summary>
        /// <returns>The escaped string.</returns>
        /// <param name="s">The string to escape.</param>
        public static string Escape(this String s)
        {
            var builder = new System.Text.StringBuilder(s.Length);
            foreach(var c in s) {
                if(c == '\\' || c == '"') {
                    builder.Append('\\');
                }
                builder.Append(c);
            }

            return builder.ToString();
        }
    }
}
