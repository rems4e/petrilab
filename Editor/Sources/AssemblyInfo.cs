//
//  AssemblyInfo.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2014-12-09.
//

using System.Reflection;

[assembly: AssemblyTitle("PetriLab")]
[assembly: AssemblyDescription("A petri net editor, code generator, compiler and debugger")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Sigilence Technologies")]
[assembly: AssemblyProduct("PetriLab")]
[assembly: AssemblyCopyright("Rémi Saurel")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: AssemblyVersion("3.0.0")]
