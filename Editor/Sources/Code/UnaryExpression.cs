//
//  UnaryExpression.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-05.
//

using System;
using System.Collections.Generic;

namespace Petri.Code
{
    /// <summary>
    /// A unary expression.
    /// </summary>
    public class UnaryExpression : Expression
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Code.UnaryExpression"/> class.
        /// </summary>
        /// <param name="language">The programming language of the expression.</param>
        /// <param name="o">The operator of the expression.</param>
        /// <param name="expr">The expression associated to the unary operator.</param>
        public UnaryExpression(Language language, Operator.Name o, Expression expr) : base(language,
                                                                                           o)
        {
            Expression = expr;
        }

        /// <summary>
        /// Gets the expression associated to the unary operator.
        /// </summary>
        /// <value>The expression.</value>
        public Expression Expression {
            get;
            private set;
        }

        /// <summary>
        /// Checks whether this instance uses the specified function as part of one of its subexpression or itself.
        /// </summary>
        /// <returns>true</returns>
        /// <c>false</c>
        /// <param name="f">The function to test.</param>
        public override bool UsesFunction(Function f)
        {
            return Expression.UsesFunction(f);
        }

        /// <summary>
        /// Generates the source code corresponding to the expression.
        /// </summary>
        /// <returns>The code.</returns>
        public override string MakeCode()
        {
            string parenthesized = Expression.Parenthesize(this,
                                                           Expression,
                                                           Expression.MakeCode());
            switch(Operator) {
            case Code.Operator.Name.FunCall:
                throw new Exception(Application.Configuration.GetLocalized("Already managed in FunctionInvocation class!"));
            case Code.Operator.Name.UnaryPlus:
                return "+" + parenthesized;
            case Code.Operator.Name.UnaryMinus:
                return "-" + parenthesized;
            case Code.Operator.Name.LogicalNot:
                return "!" + parenthesized;
            case Code.Operator.Name.BitwiseNot:
                return "~" + parenthesized;
            case Code.Operator.Name.Indirection:
                return "*" + parenthesized;
            case Code.Operator.Name.AddressOf:
                return "&" + parenthesized;
            case Code.Operator.Name.PreIncr:
                return "++" + parenthesized;
            case Code.Operator.Name.PreDecr:
                return "--" + parenthesized;
            case Code.Operator.Name.PostIncr:
                return parenthesized + "++";
            case Code.Operator.Name.PostDecr:
                return parenthesized + "--";
            }
            throw new Exception(Application.Configuration.GetLocalized("Operator not implemented!"));
        }

        /// <summary>
        /// Generates a user-readable form of the expression.
        /// </summary>
        /// <returns>The user readable.</returns>
        public override string MakeUserReadable()
        {
            string parenthesized = Expression.Parenthesize(this,
                                                           Expression,
                                                           Expression.MakeUserReadable());
            switch(Operator) {
            case Code.Operator.Name.FunCall:
                throw new Exception(Application.Configuration.GetLocalized("Already managed in FunctionInvocation class!"));
            case Code.Operator.Name.UnaryPlus:
                return "+" + parenthesized;
            case Code.Operator.Name.UnaryMinus:
                return "-" + parenthesized;
            case Code.Operator.Name.LogicalNot:
                return "!" + parenthesized;
            case Code.Operator.Name.BitwiseNot:
                return "~" + parenthesized;
            case Code.Operator.Name.Indirection:
                return "*" + parenthesized;
            case Code.Operator.Name.AddressOf:
                return "&" + parenthesized;
            case Code.Operator.Name.PreIncr:
                return "++" + parenthesized;
            case Code.Operator.Name.PreDecr:
                return "--" + parenthesized;
            case Code.Operator.Name.PostIncr:
                return parenthesized + "++";
            case Code.Operator.Name.PostDecr:
                return parenthesized + "--";
            }

            throw new Exception(Application.Configuration.GetLocalized("Operator not implemented!"));
        }

        /// <summary>
        /// Gets all the literals used to construct this expression.
        /// </summary>
        /// <returns>The literals.</returns>
        public override List<LiteralExpression> GetLiterals()
        {
            return Expression.GetLiterals();
        }
    }
}

