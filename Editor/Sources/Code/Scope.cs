//
//  Scope.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-24.
//

using System;

namespace Petri.Code
{
    /// <summary>
    /// A scope such as a namespace or a class. A Scope object can be enclosed in another Scope object, modeling nested scopes.
    /// </summary>
    public class Scope : IEquatable<Scope>, ICloneable
    {
        /// <summary>
        /// Creates a namespace.
        /// </summary>
        /// <returns>The new scope.</returns>
        /// <param name="language">The programming language.</param>
        /// <param name="ns">The name of the new namespace</param>
        /// <param name="enclosing">The optional enclosing scope of the new namespace.</param>
        public static Scope MakeFromNamespace(Language language, string ns, Scope enclosing = null)
        {
            Scope s = new Scope();
            s.Language = language;
            s.Class = null;
            s.Namespace = ns;
            s.Enclosing = enclosing;

            return s;
        }

        /// <summary>
        /// Creates a new class.
        /// </summary>
        /// <returns>The new class.</returns>
        /// <param name="classType">The type that represents the class.</param>
        public static Scope MakeFromClass(Type classType)
        {
            Scope s = new Scope();
            s.Language = classType.Language;
            s.Class = classType;
            s.Namespace = null;
            s.Enclosing = classType.Enclosing;

            return s;
        }

        /// <summary>
        /// Combines 2 scopes into one, making the most outer scope of the <paramref name="inner"/> scope having scope <paramref name="enclosing"/> as enclosing scope.
        /// </summary>
        /// <returns>The new scopes.</returns>
        /// <param name="enclosing">The new most outer scope of the the .</param>
        /// <param name="inner">Inner.</param>
        public static Scope MakeFromScopes(Scope enclosing, Scope inner)
        {
            if(inner != null) {
                inner = (Scope)inner.Clone();
                Scope i = inner;
                while(true) {
                    if(i.Enclosing != null) {
                        i = i.Enclosing;
                    } else {
                        i.Enclosing = enclosing;
                        break;
                    }
                }

                return inner;
            } else {
                return enclosing;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is a class.
        /// </summary>
        /// <value><c>true</c> if this instance is a class; otherwise, <c>false</c>.</value>
        public bool IsClass {
            get {
                return Class != null;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is a namespace.
        /// </summary>
        /// <value><c>true</c> if this instance is a namespace; otherwise, <c>false</c>.</value>
        public bool IsNamespace {
            get {
                return Namespace != null;
            }
        }

        /// <summary>
        /// Gets the class of the scope. If the scope is a namespace, this returns <c>null</c>.
        /// </summary>
        /// <value>The class.</value>
        public Type Class {
            get;
            private set;
        }

        /// <summary>
        /// Gets the namespace of the scope. If the scope is a class, this returns <c>null</c>.
        /// </summary>
        /// <value>The class.</value>
        public string Namespace {
            get;
            private set;
        }

        /// <summary>
        /// Gets the programming language of the scope.
        /// </summary>
        /// <value>The language.</value>
        public Language Language {
            get;
            private set;
        }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents the current <see cref="Petri.Code.Scope"/>.
        /// </summary>
        /// <returns>A <see cref="System.String"/> that represents the current <see cref="Petri.Code.Scope"/>.</returns>
        public override string ToString()
        {
            string enclosing = "";
            if(Enclosing != null) {
                enclosing = Enclosing.ToString();
            }

            if(IsNamespace) {
                return enclosing + (Namespace.Length > 0 ? Namespace + Separator : "");
            } else {
                return Class.ToString() + Separator;
            }
        }

        /// <summary>
        /// Gets the string separating the components of a scope. This depends on the language (:: for C++, . for C#, etc.).
        /// </summary>
        /// <value>The separator.</value>
        public string Separator {
            get {
                return Scope.GetSeparator(Language);
            }
        }

        /// <summary>
        /// Gets the string separating the components of a scope corresponding to the given language.
        /// </summary>
        /// <returns>The separator.</returns>
        /// <param name="lang">Lang.</param>
        public static string GetSeparator(Language lang)
        {
            switch(lang) {
            case Language.C: // Here we need a separator, but it should not be met in real code…
            case Language.EmbeddedC: // Same as before
            case Language.Cpp:
                return "::";
            case Language.CSharp:
            case Language.Python:
                return ".";
            }

            throw new Exception("Scope.GetSeparator: Should not get there!");
        }

        /// <summary>
        /// Gets the enclosing scope, or <c>null</c> if none exist.
        /// </summary>
        /// <value>The enclosing.</value>
        public Scope Enclosing {
            get;
            private set;
        }

        /// <summary>
        /// Determines whether the specified <see cref="Petri.Code.Scope"/> is equal to the current <see cref="Petri.Code.Scope"/>.
        /// </summary>
        /// <param name="s">The <see cref="Petri.Code.Scope"/> to compare with the current <see cref="Petri.Code.Scope"/>.</param>
        /// <returns><c>true</c> if the specified <see cref="Petri.Code.Scope"/> is equal to the current
        /// <see cref="Petri.Code.Scope"/>; otherwise, <c>false</c>.</returns>
        public bool Equals(Scope s)
        {
            return ToString() == s.ToString();
        }

        /// <summary>
        /// Clone this instance.
        /// </summary>
        public object Clone()
        {
            var enclosing = Enclosing;
            if(enclosing != null) {
                enclosing = (Scope)enclosing.Clone();
            }

            var scope = new Scope();
            scope.Enclosing = enclosing;
            scope.Class = Class;
            scope.Namespace = Namespace;
            scope.Language = Language;

            return scope;
        }
    }
}

