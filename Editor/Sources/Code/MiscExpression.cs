//
//  MiscExpression.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-05.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Petri.Code
{
    /// <summary>
    /// An empty expression.
    /// </summary>
    public class EmptyExpression : Expression
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Code.EmptyExpression"/> class.
        /// </summary>
        /// <param name="language">Th programming language.</param>
        /// <param name="doWeCare">If set to <c>true</c>, then an exception will be thrown upon code generation. Otherwise, a blank string will be generated.</param>
        public EmptyExpression(Language language, bool doWeCare) : base(language,
                                                                        Code.Operator.Name.None)
        {
            DoWeCare = doWeCare;
        }

        /// <summary>
        /// Checks whether this instance uses the specified function. Always <c>false</c> here.
        /// </summary>
        /// <returns>true</returns>
        /// <c>false</c>
        /// <param name="f">The function to test.</param>
        public override bool UsesFunction(Function f)
        {
            return false;
        }

        /// <summary>
        /// Generates the source code corresponding to the expression.
        /// </summary>
        /// <returns>The code.</returns>
        public override string MakeCode()
        {
            if(DoWeCare) {
                throw new Exception(Application.Configuration.GetLocalized("Empty expression!"));
            } else
                return "";
        }

        /// <summary>
        /// Generates a user-readable form of the expression.
        /// </summary>
        /// <returns>The user readable.</returns>
        public override string MakeUserReadable()
        {
            return "";
        }

        /// <summary>
        /// Gets all the literals used to construct this expression.
        /// </summary>
        /// <returns>The literals.</returns>
        public override List<LiteralExpression> GetLiterals()
        {
            return new List<LiteralExpression>();
        }

        /// <summary>
        /// Gets a value indicating whether this <see cref="Petri.Code.EmptyExpression"/> will throw upon code generation or not.
        /// </summary>
        /// <value><c>true</c> if do we care; otherwise, <c>false</c>.</value>
        public bool DoWeCare {
            get;
            private set;
        }

    }

    /// <summary>
    /// A Bracketed expression.
    /// </summary>
    public class BracketedExpression : Expression
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Code.BracketedExpression"/> class.
        /// </summary>
        /// <param name="b">The expression that comes before the brackets.</param>
        /// <param name="expr">The bracketed expression.</param>
        /// <param name="a">The expression that comes after the brackets.</param>
        public BracketedExpression(Expression b, Expression expr, Expression a) : base(b.Language,
                                                                                       Code.Operator.Name.None)
        {
            Before = b;
            Expression = expr;
            After = a;
        }

        /// <summary>
        /// Gets the expression.
        /// </summary>
        /// <value>The expression.</value>
        public Expression Expression {
            get; private set;
        }

        /// <summary>
        /// Gets the expression that comes before the brackets.
        /// </summary>
        /// <value>The before.</value>
        public Expression Before {
            get; private set;
        }

        /// <summary>
        /// Gets the expression that comes after the brackets.
        /// </summary>
        /// <value>The after.</value>
        public Expression After {
            get; private set;
        }

        /// <summary>
        /// Checks whether this instance uses the specified function as part of one of its subexpression or itself.
        /// </summary>
        /// <returns>true</returns>
        /// <c>false</c>
        /// <param name="f">The function to test.</param>
        public override bool UsesFunction(Function f)
        {
            return Expression.UsesFunction(f) || Before.UsesFunction(f) || After.UsesFunction(f);
        }

        /// <summary>
        /// Generates the source code corresponding to the expression.
        /// </summary>
        /// <returns>The code.</returns>
        public override string MakeCode()
        {
            return Before.MakeCode() + "{" + Expression.MakeCode() + "}" + After.MakeCode();
        }

        /// <summary>
        /// Generates a user-readable form of the expression.
        /// </summary>
        /// <returns>The user readable.</returns>
        public override string MakeUserReadable()
        {
            return Before.MakeUserReadable() + "{" + Expression.MakeUserReadable() + "}" + After.MakeUserReadable();
        }

        /// <summary>
        /// Gets all the literals used to construct this expression.
        /// </summary>
        /// <returns>The literals.</returns>
        public override List<LiteralExpression> GetLiterals()
        {
            var l = Before.GetLiterals();
            l.AddRange(Expression.GetLiterals());
            l.AddRange(After.GetLiterals());
            return l;
        }

    }

    /// <summary>
    /// A literal expression.
    /// </summary>
    public class LiteralExpression : Expression
    {
        /// <summary>
        /// Creates a literal expression from a string.
        /// If the expression starts with a $ sign, then it is created as a <see cref="Petri.Code.VariableExpression"/>.
        /// If the expression contains a bracketed component, a new <see cref="Petri.Code.BracketedExpression"/> is created.
        /// </summary>
        /// <returns>The new literal expression.</returns>
        /// <param name="s">S.</param>
        /// <param name="language">Language.</param>
        static public Expression CreateFromString(string s, Language language)
        {
            if(s.Length >= 2 && s.StartsWithInv("$") && (char.IsLower(s[1]) || s[1] == '_')) {
                return new VariableExpression(s, language);
            }
            if(s.Contains("{")) {
                var tup = Code.Expression.Tokenize(s);
                int currentIndex = 0;
                while(currentIndex < tup.Item1.Length) {
                    int index = tup.Item1.Substring(currentIndex).IndexOfInv("@") + currentIndex;
                    if(index == -1) {
                        break;
                    }
                    int lastIndex = tup.Item1.Substring(index + 1).IndexOfInv("@") + index + 1;
                    int expr = int.Parse(tup.Item1.Substring(index + 1,
                                                             lastIndex - (index + 1)));
                    if(tup.Item2[expr].Item1 == Token.Brackets) {
                        return new BracketedExpression(Code.Expression.CreateFromTokenizedString(tup.Item1.Substring(0,
                                                                                                                     index),
                                                                                                 language,
                                                                                                 null,
                                                                                                 null,
                                                                                                 tup.Item2),
                                                       Code.Expression.CreateFromTokenizedString(tup.Item2[expr].Item2.Substring(1,
                                                                                                                                 tup.Item2[expr].Item2.Length - 2),
                                                                                                 language,
                                                                                                 null,
                                                                                                 null,
                                                                                                 tup.Item2),
                                                       Code.Expression.CreateFromTokenizedString(tup.Item1.Substring(lastIndex + 1),
                                                                                                 language,
                                                                                                 null,
                                                                                                 null,
                                                                                                 tup.Item2));
                    } else {
                        currentIndex = lastIndex + 1;
                    }
                }
            }
            return new LiteralExpression(language, s);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Code.LiteralExpression"/> class.
        /// </summary>
        /// <param name="language">The programing language.</param>
        /// <param name="expr">The expression</param>
        protected LiteralExpression(Language language, string expr) : base(language,
                                                                           Code.Operator.Name.None)
        {
            Expression = expr.Trim();
        }

        /// <summary>
        /// Gets or sets the expression.
        /// </summary>
        /// <value>The expression.</value>
        public virtual string Expression {
            get;
            set;
        }

        /// <summary>
        /// Checks whether this instance uses the specified function as part of one of its subexpression or itself.
        /// </summary>
        /// <returns>true</returns>
        /// <c>false</c>
        /// <param name="f">The function to test.</param>
        public override bool UsesFunction(Function f)
        {
            return false;
        }

        /// <summary>
        /// Generates the source code corresponding to the expression.
        /// </summary>
        /// <returns>The code.</returns>
        public override string MakeCode()
        {
            return Expression;
        }

        /// <summary>
        /// Generates a user-readable form of the expression.
        /// </summary>
        /// <returns>The user readable.</returns>
        public override string MakeUserReadable()
        {
            return Expression;
        }

        /// <summary>
        /// Gets all the literals used to construct this expression.
        /// </summary>
        /// <returns>The literals.</returns>
        public override List<LiteralExpression> GetLiterals()
        {
            return new List<LiteralExpression> { this };
        }

        /// <summary>
        /// Gets the default delay literal for the specified language. It is expected to mean 1 second.
        /// </summary>
        /// <returns>The default delay literal for the language.</returns>
        /// <param name="lang">the programming language.</param>
        public static LiteralExpression DefaultDelayLiteralForLanguage(Language lang)
        {
            switch(lang) {
            case Language.C:
            case Language.EmbeddedC:
                return (LiteralExpression)LiteralExpression.CreateFromString("1.0", lang);
            case Language.Cpp:
                return (LiteralExpression)LiteralExpression.CreateFromString("1s", lang);
            case Language.CSharp:
                return (LiteralExpression)LiteralExpression.CreateFromString("1.0", lang);
            case Language.Python:
                return (LiteralExpression)LiteralExpression.CreateFromString("1.0", lang);
            }

            throw new Exception("LitteralExpression.DefaultLiteralForLanguage: Should not get there!");
        }

    }

    /// <summary>
    /// An expression made from a variable literal
    /// </summary>
    public class VariableExpression : LiteralExpression, IComparable<VariableExpression>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Code.VariableExpression"/> class.
        /// </summary>
        /// <param name="expr">Expr.</param>
        /// <param name="language">Language.</param>
        public VariableExpression(string expr, Language language) : base(language, expr)
        {
            if(Expression.StartsWithInv("$")) {
                Expression = Expression.Substring(1);
            }
            Regex name = new Regex(Parser.GetNamePattern(true));
            Match nameMatch = name.Match(Expression);
            if(!nameMatch.Success) {
                throw new Exception(Application.Configuration.GetLocalized("Invalid variable name specified!"));
            }
        }

        /// <summary>
        /// Generates the source code corresponding to the expression.
        /// </summary>
        /// <returns>The code.</returns>
        public override string MakeCode()
        {
            if(Language == Language.C) {
                return MakeCCode();
            } else if(Language == Language.EmbeddedC) {
                return "_PETRI_PRIVATE_VARIABLES_[" + EnumeratorName + "]";
            } else if(Language == Language.Cpp) {
                return "_PETRI_PRIVATE_GET_VARIABLES_[" + EnumeratorName + "].value()";
            } else if(Language == Language.CSharp) {
                // The cast is mandatory here. Indeed, when we try to evaluate an expression in the debugger,
                // as an Enum is not marshallable, we run into issues. If we cast first, they are avoided.
                return "_PETRI_PRIVATE_GET_VARIABLES_[(UInt32)" + EnumeratorName + "].Value";
            } else if(Language == Language.Python) {
                return "_PETRI_PRIVATE_GET_VARIABLES_[" + EnumeratorName + "].value";
            }

            throw new Exception("VariableExpression.MakeCode: Should not get there!");
        }

        /// <summary>
        /// Returns the C code.
        /// </summary>
        /// <returns>The C code.</returns>
        public string MakeCCode()
        {
            string enumName = EnumeratorName;
            if(Language == Language.Python) {
                enumName = DefaultVarEnumName + "_V" + Expression;
            }
            return "(*PetriVarSlot_getVariable(_PETRI_PRIVATE_GET_VARIABLES_, (uint_fast32_t)(" + enumName + ")))";
        }

        /// <summary>
        /// Generates a user-readable form of the expression.
        /// </summary>
        /// <returns>The user readable.</returns>
        public override string MakeUserReadable()
        {
            return "$" + Expression;
        }

        /// <summary>
        /// Gets the enumerator's name that is used during code generation.
        /// </summary>
        /// <value>The enumarator's name.</value>
        /// <param name="enumName">The name of the enumeration.</param>
        public string GetEnumeratorName(string enumName)
        {
            if(Language == Language.C || Language == Language.EmbeddedC) {
                return enumName + "_V" + Expression;
            } else if(Language == Language.Cpp) {
                return enumName + "_V" + Expression;
            } else if(Language == Language.CSharp) {
                return enumName + ".V" + Expression;
            } else if(Language == Language.Python) {
                return enumName + "['V" + Expression + "']";
            }

            throw new Exception("VariableExpression.GetEnumeratorName: Should not get there!");
        }
        /// <summary>
        /// Gets the enumerator's name that is used during code generation.
        /// </summary>
        /// <value>The enumarator's name.</value>
        public string EnumeratorName {
            get {
                return GetEnumeratorName(DefaultVarEnumName);
            }
        }

        /// <summary>
        /// Returns the sort order of the current instance compared to the specified object.
        /// </summary>
        /// <returns>The comparison result.</returns>
        /// <param name="o">The object 'this' is compared against.</param>
        public int CompareTo(VariableExpression o)
        {
            return string.Compare(MakeUserReadable(), o.MakeUserReadable(), StringComparison.InvariantCulture);
        }

        /// <summary>
        /// Serves as a hash function for a <see cref="T:Petri.Code.VariableExpression"/> object.
        /// </summary>
        /// <returns>A hash code for this instance that is suitable for use in hashing algorithms and data structures such as a
        /// hash table.</returns>
        public override int GetHashCode()
        {
            return MakeUserReadable().GetHashCode();
        }

        /// <summary>
        /// Determines whether the specified <see cref="object"/> is equal to the current <see cref="T:Petri.Code.VariableExpression"/>.
        /// </summary>
        /// <param name="obj">The <see cref="object"/> to compare with the current <see cref="T:Petri.Code.VariableExpression"/>.</param>
        /// <returns><c>true</c> if the specified <see cref="object"/> is equal to the current
        /// <see cref="T:Petri.Code.VariableExpression"/>; otherwise, <c>false</c>.</returns>
        public override bool Equals(object obj)
        {
            var vv = obj as VariableExpression;
            if(vv == null) {
                return false;
            }

            return MakeUserReadable().Equals(vv.MakeUserReadable());
        }

        /// <summary>
        /// Determines whether a specified instance of <see cref="Petri.Code.VariableExpression"/> is equal to another
        /// specified <see cref="Petri.Code.VariableExpression"/>.
        /// </summary>
        /// <param name="a">The first <see cref="Petri.Code.VariableExpression"/> to compare.</param>
        /// <param name="b">The second <see cref="Petri.Code.VariableExpression"/> to compare.</param>
        /// <returns><c>true</c> if <c>a</c> and <c>b</c> are equal; otherwise, <c>false</c>.</returns>
        public static bool operator ==(VariableExpression a, VariableExpression b)
        {
            // If both are null, or both are same instance, return true.
            if(ReferenceEquals(a, b)) {
                return true;
            }

            // If one is null, but not both, return false.
            if(((object)a == null) || ((object)b == null)) {
                return false;
            }

            return a.Equals(b);
        }

        /// <summary>
        /// Determines whether a specified instance of <see cref="Petri.Code.VariableExpression"/> is not equal to
        /// another specified <see cref="Petri.Code.VariableExpression"/>.
        /// </summary>
        /// <param name="a">The first <see cref="Petri.Code.VariableExpression"/> to compare.</param>
        /// <param name="b">The second <see cref="Petri.Code.VariableExpression"/> to compare.</param>
        /// <returns><c>true</c> if <c>a</c> and <c>b</c> are not equal; otherwise, <c>false</c>.</returns>
        public static bool operator !=(VariableExpression a, VariableExpression b)
        {
            return !(a == b);
        }

        /// <summary>
        /// Gets the name of the variables enum that is used for code generation.
        /// </summary>
        /// <value>The name of the enum.</value>
        public static string DefaultVarEnumName {
            get {
                return "Petri_Var_Enum";
            }
        }

        /// <summary>
        /// Gets the name of the parameters enum that is used for code generation.
        /// </summary>
        /// <value>The name of the enum.</value>
        public static string DefaultParamEnumName {
            get {
                return "Petri_Param_Enum";
            }
        }
    }

    /// <summary>
    /// Ternary expression.
    /// </summary>
    public abstract class TernaryExpression : Expression
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Code.TernaryExpression"/> class.
        /// </summary>
        /// <param name="language">Language.</param>
        /// <param name="expr1">The first operand.</param>
        /// <param name="expr2">The second operand.</param>
        /// <param name="expr3">The third operand.</param>
        protected TernaryExpression(Language language,
                                    Expression expr1,
                                    Expression expr2,
                                    Expression expr3) : base(language,
                                                             Code.Operator.Name.TernaryConditional)
        {
            Expression1 = expr1;
            Expression2 = expr2;
            Expression3 = expr3;
        }

        /// <summary>
        /// The first operand.
        /// </summary>
        /// <value>The expression1.</value>
        public Expression Expression1 {
            get; private set;
        }

        /// <summary>
        /// The second operand.
        /// </summary>
        /// <value>The expression2.</value>
        public Expression Expression2 {
            get; private set;
        }

        /// <summary>
        /// The third operand.
        /// </summary>
        /// <value>The expression3.</value>
        public Expression Expression3 {
            get; private set;
        }

        /// <summary>
        /// Checks whether this instance uses the specified function as part of one of its subexpression or itself.
        /// </summary>
        /// <returns>true</returns>
        /// <c>false</c>
        /// <param name="f">The function to test.</param>
        public override bool UsesFunction(Function f)
        {
            return Expression1.UsesFunction(f) || Expression2.UsesFunction(f) || Expression3.UsesFunction(f);
        }

        /// <summary>
        /// Generates the source code corresponding to the expression.
        /// </summary>
        /// <returns>The code.</returns>
        public override string MakeCode()
        {
            return Parenthesize(this, Expression1, Expression1.MakeCode()) + " ? " + Parenthesize(this,
                                                                                                  Expression1,
                                                                                                  Expression2.MakeCode()) + " : " + Parenthesize(this,
                                                                                                                                                 Expression3,
                                                                                                                                                 Expression3.MakeCode());
        }

        /// <summary>
        /// Generates a user-readable form of the expression.
        /// </summary>
        /// <returns>The user readable.</returns>
        public override string MakeUserReadable()
        {
            return Parenthesize(this, Expression1, Expression1.MakeUserReadable()) + " ? " + Parenthesize(this,
                                                                                                          Expression1,
                                                                                                          Expression2.MakeUserReadable()) + " : " + Parenthesize(this,
                                                                                                                                                                 Expression3,
                                                                                                                                                                 Expression3.MakeUserReadable());
        }

        /// <summary>
        /// Gets all the literals used to construct this expression.
        /// </summary>
        /// <returns>The literals.</returns>
        public override List<LiteralExpression> GetLiterals()
        {
            var l1 = Expression1.GetLiterals();
            var l2 = Expression2.GetLiterals();
            var l3 = Expression3.GetLiterals();
            l1.AddRange(l2);
            l1.AddRange(l3);
            return l1;
        }

    }

    /// <summary>
    /// An expression list. For example, 'a = 3; ++b' is a list of 2 expressions.
    /// </summary>
    public class ExpressionList : Expression
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Code.ExpressionList"/> class.
        /// </summary>
        /// <param name="language">The programming language.</param>
        /// <param name="expressions">The expressions that make the list.</param>
        public ExpressionList(Language language, IEnumerable<Expression> expressions) : base(language,
                                                                                             Code.Operator.Name.None)
        {
            Expressions = new List<Expression>(expressions);
        }

        /// <summary>
        /// Gets the expressions of the list.
        /// </summary>
        /// <value>The expressions.</value>
        public List<Expression> Expressions {
            get; private set;
        }

        /// <summary>
        /// Checks whether this instance uses the specified function as part of one of its subexpression or itself.
        /// </summary>
        /// <returns>true</returns>
        /// <c>false</c>
        /// <param name="f">The function to test.</param>
        public override bool UsesFunction(Function f)
        {
            foreach(var e in Expressions) {
                if(e.UsesFunction(f)) {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Generates the source code corresponding to the expression.
        /// </summary>
        /// <returns>The code.</returns>
        public override string MakeCode()
        {
            string sep = ";\n";
            if(Language == Language.C || Language == Language.EmbeddedC) {
                sep = ",\n";
            } else if(Language == Language.Python) {
                sep = "; ";
            }
            return String.Join(sep,
                               from e in Expressions
                               select e.MakeCode());
        }

        /// <summary>
        /// Generates a user-readable form of the expression.
        /// </summary>
        /// <returns>The user readable.</returns>
        public override string MakeUserReadable()
        {
            return String.Join("; ",
                               from e in Expressions
                               select e.MakeUserReadable());
        }

        /// <summary>
        /// Gets all the literals used to construct this expression.
        /// </summary>
        /// <returns>The literals.</returns>
        public override List<LiteralExpression> GetLiterals()
        {
            var l = new List<LiteralExpression>();
            foreach(var e in Expressions) {
                l.AddRange(e.GetLiterals());
            }

            return l;
        }

    }
}
