//
//  Language.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2014-12-09.
//

namespace Petri.Code
{
    /// <summary>
    /// Represents a programing language.
    /// </summary>
    public enum Language
    {
        /// <summary>
        /// The C programming language.
        /// </summary>
        C,

        /// <summary>
        /// The C++ programming language.
        /// </summary>
        Cpp,

        /// <summary>
        /// The C# programming language.
        /// </summary>
        CSharp,

        /// <summary>
        /// The C programming language, for embedded platforms (simplified runtime).
        /// </summary>
        EmbeddedC,

        /// <summary>
        /// The Python programming language.
        /// </summary>
        Python,
    }
}
