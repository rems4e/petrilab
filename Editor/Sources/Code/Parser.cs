//
//  Parser.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2014-11-13.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Petri.Code
{
    /// <summary>
    /// A parser class. It is used to extract function declarations from headers and source code files.
    /// </summary>
    public static class Parser
    {
        /// <summary>
        /// The visibility of a method.
        /// </summary>
        enum Visibility
        {
            /// <summary>
            /// The public visibility.
            /// </summary>
            Public,

            /// <summary>
            /// The protected visibility.
            /// </summary>
            Protected,

            /// <summary>
            /// The private visibility.
            /// </summary>
            Private
        }

        /// <summary>
        /// A pattern that matches code identifiers.
        /// </summary>
        /// <returns>The name pattern.</returns>
        /// <param name="strict">If set to <c>true</c>, then the pattern will be used to match the whole string. Otherwise, it may only match a part of the string.</param>
        public static string GetNamePattern(bool strict)
        {
            var pattern = "(?<name>[a-zA-Z_][a-zA-Z0-9_]*)";
            if(strict) {
                pattern = "^" + pattern + "$";
            }
            return pattern;
        }

        /// <summary>
        /// A patter that matches either a code identifier or a petri net variable starting with '$'.
        /// </summary>
        /// <value>The variable pattern.</value>
        public static string VariablePattern {
            get {
                return "(?<name>\\$?[a-zA-Z_][a-zA-Z0-9_]*)";
            }
        }

        /// <summary>
        /// Gets the pattern that matches exponent part in a floating-point number literal.
        /// </summary>
        /// <value>The exponent pattern.</value>
        public static string ExponentPattern {
            get {
                return "((e|E)(\\+|-)?[0-9]+)";
            }
        }

        /// <summary>
        /// Gets the pattern that matche a number, integer or floating point, in decimal, octal or hexadecimal.
        /// </summary>
        /// <value>The number pattern.</value>
        public static string NumberPattern {
            get {
                return string.Format(@"(?<number>((\.[0-9]+|([0-9]+(\.([0-9]+)?)?){0}?))|(0[xX][0-9a-fA-F]+))",
                                     ExponentPattern);
            }
        }

        /// <summary>
        /// Matches C++ template specialization of a type (which may contain (), &lt;&gt;, :, letters, numbers, white spaces…
        /// Probably not complete due to horrible syntax disambiguation needed…
        /// </summary>
        /// <value>The template pattern.</value>
        public static string TemplatePattern {
            get {
                return @"(?<template>([ ]*<([^>]*>[ ]*([> ]|::[ ]*[a-zA-Z0-9_()]*)*))?)";
            }
        }

        /// <summary>
        /// Gets the class declaration pattern.
        /// </summary>
        /// <value>The class declaration.</value>
        public static string ClassDeclarationPattern {
            get {
                return "^(class|struct) " + GetNamePattern(false);
            }
        }

        /// <summary>
        /// Gets the namespace declaration pattern.
        /// </summary>
        /// <value>The namespace declaration pattern.</value>
        public static string NamespaceDeclarationPattern {
            get {
                return "^(namespace) " + GetNamePattern(false);
            }
        }

        /// <summary>
        /// Gets the single line comment pattern.
        /// </summary>
        /// <returns>The single line comment pattern.</returns>
        /// <param name="language">Language.</param>
        public static string GetSingleLineCommentPattern(Language language)
        {
            return language == Language.Python ? "#" : "//";
        }

        /// <summary>
        /// Gets the header section delimiter for the specified delimiter pattern.
        /// </summary>
        /// <returns>The header section regex for delimiter.</returns>
        /// <param name="language">The language we are working with.</param>
        /// <param name="pattern">The pattern that is part of a delimiter.</param>
        static string GetHeaderSectionPattern(Language language, string pattern)
        {
            return string.Format(
                @"^\s*{1}\s*@{0}@\s*(?<section>.*)\s*",
                pattern,
                GetSingleLineCommentPattern(language)
            );
        }

        /// <summary>
        /// Extracts a list of function declarations matched by their header section from the header at the given filename.
        /// </summary>
        /// <param name="language">The programming language.</param>
        /// <param name="filename">The name of the file to dig into.</param>
        /// <param name="delimiterPattern">The pattern that is part of a section delimiter.</param>
        public static Dictionary<string, List<Function>> Parse(Language language,
                                                               string filename,
                                                               string delimiterPattern)
        {
            var sectionRegex = new Regex(GetHeaderSectionPattern(language, delimiterPattern));

            string currentSection = "";
            var functions = new Dictionary<string, List<Function>>();

            System.IO.StreamReader file = System.IO.File.OpenText(filename);
            string s = file.ReadToEnd();
            s = Preprocess(s, language, sectionRegex);

            string[] lines = s.Split('\n');

            // The tuple contains the current Scope as its first element, current scope's braces nesting level and visibility (only used into classes and structs)
            var currentScope = new Stack<Tuple<Scope, int, Visibility>>();
            if(language == Language.Python) {
                currentScope.Push(new Tuple<Scope, int, Visibility>(
                    Scope.MakeFromNamespace(language, System.IO.Path.GetFileNameWithoutExtension(filename)),
                    0,
                    Visibility.Public
                ));
            } else {
                currentScope.Push(new Tuple<Scope, int, Visibility>(null, 0, Visibility.Public));
            }

            int depth = 0;
            var lastDepths = new Stack<int>();
            lastDepths.Push(0);

            var classRegex = new Regex(ClassDeclarationPattern);
            var namespaceRegex = new Regex(NamespaceDeclarationPattern);
            var functionRegex = Function.Regex;

            for(int li = 0; li != lines.Length; ++li) {
                var l = lines[li];
                bool incScope = false;
                int decScope = 0;
                if(language == Language.Python) {
                    var lastDepth = lastDepths.Peek();
                    var tabCount = l.LastIndexOf('\t') + 1;
                    var tabless = l.TrimStart(new char[] { '\t' });
                    if(tabless != "") {
                        if(tabCount > lastDepth) {
                            incScope = true;
                            lastDepths.Push(tabCount);
                        } else {
                            while(tabCount < lastDepth) {
                                decScope += 1;
                                lastDepths.Pop();
                                lastDepth = lastDepths.Peek();
                            }
                        }
                        l = tabless;
                    }
                } else {
                    incScope = l == "{";
                    decScope = l == "}" ? 1 : 0;
                }
                if(incScope) {
                    ++depth;
                } else {
                    while(decScope-- > 0) {
                        if(currentScope.Count > 1 && currentScope.Peek().Item2 == depth) {
                            currentScope.Pop();
                        }
                        --depth;
                    }
                }

                var sectionMatch = sectionRegex.Match(l);
                if(sectionMatch.Success) {
                    currentSection = sectionMatch.Groups["section"].Value;
                }

                var match = classRegex.Match(l);
                if(!l.EndsWithInv(";") && match.Success) {
                    Visibility vis = Visibility.Public;
                    if(l.StartsWithInv("class") && language != Language.Python) {
                        vis = Visibility.Private;
                    }
                    currentScope.Push(Tuple.Create(Scope.MakeFromClass(new Type(language,
                                                                                match.Groups["name"].Value,
                                                                                currentScope.Peek().Item1)),
                                                   depth + 1,
                                                   vis));
                }

                match = namespaceRegex.Match(l);
                if(match.Success) {
                    currentScope.Push(Tuple.Create(Scope.MakeFromScopes(currentScope.Peek().Item1,
                                                                        Scope.MakeFromNamespace(language,
                                                                                                match.Groups["name"].Value,
                                                                                                currentScope.Peek().Item1)),
                                                   depth + 1,
                                                   Visibility.Public));
                }

                // If we are in a class, change the visibility of its members according to their visibility specifiers
                if(currentScope.Count > 1) {
                    Visibility vis = currentScope.Peek().Item3;
                    if(Regex.IsMatch(l, @"public\s*:")) {
                        vis = Visibility.Public;
                    } else if(Regex.IsMatch(l, @"protected\s*:")) {
                        vis = Visibility.Protected;
                    } else if(Regex.IsMatch(l, @"private\s*:")) {
                        vis = Visibility.Private;
                    }
                    if(vis != currentScope.Peek().Item3) {
                        var tup = currentScope.Pop();
                        tup = Tuple.Create(tup.Item1, tup.Item2, vis);
                        currentScope.Push(tup);
                    }
                }

                // Make sure that we don't go inside a function or method body
                if(depth == currentScope.Peek().Item2) {
                    Match fmatch = functionRegex.Match(l);
                    if(fmatch.Success) {
                        Function function = null;
                        if(currentScope.Count == 1 || currentScope.Peek().Item1.IsNamespace || (fmatch.Groups["static"].Success && (currentScope.Peek().Item3 == Visibility.Public || fmatch.Groups["visibility"].Value == "public"))) {
                            function = ParseFunction(language,
                                                     fmatch,
                                                     currentScope.Peek().Item1,
                                                     filename);
                        } else if(currentScope.Peek().Item3 == Visibility.Public) {
                            function = ParseMethod(language,
                                                   fmatch,
                                                   currentScope.Peek().Item1,
                                                   filename);
                        }

                        if(function != null) {
                            List<Function> sectionFunctions;
                            functions.TryGetValue(currentSection, out sectionFunctions);
                            if(sectionFunctions == null) {
                                sectionFunctions = new List<Function>();
                            }
                            sectionFunctions.Add(function);
                            functions[currentSection] = sectionFunctions;
                        }
                    }
                }
            }

            return functions;
        }

        /// <summary>
        /// Splits the given string in parts that are delimited by the given separator. It tries to keep the nesting levels according to a balanced parentheses language.
        /// </summary>
        /// <returns>The list of parts after the split.</returns>
        /// <param name="s">The string to split.</param>
        /// <param name="separator">The delimiter of the parts.</param>
        public static List<string> SyntacticSplit(string s, string separator)
        {
            var result = new List<string>();
            int paren = 0;
            bool quote = false, apostrophe = false, escaped = false;
            int lastIndex = 0;
            for(int i = 0; i < s.Length; ++i) {
                switch(s[i]) {
                case '(':
                case '{':
                case '[':
                case '<':
                    escaped = false;
                    if(!quote && !apostrophe) {
                        ++paren;
                    }
                    break;
                case ')':
                case '}':
                case ']':
                case '>':
                    escaped = false;
                    if(!quote && !apostrophe) {
                        --paren;
                    }
                    break;
                case '\'':
                    if(!quote && !escaped) {
                        apostrophe = !apostrophe;
                    }
                    escaped = false;
                    break;
                case '"':
                    if(!apostrophe && !escaped) {
                        quote = !quote;
                    }
                    escaped = false;
                    break;
                case '\\':
                    escaped = !escaped;
                    break;
                default:
                    escaped = false;
                    if(s[i] == separator[0]
                           && (i + separator.Length <= s.Length)
                           && (s.Substring(i, separator.Length) == separator) && !quote && !apostrophe && paren == 0) {
                        result.Add(s.Substring(lastIndex, i - lastIndex));
                        lastIndex = i + separator.Length;
                    }
                    break;
                }
            }

            if(lastIndex < s.Length) {
                result.Add(s.Substring(lastIndex, s.Length - lastIndex));
            }

            return result;
        }

        /// <summary>
        /// Removes the parenthesis around a string, if any.
        /// </summary>
        /// <returns>The string without its outer parentheses, if any.</returns>
        /// <param name="s">The string to operate on.</param>
        public static string RemoveParenthesis(string s)
        {
            while(true) {
                s = s.Trim();
                if(!s.StartsWithInv("(") || !s.EndsWithInv(")")) {
                    break;
                }

                int parent = 1;
                for(int i = 1; i < s.Length - 1; ++i) {
                    if(s[i] == '(') {
                        ++parent;
                    } else if(s[i] == ')') {
                        --parent;
                        if(parent == 0) {
                            // Did not found parentheses strictly around the string
                            return s;
                        }
                    }
                }

                s = s.Substring(1, s.Length - 2);
            }

            return s;
        }

        /// <summary>
        /// Separates the scope and name of a qualified name of an object.
        /// </summary>
        /// <returns>The scope and name of the object.</returns>
        /// <param name="language">Language.</param>
        /// <param name="name">The string to operate on.</param>
        public static Tuple<Scope, string> ExtractScope(Language language, string name)
        {
            int index = name.LastIndexOfInv(Scope.GetSeparator(language));
            Tuple<Scope, string> tup;
            if(index == -1) {
                return new Tuple<Scope, string>(null, name);
            } else {
                tup = ExtractScope(language, name.Substring(0, index));
            }

            return Tuple.Create(
                Scope.MakeFromNamespace(language, tup.Item2, tup.Item1),
                name.Substring(index + Scope.GetSeparator(language).Length)
            );
        }

        /// <summary>
        /// Extracts a function name, return type and arguments from a regex match.
        /// </summary>
        /// <returns>The new function.</returns>
        /// <param name="language">The programming language.</param>
        /// <param name="match">Match.</param>
        /// <param name="enclosing">The enclosing scope of the function.</param>
        /// <param name="filename">The header from where comes the function declaration.</param>
        static Function ParseFunction(Language language,
                                      Match match,
                                      Scope enclosing,
                                      string filename)
        {
            var param = new Regex(@" ?" + Type.RegexPattern + @" ?(" + GetNamePattern(false) + ")?");
            string[] parameters = match.Groups["parameters"].Value.Split(',');

            var returnType = language == Language.Python
                                                 ? Type.UnknownType(Language.Python)
                                                 : new Type(language, match.Groups["type"].Value);
            Function function = new Function(returnType,
                                             enclosing,
                                             match.Groups["name"].Value,
                                             match.Groups["template"].Value != "");

            foreach(string parameter in parameters) {
                Match paramMatch = param.Match(parameter);
                if(paramMatch.Success) {
                    var p = new Param(new Type(language, paramMatch.Groups["type"].Value),
                                      paramMatch.Groups["name"].Value);
                    function.AddParam(p);
                }
            }

            function.Header = filename;

            return function;
        }

        /// <summary>
        /// Extracts a method name, return type and arguments from a regex match.
        /// If the current language is Python, we use the first argument's presence and name (must be self) to check
        /// for a method. If not, this is a static method.
        /// </summary>
        /// <returns>The new method.</returns>
        /// <param name="language">The programming language.</param>
        /// <param name="match">Match.</param>
        /// <param name="enclosing">The enclosing scope of the method, that may be a type.</param>
        /// <param name="filename">The header from where comes the function declaration.</param>
        static Function ParseMethod(Language language,
                                    Match match,
                                    Scope enclosing,
                                    string filename)
        {
            if(language == Language.Python) {
                return ParseFunction(language, match, enclosing, filename);
            }

            var param = new Regex(@" ?" + Type.RegexPattern + @" ?(" + GetNamePattern(false) + ")?");
            string[] parameters = match.Groups["parameters"].Value.Split(',');

            var returnType = language == Language.Python
                                                 ? Type.UnknownType(Language.Python)
                                                 : new Type(language, match.Groups["type"].Value);
            var method = new Method(
                enclosing.Class,
                returnType,
                match.Groups["name"].Value,
                match.Groups["start_qualifiers"].Value,
                match.Groups["end_qualifiers"].Value,
                match.Groups["template"].Value != ""
            );

            foreach(string parameter in parameters) {
                var paramMatch = param.Match(parameter);
                if(paramMatch.Success) {
                    var p = new Param(new Type(language, paramMatch.Groups["type"].Value),
                                      paramMatch.Groups["name"].Value);
                    method.AddParam(p);
                }
            }

            method.Header = filename;

            return method;
        }

        /// <summary>
        /// Preprocesses the specified header content so that it is easier to process by this crippled parser.
        /// In effect, turns tabs into spaces, removes multiline comments, puts braces on their own line, put newline after semicolons at line end…
        /// </summary>
        /// <param name="s">The source code.</param>
        /// <param name="language">The programming language we are working with.</param>
        /// <param name="sectionRegex">The pattern that is part of a section delimiter.</param>
        static string Preprocess(string s, Language language, Regex sectionRegex)
        {
            var commentPattern = GetSingleLineCommentPattern(language);
            s = s.Replace('\t', ' ');

            {
                var lines = s.Split('\n');

                for(int i = 0; i < lines.Length; ++i) {
                    // Removing blank lines
                    if(lines[i].Trim() == "") {
                        lines[i] = "";
                    }
                    if(language != Language.Python) {
                        // Removing preprocessor directives
                        if(lines[i].StartsWithInv("#")) {
                            lines[i] = "";
                        }
                    } else {
                        var match = Regex.Match(lines[i], @"^( +)(.*)");
                        if(match.Success) {
                            var replacement = new string('\t', match.Groups[1].Length) + "$2";
                            lines[i] = match.Result(replacement);
                        }
                    }
                }

                s = String.Join("\n", lines);
            }

            // Removing multi-lines comments
            string s2 = "";
            char lastStringChar = '\0';
            bool inString = false;
            bool inComment = false;
            for(int i = 0; i < s.Length; ++i) {
                if((s[i] == '"' || s[i] == '\'') && (!inString || s[i] == lastStringChar) && !inComment) {
                    inString = !inString;
                    lastStringChar = s[i];
                } else if(!inString) {
                    // Remove single line comments that don't create a new section
                    if(!inComment && i <= s.Length - commentPattern.Length && s.Substring(i, commentPattern.Length) == commentPattern) {
                        if(!sectionRegex.IsMatch(s.Substring(i))) {
                            while(i < s.Length - 1 && s[++i] != '\n') { }
                        }

                    } else {
                        if(language != Language.Python) {
                            if(!inComment && s[i] == '/' && i < s.Length - 1 && s[i + 1] == '*') {
                                inComment = true;
                                continue;
                            }

                            if(inComment && s[i] == '*' && i < s.Length - 1 && s[i + 1] == '/') {
                                ++i;
                                inComment = false;
                                continue;
                            }
                        }
                    }
                }


                if(!inComment) {
                    char lastChar = s2.Length > 0 ? s2[s2.Length - 1] : '\n';
                    if(language != Language.Python && !inString && (s[i] == '{' || s[i] == '}')) {
                        // Here we want to have braces on their own line
                        if((i > 0 && lastChar != '\n')) {
                            s2 += '\n';
                        }
                        s2 += s[i];
                        if((i < s.Length - 1 && s[i + 1] != '\n')) {
                            s2 += '\n';
                        }
                    } else if(inString || i >= s.Length - 1) {
                        // We are in a string or there is no character left
                        s2 += s[i];
                        lastChar = s[i];
                    } else {
                        // We act according to the characters (i, i + 1)
                        var substr = s.Substring(i, 2);
                        if(substr == "\n\n" || substr == " \n" || substr == "  " || substr == " :" || substr == " {" || substr == " }") {
                            // Ignore first char
                        } else if(substr == "\n " || substr == ": " || substr == "{ " || substr == "} ") {
                            // Here we ignore the second char while the pair it forms with the first char is undesirable
                            int j = i + 1;
                            while(j < s.Length) {
                                substr = new string(new char[] { s[i], s[j] });
                                if(substr == "\n " || substr == ": " || substr == "{ " || substr == "} ") {
                                    ++j;
                                } else {
                                    s2 += s[i];
                                    lastChar = s[i];
                                    break;
                                }
                            }
                            i = j - 1;
                        } else if(s[i] == ';' && s[i + 1] != '\n') {
                            // End all semicolon with a newline
                            s2 += ";\n";
                            lastChar = '\n';
                        } else {
                            s2 += s[i];
                            lastChar = s[i];
                        }
                    }
                }
            }
            s = s2;

            return s;
        }
    }
}

