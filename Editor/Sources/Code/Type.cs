//
//  Type.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-24.
//

using System;
using System.Collections.Generic;

namespace Petri.Code
{
    /// <summary>
    /// const and volatile qualifiers for a type part.
    /// </summary>
    [Flags]
    public enum CVQualifier
    {
        /// <summary>
        /// No CV-qualifier.
        /// </summary>
        None = 0,

        /// <summary>
        /// The const qualifier.
        /// </summary>
        Const = 1,

        /// <summary>
        /// The volatile qualifier.
        /// </summary>
        Volatile = 2
    }

    /// <summary>
    /// Kind of reference for a type (C++).
    /// </summary>
    public enum ReferenceKind
    {
        /// <summary>
        /// No reference.
        /// </summary>
        None,

        /// <summary>
        /// lvalue reference.
        /// </summary>
        Ref,

        /// <summary>
        /// rvalue reference.
        /// </summary>
        RValueRef,
    }

    /// <summary>
    /// A class representing a type in a C-like programming language.
    /// </summary>
    public class Type : IEquatable<Type>, IEquatable<string>
    {
        /// <summary>
        /// A placeholder for types that are not knwown.
        /// </summary>
        /// <returns>The type.</returns>
        /// <param name="language">The programming language.</param>
        public static Type UnknownType(Language language)
        {
            Type type;
            if(!_unknownTypes.TryGetValue(language, out type)) {
                type = new Type(language, "UnknownType", Scope.MakeFromNamespace(language, "Petri"));
                _unknownTypes.Add(language, type);
            }
            return type;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Code.Type"/> class.
        /// </summary>
        /// <param name="language">The programming language.</param>
        /// <param name="name">The name of the type.</param>
        /// <param name="enclosing">The enclosing scope of the type.</param>
        public Type(Language language, string name, Scope enclosing = null)
        {
            Language = language;

            String s = name.Clone() as string;
            s.Replace("*", " * ");
            s = s.TrimSpaces();

            s = s.TrimStart(new char[] { ' ' }).TrimEnd(new char[] { ' ' });

            if(s.EndsWithInv("&&")) {
                HasReference = ReferenceKind.RValueRef;
                s = s.Substring(0, s.Length - 2);
            } else if(s.EndsWithInv("&")) {
                HasReference = ReferenceKind.Ref;
                s = s.Substring(0, s.Length - 1);
            } else {
                HasReference = ReferenceKind.None;
            }

            CVQualifier nameQualifier = CVQualifier.None;

            // Parse right-associative cv-qualifiers, as in const int
            if(s.StartsWithInv("const volatile ") || s.StartsWithInv("volatile const ")) {
                s = s.Substring("const volatile ".Length);
                nameQualifier = CVQualifier.Const | CVQualifier.Volatile;
            } else if(s.StartsWithInv("const ")) {
                s = s.Substring("const ".Length);
                nameQualifier = CVQualifier.Const;
            } else if(s.StartsWithInv("volatile ")) {
                s = s.Substring("volatile ".Length);
                nameQualifier = CVQualifier.Volatile;
            }

            var q = new List<CVQualifier>();

            s = ParseLeftAssociativeQualifiers(s, q).TrimEnd(new char[] { ' ' });

            if(s.EndsWithInv(" const volatile") || s.EndsWithInv(" volatile const")) {
                s = s.Substring(0, s.Length - " const volatile".Length);
                nameQualifier = CVQualifier.Const | CVQualifier.Volatile;
            } else if(s.EndsWithInv(" const")) {
                s = s.Substring(0, s.Length - " const".Length);
                nameQualifier |= CVQualifier.Const;
            } else if(s.EndsWithInv(" volatile")) {
                s = s.Substring(0, s.Length - " volatile".Length);
                nameQualifier |= CVQualifier.Volatile;
            }

            q.Insert(0, nameQualifier);

            int index = s.IndexOf('<');
            if(index != -1) {
                /*var t = Parser.SyntacticSplit(s.Substring(index + 1, s.Length - index - 2));
                    foreach(var e in t) {
                        template.Add(Expression.CreateFromString<Expression>(e, notnullplease, new List<Function>()));
                    }*/
                Template = s.Substring(index + 1, s.Length - index - 2);
            } else {
                Template = "";
                index = s.Length;
            }

            var tup = Parser.ExtractScope(language, s.Substring(0, index).Trim());
            Name = tup.Item2;
            Enclosing = Scope.MakeFromScopes(enclosing, tup.Item1);

            _cvQualifiers = q.ToArray();
        }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents the current <see cref="Petri.Code.Type"/>.
        /// </summary>
        /// <returns>A <see cref="System.String"/> that represents the current <see cref="Petri.Code.Type"/>.</returns>
        public override string ToString()
        {
            string val = (Enclosing?.ToString() ?? "") + Name;
            if(Template.Length > 0) {
                /*string v = "";
                    foreach(var e in Template) {
                        v += e.MakeUserReadable();
                    }
                    val += "<" + val + ">";*/
                val += "<" + Template + ">";
            }

            for(int i = 0; i < _cvQualifiers.Length; ++i) {
                if(i != 0) {
                    val += "*";
                }

                if(_cvQualifiers[i].HasFlag(CVQualifier.Volatile)) {
                    val += " volatile";
                }
                if(_cvQualifiers[i].HasFlag(CVQualifier.Const)) {
                    val += " const";
                }
            }

            if(HasReference == ReferenceKind.Ref) {
                val += "&";
            } else if(HasReference == ReferenceKind.RValueRef) {
                val += "&&";
            }

            return val;
        }

        /// <summary>
        /// Gets the name of the type.
        /// </summary>
        /// <value>The name.</value>
        public string Name {
            get;
            private set;
        }

        /// <summary>
        /// Gets the language the type was created with.
        /// </summary>
        /// <value>The language.</value>
        public Language Language {
            get;
            private set;
        }

        /// <summary>
        /// Gets the enclosing scope of the type.
        /// </summary>
        /// <value>The enclosing.</value>
        public Scope Enclosing {
            get;
            private set;
        }

        /// <summary>
        /// Gets the template arguments list of the type.
        /// </summary>
        /// <value>The template.</value>
        public /*List<Expression>*/string Template {
            get;
            private set;
        }

        /// <summary>
        /// Gets the CV qualifiers of the type. They are related to the levels of indirection. e.g. 'int * const' will have two CV qualifiers : <c>None</c> and <c>Const</c>.
        /// </summary>
        /// <value>The CV qualifiers.</value>
        public IEnumerable<CVQualifier> CVQualifiers {
            get {
                return _cvQualifiers;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the type is a reference type or a value type (C++).
        /// </summary>
        /// <value>The kind of reference.</value>
        public ReferenceKind HasReference {
            get;
            set;
        }

        /// <summary>
        /// Determines whether the specified <see cref="Petri.Code.Type"/> is equal to the current <see cref="Petri.Code.Type"/>.
        /// </summary>
        /// <param name="type">The <see cref="Petri.Code.Type"/> to compare with the current <see cref="Petri.Code.Type"/>.</param>
        /// <returns><c>true</c> if the specified <see cref="Petri.Code.Type"/> is equal to the current
        /// <see cref="Petri.Code.Type"/>; otherwise, <c>false</c>.</returns>
        public bool Equals(Type type)
        {
            if(Name != type.Name || Template != type.Template || HasReference != type.HasReference)
                return false;

            if(_cvQualifiers.Length != type._cvQualifiers.Length) {
                return false;
            }

            for(int i = 0; i < _cvQualifiers.Length; ++i) {
                if(_cvQualifiers[i] != type._cvQualifiers[i]) {
                    return false;
                }
            }

            bool enclosingNull = Enclosing == null || type.Enclosing == null;
            if(enclosingNull && Enclosing != type.Enclosing) {
                return false;
            }
            if(!enclosingNull && !Enclosing.Equals(type.Enclosing)) {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.String"/> is equal to the current <see cref="Petri.Code.Type"/>.
        /// The string is used to create a new type, which is the one being compared.
        /// </summary>
        /// <param name="type">The <see cref="System.String"/> to compare with the current <see cref="Petri.Code.Type"/>.</param>
        /// <returns><c>true</c> if the specified <see cref="System.String"/> is equal to the current
        /// <see cref="Petri.Code.Type"/>; otherwise, <c>false</c>.</returns>
        public bool Equals(string type)
        {
            return Equals(new Type(Language, type, Enclosing));
        }

        /// <summary>
        /// Determines whether this instance is convertible to the specified type. Very basic check that does not manage type aliases.
        /// </summary>
        /// <returns><c>true</c> if this instance is convertible to the specified type; otherwise, <c>false</c>.</returns>
        /// <param name="type">Type.</param>
        public bool IsConvertibleTo(Type type)
        {
            if(Name != type.Name || Template != type.Template || _cvQualifiers.Length != type._cvQualifiers.Length) {
                return false;
            }

            int cvCount = _cvQualifiers.Length;

            int lastTest = (type.HasReference == ReferenceKind.None) ? _cvQualifiers.Length - 1 : _cvQualifiers.Length;
            for(int i = 0; i < lastTest; ++i) {
                if(!ConvertibleTo(_cvQualifiers[i], type._cvQualifiers[i])) {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Determines whether this instance is assignable from the specified type.
        /// Actually 
        /// </summary>
        /// <returns><c>true</c> if this instance is assignable from the specified type; otherwise, <c>false</c>.</returns>
        /// <param name="type">Type.</param>
        public bool IsAssignableFrom(Type type)
        {
            return type.IsConvertibleTo(this);
        }

        /// <summary>
        /// Checks whether <paramref name="sourceCV"/> can be implicitely converted to <c>destCV</c>
        /// </summary>
        /// <returns><c>true</c>, if to was convertibled, <c>false</c> otherwise.</returns>
        /// <param name="sourceCV">Source C.</param>
        /// <param name="destCV">Destination C.</param>
        public static bool ConvertibleTo(CVQualifier sourceCV, CVQualifier destCV)
        {
            if(sourceCV.HasFlag(CVQualifier.Const) && destCV.HasFlag(CVQualifier.Const)) {
                return false;
            }
            if(sourceCV.HasFlag(CVQualifier.Volatile) && destCV.HasFlag(CVQualifier.Volatile)) {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Gets the regex pattern for a type declaration.
        /// </summary>
        /// <value>The regex pattern.</value>
        public static string RegexPattern {
            get {
                // Matches const and volatile qualifiers as well as pointer and reference specifiers
                string constVolatilePattern = @"(((const|volatile))*[ ]*[&&?\* ?]*)*";

                // Matches the cv-qualifiers, the potential virtual, the type name, its template specialization, again cv-qualifiers (may be found before or after the type…)
                string typePattern = @"[ ]*(?<type>(" + constVolatilePattern + @"(virtual[ ]+)?[:a-zA-Z_][:a-zA-Z0-9_]*" + Parser.TemplatePattern + constVolatilePattern + @"))";
                return typePattern;
            }
        }

        /// <summary>
        /// Parses the CV-qualifiers that are on the right of their type compenent.
        /// </summary>
        /// <returns>The left associative qualifiers.</returns>
        /// <param name="s">The string representation of the type.</param>
        /// <param name="qualifiers">Qualifiers.</param>
        string ParseLeftAssociativeQualifiers(string s, List<CVQualifier> qualifiers)
        {
            s = s.TrimEnd(new char[] { ' ' });

            if(s.EndsWithInv(" * const volatile") || s.EndsWithInv(" * volatile const")) {
                s = ParseLeftAssociativeQualifiers(s.Substring(0, s.Length - " * const volatile".Length),
                                                   qualifiers);
                qualifiers.Add(CVQualifier.Const | CVQualifier.Volatile);
            } else if(s.EndsWithInv(" * const")) {
                s = ParseLeftAssociativeQualifiers(s.Substring(0, s.Length - " * const".Length),
                                                   qualifiers);
                qualifiers.Add(CVQualifier.Const);
            } else if(s.EndsWithInv(" * volatile")) {
                s = ParseLeftAssociativeQualifiers(s.Substring(0, s.Length - " * volatile".Length),
                                                   qualifiers);
                qualifiers.Add(CVQualifier.Volatile);
            } else if(s.EndsWithInv(" *")) {
                s = ParseLeftAssociativeQualifiers(s.Substring(0, s.Length - " *".Length),
                                                   qualifiers);
                qualifiers.Add(CVQualifier.None);
            }

            return s;
        }

        CVQualifier[] _cvQualifiers;
        //List<Expression> _template;
        static Dictionary<Language, Type> _unknownTypes = new Dictionary<Language, Type>();
    }
}

