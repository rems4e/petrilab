//
//  Expression.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2014-11-13.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Petri.Code
{
    /// <summary>
    /// A code expression.
    /// </summary>
    public abstract class Expression
    {
        /// <summary>
        /// A token type.
        /// </summary>
        public enum Token
        {
            /// <summary>
            /// The parenthesized expression token.
            /// </summary>
            Parenthesis,

            /// <summary>
            /// The function invocation token.
            /// </summary>
            Invocation,

            /// <summary>
            /// The subscript operator token.
            /// </summary>
            Subscript,

            /// <summary>
            /// The template list token.
            /// </summary>
            Template,

            /// <summary>
            /// The quoted expression token.
            /// </summary>
            Quote,

            /// <summary>
            /// The doubly quoted expression token.
            /// </summary>
            DoubleQuote,

            /// <summary>
            /// The bracketed expression token.
            /// </summary>
            Brackets,

            /// <summary>
            /// The number token.
            /// </summary>
            Number,

            /// <summary>
            /// The identifier token.
            /// </summary>
            ID
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Code.Expression"/> class.
        /// </summary>
        /// <param name="language">The programming language of the expression.</param>
        /// <param name="op">The operator of the expression.</param>
        protected Expression(Language language, Operator.Name op)
        {
            Operator = op;
            Language = language;
            Unexpanded = "";
        }

        /// <summary>
        /// Checks whether this instance uses the specified function as part of one of its subexpression or itself.
        /// </summary>
        /// <returns><c>true</c>, if function is used, <c>false</c> otherwise.</returns>
        /// <param name="f">The function to test.</param>
        public abstract bool UsesFunction(Function f);

        /// <summary>
        /// Generates the source code corresponding to the expression.
        /// </summary>
        /// <returns>The code.</returns>
        public abstract string MakeCode();

        /// <summary>
        /// Generates a user-readable form of the expression. It may be similar to the result of <c>MakeCode()</c> result, but in a simpler form.
        /// </summary>
        /// <returns>The user readable.</returns>
        public abstract string MakeUserReadable();

        /// <summary>
        /// Gets all the literals used to construct this expression.
        /// </summary>
        /// <returns>The literals.</returns>
        public abstract List<LiteralExpression> GetLiterals();

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:Petri.Code.Expression"/>.
        /// </summary>
        /// <returns>A <see cref="T:System.String"/> that represents the current <see cref="T:Petri.Code.Expression"/>.</returns>
        public override string ToString()
        {
            return MakeUserReadable();
        }

        /// <summary>
        /// Gets all the literals expressions that are of type <c>VariableExpression</c>.
        /// </summary>
        /// <returns>The variables.</returns>
        public List<VariableExpression> GetVariables()
        {
            var result = new List<VariableExpression>();
            var l = GetLiterals();
            foreach(var ll in l) {
                if(ll is VariableExpression) {
                    result.Add((VariableExpression)ll);
                }
            }

            return result;
        }

        /// <summary>
        /// Creates an expression from a string.
        /// </summary>
        /// <returns>The new expression.</returns>
        /// <param name="s">The representation of the expression to create.</param>
        /// <param name="language">The programming language of the expression.</param>
        /// <param name="functions">A list of known functions. If not null, attempt will be made to match function and method calls found in the expressionto this list.</param>
        /// <param name="macros">The macros. These work like the C preprocessor, and every found key in the string is replaced by its associated value. If null, no macro expansion takes place.</param>
        public static Expression CreateFromString(string s,
                                                  Language language,
                                                  IEnumerable<Function> functions = null,
                                                  Dictionary<string, string> macros = null)
        {
            return CreateFromString<Expression>(s, language, functions, macros);
        }

        /// <summary>
        /// Creates an expression from a string.
        /// </summary>
        /// <returns>The new expression.</returns>
        /// <param name="s">The representation of the expression to create.</param>
        /// <param name="language">The programming language of the expression.</param>
        /// <param name="functions">A list of known functions. If not null, attempt will be made to match function and method calls found in the expressionto this list.</param>
        /// <param name="macros">The macros. These work like the C preprocessor, and every found key in the string is replaced by its associated value. If null, no macro expansion takes place.</param>
        /// <typeparam name="ExpressionType">The type of the expression that is expected. An exception is thrown if the actual expression type is different.</typeparam>
        public static ExpressionType CreateFromString<ExpressionType>(string s,
                                                                      Language language,
                                                                      IEnumerable<Function> functions = null,
                                                                      Dictionary<string, string> macros = null) where ExpressionType : Expression
        {
            string unexpanded = s;
            string expanded = Expand(s, macros);
            s = expanded;

            var tokenized = Expression.Tokenize(s);
            var exprList = tokenized.Item1.Split(new char[] { ';' });
            var parsedList = from e in exprList
                             select Expression.CreateFromTokenizedString(e,
                                                                         language,
                                                                         functions,
                                                                         macros,
                                                                         tokenized.Item2);

            Expression result;
            if(parsedList.Count() > 1) {
                result = new ExpressionList(language, parsedList);
            } else {
                result = parsedList.First();
            }
            if(!(result is ExpressionType)) {
                throw new Exception(Application.Configuration.GetLocalized("Unable to get a valid expression."));
            }
            result.Unexpanded = unexpanded;
            result.NeedsExpansion = !unexpanded.Equals(expanded);

            return (ExpressionType)result;
        }

        /// <summary>
        /// Gets the programming language the expression if written in.
        /// </summary>
        /// <value>The language.</value>
        public Language Language {
            get;
            private set;
        }

        /// <summary>
        /// Gets the operator of the expression. May be <c>Operator.Name.None</c> if this is a literal, or any other operator.
        /// </summary>
        /// <value>The operator.</value>
        public Operator.Name Operator {
            get;
            private set;
        }

        /// <summary>
        /// Gets the string that was used to create this expression before the macros preprocessing took place.
        /// </summary>
        /// <value>The unexpanded.</value>
        public string Unexpanded {
            get;
            private set;
        }

        /// <summary>
        /// Gets a value indicating whether this <see cref="Expression"/> needs macro expansion, i.e. if the string used to create the expression contained one or more macros passed at construction.
        /// </summary>
        /// <value><c>true</c> if needs expansion; otherwise, <c>false</c>.</value>
        public bool NeedsExpansion {
            get;
            private set;
        }

        /// <summary>
        /// Expand the specified expression using the specified macros macros.
        /// </summary>
        /// <param name="expression">Expression.</param>
        /// <param name="macros">Macros.</param>
        static string Expand(string expression, IDictionary<string, string> macros)
        {
            if(macros != null) {
                foreach(var macro in macros) {
                    expression = expression.Replace(macro.Key, macro.Value);
                }
            }
            return expression;
        }

        /// <summary>
        /// Tokenize the specified string into identifiers, subexpressions and operators.
        /// </summary>
        /// <param name="s">The expression.</param>
        protected static Tuple<string, List<Tuple<Token, string>>> Tokenize(string s)
        {
            bool escaped = false;

            s = Parser.RemoveParenthesis(s.Trim()).Trim();
            s = s.Replace("@", "@at;");
            var subexprs = new List<Tuple<Token, string>>();
            string namePattern = Parser.VariablePattern;
            namePattern = namePattern.Substring(0, namePattern.Length - 1) + "\\s*)?.*";
            var findName = new Regex(namePattern);
            var findNumber = new Regex("(" + Parser.NumberPattern + ")?.*");

            // A stack of (token kind, index of the token's start).
            var nesting = new Stack<Tuple<Token, int>>();

            for(int i = 0; i < s.Length;) {
                // Replace ID and numbers if it is the root expression, or we are not in a string
                if(nesting.Count == 0 || (nesting.Peek().Item1 != Token.Quote && nesting.Peek().Item1 != Token.DoubleQuote)) {
                    var sub = s.Substring(i);
                    var m1 = findName.Match(sub);
                    if(m1.Success) {
                        var id = m1.Groups["name"].Value;
                        if(id.Length > 0) {
                            subexprs.Add(Tuple.Create(Token.ID, id));
                            var newstr = "@" + (subexprs.Count - 1).ToString() + "@";
                            s = s.Remove(i, id.Length).Insert(i, newstr);
                            i += newstr.Length;
                            continue;
                        }
                    }
                    var m2 = findNumber.Match(sub);
                    if(m2.Success) {
                        var num = m2.Groups["number"].Value;
                        if(num.Length > 0) {
                            subexprs.Add(Tuple.Create(Token.Number, num));
                            var newstr = "@" + (subexprs.Count - 1).ToString() + "@";
                            s = s.Remove(i, num.Length).Insert(i, newstr);
                            i += newstr.Length;
                            continue;
                        }
                    }
                }

                // Replace subexprexpressions
                switch(s[i]) {
                case '(':
                    if(nesting.Count == 0 || (nesting.Peek().Item1 != Token.DoubleQuote && nesting.Peek().Item1 != Token.Quote)) {
                        bool special = false;
                        if(i > 0 && s[i - 1] == '@') {
                            special = true; // It is a call operator invocation
                        }
                        nesting.Push(Tuple.Create(special ? Token.Invocation : Token.Parenthesis, i));
                    }
                    break;
                case ')':
                    if(nesting.Count > 0 && (nesting.Peek().Item1 == Token.Invocation || nesting.Peek().Item1 == Token.Parenthesis)) {
                        int length = i - nesting.Peek().Item2 + 1;
                        subexprs.Add(Tuple.Create(nesting.Peek().Item1,
                                                  s.Substring(nesting.Peek().Item2, length)));
                        var newstr = "@" + (subexprs.Count - 1).ToString() + "@" + (nesting.Peek().Item1 == Token.Invocation ? "()" : "");
                        s = s.Remove(nesting.Peek().Item2, length).Insert(nesting.Peek().Item2,
                                                                          newstr);
                        i += newstr.Length - length;
                        nesting.Pop();
                    } else if(nesting.Count == 0 || nesting.Peek().Item1 != Token.DoubleQuote && nesting.Peek().Item1 != Token.Quote) {
                        throw new Exception(Application.Configuration.GetLocalized("Unexpected closing parenthesis found!"));
                    }
                    break;
                case '{':
                    if(nesting.Count == 0 || (nesting.Peek().Item1 != Token.DoubleQuote && nesting.Peek().Item1 != Token.Quote)) {
                        nesting.Push(Tuple.Create(Token.Brackets, i));
                    }
                    break;
                case '}':
                    if(nesting.Count > 0 && nesting.Peek().Item1 == Token.Brackets) {
                        int length = i - nesting.Peek().Item2 + 1;
                        subexprs.Add(Tuple.Create(Token.Brackets,
                                                  s.Substring(nesting.Peek().Item2, length)));
                        var newstr = "@" + (subexprs.Count - 1).ToString() + "@";
                        s = s.Remove(nesting.Peek().Item2, length).Insert(nesting.Peek().Item2,
                                                                          newstr);
                        i += newstr.Length - length;
                        nesting.Pop();
                    } else if(nesting.Count == 0 || nesting.Peek().Item1 != Token.DoubleQuote && nesting.Peek().Item1 != Token.Quote) {
                        throw new Exception(Application.Configuration.GetLocalized("Unexpected closing bracket found!"));
                    }
                    break;
                case '[':
                    if(nesting.Count == 0 || (nesting.Peek().Item1 != Token.DoubleQuote && nesting.Peek().Item1 != Token.Quote)) {
                        nesting.Push(Tuple.Create(Token.Subscript, i));
                    }
                    break;
                case ']':
                    if(nesting.Count > 0 && nesting.Peek().Item1 == Token.Subscript) {
                        int length = i - nesting.Peek().Item2 + 1;
                        subexprs.Add(Tuple.Create(nesting.Peek().Item1,
                                                  s.Substring(nesting.Peek().Item2, length)));
                        var newstr = "@" + (subexprs.Count - 1).ToString() + "@";
                        s = s.Remove(nesting.Peek().Item2, length).Insert(nesting.Peek().Item2,
                                                                          newstr);
                        i += newstr.Length - length;
                        nesting.Pop();
                    } else if(nesting.Count == 0 || nesting.Peek().Item1 != Token.DoubleQuote && nesting.Peek().Item1 != Token.Quote) {
                        throw new Exception(Application.Configuration.GetLocalized("Unexpected closing bracket found!"));
                    }
                    break;
                case '"':// First quote
                    if(nesting.Count == 0 || (nesting.Peek().Item1 != Token.DoubleQuote && nesting.Peek().Item1 != Token.Quote)) {
                        nesting.Push(Tuple.Create(Token.DoubleQuote, i));
                    }
                    // Second quote
                    else if(nesting.Count > 0 && nesting.Peek().Item1 == Token.DoubleQuote && !escaped) {
                        int length = i - nesting.Peek().Item2 + 1;
                        subexprs.Add(Tuple.Create(Token.DoubleQuote,
                                                  s.Substring(nesting.Peek().Item2, length)));
                        var newstr = "@" + (subexprs.Count - 1).ToString() + "@";
                        s = s.Remove(nesting.Peek().Item2, length).Insert(nesting.Peek().Item2,
                                                                          newstr);
                        i += newstr.Length - length;
                        nesting.Pop();
                    } else if(nesting.Peek().Item1 != Token.Quote && !escaped) {
                        throw new Exception(Application.Configuration.GetLocalized("{0} expected, but \" found!",
                                                                              nesting.Peek().Item1));
                    }
                    break;
                case '\'':// First quote
                    if(nesting.Count == 0 || (nesting.Peek().Item1 != Token.Quote && nesting.Peek().Item1 != Token.DoubleQuote)) {
                        nesting.Push(Tuple.Create(Token.Quote, i));
                    }
                    // Second quote
                    else if(nesting.Count > 0 && nesting.Peek().Item1 == Token.Quote && !escaped) {
                        int length = i - nesting.Peek().Item2 + 1;
                        subexprs.Add(Tuple.Create(Token.Quote,
                                                  s.Substring(nesting.Peek().Item2, length)));
                        var newstr = "@" + (subexprs.Count - 1).ToString() + "@";
                        s = s.Remove(nesting.Peek().Item2, length).Insert(nesting.Peek().Item2,
                                                                          newstr);
                        i += newstr.Length - length;
                        nesting.Pop();
                    } else if(nesting.Peek().Item1 != Token.DoubleQuote && !escaped) {
                        throw new Exception(Application.Configuration.GetLocalized("{0} expected, but ' found!",
                                                                              nesting.Peek().Item1));
                    }
                    break;
                }
                if(escaped) {
                    escaped = false;
                } else if(s[i] == '\\') {
                    escaped = true;
                }
                ++i;
            }

            var newExprs = new List<Tuple<Token, string>>();
            foreach(var name in Code.Operator.Lex) {
                s = s.Replace(Code.Operator.Properties[name].Code,
                              Code.Operator.Properties[name].Lexed);
            }

            foreach(var expr in subexprs) {
                string val = expr.Item2;
                switch(expr.Item1) {
                case Token.Brackets:
                case Token.Parenthesis:
                case Token.Subscript:
                case Token.Invocation:
                case Token.Template:
                    foreach(var name in Code.Operator.Lex) {
                        val = val.Replace(Code.Operator.Properties[name].Code,
                                          Code.Operator.Properties[name].Lexed);
                    }
                    break;
                }
                newExprs.Add(Tuple.Create(expr.Item1, val));
            }

            subexprs.Clear();
            foreach(var expr in newExprs) {
                var value = expr.Item2;
                if(expr.Item1 != Token.DoubleQuote && expr.Item1 != Token.Quote) {
                    value = expr.Item2.Replace("\t", "").Replace(" ", "");
                } else {
                    value = value.Trim();
                }
                subexprs.Add(Tuple.Create(expr.Item1, value));
            }

            s = s.Replace("\t", "");
            s = s.Replace(" ", "");

            return Tuple.Create(s, subexprs);
        }

        /// <summary>
        /// Checks whether the specified operator can be found at the given position in the expression.
        /// </summary>
        /// <param name="opIndex">The index in the expression to search the operator at.</param>
        /// <param name="s">The expression.</param>
        /// <param name="prop">The expression.</param>
        protected static bool Match(int opIndex, string s, Operator.Op prop)
        {
            if(prop.Type == Code.Operator.Type.Binary) {
                if(opIndex == 0 || (s[opIndex - 1] != '@' && s[opIndex - 1] != '#') || opIndex + prop.Lexed.Length == s.Length || (s[opIndex + prop.Lexed.Length] != '@' && s[opIndex + prop.Lexed.Length] != '#')) {
                    return false;
                }
            } else if(prop.Type == Code.Operator.Type.PrefixUnary) {
                if((opIndex != 0 && s[opIndex - 1] == '@') || opIndex + prop.Lexed.Length == s.Length || (s[opIndex + prop.Lexed.Length] != '@' && s[opIndex + prop.Lexed.Length] != '#')) {
                    return false;
                }
            } else if(prop.Type == Code.Operator.Type.SuffixUnary) {
                if(opIndex == 0 || (s[opIndex - 1] != '@' && s[opIndex - 1] != '#') || (opIndex + prop.Lexed.Length != s.Length && s[opIndex + prop.Lexed.Length] == '@')) {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Creates an expression from a tokenized string.
        /// </summary>
        /// <returns>The expression.</returns>
        /// <param name="s">The representation of the expression.</param>
        /// <param name="language">The programming language of the expression.</param>
        /// <param name="functions">A list of known functions. If not null, attempt will be made to match function and method calls found in the expressionto this list.</param>
        /// <param name="macros">The macros. These work like the C preprocessor, and every found key in the string is replaced by its associated value. If null, no macro expansion takes place.</param>
        /// <param name="subexprs">The types and representations of the subexpressions of the tokenized string.</param>
        protected static Expression CreateFromTokenizedString(string s,
                                                              Language language,
                                                              IEnumerable<Function> functions,
                                                              Dictionary<string, string> macros,
                                                              List<Tuple<Token, string>> subexprs)
        {
            if(Regex.Match(s, "^@[0-9]+@$").Success) {
                int nb = int.Parse(s.Substring(1, s.Length - 2));
                switch(subexprs[nb].Item1) {
                case Token.Parenthesis:
                case Token.Invocation:
                case Token.Template:
                case Token.Subscript:
                    s = Expression.GetStringFromTokenized(s, subexprs);
                    var tup = Expression.Tokenize(s);
                    s = tup.Item1;
                    subexprs = tup.Item2;
                    break;
                }
            }
            for(int i = 17; i >= 0; --i) {
                int bound;
                int direction;
                if(Code.Operator.Properties[Code.Operator.ByPrecedence[i][0]].Associativity == Code.Operator.Associativity.RightToLeft) {
                    bound = s.Length;
                    direction = -1;
                } else {
                    bound = -1;
                    direction = 1;
                }
                int index = bound;
                var foundOperator = Code.Operator.Name.None;
                foreach(var op in Code.Operator.ByPrecedence[i]) {
                    var prop = Code.Operator.Properties[op];
                    if(prop.Implemented) {
                        int currentIndex = 0;
                        while(true) {
                            int opIndex = s.Substring(currentIndex).IndexOfInv(prop.Lexed);
                            if(opIndex == -1)
                                break;
                            opIndex += currentIndex;// If we have found an operator closer to the end of the string (in relation to the operator associativity)
                            if(opIndex.CompareTo(index) == direction) {
                                if(!Match(opIndex, s, prop)) {
                                    currentIndex = opIndex + prop.Lexed.Length;
                                    continue;
                                }
                                index = opIndex;
                                foundOperator = op;
                            }
                            ++currentIndex;
                        }
                    }
                }
                if(index != bound) {
                    var prop = Code.Operator.Properties[foundOperator];
                    if(prop.Type == Code.Operator.Type.Binary) {
                        string e1 = s.Substring(0, index);
                        string e2 = s.Substring(index + prop.Lexed.Length);

                        bool methodCall = false;
                        if(foundOperator == Code.Operator.Name.SelectionRef || foundOperator == Code.Operator.Name.SelectionPtr) {
                            // Dirty hack, but if the right hand side of the selection operator is more than just an item, it must be a method call.
                            methodCall = Regex.Match(e2, "^@\\d+@@").Success;
                        }
                        if(methodCall) {
                            string that = Expression.GetStringFromTokenized(e1, subexprs);
                            string invocation = Expression.GetStringFromTokenized(e2, subexprs);
                            return CreateMethodInvocation(foundOperator == Code.Operator.Name.SelectionPtr,
                                                          that,
                                                          invocation,
                                                          language,
                                                          functions,
                                                          macros);
                        } else {
                            return new BinaryExpression(language,
                                                        foundOperator,
                                                        Expression.CreateFromTokenizedString(e1,
                                                                                             language,
                                                                                             functions,
                                                                                             macros,
                                                                                             subexprs),
                                                        Expression.CreateFromTokenizedString(e2,
                                                                                             language,
                                                                                             functions,
                                                                                             macros,
                                                                                             subexprs));
                        }
                    } else if(prop.Type == Code.Operator.Type.PrefixUnary) {
                        return new UnaryExpression(language,
                                                   foundOperator,
                                                   Expression.CreateFromTokenizedString(s.Substring(index + prop.Lexed.Length),
                                                                                        language,
                                                                                        functions,
                                                                                        macros,
                                                                                        subexprs));
                    } else if(prop.Type == Code.Operator.Type.SuffixUnary) {
                        if(foundOperator == Code.Operator.Name.FunCall) {
                            return CreateFunctionInvocation(GetStringFromTokenized(s, subexprs),
                                                            language,
                                                            functions,
                                                            macros);
                        }
                        return new UnaryExpression(language,
                                                   foundOperator,
                                                   Expression.CreateFromTokenizedString(s.Substring(0,
                                                                                                    index),
                                                                                        language,
                                                                                        functions,
                                                                                        macros,
                                                                                        subexprs));
                    }
                }
            }
            return LiteralExpression.CreateFromString(GetStringFromTokenized(s, subexprs),
                                                      language);
        }

        /// <summary>
        /// Gets the original string back from a tokenized form.
        /// </summary>
        /// <returns>The original string of the expression.</returns>
        /// <param name="tokenized">The tokenized version of the expression.</param>
        /// <param name="subexprs">The types and representations of the subexpressions of the tokenized string.</param>
        protected static string GetStringFromTokenized(string tokenized,
                                                       List<Tuple<Token, string>> subexprs)
        {
            foreach(var name in Code.Operator.Lex) {
                tokenized = tokenized.Replace(Code.Operator.Properties[name].Lexed,
                                              " " + Code.Operator.Properties[name].Code + " ");
                var newExprs = new List<Tuple<Token, string>>();
                foreach(var expr in subexprs) {
                    switch(expr.Item1) {
                    case Token.Brackets:
                    case Token.Parenthesis:
                    case Token.Subscript:
                    case Token.Invocation:
                    case Token.Template:
                        newExprs.Add(Tuple.Create(expr.Item1,
                                                  expr.Item2.Replace(Code.Operator.Properties[name].Lexed,
                                                                     " " + Code.Operator.Properties[name].Code + " ")));
                        break;
                    default:
                        newExprs.Add(expr);
                        break;
                    }
                }
                subexprs = newExprs;
            }
            int index;
            while(true) {
                int searchPos = 0;
                while(true) {
                    index = tokenized.IndexOfInv("@", searchPos);
                    if(index == -1) {
                        break;
                    }

                    if(tokenized.IndexOfInv("@at;", searchPos) == index) {
                        searchPos = index + 1;
                    } else {
                        break;
                    }
                }
                if(index == -1) {
                    break;
                }

                int lastIndex = tokenized.Substring(index + 1).IndexOfInv("@") + index + 1;
                int expr = int.Parse(tokenized.Substring(index + 1, lastIndex - (index + 1)));
                switch(subexprs[expr].Item1) {
                case Token.DoubleQuote:
                case Token.Quote:
                case Token.Parenthesis:
                case Token.Brackets:
                case Token.Subscript:
                case Token.ID:
                case Token.Number:
                    tokenized = tokenized.Remove(index, lastIndex - index + 1).Insert(index,
                                                                                      subexprs[expr].Item2);
                    break;
                case Token.Invocation:
                    tokenized = tokenized.Remove(index, lastIndex - index + 5).Insert(index,
                                                                                      subexprs[expr].Item2);
                    break;
                }
            }
            return tokenized.Replace("@at;", "@");
        }

        /// <summary>
        /// Returns the scope of a function, its name and its arguments list of its invocation.
        /// </summary>
        /// <returns>The scope name and arguments of the invocation.</returns>
        /// <param name="invocation">The expression to parse.</param>
        /// <param name="language">The programming language of the expression.</param>
        /// <param name="functions">A list of known functions. If not null, attempt will be made to match function and method calls found in the expressionto this list.</param>
        /// <param name="macros">The macros. These work like the C preprocessor, and every found key in the string is replaced by its associated value. If null, no macro expansion takes place.</param>
        static Tuple<Scope, string, List<Expression>> ExtractScopeNameAndArgs(string invocation,
                                                                              Language language,
                                                                              IEnumerable<Function> functions,
                                                                              Dictionary<string, string> macros)
        {
            var func = Expand(invocation, macros);
            func = Parser.RemoveParenthesis(func);
            int index = func.IndexOfInv("(");
            if(index == 0) {
                var prep = Tokenize(func);
                index = prep.Item2[0].Item2.Length + 2;
            }
            var args = Parser.RemoveParenthesis(func.Substring(index));
            func = func.Substring(0, index);
            var tup = Parser.ExtractScope(language, func);
            var argsList = Parser.SyntacticSplit(args, ",");
            var exprList = new List<Expression>();
            foreach(var ss in argsList) {
                exprList.Add(Expression.CreateFromString<Expression>(Parser.RemoveParenthesis(ss),
                                                                     language,
                                                                     functions,
                                                                     macros));
            }
            return Tuple.Create(tup.Item1, tup.Item2.Trim(), exprList);
        }

        /// <summary>
        /// Creates a function invocation from the given expression.
        /// </summary>
        /// <returns>The new function invocation expression.</returns>
        /// <param name="invocation">The expressio to parse.</param>
        /// <param name="language">The programming language of the expression.</param>
        /// <param name="functions">A list of known functions. If not null, attempt will be made to match function and method calls found in the expressionto this list.</param>
        /// <param name="macros">The macros. These work like the C preprocessor, and every found key in the string is replaced by its associated value. If null, no macro expansion takes place.</param>
        static FunctionInvocation CreateFunctionInvocation(string invocation,
                                                           Language language,
                                                           IEnumerable<Function> functions,
                                                           Dictionary<string, string> macros)
        {
            var scopeNameAndArgs = ExtractScopeNameAndArgs(invocation,
                                                           language,
                                                           functions,
                                                           macros);
            Function f = null;
            if(functions != null) {
                f = (functions.FirstOrDefault(delegate (Function ff) {
                    return !(ff is Method) && (ff.Parameters.Count == scopeNameAndArgs.Item3.Count || ff.IsVariadic) && scopeNameAndArgs.Item2 == ff.Name && ((scopeNameAndArgs.Item1 == null && ff.Enclosing == null) || (scopeNameAndArgs.Item1 != null && scopeNameAndArgs.Item1.Equals(ff.Enclosing)));
                })) as Function;
            }

            if(f == null) {
                f = new Function(Type.UnknownType(language),
                                 scopeNameAndArgs.Item1,
                                 scopeNameAndArgs.Item2);
                int i = 0;
                foreach(Expression e in scopeNameAndArgs.Item3) {
                    f.Parameters.Add(new Param(Type.UnknownType(language),
                                               "param" + (i++).ToString()));
                }
            }

            return new FunctionInvocation(f, scopeNameAndArgs.Item3.ToArray());
        }

        /// <summary>
        /// Creates a function invocation from the given expression.
        /// </summary>
        /// <returns>The new function invocation expression.</returns>
        /// <param name="indirection">Whether the 'this' object of the invocation is a pointer or a reference.</param>
        /// <param name="that">The 'this' object of the invocation.</param>
        /// <param name="invocation">The expressio to parse.</param>
        /// <param name="language">The programming language of the expression.</param>
        /// <param name="functions">A list of known functions. If not null, attempt will be made to match function and method calls found in the expressionto this list.</param>
        /// <param name="macros">The macros. These work like the C preprocessor, and every found key in the string is replaced by its associated value. If null, no macro expansion takes place.</param>
        static FunctionInvocation CreateMethodInvocation(bool indirection,
                                                         string that,
                                                         string invocation,
                                                         Language language,
                                                         IEnumerable<Function> functions,
                                                         Dictionary<string, string> macros)
        {
            var scopeNameAndArgs = ExtractScopeNameAndArgs(invocation,
                                                           language,
                                                           functions,
                                                           macros);
            if(Scope.GetSeparator(language) == Code.Operator.Properties[Code.Operator.Name.SelectionRef].Code
                   && Regex.Match(that, "^(" + Parser.GetNamePattern(false) + " ?" + Scope.GetSeparator(language) + " ?)*" + Parser.GetNamePattern(false) + "$").Success) {
                var scopes = that.Split(new string[] { Scope.GetSeparator(language) },
                                        StringSplitOptions.None);
                Scope outerScope = null;
                foreach(var s in scopes) {
                    outerScope = Scope.MakeFromNamespace(language, s.Trim(), outerScope);
                }
                var scope = Scope.MakeFromScopes(scopeNameAndArgs.Item1, outerScope);

                return CreateFunctionInvocation(scope.ToString() + invocation,
                                                language,
                                                functions,
                                                macros);
            }
            Method m = null;
            if(functions != null) {
                m = (functions.FirstOrDefault(delegate (Function ff) {
                    return (ff is Method) && ff.Parameters.Count == scopeNameAndArgs.Item3.Count && scopeNameAndArgs.Item2 == ff.Name && (scopeNameAndArgs.Item1?.Equals(ff.Enclosing) ?? ff.Enclosing == null);
                })) as Method;
            }
            if(m == null) {
                m = new Method(Type.UnknownType(language),
                               Type.UnknownType(language),
                               scopeNameAndArgs.Item2,
                               "",
                               "",
                               false);
                int i = 0;
                foreach(Expression e in scopeNameAndArgs.Item3) {
                    m.Parameters.Add(new Param(Type.UnknownType(language),
                                               "param" + (i++).ToString()));
                }
            }
            return new MethodInvocation(m,
                                        Expression.CreateFromString<Expression>(that,
                                                                                language,
                                                                                functions,
                                                                                macros),
                                        indirection,
                                        scopeNameAndArgs.Item3.ToArray());
        }

        /// <summary>
        /// Checks whether the child expression needs to be parenthesized, and adds the parens if needed.
        /// </summary>
        /// <param name="parent">The expression "wrapping" the expression to parenthesized.</param>
        /// <param name="child">The expression to parenthesize.</param>
        /// <param name="representation">The string that will be put into parentheses if some need to be added.</param>
        protected static string Parenthesize(Expression parent,
                                             Expression child,
                                             string representation)
        {
            bool parenthesize = false;
            if(child.Operator != Code.Operator.Name.None) {
                var parentProperties = Code.Operator.Properties[parent.Operator];
                var childProperties = Code.Operator.Properties[child.Operator];
                if(parentProperties.Precedence < childProperties.Precedence) {
                    parenthesize = true;
                } else if(parentProperties.Precedence == childProperties.Precedence) {// No need to manage unary operators
                    // We assume the ternary conditional operator does not need to be parenthesized either
                    if(parentProperties.Type == Code.Operator.Type.Binary) {
                        var castedParent = (BinaryExpression)parent;// We can assume the associativity is the same for both operators as they have the same precedence
                        // If the operator is left-associative, but the expression was parenthesized from right to left
                        // eg. a + (b + c) (do not forget that IEEE floating point values are not communtative with regards to addition, among others).
                        // So we need to preserve the associativity the user gave at first.
                        if(parentProperties.Associativity == Code.Operator.Associativity.LeftToRight && castedParent.Expression2 == child) {
                            parenthesize = true;
                        }
                        // If the operator is right-associative, but the expression was parenthesize from left to right
                        else if(parentProperties.Associativity == Code.Operator.Associativity.RightToLeft && castedParent.Expression1 == child) {
                            parenthesize = true;
                        }
                    }
                }
            }
            if(parenthesize) {
                return "(" + representation + ")";
            } else {
                return representation;
            }
        }

    }
}
