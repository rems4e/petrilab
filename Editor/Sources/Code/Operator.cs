//
//  Operator.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2014-11-13.
//

using System.Collections.Generic;
using System.Linq;

namespace Petri.Code
{
    /// <summary>
    /// Namespace-like class that contains stuffs related to operators.
    /// </summary>
    public static class Operator
    {
        /// <summary>
        /// The list of all the available operators.
        /// </summary>
        public enum Name
        {
            /// <summary>
            /// ::
            /// </summary>
            ScopeResolution,


            /// <summary>
            /// expr++
            /// </summary>
            PostIncr,

            /// <summary>
            /// expr--
            /// </summary>
            PostDecr,

            /// <summary>
            /// type(expr)
            /// </summary>
            FunctionalCast,

            /// <summary>
            /// ()
            /// </summary>
            FunCall,

            /// <summary>
            /// []
            /// </summary>
            Subscript,

            /// <summary>
            /// .
            /// </summary>
            SelectionRef,

            /// <summary>
            /// ->
            /// </summary>
            SelectionPtr,



            /// <summary>
            /// ++expr
            /// </summary>
            PreIncr,

            /// <summary>
            /// --
            /// </summary>
            PreDecr,

            /// <summary>
            /// +
            /// </summary>
            UnaryPlus,

            /// <summary>
            /// -
            /// </summary>
            UnaryMinus,

            /// <summary>
            /// !
            /// </summary>
            LogicalNot,

            /// <summary>
            /// ~
            /// </summary>
            BitwiseNot,

            /// <summary>
            /// (type)expr
            /// </summary>
            CCast,

            /// <summary>
            /// *
            /// </summary>
            Indirection,

            /// <summary>
            /// &amp;
            /// </summary>
            AddressOf,

            /// <summary>
            /// sizeof
            /// </summary>
            Sizeof,

            /// <summary>
            /// new
            /// </summary>
            New,

            /// <summary>
            /// new[]
            /// </summary>
            NewTab,

            /// <summary>
            /// delete
            /// </summary>
            Delete,

            /// <summary>
            /// delete[]
            /// </summary>
            DeleteTab,


            /// <summary>
            /// .*
            /// </summary>
            PtrToMemRef,

            /// <summary>
            /// .->
            /// </summary>
            PtrToMemPtr,

            /// <summary>
            /// *
            /// </summary>
            Mult,

            /// <summary>
            /// /
            /// </summary>
            Div,

            /// <summary>
            /// %
            /// </summary>
            Mod,

            /// <summary>
            /// +
            /// </summary>
            Plus,

            /// <summary>
            /// -
            /// </summary>
            Minus,

            /// <summary>
            /// &lt;&lt;
            /// </summary>
            ShiftLeft,

            /// <summary>
            /// &gt;&gt;
            /// </summary>
            ShiftRight,

            /// <summary>
            /// &lt;=
            /// </summary>
            LessEqual,

            /// <summary>
            /// &lt;
            /// </summary>
            Less,

            /// <summary>
            /// >=
            /// </summary>
            GreaterEqual,

            /// <summary>
            /// >
            /// </summary>
            Greater,

            /// <summary>
            /// ==
            /// </summary>
            Equal,

            /// <summary>
            /// !=
            /// </summary>
            NotEqual,

            /// <summary>
            /// &amp;
            /// </summary>
            BitwiseAnd,

            /// <summary>
            /// ^
            /// </summary>
            BitwiseXor,

            /// <summary>
            /// |
            /// </summary>
            BitwiseOr,

            /// <summary>
            /// &amp;&amp;
            /// </summary>
            LogicalAnd,

            /// <summary>
            /// ||
            /// </summary>
            LogicalOr,


            /// <summary>
            /// ?:
            /// </summary>
            TernaryConditional,

            /// <summary>
            /// =
            /// </summary>
            Assignment,

            /// <summary>
            /// +=
            /// </summary>
            PlusAssign,

            /// <summary>
            /// -=
            /// </summary>
            MinusAssign,

            /// <summary>
            /// *=
            /// </summary>
            MultAssign,

            /// <summary>
            /// /=
            /// </summary>
            DivAssign,

            /// <summary>
            /// %=
            /// </summary>
            ModAssign,

            /// <summary>
            /// &lt;&lt;=
            /// </summary>
            ShiftLeftAssign,

            /// <summary>
            /// &gt;&gt;=
            /// </summary>
            ShiftRightAssign,

            /// <summary>
            /// &amp;=
            /// </summary>
            BitwiseAndAssig,

            /// <summary>
            /// ^=
            /// </summary>
            BitwiseXorAssign,

            /// <summary>
            /// |=
            /// </summary>
            BitwiseOrAssign,

            /// <summary>
            /// throw
            /// </summary>
            Throw,

            /// <summary>
            /// ,
            /// </summary>
            Comma,

            /// <summary>
            /// No operator for the given expression
            /// </summary>
            None
        }

        /// <summary>
        /// The associativity of an operator.
        /// </summary>
        public enum Associativity
        {
            /// <summary>
            /// The left to right associativity.
            /// </summary>
            LeftToRight,

            /// <summary>
            /// The right to left associativity.
            /// </summary>
            RightToLeft
        }

        /// <summary>
        /// The type of an operator.
        /// </summary>
        public enum Type
        {
            /// <summary>
            /// A binary operator (e.g. in 'a + b').
            /// </summary>
            Binary,

            /// <summary>
            /// A prefix unary operator (e.g. in '++a').
            /// </summary>
            PrefixUnary,

            /// <summary>
            /// A suffix unary operator (e.g. in 'a++').
            /// </summary>
            SuffixUnary,

            /// <summary>
            /// A ternary operator (e.g. 'a ? b : c').
            /// </summary>
            Ternary
        }

        /// <summary>
        /// A struct containing all the characteristics of a programming language operator.
        /// </summary>
        public struct Op
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Petri.Code.Operator.Op"/> struct.
            /// </summary>
            /// <param name="a">The associativity.</param>
            /// <param name="t">The operator type.</param>
            /// <param name="code">The code representation of the operator.</param>
            /// <param name="prec">The precedence of the operator.</param>
            /// <param name="impl">If <c>true</c> then the operator is considered as implemented by the parser/generator framework.</param>
            public Op(Associativity a, Type t, string code, int prec, bool impl)
            {
                Associativity = a;
                Type = t;
                Precedence = prec;
                Implemented = impl;
                Code = code;
                Id = _id++;
                if(!_lexed.ContainsKey(Code)) {
                    _lexed[Code] = Id;
                }
                Lexed = "#" + _lexed[Code].ToString() + "#";
            }

            /// <summary>
            /// The code representation of the operator.
            /// </summary>
            /// <value>The code.</value>
            public string Code {
                get;
                private set;
            }

            /// <summary>
            /// Gets a value indicating whether this <see cref="Petri.Code.Operator.Op"/> is implemented by the parser/generator framework.
            /// </summary>
            /// <value><c>true</c> if implemented; otherwise, <c>false</c>.</value>
            public bool Implemented {
                get;
                private set;
            }

            /// <summary>
            /// Gets the associativity of the operator.
            /// </summary>
            /// <value>The associativity.</value>
            public Associativity Associativity {
                get;
                private set;
            }

            /// <summary>
            /// Gets the type of the operator.
            /// </summary>
            /// <value>The type.</value>
            public Type Type {
                get;
                private set;
            }

            /// <summary>
            /// Gets the precedence of the operator.
            /// </summary>
            /// <value>The precedence.</value>
            public int Precedence {
                get;
                private set;
            }

            /// <summary>
            /// Gets the id of the operator.
            /// </summary>
            /// <value>The identifier.</value>
            public int Id {
                get;
                private set;
            }

            /// <summary>
            /// Gets the lexed form of the operator.
            /// </summary>
            /// <value>The lexed.</value>
            public string Lexed {
                get;
                private set;
            }


            static int _id = 0;
            static Dictionary<string, int> _lexed = new Dictionary<string, int>();
        }

        /// <summary>
        /// Gets the properties of the operators mapped by their name.
        /// </summary>
        /// <value>The properties.</value>
        public static Dictionary<Name, Op> Properties {
            get;
            private set;
        }

        /// <summary>
        /// Gets the list of operators mapped by their precedence.
        /// </summary>
        /// <value>The by precedence.</value>
        public static Dictionary<int, List<Name>> ByPrecedence {
            get;
            private set;
        }

        /// <summary>
        /// Gets the operators mapped by their ID.
        /// </summary>
        /// <value>The by I.</value>
        public static Dictionary<int, Name> ByID {
            get;
            private set;
        }

        /// Gets the lexed forms of the operators.
        public static List<Name> Lex {
            get;
            private set;
        }

        static Operator()
        {
            Properties = new Dictionary<Name, Op>();

            Properties[Name.ScopeResolution] = new Op(Associativity.LeftToRight,
                                                      Type.Binary,
                                                      "::",
                                                      0,
                                                      false);

            Properties[Name.PostIncr] = new Op(Associativity.LeftToRight,
                                               Type.SuffixUnary,
                                               "++",
                                               1,
                                               true);
            Properties[Name.PostDecr] = new Op(Associativity.LeftToRight,
                                               Type.SuffixUnary,
                                               "--",
                                               1,
                                               true);
            Properties[Name.FunctionalCast] = new Op(Associativity.LeftToRight,
                                                     Type.PrefixUnary,
                                                     "",
                                                     1,
                                                     false);
            Properties[Name.FunCall] = new Op(Associativity.LeftToRight,
                                              Type.SuffixUnary,
                                              "()",
                                              1,
                                              true);
            Properties[Name.Subscript] = new Op(Associativity.LeftToRight,
                                                Type.SuffixUnary,
                                                "",
                                                1,
                                                false);

            // Actual precedence is the same as function call for . and ->, but we cheat for f().X being parsed
            Properties[Name.SelectionRef] = new Op(Associativity.LeftToRight,
                                                   Type.Binary,
                                                   ".",
                                                   2,
                                                   true);
            Properties[Name.SelectionPtr] = new Op(Associativity.LeftToRight,
                                                   Type.Binary,
                                                   "->",
                                                   2,
                                                   true);

            Properties[Name.PreIncr] = new Op(Associativity.RightToLeft,
                                              Type.PrefixUnary,
                                              "++",
                                              3,
                                              true);
            Properties[Name.PreDecr] = new Op(Associativity.RightToLeft,
                                              Type.PrefixUnary,
                                              "--",
                                              3,
                                              true);
            Properties[Name.UnaryPlus] = new Op(Associativity.RightToLeft,
                                                Type.PrefixUnary,
                                                "+",
                                                3,
                                                true);
            Properties[Name.UnaryMinus] = new Op(Associativity.RightToLeft,
                                                 Type.PrefixUnary,
                                                 "-",
                                                 3,
                                                 true);
            Properties[Name.LogicalNot] = new Op(Associativity.RightToLeft,
                                                 Type.PrefixUnary,
                                                 "!",
                                                 3,
                                                 true);
            Properties[Name.BitwiseNot] = new Op(Associativity.RightToLeft,
                                                 Type.PrefixUnary,
                                                 "~",
                                                 3,
                                                 true);
            Properties[Name.CCast] = new Op(Associativity.RightToLeft,
                                            Type.PrefixUnary,
                                            "",
                                            3,
                                            false);
            Properties[Name.Indirection] = new Op(Associativity.RightToLeft,
                                                  Type.PrefixUnary,
                                                  "*",
                                                  3,
                                                  false);
            Properties[Name.AddressOf] = new Op(Associativity.RightToLeft,
                                                Type.PrefixUnary,
                                                "&",
                                                3,
                                                true);
            Properties[Name.Sizeof] = new Op(Associativity.RightToLeft,
                                             Type.PrefixUnary,
                                             "",
                                             3,
                                             false);
            Properties[Name.New] = new Op(Associativity.RightToLeft,
                                          Type.PrefixUnary,
                                          "",
                                          3,
                                          false);
            Properties[Name.NewTab] = new Op(Associativity.RightToLeft,
                                             Type.PrefixUnary,
                                             "",
                                             3,
                                             false);
            Properties[Name.Delete] = new Op(Associativity.RightToLeft,
                                             Type.PrefixUnary,
                                             "",
                                             3,
                                             false);
            Properties[Name.DeleteTab] = new Op(Associativity.RightToLeft,
                                                Type.PrefixUnary,
                                                "",
                                                3,
                                                false);

            Properties[Name.PtrToMemRef] = new Op(Associativity.LeftToRight,
                                                  Type.Binary,
                                                  ".*",
                                                  4,
                                                  false);
            Properties[Name.PtrToMemPtr] = new Op(Associativity.LeftToRight,
                                                  Type.Binary,
                                                  "->*",
                                                  4,
                                                  false);

            Properties[Name.Mult] = new Op(Associativity.LeftToRight,
                                           Type.Binary,
                                           "*",
                                           5,
                                           true);
            Properties[Name.Div] = new Op(Associativity.LeftToRight, Type.Binary, "/", 5, true);
            Properties[Name.Mod] = new Op(Associativity.LeftToRight, Type.Binary, "%", 5, true);

            Properties[Name.Plus] = new Op(Associativity.LeftToRight,
                                           Type.Binary,
                                           "+",
                                           6,
                                           true);
            Properties[Name.Minus] = new Op(Associativity.LeftToRight,
                                            Type.Binary,
                                            "-",
                                            6,
                                            true);

            Properties[Name.ShiftLeft] = new Op(Associativity.LeftToRight,
                                                Type.Binary,
                                                "<<",
                                                7,
                                                true);
            Properties[Name.ShiftRight] = new Op(Associativity.LeftToRight,
                                                 Type.Binary,
                                                 ">>",
                                                 7,
                                                 true);

            Properties[Name.LessEqual] = new Op(Associativity.LeftToRight,
                                                Type.Binary,
                                                "<=",
                                                8,
                                                true);
            Properties[Name.Less] = new Op(Associativity.LeftToRight,
                                           Type.Binary,
                                           "<",
                                           8,
                                           true);
            Properties[Name.GreaterEqual] = new Op(Associativity.LeftToRight,
                                                   Type.Binary,
                                                   ">=",
                                                   8,
                                                   true);
            Properties[Name.Greater] = new Op(Associativity.LeftToRight,
                                              Type.Binary,
                                              ">",
                                              8,
                                              true);

            Properties[Name.Equal] = new Op(Associativity.LeftToRight,
                                            Type.Binary,
                                            "==",
                                            9,
                                            true);
            Properties[Name.NotEqual] = new Op(Associativity.LeftToRight,
                                               Type.Binary,
                                               "!=",
                                               9,
                                               true);

            Properties[Name.BitwiseAnd] = new Op(Associativity.LeftToRight,
                                                 Type.Binary,
                                                 "&",
                                                 10,
                                                 false);
            Properties[Name.BitwiseXor] = new Op(Associativity.LeftToRight,
                                                 Type.Binary,
                                                 "^",
                                                 11,
                                                 false);
            Properties[Name.BitwiseOr] = new Op(Associativity.LeftToRight,
                                                Type.Binary,
                                                "|",
                                                12,
                                                false);
            Properties[Name.LogicalAnd] = new Op(Associativity.LeftToRight,
                                                 Type.Binary,
                                                 "&&",
                                                 13,
                                                 true);
            Properties[Name.LogicalOr] = new Op(Associativity.LeftToRight,
                                                Type.Binary,
                                                "||",
                                                14,
                                                true);

            Properties[Name.TernaryConditional] = new Op(Associativity.RightToLeft,
                                                         Type.Ternary,
                                                         "?",
                                                         15,
                                                         false);
            Properties[Name.Assignment] = new Op(Associativity.RightToLeft,
                                                 Type.Binary,
                                                 "=",
                                                 15,
                                                 true);
            Properties[Name.PlusAssign] = new Op(Associativity.RightToLeft,
                                                 Type.Binary,
                                                 "+=",
                                                 15,
                                                 false);
            Properties[Name.MinusAssign] = new Op(Associativity.RightToLeft,
                                                  Type.Binary,
                                                  "-=",
                                                  15,
                                                  false);
            Properties[Name.MultAssign] = new Op(Associativity.RightToLeft,
                                                 Type.Binary,
                                                 "*=",
                                                 15,
                                                 false);
            Properties[Name.DivAssign] = new Op(Associativity.RightToLeft,
                                                Type.Binary,
                                                "/=",
                                                15,
                                                false);
            Properties[Name.ModAssign] = new Op(Associativity.RightToLeft,
                                                Type.Binary,
                                                "%=",
                                                15,
                                                false);
            Properties[Name.ShiftLeftAssign] = new Op(Associativity.RightToLeft,
                                                      Type.Binary,
                                                      "<<=",
                                                      15,
                                                      false);
            Properties[Name.ShiftRightAssign] = new Op(Associativity.RightToLeft,
                                                       Type.Binary,
                                                       ">>=",
                                                       15,
                                                       false);
            Properties[Name.BitwiseAndAssig] = new Op(Associativity.RightToLeft,
                                                      Type.Binary,
                                                      "&=",
                                                      15,
                                                      false);
            Properties[Name.BitwiseXorAssign] = new Op(Associativity.RightToLeft,
                                                       Type.Binary,
                                                       "^=",
                                                       15,
                                                       false);
            Properties[Name.BitwiseOrAssign] = new Op(Associativity.RightToLeft,
                                                      Type.Binary,
                                                      "|=",
                                                      15,
                                                      false);

            Properties[Name.Throw] = new Op(Associativity.LeftToRight,
                                            Type.PrefixUnary,
                                            "",
                                            16,
                                            false);

            Properties[Name.Comma] = new Op(Associativity.LeftToRight,
                                            Type.Binary,
                                            ",",
                                            17,
                                            true);

            ByID = Properties.ToDictionary(x => x.Value.Id, x => x.Key);
            ByPrecedence = new Dictionary<int, List<Name>>();
            for(int i = 0; i <= 17; ++i) {
                ByPrecedence[i] = new List<Name>();
                ByPrecedence[i].AddRange(from prop in Properties
                                         where prop.Value.Precedence == i
                                         select prop.Key);
            }

            Lex = new List<Name>();
            foreach(var tup in Properties) {
                if(tup.Value.Implemented) {
                    Lex.Add(tup.Key);
                }
            }
            Lex.Sort((x, y) => {
                return -Properties[x].Code.Length.CompareTo(Properties[y].Code.Length);
            });
            for(int i = 1; i < Lex.Count;) {
                if(Properties[Lex[i]].Code == Properties[Lex[i - 1]].Code) {
                    Lex.RemoveAt(i);
                } else {
                    ++i;
                }
            }
        }
    }
}

