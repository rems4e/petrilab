//
//  Enum.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-24.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Petri.Code
{
    /// <summary>
    /// Represents an enumeration.
    /// </summary>
    public class Enum : IEquatable<Enum>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Code.Enum"/> class.
        /// </summary>
        /// <param name="language">The programming language of the enum.</param>
        /// <param name="name">The name of the enum.</param>
        /// <param name="members">The enum's members' name.</param>
        public Enum(Language language, String name, IEnumerable<string> members)
        {
            Language = language;
            Name = name;
            Members = members.ToArray();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Code.Enum"/> class.
        /// </summary>
        /// <param name="language">The programming language of the enum.</param>
        /// <param name="commaSeparatedList">A comma-separated list of identifiers, the first of which is the name of the enum, and the others the members of the enum.</param>
        public Enum(Language language, string commaSeparatedList)
        {
            Language = language;

            commaSeparatedList = commaSeparatedList.Replace(" ", "");
            commaSeparatedList = commaSeparatedList.Replace("\t", "");
            var lst = commaSeparatedList.Split(new char[] { ',' }, StringSplitOptions.None);
            Regex name = new Regex(Code.Parser.GetNamePattern(true));

            bool ok = lst.Length >= 2;
            if(ok) {
                foreach(var v in lst) {
                    Match nameMatch = name.Match(v);
                    if(!nameMatch.Success || nameMatch.Value != v) {
                        ok = false;
                        break;
                    }
                }
            }

            if(!ok) {
                throw new Exception(Application.Configuration.GetLocalized("Invalid comma separated-stored enum"));
            }

            Name = lst[0];
            Members = lst.Skip(1).ToArray();
        }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents the current <see cref="Petri.Code.Enum"/>.
        /// It is represented as a comma separated-list of identifiers, as per the second constructor of this class.
        /// </summary>
        /// <returns>A <see cref="System.String"/> that represents the current <see cref="Petri.Code.Enum"/>.</returns>
        public override string ToString()
        {
            return Name + "," + String.Join(",", Members);
        }

        /// <summary>
        /// Gets the name of the enum.
        /// </summary>
        /// <value>The name.</value>
        public string Name {
            get;
            private set;
        }

        /// <summary>
        /// Gets the programming language of the enum.
        /// </summary>
        /// <value>The language.</value>
        public Language Language {
            get;
            private set;
        }

        /// <summary>
        /// Gets a type representing the enum.
        /// </summary>
        /// <value>The type.</value>
        public Type Type {
            get {
                return new Type(Language, Name);
            }
        }

        /// <summary>
        /// Gets the list of members of the enum.
        /// </summary>
        /// <value>The members.</value>
        public string[] Members {
            get;
            private set;
        }

        /// <summary>
        /// Determines whether the specified <see cref="Petri.Code.Enum"/> is equal to the current <see cref="Petri.Code.Enum"/>.
        /// </summary>
        /// <param name="e">The <see cref="Petri.Code.Enum"/> to compare with the current <see cref="Petri.Code.Enum"/>.</param>
        /// <returns><c>true</c> if the specified <see cref="Petri.Code.Enum"/> is equal to the current
        /// <see cref="Petri.Code.Enum"/>; otherwise, <c>false</c>.</returns>
        public bool Equals(Enum e)
        {
            if(Name != e.Name || Members.Length != e.Members.Length) {
                return false;
            }

            for(int i = 0; i < Members.Length; ++i) {
                if(Members[i] != e.Members[i]) {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object"/> is equal to the current <see cref="Petri.Code.Enum"/>.
        /// </summary>
        /// <param name="e">The <see cref="System.Object"/> to compare with the current <see cref="Petri.Code.Enum"/>.</param>
        /// <returns><c>true</c> if the specified <see cref="System.Object"/> is equal to the current
        /// <see cref="Petri.Code.Enum"/>; otherwise, <c>false</c>.</returns>
        public override bool Equals(object e)
        {
            if(e is Enum) {
                return Equals((Enum)e);
            }

            return false;
        }

        /// <summary>
        /// Serves as a hash function for a <see cref="Petri.Code.Enum"/> object.
        /// </summary>
        /// <returns>A hash code for this instance that is suitable for use in hashing algorithms and data structures such as a
        /// hash table.</returns>
        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }
    }
}

