//
//  Function.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-24.
//

using System.Collections.Generic;

namespace Petri.Code
{
    /// <summary>
    /// A function parameter
    /// </summary>
    public class Param
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Code.Param"/> class.
        /// </summary>
        /// <param name="type">The type of the parameter.</param>
        /// <param name="name">The name of the parameter.</param>
        public Param(Type type, string name)
        {
            Type = type;
            Name = name.Trim();
        }

        /// <summary>
        /// Gets the type of the parameter.
        /// </summary>
        /// <value>The type.</value>
        public Type Type {
            get; private set;
        }

        /// <summary>
        /// Gets the name of the parameter.
        /// </summary>
        /// <value>The name.</value>
        public string Name {
            get; private set;
        }
    }

    /// <summary>
    /// A function.
    /// </summary>
    public class Function
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Code.Function"/> class.
        /// </summary>
        /// <param name="returnType">The return type of the function.</param>
        /// <param name="enclosing">The enclosing scope of the function. If not null, this creates a namespace-enclosed function, or a static method.</param>
        /// <param name="name">The name of the function.</param>
        /// <param name="template">If set to <c>true</c>, then the function is template.</param>
        /// <param name="isVariadic">Whether the function takes a variable number of arguments.</param>
        public Function(Type returnType, Scope enclosing, string name, bool template = false, bool isVariadic = false)
        {
            ReturnType = returnType;
            Name = name.Trim();

            Parameters = new List<Param>();
            Enclosing = enclosing;

            Template = template;
            IsVariadic = isVariadic;
        }

        /// <summary>
        /// Gets the language of the function.
        /// </summary>
        /// <value>The language.</value>
        public Language Language {
            get {
                return ReturnType.Language;
            }
        }

        /// <summary>
        /// Gets the return type of the function.
        /// </summary>
        /// <value>The return type.</value>
        public Type ReturnType {
            get;
            private set;
        }

        /// <summary>
        /// Gets the name of the function.
        /// </summary>
        /// <value>The name.</value>
        public string Name {
            get;
            private set;
        }

        /// <summary>
        /// Gets a value indicating whether this <see cref="Petri.Code.Function"/> is template.
        /// </summary>
        /// <value><c>true</c> if template; otherwise, <c>false</c>.</value>
        public bool Template {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets the template arguments of the function.
        /// </summary>
        /// <value>The template arguments.</value>
        public string TemplateArguments {
            get;
            set;
        }

        /// <summary>
        /// Gets the qualified name of the function, i.e. its scope concatenated with its name.
        /// </summary>
        /// <value>The name of the qualified.</value>
        public string QualifiedName {
            get {
                string qn = "";
                if(Enclosing != null) {
                    qn = Enclosing.ToString();
                }

                return qn + Name;
            }
        }

        /// <summary>
        /// Gets the enclosing scope of the function.
        /// </summary>
        /// <value>The enclosing scope.</value>
        public Scope Enclosing {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets the header which declares the function.
        /// </summary>
        /// <value>The header.</value>
        public string Header {
            get;
            set;
        }

        /// <summary>
        /// Gets the list of parameters of the functions.
        /// </summary>
        /// <value>The parameters.</value>
        public List<Param> Parameters {
            get;
            private set;
        }

        /// <summary>
        /// Adds a parameter to the function.
        /// </summary>
        /// <param name="p">The new parameter</param>
        public void AddParam(Param p)
        {
            Parameters.Add(p);
        }

        /// <summary>
        /// Gets the signature of the function, in the C++ meaning of the term.
        /// </summary>
        /// <value>The signature.</value>
        public virtual string Signature {
            get {
                string s = QualifiedName + "(";
                foreach(var p in Parameters) {
                    s += p.Type + (string.IsNullOrEmpty(p.Name) ? "" : " ") + p.Name + ", ";
                }
                if(s.EndsWithInv(", ")) {
                    s = s.Substring(0, s.Length - 2);
                }
                s += ")";

                return s;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this <see cref="T:Petri.Code.Function"/> takes a variable
        /// number of arguments.
        /// </summary>
        /// <value><c>true</c> if is variadic; otherwise, <c>false</c>.</value>
        public bool IsVariadic {
            get;
            private set;
        }

        /// <summary>
        /// Gets the prototype of the function, in the C++ meaning of the term.
        /// </summary>
        /// <value>The prototype.</value>
        public string Prototype {
            get {
                if(Language == Language.Python) {
                    return Signature;
                }
                return ReturnType + " " + Signature;
            }
        }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents the current <see cref="Petri.Code.Function"/>.
        /// </summary>
        /// <returns>A <see cref="System.String"/> that represents the current <see cref="Petri.Code.Function"/>.</returns>
        public override string ToString()
        {
            return Prototype;
        }


        static string ParametersPattern {
            get {
                return @"\((?<parameters>([^)]*)*)\)";
            }
        }

        static string VisibilityPattern {
            get {
                return "((?<visibility>(public|protected|private|internal))?)";
            }
        }

        static string StartQualifiersPattern {
            get {
                return "(?<start_qualifiers>(new|override|virtual|abstract|inline|(?<static>(static))| )*)";
            }
        }

        static string EndQualifiersPattern {
            get {
                return "(?<end_qualifiers>(&|&&|const|volatile|override|final|nothrow|= ?(default|0|delete)| )*)";
            }
        }

        static string FunctionPattern {
            get {
                return @"^" + VisibilityPattern + " ?" + Function.StartQualifiersPattern + " ?" + Type.RegexPattern + @" " + Parser.GetNamePattern(false) + " ?" + Function.ParametersPattern + " ?" + Function.EndQualifiersPattern;
            }
        }

        /// <summary>
        /// Gets a regex that matches function declarations.
        /// This currently matches C, C++ and C# function declarations, so it may detect invalid code. Not a problem, as it should not compile anyway.
        /// </summary>
        /// <value>The regex.</value>
        public static System.Text.RegularExpressions.Regex Regex {
            get {
                return new System.Text.RegularExpressions.Regex(FunctionPattern);
            }
        }
    }

    /// <summary>
    /// A method.
    /// </summary>
    public class Method : Function
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Code.Method"/> class.
        /// </summary>
        /// <param name="classType">The class enclosing the method.</param>
        /// <param name="returnType">The return type of the function.</param>
        /// <param name="name">The name of the function.</param>
        /// <param name="startQualifiers">The start qualifiers of the method, such as static, inline, override for C#.</param>
        /// <param name="endQualifiers">The end qualifiers of the method, such as override and const for C++.</param>
        /// <param name="template">If set to <c>true</c>, then the function is template.</param>
        public Method(Type classType,
                      Type returnType,
                      string name,
                      string startQualifiers,
                      string endQualifiers,
                      bool template = false) : base(returnType,
                                                    Scope.MakeFromClass(classType),
                                                    name,
                                                    template)
        {
            StartQualifiers = startQualifiers;
            EndQualifiers = endQualifiers;
            Class = classType;
        }

        /// <summary>
        /// Gets the start qualifiers of the method, such as static, inline, override for C#.
        /// </summary>
        /// <value>The start qualifiers.</value>
        public string StartQualifiers {
            get;
            private set;
        }

        /// <summary>
        /// Gets the end qualifiers of the method, such as override and const for C++.
        /// </summary>
        /// <value>The end qualifiers.</value>
        public string EndQualifiers {
            get;
            private set;
        }

        /// <summary>
        /// The class enclosing the method.
        /// </summary>
        /// <value>The class.</value>
        public Type Class {
            get;
            private set;
        }

        /// <summary>
        /// Gets the signature of the method, in the C++ meaning of the term.
        /// </summary>
        /// <value>The signature.</value>
        public override string Signature {
            get {
                return (StartQualifiers.Length > 0 ? " " + StartQualifiers : "") + base.Signature + (EndQualifiers.Length > 0 ? " " + EndQualifiers : "");
            }
        }
    }
}

