//
//  BinaryExpression.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-05.
//

using System;
using System.Collections.Generic;

namespace Petri.Code
{
    /// <summary>
    /// A code expression made of two subexpressions and a binary operator.
    /// </summary>
    public class BinaryExpression : Expression
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Code.BinaryExpression"/> class.
        /// </summary>
        /// <param name="language">The language of the expression.</param>
        /// <param name="o">The operator.</param>
        /// <param name="expr1">Subexpression 1.</param>
        /// <param name="expr2">Subexpression 2.</param>
        public BinaryExpression(Language language,
                                Operator.Name o,
                                Expression expr1,
                                Expression expr2) : base(language,
                                                         o)
        {
            Expression1 = expr1;
            Expression2 = expr2;
        }

        /// <summary>
        /// The first subexpression.
        /// </summary>
        /// <value>The expressionv1.</value>
        public Expression Expression1 {
            get; private set;
        }

        /// <summary>
        /// The second subexpression.
        /// </summary>
        /// <value>The expressionv2.</value>
        public Expression Expression2 {
            get; private set;
        }

        /// <summary>
        /// Checks whether this instance uses the specified function as part of one of its subexpression.
        /// </summary>
        /// <returns><c>true</c>, if function is used, <c>false</c> otherwise.</returns>
        /// <param name="f">The function to test.</param>
        public override bool UsesFunction(Function f)
        {
            return Expression1.UsesFunction(f) || Expression2.UsesFunction(f);
        }

        /// <summary>
        /// Generates the source code corresponding to the expression
        /// </summary>
        /// <returns>The code.</returns>
        public override string MakeCode()
        {
            string e1 = Expression.Parenthesize(this,
                                                Expression1,
                                                Expression1.MakeCode());
            string e2 = Expression.Parenthesize(this,
                                                Expression2,
                                                Expression2.MakeCode());
            switch(Operator) {
            case Petri.Code.Operator.Name.SelectionRef:
                return e1 + "." + e2;
            case Petri.Code.Operator.Name.SelectionPtr:
                return e1 + "->" + e2;
            case Petri.Code.Operator.Name.Mult:
                return e1 + " * " + e2;
            case Petri.Code.Operator.Name.Div:
                return e1 + " / " + e2;
            case Petri.Code.Operator.Name.Mod:
                return e1 + " % " + e2;
            case Petri.Code.Operator.Name.Plus:
                return e1 + " + " + e2;
            case Petri.Code.Operator.Name.Minus:
                return e1 + " - " + e2;
            case Petri.Code.Operator.Name.ShiftLeft:
                return e1 + " << " + e2;
            case Petri.Code.Operator.Name.ShiftRight:
                return e1 + " >> " + e2;
            case Petri.Code.Operator.Name.Less:
                return e1 + " < " + e2;
            case Petri.Code.Operator.Name.LessEqual:
                return e1 + " <= " + e2;
            case Petri.Code.Operator.Name.Greater:
                return e1 + " > " + e2;
            case Petri.Code.Operator.Name.GreaterEqual:
                return e1 + " >= " + e2;
            case Petri.Code.Operator.Name.Equal:
                return e1 + " == " + e2;
            case Petri.Code.Operator.Name.NotEqual:
                return e1 + " != " + e2;
            case Petri.Code.Operator.Name.BitwiseAnd:
                return e1 + " & " + e2;
            case Petri.Code.Operator.Name.BitwiseXor:
                return e1 + " ^ " + e2;
            case Petri.Code.Operator.Name.BitwiseOr:
                return e1 + " | " + e2;
            case Petri.Code.Operator.Name.LogicalAnd:
                return e1 + " && " + e2;
            case Petri.Code.Operator.Name.LogicalOr:
                return e1 + " || " + e2;
            case Petri.Code.Operator.Name.Assignment:
                return e1 + " = " + e2;
            case Petri.Code.Operator.Name.Comma:
                return e1 + ", " + e2;
            }
            throw new Exception(Application.Configuration.GetLocalized("Operator not implemented!"));
        }

        /// <summary>
        /// Generates a user-readable form of the expression. It may be similar to the result of <c>MakeCode()</c> result, but in a simpler form.
        /// </summary>
        /// <returns>The user readable.</returns>
        public override string MakeUserReadable()
        {
            string p1 = Expression.Parenthesize(this,
                                                Expression1,
                                                Expression1.MakeUserReadable());
            string p2 = Expression.Parenthesize(this,
                                                Expression2,
                                                Expression2.MakeUserReadable());
            switch(Operator) {
            case Petri.Code.Operator.Name.SelectionRef:
                return p1 + "." + p2;
            case Petri.Code.Operator.Name.SelectionPtr:
                return p1 + "->" + p2;
            case Petri.Code.Operator.Name.Mult:
                return p1 + " * " + p2;
            case Petri.Code.Operator.Name.Div:
                return p1 + " / " + p2;
            case Petri.Code.Operator.Name.Mod:
                return p1 + " % " + p2;
            case Petri.Code.Operator.Name.Plus:
                return p1 + " + " + p2;
            case Petri.Code.Operator.Name.Minus:
                return p1 + " - " + p2;
            case Petri.Code.Operator.Name.ShiftLeft:
                return p1 + " << " + p2;
            case Petri.Code.Operator.Name.ShiftRight:
                return p1 + " >> " + p2;
            case Petri.Code.Operator.Name.Less:
                return p1 + " < " + p2;
            case Petri.Code.Operator.Name.LessEqual:
                return p1 + " <= " + p2;
            case Petri.Code.Operator.Name.Greater:
                return p1 + " > " + p2;
            case Petri.Code.Operator.Name.GreaterEqual:
                return p1 + " >= " + p2;
            case Petri.Code.Operator.Name.Equal:
                return p1 + " == " + p2;
            case Petri.Code.Operator.Name.NotEqual:
                return p1 + " != " + p2;
            case Petri.Code.Operator.Name.BitwiseAnd:
                return p1 + " & " + p2;
            case Petri.Code.Operator.Name.BitwiseXor:
                return p1 + " ^ " + p2;
            case Petri.Code.Operator.Name.BitwiseOr:
                return p1 + " | " + p2;
            case Petri.Code.Operator.Name.LogicalAnd:
                return p1 + " && " + p2;
            case Petri.Code.Operator.Name.LogicalOr:
                return p1 + " || " + p2;
            case Petri.Code.Operator.Name.Assignment:
                return p1 + " = " + p2;
            case Petri.Code.Operator.Name.PlusAssign:
                return p1 + " += " + p2;
            case Petri.Code.Operator.Name.MinusAssign:
                return p1 + " -= " + p2;
            case Petri.Code.Operator.Name.MultAssign:
                return p1 + " *= " + p2;
            case Petri.Code.Operator.Name.DivAssign:
                return p1 + " /= " + p2;
            case Petri.Code.Operator.Name.ModAssign:
                return p1 + " %= " + p2;
            case Petri.Code.Operator.Name.ShiftLeftAssign:
                return p1 + " <<= " + p2;
            case Petri.Code.Operator.Name.ShiftRightAssign:
                return p1 + " >>= " + p2;
            case Petri.Code.Operator.Name.BitwiseAndAssig:
                return p1 + " &= " + p2;
            case Petri.Code.Operator.Name.BitwiseXorAssign:
                return p1 + " ^= " + p2;
            case Petri.Code.Operator.Name.BitwiseOrAssign:
                return p1 + " |= " + p2;
            case Petri.Code.Operator.Name.Comma:
                return p1 + ", " + p2;
            }
            throw new Exception(Application.Configuration.GetLocalized("Operator not implemented!"));
        }

        /// <summary>
        /// Gets all the literals used to construct this expression.
        /// </summary>
        /// <returns>The literals.</returns>
        public override List<LiteralExpression> GetLiterals()
        {
            var l1 = Expression1.GetLiterals();
            var l2 = Expression2.GetLiterals();
            l1.AddRange(l2);
            return l1;
        }

    }
}

