//
//  FunctionInvocation.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-24.
//

using System;
using System.Collections.Generic;

namespace Petri.Code
{
    /// <summary>
    /// Function invocation.
    /// </summary>
    public class FunctionInvocation : Expression
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Code.FunctionInvocation"/> class.
        /// </summary>
        /// <param name="function">The function that is called.</param>
        /// <param name="arguments">The arguments to pass to the function.</param>
        public FunctionInvocation(Function function,
                                  params Expression[] arguments) : base(function.Language,
                                                                        Code.Operator.Name.FunCall)
        {
            if(!function.IsVariadic && arguments.Length != function.Parameters.Count) {
                throw new Exception(Application.Configuration.GetLocalized("Invalid arguments count."));
            }

            Arguments = new List<Expression>();
            foreach(var arg in arguments) {
                var a = arg;
                if(a.MakeUserReadable() == "") {
                    a = LiteralExpression.CreateFromString("void", Language);
                }

                Arguments.Add(a);
            }

            // TODO: Perform type verification here
            Function = function;
        }

        /// <summary>
        /// Gets the arguments that are passed as the function parameters.
        /// </summary>
        /// <value>The arguments.</value>
        public List<Expression> Arguments {
            get;
            private set;
        }

        /// <summary>
        /// Gets the function that is being called.
        /// </summary>
        /// <value>The function.</value>
        public Function Function {
            get;
            private set;
        }

        /// <summary>
        /// Checks whether this instance uses the specified function as part of one of its subexpression or itself.
        /// </summary>
        /// <returns>true</returns>
        /// <c>false</c>
        /// <param name="f">The function to test.</param>
        public override bool UsesFunction(Function f)
        {
            bool res = false;
            res = res || Function == f;
            foreach(var e in Arguments) {
                res = res || e.UsesFunction(f);
            }

            return res;
        }

        /// <summary>
        /// Generates the source code corresponding to the expression.
        /// </summary>
        /// <returns>The code.</returns>
        public override string MakeCode()
        {
            string args = "";
            for(int i = 0; i < Function.Parameters.Count; ++i) {
                if(i > 0) {
                    args += ", ";
                }

                var code = Arguments[i].MakeCode();
                bool cast = Language != Language.Python
                                && !(Function.Parameters[i].Type.Equals(Type.UnknownType(Language)));

                switch(Language) {
                case Language.Cpp:
                    if(cast) {
                        code = "static_cast<" + Function.Parameters[i].Type + ">(" + code + ")";
                    }
                    break;
                case Language.C:
                case Language.CSharp:
                case Language.Python:
                case Language.EmbeddedC:
                    if(cast) {
                        code = "(" + Function.Parameters[i].Type + ")(" + code + ")";
                    }
                    break;
                default:
                    throw new Exception("FunctionInvoction.MakeCode: Should not get there!");
                }

                args += code;
            }

            string template = "";
            if(Function.Template) {
                template = "<" + Function.TemplateArguments + ">";
            }

            return Function.QualifiedName + template + "(" + args + ")";
        }

        /// <summary>
        /// Generates a user-readable form of the expression.
        /// </summary>
        /// <returns>The user readable.</returns>
        public override string MakeUserReadable()
        {
            string args = "";
            foreach(var arg in Arguments) {
                if(args.Length > 0)
                    args += ", ";
                args += arg.MakeUserReadable();
            }

            return Function.QualifiedName + "(" + args + ")";
        }

        /// <summary>
        /// Gets all the literals used to construct this expression.
        /// </summary>
        /// <returns>The literals.</returns>
        public override List<LiteralExpression> GetLiterals()
        {
            var l1 = new List<LiteralExpression>();
            foreach(var e in Arguments) {
                var l2 = e.GetLiterals();
                l1.AddRange(l2);
            }

            return l1;
        }
    }

    /// <summary>
    /// Method invocation.
    /// </summary>
    public class MethodInvocation : FunctionInvocation
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Code.MethodInvocation"/> class.
        /// </summary>
        /// <param name="function">The method that is being called.</param>
        /// <param name="that">The 'this' object the method is called upon.</param>
        /// <param name="indirection">If set to <c>true</c>, then 'this' is considered as a pointer (C++ only). Otherwise, 'this' is used as a reference.</param>
        /// <param name="arguments">The arguments to pass to the function.</param>
        public MethodInvocation(Method function,
                                Expression that,
                                bool indirection,
                                params Expression[] arguments) : base(function,
                                                                      arguments)
        {
            This = that;
            Indirection = indirection;
        }

        /// <summary>
        /// Gets the 'this' object of the method invocation.
        /// </summary>
        /// <value>The this object.</value>
        public Expression This {
            get;
            private set;
        }

        /// <summary>
        /// Gets the typical name for "this" object of the method.
        /// </summary>
        /// <value>The name of the method object.</value>
        public string MethodObjectName {
            get {
                if(This.Language == Language.Python) {
                    return "self";
                } else if(This.Language == Language.CSharp) {
                    return "this";
                } else if(This.Language == Language.Cpp) {
                    return "*this";
                }

                throw new Exception("MethodInvocation.MethodObjectName: Should not get there!");
            }
        }

        /// <summary>
        /// Gets a value indicating whether the 'this' object is to be considered as a pointer or not (C++ only).
        /// </summary>
        /// <value><c>true</c> if indirection; otherwise, <c>false</c>.</value>
        public bool Indirection {
            get;
            private set;
        }

        /// <summary>
        /// Generates a user-readable form of the expression.
        /// </summary>
        /// <returns>The user readable.</returns>
        public override string MakeUserReadable()
        {
            string args = "";
            foreach(var arg in Arguments) {
                if(args.Length > 0)
                    args += ", ";
                args += arg.MakeUserReadable();
            }

            return This.MakeUserReadable() + (Indirection ? "->" : ".") + Function.Name + "(" + args + ")";
        }

        /// <summary>
        /// Gets all the literals used to construct this expression.
        /// </summary>
        /// <returns>The literals.</returns>
        public override List<LiteralExpression> GetLiterals()
        {
            var l1 = base.GetLiterals();
            l1.AddRange(This.GetLiterals());

            return l1;
        }
    }

    /// <summary>
    /// This class wraps an expression into a function call. For instance, an Action can have the expression ++$i attached to it, wrapped into a function for the runtime.
    /// </summary>
    public class WrapperFunctionInvocation : FunctionInvocation
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Code.WrapperFunctionInvocation"/> class.
        /// </summary>
        /// <param name="returnType">The return type of the function invocation.</param>
        /// <param name="expr">The expression to wrap.</param>
        /// <param name="error">The error the wrapper was created with.</param>
        public WrapperFunctionInvocation(Type returnType,
                                         Expression expr,
                                         string error = null) : base(GetWrapperFunction(returnType),
                                                                     expr)
        {
            Error = error;
        }

        /// <summary>
        /// Gets the wrapper function.
        /// </summary>
        /// <returns>The wrapper function.</returns>
        /// <param name="returnType">The return type of the function invocation.</param>
        static Function GetWrapperFunction(Type returnType)
        {
            var f = new Function(returnType,
                                 Scope.MakeFromNamespace(returnType.Language, "Utility"),
                                 "");
            f.AddParam(new Param(new Type(returnType.Language, "void"), "param"));
            return f;
        }

        /// <summary>
        /// Generates the source code corresponding to the expression.
        /// </summary>
        /// <returns>The code.</returns>
        public override string MakeCode()
        {
            if(Language == Language.C) {
                return "(" + Arguments[0].MakeCode() + ", PetriUtility_doNothing())";
            } else if(Language == Language.EmbeddedC) {
                return "(" + Arguments[0].MakeCode() + ", Petri_doNothing())";
            } else if(Language == Language.Cpp) {
                return "([&_PETRI_PRIVATE_GET_VARIABLES_]() { " + Arguments[0].MakeCode() + "; return (" + Function.ReturnType.Name + ")Petri::Utility::doNothing(); })()";
            } else if(Language == Language.CSharp) {
                return "new System.Func<ActionResult>(() => { " + Arguments[0].MakeCode() + "; return (" + Function.ReturnType + ")Petri.Runtime.Utility.DoNothing(); }).Invoke()";
            } else if(Language == Language.Python) {
                return "(eval(compile(\"" + Arguments[0].MakeCode().Escape() + "\", '', 'exec')), PetriUtility_doNothing())[-1]";
            }


            throw new Exception("WrapperFunctionInvocation.MakeCode: Should not get there!");
        }

        /// <summary>
        /// Generates a user-readable form of the expression.
        /// </summary>
        /// <returns>The user readable.</returns>
        public override string MakeUserReadable()
        {
            return Arguments[0].MakeUserReadable();
        }

        /// <summary>
        /// Gets the error the wrapper was created with, or null if none.
        /// </summary>
        /// <value>The error.</value>
        public string Error {
            get;
            private set;
        }
    }
}
