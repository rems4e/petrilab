﻿//
//  PathUtility.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2018-05-01.
//

using System;
using System.IO;
using System.Runtime.InteropServices;

namespace Petri.Application
{
    /// <summary>
    /// Some filesystem path utilities.
    /// </summary>
    public static class PathUtility
    {
        [DllImport("libc")]
        static extern IntPtr realpath([MarshalAs(UnmanagedType.LPTStr)] string path, IntPtr resolved_path);

        [DllImport("libc")]
        static extern void free(IntPtr ptr);

        /// <summary>
        /// Calls realpath(3) from C stlib with the path as its first argument and NULL as its second argument.
        /// </summary>
        /// <returns>The get.</returns>
        /// <param name="path">Path.</param>
        public static string RealPath(string path)
        {
            if(File.Exists(path) || Directory.Exists(path)) {
                var result = realpath(path, IntPtr.Zero);
                if(result == IntPtr.Zero) {
                    throw new IOException(string.Format("Could not resolve the given path: '{0}'", path));
                }
                var str = Marshal.PtrToStringAuto(result);
                free(result);
                return str;
            } else {
                var parent = Directory.GetParent(path)?.FullName ?? null;
                if(parent != null) {
                    var result = RealPath(parent);
                    if(!result.EndsWithInv(Path.DirectorySeparatorChar.ToString())) {
                        result = result + Path.DirectorySeparatorChar;
                    }
                    return result + Path.GetFileName(path);
                }
            }

            return path;
        }

        /// <summary>
        /// Gets the path of <paramref name="file"/> relative to <paramref name="folder"/>.
        /// </summary>
        /// <returns>The relative path.</returns>
        /// <param name="file">File.</param>
        /// <param name="folder">Folder.</param>
        public static string GetRelativePath(string file, string folder)
        {
            file = RealPath(file);
            folder = RealPath(folder);
            var fileUri = new Uri("file://" + file);
            if(!folder.EndsWithInv(Path.DirectorySeparatorChar.ToString())) {
                folder += Path.DirectorySeparatorChar;
            }
            var folderUri = new Uri("file://" + folder);
            return Uri.UnescapeDataString(folderUri.MakeRelativeUri(fileUri).ToString().Replace('/', Path.DirectorySeparatorChar));
        }

        /// <summary>
        /// Returns an absolute path and resolves symlinks.
        /// </summary>
        /// <returns>The full path.</returns>
        /// <param name="path">Path.</param>
        public static string GetFullPath(string path)
        {
            return RealPath(Path.GetFullPath(path));
        }

    }
}
