//
//  CLikePetriGen.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-05-07.
//

using System;
using System.Collections.Generic;

using Petri.Code;
using Petri.Model;

using Action = Petri.Model.Action;

namespace Petri.CodeGen
{
    /// <summary>
    /// A PetriGen that generates code for a C, C++ or C# petri net.
    /// </summary>
    public abstract class CLikePetriGen : ConcurrentRuntimePetriGen
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.CodeGen.CLikePetriGen"/> class.
        /// </summary>
        /// <param name="doc">Document.</param>
        /// <param name="language">The language.</param>
        /// <param name="headerGen">An optional header generator.</param>
        protected CLikePetriGen(Application.Document doc,
                                Language language,
                                CodeGen headerGen = null) : base(doc,
                                                          language,
                                                          new CLikeCodeGen(language))
        {
            _functionBodies = new CLikeCodeGen(language);
            _headerGen = headerGen ?? new CLikeCodeGen(language);
        }

        /// <summary>
        /// Generates the code for the petri net of the document.
        /// </summary>
        public override void WritePetriNet()
        {
            base.WritePetriNet();

            string path = PathToFile(Document.Settings.Name + PetriGen.HeaderExtensionFromLanguage(_headerGen.Language));

            string headerCode = GetGeneratedHeader();
            if(System.IO.File.Exists(path)) {
                string existing = System.IO.File.ReadAllText(path);
                if(existing.Length > 1 && existing.Substring(0, existing.Length - 1) == headerCode) {
                    return;
                }
            }

            System.IO.File.WriteAllText(path, _headerGen.Value);
        }

        /// <summary>
        /// Gets the content of the generated header source file.
        /// </summary>
        /// <returns>The header's content.</returns>
        public string GetGeneratedHeader()
        {
            // Codegen
            GetGeneratedCode();

            return _headerGen.Value;
        }

        /// <summary>
        /// Appends the functions' bodies to the generated code.
        /// </summary>
        protected void AppendFunctions()
        {
            var keys = new List<UInt64>(CodeRanges.Keys);
            foreach(var key in keys) {
                var range = CodeRanges[key];
                range.FirstLine += _linesToFunctions;
                range.LastLine += _linesToFunctions;
                CodeRanges[key] = range;
            }

            // We add the functions bodies to the generated code.
            CodeGen.Value = CodeGen.Value.Substring(0, _functionsIndex) + _functionBodies.Value + "\n" + CodeGen.Value.Substring(_functionsIndex);
        }

        /// <summary>
        /// Casts an enum member to the petri net runtime's action result type.
        /// </summary>
        /// <returns>The enum member.</returns>
        /// <param name="member">Member.</param>
        /// <param name="actualCast">Whether to perform a cast.</param>
        protected abstract string CastEnumMember(string member, bool actualCast);

        /// <summary>
        /// Makes the action code.
        /// </summary>
        /// <param name="a">The action.</param>
        protected abstract void MakeActionCode(Action a);

        /// <summary>
        /// Makes the transition code.
        /// </summary>
        /// <param name="t">The transition.</param>
        protected abstract void MakeTransitionCode(Transition t);

        /// <summary>
        /// Generates the code for importing an external petri net.
        /// </summary>
        /// <param name="pn">The petri net to import.</param>
        protected abstract void MakeExternalPetriNetCode(ExternalInnerPetriNetProxy pn);

        /// <summary>
        /// Generates the code for an action.
        /// </summary>
        /// <param name="a">Action.</param>
        protected override void GenerateAction(Action a)
        {
            var old = new Dictionary<LiteralExpression, string>();

            var litterals = a.Invocation.GetLiterals();
            foreach(LiteralExpression le in litterals) {
                if(le.Expression == "$Name") {
                    old.Add(le, le.Expression);
                    le.Expression = "\"" + a.Name + "\"";
                } else if(le.Expression == "$ID") {
                    old.Add(le, le.Expression);
                    le.Expression = a.ID.ToString();
                } else {
                    foreach(string e in Document.Settings.Enum.Members) {
                        if(le.Expression == e) {
                            old.Add(le, le.Expression);
                            le.Expression = CastEnumMember(le.Expression, true);
                        }
                    }
                }
            }

            MakeActionCode(a);

            foreach(var tup in old) {
                tup.Key.Expression = tup.Value;
            }
        }

        /// <summary>
        /// Generates the code for a transition.
        /// </summary>
        /// <param name="t">Transition.</param>
        protected override void GenerateTransition(Transition t)
        {
            var old = new Dictionary<LiteralExpression, string>();

            foreach(LiteralExpression le in t.Condition.GetLiterals()) {
                if(le.Expression == "$Res" || le.Expression == "$Result") {
                    old.Add(le, le.Expression);
                    le.Expression = "_PETRI_PRIVATE_GET_ACTION_RESULT_";
                } else if(le.Expression == "$Name") {
                    old.Add(le, le.Expression);
                    le.Expression = "\"" + t.Name + "\"";
                } else if(le.Expression == "$ID") {
                    old.Add(le, le.Expression);
                    le.Expression = t.ID.ToString();
                } else {
                    foreach(string e in Document.Settings.Enum.Members) {
                        if(le.Expression == e) {
                            old.Add(le, le.Expression);
                            le.Expression = CastEnumMember(e, false);
                        }
                    }
                }
            }

            MakeTransitionCode(t);

            foreach(var tup in old) {
                tup.Key.Expression = tup.Value;
            }
        }

        /// <summary>
        /// Generates the code for importing an external petri net.
        /// </summary>
        /// <param name="pn">The petri net to import.</param>
        protected override void GenerateExternalPetriNet(ExternalInnerPetriNetProxy pn)
        {
            MakeExternalPetriNetCode(pn);
        }

        /// <summary>
        /// The code generated for function bodies.
        /// </summary>
        protected CodeGen _functionBodies;

        /// <summary>
        /// The index where to insert the functions bodies.
        /// </summary>
        protected int _functionsIndex;

        /// <summary>
        /// The number of lines where to insert functions bodies.
        /// </summary>
        protected int _linesToFunctions;

        /// <summary>
        /// The code generator for the generated code's header.
        /// </summary>
        protected CodeGen _headerGen;
    }
}
