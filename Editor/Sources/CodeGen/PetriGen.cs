//
//  PetriGen.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2015-06-29.
//

using System;
using System.Collections.Generic;

using Petri.Code;

namespace Petri.CodeGen
{
    /// <summary>
    /// A simple struct that represents a code range by its first and last lines.
    /// </summary>
    public struct CodeRange
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.CodeGen.CodeRange"/> struct.
        /// </summary>
        /// <param name="first">The start of the range.</param>
        /// <param name="last">The end of the range.</param>
        public CodeRange(int first, int last)
        {
            FirstLine = first;
            LastLine = last;
        }

        /// <summary>
        /// Gets or sets the first line of the range.
        /// </summary>
        /// <value>The first line.</value>
        public int FirstLine {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the last line of the range.
        /// </summary>
        /// <value>The last line.</value>
        public int LastLine {
            get;
            set;
        }
    }

    /// <summary>
    /// A class allowing for the generation of a petri net's source code.
    /// </summary>
    public abstract class PetriGen
    {
        /// <summary>
        /// Creates a PetriGen instance from the given document
        /// </summary>
        /// <returns>The code generator.</returns>
        /// <param name="document">Document.</param>
        public static PetriGen PetriGenForDoc(Application.Document document)
        {
            if(document.Settings.Language == Language.Cpp) {
                return new CppPetriGen(document);
            } else if(document.Settings.Language == Language.C) {
                return new CPetriGen(document);
            } else if(document.Settings.Language == Language.CSharp) {
                return new CSharpPetriGen(document);
            } else if(document.Settings.Language == Language.Python) {
                return new PythonPetriGen(document);
            } else if(document.Settings.Language == Language.EmbeddedC) {
                return new EmbeddedPetriGen(document);
            }

            throw new Exception("Unsupported language: " + document.Settings.Language);
        }

        /// <summary>
        /// Gets a the canonical source file extension for the provided language
        /// </summary>
        /// <returns>The extension from the language.</returns>
        /// <param name="language">Language.</param>
        public static string SourceExtensionFromLanguage(Language language)
        {
            if(language == Language.Cpp) {
                return ".cpp";
            } else if(language == Language.C || language == Language.EmbeddedC || language == Language.Python) {
                return ".c";
            } else if(language == Language.CSharp) {
                return "_lib.cs";
            }

            throw new Exception("Unsupported language: " + language);
        }

        /// <summary>
        /// Gets a the canonical source file header for the provided language
        /// </summary>
        /// <returns>The extension from the language.</returns>
        /// <param name="language">Language.</param>
        public static string HeaderExtensionFromLanguage(Language language)
        {
            if(language == Language.Cpp || language == Language.C || language == Language.EmbeddedC) {
                return ".h";
            } else if(language == Language.CSharp) {
                return ".cs";
            } else if(language == Language.Python) {
                return "_py.py";
            }

            throw new Exception("Unsupported language: " + language);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.CodeGen.PetriGen"/> class.
        /// </summary>
        /// <param name="doc">Document.</param>
        /// <param name="language">Language.</param>
        /// <param name="generator">Generator.</param>
        protected PetriGen(Application.Document doc, Language language, CodeGen generator)
        {
            CodeGen = generator;
            Document = doc;
            Profile = doc.Settings.MatchedProfile;
            Language = language;
            CodeRanges = new Dictionary<UInt64, CodeRange>();
        }

        /// <summary>
        /// Gets the absolute path of a file from its path relative to the source output path.
        /// </summary>
        /// <returns>The path to the file.</returns>
        /// <param name="filename">Filename.</param>
        protected string PathToFile(string filename)
        {
            return System.IO.Path.Combine(
                System.IO.Path.Combine(
                    System.IO.Directory.GetParent(Document.Path).FullName,
                    Profile.RelativeSourceOutputPath
                ),
                filename
            );
        }

        /// <summary>
        /// Gets the language the petri net code generator has been configured with.
        /// </summary>
        /// <value>The language.</value>
        public Language Language {
            get;
            private set;
        }

        /// <summary>
        /// Gets the document.
        /// </summary>
        /// <value>The document.</value>
        protected Application.Document Document {
            get;
            private set;
        }

        /// <summary>
        /// Gets the settings profile.
        /// </summary>
        /// <value>The profile.</value>
        protected Application.DocumentSettingsProfile Profile {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets the underlying code generator.
        /// </summary>
        /// <value>The code gen.</value>
        protected CodeGen CodeGen {
            get;
            set;
        }

        /// <summary>
        /// Gets the code ranges.
        /// </summary>
        /// <value>The code ranges.</value>
        public Dictionary<UInt64, CodeRange> CodeRanges {
            get;
            private set;
        }

        /// <summary>
        /// Gets the hash of the petri net by generating the code and discarding it.
        /// </summary>
        /// <returns>The hash.</returns>
        public abstract string GetHash();

        /// <summary>
        /// Generates the code for the petri net of the document.
        /// </summary>
        public abstract void WritePetriNet();


        /// <summary>
        /// Writes the expression evaluator code corresponding to the given expression to the given path.
        /// </summary>
        /// <param name="expression">Expression.</param>
        /// <param name="path">Path.</param>
        /// <param name="userData">Additional and optional user data that will be used to generate the code.</param>
        public abstract void WriteExpressionEvaluator(Expression expression,
                                                      string path,
                                                      params object[] userData);

        /// <summary>
        /// Gets the petri net's generated source code.
        /// </summary>
        /// <returns>The generated petri net source code.</returns>
        public abstract string GetGeneratedCode();

        /// <summary>
        /// Gets the headers to put in the generated source files.
        /// </summary>
        /// <value>The source headers.</value>
        public string[] SourceHeaders {
            get {
                return new string[] { "Generated by PetriLab - " + Application.Application.WebSiteInfo,
                                      "Version " + Application.Application.Version
                };
            }
        }
    }
}

