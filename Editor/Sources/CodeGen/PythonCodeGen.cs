//
//  PythonCodeGen.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2014-12-09.
//

namespace Petri.CodeGen
{
    /// <summary>
    /// A CodeGen specialized for the Python programming language.
    /// </summary>
    public class PythonCodeGen : CodeGen
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.CodeGen.PythonCodeGen"/> class.
        /// </summary>
        public PythonCodeGen()
        {
            _value = new System.Text.StringBuilder();
        }

        /// <summary>
        /// Gets the programming language of the generated code.
        /// </summary>
        /// <value>The language.</value>
        public override Code.Language Language {
            get {
                return Code.Language.Python;
            }
        }

        /// <summary>
        /// Gets or sets the generated code.
        /// </summary>
        /// <value>The value.</value>
        public override string Value {
            get {
                return _value.ToString();
            }
            set {
                _value.Clear();
                _value.Append(value);
            }
        }

        /// <summary>
        /// Formats the generated source code.
        /// </summary>
        public override void Format()
        {
        }

        /// <summary>
        /// Pops the scope.
        /// </summary>
        public void PopScope()
        {
            _scopeIndicator = _scopeIndicator.Substring(0, _scopeIndicator.Length - 4);
        }

        /// <summary>
        /// This method is responsible for the actual code addition.
        /// </summary>
        /// <param name="line">The new line of code.</param>
        protected override void AddInternal(string line)
        {
            _value.Append(_scopeIndicator + line);
            if(line.EndsWithInv(":\n")) {
                _scopeIndicator += "    ";
            }
        }

        System.Text.StringBuilder _value;
        string _scopeIndicator = "";
    }
}

