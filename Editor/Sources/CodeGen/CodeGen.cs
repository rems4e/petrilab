//
//  CodeGen.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2014-12-09.
//

namespace Petri.CodeGen
{
    /// <summary>
    /// Generated code wrapper.
    /// </summary>
    public abstract class CodeGen
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.CodeGen.CodeGen"/> class.
        /// </summary>
        protected CodeGen()
        {
            LineCount = 0;
        }

        /// <summary>
        /// Gets the programming language of the generated code.
        /// </summary>
        /// <value>The language.</value>
        public abstract Code.Language Language {
            get;
        }

        /// <summary>
        /// Gets or sets the generated code.
        /// </summary>
        /// <value>The value.</value>
        public abstract string Value {
            get;
            set;
        }

        /// <summary>
        /// Gets a value indicating whether this <see cref="T:Petri.CodeGen.CodeGen"/> contains no code.
        /// </summary>
        /// <value><c>true</c> if empty; otherwise, <c>false</c>.</value>
        public bool Empty {
            get {
                return Value == "";
            }
        }

        /// <summary>
        /// Formats the generated source code.
        /// </summary>
        public abstract void Format();

        /// <summary>
        /// Adds new code to the generated code.
        /// </summary>
        /// <param name="line">Line.</param>
        public void Add(string line)
        {
            if(line != null) {
                AddInternal(line);
                foreach(char c in line) {
                    if(c == '\n') {
                        ++LineCount;
                    }
                }
            }
        }

        /// <summary>
        /// Adds new code followed by a new line.
        /// </summary>
        /// <param name="line">Line.</param>
        public void AddLine(string line = "")
        {
            if(line != null) {
                Add(line + '\n');
            }
        }

        /// <param name="gen">The CodeGen instance to add code to.</param>
        /// <param name="s">The code to add to the generator, followed by a new line.</param>
        public static CodeGen operator +(CodeGen gen, string s)
        {
            gen.AddLine(s);
            return gen;
        }

        /// <summary>
        /// Gets the current line count.
        /// </summary>
        /// <value>The line count.</value>
        public int LineCount {
            get;
            private set;
        }

        /// <summary>
        /// This method is responsible for the actual code addition.
        /// </summary>
        /// <param name="line">The new line of code.</param>
        protected abstract void AddInternal(string line);
    }
}

