//
//  CFamilyPetriGen.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-05-07.
//

using Petri.Code;

namespace Petri.CodeGen
{
    /// <summary>
    /// A PetriGen that generates code for a C or C++ petri net.
    /// </summary>
    public abstract class CFamilyPetriGen : CLikePetriGen
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.CodeGen.CFamilyPetriGen"/> class.
        /// </summary>
        /// <param name="doc">Document.</param>
        /// <param name="language">Language.</param>
        protected CFamilyPetriGen(Application.Document doc, Language language) : base(doc, language)
        {
        }

        /// <summary>
        /// Appends the required headers to the generated code.
        /// </summary>
        /// <param name="generator">Generator.</param>
        protected void AppendHeaders(CodeGen generator)
        {
            if(Document.Path != "") {
                var p2 = Application.PathUtility.GetFullPath(
                    System.IO.Path.Combine(
                        System.IO.Directory.GetParent(Document.Path).FullName,
                        Profile.RelativeSourceOutputPath
                    )
                );
                foreach(var s in Document.Settings.Headers) {
                    var p1 = System.IO.Path.Combine(
                        System.IO.Directory.GetParent(Document.Path).FullName,
                        s
                    );
                    generator += "#include \"" + Application.PathUtility.GetRelativePath(p1, p2) + "\"";
                }
            }
        }
    }
}
