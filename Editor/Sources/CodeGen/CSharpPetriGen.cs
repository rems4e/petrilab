//
//  CSharpPetriGen.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-17.
//

using System.Collections.Generic;
using System.Linq;

using Petri.Code;
using Petri.Model;

namespace Petri.CodeGen
{
    /// <summary>
    /// A PetriGen that generates code for a C# petri net.
    /// </summary>
    public class CSharpPetriGen : CLikePetriGen
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.CodeGen.CSharpPetriGen"/> class.
        /// </summary>
        /// <param name="doc">Document.</param>
        public CSharpPetriGen(Application.Document doc) : base(doc, Language.CSharp)
        {
        }

        /// <summary>
        /// Writes the expression evaluator code corresponding to the given expression to the given path.
        /// </summary>
        /// <param name="expression">Expression.</param>
        /// <param name="path">Path.</param>
        /// <param name="userData">Additional and optional user data that will be used to generate the code.</param>
        public override void WriteExpressionEvaluator(Expression expression,
                                                      string path,
                                                      params object[] userData)
        {
            string expr;
            if(expression is ExpressionList) {
                expr = string.Join(" + '\\n' + ", (from e in ((ExpressionList)expression).Expressions select "(" + e.MakeCode() + ").ToString()"));
            } else {
                expr = "(" + expression.MakeCode() + ").ToString()";
            }

            CodeGen generator = new CLikeCodeGen(Language.CSharp);

            generator += "using Petri.Runtime;";
            generator += "using System;";

            generator += "namespace Petri.Generated {";
            generator += "public class " + Document.RootDocument.CodePrefix + "Evaluator : MarshalByRefObject, Petri.Runtime.IEvaluator {";
            generator += GenerateVarEnum();
            generator += "public string Evaluate(Petri.Runtime.VarSlot _PETRI_PRIVATE_GET_VARIABLES_) {";
            generator += "return " + expr + ";";
            generator += "}";
            generator += "}";
            generator += "}\n";

            System.IO.File.WriteAllText(path, generator.Value);
        }

        /// <summary>
        /// Called before any entity generation and gives the opportunity to emit some setup code.
        /// </summary>
        protected override void Begin()
        {
            CodeGen += "/*";
            foreach(var h in SourceHeaders) {
                CodeGen += " * " + h;
            }
            CodeGen += " */";
            CodeGen.AddLine();

            CodeGen += "using System;";
            CodeGen += "using Petri.Runtime;";
            CodeGen += "using PNAction = Petri.Runtime.Action;";

            CodeGen.AddLine();

            CodeGen += "namespace Petri.Generated\n{";
            CodeGen += "public partial class " + ClassName + " : Petri.Runtime.CSharpGeneratedDynamicLib\n{";
            CodeGen += GenerateVarEnum();

            CodeGen += "public " + ClassName + "()";
            CodeGen += "{";
            CodeGen += "_lib = new DynamicLib(Create, CreateDebug, Evaluate, Hash, \"" + Document.CodePrefix + "\", Port);";
            CodeGen += "}\n";

            foreach(var pn in UniqueExternalPetriNets) {
                CodeGen += GenerateParamEnum(pn.Parameters, pn.ClassName);
            }

            CodeGen += "static void FillVariables(VarSlot variables)\n{";
            foreach(var v in Document.FlatPetriNet.Variables) {
                CodeGen += "variables[" + v.Key.EnumeratorName + "].Name = \"" + v.Key.MakeUserReadable() + "\";";
                CodeGen += "variables[" + v.Key.EnumeratorName + "].DefaultValue = " + v.Value + ";";
            }
            CodeGen += "}";
            CodeGen.AddLine();

            _functionsIndex = CodeGen.Value.Length;
            _linesToFunctions = CodeGen.LineCount;

            CodeGen += "public static Tuple<PNAction, PNAction> Fill(PetriNet petriNet, UInt64 entitiesOffset, bool firstLevel, ParametrizedActionCallableDel initEntryPtr, ParametrizedActionCallableDel exitActionPtr) {";
        }

        /// <summary>
        /// Finishes the code generation and compute the Hash value of the petri net.
        /// </summary>
        protected override void End()
        {
            AppendFunctions();

            CodeGen += "\nreturn Tuple.Create(state_0, " + Document.FlatPetriNet.ExitPoint.CodeIdentifier + ");";
            CodeGen += "}\n"; // Fill()

            var verbosity = "PetriNet.LogVerbosity." + Document.Settings.DefaultExecutionVerbosity.ToString().Replace(" ", "").Replace(",", " | PetriNet.LogVerbosity.");
            CodeGen += "static partial void CreatePetriNet(ref PetriNet petriNet, bool debug) {";
            CodeGen += "if(debug) {";
            CodeGen += "petriNet = new PetriDebug(\"" + ClassName + "\", " + Document.FlatPetriNet.Variables.Count + ");";
            CodeGen += "} else {";
            CodeGen += "petriNet = new PetriDebug(\"" + ClassName + "\", " + Document.FlatPetriNet.Variables.Count + ");";
            CodeGen += "}"; // if(debug)
            CodeGen += "petriNet.SetLogVerbosity(" + verbosity + ");";
            if(Profile.MaxConcurrency != null) {
                CodeGen += "petriNet.MaxConcurrency = " + Profile.MaxConcurrency.Value + ";";
            }
            CodeGen += "Fill(petriNet, 0, true, EntryInit, ExitAction);";
            CodeGen += "FillVariables(petriNet.Variables);";
            CodeGen += "}";

            string toHash = CodeGen.Value;
            var sha = new System.Security.Cryptography.SHA256CryptoServiceProvider();
            Hash = System.BitConverter.ToString(
                sha.ComputeHash(System.Text.Encoding.UTF8.GetBytes(toHash))
            ).Replace("-", "");

            CodeGen += "";

            CodeGen += "static PetriNet Create() {";
            CodeGen += "PetriNet petriNet = null;";
            CodeGen += "CreatePetriNet(ref petriNet, false);";
            CodeGen += "return petriNet;";
            CodeGen += "}"; // create()

            CodeGen += "";

            CodeGen += "static PetriDebug CreateDebug() {";
            CodeGen += "PetriNet petriNet = null;";
            CodeGen += "CreatePetriNet(ref petriNet, true);";
            CodeGen += "return (PetriDebug)petriNet;";
            CodeGen += "}"; // createDebug()

            CodeGen += "";

            CodeGen += "static UInt16 Port {\nget {\nreturn " + Profile.DebugPort + ";\n}\n}";

            CodeGen += "";

            CodeGen += "static string Evaluate(IntPtr vars, string libName) {";
            CodeGen += "try {";
            CodeGen += "var evLibProxy = new CSharpGeneratedDynamicLibProxy(System.IO.Directory.GetParent(libName).FullName, System.IO.Path.GetFileName(libName), \"" + Document.CodePrefix + "Evaluator\");";
            CodeGen += "var evDylib = evLibProxy.Load<Runtime.IEvaluator>();";

            CodeGen += "if(evDylib == null) {";
            CodeGen += "throw new Exception(\"Unable to load the evaluator!\");";
            CodeGen += "}";

            CodeGen += "string value = evDylib.Evaluate(new VarSlot(vars));";
            CodeGen += "evLibProxy.Unload();";

            CodeGen += "return value;";
            CodeGen += "} catch(Exception e) {";
            CodeGen += "return \"Evaluation error: \" + e.Message;";
            CodeGen += "}";
            CodeGen += "}";

            CodeGen += "";

            CodeGen += "static string Hash() {";
            CodeGen += "return \"" + Hash + "\";";
            CodeGen += "}";

            CodeGen += "}"; // class
            CodeGen += "}"; // namespace

            CodeGen.Format();

            _headerGen += "/*";
            foreach(var h in SourceHeaders) {
                _headerGen += " * " + h;
            }
            _headerGen += " */";
            _headerGen.AddLine();

            _headerGen += "using Petri.Runtime;";
            _headerGen += "using System;";

            _headerGen.AddLine();

            _headerGen += "namespace Petri.Generated";
            _headerGen += "{";
            _headerGen += "public partial class " + ClassName;
            _headerGen += "{";
            _headerGen += GenerateParamEnum(Document.FlatPetriNet.Parameters, null);

            _headerGen += "public static PetriNet CreatePetriNet()\n{";
            _headerGen += "PetriNet petriNet = null;";
            _headerGen += "CreatePetriNet(ref petriNet, false);";
            _headerGen += "return petriNet;";
            _headerGen += "}";

            _headerGen.AddLine();

            _headerGen += "public static PetriNet CreateDebugPetriNet()\n{";
            _headerGen += "PetriNet petriNet = null;";
            _headerGen += "CreatePetriNet(ref petriNet, true);";
            _headerGen += "return (PetriDebug)petriNet;";
            _headerGen += "}";

            _headerGen += "";
            _headerGen += "public static DynamicLib CreateLib(string customPath = \"\")";
            _headerGen += "{";
            _headerGen += "return new CSharpGeneratedDynamicLibProxy(customPath, \"" + ClassName + ".dll\", \"" + ClassName + "\").Load<IGeneratedDynamicLib>().Lib;";
            _headerGen += "}";

            _headerGen.AddLine();

            _headerGen += "static partial void CreatePetriNet(ref PetriNet petriNet, bool debug);";

            _headerGen += "}";
            _headerGen += "}";

            _headerGen.AddLine();

            _headerGen.Format();
        }

        /// <summary>
        /// Casts an enum member to the petri net runtime's action result type.
        /// </summary>
        /// <returns>The enum member.</returns>
        /// <param name="member">Member.</param>
        /// <param name="actualCast">Whether to perform a cast.</param>
        protected override string CastEnumMember(string member, bool actualCast)
        {
            var result = Document.Settings.Enum.Name + "." + member;
            if(actualCast) {
                result = "(Int32)(" + result + ")";
            }
            return result;
        }

        /// <summary>
        /// Generates the code for an action.
        /// </summary>
        /// <param name="a">Action.</param>
        protected override void MakeActionCode(Action a)
        {
            var code = "(Int32)(" + a.Invocation.MakeCode() + ")";

            var vars = new SortedSet<VariableExpression>();
            a.GetVariables(vars);

            CodeRange range = new CodeRange();
            range.FirstLine = _functionBodies.LineCount + 1;
            var funcName = a.IsStartState ? "EntryInit" : (a == Document.FlatPetriNet.ExitPoint ? "ExitAction" : a.CodeIdentifier + "_invocation");
            if(vars.Count == 0 && !a.IsStartState && a != Document.FlatPetriNet.ExitPoint) {
                _functionBodies += "static Int32 " + funcName + "() {";
            } else {
                _functionBodies += (a.IsStartState || (a == Document.FlatPetriNet.ExitPoint) ? "public " : "") + "static Int32 " + funcName + "(IntPtr _PETRI_PRIVATE_GET_VARIABLES_PTR_) {\nvar _PETRI_PRIVATE_GET_VARIABLES_ = new VarSlot(_PETRI_PRIVATE_GET_VARIABLES_PTR_);\n";
            }
            if(a.IsStartState) {
                _functionBodies += "if(!_PETRI_PRIVATE_GET_VARIABLES_.IsFirstSlot) {";
                _functionBodies += "_PETRI_PRIVATE_GET_VARIABLES_.PushVariables(" + Document.FlatPetriNet.Variables.Count + ");";
                _functionBodies += "}";
                _functionBodies += "FillVariables(_PETRI_PRIVATE_GET_VARIABLES_);";
                _functionBodies += "return " + code + ";";
                _functionBodies += "}\n";
            } else {
                if(a == Document.FlatPetriNet.ExitPoint) {
                    _functionBodies += "var _PETRI_PRIVATE_EXEC_RESULT_ = " + code + ";";
                    int i = 0;
                    foreach(var ret in Document.FlatPetriNet.ReturnValues) {
                        _functionBodies += "var _PETRI_PRIVATE_GET_RETURN_VALUE_" + i + "_ = " + ret.Value.MakeCode() + ";";
                        ++i;
                    }
                    _functionBodies += "_PETRI_PRIVATE_GET_VARIABLES_.Pop();";
                    _functionBodies += "_PETRI_PRIVATE_GET_VARIABLES_.PushReturnValues(" + Document.FlatPetriNet.ReturnValues.Count + ");";
                    i = 0;
                    foreach(var ret in Document.FlatPetriNet.ReturnValues) {
                        _functionBodies += "_PETRI_PRIVATE_GET_VARIABLES_[" + i + "].Name = \"" + ret.Key + "\";";
                        _functionBodies += "_PETRI_PRIVATE_GET_VARIABLES_[" + i + "].Value = _PETRI_PRIVATE_GET_RETURN_VALUE_" + i + "_;";
                        ++i;
                    }
                    _functionBodies += "return _PETRI_PRIVATE_EXEC_RESULT_;";
                } else {
                    _functionBodies += "return " + code + ";";
                }
                _functionBodies += "}\n";
            }

            range.LastLine = _functionBodies.LineCount - 1;

            CodeRanges[a.ID] = range;

            string action = a.IsStartState ? "initEntryPtr" : (a == Document.FlatPetriNet.ExitPoint ? "exitActionPtr" : action = a.CodeIdentifier + "_invocation");

            CodeGen += "var " + a.CodeIdentifier + " = " + "new PNAction(" + a.ID.ToString() + " + entitiesOffset, \"" + a.Name + "\", " + action + ", " + a.RequiredTokens.ToString() + ");";
            CodeGen += "petriNet.AddAction(" + a.CodeIdentifier + ", " + (a.IsStartState ? "firstLevel" : "false") + ");";
            foreach(var v in vars) {
                CodeGen += a.CodeIdentifier + ".AddVariable(" + v.EnumeratorName + ");";
            }
        }

        /// <summary>
        /// Generates the code for a transition.
        /// </summary>
        /// <param name="t">Transition.</param>
        protected override void MakeTransitionCode(Transition t)
        {
            string code = "return " + t.Condition.MakeCode() + ";";

            var vars = new SortedSet<VariableExpression>();
            t.GetVariables(vars);

            CodeRange range = new CodeRange();
            range.FirstLine = _functionBodies.LineCount + 1;
            if(vars.Count == 0) {
                _functionBodies += "static bool " + t.CodeIdentifier + "_invocation(Int32 _PETRI_PRIVATE_GET_ACTION_RESULT_) {\n" + code + "\n}\n";
            } else {
                _functionBodies += "static bool " + t.CodeIdentifier + "_invocation(IntPtr _PETRI_PRIVATE_GET_VARIABLES_PTR_, Int32 _PETRI_PRIVATE_GET_ACTION_RESULT_) {\nvar _PETRI_PRIVATE_GET_VARIABLES_ = new VarSlot(_PETRI_PRIVATE_GET_VARIABLES_PTR_);\n" + code + "\n}\n";
            }
            range.LastLine = _functionBodies.LineCount - 1;

            CodeRanges[t.ID] = range;

            code = t.CodeIdentifier + "_invocation";

            string before, after;

            if(t.Before is ExternalInnerPetriNetProxy) {
                before = t.Before.CodeIdentifier + "_endpoints.Item2";
            } else {
                before = t.Before.CodeIdentifier;
            }
            if(t.After is ExternalInnerPetriNetProxy) {
                after = t.After.CodeIdentifier + "_endpoints.Item1";
            } else {
                after = t.After.CodeIdentifier;
            }

            var decl = vars.Count > 0 ? "var " + t.CodeIdentifier + " = " : "";
            CodeGen += decl + before + ".AddTransition(" + t.ID.ToString() + " + entitiesOffset, \"" + t.Name + "\", " + after + ", " + code + ");";
            foreach(var v in vars) {
                CodeGen += t.CodeIdentifier + ".AddVariable(" + v.EnumeratorName + ");";
            }
        }

        /// <summary>
        /// Generates the code for importing an external petri net.
        /// </summary>
        /// <param name="pn">The petri net to import.</param>
        protected override void MakeExternalPetriNetCode(ExternalInnerPetriNetProxy pn)
        {
            CodeRange range = new CodeRange();
            range.FirstLine = _functionBodies.LineCount + 1;
            if(pn.Arguments.Count > 0) {
                _functionBodies += "static Int32 " + pn.CodeIdentifier + "_invocation(IntPtr _PETRI_PRIVATE_GET_VARIABLES_PTR_) {\nvar _PETRI_PRIVATE_GET_VARIABLES_ = new VarSlot(_PETRI_PRIVATE_GET_VARIABLES_PTR_);\n";
            } else {
                _functionBodies += "static Int32 " + pn.CodeIdentifier + "_invocation(IntPtr _PETRI_PRIVATE_GET_VARIABLES_PTR_) {\n";
            }
            int i = 0;
            foreach(var arg in pn.Arguments) {
                _functionBodies += "var _PETRI_PRIVATE_ARG_" + i++ + "_ = " + arg.Value.MakeCode() + ";";
            }
            _functionBodies += "var _PETRI_PRIVATE_TEMP_ = " + pn.ClassName + ".EntryInit(_PETRI_PRIVATE_GET_VARIABLES_PTR_);";
            i = 0;
            foreach(var arg in pn.Arguments) {
                _functionBodies += "_PETRI_PRIVATE_GET_VARIABLES_[" + arg.Key.GetEnumeratorName(VariableExpression.DefaultParamEnumName + "_" + pn.ClassName) + "].Value = _PETRI_PRIVATE_ARG_" + i++ + "_;";
            }
            _functionBodies += "return _PETRI_PRIVATE_TEMP_;";
            _functionBodies += "}\n";

            range.LastLine = _functionBodies.LineCount - 1;

            CodeRanges[pn.ID] = range;

            _functionBodies += "static Int32 " + pn.CodeIdentifier + "_fetchReturnValues(IntPtr _PETRI_PRIVATE_GET_VARIABLES_PTR_) {\nvar _PETRI_PRIVATE_GET_VARIABLES_ = new VarSlot(_PETRI_PRIVATE_GET_VARIABLES_PTR_);\n";
            _functionBodies += "var _PETRI_PRIVATE_EXEC_RESULT_ = Petri.Generated." + pn.ClassName + ".ExitAction(_PETRI_PRIVATE_GET_VARIABLES_PTR_);";
            i = 0;
            foreach(var ret in pn.ReturnValues) {
                if(pn.ReturnValuesDestination.TryGetValue(ret.Key, out VariableExpression dest)) {
                    _functionBodies += "var _PETRI_PRIVATE_GET_RETURN_VALUE_" + dest.Expression + " = _PETRI_PRIVATE_GET_VARIABLES_[" + i + "].Value; // " + ret.Key;
                }
                ++i;
            }
            _functionBodies += "_PETRI_PRIVATE_GET_VARIABLES_.Pop();";

            foreach(var ret in pn.ReturnValuesDestination) {
                _functionBodies += ret.Value.MakeCode() + " = _PETRI_PRIVATE_GET_RETURN_VALUE_" + ret.Value.Expression + ";";
            }

            _functionBodies += "return _PETRI_PRIVATE_EXEC_RESULT_;";
            _functionBodies += "}";
            _functionBodies.AddLine();

            CodeGen += "var " + pn.CodeIdentifier + "_endpoints = "
                              + "Petri.Generated." + pn.ClassName + ".Fill(petriNet, entitiesOffset + " + pn.EntitiesOffset + ", "
                              + (pn.IsStartState ? "firstLevel" : "false") + ", " + pn.CodeIdentifier + "_invocation, " + pn.CodeIdentifier + "_fetchReturnValues);";
            CodeGen += pn.CodeIdentifier + "_endpoints.Item1.RequiredTokens = " + pn.RequiredTokens + ";";
        }

        /// <summary>
        /// Generates the enum source code that allows for the access and modifications of the petri net's variables.
        /// </summary>
        /// <returns>The variables enum.</returns>
        string GenerateVarEnum()
        {
            int var = 0;
            var vars = new List<string>((
                from p in Document.FlatPetriNet.Parameters
                select "V" + p.Expression + " = " + var++
            ).Union(
                from v in Document.FlatPetriNet.Variables
                where !Document.FlatPetriNet.Parameters.Contains(v.Key)
                select "V" + v.Key.Expression + " = " + var++
            ));
            if(vars.Count > 0) {
                return "enum " + VariableExpression.DefaultVarEnumName + " : UInt32 {" + string.Join(", ", vars) + "};\n";
            }

            return null;
        }

        /// <summary>
        /// Generates the enum source code that allows for setting the petri net's parameters.
        /// </summary>
        /// <returns>The parameters enum.</returns>
        string GenerateParamEnum(SortedSet<VariableExpression> parameters, string className)
        {
            int var = 0;
            var vars = from v in parameters
                       select "V" + v.Expression + " = " + var++;
            if(parameters.Count > 0) {
                return "public enum " + VariableExpression.DefaultParamEnumName + (className != null ? "_" + className : "") + " : UInt32 {" + string.Join(", ", vars) + "};\n";
            }

            return null;
        }
    }
}
