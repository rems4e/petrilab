//
//  CPetriGen.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2015-07-05.
//

using System.Collections.Generic;
using System.Linq;

using Petri.Code;
using Petri.Model;

namespace Petri.CodeGen
{
    /// <summary>
    /// A PetriGen that generates code for a C petri net.
    /// </summary>
    public class CPetriGen : CFamilyPetriGen
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.CodeGen.CPetriGen"/> class.
        /// </summary>
        /// <param name="doc">Document.</param>
        public CPetriGen(Application.Document doc) : base(doc, Language.C)
        {
        }

        /// <summary>
        /// Writes the expression evaluator code corresponding to the given expression to the given path.
        /// </summary>
        /// <param name="expression">Expression.</param>
        /// <param name="path">Path.</param>
        /// <param name="userData">Additional and optional user data that will be used to generate the code.</param>
        public override void WriteExpressionEvaluator(Expression expression,
                                                      string path,
                                                      params object[] userData)
        {
            string expr;
            if(expression is ExpressionList) {
                expr = string.Join(", ", (from e in ((ExpressionList)expression).Expressions select "(" + e.MakeCode() + ")"));

            } else {
                expr = "(" + expression.MakeCode() + ")";
            }

            CodeGen generator = new CLikeCodeGen(Language.C);
            generator += "#define _GNU_SOURCE"; // For gcc to declare asprintf
            generator += "#include <stdio.h>";

            AppendHeaders(generator);

            generator += "#include <petrilab/C/PetriLab.h>";
            generator += "#include <inttypes.h>";

            generator += GenerateVarEnum();

            generator += "char *" + Document.RootDocument.CodePrefix + "_evaluate(void *_PETRI_PRIVATE_GET_VARIABLES_PTR_) {";
            generator += "struct PetriVarSlot *_PETRI_PRIVATE_GET_VARIABLES_ = _PETRI_PRIVATE_GET_VARIABLES_PTR_;";

            generator += "char *_PETRI_PRIVATE_EVAL_RESULT_BUF_;";
            generator += "asprintf(&_PETRI_PRIVATE_EVAL_RESULT_BUF_, \"" + userData[0] + "\", " + expr + ");";

            generator += "return _PETRI_PRIVATE_EVAL_RESULT_BUF_;";
            generator += "}\n";

            System.IO.File.WriteAllText(path, generator.Value);
        }

        /// <summary>
        /// Called before any entity generation and gives the opportunity to emit some setup code.
        /// </summary>
        protected override void Begin()
        {
            CodeGen += "/*";
            foreach(var h in SourceHeaders) {
                CodeGen += "* " + h;
            }
            CodeGen += " */";
            CodeGen.AddLine();

            CodeGen += "#include <math.h>";
            CodeGen += "#include <stdint.h>";
            CodeGen += "#include <stdbool.h>";
            CodeGen += "#include <time.h>";
            CodeGen += "#include <petrilab/C/Action.h>";
            CodeGen += "#include <petrilab/C/PetriDynamicLib.h>";
            CodeGen += "#include <petrilab/C/PetriNet.h>";
            CodeGen += "#include <petrilab/C/PetriUtils.h>";
            CodeGen += "#include <petrilab/C/VarSlot.h>";
            AppendHeaders(CodeGen);

            CodeGen.AddLine();

            CodeGen += "\nstruct FillResult {";
            CodeGen += "void *start;";
            CodeGen += "void *end;";
            CodeGen += "};\n";

            foreach(var pn in UniqueExternalPetriNets) {
                CodeGen += "struct FillResult " + pn.ClassName + "_fill(struct PetriNet *petriNet, uint64_t entitiesOffset, bool firstLevel, Petri_actionResult_t (*initEntryPtr)(struct PetriVarSlot *), Petri_actionResult_t (*exitActionPtr)(struct PetriVarSlot *));";
                CodeGen += "Petri_actionResult_t " + pn.ClassName + "_entryInit(struct PetriVarSlot *);";
                CodeGen += "Petri_actionResult_t " + pn.ClassName + "_exitAction(struct PetriVarSlot *);";
                CodeGen += GenerateParamEnum(pn.Parameters, pn.ClassName);
            }

            CodeGen += GenerateVarEnum();

            CodeGen += "static void fillVariables(struct PetriVarSlot *variables) {";
            foreach(var v in Document.FlatPetriNet.Variables) {
                CodeGen += "PetriVarSlot_setVariableName(variables, (uint32_t)(" + v.Key.EnumeratorName + "), \"" + v.Key.MakeUserReadable() + "\");";
                CodeGen += "PetriVarSlot_setVariableDefaultValue(variables, (uint32_t)(" + v.Key.EnumeratorName + "), " + v.Value + ");";
            }
            CodeGen += "}";
            CodeGen.AddLine();


            _functionsIndex = CodeGen.Value.Length;
            _linesToFunctions = CodeGen.LineCount;

            CodeGen += "struct FillResult " + ClassName + "_fill(struct PetriNet *petriNet, uint64_t entitiesOffset, bool firstLevel, Petri_actionResult_t (*initEntryPtr)(struct PetriVarSlot *), Petri_actionResult_t (*exitActionPtr)(struct PetriVarSlot *)) {";
        }

        /// <summary>
        /// Finishes the code generation and compute the Hash value of the petri net.
        /// </summary>
        protected override void End()
        {
            AppendFunctions();

            CodeGen += "\nreturn (struct FillResult){state_0, " + Document.FlatPetriNet.ExitPoint.CodeIdentifier + "};";
            CodeGen += "}"; // fill()

            var verbosity = "PetriNet_Verbosity" + Document.Settings.DefaultExecutionVerbosity.ToString().Replace(" ", "").Replace(",", " | PetriNet_Verbosity");
            CodeGen += "struct PetriNet *" + ClassName + "_createPetriNet() {";
            CodeGen += "struct PetriNet *petriNet = PetriNet_createVariablesCount(\"" + ClassName + "\", " + Document.FlatPetriNet.Variables.Count + ");";
            CodeGen += "PetriNet_setLogVerbosity(petriNet, " + verbosity + ");";
            if(Profile.MaxConcurrency != null) {
                CodeGen += "PetriNet_setMaxConcurrency(petriNet, " + Profile.MaxConcurrency + ");";
            }
            CodeGen += ClassName + "_fill(petriNet, 0, true, &" + ClassName + "_entryInit, &" + ClassName + "_exitAction);";
            CodeGen += "fillVariables(PetriNet_getVariables(petriNet));";
            CodeGen += "return petriNet;";
            CodeGen += "}"; // createPetriNet()

            CodeGen += "";

            CodeGen += "struct PetriNet *" + ClassName + "_createDebugPetriNet() {";
            CodeGen += "struct PetriNet *petriNet = PetriNet_createDebugVariablesCount(\"" + ClassName + "\", " + Document.FlatPetriNet.Variables.Count + ");";
            CodeGen += "PetriNet_setLogVerbosity(petriNet, " + verbosity + ");";
            if(Profile.MaxConcurrency != null) {
                CodeGen += "PetriNet_setMaxConcurrency(petriNet, " + Profile.MaxConcurrency + ");";
            }
            CodeGen += ClassName + "_fill(petriNet, 0, true, &" + ClassName + "_entryInit, &" + ClassName + "_exitAction);";
            CodeGen += "fillVariables(PetriNet_getVariables(petriNet));";
            CodeGen += "return petriNet;";
            CodeGen += "}"; // createDebugPetriNet()

            string toHash = CodeGen.Value;
            var sha = new System.Security.Cryptography.SHA256CryptoServiceProvider();
            Hash = System.BitConverter.ToString(
                sha.ComputeHash(System.Text.Encoding.UTF8.GetBytes(toHash))
            ).Replace("-", "");

            CodeGen += "";

            CodeGen += "void *" + ClassName + "_create() {";
            CodeGen += "return " + ClassName + "_createPetriNet();";
            CodeGen += "}";

            CodeGen += "";

            CodeGen += "void *" + ClassName + "_createDebug() {";
            CodeGen += "return " + ClassName + "_createDebugPetriNet();";
            CodeGen += "}";

            CodeGen += "";

            CodeGen += "char *" + ClassName + "_evaluate(void *vars, char const *libPath) {";
            CodeGen += "return PetriUtility_loadEvaluateAndInvoke(vars, libPath, \"" + Document.CodePrefix + "\");";
            CodeGen += "}";

            CodeGen += "";

            CodeGen += "char const *" + ClassName + "_getHash() {";
            CodeGen += "return \"" + Hash + "\";";
            CodeGen += "}";

            CodeGen += "";

            CodeGen += "struct PetriDynamicLib *" + ClassName + "_createLibForEditor() {";
            CodeGen += "return PetriDynamicLib_create(\"" + Document.CodePrefix + "\", " + Profile.DebugPort + ", \"./\");";
            CodeGen += "}";

            CodeGen += "";

            CodeGen.Format();

            _headerGen += "/*";
            foreach(var h in SourceHeaders) {
                _headerGen += " * " + h;
            }
            _headerGen += " */";
            _headerGen.AddLine();

            _headerGen += "#ifndef PETRI_GENERATED_" + ClassName + "_H";
            _headerGen += "#define PETRI_GENERATED_" + ClassName + "_H";

            _headerGen += "";
            _headerGen += "#include <petrilab/C/PetriDynamicLib.h>";
            _headerGen += "";

            _headerGen += GenerateParamEnum(Document.FlatPetriNet.Parameters, ClassName);

            _headerGen += "#ifdef __cplusplus";
            _headerGen += "extern \"C\" {";
            _headerGen += "#endif";

            _headerGen += "";

            _headerGen += "struct PetriNet *" + ClassName + "_createPetriNet();";
            _headerGen += "struct PetriDebug *" + ClassName + "_createDebugPetriNet();";
            _headerGen.AddLine();

            _headerGen += "inline struct PetriDynamicLib *" + ClassName + "_createLib() {";
            _headerGen += "return PetriDynamicLib_create(\"" + Document.CodePrefix + "\", "
                              + Profile.DebugPort + ", \"\");";
            _headerGen += "}";

            _headerGen += "";

            _headerGen += "inline struct PetriDynamicLib *" + ClassName + "_createLibWithPath(char const *customPath) {";
            _headerGen += "return PetriDynamicLib_create(\"" + Document.CodePrefix + "\", "
                              + Profile.DebugPort + ", customPath);";
            _headerGen += "}";

            _headerGen += "";

            _headerGen += "struct PetriDynamicLib *" + ClassName + "_createLib();";
            _headerGen += "struct PetriDynamicLib *" + ClassName + "_createLibWithPath(char const *customPath);";

            _headerGen += "";

            _headerGen += "#ifdef __cplusplus";
            _headerGen += "}";
            _headerGen += "#endif";
            _headerGen += "";
            _headerGen += "#endif"; // ifndef header guard

            _headerGen.Format();
        }

        /// <summary>
        /// Casts an enum member to the petri net runtime's action result type.
        /// </summary>
        /// <returns>The enum member.</returns>
        /// <param name="member">Member.</param>
        /// <param name="actualCast">Whether to perform a cast.</param>
        protected override string CastEnumMember(string member, bool actualCast)
        {
            var result = Document.Settings.Enum.Name + "_" + member;
            if(actualCast) {
                result = "(Petri_actionResult_t)(" + result + ")";
            }
            return result;
        }

        /// <summary>
        /// Generates the code for an action.
        /// </summary>
        /// <param name="a">Action.</param>
        protected override void MakeActionCode(Action a)
        {
            var vars = new SortedSet<VariableExpression>();
            a.GetVariables(vars);

            CodeRange range = new CodeRange();
            range.FirstLine = _functionBodies.LineCount + 1;
            if(a.IsStartState) {
                _functionBodies += "Petri_actionResult_t " + ClassName + "_entryInit(struct PetriVarSlot *_PETRI_PRIVATE_GET_VARIABLES_) {";
                _functionBodies += "if(!PetriVarSlot_isFirstSlot(_PETRI_PRIVATE_GET_VARIABLES_)) {";
                _functionBodies += "PetriVarSlot_pushVariables(_PETRI_PRIVATE_GET_VARIABLES_, " + Document.FlatPetriNet.Variables.Count + ");";
                _functionBodies += "}";
                _functionBodies += "fillVariables(_PETRI_PRIVATE_GET_VARIABLES_);";
                _functionBodies += "Petri_actionResult_t _PETRI_PRIVATE_EXEC_RESULT_ = (Petri_actionResult_t)(" + a.Invocation.MakeCode() + ")" + ";";
                _functionBodies += "return _PETRI_PRIVATE_EXEC_RESULT_;";
                _functionBodies += "}\n";
            } else {
                if(a == Document.FlatPetriNet.ExitPoint) {
                    _functionBodies += "Petri_actionResult_t " + ClassName + "_exitAction(struct PetriVarSlot *_PETRI_PRIVATE_GET_VARIABLES_) {";
                    _functionBodies += "Petri_actionResult_t _PETRI_PRIVATE_EXEC_RESULT_ = " + a.Invocation.MakeCode() + ";";
                    int i = 0;
                    foreach(var ret in Document.FlatPetriNet.ReturnValues) {
                        _functionBodies += "Petri_actionResult_t _PETRI_PRIVATE_GET_RETURN_VALUE_" + i + "_ = " + ret.Value.MakeCode() + ";";
                        ++i;
                    }
                    _functionBodies += "PetriVarSlot_pop(_PETRI_PRIVATE_GET_VARIABLES_);";

                    _functionBodies += "PetriVarSlot_pushReturnValues(_PETRI_PRIVATE_GET_VARIABLES_, " + Document.FlatPetriNet.ReturnValues.Count + ");";
                    i = 0;
                    foreach(var ret in Document.FlatPetriNet.ReturnValues) {
                        _functionBodies += "PetriVarSlot_setVariableName(_PETRI_PRIVATE_GET_VARIABLES_, " + i + ", \"" + ret.Key + "\");";
                        _functionBodies += "PetriVarSlot_setVariableValue(_PETRI_PRIVATE_GET_VARIABLES_, " + i + ", _PETRI_PRIVATE_GET_RETURN_VALUE_" + i + "_);";
                        ++i;
                    }
                    _functionBodies += "return _PETRI_PRIVATE_EXEC_RESULT_;";

                } else {
                    _functionBodies += "static Petri_actionResult_t " + a.CodeIdentifier + "_invocation(struct PetriVarSlot *_PETRI_PRIVATE_GET_VARIABLES_) {";
                    _functionBodies += "return " + a.Invocation.MakeCode() + ";";
                }
                _functionBodies += "}\n";
            }

            range.LastLine = _functionBodies.LineCount - 1;

            CodeRanges[a.ID] = range;

            string action = a.IsStartState ? "initEntryPtr" : (a == Document.FlatPetriNet.ExitPoint ? "exitActionPtr" : "&" + a.CodeIdentifier + "_invocation");

            CodeGen += "struct PetriAction *"
                    + a.CodeIdentifier
                    + " = PetriAction_createWithParam("
                    + a.ID.ToString()
                    + " + entitiesOffset, \"" + a.Name + "\", "
                    + action + ", "
                    + a.RequiredTokens.ToString() + ");";
            CodeGen += "PetriNet_addAction(petriNet, " + a.CodeIdentifier + ", " + (a.IsStartState ? "firstLevel" : "false") + ");";
            foreach(var v in vars) {
                CodeGen += "PetriAction_addVariable(" + a.CodeIdentifier + ", (uint32_t)(" + v.EnumeratorName + "));";
            }
        }

        /// <summary>
        /// Generates the code for a transition.
        /// </summary>
        /// <param name="t">Transition.</param>
        protected override void MakeTransitionCode(Transition t)
        {
            var vars = new SortedSet<VariableExpression>();
            t.GetVariables(vars);

            CodeRange range = new CodeRange();
            range.FirstLine = _functionBodies.LineCount + 1;
            _functionBodies += "static bool " + t.CodeIdentifier + "_invocation(struct PetriVarSlot *_PETRI_PRIVATE_GET_VARIABLES_, Petri_actionResult_t _PETRI_PRIVATE_GET_ACTION_RESULT_) {";

            if(t.Condition is WrapperFunctionInvocation) {
                _functionBodies += t.Condition.MakeCode();
            } else {
                _functionBodies += "bool _PETRI_PRIVATE_TRANSITION_RESULT_ = " + t.Condition.MakeCode() + ";";
            }

            if(t.Condition is WrapperFunctionInvocation) {
                _functionBodies += "return true;";
            } else {
                _functionBodies += "return _PETRI_PRIVATE_TRANSITION_RESULT_;";
            }

            _functionBodies += "}\n";

            range.LastLine = _functionBodies.LineCount - 1;

            CodeRanges[t.ID] = range;

            string before, after;

            if(t.Before is ExternalInnerPetriNetProxy) {
                before = "((struct PetriAction *)" + t.Before.CodeIdentifier + "_endpoints.end)";
            } else {
                before = t.Before.CodeIdentifier;
            }
            if(t.After is ExternalInnerPetriNetProxy) {
                after = "((struct PetriAction *)" + t.After.CodeIdentifier + "_endpoints.start)";
            } else {
                after = t.After.CodeIdentifier;
            }

            var decl = vars.Count > 0 ? "struct PetriTransition *" + t.CodeIdentifier + " = " : "";
            CodeGen += decl
                    + "PetriAction_addTransitionWithParam("
                    + before
                    + ", " + t.ID.ToString() + " + entitiesOffset"
                    + ", \"" + t.Name
                    + "\", " + after
                    + ", &" + t.CodeIdentifier + "_invocation);";
            foreach(var v in vars) {
                CodeGen += "PetriTransition_addVariable(" + t.CodeIdentifier + ", (uint32_t)(" + v.EnumeratorName + "));";
            }
        }

        /// <summary>
        /// Generates the code for importing an external petri net.
        /// </summary>
        /// <param name="pn">The petri net to import.</param>
        protected override void MakeExternalPetriNetCode(ExternalInnerPetriNetProxy pn)
        {
            CodeRange range = new CodeRange();
            range.FirstLine = _functionBodies.LineCount + 1;
            _functionBodies += "static Petri_actionResult_t " + pn.CodeIdentifier + "_invocation(struct PetriVarSlot *_PETRI_PRIVATE_GET_VARIABLES_) {";
            int i = 0;
            foreach(var arg in pn.Arguments) {
                _functionBodies += "int64_t _PETRI_PRIVATE_ARG_" + i++ + "_ = " + arg.Value.MakeCode() + ";";
            }
            _functionBodies += "Petri_actionResult_t _PETRI_PRIVATE_TEMP_ = " + pn.ClassName + "_entryInit(_PETRI_PRIVATE_GET_VARIABLES_);";
            i = 0;
            foreach(var arg in pn.Arguments) {
                _functionBodies += "PetriVarSlot_setVariableValue(_PETRI_PRIVATE_GET_VARIABLES_, (uint32_t)(" + arg.Key.GetEnumeratorName(VariableExpression.DefaultParamEnumName + "_" + pn.ClassName) + "), _PETRI_PRIVATE_ARG_" + i++ + "_);";
            }
            _functionBodies += "return _PETRI_PRIVATE_TEMP_;";
            _functionBodies += "}\n";

            range.LastLine = _functionBodies.LineCount - 1;

            CodeRanges[pn.ID] = range;

            _functionBodies += "static Petri_actionResult_t " + pn.CodeIdentifier + "_fetchReturnValues(struct PetriVarSlot *_PETRI_PRIVATE_GET_VARIABLES_) {";
            _functionBodies += "Petri_actionResult_t _PETRI_PRIVATE_EXEC_RESULT_ = " + pn.ClassName + "_exitAction(_PETRI_PRIVATE_GET_VARIABLES_);";
            i = 0;
            foreach(var ret in pn.ReturnValues) {
                if(pn.ReturnValuesDestination.TryGetValue(ret.Key, out VariableExpression dest)) {
                    _functionBodies += "Petri_actionResult_t _PETRI_PRIVATE_GET_RETURN_VALUE_" + dest.Expression + " = PetriVarSlot_getVariableValue(_PETRI_PRIVATE_GET_VARIABLES_, " + i + "); // " + ret.Key;
                }
                ++i;
            }
            _functionBodies += "PetriVarSlot_pop(_PETRI_PRIVATE_GET_VARIABLES_ );";

            foreach(var ret in pn.ReturnValuesDestination) {
                _functionBodies += ret.Value.MakeCode() + " = _PETRI_PRIVATE_GET_RETURN_VALUE_" + ret.Value.Expression + ";";
            }

            _functionBodies += "return _PETRI_PRIVATE_EXEC_RESULT_;";
            _functionBodies += "}";
            _functionBodies.AddLine();

            CodeGen += "struct FillResult " + pn.CodeIdentifier + "_endpoints = "
                                            + pn.ClassName + "_fill(petriNet, entitiesOffset + " + pn.EntitiesOffset + ", "
                                            + (pn.IsStartState ? "firstLevel" : "false") + ", " + pn.CodeIdentifier + "_invocation, " + pn.CodeIdentifier + "_fetchReturnValues);";
            CodeGen += "PetriAction_setRequiredTokens((struct PetriAction *)" + pn.CodeIdentifier + "_endpoints.start, " + pn.RequiredTokens + ");";
        }

        /// <summary>
        /// Generates the enum source code that allows for the access and modifications of the petri net's variables.
        /// </summary>
        /// <returns>The variables enum.</returns>
        string GenerateVarEnum()
        {
            int var = 0;
            var vars = new List<string>((
                from p in Document.FlatPetriNet.Parameters
                select p.EnumeratorName + " = " + var++
            ).Union(
                from v in Document.FlatPetriNet.Variables
                where !Document.FlatPetriNet.Parameters.Contains(v.Key)
                select v.Key.EnumeratorName + " = " + var++
            ));
            if(vars.Count > 0) {
                return "enum " + VariableExpression.DefaultVarEnumName + " {" + string.Join(", ", vars) + "};\n";
            }

            return null;
        }

        /// <summary>
        /// Generates the enum source code that allows for setting the petri net's parameters.
        /// </summary>
        /// <returns>The parameters enum.</returns>
        string GenerateParamEnum(SortedSet<VariableExpression> parameters, string className)
        {
            int var = 0;
            var vars = from p in parameters
                       select p.GetEnumeratorName(VariableExpression.DefaultParamEnumName + "_" + className) + " = " + var++;
            if(parameters.Count > 0) {
                return "enum " + VariableExpression.DefaultParamEnumName + "_" + className + " {" + string.Join(", ", vars) + "};\n";
            }

            return null;
        }

    }
}
