//
//  CLikeCodeGen.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2015-06-29.
//

using System.Collections.Generic;

using Token = Petri.Code.Expression.Token;

namespace Petri.CodeGen
{
    /// <summary>
    /// A CodeGen specialized for the C-like programming languages. Currently, C, C++ and C#.
    /// </summary>
    public class CLikeCodeGen : CodeGen
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.CodeGen.CLikeCodeGen"/> class.
        /// </summary>
        /// <param name="language">The programming language of the code generator.</param>
        public CLikeCodeGen(Code.Language language)
        {
            _lang = language;
            _value = new System.Text.StringBuilder();
        }

        /// <summary>
        /// Gets the programming language of the generated code.
        /// </summary>
        /// <value>The language.</value>
        public override Code.Language Language {
            get {
                return _lang;
            }
        }

        /// <summary>
        /// Gets or sets the generated code.
        /// </summary>
        /// <value>The value.</value>
        public override string Value {
            get {
                return _value.ToString();
            }
            set {
                _value.Clear();
                _value.Append(value);
            }
        }

        /// <summary>
        /// Formats the generated source code.
        /// </summary>
        public override void Format()
        {
            var newVal = new System.Text.StringBuilder();

            int currentIndent = 0;

            var nesting = new Stack<Token>();

            var lines = Value.Split('\n');
            foreach(string line in lines) {
                string newLine = line;

                if(!line.StartsWithInv("#")) {
                    int existingIndent = 0;
                    for(int i = 0; i < line.Length; ++i) {
                        if(line[i] == '\t') {
                            ++existingIndent;
                        } else {
                            break;
                        }
                    }
                    int firstIndent = 0;
                    int deltaNext = 0;

                    for(int i = 0; i < line.Length; ++i) {
                        int delta = 0;
                        switch(line[i]) {
                        case '(':
                            nesting.Push(Token.Parenthesis);
                            delta = 2;
                            break;
                        case ')':
                            if(nesting.Count > 0 && nesting.Peek() == Token.Parenthesis) {
                                delta = -2;
                                nesting.Pop();
                            }
                            break;
                        case '{':
                            delta = 1;
                            nesting.Push(Token.Brackets);
                            break;
                        case '}':
                            if(nesting.Count > 0 && nesting.Peek() == Token.Brackets) {
                                delta = -1;
                                nesting.Pop();
                            }
                            break;
                        case '[':
                            delta = 2;
                            nesting.Push(Token.Subscript);
                            break;
                        case ']':
                            if(nesting.Count > 0 && nesting.Peek() == Token.Subscript) {
                                delta = -2;
                                nesting.Pop();
                            }
                            break;
                        case '"':
                            if(i == 0 || line[i - 1] != '\\') {
                                // First quote
                                if(nesting.Count == 0 || (nesting.Peek() != Token.DoubleQuote && nesting.Peek() != Token.Quote)) {
                                    nesting.Push(Token.DoubleQuote);
                                }
                                // Second quote
                                else if(nesting.Count > 0 && nesting.Peek() == Token.DoubleQuote && line[i - 1] != '\\') {
                                    nesting.Pop();
                                }
                            }
                            break;
                        case '\'':
                            if(i == 0 || line[i - 1] != '\\') {
                                // First quote
                                if(nesting.Count == 0 || (nesting.Peek() != Token.Quote && nesting.Peek() != Token.DoubleQuote)) {
                                    nesting.Push(Token.Quote);
                                }
                            // Second quote
                            else if(nesting.Count > 0 && nesting.Peek() == Token.Quote && line[i - 1] != '\\') {
                                    nesting.Pop();
                                }
                            }
                            break;
                        }

                        if(i == 0 && delta < 0) {
                            firstIndent = delta;
                        }

                        deltaNext += delta;
                    }

                    if(line.Length > 0) {
                        newLine = GetNTab(currentIndent + firstIndent - existingIndent) + line;
                    }
                    currentIndent += deltaNext;
                }

                newVal.Append(newLine + "\n");
            }

            _value = newVal;
        }

        /// <summary>
        /// This method is responsible for the actual code addition.
        /// </summary>
        /// <param name="line">The new line of code.</param>
        protected override void AddInternal(string line)
        {
            _value.Append(line);
        }

        /// <summary>
        /// Gets N tab characters concatenated into a new string.
        /// </summary>
        /// <returns>The N tab.</returns>
        /// <param name="n">N.</param>
        static string GetNTab(int n)
        {
            return new string('\t', n);
        }

        Code.Language _lang;
        System.Text.StringBuilder _value;
    }
}

