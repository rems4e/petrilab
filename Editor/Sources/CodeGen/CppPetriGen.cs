//
//  CppPetriGen.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2015-06-29.
//

using System.Collections.Generic;
using System.Linq;

using Petri.Code;
using Petri.Model;

namespace Petri.CodeGen
{
    /// <summary>
    /// A PetriGen that generates code for a C++ petri net.
    /// </summary>
    public class CppPetriGen : CFamilyPetriGen
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.CodeGen.CppPetriGen"/> class.
        /// </summary>
        /// <param name="doc">Document.</param>
        public CppPetriGen(Application.Document doc) : base(doc, Language.Cpp)
        {
        }

        /// <summary>
        /// Writes the expression evaluator code corresponding to the given expression to the given path.
        /// </summary>
        /// <param name="expression">Expression.</param>
        /// <param name="path">Path.</param>
        /// <param name="userData">Additional and optional user data that will be used to generate the code.</param>
        public override void WriteExpressionEvaluator(Expression expression,
                                                      string path,
                                                      params object[] userData)
        {
            string expr;
            if(expression is ExpressionList) {
                expr = string.Join(" << std::endl << ", (from e in ((ExpressionList)expression).Expressions select "(" + e.MakeCode() + ")"));
            } else {
                expr = "(" + expression.MakeCode() + ")";
            }

            CodeGen generator = new CLikeCodeGen(Language.Cpp);
            AppendHeaders(generator);

            generator += "#include <petrilab/Cpp/PetriLab.h>";
            generator += "#include <string>";
            generator += "#include <sstream>";
            generator += "#include <cstring>";

            generator += "using namespace Petri;";

            generator += GenerateVarEnum();

            generator += "extern \"C\" char *" + Document.RootDocument.CodePrefix + "_evaluate(void *_PETRI_PRIVATE_GET_VARIABLES_PTR_) {";
            generator += "auto &_PETRI_PRIVATE_GET_VARIABLES_ = *static_cast<Petri::VarSlot *>(_PETRI_PRIVATE_GET_VARIABLES_PTR_);";
            generator += "std::ostringstream _PETRI_PRIVATE_EVAL_RESULT_STREAM_;";
            generator += "_PETRI_PRIVATE_EVAL_RESULT_STREAM_ << " + expr + ";";
            generator += "auto _PETRI_PRIVATE_EVAL_RESULT_ = _PETRI_PRIVATE_EVAL_RESULT_STREAM_.str();";
            generator += "char *_PETRI_PRIVATE_EVAL_RESULT_BUF_ = static_cast<char *>(malloc(_PETRI_PRIVATE_EVAL_RESULT_.size() + 1));";
            generator += "memcpy(_PETRI_PRIVATE_EVAL_RESULT_BUF_, _PETRI_PRIVATE_EVAL_RESULT_.c_str(), _PETRI_PRIVATE_EVAL_RESULT_.size() + 1);";
            generator += "return _PETRI_PRIVATE_EVAL_RESULT_BUF_;";
            generator += "}\n";

            System.IO.File.WriteAllText(path, generator.Value);
        }

        /// <summary>
        /// Called before any entity generation and gives the opportunity to emit some setup code.
        /// </summary>
        protected override void Begin()
        {
            CodeGen += "/*";
            foreach(var h in SourceHeaders) {
                CodeGen += " * " + h;
            }
            CodeGen += " */";
            CodeGen.AddLine();

            CodeGen += "#include <cstdint>";
            CodeGen += "#include <petrilab/Cpp/Action.h>";
            CodeGen += "#include <petrilab/Cpp/Variable.h>";
            CodeGen += "#include <petrilab/Cpp/MemberPetriDynamicLib.h>";
            CodeGen += "#include <petrilab/Cpp/PetriDebug.h>";
            CodeGen += "#include <petrilab/Cpp/PetriUtils.h>";
            CodeGen += "#include <petrilab/Cpp/VarSlot.h>";
            AppendHeaders(CodeGen);

            CodeGen.AddLine();

            CodeGen += "using namespace Petri;";

            CodeGen += "\nstruct FillResult {";
            CodeGen += "void *start;";
            CodeGen += "void *end;";
            CodeGen += "};\n";

            foreach(var pn in UniqueExternalPetriNets) {
                CodeGen += "extern \"C\" FillResult " + pn.ClassName + "_fill(PetriNet &petriNet, std::uint64_t entitiesOffset, bool firstLevel, Petri_actionResult_t (*initEntryPtr)(VarSlot &), Petri_actionResult_t (*exitActionPtr)(VarSlot &));";
                CodeGen += "extern \"C\" Petri_actionResult_t " + pn.ClassName + "_entryInit(VarSlot &);";
                CodeGen += "extern \"C\" Petri_actionResult_t " + pn.ClassName + "_exitAction(VarSlot &);";
                CodeGen += GenerateParamEnum(pn.Parameters, pn.ClassName);
            }

            CodeGen += GenerateVarEnum();

            CodeGen += "static void fillVariables(Petri::VarSlot &variables) {";
            foreach(var v in Document.FlatPetriNet.Variables) {
                CodeGen += "variables[" + v.Key.EnumeratorName + "].setName(\"" + v.Key.MakeUserReadable() + "\");";
                CodeGen += "variables[" + v.Key.EnumeratorName + "].setDefaultValue(" + v.Value + ");";
            }
            CodeGen += "}";
            CodeGen.AddLine();

            _functionsIndex = CodeGen.Value.Length;
            _linesToFunctions = CodeGen.LineCount;
            CodeGen += "extern \"C\" FillResult " + ClassName + "_fill(PetriNet &petriNet, std::uint64_t entitiesOffset, bool firstLevel, Petri_actionResult_t (*initEntryPtr)(VarSlot &), Petri_actionResult_t (*exitActionPtr)(VarSlot &)) {";
        }

        /// <summary>
        /// Finishes the code generation and compute the Hash value of the petri net.
        /// </summary>
        protected override void End()
        {
            AppendFunctions();

            CodeGen += "\nreturn (FillResult){&state_0, &" + Document.FlatPetriNet.ExitPoint.CodeIdentifier + "};";
            CodeGen += "}"; // fill()

            CodeGen += "namespace Petri {";
            CodeGen += "namespace Generated {";
            CodeGen += "namespace " + ClassName + " {";

            var verbosity = "PetriNet::Verbosity" + Document.Settings.DefaultExecutionVerbosity.ToString().Replace(" ", "").Replace(",", " | PetriNet::Verbosity");
            CodeGen += "std::unique_ptr<::Petri::PetriNet> createPetriNet() {";
            CodeGen += "auto petriNet = std::make_unique<PetriNet>(\"" + ClassName + "\", " + Document.FlatPetriNet.Variables.Count + ");";
            CodeGen += "petriNet->setLogVerbosity(" + verbosity + ");";
            if(Profile.MaxConcurrency != null) {
                CodeGen += "petriNet->setMaxConcurrency(" + Profile.MaxConcurrency + ");";
            }
            CodeGen += ClassName + "_fill(*petriNet, 0, true, &" + ClassName + "_entryInit, &" + ClassName + "_exitAction);";
            CodeGen += "fillVariables(petriNet->variables());";
            CodeGen += "return petriNet;";
            CodeGen += "}";
            CodeGen += "";

            CodeGen += "std::unique_ptr<::Petri::PetriDebug> createDebugPetriNet() {";
            CodeGen += "auto petriNet = std::make_unique<PetriDebug>(\"" + ClassName + "\", " + Document.FlatPetriNet.Variables.Count + ");";
            CodeGen += "petriNet->setLogVerbosity(" + verbosity + ");";
            if(Profile.MaxConcurrency != null) {
                CodeGen += "petriNet->setMaxConcurrency(" + Profile.MaxConcurrency + ");";
            }
            CodeGen += ClassName + "_fill(*petriNet, 0, true, &" + ClassName + "_entryInit, &" + ClassName + "_exitAction);";
            CodeGen += "fillVariables(petriNet->variables());";
            CodeGen += "return petriNet;";
            CodeGen += "}";

            CodeGen += "}"; // namespace "ClassName"
            CodeGen += "}"; // namespace Generated
            CodeGen += "}"; // namespace Petri

            string toHash = CodeGen.Value;
            var sha = new System.Security.Cryptography.SHA256CryptoServiceProvider();
            Hash = System.BitConverter.ToString(
                sha.ComputeHash(System.Text.Encoding.UTF8.GetBytes(toHash))
            ).Replace("-", "");

            CodeGen += "";

            CodeGen += "extern \"C\" void *" + ClassName + "_create() {";
            CodeGen += "return Petri::Generated::" + ClassName + "::createPetriNet().release();";
            CodeGen += "}"; // create()

            CodeGen += "";

            CodeGen += "extern \"C\" void *" + ClassName + "_createDebug() {";
            CodeGen += "return Petri::Generated::" + ClassName + "::createDebugPetriNet().release();";
            CodeGen += "}"; // createDebug()

            CodeGen += "";

            CodeGen += "extern \"C\" char *" + ClassName + "_evaluate(void *vars, char const *libPath) {";
            CodeGen += "return Petri::Utility::loadEvaluateAndInvoke(vars, libPath, \"" + Document.CodePrefix + "\");";
            CodeGen += "}";

            CodeGen += "extern \"C\" char const *" + ClassName + "_getHash() {";
            CodeGen += "return \"" + Hash + "\";";
            CodeGen += "}";

            CodeGen += "";

            CodeGen += "";
            CodeGen += "extern \"C\" void *" + ClassName + "_createLibForEditor() {";
            CodeGen += "return ::Petri::MemberPetriDynamicLib::libForEditor(\"" + Document.CodePrefix + "\", "
                           + Profile.DebugPort + ");";
            CodeGen += "}";

            CodeGen += "";

            CodeGen.Format();

            _headerGen += "/*";
            foreach(var h in SourceHeaders) {
                _headerGen += " * " + h;
            }
            _headerGen += " */";
            _headerGen.AddLine();

            _headerGen += "#ifndef PETRI_GENERATED_" + ClassName + "_H";
            _headerGen += "#define PETRI_GENERATED_" + ClassName + "_H";

            _headerGen += "";
            _headerGen += "#include <petrilab/Cpp/MemberPetriDynamicLib.h>";
            _headerGen += "#include <memory>";
            _headerGen += "";

            _headerGen += "namespace Petri {";
            _headerGen += "namespace Generated {";
            _headerGen += "namespace " + ClassName + " {";

            _headerGen += GenerateParamEnum(Document.FlatPetriNet.Parameters, null);

            _headerGen += "std::unique_ptr<::Petri::PetriNet> createPetriNet();";
            _headerGen += "std::unique_ptr<::Petri::PetriDebug> createDebugPetriNet();";
            _headerGen.AddLine();

            _headerGen += "inline std::unique_ptr<::Petri::PetriDynamicLib> createLib(std::string const &customPath = \"\") {";
            _headerGen += "return std::make_unique<::Petri::MemberPetriDynamicLib>(false, \"" + Document.CodePrefix + "\", "
                              + Profile.DebugPort + ", customPath);";
            _headerGen += "}";
            _headerGen += "}";
            _headerGen += "}";
            _headerGen += "}";

            _headerGen.AddLine();

            _headerGen += "#endif"; // ifndef header guard

            _headerGen.Format();
        }

        /// <summary>
        /// Casts an enum member to the petri net runtime's action result type.
        /// </summary>
        /// <returns>The enum member.</returns>
        /// <param name="member">Member.</param>
        /// <param name="actualCast">Whether to perform a cast.</param>
        protected override string CastEnumMember(string member, bool actualCast)
        {
            var result = Document.Settings.Enum.Name + "::" + member;
            if(actualCast) {
                result = "static_cast<actionResult_t>(" + result + ")";
            }
            return result;
        }

        /// <summary>
        /// Generates the code for an action.
        /// </summary>
        /// <param name="a">Action.</param>
        protected override void MakeActionCode(Action a)
        {
            var code = "static_cast<actionResult_t>(" + a.Invocation.MakeCode() + ")";

            var vars = new SortedSet<VariableExpression>();
            a.GetVariables(vars);

            CodeRange range = new CodeRange();
            range.FirstLine = _functionBodies.LineCount + 1;
            if(a.IsStartState) {
                _functionBodies += "extern \"C\" Petri_actionResult_t " + ClassName + "_entryInit(VarSlot &_PETRI_PRIVATE_GET_VARIABLES_) {";
                _functionBodies += "if(!_PETRI_PRIVATE_GET_VARIABLES_.isFirstSlot()) {";
                _functionBodies += "_PETRI_PRIVATE_GET_VARIABLES_.pushVariables(" + Document.FlatPetriNet.Variables.Count + ");";
                _functionBodies += "}";
                _functionBodies += "fillVariables(_PETRI_PRIVATE_GET_VARIABLES_);";
                _functionBodies += "return " + code + ";";
                _functionBodies += "}\n";
            } else {
                if(a == Document.FlatPetriNet.ExitPoint) {
                    _functionBodies += "extern \"C\" Petri_actionResult_t " + ClassName + "_exitAction(VarSlot &_PETRI_PRIVATE_GET_VARIABLES_) {";
                    _functionBodies += "auto _PETRI_PRIVATE_EXEC_RESULT_ = " + code + ";";
                    int i = 0;
                    foreach(var ret in Document.FlatPetriNet.ReturnValues) {
                        _functionBodies += "auto _PETRI_PRIVATE_GET_RETURN_VALUE_" + i + "_ = " + ret.Value.MakeCode() + ";";
                        ++i;
                    }
                    _functionBodies += "_PETRI_PRIVATE_GET_VARIABLES_.pop();";
                    _functionBodies += "_PETRI_PRIVATE_GET_VARIABLES_.pushReturnValues(" + Document.FlatPetriNet.ReturnValues.Count + ");";
                    i = 0;
                    foreach(var ret in Document.FlatPetriNet.ReturnValues) {
                        _functionBodies += "_PETRI_PRIVATE_GET_VARIABLES_[" + i + "].setName(\"" + ret.Key + "\");";
                        _functionBodies += "_PETRI_PRIVATE_GET_VARIABLES_[" + i + "].value() = _PETRI_PRIVATE_GET_RETURN_VALUE_" + i + "_;";
                        ++i;
                    }
                    _functionBodies += "return _PETRI_PRIVATE_EXEC_RESULT_;";
                } else {
                    _functionBodies += "static Petri_actionResult_t " + a.CodeIdentifier + "_invocation(VarSlot &_PETRI_PRIVATE_GET_VARIABLES_) {";
                    _functionBodies += "return " + code + ";";
                }
                _functionBodies += "}\n";
            }

            range.LastLine = _functionBodies.LineCount - 1;

            CodeRanges[a.ID] = range;

            string action = a.IsStartState ? "initEntryPtr" : (a == Document.FlatPetriNet.ExitPoint ? "exitActionPtr" : "&" + a.CodeIdentifier + "_invocation");

            CodeGen += "auto &" + a.CodeIdentifier + " = " + "petriNet.addAction("
                                + "Action(" + a.ID.ToString() + " + entitiesOffset, \"" + a.Name + "\", " + action + ", " + a.RequiredTokens.ToString() + "), "
                                + (a.IsStartState ? "firstLevel" : "false") + ");";
            if(a.TimeoutMS != 0) {
                // TODO: change unit?
                // FIXME: re-enable
                //CodeGen += a.CodeIdentifier + ".setTimeout(std::chrono::milliseconds{" + a.TimeoutMS + "});";
            }
            foreach(var v in vars) {
                CodeGen += a.CodeIdentifier + ".addVariable(" + v.EnumeratorName + ");";
            }
        }

        /// <summary>
        /// Generates the code for a transition.
        /// </summary>
        /// <param name="t">Transition.</param>
        protected override void MakeTransitionCode(Transition t)
        {
            string code = "return " + t.Condition.MakeCode() + ";";

            var vars = new SortedSet<VariableExpression>();
            t.GetVariables(vars);

            CodeRange range = new CodeRange();
            range.FirstLine = _functionBodies.LineCount + 1;
            _functionBodies += "static bool " + t.CodeIdentifier + "_invocation(VarSlot const &_PETRI_PRIVATE_GET_VARIABLES_, Petri_actionResult_t _PETRI_PRIVATE_GET_ACTION_RESULT_) {\n" + code + "\n}\n";
            range.LastLine = _functionBodies.LineCount - 1;

            CodeRanges[t.ID] = range;

            code = "&" + t.CodeIdentifier + "_invocation";

            string before, after;

            if(t.Before is ExternalInnerPetriNetProxy) {
                before = "(*reinterpret_cast<Action *>(" + t.Before.CodeIdentifier + "_endpoints.end))";
            } else {
                before = t.Before.CodeIdentifier;
            }
            if(t.After is ExternalInnerPetriNetProxy) {
                after = "(*reinterpret_cast<Action *>(" + t.After.CodeIdentifier + "_endpoints.start))";
            } else {
                after = t.After.CodeIdentifier;
            }

            var decl = vars.Count > 0 ? "auto &" + t.CodeIdentifier + " = " : "";
            CodeGen += decl + before + ".addTransition(" + t.ID.ToString() + " + entitiesOffset, \"" + t.Name + "\", " + after + ", " + code + ");";
            foreach(var v in vars) {
                CodeGen += t.CodeIdentifier + ".addVariable(" + v.EnumeratorName + ");";
            }
        }

        /// <summary>
        /// Generates the code for importing an external petri net.
        /// </summary>
        /// <param name="pn">The petri net to import.</param>
        protected override void MakeExternalPetriNetCode(ExternalInnerPetriNetProxy pn)
        {
            CodeRange range = new CodeRange();
            range.FirstLine = _functionBodies.LineCount + 1;
            _functionBodies += "static Petri_actionResult_t " + pn.CodeIdentifier + "_invocation(VarSlot &_PETRI_PRIVATE_GET_VARIABLES_) {";
            int i = 0;
            foreach(var arg in pn.Arguments) {
                _functionBodies += "auto _PETRI_PRIVATE_ARG_" + i++ + "_ = " + arg.Value.MakeCode() + ";";
            }
            _functionBodies += "auto _PETRI_PRIVATE_TEMP_ = " + pn.ClassName + "_entryInit(_PETRI_PRIVATE_GET_VARIABLES_);";
            i = 0;
            foreach(var arg in pn.Arguments) {
                _functionBodies += "_PETRI_PRIVATE_GET_VARIABLES_[" + arg.Key.GetEnumeratorName(VariableExpression.DefaultParamEnumName + "_" + pn.ClassName) + "].value() = _PETRI_PRIVATE_ARG_" + i++ + "_;";
            }
            _functionBodies += "return _PETRI_PRIVATE_TEMP_;";
            _functionBodies += "}\n";

            range.LastLine = _functionBodies.LineCount - 1;

            CodeRanges[pn.ID] = range;

            _functionBodies += "static Petri_actionResult_t " + pn.CodeIdentifier + "_fetchReturnValues(VarSlot &_PETRI_PRIVATE_GET_VARIABLES_) {";
            _functionBodies += "auto _PETRI_PRIVATE_EXEC_RESULT_ = " + pn.ClassName + "_exitAction(_PETRI_PRIVATE_GET_VARIABLES_);";
            i = 0;
            foreach(var ret in pn.ReturnValues) {
                if(pn.ReturnValuesDestination.TryGetValue(ret.Key, out VariableExpression dest)) {
                    _functionBodies += "auto _PETRI_PRIVATE_GET_RETURN_VALUE_" + dest.Expression + " = _PETRI_PRIVATE_GET_VARIABLES_[" + i + "].value(); // " + ret.Key;
                }
                ++i;
            }
            _functionBodies += "_PETRI_PRIVATE_GET_VARIABLES_.pop();";

            foreach(var ret in pn.ReturnValuesDestination) {
                _functionBodies += ret.Value.MakeCode() + " = _PETRI_PRIVATE_GET_RETURN_VALUE_" + ret.Value.Expression + ";";
            }

            _functionBodies += "return _PETRI_PRIVATE_EXEC_RESULT_;";
            _functionBodies += "}";
            _functionBodies.AddLine();

            CodeGen += "auto " + pn.CodeIdentifier + "_endpoints = "
                        + pn.ClassName + "_fill(petriNet, entitiesOffset + " + pn.EntitiesOffset + ", "
                        + (pn.IsStartState ? "firstLevel" : "false") + ", " + pn.CodeIdentifier + "_invocation, " + pn.CodeIdentifier + "_fetchReturnValues);";
            CodeGen += "(*reinterpret_cast<Action *>(" + pn.CodeIdentifier + "_endpoints.start)).setRequiredTokens(" + pn.RequiredTokens + ");";
        }

        /// <summary>
        /// Generates the enum source code that allows for the access and modifications of the petri net's variables.
        /// </summary>
        /// <returns>The variables enum.</returns>
        string GenerateVarEnum()
        {
            int var = 0;
            var vars = new List<string>((
                from p in Document.FlatPetriNet.Parameters
                select p.EnumeratorName + " = " + var++
            ).Union(
                from v in Document.FlatPetriNet.Variables
                where !Document.FlatPetriNet.Parameters.Contains(v.Key)
                select v.Key.EnumeratorName + " = " + var++
            ));
            if(vars.Count > 0) {
                return "enum " + VariableExpression.DefaultVarEnumName + " : std::uint_fast32_t {" + string.Join(", ", vars) + "};\n";
            }

            return null;
        }

        /// <summary>
        /// Generates the enum source code that allows for setting the petri net's parameters.
        /// </summary>
        /// <returns>The parameters enum.</returns>
        string GenerateParamEnum(SortedSet<VariableExpression> parameters, string className)
        {
            int var = 0;
            var vars = from v in parameters
                       select v.GetEnumeratorName(VariableExpression.DefaultParamEnumName + (className != null ? "_" + className : "")) + " = " + var++;
            if(parameters.Count > 0) {
                return "enum " + VariableExpression.DefaultParamEnumName + (className != null ? "_" + className : "") + " : std::uint_fast32_t {" + string.Join(", ", vars) + "};\n";
            }

            return null;
        }
    }
}
