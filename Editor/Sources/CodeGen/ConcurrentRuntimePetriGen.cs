//
//  ConcurrentRuntimePetriGen.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2015-06-29.
//

using System.Collections.Generic;
using System.Linq;

using Petri.Code;
using Petri.Model;

namespace Petri.CodeGen
{
    /// <summary>
    /// A class allowing for the generation of a petri net's source code.
    /// </summary>
    public abstract class ConcurrentRuntimePetriGen : PetriGen
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.CodeGen.ConcurrentRuntimePetriGen"/> class.
        /// </summary>
        /// <param name="doc">Document.</param>
        /// <param name="language">Language.</param>
        /// <param name="generator">Generator.</param>
        protected ConcurrentRuntimePetriGen(Application.Document doc,
                                            Language language,
                                            CodeGen generator) : base(doc, language, generator)
        {
        }

        /// <summary>
        /// Called before any entity generation and gives the opportunity to emit some setup code.
        /// </summary>
        protected abstract void Begin();

        /// <summary>
        /// Generates the code for an action.
        /// </summary>
        /// <param name="a">Action.</param>
        protected abstract void GenerateAction(Action a);

        /// <summary>
        /// Generates the code for a transition.
        /// </summary>
        /// <param name="t">Transition.</param>
        protected abstract void GenerateTransition(Transition t);

        /// <summary>
        /// Generates the code for importing an external petri net.
        /// </summary>
        /// <param name="pn">The petri net to import.</param>
        protected abstract void GenerateExternalPetriNet(ExternalInnerPetriNetProxy pn);

        /// <summary>
        /// Finishes the code generation and compute the Hash value of the petri net.
        /// </summary>
        protected abstract void End();

        /// <summary>
        /// Generates the source code for the petri net.
        /// </summary>
        void GeneratePetriNet()
        {
            foreach(var s in Document.FlatPetriNet.States) {
                if(Document.FlatPetriNet.IsReachable(s)) {
                    if(s is Action) {
                        GenerateAction((Action)s);
                    } else {
                        GenerateExternalPetriNet((ExternalInnerPetriNetProxy)s);
                    }
                }
            }

            CodeGen += "\n";

            foreach(Transition t in Document.FlatPetriNet.Transitions) {
                if(Document.FlatPetriNet.IsReachable(t.Before)
                       && Document.FlatPetriNet.IsReachable(t.After)) {
                    GenerateTransition(t);
                }
            }
        }

        /// <summary>
        /// Gets the hash of the petri net by generating the code and discarding it.
        /// </summary>
        /// <returns>The hash.</returns>
        public override string GetHash()
        {
            // Codegen
            GetGeneratedCode();

            return Hash;
        }

        /// <summary>
        /// Generates the code for the petri net of the document.
        /// </summary>
        public override void WritePetriNet()
        {
            var value = GetGeneratedCode();

            System.IO.File.WriteAllText(
                PathToFile(Document.Settings.Name + PetriGen.SourceExtensionFromLanguage(Language)),
                value
            );
        }

        /// <summary>
        /// Gets the petri net generated code.
        /// </summary>
        /// <returns>The petri net generated code.</returns>
        public override string GetGeneratedCode()
        {
            if(CodeGen.Empty) {
                Begin();
                GeneratePetriNet();
                End();
            }

            return CodeGen.Value;
        }

        /// <summary>
        /// Gets or sets a value indicating this instance hash.
        /// </summary>
        protected string Hash {
            get;
            set;
        }

        /// <summary>
        /// Gets the name of the class name.
        /// </summary>
        /// <value>The name of the class.</value>
        protected string ClassName {
            get {
                return Document.Settings.Name;
            }
        }

        /// <summary>
        /// Gets the external petri nets's class names.
        /// </summary>
        /// <value>The external class names.</value>
        protected HashSet<ExternalInnerPetriNetProxy> UniqueExternalPetriNets {
            get {

                return new HashSet<ExternalInnerPetriNetProxy>(from pn in Document.FlatPetriNet.ExternalPetriNets select pn, new ExternalPathComparer());
            }
        }

        class ExternalPathComparer : IEqualityComparer<ExternalInnerPetriNetProxy>
        {
            bool IEqualityComparer<ExternalInnerPetriNetProxy>.Equals(ExternalInnerPetriNetProxy x, ExternalInnerPetriNetProxy y)
            {
                return x.Path == y.Path;
            }

            int IEqualityComparer<ExternalInnerPetriNetProxy>.GetHashCode(ExternalInnerPetriNetProxy obj)
            {
                return obj.Path.GetHashCode();
            }
        }
    }
}
