//
//  State.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2014-11-13.
//

using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Linq;

namespace Petri.Model
{
    /// <summary>
    /// A state of a petri net.
    /// </summary>
    public abstract class State : Entity
    {
        /// <summary>
        /// Creates a state from the given XML serialized form
        /// </summary>
        /// <returns>The from xml.</returns>
        /// <param name="factory">The entity factory.</param>
        /// <param name="descriptor">The XML serialization of the state.</param>
        /// <param name="parent">The petri net to put the state into.</param>
        /// <param name="isLoadingDocument">Whether the state is created while a document is being constructed.</param>
        public static State FromXml(EntityFactory factory,
                                    XElement descriptor,
                                    PetriNet parent,
                                    bool isLoadingDocument)
        {
            switch(descriptor.Name.ToString()) {
            case "Action":
                return new Action(factory, parent, descriptor);
            case "Exit":
                return new ExitPoint(factory, parent, descriptor);
            case "PetriNet":
                if(parent == null) {
                    // Should not happen
                    break;
                } else {
                    return new LocalInnerPetriNet(factory, parent, descriptor);
                }
            case "PetriNetRef":
                var pn = new ExternalInnerPetriNet(factory, parent, descriptor);
                if(isLoadingDocument) { // If we are loading the document
                    pn.Document.FirstLevelExternalPetriNets.Add(pn);
                }
                return pn;
            case "PetriNetPlaceholder":
                return new ExternalInnerPetriNetXmlPlaceholder(factory, parent, descriptor);
            }

            throw new ArgumentException(string.Format("Invalid entity type {0}", descriptor.Name));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Model.State"/> class.
        /// </summary>
        /// <param name="factory">The entity factory.</param>
        /// <param name="parent">Parent.</param>
        /// <param name="active">If set to <c>true</c> then the state is active at t=0.</param>
        /// <param name="requiredTokens">Required tokens.</param>
        /// <param name="pos">Position.</param>
        protected State(EntityFactory factory,
                        PetriNet parent,
                        bool active,
                        UInt32 requiredTokens,
                        Point pos) : base(factory, parent)
        {
            TransitionsBefore = new HashSet<Transition>();
            TransitionsAfter = new HashSet<Transition>();

            IsStartState = active;
            RequiredTokens = requiredTokens;
            Position = pos;
            Name = "";
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Model.State"/> class.
        /// </summary>
        /// <param name="factory">The entity factory.</param>
        /// <param name="parent">Parent.</param>
        /// <param name="descriptor">Descriptor.</param>
        protected State(EntityFactory factory, PetriNet parent, XElement descriptor) : base(factory,
                                                                                            parent,
                                                                                            descriptor)
        {
            TransitionsBefore = new HashSet<Transition>();
            TransitionsAfter = new HashSet<Transition>();

            IsStartState = XmlConvert.ToBoolean(descriptor.Attribute("Active").Value);
            RequiredTokens = XmlConvert.ToUInt32(descriptor.Attribute("RequiredTokens").Value);
            Radius = XmlConvert.ToDouble(descriptor.Attribute("Radius").Value);
        }

        /// <summary>
        /// Serializes the instance into the provided XML element.
        /// </summary>
        /// <param name="element">The XML element to fill with the serialization data.</param>
        protected override void Serialize(XElement element)
        {
            base.Serialize(element);
            element.SetAttributeValue("Active", IsStartState);
            element.SetAttributeValue("RequiredTokens", RequiredTokens);
            element.SetAttributeValue("Radius", Radius);
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="Petri.Model.State"/> is active when the petri net starts its execution.
        /// </summary>
        /// <value><c>true</c> if active; otherwise, <c>false</c>.</value>
        public virtual bool IsStartState {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the required tokens that must be brought by transitions to activate the state.
        /// </summary>
        /// <value>The required tokens.</value>
        public virtual UInt32 RequiredTokens {
            get;
            set;
        }

        /// <summary>
        /// Gets the list of transitions that lead to <c>this</c>.
        /// </summary>
        /// <value>The transitions before.</value>
        public HashSet<Transition> TransitionsBefore {
            get;
            private set;
        }

        /// <summary>
        /// Gets the list of transitions that go from <c>this</c> to another state.
        /// </summary>
        /// <value>The transitions after.</value>
        public HashSet<Transition> TransitionsAfter {
            get;
            private set;
        }

        /// <summary>
        /// Adds a transition before.
        /// <see cref="Petri.Model.State.TransitionsBefore"/>
        /// </summary>
        /// <param name="transition">The transition.</param>
        public void AddTransitionBefore(Transition transition)
        {
            TransitionsBefore.Add(transition);
        }

        /// <summary>
        /// Adds a transition after.
        /// <see cref="Petri.Model.State.TransitionsAfter"/>
        /// </summary>
        /// <param name="transition">The transition.</param>
        public void AddTransitionAfter(Transition transition)
        {
            TransitionsAfter.Add(transition);
        }

        /// <summary>
        /// Removes a transition before.
        /// <see cref="Petri.Model.State.TransitionsBefore"/>
        /// </summary>
        /// <param name="t">The transition to remove.</param>
        public void RemoveTransitionBefore(Transition t)
        {
            TransitionsBefore.Remove(t);
        }

        /// <summary>
        /// Removes a transition after.
        /// <see cref="Petri.Model.State.TransitionsAfter"/>
        /// </summary>
        /// <param name="t">The transition to remove.</param>
        public void RemoveTransitionAfter(Transition t)
        {
            TransitionsAfter.Remove(t);
        }

        /// <summary>
        /// Gets a value indicating whether this <see cref="T:Petri.Model.State"/> is reachable by the root petri net's execution flow.
        /// </summary>
        /// <value><c>true</c> if is reachable; otherwise, <c>false</c>.</value>
        public bool IsReachable {
            get {
                return Document.IsReachable(this);
            }
        }

        /// <summary>
        /// Gets or sets the position as in <c>Entity.Position</c>, except that the transitions attached to this instance are given a chance to gracefully update their position.
        /// </summary>
        /// <value>The position.</value>
        public override Point Position {
            get {
                return base.Position;
            }
            set {
                // Prevent execution during State construction
                if(TransitionsBefore != null) {
                    foreach(Transition t in TransitionsBefore) {
                        t.FollowStatesPosition(t.Before.Position, value);
                    }
                    foreach(Transition t in TransitionsAfter) {
                        t.FollowStatesPosition(value, t.After.Position);
                    }
                }
                base.Position = value;
            }
        }

        /// <summary>
        /// Gets or sets the radius of the circle representing the state.
        /// </summary>
        /// <value>The radius.</value>
        public double Radius {
            get;
            set;
        }

        /// <summary>
        /// Gets the code identifier of the entity, i.e. the name of the variable containing the entity in the generated code.
        /// </summary>
        /// <value>The code identifier.</value>
        public override string CodeIdentifier {
            get {
                return "state_" + ID.ToString();
            }
        }

        /// <summary>
        /// Checks whether the parameter is enclosed into this instance's shape.
        /// </summary>
        /// <returns><c>true</c>, if in <c>point</c> is contained by this instance, <c>false</c> otherwise.</returns>
        /// <param name="p">P.</param>
        public virtual bool PointInState(Point p)
        {
            if((p - Position).SquaredNorm < Math.Pow(Radius, 2)) {
                return true;
            }

            return false;
        }
    }
}
