//
//  ISourceCodeProvider.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2014-12-09.
//

using System.Collections.Generic;

using Petri.Code;

namespace Petri.Model
{
    /// <summary>
    /// Source code provider.
    /// </summary>
    public interface ISourceCodeProvider
    {
        /// <summary>
        /// Gets the enum.
        /// </summary>
        /// <value>The enum.</value>
        Enum Enum {
            get;
        }

        /// <summary>
        /// Gets the language.
        /// </summary>
        /// <value>The language.</value>
        Language Language {
            get;
        }

        /// <summary>
        /// Gets all the known functions.
        /// </summary>
        /// <value>All functions.</value>
        IEnumerable<Function> AllFunctions {
            get;
        }

    }
}
