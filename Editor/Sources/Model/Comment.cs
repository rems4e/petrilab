//
//  Comment.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2014-12-09.
//

using System.Xml;
using System.Collections.Generic;
using System.Xml.Linq;

using Cairo;

namespace Petri.Model
{
    /// <summary>
    /// A graphical entity containing a text comment. It does not interact in any way with the other Entity subclasses, other than being contained in PetriNets.
    /// Only for documentation/hints purpose.
    /// </summary>
    public class Comment : Entity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Model.Comment"/> class.
        /// </summary>
        /// <param name="factory">The entity factory.</param>
        /// <param name="parent">Parent.</param>
        /// <param name="comment">The content of the comment.</param>
        /// <param name="pos">Position.</param>
        public Comment(EntityFactory factory,
                       PetriNet parent,
                       string comment,
                       Point pos = new Point()) : base(factory, parent)
        {
            Position = pos;
            Name = comment;
            SizeToFit();

            Color = new Color(1, 1, 0.7, 1);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Model.Comment"/> class.
        /// </summary>
        /// <param name="factory">The entity factory.</param>
        /// <param name="parent">Parent.</param>
        /// <param name="descriptor">Descriptor.</param>
        public Comment(EntityFactory factory, PetriNet parent, XElement descriptor) : base(factory,
                                                                                           parent,
                                                                                           descriptor)
        {
            var size = new Vector();
            size.X = XmlConvert.ToDouble(descriptor.Attribute("Width").Value);
            Size = size;

            var color = new Color();
            color.R = XmlConvert.ToDouble(descriptor.Attribute("R").Value);
            color.G = XmlConvert.ToDouble(descriptor.Attribute("G").Value);
            color.B = XmlConvert.ToDouble(descriptor.Attribute("B").Value);
            color.A = XmlConvert.ToDouble(descriptor.Attribute("A").Value);
            Color = color;
        }

        /// <summary>
        /// Creates an empty node correctly named to represent an entity, and fill it with the serialization data.
        /// </summary>
        /// <returns>The new, populated node.</returns>
        public override XElement GetXML()
        {
            var elem = new XElement("Comment");
            Serialize(elem);
            return elem;
        }

        /// <summary>
        /// Serializes the instance into the provided XML element.
        /// </summary>
        /// <param name="element">The XML element to fill with the serialization data.</param>
        protected override void Serialize(XElement element)
        {
            base.Serialize(element);
            element.SetAttributeValue("Width", Size.X);

            element.SetAttributeValue("R", Color.R);
            element.SetAttributeValue("G", Color.G);
            element.SetAttributeValue("B", Color.B);
            element.SetAttributeValue("A", Color.A);
        }

        /// <summary>
        /// Gets or sets the size of the comment's frame.
        /// </summary>
        /// <value>The size.</value>
        public Vector Size {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the color of the comment's background.
        /// </summary>
        /// <value>The color.</value>
        public Color Color {
            get;
            set;
        }

        /// <summary>
        /// Nothing to see here, as the Entity is not code generated at all.
        /// </summary>
        /// <returns><c>true</c>, if function was used, <c>false</c> otherwise.</returns>
        /// <param name="f">F.</param>
        public override bool UsesFunction(Code.Function f)
        {
            return false;
        }

        /// <summary>
        /// Adds the VariableExpressions contained in the entity to the collection passed as an argument.
        /// </summary>
        /// <param name="res">Result.</param>
        public override void GetVariables(ICollection<Code.VariableExpression> res)
        {
        }


        /// <summary>
        /// Nothing to see here, as the Entity is not code generated at all.
        /// </summary>
        /// <value>The code identifier.</value>
        public override string CodeIdentifier {
            get {
                return null;
            }
        }

        /// <summary>
        /// Sets the size of the comment to match the size of the rendered comment's text.
        /// </summary>
        public void SizeToFit()
        {
            var screen = Gdk.Screen.Default;
            if(screen != null) {
                var layout = new Pango.Layout(Gdk.PangoHelper.ContextGet());

                layout.FontDescription = new Pango.FontDescription();
                layout.FontDescription.Family = "Arial";
                layout.FontDescription.Size = Pango.Units.FromPixels(12);

                layout.SetText(Name);

                int width, height;
                layout.GetPixelSize(out width, out height);

                Size = new Vector(width + 10, height + 10);
            }
        }
    }
}
