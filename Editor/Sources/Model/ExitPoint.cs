//
//  ExitPoint.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2014-11-13.
//

using System;
using System.Xml.Linq;

namespace Petri.Model
{
    /// <summary>
    /// The exit point of an inner petri net.
    /// </summary>
    public class ExitPoint : Action
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Model.ExitPoint"/> class.
        /// </summary>
        /// <param name="factory">The entity factory.</param>
        /// <param name="parent">Parent.</param>
        /// <param name="pos">Position.</param>
        public ExitPoint(EntityFactory factory, PetriNet parent, Point pos) : base(factory,
                                                                                   parent,
                                                                                   false,
                                                                                   pos)
        {
            Radius = 25;
            Name = "End";
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Model.ExitPoint"/> class.
        /// </summary>
        /// <param name="factory">The entity factory.</param>
        /// <param name="parent">Parent.</param>
        /// <param name="descriptor">Descriptor.</param>
        public ExitPoint(EntityFactory factory, PetriNet parent, XElement descriptor) : base(factory,
                                                                                             parent,
                                                                                             descriptor)
        {
            Name = "End";
        }

        /// <summary>
        /// Creates an empty node correctly named to represent an entity, and fill it with the serialization data.
        /// </summary>
        /// <returns>The new, populated node.</returns>
        public override XElement GetXML()
        {
            var elem = new XElement("Exit");
            Serialize(elem);
            return elem;
        }

        /// <summary>
        /// Forces the value to <c>false</c> as the instance can never be active.
        /// </summary>
        /// <value><c>true</c> if active; otherwise, <c>false</c>.</value>
        public override bool IsStartState {
            get {
                return false;
            }
            set {
                base.IsStartState = false;
            }
        }

        /// <summary>
        /// Always require as much token as there are transitions leading to <c>this</c>, so that every state is sync'ed before exiting.
        /// </summary>
        /// <value>The required tokens.</value>
        public override UInt32 RequiredTokens {
            get {
                return (UInt32)TransitionsBefore.Count;
            }
            set {
            }
        }

        /// <summary>
        /// Gets a plain action from this instance.
        /// </summary>
        /// <returns>The action.</returns>
        public Action ToAction()
        {
            var xml = GetXML();
            xml.Name = "Action";
            xml.SetAttributeValue("Radius", 20);

            return new Action(Document.EntityFactory, Parent, xml);
        }
    }
}

