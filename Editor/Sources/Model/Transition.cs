//
//  Transition.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2014-11-13.
//

using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Linq;

using Petri.Code;

namespace Petri.Model
{
    /// <summary>
    /// A transition between two states of the petri net. The transition is only crossed when its condition is matched.
    /// </summary>
    public class Transition : Entity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Model.Transition"/> class.
        /// </summary>
        /// <param name="factory">The entity factory.</param>
        /// <param name="parent">Parent.</param>
        /// <param name="before">Before.</param>
        /// <param name="after">After.</param>
        public Transition(EntityFactory factory, PetriNet parent, State before, State after) : base(factory, parent)
        {
            Before = before;
            After = after;

            Name = "";

            Width = 50;
            Height = 30;

            base.Position = new Point();

            Condition = factory.TrueExpression;

            Position = GetPosition(Before.Position, After.Position) + GetDefaultShift(Before == After);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Model.Transition"/> class.
        /// </summary>
        /// <param name="factory">The entity factory.</param>
        /// <param name="parent">Parent.</param>
        /// <param name="descriptor">Descriptor.</param>
        /// <param name="statesTable">A table allowing retrieval of a state by its ID Used for transitions creation.</param>
        public Transition(EntityFactory factory,
                          PetriNet parent,
                          XElement descriptor,
                          IDictionary<UInt64, State> statesTable) : base(factory,
                                                                         parent,
                                                                         descriptor)
        {
            string id = null;
            try {
                id = descriptor.Attribute("BeforeID").Value;
                Before = statesTable[UInt64.Parse(id)];
                id = descriptor.Attribute("AfterID").Value;
                After = statesTable[UInt64.Parse(id)];
            } catch(KeyNotFoundException) {
                throw new ArgumentException(string.Format("The state with ID {0} was not found in the descriptor.", id));
            }
            Condition = factory.TryGetCondition(descriptor.Attribute("Condition").Value);

            Width = double.Parse(descriptor.Attribute("W").Value);
            Height = double.Parse(descriptor.Attribute("H").Value);

            // Updates the shift values.
            Position = Position;
        }

        /// <summary>
        /// Creates an empty node correctly named to represent an entity, and fill it with the serialization data.
        /// </summary>
        /// <returns>The new, populated node.</returns>
        public override XElement GetXML()
        {
            var elem = new XElement("Transition");
            Serialize(elem);
            return elem;
        }

        /// <summary>
        /// Serializes the instance into the provided XML element.
        /// </summary>
        /// <param name="element">The XML element to fill with the serialization data.</param>
        protected override void Serialize(XElement element)
        {
            base.Serialize(element);
            element.SetAttributeValue("BeforeID", Before.ID);
            element.SetAttributeValue("AfterID", After.ID);

            element.SetAttributeValue("Condition", Condition.MakeUserReadable());

            element.SetAttributeValue("W", Width);
            element.SetAttributeValue("H", Height);
        }

        /// <summary>
        /// Checks if the entity uses the specified function.
        /// </summary>
        /// <returns>true</returns>
        /// <param name="f">The function to check.</param>
        public override bool UsesFunction(Function f)
        {
            return Condition.UsesFunction(f);
        }

        /// <summary>
        /// Gets the global direction of the transition, i.e. the direction of the vector going from one end of the transtition to the other.
        /// If the transition comes from and goes from the same state, then a not null vector is returned.
        /// </summary>
        /// <value>The direction.</value>
        public Vector Direction {
            get {
                if(Before == After) {
                    var dir = Before.Position - Position;
                    return dir;
                }

                return After.Position - Before.Position;
            }
        }

        /// <summary>
        /// Updates the position of the transition to follow an update of its ends' positions.
        /// The transition keeps its 'height' with regards to the (begin-end) line, but moves proportionnally accross this direction.
        /// </summary>
        /// <param name="newBeforePosition">The new position of the before node.</param>
        /// <param name="newAfterPosition">The new position of the after node.</param>
        public void FollowStatesPosition(Point newBeforePosition, Point newAfterPosition)
        {
            var oldDirection = After.Position - Before.Position;
            var newDirection = newAfterPosition - newBeforePosition;
            if(Math.Min(oldDirection.Norm, newDirection.Norm) > 1e-3) {
                var beforeToTransition = Position - Before.Position;
                var normalizedDirection = oldDirection.Normalized;
                var projectedTransitionLength = beforeToTransition.X * normalizedDirection.X + beforeToTransition.Y * normalizedDirection.Y;
                var directionToTransition = beforeToTransition - projectedTransitionLength * normalizedDirection;

                var newDirectionNormalized = newDirection.Normalized;
                var newProjectedTransition = newBeforePosition + newDirection * (projectedTransitionLength / oldDirection.Norm);
                var newDirectionNormal = new Vector(newDirectionNormalized.Y, -newDirectionNormalized.X);
                newDirectionNormal *= Math.Sign(newDirectionNormal.X * directionToTransition.X + newDirectionNormal.Y * directionToTransition.Y);
                Position = newProjectedTransition + newDirectionNormal * directionToTransition.Norm;
            }
        }

        /// <summary>
        /// Computes the position of a transition from its origin, destination and shift values.
        /// </summary>
        /// <returns>The position.</returns>
        /// <param name="origin">Origin.</param>
        /// <param name="destination">Destination.</param>
        public static Point GetPosition(Point origin, Point destination)
        {
            var center = new Point((origin.X + destination.X) / 2,
                                   (origin.Y + destination.Y) / 2);
            return center;
        }

        /// <summary>
        /// Gets the default shift for a transition.
        /// </summary>
        /// <value>The default shift.</value>
        public static Vector GetDefaultShift(bool isSame)
        {
            if(isSame) {
                return new Vector(60, 60);
            }

            return new Vector(0, 0);
        }

        /// <summary>
        /// Gets or sets the state before, i.e. NOT the one pointed by the transition's arrow.
        /// </summary>
        /// <value>The state before.</value>
        public State Before {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the state after, i.e. the one pointed by the transition's arrow.
        /// </summary>
        /// <value>The state after.</value>
        public State After {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the width of the transition's shape.
        /// </summary>
        /// <value>The width.</value>
        public double Width {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the height of the transition's shape.
        /// </summary>
        /// <value>The height.</value>
        public double Height {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the expression that is evaluated when the question "Can the transition be crossed?" is asked.
        /// </summary>
        /// <value>The condition.</value>
        public Expression Condition {
            get;
            set;
        }

        /// <summary>
        /// Gets the code identifier of the entity, i.e. the name of the variable containing the entity in the generated code.
        /// </summary>
        /// <value>The code identifier.</value>
        public override string CodeIdentifier {
            get {
                return "transition_" + ID.ToString();
            }
        }

        /// <summary>
        /// Gets a value indicating whether this <see cref="Petri.Model.Transition"/> stick to grid.
        /// </summary>
        /// <value><c>true</c> if stick to grid; otherwise, <c>false</c>.</value>
        public override bool StickToGrid {
            get {
                return false;
            }
        }

        /// <summary>
        /// Adds the VariableExpressions contained in the entity to the collection passed as an argument.
        /// </summary>
        /// <param name="result">Result.</param>
        public override void GetVariables(ICollection<VariableExpression> result)
        {
            var l = Condition.GetVariables();
            foreach(var ll in l) {
                result.Add(ll);
            }
        }
    }
}
