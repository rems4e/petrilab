//
//  Vector.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-10-02.
//

namespace Petri.Model
{
    /// <summary>
    /// A vector type that supports basic operations.
    /// </summary>
    public struct Vector
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Model.Vector"/> struct by copying from another instance.
        /// </summary>
        /// <param name="v">The vector to get the coordinates from.</param>
        public Vector(Vector v) : this(v.X, v.Y)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Model.Vector"/> struct by settings the coordinates to the same specified value.
        /// </summary>
        /// <param name="xy">The value of both coordinates.</param>
        public Vector(double xy)
        {
            X = xy;
            Y = xy;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Model.Vector"/> struct.
        /// </summary>
        /// <param name="x">The x coordinate.</param>
        /// <param name="y">The y coordinate.</param>
        public Vector(double x, double y)
        {
            X = x;
            Y = y;
        }

        /// <summary>
        /// Gets or sets the X coordinate of the vector.
        /// </summary>
        /// <value>The x coordinate of the vector.</value>
        public double X {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the Y coordinate.
        /// </summary>
        /// <value>The y coordinate of the vector.</value>
        public double Y {
            get;
            set;
        }

        /// <summary>
        /// Gets the norm of the vector.
        /// </summary>
        /// <value>The norm.</value>
        public double Norm {
            get {
                return System.Math.Sqrt(SquaredNorm);
            }
        }

        /// <summary>
        /// Gets the squared norm of the vector.
        /// </summary>
        /// <value>The squared norm.</value>
        public double SquaredNorm {
            get {
                return X * X + Y * Y;
            }
        }

        /// <summary>
        /// Gets a new normalized vector with the same direction as <c>this</c>.
        /// </summary>
        /// <value>The normalized.</value>
        public Vector Normalized {
            get {
                return this / Norm;
            }
        }

        /// <summary>
        /// Adds a <see cref="Petri.Model.Vector"/> to a <see cref="Petri.Model.Point"/>, yielding a new <see cref="T:Petri.Model.Point"/>.
        /// </summary>
        /// <param name="v1">The first <see cref="Petri.Model.Vector"/> to add.</param>
        /// <param name="v2">The second <see cref="Petri.Model.Point"/> to add.</param>
        /// <returns>The <see cref="T:Petri.Model.Point"/> that is the sum of the values of <c>v1</c> and <c>v2</c>.</returns>
        public static Point operator +(Vector v1, Point v2)
        {
            return new Point(v1.X + v2.X, v1.Y + v2.Y);
        }

        /// <summary>
        /// Adds a <see cref="Petri.Model.Point"/> to a <see cref="Petri.Model.Vector"/>, yielding a new <see cref="T:Petri.Model.Point"/>.
        /// </summary>
        /// <param name="v1">The first <see cref="Petri.Model.Point"/> to add.</param>
        /// <param name="v2">The second <see cref="Petri.Model.Vector"/> to add.</param>
        /// <returns>The <see cref="T:Petri.Model.Point"/> that is the sum of the values of <c>v1</c> and <c>v2</c>.</returns>
        public static Point operator +(Point v1, Vector v2)
        {
            return new Point(v1.X + v2.X, v1.Y + v2.Y);
        }

        /// <summary>
        /// Adds a <see cref="Petri.Model.Vector"/> to a <see cref="Petri.Model.Vector"/>, yielding a new <see cref="T:Petri.Model.Vector"/>.
        /// </summary>
        /// <param name="v1">The first <see cref="Petri.Model.Vector"/> to add.</param>
        /// <param name="v2">The second <see cref="Petri.Model.Vector"/> to add.</param>
        /// <returns>The <see cref="T:Petri.Model.Vector"/> that is the sum of the values of <c>v1</c> and <c>v2</c>.</returns>
        public static Vector operator +(Vector v1, Vector v2)
        {
            return new Vector(v1.X + v2.X, v1.Y + v2.Y);
        }

        /// <summary>
        /// Subtracts a <see cref="Petri.Model.Vector"/> from a <see cref="Petri.Model.Point"/>, yielding a new <see cref="T:Petri.Model.Point"/>.
        /// </summary>
        /// <param name="v1">The <see cref="Petri.Model.Vector"/> to subtract from (the minuend).</param>
        /// <param name="v2">The <see cref="Petri.Model.Point"/> to subtract (the subtrahend).</param>
        /// <returns>The <see cref="T:Petri.Model.Point"/> that is the <c>v1</c> minus <c>v2</c>.</returns>
        public static Point operator -(Vector v1, Point v2)
        {
            return new Point(v1.X - v2.X, v1.Y - v2.Y);
        }

        /// <summary>
        /// Subtracts a <see cref="Petri.Model.Point"/> from a <see cref="Petri.Model.Vector"/>, yielding a new <see cref="T:Petri.Model.Point"/>.
        /// </summary>
        /// <param name="v1">The <see cref="Petri.Model.Point"/> to subtract from (the minuend).</param>
        /// <param name="v2">The <see cref="Petri.Model.Vector"/> to subtract (the subtrahend).</param>
        /// <returns>The <see cref="T:Petri.Model.Point"/> that is the <c>v1</c> minus <c>v2</c>.</returns>
        public static Point operator -(Point v1, Vector v2)
        {
            return new Point(v1.X - v2.X, v1.Y - v2.Y);
        }

        /// <summary>
        /// Subtracts a <see cref="Petri.Model.Vector"/> from a <see cref="Petri.Model.Vector"/>, yielding a new <see cref="T:Petri.Model.Vector"/>.
        /// </summary>
        /// <param name="v1">The <see cref="Petri.Model.Vector"/> to subtract from (the minuend).</param>
        /// <param name="v2">The <see cref="Petri.Model.Vector"/> to subtract (the subtrahend).</param>
        /// <returns>The <see cref="T:Petri.Model.Vector"/> that is the <c>v1</c> minus <c>v2</c>.</returns>
        public static Vector operator -(Vector v1, Vector v2)
        {
            return new Vector(v1.X - v2.X, v1.Y - v2.Y);
        }

        /// <summary>
        /// Gets the opposite of this instance.
        /// </summary>
        /// <param name="v">The <see cref="Petri.Model.Vector"/> to get the opposite.</param>
        /// <returns>The opposite of the <see cref="T:Petri.Model.Vector"/>.</returns>
        public static Vector operator -(Vector v)
        {
            return new Vector(-v.X, -v.Y);
        }

        /// <summary>
        /// Computes the product of <c>a</c> and <c>v</c>, yielding a new <see cref="T:Petri.Model.Vector"/>.
        /// </summary>
        /// <param name="a">The <see cref="double"/> to multiply.</param>
        /// <param name="v">The <see cref="Petri.Model.Vector"/> to multiply.</param>
        /// <returns>The <see cref="T:Petri.Model.Vector"/> that is the <c>a</c> * <c>v</c>.</returns>
        public static Vector operator *(double a, Vector v)
        {
            return new Vector(a * v.X, a * v.Y);
        }

        /// <summary>
        /// Computes the product of <c>v</c> and <c>a</c>, yielding a new <see cref="T:Petri.Model.Vector"/>.
        /// </summary>
        /// <param name="v">The <see cref="Petri.Model.Vector"/> to multiply.</param>
        /// <param name="a">The <see cref="double"/> to multiply.</param>
        /// <returns>The <see cref="T:Petri.Model.Vector"/> that is the <c>v</c> * <c>a</c>.</returns>
        public static Vector operator *(Vector v, double a)
        {
            return new Vector(v.X * a, v.Y * a);
        }

        /// <summary>
        /// Computes the division of <c>v</c> and <c>a</c>, yielding a new <see cref="T:Petri.Model.Vector"/>.
        /// </summary>
        /// <param name="v">The <see cref="Petri.Model.Vector"/> to divide (the divident).</param>
        /// <param name="a">The <see cref="double"/> to divide (the divisor).</param>
        /// <returns>The <see cref="T:Petri.Model.Vector"/> that is the <c>v</c> / <c>a</c>.</returns>
        public static Vector operator /(Vector v, double a)
        {
            return new Vector(v.X / a, v.Y / a);
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:Petri.Model.Vector"/>.
        /// </summary>
        /// <returns>A <see cref="T:System.String"/> that represents the current <see cref="T:Petri.Model.Vector"/>.</returns>
        public override string ToString()
        {
            return string.Format("{{{0}, {1}}}", X, Y);
        }
    }
}
