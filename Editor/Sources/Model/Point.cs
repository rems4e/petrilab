//
//  Point.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-01.
//

namespace Petri.Model
{
    /// <summary>
    /// A struct that models a 2D point/
    /// </summary>
    public struct Point
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Model.Point"/> struct by copying the specified instance.
        /// </summary>
        /// <param name="p">P.</param>
        public Point(Point p) : this(p.X, p.Y)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Model.Point"/> struct.
        /// </summary>
        /// <param name="x">The x coordinate.</param>
        /// <param name="y">The y coordinate.</param>
        public Point(double x, double y)
        {
            X = x;
            Y = y;
        }

        /// <summary>
        /// Gets or sets the X coordinate.
        /// </summary>
        /// <value>The x coordinate of the point.</value>
        public double X {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the Y coordinate.
        /// </summary>
        /// <value>The y coordinate of the point.</value>
        public double Y {
            get;
            set;
        }

        /// <summary>
        /// Subtracts a <see cref="Petri.Model.Point"/> from a <see cref="Petri.Model.Point"/>, yielding a new <see cref="T:Petri.Model.Vector"/>.
        /// </summary>
        /// <param name="v1">The <see cref="Petri.Model.Point"/> to subtract from (the minuend).</param>
        /// <param name="v2">The <see cref="Petri.Model.Point"/> to subtract (the subtrahend).</param>
        /// <returns>The <see cref="T:Petri.Model.Vector"/> that is the <c>v1</c> minus <c>v2</c>.</returns>
        public static Vector operator -(Point v1, Point v2)
        {
            return new Vector(v1.X - v2.X, v1.Y - v2.Y);
        }

        /// <summary>
        /// Implicit conversion to <see cref="T:Cairo.PointD"/>.
        /// </summary>
        /// <returns>The new point.</returns>
        /// <param name="p">The operand to convert.</param>
        public static implicit operator Cairo.PointD(Point p)
        {
            return new Cairo.PointD(p.X, p.Y);
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:Petri.Model.Point"/>.
        /// </summary>
        /// <returns>A <see cref="T:System.String"/> that represents the current <see cref="T:Petri.Model.Point"/>.</returns>
        public override string ToString()
        {
            return string.Format("{{{0}, {1}}}", X, Y);
        }
    }
}
