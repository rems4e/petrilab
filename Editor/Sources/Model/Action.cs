//
//  Action.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2014-11-13.
//

using System;
using System.Collections.Generic;
using System.Xml.Linq;

using Petri.Code;

namespace Petri.Model
{
    /// <summary>
    /// A state that has an action attached to it and which will be executed every time the state gets active.
    /// </summary>
    public class Action : State
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Model.Action"/> class.
        /// </summary>
        /// <param name="factory">The entity factory.</param>
        /// <param name="parent">The parent petri net.</param>
        /// <param name="active">If set to <c>true</c>, the state is active at t=0.</param>
        /// <param name="pos">Position.</param>
        public Action(EntityFactory factory,
                      PetriNet parent,
                      bool active,
                      Point pos = new Point()) : base(factory,
                                                      parent,
                                                      active,
                                                      0,
                                                      pos)
        {
            Radius = 20;

            Invocation = new FunctionInvocation(factory.DoNothingFunction);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Model.Action"/> class.
        /// </summary>
        /// <param name="factory">The entity factory.</param>
        /// <param name="parent">Parent.</param>
        /// <param name="descriptor">Descriptor.</param>
        public Action(EntityFactory factory, PetriNet parent, XElement descriptor) : base(factory,
                                                                                          parent,
                                                                                          descriptor)
        {
            var fun = descriptor.Attribute("Function");
            if(fun != null) {
                Invocation = factory.TryGetFunction(fun.Value);
            } else {
                Invocation = new FunctionInvocation(factory.DoNothingFunction);
            }

            var timeout = descriptor.Attribute("Timeout");
            if(timeout != null) {
                TimeoutMS = uint.Parse(descriptor.Attribute("Timeout").Value);
            }
        }

        /// <summary>
        /// Creates an empty node correctly named to represent an entity, and fill it with the serialization data.
        /// </summary>
        /// <returns>The new, populated node.</returns>
        public override XElement GetXML()
        {
            var elem = new XElement("Action");
            Serialize(elem);
            return elem;
        }

        /// <summary>
        /// Serializes the instance into the provided XML element.
        /// </summary>
        /// <param name="element">The XML element to fill with the serialization data.</param>
        protected override void Serialize(XElement element)
        {
            base.Serialize(element);
            element.SetAttributeValue("Function", Invocation.MakeUserReadable());
            if(TimeoutMS != 0) {
                element.SetAttributeValue("Timeout", TimeoutMS);
            }
        }

        /// <summary>
        /// Gets or sets the function invocation that will be run when the action becomes active.
        /// </summary>
        /// <value>The function.</value>
        public FunctionInvocation Invocation {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the timeout of the action as a milliseconds amount. <c>0</c> if no timeout.
        /// </summary>
        /// <value>The timeout in milliseconds.</value>
        public uint TimeoutMS {
            // TODO: change unit?
            get;
            set;
        }

        /// <summary>
        /// Checks if the entity uses the specified function.
        /// </summary>
        /// <returns><c>true</c> if function is used by the entity.</returns>
        /// <param name="f">The function to check.</param>
        public override bool UsesFunction(Function f)
        {
            return Invocation.UsesFunction(f);
        }

        /// <summary>
        /// Adds the VariableExpressions contained in the entity to the collection passed as an argument.
        /// </summary>
        /// <param name="res">Result.</param>
        public override void GetVariables(ICollection<VariableExpression> res)
        {
            var l = Invocation.GetVariables();
            foreach(var ll in l) {
                res.Add(ll);
            }
        }
    }
}
