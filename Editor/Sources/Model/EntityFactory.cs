//
//  EntityFactory.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2014-11-13.
//

using System;

using Petri.Code;

namespace Petri.Model
{
    /// <summary>
    /// A class that encapsulates an ever increasing sequence of identifiers.
    /// </summary>
    public class EntityFactory
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Model.EntityFactory"/> with the parameter as the first ID that will be consumed.
        /// </summary>
        /// <param name="languageProvider">The language provider.</param>
        public EntityFactory(ISourceCodeProvider languageProvider)
        {
            ID = 0;
            _sourceCodeProvider = languageProvider;
        }

        /// <summary>
        /// Consume a new ID from this instance and returns it.
        /// </summary>
        public UInt64 ConsumeID()
        {
            return ID++;
        }

        /// <summary>
        /// Gets the current ID of this instance.
        /// </summary>
        /// <value>The ID.</value>
        public UInt64 ID {
            get;
            private set;
        }

        /// <summary>
        /// Resets the internal ID to the provided one.
        /// </summary>
        /// <param name="reference">The new ID reference.</param>
        public void ResetID(UInt64 reference)
        {
            ID = reference;
        }

        /// <summary>
        /// Gets the type of the enum an Action's invocation returns.
        /// </summary>
        /// <value>The enum.</value>
        public Code.Enum Enum => _sourceCodeProvider.Enum;

        /// <summary>
        /// Gets the programming language of the entities' source code.
        /// </summary>
        /// <value>The language.</value>
        public Language Language => _sourceCodeProvider.Language;

        /// <summary>
        /// Creates an expression from a string and a document. The document is only used to retrieve different parameters to pass to the <c>CreateFromString</c> methods.
        /// </summary>
        /// <returns>The new expression.</returns>
        /// <param name="s">The representation of the expression to create.</param>
        public Expression CreateExpressionFromString(string s)
        {
            return Expression.CreateFromString(s, Language, _sourceCodeProvider.AllFunctions, null/*PreprocessorMacros*/);
        }

        /// <summary>
        /// Returns a true-ish expression according to the current language.
        /// </summary>
        /// <returns>The true-ish expression.</returns>
        public Expression TrueExpression {
            get {
                if(Language == Language.Python) {
                    return CreateExpressionFromString("True");
                } else {
                    return CreateExpressionFromString("true");
                }
            }
        }

        /// <summary>
        /// Create a function invocation from the given string.
        /// </summary>
        /// <param name="s">S.</param>
        public FunctionInvocation TryGetFunction(string s)
        {
            try {
                var exp = CreateExpressionFromString(s);
                if(exp is FunctionInvocation) {
                    return (FunctionInvocation)exp;
                } else {
                    return new WrapperFunctionInvocation(Enum.Type, exp);
                }
            } catch(Exception e) {
                return new WrapperFunctionInvocation(
                    Enum.Type,
                    LiteralExpression.CreateFromString(s, Language),
                    e.Message
                );
            }
        }

        /// <summary>
        /// Tries the set condition. On error, the transition is added to its document's conflicting entities.
        /// </summary>
        /// <param name="s">S.</param>
        public Expression TryGetCondition(string s)
        {
            try {
                return CreateExpressionFromString(s);
            } catch(Exception e) {
                return new WrapperFunctionInvocation(
                    new Code.Type(Language, "bool"),
                    LiteralExpression.CreateFromString(s, Language),
                    e.Message
                );
            }
        }

        /// <summary>
        /// Gets the function that pretty prints the petri net action at runtime.
        /// </summary>
        /// <returns>The print function.</returns>
        public Function PrintActionFunction {
            get {
                var lang = _sourceCodeProvider.Language;

                if(lang == Language.Cpp) {
                    var f = new Function(
                        _sourceCodeProvider.Enum.Type,
                        Scope.MakeFromNamespace(lang, "Petri::Utility"),
                        "printAction"
                    );
                    f.AddParam(new Param(new Code.Type(lang, "std::string const &"), "name"));
                    f.AddParam(new Param(new Code.Type(lang, "std::uint64_t"), "id"));
                    return f;
                } else if(lang == Language.C) {
                    var f = new Function(
                        _sourceCodeProvider.Enum.Type,
                        null,
                        "PetriUtility_printAction"
                    );
                    f.AddParam(new Param(new Code.Type(lang, "char const *"), "name"));
                    f.AddParam(new Param(new Code.Type(lang, "uint64_t"), "id"));
                    return f;
                } else if(lang == Language.EmbeddedC) {
                    throw new NotSupportedException();
                } else if(lang == Language.CSharp) {
                    var f = new Function(
                        _sourceCodeProvider.Enum.Type,
                        Scope.MakeFromNamespace(lang, "Petri.Runtime.Utility"),
                        "PrintAction"
                    );
                    f.AddParam(new Param(new Code.Type(lang, "string"), "name"));
                    f.AddParam(new Param(new Code.Type(lang, "UInt64"), "id"));
                    return f;
                } else if(lang == Language.Python) {
                    var f = new Function(
                        _sourceCodeProvider.Enum.Type,
                        null,
                        "PetriUtility_printAction"
                    );
                    f.AddParam(new Param(new Code.Type(lang, "string"), "name"));
                    f.AddParam(new Param(new Code.Type(lang, "long"), "id"));
                    return f;
                }

                throw new Exception("RuntimeFunctions.PrintFunction: Should not get there !");
            }
        }

        /// <summary>
        /// Gets the function that does nothing at runtime.
        /// </summary>
        /// <returns>The do nothing function.</returns>
        public Function DoNothingFunction {
            get {
                var lang = _sourceCodeProvider.Language;
                if(lang == Language.Cpp) {
                    return new Function(
                        _sourceCodeProvider.Enum.Type,
                        Scope.MakeFromNamespace(lang, "Petri::Utility"),
                        "doNothing"
                    );
                } else if(lang == Language.C) {
                    return new Function(
                        _sourceCodeProvider.Enum.Type,
                        null,
                        "PetriUtility_doNothing"
                    );
                } else if(lang == Language.EmbeddedC) {
                    return new Function(
                        _sourceCodeProvider.Enum.Type,
                        null,
                        "Petri_doNothing"
                    );
                } else if(lang == Language.CSharp) {
                    return new Function(
                        _sourceCodeProvider.Enum.Type,
                        Scope.MakeFromNamespace(lang, "Petri.Runtime.Utility"),
                        "DoNothing"
                    );
                } else if(lang == Language.Python) {
                    return new Function(
                        _sourceCodeProvider.Enum.Type,
                        null,
                        "PetriUtility_doNothing"
                    );
                }

                throw new Exception("RuntimeFunctions.DoNothingFunction: Should not get there !");
            }
        }

        /// <summary>
        /// Gets the function that make the calling thread to pause for the requested amount of time.
        /// </summary>
        /// <returns>The pause function.</returns>
        public Function PauseFunction {
            get {
                var lang = _sourceCodeProvider.Language;

                if(lang == Language.Cpp) {
                    var f = new Function(
                        _sourceCodeProvider.Enum.Type,
                        Scope.MakeFromNamespace(lang, "Petri::Utility"),
                        "pause"
                    );
                    f.AddParam(new Param(new Code.Type(lang, "std::chrono::milliseconds"), "delay"));
                    return f;
                } else if(lang == Language.C) {
                    var f = new Function(
                        _sourceCodeProvider.Enum.Type,
                        null,
                        "PetriUtility_pause"
                    );
                    f.AddParam(new Param(new Code.Type(lang, "double"), "delayInSeconds"));
                    return f;
                } else if(lang == Language.EmbeddedC) {
                    throw new NotSupportedException();
                } else if(lang == Language.CSharp) {
                    var f = new Function(
                        _sourceCodeProvider.Enum.Type,
                        Scope.MakeFromNamespace(lang, "Petri.Runtime.Utility"),
                        "Pause"
                    );
                    f.AddParam(new Param(new Code.Type(lang, "double"), "delayInSeconds"));
                    return f;
                } else if(lang == Language.Python) {
                    var f = new Function(
                        _sourceCodeProvider.Enum.Type,
                        null,
                        "PetriUtility_pause"
                    );
                    f.AddParam(new Param(new Code.Type(lang, "float"), "delay_in_seconds"));
                    return f;
                }


                throw new Exception("RuntimeFunctions.PauseFunction: Should not get there !");
            }
        }

        /// <summary>
        /// Gets the function that prints some variables.
        /// </summary>
        /// <returns>The print variable function.</returns>
        public Function PrintVariablesFunction {
            get {
                var lang = _sourceCodeProvider.Language;
                if(lang == Language.Cpp) {
                    var f = new Function(
                        _sourceCodeProvider.Enum.Type,
                        Scope.MakeFromNamespace(lang, "Petri::Utility"),
                        "printAllVars",
                        isVariadic: true
                    );
                    f.AddParam(new Param(new Code.Type(lang, "Petri::VarSlot &"), "variables"));
                    return f;
                } else if(lang == Language.C) {
                    var f = new Function(
                        _sourceCodeProvider.Enum.Type,
                        null,
                        "PetriUtility_printAllVars",
                        isVariadic: true
                    );
                    f.AddParam(new Param(new Code.Type(lang, "struct PetriVarSlot *"), "variables"));
                    return f;
                } else if(lang == Language.EmbeddedC) {
                    throw new NotSupportedException();
                } else if(lang == Language.CSharp) {
                    var f = new Function(
                        _sourceCodeProvider.Enum.Type,
                        Scope.MakeFromNamespace(lang, "Petri.Runtime.Utility"),
                        "PrintAllVars",
                        isVariadic: true
                    );
                    f.AddParam(new Param(new Code.Type(lang, "Petri.Runtime.VarSlot"), "variables"));
                    return f;
                } else if(lang == Language.Python) {
                    var f = new Function(
                        _sourceCodeProvider.Enum.Type,
                        null,
                        "PetriUtility_printAllVars",
                        isVariadic: true
                    );
                    f.AddParam(new Param(new Code.Type(lang, "PetriRuntime_VarSlot"), "variables"));
                    return f;
                }

                throw new Exception("RuntimeFunctions.PrintVariableFunction: Should not get there !");
            }
        }

        /// <summary>
        /// Gets the function that prints some text.
        /// </summary>
        /// <returns>The print text function.</returns>
        public Function PrintTextFunction {
            get {
                var lang = _sourceCodeProvider.Language;
                if(lang == Language.Cpp) {
                    var f = new Function(
                        _sourceCodeProvider.Enum.Type,
                        Scope.MakeFromNamespace(lang, "Petri::Utility"),
                        "printText"
                    );
                    f.AddParam(new Param(new Code.Type(lang, "std::string const &"), "value"));
                    return f;
                } else if(lang == Language.C) {
                    var f = new Function(
                        _sourceCodeProvider.Enum.Type,
                        null,
                        "PetriUtility_printText"
                    );
                    f.AddParam(new Param(new Code.Type(lang, "char const *"), "value"));
                    return f;
                } else if(lang == Language.EmbeddedC) {
                    throw new NotSupportedException();
                } else if(lang == Language.CSharp) {
                    var f = new Function(
                        _sourceCodeProvider.Enum.Type,
                        Scope.MakeFromNamespace(lang, "Petri.Runtime.Utility"),
                        "PrintText"
                    );
                    f.AddParam(new Param(new Code.Type(lang, "string"), "value"));
                    return f;
                } else if(lang == Language.Python) {
                    var f = new Function(
                        _sourceCodeProvider.Enum.Type,
                        null,
                        "PetriUtility_printText"
                    );
                    f.AddParam(new Param(new Code.Type(lang, "string"), "text"));
                    return f;
                }

                throw new Exception("RuntimeFunctions.PrintTextFunction: Should not get there !");
            }
        }

        /// <summary>
        /// Gets the function that does nothing at runtime.
        /// </summary>
        /// <returns>The do nothing function.</returns>
        public Function RandomFunction {
            get {
                var lang = _sourceCodeProvider.Language;
                if(lang == Language.Cpp) {
                    var f = new Function(
                        new Code.Type(lang, "int64_t"),
                        Scope.MakeFromNamespace(lang, "Petri::Utility"),
                        "random"
                    );
                    f.AddParam(new Param(new Code.Type(lang, "int64_t"), "lowerBound"));
                    f.AddParam(new Param(new Code.Type(lang, "int64_t"), "upperBound"));
                    return f;
                } else if(lang == Language.C) {
                    var f = new Function(new Code.Type(lang, "int64_t"), null, "PetriUtility_random");
                    f.AddParam(new Param(new Code.Type(lang, "int64_t"), "lowerBound"));
                    f.AddParam(new Param(new Code.Type(lang, "int64_t"), "upperBound"));
                    return f;
                } else if(lang == Language.CSharp) {
                    var f = new Function(
                        new Code.Type(lang, "Int64"),
                        Scope.MakeFromNamespace(lang, "Petri.Runtime.Utility"),
                        "random"
                    );
                    f.AddParam(new Param(new Code.Type(lang, "Int64"), "lowerBound"));
                    f.AddParam(new Param(new Code.Type(lang, "Int64"), "upperBound"));
                    return f;
                } else if(lang == Language.Python) {
                    var f = new Function(new Code.Type(lang, "long"), null, "PetriUtility_random");
                    f.AddParam(new Param(new Code.Type(lang, "long"), "lower_bound"));
                    f.AddParam(new Param(new Code.Type(lang, "long"), "upper_bound"));
                    return f;
                }

                throw new Exception("RuntimeFunctions.RandomFunction: Should not get there !");
            }
        }

        /// <summary>
        /// Gets the invocation with default parameters for the given function.
        /// </summary>
        /// <returns>The invocation for function.</returns>
        /// <param name="function">Function.</param>
        public FunctionInvocation DefaultInvocationForFunction(Code.Function function)
        {
            if(function.Signature == PrintActionFunction?.Signature) {
                return new FunctionInvocation(
                    function,
                    LiteralExpression.CreateFromString("$Name", _sourceCodeProvider.Language),
                    LiteralExpression.CreateFromString("$ID", _sourceCodeProvider.Language)
                );
            } else if(function.Signature == PauseFunction?.Signature) {
                return new FunctionInvocation(
                    function,
                    LiteralExpression.DefaultDelayLiteralForLanguage(_sourceCodeProvider.Language)
                );
            } else if(function.Signature == PrintVariablesFunction?.Signature) {
                return new FunctionInvocation(
                    function,
                    LiteralExpression.CreateFromString("_PETRI_PRIVATE_GET_VARIABLES_", _sourceCodeProvider.Language),
                    new EmptyExpression(_sourceCodeProvider.Language, true)
                );
            }

            var args = new Expression[function.Parameters.Count];
            for(int i = 0; i < args.Length; ++i) {
                args[i] = new EmptyExpression(_sourceCodeProvider.Language, true);
            }
            if(function is Method) {
                return new MethodInvocation(
                    function as Method,
                    new EmptyExpression(_sourceCodeProvider.Language, true),
                    false,
                    args
                );
            } else {
                return new FunctionInvocation(function, args);
            }
        }

        ISourceCodeProvider _sourceCodeProvider;
    }
}
