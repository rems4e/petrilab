//
//  Entity.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2014-11-13.
//

using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Linq;

namespace Petri.Model
{
    /// <summary>
    /// The base class from every entity enclosed in a petri net.
    /// </summary>
    public abstract class Entity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Model.Entity"/> class.
        /// </summary>
        /// <param name="factory">The entity factory.</param>
        /// <param name="parent">Parent.</param>
        protected Entity(EntityFactory factory, PetriNet parent)
        {
            Parent = parent;
            ID = factory.ConsumeID();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Model.Entity"/> class.
        /// </summary>
        /// <param name="factory">The entity factory.</param>
        /// <param name="parent">Parent.</param>
        /// <param name="descriptor">Descriptor.</param>
        protected Entity(EntityFactory factory, PetriNet parent, XElement descriptor)
        {
            Parent = parent;
            ID = XmlConvert.ToUInt64(descriptor.Attribute("ID").Value);

            Name = descriptor.Attribute("Name").Value;
            Position = new Point(XmlConvert.ToDouble(descriptor.Attribute("X").Value),
                                 XmlConvert.ToDouble(descriptor.Attribute("Y").Value));

            if(ID >= factory.ID) {
                factory.ResetID(ID + 1);
            }
        }

        /// <summary>
        /// Creates an empty node correctly named to represent an entity, and fill it with the serialization data.
        /// </summary>
        /// <returns>The new, populated node.</returns>
        public abstract XElement GetXML();

        /// <summary>
        /// Serializes the instance into the provided XML element.
        /// </summary>
        /// <param name="element">The XML element to fill with the serialization data.</param>
        protected virtual void Serialize(XElement element)
        {
            element.SetAttributeValue("ID", ID);
            element.SetAttributeValue("Name", Name);
            element.SetAttributeValue("X", Position.X);
            element.SetAttributeValue("Y", Position.Y);
        }

        /// <summary>
        /// Gets or sets the identifier of the instance.
        /// </summary>
        /// <value>The identifier of the instance.</value>
        public UInt64 ID {
            get;
            set;
        }

        /// <summary>
        /// Returns the ID corresponding to the given entity.
        /// The IDs are offsetted so that they form a dense increasing sequence accross all the imported petri nets, child or parent of this document.
        /// </summary>
        /// <returns>The entity's global ID.</returns>
        public UInt64 GlobalID {
            get {
                UInt64 offset = 0;
                var pn = Document.Parent;
                while(pn != null) {
                    offset += pn.ID; // This is intentionnally the base ID of the entity
                    pn = pn.Document.Parent;
                }

                return ID + offset;
            }
        }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <exception cref="ArgumentException">When the value contains two consecutive '_' characters, or starts or ends with '_' (this has to do with C++ name mangling and UB).</exception>
        /// <value>The name.</value>
        public virtual string Name {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the parent.
        /// </summary>
        /// <value>The parent.</value>
        public PetriNet Parent {
            get;
            set;
        }

        /// <summary>
        /// Gets the full path of the entity, in the form Root/InnerPetriNet1/InnerPetriNet2/Name.
        /// </summary>
        /// <value>The full path.</value>
        public virtual string FullPath {
            get {
                return Parent.FullPath + "/" + Name;
            }
        }

        /// <summary>
        /// Gets or sets the parent's document.
        /// </summary>
        /// <value>The document.</value>
        public virtual Application.Document Document {
            get {
                return Parent.Document;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this <see cref="Petri.Model.Entity"/> sticks to the grid.
        /// </summary>
        /// <value><c>true</c> if stick to grid; otherwise, <c>false</c>.</value>
        public virtual bool StickToGrid {
            get {
                return true;
            }
        }

        /// <summary>
        /// Gets the size of the grid on which the entities will stick if requested.
        /// </summary>
        /// <value>The size of the grid.</value>
        static public int GridSize {
            get {
                return 10;
            }
        }

        /// <summary>
        /// Gets or sets the position of the entity in its parent's frame.
        /// </summary>
        /// <value>The position.</value>
        public virtual Point Position {
            get {
                return _position;
            }
            set {
                _position = new Point(value);
            }
        }

        /// <summary>
        /// Checks if the entity uses the specified function.
        /// </summary>
        /// <returns><c>true</c> if function is used by the entity.</returns>
        /// <param name="f">The function to check.</param>
        public abstract bool UsesFunction(Petri.Code.Function f);

        /// <summary>
        /// Adds the VariableExpressions contained in the entity to the collection passed as an argument.
        /// </summary>
        /// <param name="res">Result.</param>
        public abstract void GetVariables(ICollection<Code.VariableExpression> res);

        /// <summary>
        /// Gets the code identifier of the entity, i.e. the name of the variable containing the entity in the generated code.
        /// </summary>
        /// <value>The code identifier.</value>
        public abstract string CodeIdentifier {
            get;
        }

        /// <summary>
        /// Gets the deepest common parent of <paramref name="e1"/> and <paramref name="e2"/>.
        /// </summary>
        /// <returns>The root.</returns>
        /// <param name="e1">The first entity.</param>
        /// <param name="e2">The second entity.</param>
        public static PetriNet GetCommonRoot(Entity e1, Entity e2)
        {
            int sz1 = 0, sz2 = 0;
            var ee1 = e1;
            var ee2 = e2;

            while(ee1 != null) {
                ee1 = ee1.Parent;
                ++sz1;
            }
            while(ee2 != null) {
                ee2 = ee2.Parent;
                ++sz2;
            }

            var diff = sz1 - sz2;
            while(diff < 0) {
                e2 = e2.Parent;
                ++diff;
            }
            while(diff > 0) {
                e1 = e1.Parent;
                --diff;
            }

            while(e1 != e2) {
                e1 = e1.Parent;
                e2 = e2.Parent;
            }

            return (PetriNet)e1;
        }

        Point _position;
    }
}
