//
//  FlatPetriNet.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-01.
//

using System;
using System.Collections.Generic;

using Petri.Application;
using Petri.Code;

namespace Petri.Model
{
    /// <summary>
    /// A flat petri net, with all its actions in the same nesting level.
    /// </summary>
    public class FlatPetriNet
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Model.FlatPetriNet"/> class.
        /// </summary>
        /// <param name="petriNet">The petri net to flatten.</param>
        public FlatPetriNet(RootPetriNet petriNet)
        {
            var tup = Flatten(petriNet);

            var flattened = tup.Item1;
            NextID = tup.Item2;

            States = new List<State>(flattened.States);
            Transitions = flattened.Transitions;
            Variables = new SortedDictionary<VariableExpression, Int64>(flattened.Variables);
            Parameters = flattened.Parameters;
            ReturnValues = flattened.ReturnValues;
            ExitPoint = flattened.ExitPoint;
            ExternalPetriNets = new List<ExternalInnerPetriNetProxy>();

            flattened.Document.ClearReachability();
            foreach(var s in States) {
                if(s is ExternalInnerPetriNetProxy) {
                    ExternalPetriNets.Add((ExternalInnerPetriNetProxy)s);
                }
                _isReachable[s] = s.IsReachable;
            }
            flattened.Document.ClearReachability();

            _isReachable[flattened.ExitPoint] = true;
        }

        /// <summary>
        /// Gets the next identifier available..
        /// </summary>
        /// <value>The next identifier.</value>
        public UInt64 NextID {
            get;
            private set;
        }

        /// <summary>
        /// Gets the actions.
        /// </summary>
        /// <value>The actions.</value>
        public List<State> States {
            get;
            private set;
        }

        /// <summary>
        /// Gets the transitions.
        /// </summary>
        /// <value>The transitions.</value>
        public List<Transition> Transitions {
            get;
            private set;
        }

        /// <summary>
        /// Gets the variables.
        /// </summary>
        /// <value>The variables.</value>
        public SortedDictionary<VariableExpression, Int64> Variables {
            get;
            private set;
        }

        /// <summary>
        /// Gets the parameters.
        /// </summary>
        /// <value>The parameters.</value>
        public SortedSet<VariableExpression> Parameters {
            get;
            private set;
        }

        /// <summary>
        /// Gets the return values.
        /// </summary>
        /// <value>The return values.</value>
        public SortedDictionary<string, Expression> ReturnValues {
            get;
            private set;
        }

        /// <summary>
        /// Returns the reachabiblity of the action.
        /// </summary>
        /// <returns><c>true</c>, if reachable, <c>false</c> otherwise.</returns>
        /// <param name="s">The state.</param>
        public bool IsReachable(State s)
        {
            return _isReachable[s];
        }

        /// <summary>
        /// Gets the exit point.
        /// </summary>
        /// <value>The exit point.</value>
        public ExitPoint ExitPoint {
            get;
            private set;
        }

        /// <summary>
        /// Gets the external petri nets.
        /// </summary>
        /// <value>The external petri nets.</value>
        public List<ExternalInnerPetriNetProxy> ExternalPetriNets {
            get;
            private set;
        }

        /// <summary>
        /// Flattens the petri net, by returning an equivalent petri net without any inner petri net (all states and transitions in the root petri net).
        /// </summary>
        /// <returns>The new, flattened petri net, and the next available entity ID.</returns>
        /// <param name="petriNet">The petri net to flatten.</param>
        public static Tuple<RootPetriNet, UInt64> Flatten(RootPetriNet petriNet)
        {
            var xml = petriNet.GetXML();
            var map = new Dictionary<UInt64, ExternalInnerPetriNet>();

            bool needsFullFlatten = petriNet.Document.Settings.Language == Language.EmbeddedC;

            if(!needsFullFlatten) {
                // If we don't need a full flattening, we replace all external petri nets by a special
                // entity containing just enough information to generate the source code,
                // but not their inner entities.
                foreach(var pn in petriNet.Document.FirstLevelExternalPetriNets) {
                    map[pn.ID] = pn;
                }
                foreach(var e in xml.Descendants("PetriNetRef")) {
                    e.Name = "PetriNetPlaceholder";
                }
            }

            // Build a clone of the parameter
            var lastID = petriNet.Document.EntityFactory.ID;
            var enclosing = new RootPetriNet(petriNet.Document, null);
            foreach(var kvp in petriNet.Variables) {
                enclosing.Variables.Add(kvp.Key, kvp.Value);
            }
            foreach(var param in petriNet.Parameters) {
                enclosing.Parameters.Add(param);
            }
            foreach(var kvp in petriNet.ReturnValues) {
                enclosing.ReturnValues.Add(kvp.Key, kvp.Value);
            }
            var clone = new LocalInnerPetriNet(petriNet.Document.EntityFactory, enclosing, xml);
            clone.ID = 0;
            enclosing.AddState(clone);

            petriNet.Document.EntityFactory.ResetID(lastID);

            if(needsFullFlatten) {
                enclosing.Variables.Clear();
                // Reassigning the entities' IDs to be on the same scale
                var idm = new EntityFactory(null);
                var list = clone.BuildAllEntitiesList();
                foreach(var e in list) {
                    e.ID = idm.ConsumeID();
                }

                // Namespacing the variables to keep their local scope.
                var varList = new List<VariableExpression>();
                foreach(Entity e in list) {
                    if(e is Action) {
                        ((Action)e).GetVariables(varList);
                    }
                    if(e is Transition) {
                        ((Transition)e).GetVariables(varList);
                    }

                    foreach(var v in varList) {
                        var id = e.Document is ExternalDocument ? e.Document.Parent.ID : e.Document.PetriNet.ID;
                        var value = e.Document.PetriNet.Variables[v];
                        v.Expression = string.Format("{0}_{1}", v.Expression, id);
                        enclosing.Variables[v] = value;
                    }

                    varList.Clear();
                }
                lastID = idm.ID;
            }

            // Flatten the cloned petri net
            var factory = new EntityFactory(petriNet.Document.Settings);
            factory.ResetID(lastID);
            Flatten(petriNet.Document, enclosing, factory, map);

            enclosing.RemoveState(enclosing.ExitPoint);
            enclosing.ExitPoint = clone.ExitPoint;

            return Tuple.Create(enclosing, factory.ID);
        }

        /// <summary>
        /// Flattens the petri net by recursively taking all the inner entities and putting them in their parent.
        /// </summary>
        /// <param name="doc">The document.</param>
        /// <param name="petriNet">The petri net to flatten.</param>
        /// <param name="factory">The entity factory.</param>
        /// <param name="externalMap">ExternalInnerPetriNets mapped by their IDs.</param>
        static void Flatten(Document doc,
                            PetriNet petriNet,
                            EntityFactory factory,
                            Dictionary<UInt64, ExternalInnerPetriNet> externalMap)
        {
            // Entities to add or remove, after iteration to avoid collection inconsistency
            var toRemove = new HashSet<State>();
            var toAdd = new HashSet<State>();

            foreach(State a in petriNet.States) {
                if(a is InnerPetriNet) {
                    var inner = (InnerPetriNet)a;

                    // We plan to remove the inner petri net, after flattening
                    toRemove.Add(inner);
                    Flatten(doc, inner, factory, externalMap);

                    // Adding a substitute to the now gone inner petri net's entry point
                    var entryPoint = new Action(doc.EntityFactory,
                                                petriNet,
                                                inner.IsStartState);
                    entryPoint.ID = inner.ID;
                    entryPoint.Name = inner.EntryPointName;
                    entryPoint.RequiredTokens = inner.RequiredTokens;
                    toAdd.Add(entryPoint);

                    Action exitPoint = inner.ExitPoint;
                    foreach(State a1 in inner.States) {
                        if(a1.IsStartState) {
                            var tt = new Transition(doc.EntityFactory, petriNet, entryPoint, a1);
                            entryPoint.AddTransitionAfter(tt);
                            a1.AddTransitionBefore(tt);

                            tt.ID = factory.ConsumeID();
                            tt.Name = entryPoint.Name + "_to_" + a1.Name;
                            petriNet.AddTransition(tt);
                            a1.IsStartState = false;
                        }

                        if(a1 is Action || a1 is ExternalInnerPetriNetProxy) {
                            a1.Parent = petriNet;
                            a1.IsStartState = false;
                            a1.Name = inner.Name + "_" + a1.Name;
                            toAdd.Add(a1);
                        }
                    }

                    foreach(Transition t in inner.Transitions) {
                        t.Parent = petriNet;
                        petriNet.AddTransition(t);
                        t.Name = inner.Name + "_" + t.Name;
                    }

                    foreach(Transition t in inner.TransitionsBefore) {
                        t.After = entryPoint;
                        entryPoint.AddTransitionBefore(t);
                    }
                    foreach(Transition t in inner.TransitionsAfter) {
                        t.Before = exitPoint;
                        exitPoint.AddTransitionAfter(t);
                    }
                } else if(a is ExternalInnerPetriNetXmlPlaceholder) {
                    var placeholder = (ExternalInnerPetriNetXmlPlaceholder)a;

                    // We remove the external petri net
                    toRemove.Add(placeholder);

                    // Replacing it by a 'proxy' entity
                    var proxy = new ExternalInnerPetriNetProxy(externalMap[placeholder.ID]);
                    toAdd.Add(proxy);

                    // And remapping the transitions
                    foreach(var t in placeholder.TransitionsBefore) {
                        t.After = proxy;
                        proxy.AddTransitionBefore(t);
                    }
                    foreach(var t in placeholder.TransitionsAfter) {
                        t.Before = proxy;
                        proxy.AddTransitionAfter(t);
                    }
                }
            }

            petriNet.States.RemoveAll(s => toRemove.Contains(s));
            petriNet.States.AddRange(toAdd);
        }

        Dictionary<State, bool> _isReachable = new Dictionary<State, bool>();
    }
}
