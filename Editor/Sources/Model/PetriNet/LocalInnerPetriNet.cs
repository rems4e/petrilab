//
//  LocalInnerPetriNet.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-11-06.
//

using System.Xml.Linq;

namespace Petri.Model
{
    /// <summary>
    /// an InnerPetriNet providing its own LocalPetriSource.
    /// </summary>
    public class LocalInnerPetriNet : InnerPetriNet
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Model.LocalInnerPetriNet"/> class.
        /// </summary>
        /// <param name="factory">The entity factory.</param>
        /// <param name="parent">Parent.</param>
        /// <param name="active">If set to <c>true</c> active.</param>
        /// <param name="pos">Position.</param>
        public LocalInnerPetriNet(EntityFactory factory,
                                  PetriNet parent,
                                  bool active,
                                  Point pos = new Point()) : base(factory,
                                                                  parent,
                                                                  "",
                                                                  typeof(LocalPetriNetSource),
                                                                  active,
                                                                  pos)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Model.LocalInnerPetriNet"/> class.
        /// </summary>
        /// <param name="factory">The entity factory.</param>
        /// <param name="parent">Parent.</param>
        /// <param name="descriptor">Descriptor.</param>
        public LocalInnerPetriNet(EntityFactory factory,
                                  PetriNet parent,
                                  XElement descriptor) : base(factory,
                                                              parent,
                                                              typeof(LocalPetriNetSource),
                                                              descriptor)
        {
        }
    }
}
