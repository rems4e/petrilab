//
//  RootPetriNet.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2014-11-13.
//

using System;
using System.Collections.Generic;
using System.Xml.Linq;

namespace Petri.Model
{
    /// <summary>
    /// The top-level petri net of a document.
    /// </summary>
    public class RootPetriNet : PetriNet
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Model.RootPetriNet"/> class.
        /// </summary>
        /// <param name="doc">Document.</param>
        /// <param name="parent">The petri net enclosing this instance. <c>null</c> if this is an actual root petri net, otherwise it is the root of an external document.</param>
        public RootPetriNet(Application.Document doc, PetriNet parent) : base(doc.EntityFactory, parent, true, new Point())
        {
            _document = doc;
            Variables = new SortedDictionary<Code.VariableExpression, Int64>();
            Parameters = new SortedSet<Code.VariableExpression>();
            ReturnValues = new SortedDictionary<string, Code.Expression>();
            Init(doc.EntityFactory, typeof(LocalPetriNetSource), "");
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Model.RootPetriNet"/> class.
        /// </summary>
        /// <param name="doc">Document.</param>
        /// <param name="parent">The petri net enclosing this instance. <c>null</c> if this is an actual root petri net, otherwise it is the root of an external document.</param>
        /// <param name="descriptor">The XML describing this instance.</param>
        public RootPetriNet(Application.Document doc,
                            PetriNet parent,
                            XElement descriptor) : base(doc.EntityFactory, parent, descriptor)
        {
            _document = doc;
            Variables = new SortedDictionary<Code.VariableExpression, Int64>();
            Parameters = new SortedSet<Code.VariableExpression>();
            var variables = descriptor.Elements("Variables").Elements("Variable");
            foreach(var v in variables) {
                bool isParam = false;
                var attr = v.Attribute("IsParameter");
                if(attr != null) {
                    bool.TryParse(attr.Value, out isParam);
                }
                var vv = Code.Expression.CreateFromString<Code.VariableExpression>(v.Attribute("Name").Value, doc.Settings.Language);
                Variables.Add(
                    vv,
                    Int64.Parse(v.Attribute("Value").Value)
                );
                if(isParam) {
                    Parameters.Add(vv);
                }

            }
            ReturnValues = new SortedDictionary<string, Code.Expression>();
            var returnValues = descriptor.Elements("ReturnValues").Elements("ReturnValue");
            foreach(var r in returnValues) {
                ReturnValues.Add(
                    r.Attribute("Name").Value,
                    Code.Expression.CreateFromString<Code.VariableExpression>(r.Attribute("Value").Value, doc.Settings.Language)
                );
            }

            Init(doc.EntityFactory, typeof(LocalPetriNetSource), descriptor);
        }

        /// <summary>
        /// Serializes the instance into the provided XML element.
        /// </summary>
        /// <param name="element">The XML element to fill with the serialization data.</param>
        protected override void Serialize(XElement element)
        {
            base.Serialize(element);

            if(ReturnValues.Count > 0) {
                var returnValues = new XElement("ReturnValues");
                foreach(var r in ReturnValues) {
                    var rr = new XElement("ReturnValue");
                    rr.SetAttributeValue("Name", r.Key);
                    rr.SetAttributeValue("Value", r.Value.MakeUserReadable());
                    returnValues.Add(rr);
                }
                element.AddFirst(returnValues);
            }

            if(Variables.Count > 0) {
                var variables = new XElement("Variables");
                foreach(var v in Variables) {
                    var vv = new XElement("Variable");
                    vv.SetAttributeValue("Name", v.Key.MakeUserReadable());
                    vv.SetAttributeValue("Value", v.Value.ToString());
                    if(Parameters.Contains(v.Key)) {
                        vv.SetAttributeValue("IsParameter", true);
                    }
                    variables.Add(vv);
                }
                element.AddFirst(variables);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="Petri.Model.RootPetriNet"/> is active.
        /// </summary>
        /// <value><c>true</c> if active; otherwise, <c>false</c>.</value>
        public override bool IsStartState {
            get {
                return true;
            }
            set {
                base.IsStartState = true;
            }
        }

        /// <summary>
        /// Gets or sets the required tokens that must be brought by transitions to activate the state.
        /// </summary>
        /// <value>The required tokens.</value>
        public override UInt32 RequiredTokens {
            get {
                return 0;
            }
            set {
            }
        }

        /// <summary>
        /// The name is forced to "Root" here.
        /// </summary>
        /// <value>The name.</value>
        public override string Name {
            get {
                return "Root";
            }
            set {
            }
        }

        /// <summary>
        /// Gets the full path of the entity.
        /// </summary>
        /// <value>The full path.</value>
        public override string FullPath {
            get {
                if(Document.Parent != null) {
                    return Document.Parent.FullPath + "/" + Name;
                } else {
                    return Name;
                }
            }
        }

        /// <summary>
        /// Gets or sets the document.
        /// </summary>
        /// <value>The document.</value>
        public override Application.Document Document {
            get {
                return _document;
            }
        }

        /// <summary>
        /// Gets the variables of the petri net.
        /// </summary>
        /// <value>The variables.</value>
        public SortedDictionary<Code.VariableExpression, Int64> Variables {
            get;
            private set;
        }

        /// <summary>
        /// The list of parameters the petri net accepts.
        /// </summary>
        /// <value>The parameters.</value>
        public SortedSet<Code.VariableExpression> Parameters {
            get;
            private set;
        }

        /// <summary>
        /// Gets the return values of the petri net.
        /// </summary>
        /// <value>The arguments.</value>
        public SortedDictionary<string, Code.Expression> ReturnValues {
            get;
            private set;
        }

        /// <summary>
        /// Adds the VariableExpressions contained in the entity to the collection passed as an argument.
        /// </summary>
        /// <param name="res">Result.</param>
        public override void GetVariables(ICollection<Code.VariableExpression> res)
        {
            foreach(var e in BuildLocalEntitiesList()) {
                if(!(e is PetriNet)) {
                    e.GetVariables(res);
                }
            }
        }

        Application.Document _document;
    }
}
