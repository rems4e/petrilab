//
//  InnerPetriNet.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2014-11-13.
//

using System;
using System.Xml.Linq;

namespace Petri.Model
{
    /// <summary>
    /// A petri net inside a petri net.
    /// </summary>
    public abstract class InnerPetriNet : PetriNet
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Model.InnerPetriNet"/> class.
        /// </summary>
        /// <param name="factory">The entity factory.</param>
        /// <param name="parent">Parent.</param>
        /// <param name="path">Path.</param>
        /// <param name="sourceType">The type of the data source containing the petri net elements.</param>
        /// <param name="active">If set to <c>true</c> then the petri net will be active at t=0.</param>
        /// <param name="pos">Position.</param>
        protected InnerPetriNet(EntityFactory factory,
                                PetriNet parent,
                                string path,
                                Type sourceType,
                                bool active,
                                Point pos) : base(factory, parent, active, pos)
        {
            Init(factory, sourceType, path);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Model.InnerPetriNet"/> class.
        /// </summary>
        /// <param name="factory">The entity factory.</param>
        /// <param name="parent">Parent.</param>
        /// <param name="sourceType">The type of the data source containing the petri net elements.</param>
        /// <param name="descriptor">Descriptor.</param>
        protected InnerPetriNet(EntityFactory factory,
                                PetriNet parent,
                                Type sourceType,
                                XElement descriptor) : base(factory,
                                                            parent,
                                                            descriptor)
        {
            Init(factory, sourceType, descriptor);
        }

        /// <summary>
        /// Gets or sets the entry point name. It represents the virtual EntryPoint entity and is used for code generation.
        /// </summary>
        /// <value>The entry point name.</value>
        public string EntryPointName {
            get {
                return Name + "_Entry";
            }
        }
    }
}

