//
//  ExternalInnerPetriNet.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-11-06.
//

using System.Collections.Generic;
using System.Xml.Linq;

namespace Petri.Model
{
    /// <summary>
    /// An InnerPetriNet obtaining its content from the RootPetriNet of another document.
    /// </summary>
    public class ExternalInnerPetriNet : InnerPetriNet
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Model.ExternalInnerPetriNet"/> class.
        /// </summary>
        /// <param name="factory">The entity factory.</param>
        /// <param name="parent">Parent.</param>
        /// <param name="path">Path.</param>
        /// <param name="pos">Position.</param>
        public ExternalInnerPetriNet(EntityFactory factory,
                                     PetriNet parent,
                                     string path,
                                     Point pos = new Point()) : base(factory,
                                                                     parent,
                                                                     path,
                                                                     typeof(ExternalPetriNetSource),
                                                                     false,
                                                                     pos)
        {
            Arguments = new Dictionary<Code.VariableExpression, Code.Expression>();
            ReturnValuesDestination = new Dictionary<string, Code.VariableExpression>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Model.ExternalInnerPetriNet"/> class.
        /// </summary>
        /// <param name="factory">The entity factory.</param>
        /// <param name="parent">Parent.</param>
        /// <param name="descriptor">Descriptor.</param>
        public ExternalInnerPetriNet(EntityFactory factory,
                                     PetriNet parent,
                                     XElement descriptor) : base(factory,
                                                                 parent,
                                                                 typeof(ExternalPetriNetSource),
                                                                 descriptor)
        {
            Arguments = new Dictionary<Code.VariableExpression, Code.Expression>();
            var parameters = descriptor.Elements("Arguments").Elements("Argument");
            foreach(var p in parameters) {
                var name = Code.Expression.CreateFromString<Code.VariableExpression>(p.Attribute("Name").Value, factory.Language);
                var value = Code.Expression.CreateFromString(p.Attribute("Value").Value, factory.Language);
                Arguments[name] = value;
            }

            ReturnValuesDestination = new Dictionary<string, Code.VariableExpression>();
            var returnValuesDestination = descriptor.Elements("ReturnValuesDestination").Elements("ReturnValueDestination");
            foreach(var r in returnValuesDestination) {
                ReturnValuesDestination.Add(
                    r.Attribute("Name").Value,
                    Code.Expression.CreateFromString<Code.VariableExpression>(r.Attribute("Value").Value, factory.Language)
                );
            }
        }

        /// <summary>
        /// Serializes the instance into the provided XML element.
        /// </summary>
        /// <param name="element">The XML element to fill with the serialization data.</param>
        protected override void Serialize(XElement element)
        {
            base.Serialize(element);

            if(ReturnValuesDestination.Count > 0) {
                var returnValuesDestination = new XElement("ReturnValuesDestination");
                foreach(var ret in ReturnValuesDestination) {
                    var r = new XElement("ReturnValueDestination");
                    r.SetAttributeValue("Name", ret.Key);
                    r.SetAttributeValue("Value", ret.Value.MakeUserReadable());
                    returnValuesDestination.Add(r);
                }
                element.AddFirst(returnValuesDestination);
            }

            if(Arguments.Count > 0) {
                var arguments = new XElement("Arguments");
                foreach(var arg in Arguments) {
                    var a = new XElement("Argument");
                    a.SetAttributeValue("Name", arg.Key.MakeUserReadable());
                    a.SetAttributeValue("Value", arg.Value.MakeUserReadable());
                    arguments.Add(a);
                }
                element.AddFirst(arguments);
            }
        }

        /// <summary>
        /// Gets the document enclosed in this instance.
        /// </summary>
        /// <value>The external document.</value>
        public Application.ExternalDocument ExternalDocument {
            get {
                return ExternalPetriSource.ExternalDocument;
            }
        }

        /// <summary>
        /// Reloads the enclosed petri net if required.
        /// </summary>
        /// <returns>The update.</returns>
        public bool Update()
        {
            return ExternalPetriSource.Update();
        }

        /// <summary>
        /// Gets data source of the petri net's elements.
        /// </summary>
        /// <value>The petri source.</value>
        public ExternalPetriNetSource ExternalPetriSource {
            get {
                return (ExternalPetriNetSource)PetriSource;
            }
        }

        /// <summary>
        /// Gets the arguments this instance will pass as parameters to its embedded petri net.
        /// </summary>
        /// <value>The arguments.</value>
        public Dictionary<Code.VariableExpression, Code.Expression> Arguments {
            get;
            private set;
        }

        /// <summary>
        /// Gets the return values' destination this instance will copy its embedded petri net's return values into.
        /// </summary>
        /// <value>The arguments.</value>
        public Dictionary<string, Code.VariableExpression> ReturnValuesDestination {
            get;
            private set;
        }
    }
}
