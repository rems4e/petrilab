//
//  ExternalInnerPetriNetXmlPlaceholder.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-01.
//

using System.Collections.Generic;
using System.Xml.Linq;

using Petri.Code;

namespace Petri.Model
{
    /// <summary>
    /// A class containing enough information to generate the source code for an <see cref="ExternalInnerPetriNet"/>.
    /// </summary>
    public class ExternalInnerPetriNetXmlPlaceholder : State
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Model.ExternalInnerPetriNetXmlPlaceholder"/> class.
        /// </summary>
        /// <param name="factory">The entity factory.</param>
        /// <param name="parent">The parent entity.</param>
        /// <param name="descriptor">The XML descriptor.</param>
        public ExternalInnerPetriNetXmlPlaceholder(EntityFactory factory,
                                                   PetriNet parent,
                                                   XElement descriptor) : base(factory, parent, descriptor)
        {
        }

        /// <summary>
        /// Gets the XML descriptor.
        /// </summary>
        /// <returns>The XML descriptor.</returns>
        public override XElement GetXML()
        {
            return null;
        }

        /// <summary>
        /// Checks if the entity uses the given function.
        /// </summary>
        /// <returns><c>true</c>, if function was used, <c>false</c> otherwise.</returns>
        /// <param name="f">The function.</param>
        public override bool UsesFunction(Function f)
        {
            return false;
        }

        /// <summary>
        /// Adds the VariableExpressions contained in the entity to the collection passed as an argument.
        /// </summary>
        /// <param name="res">Result.</param>
        public override void GetVariables(ICollection<VariableExpression> res)
        {
        }
    }
}
