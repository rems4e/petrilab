//
//  ExternalInnerPetriNetProxy.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-01.
//

using System;
using System.Collections.Generic;

namespace Petri.Model
{
    /// <summary>
    /// A class containing enough information to generate the source code for an <see cref="ExternalInnerPetriNet"/>.
    /// </summary>
    public class ExternalInnerPetriNetProxy : State
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Model.ExternalInnerPetriNetProxy"/> class.
        /// </summary>
        /// <param name="petriNet">The petri net to build this instance from.</param>
        public ExternalInnerPetriNetProxy(ExternalInnerPetriNet petriNet) : base(new EntityFactory(null),
                                                                                 petriNet.Parent,
                                                                                 petriNet.IsStartState,
                                                                                 petriNet.RequiredTokens,
                                                                                 petriNet.Position)
        {
            Path = petriNet.ExternalDocument.Path;
            ClassName = petriNet.ExternalDocument.Settings.Name;
            ID = petriNet.ID; // This is intentionnaly the base entity ID
            Name = petriNet.Name;
            Arguments = new Dictionary<Code.VariableExpression, Code.Expression>(petriNet.Arguments);
            Parameters = new SortedSet<Code.VariableExpression>(petriNet.ExternalDocument.PetriNet.Parameters);
            ReturnValues = new SortedDictionary<string, Code.Expression>(petriNet.ExternalDocument.PetriNet.ReturnValues);
            ReturnValuesDestination = new Dictionary<string, Code.VariableExpression>(petriNet.ReturnValuesDestination);
        }

        /// <summary>
        /// Gets the path of the external petri net's document.
        /// </summary>
        /// <value>The path.</value>
        public string Path {
            get;
            private set;
        }

        /// <summary>
        /// Gets the name of the class in the external petri net's generated code.
        /// </summary>
        /// <value>The name of the class.</value>
        public string ClassName {
            get;
            private set;
        }

        /// <summary>
        /// Gets the arguments this instance will pass as parameters to its embedded petri net.
        /// </summary>
        /// <value>The arguments.</value>
        public Dictionary<Code.VariableExpression, Code.Expression> Arguments {
            get;
            private set;
        }

        /// <summary>
        /// Gets the parameters this instance expects to receive.
        /// </summary>
        /// <value>The parameters.</value>
        public SortedSet<Code.VariableExpression> Parameters {
            get;
            private set;
        }

        /// <summary>
        /// Gets the return values.
        /// </summary>
        /// <value>The return values.</value>
        public SortedDictionary<string, Code.Expression> ReturnValues {
            get;
            private set;
        }

        /// <summary>
        /// Gets the return values destination.
        /// </summary>
        /// <value>The return values' destination.</value>
        public Dictionary<string, Code.VariableExpression> ReturnValuesDestination {
            get;
            private set;
        }

        /// <summary>
        /// Checks if the entity uses the specified function.
        /// </summary>
        /// <returns>false</returns>
        /// <param name="f">The function to check.</param>
        public override bool UsesFunction(Code.Function f)
        {
            return false;
        }

        /// <summary>
        /// Adds the VariableExpressions contained in the entity to the collection passed as an argument.
        /// </summary>
        /// <param name="res">Result.</param>
        public override void GetVariables(ICollection<Code.VariableExpression> res)
        {
        }

        /// <summary>
        /// Creates an empty node correctly named to represent an entity, and fill it with the serialization data.
        /// </summary>
        /// <returns>Nothing, instances of this class are not meant to be serialized.</returns>
        public override System.Xml.Linq.XElement GetXML()
        {
            return null;
        }

        /// <summary>
        /// Gets the entities offset of the external petri net.
        /// </summary>
        /// <value>The entities offset.</value>
        public UInt64 EntitiesOffset {
            get {
                return ID;
            }
        }
    }
}
