//
//  PetriNet.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2014-11-13.
//

using System;
using System.Collections.Generic;
using System.Xml.Linq;

namespace Petri.Model
{
    /// <summary>
    /// A petri net, containing entities among which are actions, transition and comments.
    /// </summary>
    public abstract class PetriNet : State
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Model.PetriNet"/> class.
        /// </summary>
        /// <param name="factory">The entity factory.</param>
        /// <param name="parent">Parent.</param>
        /// <param name="active">If set to <c>true</c> then the petri net is active at t=0.</param>
        /// <param name="pos">Position.</param>
        protected PetriNet(EntityFactory factory,
                           PetriNet parent,
                           bool active,
                           Point pos) : base(factory, parent, active, 0, pos)
        {
            Radius = 30;
            Size = new Vector(0, 0);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Model.PetriNet"/> class.
        /// </summary>
        /// <param name="factory">The entity factory.</param>
        /// <param name="parent">Parent.</param>
        /// <param name="descriptor">Descriptor.</param>
        protected PetriNet(EntityFactory factory,
                           PetriNet parent,
                           XElement descriptor) : base(factory, parent, descriptor)
        {
        }

        /// <summary>
        /// Initializes the specified the instance with the given entity factory, PetriNetSource suclass type and path.
        /// </summary>
        /// <param name="factory">Factory.</param>
        /// <param name="sourceType">Source type.</param>
        /// <param name="path">Path.</param>
        protected void Init(EntityFactory factory, Type sourceType, string path)
        {
            try {
                PetriSource = (IPetriNetSource)Activator.CreateInstance(sourceType, this, path);
            } catch(System.Reflection.TargetInvocationException e) {
                throw e.InnerException;
            }

            InitExitPoint(factory);
        }

        /// <summary>
        /// Initializes the specified the instance with the given entity factory, PetriNetSource suclass type and path.
        /// </summary>
        /// <param name="factory">Factory.</param>
        /// <param name="sourceType">Source type.</param>
        /// <param name="descriptor">The entity's XML descriptor.</param>
        protected void Init(EntityFactory factory, Type sourceType, XElement descriptor)
        {
            try {
                PetriSource = (IPetriNetSource)Activator.CreateInstance(sourceType, factory, this, descriptor);
            } catch(System.Reflection.TargetInvocationException e) {
                throw e.InnerException;
            }

            InitExitPoint(factory);
        }

        void InitExitPoint(EntityFactory factory)
        {
            foreach(var s in States) {
                if(s is ExitPoint) {
                    ExitPoint = (ExitPoint)s;
                    break;
                }
            }

            if(ExitPoint == null) {
                ExitPoint = new ExitPoint(factory, this, new Point(300, 100));
                AddState(ExitPoint);
            }
        }

        /// <summary>
        /// Serializes the instance into the provided XML element.
        /// </summary>
        /// <param name="element">The XML element to fill with the serialization data.</param>
        protected override void Serialize(XElement element)
        {
            base.Serialize(element);
            PetriSource.Serialize(element);
        }

        /// <summary>
        /// Creates an empty node correctly named to represent an entity, and fill it with the serialization data.
        /// </summary>
        /// <returns>The new, populated node.</returns>
        public override XElement GetXML()
        {
            var elem = PetriSource.GetXML();
            Serialize(elem);
            return elem;
        }

        /// <summary>
        /// Checks if the entity uses the specified function.
        /// </summary>
        /// <returns>true</returns>
        /// <param name="f">The function to check.</param>
        public override bool UsesFunction(Petri.Code.Function f)
        {
            foreach(var t in Transitions) {
                if(t.UsesFunction(f)) {
                    return true;
                }
            }
            foreach(var s in States) {
                if(s.UsesFunction(f)) {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Adds a Comment entity to the instance.
        /// </summary>
        /// <param name="comment">The comment.</param>
        public void AddComment(Comment comment)
        {
            Comments.Add(comment);
        }

        /// <summary>
        /// Adds a State entity to the instance.
        /// </summary>
        /// <param name="state">The state.</param>
        public void AddState(State state)
        {
            States.Add(state);
        }

        /// <summary>
        /// Adds a Transition entity to the instance.
        /// </summary>
        /// <param name="transition">The transition.</param>
        public void AddTransition(Transition transition)
        {
            Transitions.Add(transition);
        }

        /// <summary>
        /// The size of the petri net's content.
        /// The Radius property represents the external view of a petri net, and the size property represents the size of the drawing area for the entities inside the petri net.
        /// </summary>
        /// <value>The size.</value>
        public Vector Size {
            get;
            set;
        }

        /// <summary>
        /// Gets the comment that is located at the given position, or null if none exist.
        /// </summary>
        /// <returns>The comment at the given position.</returns>
        /// <param name="position">Position.</param>
        public Comment CommentAtPosition(Point position)
        {
            // TODO: come back with a better collision detection algorithm :p
            for(int i = Comments.Count - 1; i >= 0; --i) {
                var c = Comments[i];
                if(Math.Abs(c.Position.X - position.X) <= c.Size.X / 2
                       && Math.Abs(c.Position.Y - position.Y) < c.Size.Y / 2) {
                    return c;
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the state that is located at the given position, or null if none exist.
        /// </summary>
        /// <returns>The syaye at the given position.</returns>
        /// <param name="position">Position.</param>
        public State StateAtPosition(Point position)
        {
            // TODO: come back with a better collision detection algorithm :p
            for(int i = States.Count - 1; i >= 0; --i) {
                var s = States[i];
                if(s.PointInState(position)) {
                    return s;
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the transition that is located at the given position, or null if none exist.
        /// </summary>
        /// <returns>The transition at the given position.</returns>
        /// <param name="position">Position.</param>
        public Transition TransitionAtPosition(Point position)
        {
            // TODO: come back with a better collision detection algorithm :p
            for(int i = Transitions.Count - 1; i >= 0; --i) {
                var t = Transitions[i];
                if(Math.Abs(t.Position.X - position.X) <= t.Width / 2
                       && Math.Abs(t.Position.Y - position.Y) < t.Height / 2) {
                    return t;
                }
            }

            return null;
        }

        /// <summary>
        /// Removes the comment from this instance.
        /// </summary>
        /// <param name="comment">Comment.</param>
        public void RemoveComment(Comment comment)
        {
            Comments.Remove(comment);
        }

        /// <summary>
        /// Removes the state from this instance.
        /// </summary>
        /// <param name="state">State.</param>
        public void RemoveState(State state)
        {
            States.Remove(state);
        }

        /// <summary>
        /// Removes the transition from this instance.
        /// </summary>
        /// <param name="transition">Transition.</param>
        public void RemoveTransition(Transition transition)
        {
            transition.Before.RemoveTransitionAfter(transition);
            transition.After.RemoveTransitionBefore(transition);

            Transitions.Remove(transition);
        }

        /// <summary>
        /// Gets data source of the petri net's elements.
        /// </summary>
        /// <value>The petri source.</value>
        public IPetriNetSource PetriSource {
            get;
            private set;
        }

        /// <summary>
        /// Gets the comments directly contained in this instance, and not recursively from this instance's inner petri nets.
        /// </summary>
        /// <value>The comments.</value>
        public List<Comment> Comments {
            get {
                return PetriSource.Comments;
            }
        }

        /// <summary>
        /// Gets the states directly contained in this instance, and not recursively from this instance's inner petri nets.
        /// </summary>
        /// <value>The transitions.</value>
        public List<State> States {
            get {
                return PetriSource.States;
            }
        }

        /// <summary>
        /// Gets the transitions directly contained in this instance, and not recursively from this instance's inner petri nets.
        /// </summary>
        /// <value>The transitions.</value>
        public List<Transition> Transitions {
            get {
                return PetriSource.Transitions;
            }
        }

        /// <summary>
        /// Gets the exit point of the petri net, which is supposed to be a convenient state to synchronize every other state upon the petri net's end of execution.
        /// </summary>
        /// <value>The exit point.</value>
        public ExitPoint ExitPoint {
            get;
            set;
        }

        /// <summary>
        /// Return the entity which ID is equal to the parameter, or null if none is found.
        /// </summary>
        /// <returns>The entity of ID <paramref name="id"/>, or <c>null</c> if none exist.</returns>
        /// <param name="id">Identifier.</param>
        /// <param name="localID"><c>true</c> if the entities offset is not to be taken into account.</param>
        public Entity EntityFromID(UInt64 id, bool localID = false)
        {
            if((localID ? ID : GlobalID) == id) {
                return this;
            }
            foreach(var s in States) {
                if((localID ? s.ID : s.GlobalID) == id) {
                    return s;
                }
                if(s is PetriNet) {
                    Entity e = (s as PetriNet).EntityFromID(id, localID);
                    if(e != null) {
                        return e;
                    }
                }
            }
            foreach(var t in Transitions) {
                if((localID ? t.ID : t.GlobalID) == id) {
                    return t;
                }
            }
            foreach(var c in Comments) {
                if((localID ? c.ID : c.GlobalID) == id) {
                    return c;
                }
            }

            return null;
        }

        /// <summary>
        /// Recursively gets all of the Action/PetriNet/Transitions/Comments contained in the instance.
        /// Restrains the collect to this document only, not going into external documents.
        /// Does not include <c>this</c>.
        /// </summary>
        /// <returns>The entities list.</returns>
        public List<Entity> BuildLocalEntitiesList()
        {
            return BuildEntitiesListInternal(true, true);
        }

        /// <summary>
        /// Recursively gets all of the Action/PetriNet/Transitions/Comments contained in the instance,
        /// going into external petri nets as well.
        /// Does not include <c>this</c>.
        /// </summary>
        /// <returns>The entities list.</returns>
        public List<Entity> BuildAllEntitiesList()
        {
            return BuildEntitiesListInternal(false, true);
        }
        /// <summary>
        /// Gets the full path of a variable, including the path of the external inner petri net if any.
        /// </summary>
        /// <returns>The full name.</returns>
        /// <param name="v">The variable.</param>

        public string VariableFullName(Code.VariableExpression v)
        {
            var path = Parent is ExternalInnerPetriNet
                           ? Parent.FullPath + "/"
                           : "";
            return path + v.MakeUserReadable();
        }

        /// <summary>
        /// Recursively gets all of the Action/PetriNet/Transitions/Comments contained in the instance.
        /// </summary>
        /// <returns>The entities list.</returns>
        /// <param name="localOnly">Whether to construct the list only considering local (not imported) entities.</param>
        /// <param name="addRoot">Whether to add the petri net in the list.</param>
        List<Entity> BuildEntitiesListInternal(bool localOnly, bool addRoot)
        {
            var l = new List<Entity>();
            if(addRoot) {
                l.Add(this);
            }
            l.AddRange(Comments);
            l.AddRange(States);
            int index = Comments.Count + (addRoot ? 1 : 0);
            for(int i = index; i < index + States.Count; ++i) {
                var s = l[i] as PetriNet;
                if(s != null && !(s is ExternalInnerPetriNet && localOnly)) {
                    l.AddRange(s.BuildEntitiesListInternal(localOnly, false));
                }
            }
            l.AddRange(Transitions);

            return l;
        }

        /// <summary>
        /// Adds the VariableExpressions contained in the entity to the collection passed as an argument.
        /// </summary>
        /// <param name="res">Result.</param>
        public override void GetVariables(ICollection<Code.VariableExpression> res)
        {
            Document.PetriNet.GetVariables(res);
        }
    }
}

