//
//  ExternalPetriNetSource.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-11-05.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

using Petri.Application;

namespace Petri.Model
{
    /// <summary>
    /// A petri net data source proxying its storage through a document.
    /// </summary>
    public class ExternalPetriNetSource : IPetriNetSource
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Model.ExternalPetriNetSource"/> class.
        /// </summary>
        /// <param name="parent">The parent external petri net.</param>
        /// <param name="path">The path to the imported document.</param>
        public ExternalPetriNetSource(ExternalInnerPetriNet parent, string path)
        {
            _date = DateTime.MinValue;

            _parent = parent;
            SetPath(path);
            Update();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Model.ExternalPetriNetSource"/> class.
        /// This method is invoked through reflection by <see cref="T:Petri.Model.PetriNet.PetriNet()"/>.
        /// </summary>
        /// <param name="factory">The entity factory.</param>
        /// <param name="parent">The parent external petri net.</param>
        /// <param name="descriptor">Descriptor.</param>
        public ExternalPetriNetSource(EntityFactory factory, ExternalInnerPetriNet parent, XElement descriptor)
        {
            _date = DateTime.MinValue;

            _parent = parent;
            SetPath(descriptor.Attribute("Path").Value);
            Update();
        }

        /// <summary>
        /// Gets the comments directly contained in this instance, and not recursively from this instance's inner petri nets.
        /// </summary>
        /// <value>The comments.</value>
        public List<Comment> Comments {
            get {
                return ExternalDocument.PetriNet.Comments;
            }
        }

        /// <summary>
        /// Gets the states directly contained in this instance, and not recursively from this instance's inner petri nets.
        /// </summary>
        /// <value>The transitions.</value>
        public List<State> States {
            get {
                return ExternalDocument.PetriNet.States;
            }
        }

        /// <summary>
        /// Gets the transitions directly contained in this instance, and not recursively from this instance's inner petri nets.
        /// </summary>
        /// <value>The transitions.</value>
        public List<Transition> Transitions {
            get {
                return ExternalDocument.PetriNet.Transitions;
            }
        }

        /// <summary>
        /// Serialize the specified element into the provided XML element.
        /// </summary>
        /// <param name="elem">The XML element to fill with the serialization data.</param>
        public void Serialize(XElement elem)
        {
            elem.SetAttributeValue("Path", Path);
        }

        /// <summary>
        /// Creates an empty node correctly named to represent an entity.
        /// </summary>
        /// <returns>The new, empty node.</returns>
        public XElement GetXML()
        {
            var elem = new XElement("PetriNetRef");
            return elem;
        }

        /// <summary>
        /// Gets the referenced document.
        /// </summary>
        /// <value>The external document.</value>
        public ExternalDocument ExternalDocument {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the document <see cref="T:Petri.Model.ExternalPetriNetSource"/>
        /// is referenced by a relative path.
        /// </summary>
        /// <value><c>true</c> if referenced by a relative path; <c>false</c> if by an absolute path.</value>
        public bool RelativePathReference {
            get;
            set;
        }

        /// <summary>
        /// Gets the path of the referenced petri net's document, either in the form of a relative
        /// or absolute path, depending of the value of <see cref="RelativePathReference"/>.
        /// </summary>
        /// <value>The path.</value>
        public string Path {
            get {
                return RelativePathReference ? _parent.Document.GetRelativeToDoc(_path) : _path;
            }
        }

        /// <summary>
        /// Change the referenced document to another one.
        /// </summary>
        /// <param name="path">Path.</param>
        public void RepointReference(string path)
        {
            _date = DateTime.MinValue;
            SetPath(path);
            _parent.Document.RootDocument.UpdateExternalDocuments();
        }

        /// <summary>
        /// Update this instance if the referenced petri net has been modified since last loaded.
        /// </summary>
        /// <returns>If this external petri net instance or one of its external children have been updated.</returns>
        public bool Update()
        {
            var date = System.IO.File.GetLastWriteTime(_path);
            if(date > _date) {
                var root = _parent.Document.RootDocument;

                var allExternal = new List<ExternalInnerPetriNet> { _parent };
                if(_parent.ExternalPetriSource != null) {
                    allExternal.AddRange(from doc in _parent.ExternalDocument.AllExternalDocuments select doc.Parent);
                }
                foreach(var e in allExternal) {
                    root.RemoveIssue(e);
                }

                _date = date;
                var p = _parent.Document;
                while(true) {
                    if(p.Path == _path) {
                        // Cyclic reference detected.
                        ExternalDocument = new InvalidExternalDocument(_parent,
                                                                       _path,
                                                                       Configuration.GetLocalized("Cyclic reference"),
                                                                       EntityIssue.Kind.InvalidPetriNetReference);
                        break;
                    }
                    if(p.Parent == null) {
                        try {
                            ExternalDocument = new ValidExternalDocument(_parent, _path);
                            if(ExternalDocument.Settings.Language != _parent.Document.Settings.Language) {
                                var lang = ExternalDocument.Settings.Language;
                                ExternalDocument = new InvalidExternalDocument(_parent,
                                                                               _path,
                                                                               Configuration.GetLocalized("The document is in the wrong programming language ({0})",
                                                                                                          DocumentSettings.LanguageName(lang)),
                                                                               EntityIssue.Kind.InvalidPetriNetReference);
                            }
                        } catch(System.IO.IOException e) {
                            ExternalDocument = new InvalidExternalDocument(_parent,
                                                                           _path,
                                                                           Configuration.GetLocalized("Unreachable document") + "\n" + e.Message,
                                                                           EntityIssue.Kind.InvalidPetriNetReference);
                        } catch(Exception e) {
                            ExternalDocument = new InvalidExternalDocument(_parent,
                                                                           _path,
                                                                           Configuration.GetLocalized("Invalid document") + "\n" + e.GetBaseException().Message,
                                                                           EntityIssue.Kind.InvalidPetriNetReference);
                        }

                        break;
                    }
                    p = p.Parent.Document;
                }

                return true;
            }

            return ExternalDocument.UpdateExternalDocuments();
        }

        /// <summary>
        /// Sets the path of the referenced petri net, and determines if the path is relative or absolute.
        /// </summary>
        /// <param name="path">Path.</param>
        void SetPath(string path)
        {
            if(!System.IO.Path.IsPathRooted(path)) {
                _path = PathUtility.GetFullPath(System.IO.Path.Combine(System.IO.Path.Combine(
                    System.IO.Directory.GetParent(_parent.Document.Path).FullName,
                    path
                )));
                RelativePathReference = true;
            } else {
                _path = PathUtility.GetFullPath(path);
                RelativePathReference = false;
            }
        }

        DateTime _date;

        string _path;

        ExternalInnerPetriNet _parent;
    }
}
