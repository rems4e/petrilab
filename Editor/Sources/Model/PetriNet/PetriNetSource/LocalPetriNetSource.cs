//
//  LocalPetriNetSource.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-11-05.
//

using System;
using System.Collections.Generic;
using System.Xml.Linq;

namespace Petri.Model
{
    /// <summary>
    /// A petri net data source providing its own storage.
    /// </summary>
    public class LocalPetriNetSource : IPetriNetSource
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Model.LocalPetriNetSource"/> class
        /// </summary>
        public LocalPetriNetSource(PetriNet parent, string path)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Model.LocalPetriNetSource"/> class.
        /// </summary>
        /// <param name="factory">The entity factory.</param>
        /// <param name="petriNet">Petri net.</param>
        /// <param name="descriptor">Descriptor.</param>
        public LocalPetriNetSource(EntityFactory factory, PetriNet petriNet, XElement descriptor)
        {
            // Used to map XML's IDs of Transitions to actual States, after loading them.
            var statesTable = new Dictionary<UInt64, State>();

            foreach(var e in descriptor.Element("Comments").Elements("Comment")) {
                var c = new Comment(factory, petriNet, e);
                Comments.Add(c);
            }

            foreach(var e in descriptor.Element("States").Elements()) {
                if(e.Name == "Action" || e.Name == "PetriNet" || e.Name == "PetriNetRef" || e.Name == "PetriNetPlaceholder" || e.Name == "Exit") {
                    var s = State.FromXml(factory, e, petriNet, true);
                    statesTable.Add(s.ID, s);
                    States.Add(s);
                }
            }

            foreach(var e in descriptor.Element("Transitions").Elements("Transition")) {
                var t = new Transition(factory, petriNet, e, statesTable);
                Transitions.Add(t);
                t.Before.AddTransitionAfter(t);
                t.After.AddTransitionBefore(t);
            }
        }

        /// <summary>
        /// Gets the comments directly contained in this instance, and not recursively from this instance's inner petri nets.
        /// </summary>
        /// <value>The comments.</value>
        public List<Comment> Comments {
            get {
                return _comments;
            }
        }

        /// <summary>
        /// Gets the states directly contained in this instance, and not recursively from this instance's inner petri nets.
        /// </summary>
        /// <value>The transitions.</value>
        public List<State> States {
            get {
                return _states;
            }
        }

        /// <summary>
        /// Gets the transitions directly contained in this instance, and not recursively from this instance's inner petri nets.
        /// </summary>
        /// <value>The transitions.</value>
        public List<Transition> Transitions {
            get {
                return _transitions;
            }
        }

        /// <summary>
        /// Serialize the specified element into the provided XML element.
        /// </summary>
        /// <param name="elem">The XML element to fill with the serialization data.</param>
        public void Serialize(XElement elem)
        {
            var comments = new XElement("Comments");
            foreach(var c in Comments) {
                comments.Add(c.GetXML());
            }
            var states = new XElement("States");
            foreach(var s in States) {
                states.Add(s.GetXML());
            }
            var transitions = new XElement("Transitions");
            foreach(var t in Transitions) {
                transitions.Add(t.GetXML());
            }

            elem.Add(comments);
            elem.Add(states);
            elem.Add(transitions);
        }

        /// <summary>
        /// Creates an empty node correctly named to represent an entity.
        /// </summary>
        /// <returns>The new, empty node.</returns>
        public XElement GetXML()
        {
            var elem = new XElement("PetriNet");
            return elem;
        }


        List<Comment> _comments = new List<Comment>();
        List<State> _states = new List<State>();
        List<Transition> _transitions = new List<Transition>();
    }
}
