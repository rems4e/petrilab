//
//  IPetriNetSource.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-01.
//

using System.Collections.Generic;
using System.Xml.Linq;

namespace Petri.Model
{
    /// <summary>
    /// An interface specifying the requirements of a petri net source.
    /// </summary>
    public interface IPetriNetSource
    {
        /// <summary>
        /// Gets the comments directly contained in this instance, and not recursively from this instance's inner petri nets.
        /// </summary>
        /// <value>The comments.</value>
        List<Comment> Comments {
            get;
        }

        /// <summary>
        /// Gets the states directly contained in this instance, and not recursively from this instance's inner petri nets.
        /// </summary>
        /// <value>The transitions.</value>
        List<State> States {
            get;
        }

        /// <summary>
        /// Gets the transitions directly contained in this instance, and not recursively from this instance's inner petri nets.
        /// </summary>
        /// <value>The transitions.</value>
        List<Transition> Transitions {
            get;
        }

        /// <summary>
        /// Serializes the instance into the provided XML element.
        /// </summary>
        /// <param name="elem">The XML element to fill with the serialization data.</param>
        void Serialize(XElement elem);

        /// <summary>
        /// Creates an empty node correctly named to represent an entity, and fill it with the serialization data.
        /// </summary>
        /// <returns>The new, populated node.</returns>
        XElement GetXML();
    }
}
