//
//  PetriNetProvider.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2014-12-09.
//

using Petri.Model;

namespace Petri.Application
{
    /// <summary>
    /// This class provides a petri net by proxying the calls to its underlying Document instance.
    /// </summary>
    public class PetriNetProvider
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Application.PetriNetProvider"/> class.
        /// </summary>
        /// <param name="doc">Document.</param>
        public PetriNetProvider(Document doc)
        {
            _document = doc;
        }

        /// <summary>
        /// Gets the petri net.
        /// </summary>
        /// <value>The petri net.</value>
        public RootPetriNet PetriNet => _document.PetriNet;

        Document _document;
    }
}
