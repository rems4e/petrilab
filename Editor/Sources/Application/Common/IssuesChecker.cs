//
//  IssuesChecker.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2017-07-18.
//

using System;
using System.Collections.Generic;
using System.Linq;

using Petri.Code;
using Petri.Model;

namespace Petri.Application
{
    /// <summary>
    /// A class that checks if a model entity provokes issues.
    /// </summary>
    public static class IssuesChecker
    {
        /// <summary>
        /// Checks for issues in the given entity.
        /// </summary>
        /// <param name="entity">Entity.</param>
        public static void CheckIssues(Entity entity)
        {
            var rootDocument = entity.Document.RootDocument;
            CheckIssues(entity, rootDocument, recursive: true);
            if(entity is Model.Action || entity is Transition) {
                // Check the parent petri net for things like variables usage
                CheckIssues(entity.Document.PetriNet, rootDocument, recursive: false);
            }
        }
        /// <summary>
        /// Checks the issues of an entity.
        /// </summary>
        /// <param name="entity">The entity to check.</param>
        /// <param name="rootDocument">The entity's root document.</param>
        /// <param name="recursive">If <c>true</c>, the check is performed recursively.</param>
        static void CheckIssues(Entity entity, LocalDocument rootDocument, bool recursive)
        {
            rootDocument.RemoveIssue(entity);
            if(entity is Model.Action action) {
                if(action.Invocation is WrapperFunctionInvocation) {
                    var error = ((WrapperFunctionInvocation)action.Invocation).Error;
                    if(error != null) {
                        rootDocument.AddIssue(action, error, EntityIssue.Kind.CompilationError);
                    }
                } else if(rootDocument.Settings.Language != Language.Python) {
                    if(!action.Invocation.Function.ReturnType.Equals(Code.Type.UnknownType(rootDocument.Settings.Language))
                       && !action.Invocation.Function.ReturnType.Equals(rootDocument.Settings.Enum.Type)) {
                        rootDocument.AddIssue(action,
                                              Configuration.GetLocalized("Incorrect return type for the function: {0} expected, {1} found.",
                                                                         rootDocument.Settings.Enum.Name,
                                                                         action.Invocation.Function.ReturnType.ToString()),
                                              EntityIssue.Kind.CompilationError);
                    }
                }

                var vars = new List<VariableExpression>();
                action.GetVariables(vars);
                foreach(var v in vars) {
                    if(!action.Document.PetriNet.Variables.ContainsKey(v)) {
                        rootDocument.AddIssue(action,
                                              Configuration.GetLocalized("The variable '{0}' is not declared.",
                                                                         v.MakeUserReadable()),
                                              EntityIssue.Kind.VariableUndeclared);
                    }
                }
            } else if(entity is Transition transition) {
                if(transition.Condition is WrapperFunctionInvocation) {
                    var error = ((WrapperFunctionInvocation)transition.Condition).Error;
                    if(error != null) {
                        rootDocument.AddIssue(transition, error, EntityIssue.Kind.CompilationError);
                    }
                }
                var vars = new List<VariableExpression>();
                transition.GetVariables(vars);
                foreach(var v in vars) {
                    if(!transition.Document.PetriNet.Variables.ContainsKey(v)) {
                        rootDocument.AddIssue(transition,
                                              Configuration.GetLocalized("The variable '{0}' is not declared.",
                                                                         v.MakeUserReadable()),
                                              EntityIssue.Kind.VariableUndeclared);
                    }
                }
            } else if(entity is PetriNet petriNet) {
                if(recursive) {
                    foreach(var s in petriNet.States) {
                        CheckIssues(s, rootDocument, true);
                    }
                    foreach(var t in petriNet.Transitions) {
                        CheckIssues(t, rootDocument, true);
                    }
                    foreach(var c in petriNet.Comments) {
                        CheckIssues(c, rootDocument, true);
                    }
                }

                if(petriNet is RootPetriNet rootPetriNet) {
                    if(recursive && rootPetriNet.Document is ExternalDocument) {
                        // Check the ExternalPetriNet for parameter matching, among others.
                        CheckIssues(rootPetriNet.Document.Parent, rootDocument, true);
                    }

                    var set = new HashSet<VariableExpression>();
                    rootPetriNet.GetVariables(set);
                    foreach(var v in rootPetriNet.Variables) {
                        if(!set.Contains(v.Key)) {
                            rootDocument.AddIssue(rootPetriNet,
                                                  Configuration.GetLocalized("Unused variable '{0}'.",
                                                                             v.Key.MakeUserReadable()),
                                                  EntityIssue.Kind.UnusedVariable);
                        }
                    }
                } else if(petriNet is ExternalInnerPetriNet externalInnerPetriNet) {
                    if(externalInnerPetriNet.ExternalDocument is InvalidExternalDocument) {
                        var i = (InvalidExternalDocument)externalInnerPetriNet.ExternalDocument;
                        rootDocument.AddIssue(externalInnerPetriNet, i.Message, i.Error);
                    }

                    if(externalInnerPetriNet.ExternalPetriSource != null) {
                        var argsNotFound = from arg in externalInnerPetriNet.Arguments
                                           where !externalInnerPetriNet.ExternalDocument.PetriNet.Parameters.Contains(arg.Key)
                                           select arg.Key;

                        foreach(var arg in argsNotFound) {
                            rootDocument.AddIssue(externalInnerPetriNet,
                                                  Configuration.GetLocalized("The argument '{0}' does not exist as a parameter.",
                                                                             arg.MakeUserReadable()),
                                                  EntityIssue.Kind.UnmatchedArgument);
                        }

                        var returnValuesNotFound = from dest in externalInnerPetriNet.ReturnValuesDestination
                                                   where !externalInnerPetriNet.ExternalDocument.PetriNet.ReturnValues.ContainsKey(dest.Key)
                                                   select dest.Key;

                        foreach(var dest in returnValuesNotFound) {
                            rootDocument.AddIssue(externalInnerPetriNet,
                                                  Configuration.GetLocalized("The destination '{0}' does not exist as a return value.",
                                                                             dest),
                                                  EntityIssue.Kind.UnmatchedReturnValue);
                        }
                    }
                }
            } else if(!(entity is Comment)) {
                throw new Exception(string.Format("IssuesChecker.CheckIssues: Should not get there ({0})!", entity));
            }
        }
    }
}
