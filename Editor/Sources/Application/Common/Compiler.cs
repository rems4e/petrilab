//
//  Compiler.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2014-11-22.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Petri.Application
{
    /// <summary>
    /// This class is in charge of invoking the a document's compiler program and retrieving stdout and stderr.
    /// </summary>
    public class Compiler
    {
        /// <summary>
        /// The return status of a compilation process
        /// </summary>
        public enum CompilationStatus
        {
            /// <summary>
            /// The compilation has succeeded without notice.
            /// </summary>
            Success,

            /// <summary>
            /// The compilation has succeeded, but returned a warning notice.
            /// </summary>
            Warning,

            /// <summary>
            /// The compilation has returned an error notice.
            /// </summary>
            Error,
        }

        /// <summary>
        /// The detected compiler toolchain.
        /// </summary>
        public enum Toolchain
        {
            /// <summary>
            /// Member of the GCC toolchain, like gcc or g++.
            /// </summary>
            GCC,

            /// <summary>
            /// Member of the Clang toolchain, like clang or clang++.
            /// </summary>
            Clang,

            /// <summary>
            /// The Microsoft Visual C# compiler
            /// </summary>
            MSVisualCS,

            /// <summary>
            /// The Mono C# compiler.
            /// </summary>
            MonoCS,

            /// <summary>
            /// An unknown toolchain.
            /// </summary>
            Unknown,
        }

        /// <summary>
        /// Detects the compiler toolchain by invoking an instance of the compiler and parsing the result.
        /// </summary>
        /// <returns>The toolchain.</returns>
        public virtual Toolchain DetectToolchain()
        {
            var toolchainResult = InvokeExternal(
                _document.Settings.MatchedCompilerInfo.Item1.Compiler,
                "--version",
                System.IO.Directory.GetParent(_document.Path).FullName,
                null
            );
            var toolchain = Toolchain.Unknown;
            if(_document.Settings.Language == Code.Language.CSharp) {
                if(toolchainResult.Item2.Contains("Microsoft") && toolchainResult.Item2.Contains("Visual C#")) {
                    toolchain = Toolchain.MSVisualCS;
                } else if(toolchainResult.Item2.Contains("Mono C# compiler")) {
                    toolchain = Toolchain.MonoCS;
                }
            } else {
                toolchain = Toolchain.GCC; // Most plausible?
                if(toolchainResult.Item2.Contains("clang")) {
                    toolchain = Toolchain.Clang;
                }
            }

            return toolchain;
        }

        /// <summary>
        /// Returns the worst compilation statu among the arguments.
        /// </summary>
        /// <returns>The worst status.</returns>
        /// <param name="s1">S1.</param>
        /// <param name="s2">S2.</param>
        public static CompilationStatus WorstStatus(CompilationStatus s1, CompilationStatus s2)
        {
            if(s1 == CompilationStatus.Error || s2 == CompilationStatus.Error) {
                return CompilationStatus.Error;
            } else if(s1 == CompilationStatus.Warning || s2 == CompilationStatus.Warning) {
                return CompilationStatus.Warning;
            }

            return CompilationStatus.Success;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.Compiler"/> class.
        /// </summary>
        /// <param name="doc">Document.</param>
        public Compiler(Document doc)
        {
            _document = doc;
        }

        /// <summary>
        /// Compiles the source. The source file's last modification is set to the current date.
        /// </summary>
        /// <returns>A tuple made of a boolean stating whether the compilation was successful, and stdout and stderr concatenated in that order.</returns>
        /// <param name="sources">The paths of the source files to compile.</param>
        /// <param name="intermediatePath">The intermediate compilation file to generate.</param>
        /// <param name="libPath">The library file to generate.</param>
        /// <param name="verboseOutput">The output we should write debug information, or <c>null</c> if we don't need them.</param>
        public Tuple<bool, string> CompileSource(List<Tuple<string, Code.Language>> sources,
                                                 string intermediatePath,
                                                 string libPath,
                                                 IOutput verboseOutput = null)
        {
            // Check that all paths are absolute paths.
            if(!sources.Aggregate(true,
                                  (bool rooted, Tuple<string, Code.Language> path) => {
                                      return (System.IO.Path.IsPathRooted(path.Item1) && rooted);
                                  }) || !System.IO.Path.IsPathRooted(libPath)) {
                throw new Exception("Compiler.CompileSource: Should not get there!");
            }

            var toolchain = DetectToolchain();

            if(verboseOutput != null) {
                verboseOutput.WriteLine("{0}", Configuration.GetLocalized("Compiler invocation:"));
            }
            var result = InvokeExternal(
                _document.Settings.MatchedCompilerInfo.Item1.Compiler,
                _document.Settings.GetCompilerArguments(sources, intermediatePath, toolchain),
                System.IO.Directory.GetParent(_document.Path).FullName,
                verboseOutput
            );

            var archiveArgs = _document.Settings.GetArchiverArguments(intermediatePath, libPath, toolchain);
            if(result.Item1 && archiveArgs != null) {
                if(verboseOutput != null) {
                    verboseOutput.WriteLine("{0}", Configuration.GetLocalized("Archiver invocation:"));
                }
                var archResult = InvokeExternal(
                    _document.Settings.Archiver,
                    archiveArgs,
                    System.IO.Directory.GetParent(_document.Path).FullName,
                    verboseOutput
                );

                result = Tuple.Create(result.Item1 && archResult.Item1, (result.Item2.Length > 0 ? (result.Item2 + "\n\n") : "") + archResult.Item2);
            }

            return Tuple.Create(result.Item1, result.Item2.Trim());
        }

        /// <summary>
        /// Invokes the external tool with the provided arguments.
        /// </summary>
        /// <returns>The execution result.</returns>
        /// <param name="program">Program.</param>
        /// <param name="args">Arguments.</param>
        /// <param name="wd">The tool's working directory.</param>
        /// <param name="verboseOutput">The output we should write debug information, or <c>null</c> if we don't need them.</param>
        protected virtual Tuple<bool, string> InvokeExternal(string program, string args, string wd, IOutput verboseOutput)
        {
            if(verboseOutput != null) {
                verboseOutput.WriteLine("{0} {1}", program, args);
            }
            if(args.Length > 2000) {
                return Tuple.Create(
                    false,
                    Configuration.GetLocalized(
                        "Error: the invocation is too long ({0}) characters. Try to remove some recursive inclusion paths.",
                        args.Length
                    )
                );
            }
            var p = Application.CreateProcess(program, args);
            p.StartInfo.WorkingDirectory = wd;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.RedirectStandardError = true;

            var outputBuilder = new StringBuilder();
            var errBuilder = new StringBuilder();

            p.OutputDataReceived += (sender, ev) => {
                if(ev.Data != null) {
                    outputBuilder.AppendLine(ev.Data);
                }
            };
            p.ErrorDataReceived += (sender, ev) => {
                if(ev.Data != null) {
                    errBuilder.AppendLine(ev.Data);
                }
            };

            p.Start();
            p.BeginOutputReadLine();
            p.BeginErrorReadLine();

            p.WaitForExit();

            if(outputBuilder.Length > 0 && errBuilder.Length > 0) {
                outputBuilder.Append("\n");
            }
            outputBuilder.Append(errBuilder);

            bool success = p.ExitCode == 0;
            if(!success) {
                outputBuilder.Insert(0, string.Format("{0} {1}\n", program, args));
            }

            return Tuple.Create(success, outputBuilder.ToString());
        }

        Document _document;
    }
}
