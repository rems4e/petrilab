//
//  Application.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2014-11-13.
//

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Petri.Application
{
    /// <summary>
    /// The class containing the entry point of the program.
    /// </summary>
    public class Application
    {
        /// <summary>
        /// The entry point of the program. Manages boths CLI and GUI, depending on whether arguments were given or not.
        /// </summary>
        /// <param name="args">The command-line arguments.</param>
        /// <returns>The exit code that is given to the operating system after the program ends.</returns>
        public static int Main(string[] args)
        {
            int index = Array.FindIndex(args, (s) => {
                return s == "--lang";
            });

            if(index != -1 && index < args.Length - 1) {
                Configuration.Language = args[index + 1];
                var list = new List<string>(args);
                list.RemoveAt(index);
                list.RemoveAt(index);
                args = list.ToArray();
            }

            if(args.Length > 0) {
                return CLI.CLIApplication.Main(args);
            } else {
                return GUI.GUIApplication.Main(null, false);
            }
        }

        /// <summary>
        /// Gets a value indicating whether the application is in development.
        /// </summary>
        /// <value><c>true</c> if development version; otherwise, <c>false</c>.</value>
        public static bool DevelopmentVersion {
            get {
                return VersionSuffix.EndsWithInv("-dev");
            }
        }

        /// <summary>
        /// Gets a value indicating the version suffix of the application.
        /// </summary>
        /// <value>The version suffix.</value>
        public static string VersionSuffix {
            get {
                return "-beta2";
            }
        }

        /// <summary>
        /// Gets the version of the debugger.
        /// </summary>
        /// <value>The version.</value>
        public static string Version {
            get {
                var version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
                return version.Major + "." + version.Minor + "." + version.Build + VersionSuffix;
            }
        }

        /// <summary>
        /// The web site info.
        /// </summary>
        public static readonly string WebSiteInfo = "https://sigilence-technologies.com/petrilab";

        /// <summary>
        /// Creates a process that can to be further configured.
        /// When the parent process (i.e., us) is being debugged, a normal process creation propagates the debugger command-line options in the form of environment variables.
        /// The purpose of this function is to remove the debugging command line option from the child's process' environment variables.
        /// This avoids some weird side effects, from a developper point of view.
        /// </summary>
        /// <returns>The process.</returns>
        /// <param name="program">The executable name.</param>
        /// <param name="arguments">The executable's arguments</param>
        public static System.Diagnostics.Process CreateProcess(string program = "", string arguments = "")
        {
            var process = new System.Diagnostics.Process();

            // This snippet removes the debugging environment variable from the child process, as it causes an error when the app is run into a debugger.
            string monoEnv = process.StartInfo.EnvironmentVariables["MONO_ENV_OPTIONS"];
            if(monoEnv != null) {
                monoEnv = System.Text.RegularExpressions.Regex.Replace(monoEnv,
                                                                       "--debugger-agent=transport=dt_socket,address=127.0.0.1:[0-9]{3,5}",
                                                                       "");
                process.StartInfo.EnvironmentVariables["MONO_ENV_OPTIONS"] = monoEnv;
            }

            process.StartInfo.UseShellExecute = false;
            process.StartInfo.FileName = program;
            process.StartInfo.Arguments = arguments;

            return process;
        }

        /// <summary>
        /// Splits the given string by white spaces, keeping the quoted or doubly-quoted strings as a whole. Does not keep empty parts, unless the last one if the string ends with a space, and <c>firstOnly</c> is <c>true</c>.
        /// </summary>
        /// <returns>The parts of the string.</returns>
        /// <param name="s">The string to split.</param>
        /// <param name="howMuch">If different from <c>int.MaxValue</c>, only the N specifies count of tokens are split from the string and put into the 1st item of the return value, and the remainder of the string is kept as is in the N + 1 item of the return value.</param>
        public static List<string> SpaceSplit(string s, int howMuch = int.MaxValue)
        {
            if(howMuch == 0) {
                return new List<string> { s };
            }
            var result = new List<string>();
            bool quote = false, apostrophe = false, escaped = false;
            var substring = new List<char>();
            for(int i = 0; i < s.Length; ++i) {
                switch(s[i]) {
                case '\'':
                    if(!quote && !escaped) {
                        apostrophe = !apostrophe;
                        if(!apostrophe) {
                            goto default;
                        }
                    } else {
                        if(escaped && !apostrophe) {
                            substring.Add('\\');
                        }
                        substring.Add('\'');
                    }
                    escaped = false;
                    break;
                case '"':
                    if(!apostrophe && !escaped) {
                        quote = !quote;
                        if(!quote) {
                            goto default;
                        }
                    } else {
                        if(escaped && !quote) {
                            substring.Add('\\');
                        }
                        substring.Add('"');
                    }
                    escaped = false;
                    break;
                case '\\':
                    if(escaped) {
                        substring.Add('\\');
                    }
                    escaped = !escaped;
                    break;
                default:
                    bool cut = false;
                    if((((s[i] == '\'' || s[i] == '"' || s[i] == ' ') && !escaped) || s[i] == '\t') && !quote && !apostrophe) {
                        cut = true;
                    } else {
                        substring.Add(s[i]);
                        if(i == s.Length - 1) {
                            cut = true;
                        }
                    }

                    if(cut) {
                        bool addedN = false;
                        if(substring.Count > 0) {
                            result.Add(new string(substring.ToArray()));
                            addedN = result.Count == howMuch;
                        }

                        substring.Clear();
                        if(addedN) {
                            if(i + 1 < s.Length || (i == s.Length - 1 && (s[i] == ' ' || s[i] == '\t'))) {
                                result.Add(s.Substring(i + 1));
                            }
                            return result;
                        }
                    }
                    escaped = false;
                    break;
                }
            }

            return result;
        }

        /// <summary>
        /// Returns the given path by substituting, if found, the user home directory prefix by "~".
        /// </summary>
        /// <returns>The path.</returns>
        /// <param name="path">Path.</param>
        public static string ShortPath(string path)
        {
            if(path.StartsWithInv(System.Environment.GetFolderPath(Environment.SpecialFolder.UserProfile))) {
                path = "~" + path.Substring(System.Environment.GetFolderPath(Environment.SpecialFolder.UserProfile).Length);
            }

            return path;
        }

        /// <summary>
        /// Takes a task that is not awaited, swallowing compiler warning CS4014 (not awaited).
        /// If the task throws an exception, it will be reported using the unhandled exception handler.
        /// </summary>
        /// <param name="task">The task to swallow.</param>
        public static void SwallowAsync(Task task)
        {
            Task.Factory.StartNew(() => {
                try {
                    task.Wait();
                } catch(AggregateException ee) {
                    foreach(var e in ee.InnerExceptions) {
                        Application.OnExceptionUnhandled(new UnhandledExceptionEventArgs(e, false));
                    }
                } catch(System.Threading.ThreadAbortException) {
                    System.Threading.Thread.ResetAbort();
                } catch(Exception e) {
                    Application.OnExceptionUnhandled(new UnhandledExceptionEventArgs(e, false));
                }
            });
        }

        /// <summary>
        /// Takes a action and execute it asynchronously.
        /// If the action throws an exception, it will be reported using the unhandled exception handler.
        /// </summary>
        /// <param name="action">The task to swallow.</param>
        public static void SwallowAsync(Action action)
        {
            SwallowAsync(Task.Factory.StartNew(action));
        }

        /// <summary>
        /// The delegate mapped to the <see cref="ExceptionUnhandled"/> event.
        /// </summary>
        public delegate void ExceptionUnhandledDel(UnhandledExceptionEventArgs e);

        /// <summary>
        /// Occurs when there is an update in the set of current active states.
        /// </summary>
        public static event ExceptionUnhandledDel ExceptionUnhandled;

        /// <summary>
        /// Triggers the <see cref="ExceptionUnhandled"/> event.
        /// </summary>
        /// <param name="e">E.</param>
        public static void OnExceptionUnhandled(UnhandledExceptionEventArgs e)
        {
            ExceptionUnhandled?.Invoke(e);
        }

        /// <summary>
        /// Gets the exception message from an exception object.
        /// Returns <c>null</c> if the argument is not a subclass of <see cref="System.Exception"/>.
        /// </summary>
        /// <returns>The exception message.</returns>
        /// <param name="arg">Argument.</param>
        public static string GetExceptionMessage(object arg)
        {
            if(arg is Exception) {
                var e = (Exception)arg;
                Console.Error.WriteLine("{0}", e.StackTrace);

                string message = e.Message;
                if(e is System.Reflection.TargetInvocationException && e.InnerException != null) {
                    message = e.InnerException.Message;
                }

                return message;
            }

            return null;
        }
    }
}
