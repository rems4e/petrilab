//
//  EntityIssue.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2017-04-26.
//

using System;

namespace Petri.Application
{
    /// <summary>
    /// The descriptor of an issue concerning an entity.
    /// </summary>
    public class EntityIssue : IComparable<EntityIssue>
    {
        /// <summary>
        /// The type of issue
        /// </summary>
        public enum Kind
        {
            /// <summary>
            /// The entity causes a compilation warning.
            /// </summary>
            CompilationWarning,

            /// <summary>
            /// The entity causes a compilation error.
            /// </summary>
            CompilationError,

            /// <summary>
            /// The entity is an external petri net and could not be loaded.
            /// </summary>
            InvalidPetriNetReference,

            /// <summary>
            /// The external inner petri net is not in the language of its parent.
            /// </summary>
            WrongLanguage,

            /// <summary>
            /// One of the child of the petri net has an issue.
            /// </summary>
            ChildIssue,

            /// <summary>
            /// The argument's name does not correspond to an available parameter.
            /// </summary>
            UnmatchedArgument,

            /// <summary>
            /// The return value destination's name does not correspond to an available return value.
            /// </summary>
            UnmatchedReturnValue,

            /// <summary>
            /// The variable is used but not declared.
            /// </summary>
            VariableUndeclared,

            /// <summary>
            /// The variable is declared but not used.
            /// </summary>
            UnusedVariable,
        }

        /// <summary>
        /// Checks if the given issue is an error.
        /// </summary>
        /// <returns><c>true</c>, if the issue is an error, <c>false</c> otherwise.</returns>
        /// <param name="kind">Kind.</param>
        public static bool IsErrorKind(Kind kind)
        {
            return kind != Kind.CompilationWarning && kind != Kind.ChildIssue && kind != Kind.UnusedVariable;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Application.EntityIssue"/> class.
        /// </summary>
        /// <param name="message">The message of the issue.</param>
        /// <param name="kind">The issue's kind.</param>
        public EntityIssue(string message, Kind kind)
        {
            Message = message;
            IssueKind = kind;
        }

        /// <summary>
        /// Gets the message of the issue.
        /// </summary>
        /// <value>The message.</value>
        public string Message {
            get;
            private set;
        }

        /// <summary>
        /// Gets the kind of the issue.
        /// </summary>
        /// <value>The kind of the issue.</value>
        public Kind IssueKind {
            get;
            private set;
        }

        /// <summary>
        /// Gets a value indicating whether this <see cref="T:Petri.Application.EntityIssue"/> is an error.
        /// </summary>
        /// <value><c>true</c> if error; otherwise, <c>false</c>.</value>
        public bool IsError {
            get {
                return IsErrorKind(IssueKind);
            }
        }

        int IComparable<EntityIssue>.CompareTo(EntityIssue other)
        {
            return IssueKind.CompareTo(other.IssueKind);
        }
    }
}
