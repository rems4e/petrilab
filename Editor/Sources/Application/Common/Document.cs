//
//  Document.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2015-01-30.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;

using Petri.Code;
using Petri.Model;

namespace Petri.Application
{
    /// <summary>
    /// A petri net's document.
    /// </summary>
    public abstract class Document
    {
        /// <summary>
        /// Message output when upgrading from an old API to the last.
        /// </summary>
        public static readonly string UpgradingOldNewFormat = Configuration.GetLocalized("Upgrading document from API version {0} to version {1}…");

        /// <summary>
        /// Message output when an API upgrade is complete.
        /// </summary>
        public static readonly string UpgradeCompleteString = Configuration.GetLocalized("Migration done.");

        /// <summary>
        /// The environment variable containing the petri net's name.
        /// </summary>
        public static readonly string DEPLOY_ENVVAR_NAME = "PETRI_NAME";

        /// <summary>
        /// The environment variable containing the compilation profile's name.
        /// </summary>
        public static readonly string DEPLOY_ENVVAR_PROFILE = "PETRI_PROFILE";

        /// <summary>
        /// The environment variable containing the compilation product's path.
        /// </summary>
        public static readonly string DEPLOY_ENVVAR_LIB_PATH = "PETRI_LIB_PATH";

        /// <summary>
        /// Gets the document API version.
        /// </summary>
        /// <value>The document API version.</value>
        public static int DocumentAPIVersion => 106;

        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.Document"/> class.
        /// </summary>
        /// <param name="parent">The document's parent document.</param>
        /// <param name="path">The path to the document.</param>
        /// <param name="document">The XML descriptor of the document.</param>
        protected Document(ExternalInnerPetriNet parent, string path, XDocument document) : this(parent)
        {
            Path = PathUtility.GetFullPath(path);
            LoadFromXml(document);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.Document"/> class.
        /// </summary>
        /// <param name="parent">The document's parent document.</param>
        /// <param name="path">The path to the document.</param>
        protected Document(ExternalInnerPetriNet parent, string path) : this(parent, path, XDocument.Load(path))
        {
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.Document"/> class.
        /// </summary>
        /// <param name="parent">The document's parent document.</param>
        /// <param name="language">The language the petri net will use.</param>

        protected Document(ExternalInnerPetriNet parent, Language language) : this(parent)
        {
            Path = "";
            Settings = DocumentSettings.GetDefaultSettings(this, language);
            EntityFactory = new EntityFactory(Settings);
            PetriNet = new RootPetriNet(this, parent);

            AllFunctions = new Dictionary<string, List<Function>>();

            InitBuiltins();

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.Document"/> class.
        /// <param name="parent">The document's parent document.</param>
        /// </summary>
        Document(ExternalInnerPetriNet parent)
        {
            Parent = parent;
            Output = new ConsoleOutput(true);
            Compiler = new Compiler(this);
        }

        void InitBuiltins()
        {
            var builtins = new List<Function> { EntityFactory.DoNothingFunction };
            if(Settings.Language != Language.EmbeddedC) {
                builtins.Add(EntityFactory.PrintActionFunction);
                builtins.Add(EntityFactory.PauseFunction);
                builtins.Add(EntityFactory.RandomFunction);
                builtins.Add(EntityFactory.PrintVariablesFunction);
                builtins.Add(EntityFactory.PrintTextFunction);
            }

            AllFunctions.Add(Configuration.GetLocalized("Built-in functions"), builtins);
        }

        /// <summary>
        /// Gets the petri net enclosing this document. <c>null</c> if this instance is a base document,
        /// an actual petri net if this is an imported petri net.
        /// </summary>
        /// <value>The parent.</value>
        public ExternalInnerPetriNet Parent {
            get;
            private set;
        }

        /// <summary>
        /// Gets the one above all.
        /// </summary>
        /// <value>The root document.</value>
        public LocalDocument RootDocument {
            get {
                return Parent?.Document.RootDocument ?? (LocalDocument)this;
            }
        }

        /// <summary>
        /// Gets or sets the path to the document.
        /// </summary>
        /// <value>The path.</value>
        public string Path {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the text output manager.
        /// </summary>
        /// <value>The output.</value>
        public virtual IOutput Output {
            get;
            protected set;
        }

        /// <summary>
        /// Gets or sets the compiler.
        /// </summary>
        /// <value>The compiler.</value>
        public Compiler Compiler {
            get;
            set;
        }

        /// <summary>
        /// Gets the list of source files that need to be compiled in order to generate the dynamic library.
        /// </summary>
        /// <value>The sources.</value>
        public List<Tuple<string, Language>> Sources {
            get {
                var sources = new List<Tuple<string, Language>>();
                var extensions = new Dictionary<Language, List<string>>();

                extensions[Language.CSharp] = new List<string> { ".cs" };
                extensions[Language.C] = new List<string> { ".c" };
                extensions[Language.Cpp] = new List<string> { ".C", ".cpp", ".cxx", ".cc" };
                extensions[Language.Python] = new List<string> { ".py" };

                foreach(var ext in extensions) {
                    sources.AddRange(from path in Settings.Headers
                                     where ext.Value.Contains(System.IO.Path.GetExtension(path))
                                     select Tuple.Create(GetAbsoluteFromRelativeToDoc(path), ext.Key));
                }

                sources.Add(Tuple.Create(System.IO.Path.Combine(System.IO.Directory.GetParent(Path).FullName,
                                                                Settings.RelativeSourcePath), Settings.MainSourceLanguage));
                if(Settings.Language == Language.CSharp) {
                    sources.Add(Tuple.Create(System.IO.Path.Combine(System.IO.Directory.GetParent(Path).FullName,
                                                                    Settings.RelativeSourceHeaderPath), Settings.Language));
                }

                return sources;
            }
        }

        /// <summary>
        /// Adds a header to the document.
        /// </summary>
        /// <param name="header">Header.</param>
        public virtual void AddHeader(string header)
        {
            var filename = GetAbsoluteFromRelativeToDoc(header);

            if(header.Length == 0 || Settings.Headers.Contains(header) || Settings.Headers.Contains(filename)) {
                return;
            }

            try {
                var sections = Parser.Parse(Settings.Language, filename, "PetriLab");

                // We merge the new sections with the old.
                foreach(var sect in sections) {
                    List<Function> functions;
                    AllFunctions.TryGetValue(sect.Key, out functions);
                    if(functions == null) {
                        AllFunctions[sect.Key] = sect.Value;
                    } else {
                        functions.AddRange(sect.Value);
                    }
                }

                Settings.Headers.Add(header);
            } catch(Exception e) {
                NotifyError(Configuration.GetLocalized("Unable to add the header \"{0}\": {1}",
                                                       header,
                                                       e.Message));
            }
        }

        /// <summary>
        /// Gets all the knwown functions of the document.
        /// </summary>
        /// <value>All functions list.</value>
        public Dictionary<string, List<Function>> AllFunctions {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets the top-level petri net of the document.
        /// </summary>
        /// <value>The petri net.</value>
        public RootPetriNet PetriNet {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the flat petri net.
        /// </summary>
        /// <value>The flat petri net.</value>
        public FlatPetriNet FlatPetriNet {
            get {
                if(_flatPetriNet == null) {
                    ReassignIDs();
                    _flatPetriNet = new FlatPetriNet(PetriNet);
                }

                return _flatPetriNet;
            }
        }

        /// <summary>
        /// Invalidates the flat petri net cache.
        /// </summary>
        public void InvalidateFlatPetriNet()
        {
            _flatPetriNet = null;
        }

        /// <summary>
        /// Reassigns the IDs of the entities in this document, starting from 0.
        /// </summary>
        public virtual void ReassignIDs()
        {
            InvalidateHash();
            EntityFactory.ResetID(0);

            var list = PetriNet.BuildLocalEntitiesList();

            foreach(var e in list) {
                e.ID = EntityFactory.ConsumeID();
                if(e is ExternalInnerPetriNet) {
                    EntityFactory.ResetID(EntityFactory.ID + ((ExternalInnerPetriNet)e).ExternalDocument.FlatPetriNet.NextID);
                }
            }
        }

        /// <summary>
        /// Gets a path relative to the document's path from the given path.
        /// </summary>
        /// <returns>The relative to document.</returns>
        /// <param name="path">Path.</param>
        public string GetRelativeToDoc(string path)
        {
            path = PathUtility.GetFullPath(path);

            string parent = System.IO.Directory.GetParent(Path).FullName;

            var relative = PathUtility.GetRelativePath(path, parent);
            if(relative == "") {
                relative = "./";
            }

            return relative;
        }

        /// <summary>
        /// Gets an absolute path from the specified path, which must be relative to the document's path.
        /// </summary>
        /// <returns>The absolute from relative to document.</returns>
        /// <param name="path">Path.</param>
        public string GetAbsoluteFromRelativeToDoc(string path)
        {
            if(!System.IO.Path.IsPathRooted(path)) {
                return System.IO.Path.Combine(System.IO.Directory.GetParent(Path).FullName, path);
            }

            return path;
        }

        /// <summary>
        /// Loads the document from an XML descriptor.
        /// </summary>
        protected virtual void LoadFromXml(XDocument document)
        {
            InvalidateFlatPetriNet();
            InvalidateHash();
            ClearReachability();

            AllFunctions = new Dictionary<string, List<Function>>();
            FirstLevelExternalPetriNets.Clear();

            var root = document.FirstNode as XElement;

            int apiVersion = 99; // default API version if nothing in the document
            try {
                apiVersion = int.Parse(root.Attribute("APIVersion").Value);
            } catch {
                // Invalid? None? Assume API 99.
            }

            if(apiVersion > DocumentAPIVersion) {
                NotifyInfo(Configuration.GetLocalized("This document has been created by a more recent version of this application. Things may break, and some information may be be lost forever if you overwrite the document. Be careful!"));
            } else if(apiVersion < DocumentAPIVersion) {
                UpgradeDoc(root, apiVersion);
            }

            var settings = root.Element("Settings");
            Settings = new DocumentSettings(this, settings);
            EntityFactory = new EntityFactory(Settings);
            InitBuiltins();

            var hh = new List<string>(Settings.Headers);
            Settings.Headers.Clear();
            foreach(var h in hh) {
                AddHeader(h);
            }

            PetriNet = new RootPetriNet(this, Parent, root.Element("PetriNet"));

            ReassignIDs();
        }

        /// <summary>
        /// Upgrades the document to the last API version.
        /// </summary>
        /// <returns><c>true</c>, if document was upgraded, <c>false</c> otherwise.</returns>
        /// <param name="root">The root element of the document's XML descriptor.</param>
        /// <param name="apiVersion">The old API version.</param>
        protected virtual bool UpgradeDoc(XElement root, int apiVersion)
        {
            Console.WriteLine(UpgradingOldNewFormat, apiVersion, DocumentAPIVersion);

            bool result = false;
            var upgraders = DocumentUpgrader.DocumentUpgrader.GetUpgrader(root, apiVersion);
            foreach(var upgrader in upgraders) {
                result = upgrader.UpgradeDocument() || result;
            }

            Console.WriteLine("{0}", UpgradeCompleteString);

            return result;
        }

        /// <summary>
        /// Gets the code prefix of the document.
        /// </summary>
        /// <value>The code prefix.</value>
        public string CodePrefix {
            get {
                return Settings.Name;
            }
        }

        /// <summary>
        /// Generates the code without prompting the user. If no code have ever been generated for this document, meaning we don't know where to save it, then an Exception is thrown.
        /// <exception cref="Exception">When no source output path has been defined for the document.</exception>
        /// </summary>
        /// <param name="alreadyGenerated">The list of document paths that have already been generated.</param>
        public void GenerateCode(HashSet<string> alreadyGenerated)
        {
            foreach(var pn in FirstLevelExternalPetriNets) {
                if(pn.IsReachable) {
                    pn.ExternalDocument.GenerateCode(alreadyGenerated);
                }
            }

            if(!alreadyGenerated.Contains(Path)) {
                if(Settings.MatchedProfile.RelativeSourceOutputPath.Length == 0) {
                    var message = Configuration.GetLocalized(
                            "No source output path defined. Please open the Petri net with the graphical editor and generate the <language> code once.",
                            Settings.LanguageName()
                    );
                    NotifyError(message);
                    throw new GenerationException(message);
                }

                var generator = CodeGen.PetriGen.PetriGenForDoc(this);
                generator.WritePetriNet();

                _codeRanges = generator.CodeRanges;
                alreadyGenerated.Add(Path);
            }
        }

        /// <summary>
        /// Compiles the document. If some output is made by the invoked compiler, then the method returns false, true otherwise.
        /// </summary>
        /// <returns>The compilation success status and console messages that were output.</returns>
        /// <param name="alreadyCompiled">The list of external documents that have already been compiled, to avoid recompiling them.</param>
        /// <param name="verbose">Whether we want verbose compilation output.</param>
        public async Task<Tuple<Compiler.CompilationStatus, string>> Compile(HashSet<string> alreadyCompiled, bool verbose)
        {
            var result = Compiler.CompilationStatus.Success;
            string output = "";

            foreach(var pn in FirstLevelExternalPetriNets) {
                if(pn.IsReachable) {
                    var tup = await pn.ExternalDocument.Compile(alreadyCompiled, verbose);

                    result = Compiler.WorstStatus(result, tup.Item1);
                    if(tup.Item2.Length > 0) {
                        output = output + (output.Length > 0 ? "\n" : "") + pn.ExternalDocument.Path + ":" + tup.Item2;
                    }
                    if(result == Compiler.CompilationStatus.Error) {
                        break;
                    }
                }
            }
            if(result != Compiler.CompilationStatus.Error) {
                if(!alreadyCompiled.Contains(Path)) {
                    var o = await Task.Factory.StartNew(() => {
                        if(verbose) {
                            Output.WriteLine(Configuration.GetLocalized("Compiling document {0}…", Settings.Name));
                        }
                        var res = Compiler.CompileSource(
                            Sources,
                            Settings.StandardIntermediatePath,
                            Settings.StandardLibPath,
                            verbose ? Output : null
                        );

                        return res;
                    });

                    result = Compiler.WorstStatus(result, ParseCompilationOutput(o.Item2));
                    if(!o.Item1) {
                        result = Compiler.CompilationStatus.Error;
                    }

                    output = output + (output.Length > 0 ? "\n" : "") + o.Item2;
                    alreadyCompiled.Add(Path);
                }
            }

            return Tuple.Create(result, output);
        }

        string IssuePattern {
            get {
                string linePattern = "(?<line>(\\d+))", rowPattern = "(?<row>(\\d+))";
                return "((:" + linePattern + ":" + rowPattern + ")|(\\(" + linePattern + "," + rowPattern + "\\))): ";
            }
        }

        /// <summary>
        /// Gets the error pattern that must recognize an error line among the compiler's output.
        /// </summary>
        /// <value>The error pattern.</value>
        string ErrorPattern {
            get {
                return Regex.Escape(
                    Settings.Name + CodeGen.PetriGen.SourceExtensionFromLanguage(Settings.Language)
                ) + IssuePattern + "(fatal )?error:? (?<msg>(.*))$";
            }
        }

        /// <summary>
        /// Gets the warning pattern that must recognize a warning line among the compiler's output.
        /// </summary>
        /// <value>The warning pattern.</value>
        string WarningPattern {
            get {
                return ".*" + IssuePattern + "warning:? (?<msg>(.*))$";
            }
        }

        /// <summary>
        /// Parses the compilation output.
        /// </summary>
        /// <param name="output">Output.</param>
        /// <returns>Whether the compilation process has returned some warnings.</returns>
        protected Compiler.CompilationStatus ParseCompilationOutput(string output)
        {
            var status = Compiler.CompilationStatus.Success;

            var lines = output.Split(new string[] { Environment.NewLine },
                                     StringSplitOptions.RemoveEmptyEntries);

            var errorRegex = new Regex(ErrorPattern);
            var warningRegex = new Regex(WarningPattern);
            foreach(string line in lines) {
                var match = errorRegex.Match(line);
                if(match.Success) {
                    status = Compiler.WorstStatus(status, Compiler.CompilationStatus.Error);
                    HandleIssue(match, EntityIssue.Kind.CompilationError);
                } else {
                    match = warningRegex.Match(line);
                    if(match.Success) {
                        status = Compiler.WorstStatus(status, Compiler.CompilationStatus.Warning);
                        HandleIssue(match, EntityIssue.Kind.CompilationWarning);
                    }
                }
            }

            return status;
        }

        /// <summary>
        /// Handles the issue highlighted by the given match and of the given kind.
        /// </summary>
        /// <param name="match">Match.</param>
        /// <param name="issueKind">Issue kind.</param>
        void HandleIssue(System.Text.RegularExpressions.Match match, EntityIssue.Kind issueKind)
        {
            int lineNumber = int.Parse(match.Groups["line"].Value);
            if(_codeRanges != null) {
                // TODO: sort + binary search
                foreach(var entry in _codeRanges) {
                    if(lineNumber >= entry.Value.FirstLine && lineNumber <= entry.Value.LastLine) {
                        var entity = PetriNet.EntityFromID(entry.Key, true);
                        if(entity != null) { // The opposite is true only when en error occurs on a helper entity of the flat petri net.
                            string message;
                            // NOTE TO MAINTAINER: do not factor out the args to Configuration.GetLocalized, as it would fail unit tests.
                            if(issueKind == EntityIssue.Kind.CompilationError) {
                                message = Configuration.GetLocalized("Error on Line {0}, Row {1}:",
                                                                     lineNumber,
                                                                     match.Groups["row"].Value);
                            } else {
                                message = Configuration.GetLocalized("Warning on Line {0}, Row {1}:",
                                                                     lineNumber,
                                                                     match.Groups["row"].Value);
                            }
                            RootDocument.AddIssue(entity,
                                                  message + "\n" + match.Groups["msg"].Value,
                                                  issueKind);
                            break;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the ID manager of the document.
        /// </summary>
        /// <value>The identifier manager.</value>
        public EntityFactory EntityFactory {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the document's settings.
        /// </summary>
        /// <value>The settings.</value>
        public DocumentSettings Settings {
            get;
            protected set;
        }

        /// <summary>
        /// Gets the hash of the document's generated code.
        /// </summary>
        /// <value>The SHA256 hash value of the petri net.</value>
        public string Hash {
            get {
                if(_hashCache == null) {
                    var generator = CodeGen.PetriGen.PetriGenForDoc(this);
                    _hashCache = generator.GetHash();
                }

                return _hashCache;
            }
        }

        /// <summary>
        /// Invalidates the hash cache.
        /// </summary>
        public void InvalidateHash()
        {
            _hashCache = null;
        }

        /// <summary>
        /// Returns <c>true</c> if the given state is reachable from the initial states of the petri net.
        /// </summary>
        /// <returns><c>true</c> if reachable, <c>false</c> otherwise.</returns>
        /// <param name="state">State.</param>
        public bool IsReachable(State state)
        {
            if(!state.Document._reachability.ContainsKey(state)) {
                bool reachable = false;

                // Perform a depth-first search, until we find a reachable root node, or nothing.
                var stack = new Stack<State>();
                var dict = new Dictionary<State, State>();
                stack.Push(state);

                while(stack.Count > 0) {
                    var current = stack.Pop();

                    if((current.Document._reachability.ContainsKey(current) && current.Document._reachability[current]) || current is RootPetriNet) {
                        current.Document._reachability[current] = true;

                        // This will break the search, so we do it only if we are at the root.
                        if(current.Document is LocalDocument) {
                            reachable = true;
                        }
                    }

                    // If we are not reachable yet, continue the search
                    if(!reachable) {
                        if(current.IsStartState) {
                            // We cross the inner petri net's boundary to find states before
                            var actual = current.Parent;
                            if(current is RootPetriNet) {
                                actual = current.Document.Parent;
                            }
                            if(!dict.ContainsKey(actual)) {
                                stack.Push(actual);
                                dict[actual] = current;
                            }
                        } else {
                            foreach(var t in current.TransitionsBefore) {
                                var before = t.Before;
                                if(before is PetriNet) {
                                    // Get into the petri net to follow the states' chain
                                    before = ((PetriNet)before).ExitPoint;
                                }

                                if(!dict.ContainsKey(before)) {
                                    stack.Push(before);
                                    dict[before] = current;
                                }
                            }
                        }
                    } else {
                        State top = current;
                        current.Document._reachability[current] = true;
                        // All the path that lead us to the current element is reachable as well.
                        while(dict.TryGetValue(top, out State elem)
                             // Break if we cross an external document's boundary, and this external document is not reachable
                             && !(top is ExitPoint && top.Parent is RootPetriNet && top.Document is ExternalDocument && (!top.Document.Parent.Document._reachability.ContainsKey(top.Document.Parent) || !top.Document.Parent.Document._reachability[top.Document.Parent]))) {

                            // Avoids cycles
                            dict.Remove(top);

                            elem.Document._reachability[elem] = true;
                            top = elem;
                        }

                        reachable = false;
                    }
                }

                if(!state.Document._reachability.ContainsKey(state)) {
                    state.Document._reachability[state] = false;
                }
            }

            return state.Document._reachability[state] && (state.Parent?.Document.IsReachable(state.Parent) ?? true);
        }

        /// <summary>
        /// Checks if the given entity is reachable.
        /// </summary>
        /// <returns><c>true</c> if reachable, <c>false</c> otherwise.</returns>
        /// <param name="e">The entity.</param>
        public bool IsReachable(Entity e)
        {
            if(e is State) {
                return IsReachable((State)e);
            } else if(e is Transition) {
                return IsReachable(((Transition)e).Before);
            }

            return false;
        }

        /// <summary>
        /// Clears the reachability cache.
        /// </summary>
        public void ClearReachability()
        {
            _reachability.Clear();
            foreach(var pn in AllExternalDocuments) {
                pn.InvalidateFlatPetriNet();
            }
        }

        /// <summary>
        /// Gets the first level external petri nets.
        /// </summary>
        /// <value>The first level external petri nets.</value>
        public HashSet<ExternalInnerPetriNet> FirstLevelExternalPetriNets {
            get;
            private set;
        } = new HashSet<ExternalInnerPetriNet>();

        /// <summary>
        /// Recursively gets all the external documents imported in this petri net.
        /// </summary>
        /// <value>All external documents.</value>
        public List<ExternalDocument> AllExternalDocuments {
            get {
                var result = new List<ExternalDocument>();
                foreach(var pn in FirstLevelExternalPetriNets) {
                    result.Add(pn.ExternalDocument);
                    result.AddRange(pn.ExternalDocument.AllExternalDocuments);
                }

                return result;
            }
        }

        /// <summary>
        /// Recursively gets the paths of all the external documents imported in this document that are reachable.
        /// </summary>
        /// <value>All external paths.</value>
        public Dictionary<string, ExternalDocument> AllExternalReachablePaths {
            get {
                var result = new Dictionary<string, ExternalDocument>();
                foreach(var doc in AllExternalDocuments) {
                    if(doc.Parent.IsReachable) {
                        result[doc.Path] = doc;
                    }
                }

                return result;
            }
        }

        /// <summary>
        /// Updates the external documents referenced by this document if they were reloaded since last loaded.
        /// </summary>
        /// <returns>Whether this document or one of its external child documents have been updated.</returns>
        public virtual bool UpdateExternalDocuments()
        {
            bool updated = false;
            foreach(var p in FirstLevelExternalPetriNets) {
                if(p.Update()) {
                    updated = true;
                }
            }

            return updated;
        }

        /// <summary>
        /// Gets a value indicating whether this <see cref="T:Petri.Application.Document"/> is generated.
        /// </summary>
        /// <value><c>true</c> if is generated; otherwise, <c>false</c>.</value>
        public bool IsGenerated {
            get {
                var sourcePath = Settings.StandardSourcePath;
                if(System.IO.File.Exists(sourcePath)) {
                    var content = System.IO.File.ReadAllText(sourcePath);
                    var generator = CodeGen.PetriGen.PetriGenForDoc(this);

                    return content == generator.GetGeneratedCode();
                }

                return false;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this <see cref="T:Petri.Application.Document"/> is compiled.
        /// </summary>
        /// <value><c>true</c> if is compiled; otherwise, <c>false</c>.</value>
        public bool IsCompiled {
            get {
                var sourcePath = Settings.StandardSourcePath;
                var libPath = Settings.StandardSourcePath;
                if(System.IO.File.Exists(sourcePath) && System.IO.File.Exists(libPath)) {
                    return System.IO.File.GetLastWriteTime(libPath) >= System.IO.File.GetLastWriteTime(sourcePath);
                }

                return false;
            }
        }

        /// <summary>
        /// Notifies an error to the user.
        /// </summary>
        /// <param name="error">The message.</param>
        public abstract void NotifyError(string error);

        /// <summary>
        /// Notifies a warning to the user.
        /// </summary>
        /// <param name="warning">The message.</param>
        public abstract void NotifyWarning(string warning);

        /// <summary>
        /// Notifies some information to the user.
        /// </summary>
        /// <param name="info">The message.</param>
        public abstract void NotifyInfo(string info);

        /// <summary>
        /// The flat petri net cache.
        /// </summary>
        protected FlatPetriNet _flatPetriNet = null;
        string _hashCache;

        Dictionary<State, bool> _reachability = new Dictionary<State, bool>();

        Dictionary<UInt64, CodeGen.CodeRange> _codeRanges;
    }
}
