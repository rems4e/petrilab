//
//  DebugClient.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2014-11-23.
//

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

using Newtonsoft.Json.Linq;

using Petri.Model;

namespace Petri.Application.Debugger
{
    /// <summary>
    /// The debug client, responsible for the communication with the debug server.
    /// </summary>
    public class DebugClient
    {
        /// <summary>
        /// Gets the debugger API's version, which has to match the debug server's for a handshake to take place.
        /// </summary>
        /// <value>The debugger API vrsion.</value>
        public static string DebuggerAPIVersion {
            get {
                return "106";
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.Debugger.DebugClient"/> class.
        /// </summary>
        /// <param name="doc">Document.</param>
        /// <param name="debuggable">Debuggable.</param>
        public DebugClient(LocalDocument doc, IDebuggable debuggable)
        {
            _debuggable = debuggable;
            _document = doc;
            _sessionState = SessionState.Stopped;
            _petriState = PetriState.Stopped;
        }

        /// <summary>
        /// Asserts that the session have correctly been shit down before GC.
        /// <see cref="Petri.Application.Debugger.DebugClient"/> is reclaimed by garbage collection.
        /// </summary>
        ~DebugClient()
        {
            if(CurrentPetriState != PetriState.Stopped || CurrentSessionState != SessionState.Stopped) {
                throw new Exception(Configuration.GetLocalized("Debugger still running!"));
            }
        }

        /// <summary>
        /// The state of a debugger session.
        /// </summary>
        public enum SessionState
        {
            /// <summary>
            /// A starting debugger session.
            /// </summary>
            Starting,

            /// <summary>
            /// A started debug session.
            /// </summary>
            Started,

            /// <summary>
            /// A stopped debug session
            /// </summary>
            Stopped
        }

        /// <summary>
        /// Gets the current state of the debugger session.
        /// </summary>
        /// <value>The state of the current session.</value>
        public SessionState CurrentSessionState {
            get {
                return _sessionState;
            }
            private set {
                _sessionState = value;
                _debuggable.NotifyStateChanged();
            }
        }

        /// <summary>
        /// The state of the petri net's execution
        /// </summary>
        public enum PetriState
        {
            /// <summary>
            /// A starting petri net.
            /// </summary>
            Starting,

            /// <summary>
            /// A started petri net.
            /// </summary>
            Started,

            /// <summary>
            /// A petri net that is being paused.
            /// </summary>
            Pausing,

            /// <summary>
            /// A paused petri net.
            /// </summary>
            Paused,

            /// <summary>
            /// A stopping petri net.
            /// </summary>
            Stopping,

            /// <summary>
            /// A stopped petri net.
            /// </summary>
            Stopped
        }

        /// <summary>
        /// Error returned by execPoxy
        /// </summary>
        enum ProxyError
        {
            /// <summary>
            /// Wrong usage. This should not concern us.
            /// </summary>
            Usage = 1,

            /// <summary>
            /// The specified executable could not be found.
            /// </summary>
            ExecNotFound = 2,

            /// <summary>
            /// Error trying to change directory before execution.
            /// </summary>
            DirNotFound = 3,

            /// <summary>
            /// Any other error.
            /// </summary>
            Unknown = 64,
        }


        /// <summary>
        /// Gets the state of the PetriNet enclosed in the DebugServer's dynamic library.
        /// </summary>
        /// <value><c>true</c> if the petri net running; otherwise, <c>false</c>.</value>
        public PetriState CurrentPetriState {
            get {
                return _petriState;
            }
            set {
                _petriState = value;
                if(value != PetriState.Started && value != PetriState.Starting) {
                    _debuggable.BaseDebugController.TemporaryBreakpoint = null;
                }
                _debuggable.NotifyStateChanged();
            }
        }

        /// <summary>
        /// Gets a value indicating whether this <see cref="Petri.Application.Debugger.DebugClient"/>'s petri net is paused or running.
        /// </summary>
        /// <value><c>true</c> if petri alive; otherwise, <c>false</c>.</value>
        public bool PetriAlive {
            get {
                return CurrentPetriState == PetriState.Started || CurrentPetriState == PetriState.Pausing || CurrentPetriState == PetriState.Paused;
            }
        }

        /// <summary>
        /// Sets a value indicating whether this <see cref="Petri.Application.Debugger.DebugClient"/> is paused.
        /// The pause will be effective just after the states of the petri net that are active when the message is received have finished their execution.
        /// </summary>
        /// <value><c>true</c> if pause; otherwise, <c>false</c>.</value>
        public void SetPause(bool pause)
        {
            if(PetriAlive) {
                try {
                    if(pause) {
                        CurrentPetriState = PetriState.Pausing;
                        SendObject(new JObject(new JProperty("type", "pause")));
                    } else {
                        CurrentPetriState = PetriState.Starting;
                        SendObject(new JObject(new JProperty("type", "resume")));
                    }
                } catch(Exception e) {
                    _debuggable.NotifyUnrecoverableError(Configuration.GetLocalized("An error occurred in the debugger when pausing the petri net: {0}",
                                                                                    e.Message));
                    Detach();
                }
            }
        }

        /// <summary>
        /// Listens to an incoming TCP connection and either returns <c>null</c> if the connection is interrupted
        /// (see <paramref name="interrupt"/>), a <c>TcpClient</c> instance if the connection was successful.
        /// </summary>
        /// <returns>A <c>TcpClient</c> instance or <c>null</c>, depending on whether the listen operation was interrupted.</returns>
        /// <param name="interrupt">Set to 1 to interrupt the connection.</param>
        TcpClient TryConnectAsServer(ref int interrupt)
        {
            var listener = new TcpListener(System.Net.IPAddress.Any, _document.Settings.CurrentProfile.DebugPort);
            listener.Start();
            while(Thread.VolatileRead(ref interrupt) == 0) {
                if(listener.Pending()) {
                    var client = listener.AcceptTcpClient();
                    listener.Stop();
                    return client;
                }
                Thread.Sleep(100);
            }

            listener.Stop();
            return null;
        }

        /// <summary>
        /// Tries to connect to a debugger socket and either returns <c>null</c> if the connection is interrupted
        /// (see <paramref name="interrupt"/>), a <c>TcpClient</c> instance if the connection was successful.
        /// </summary>
        /// <returns>A <c>TcpClient</c> instance or <c>null</c>, depending on whether the listen operation was interrupted.</returns>
        /// <param name="interrupt">Set to 1 to interrupt the connection.</param>
        TcpClient TryConnectAsClient(ref int interrupt)
        {
            while(Thread.VolatileRead(ref interrupt) == 0) {
                try {
                    var socket = new TcpClient();
                    socket.Connect(_document.Settings.CurrentProfile.Hostname,
                                   _document.Settings.CurrentProfile.DebugPort);
                    return socket;
                } catch {
                    // Will loop and retry, but will timeout or get interrupted if never succeeding
                }
                Thread.Sleep(100);
            }

            return null;
        }

        /// <summary>
        /// Tries to connect to the debug server's socket. Returns a connected <c>TcpClient</c> or throws an exception.
        /// </summary>
        /// <param name="runInEditor">Whether to listen to the TCP port.</param>
        /// <param name="timeout">The timeout in milliseconds for listening.</param>
        public async Task<TcpClient> TryConnect(bool runInEditor, int timeout)
        {
            if(_socket != null) {
                throw new InvalidOperationException("The socket is still alive!");
            }

            int connectionInterrupted = 0;

            Task<TcpClient> connectorTask;
            if(runInEditor) {
                connectorTask = Task.Factory.StartNew(() => TryConnectAsServer(ref connectionInterrupted));
            } else {
                connectorTask = Task.Factory.StartNew(() => TryConnectAsClient(ref connectionInterrupted));
            }

            var deadline = DateTime.Now.AddMilliseconds(timeout);
            try {
                while(true) {
                    if(DateTime.Now > deadline) {
                        // Timeout
                        throw new TimeoutException();
                    } else if(_process != null && _process.HasExited) {
                        if(runInEditor && _process.ExitCode == CLI.CLIApplication.LoadLibFailure) {
                            // Could not load the dynamic lib
                            // We can now about the exit code's meaning as we launched the server through our software.
                            throw new RunInEditorLoadLibraryDebuggerException();
                        } else {
                            // Executable could not be found or has exited
                            throw new InvalidProgramStateDebuggerException();
                        }
                    } else if(CurrentSessionState == SessionState.Stopped || _debuggable.InterruptAttach()) {
                        // User interrupted
                        throw new AttachInterruptedDebuggerException();
                    } else if(connectorTask.IsCompleted) {
                        try {
                            return connectorTask.Result;
                        } catch(AggregateException exception) {
                            throw exception.InnerException;
                        }
                    }
                    await Task.Delay(100);
                }
            } catch {
                Thread.VolatileWrite(ref connectionInterrupted, 1);
                connectorTask.Wait();
                throw;
            }
        }

        /// <summary>
        /// Attaches this instance to a DebugServer listening on the same port as in the Settings of the document.
        /// If the document's <see cref="Petri.Application.DocumentSettingsProfile.DebugMode"/> property is <c>RunInEditor</c>, then we create our own hosting process.
        /// If it equals <c>CreateHost</c>, the hosting process created is the one given by the document's settings.
        /// Else, we attach to an already existing server.
        /// </summary>
        public async Task Attach()
        {
            if(CurrentSessionState == SessionState.Stopped) {
                if(_document.Settings.CurrentProfile.DebugMode == DebugMode.RunInEditor) {
                    var copyProg = "petrilab";
                    var copyArgs = " --debug-host \"" + PathUtility.GetFullPath(_document.Path) + "\"";

                    // Launch a copy of this program that will host the petri net's runtime.
                    if(!Environment.GetEnvironmentVariables().Contains("PETRILAB_AM_I_IN_BUNDLE")) {
                        copyProg = "mono";
                        copyArgs = System.Reflection.Assembly.GetExecutingAssembly().Location + copyArgs;
                    }
                    var process = Application.CreateProcess(
                        copyProg,
                        copyArgs
                    );

                    process.StartInfo.WorkingDirectory = System.IO.Directory.GetParent(_document.Path).FullName;

                    await Attach(true, process, 10000);
                } else if(_document.Settings.CurrentProfile.DebugMode == DebugMode.CreateHost) {
                    await CreateHostAndAttach(_document.Settings.CurrentProfile.HostProgram, _document.Settings.CurrentProfile.HostProgramArguments);
                } else {
                    await Attach(false, null, 1000);
                }
            }
        }

        /// <summary>
        /// Creates a debugger host owned by us, and attempts to attachs to it.
        /// </summary>
        /// <param name="program">The host's program.</param>
        /// <param name="arguments">The host's arguments.</param>
        public async Task CreateHostAndAttach(string program, string arguments)
        {
            var process = Application.CreateProcess(
                System.IO.Path.Combine(
                    Configuration.PathToExecutable,
                    "execProxy"
                ),
                "\"" + program + "\" " + arguments  // Need the program name as we pass it to execProxy
            );

            process.StartInfo.WorkingDirectory = System.IO.Directory.GetParent(_document.Path).FullName;

            await Attach(false, process, int.MaxValue);
        }

        /// <summary>
        /// Starts the specified process (only if not null), and attaches to a debug server.
        /// </summary>
        /// <param name="runInEditor">Whether to listen on the TCP port. <c>true</c> if the petri net will run in the editor.</param>
        /// <param name="process">The process to start, if not null.</param>
        /// <param name="timeout">A timeout for the attachment process, in milliseconds.</param>
        async Task Attach(bool runInEditor, Process process, int timeout)
        {
            if(CurrentSessionState == SessionState.Stopped) {
                CurrentSessionState = SessionState.Starting;
                _debuggable.OnStartAttachAttempt();

                string message = null;
                bool interrupted = false;
                if(process != null) {
                    AcquireProcess(process);
                }

                try {
                    _socket = await TryConnect(runInEditor, timeout);
                } catch(TimeoutException) {
                    message = Configuration.GetLocalized("Timeout");
                } catch(RunInEditorLoadLibraryDebuggerException) {
                    if(await _debuggable.ShouldAttemptDylibReload()) {
                        bool success = await _document.GenerateBuildAndDeploy();
                        if(success) {
                            Detach();
                            await Attach();
                            return;
                        }
                    }
                    interrupted = true;
                } catch(InvalidProgramStateDebuggerException) {
                    if(_process != null && _process.ExitCode == (int)ProxyError.ExecNotFound) {
                        message = Configuration.GetLocalized("The specified executable file does not exist.");
                    } else {
                        message = Configuration.GetLocalized("The host has exited");
                    }
                } catch(AttachInterruptedDebuggerException) {
                    interrupted = true;
                } catch(Exception e) {
                    message = e.Message;
                }

                _debuggable.OnAttachAttemptEnd(_socket != null);

                if(_socket == null) {
                    CurrentSessionState = SessionState.Stopped;

                    Detach();

                    if(!interrupted) {
                        _debuggable.NotifyUnrecoverableError(Configuration.GetLocalized("Unable to connect to the server: {0}",
                                                                                        message));
                    }

                    return;
                }

                _receiverThread = new Thread(ReceiveLoop);
                _receiverThread.Start();

                while(CurrentSessionState == SessionState.Starting) {
                    await Task.Delay(100);
                }
            } else {
                _debuggable.NotifyUnrecoverableError(Configuration.GetLocalized("The debugger is already attached."));
            }
        }

        /// <summary>
        /// Disconnects this instance from its DebugServer.
        /// </summary>
        public void Detach(bool isDetaching = false)
        {
            if(isDetaching) {
                return;
            }

            DetachWithOptionalStop(_process != null, true);

            KillProcess();
        }

        /// <summary>
        /// Add to a process start info, start it and retain a reference to it.
        /// </summary>
        /// <param name="process">Process.</param>
        void AcquireProcess(Process process)
        {
            if(_process != null) {
                throw new InvalidOperationException("A process is already owned by the debugger.");
            }
            _process = process;

            _process.EnableRaisingEvents = true;
            _process.StartInfo.RedirectStandardOutput = true;
            _process.StartInfo.RedirectStandardError = true;

            _process.OutputDataReceived += (object sender, DataReceivedEventArgs ev) => {
                if(ev.Data != null) {
                    _document.Output.WriteLine("{0}", ev.Data);
                }
            };
            _process.ErrorDataReceived += (object sender, DataReceivedEventArgs ev) => {
                if(ev.Data != null) {
                    _document.Output.WriteErrorLine("{0}", ev.Data);
                }
            };

            try {
                _process.Start();
            } catch(Exception e) {
                _debuggable.NotifyUnrecoverableError(Configuration.GetLocalized("Unable to launch the process: {0}",
                                                                                e.Message));
                Detach();
                return;
            }

            _process.BeginOutputReadLine();
            _process.BeginErrorReadLine();
        }

        /// <summary>
        /// Kills the debugger's external process, if any.
        /// </summary>
        void KillProcess()
        {
            try {
                if(_process != null) {
                    while(!_process.HasExited) {
                        _process.Kill();
                        Thread.Sleep(100);
                    }
                    _process = null;
                }
            } catch {
                // Process not started
            }
        }

        /// <summary>
        /// Loads the library and starts a local DebugServer.
        /// Valid only when the document is set to Run in the editor.
        /// </summary>
        public static void DebugSession(Document document)
        {
            var libProxy = GeneratedDynamicLibProxy.LibProxyForLanguage(document.Settings.Language,
                                                                        System.IO.Directory.GetParent(document.Path).FullName,
                                                                        document.Settings.RelativeLibBaseName + document.Settings.LibExtension,
                                                                        document.Settings.Name);
            try {
                var dylib = libProxy.Load<Runtime.IGeneratedDynamicLib>();

                if(dylib == null) {
                    throw new CLI.LoadLibFailureException();
                }

                var petriDynamicLib = dylib.Lib;
                var server = new Runtime.DebugServer(petriDynamicLib, true);
                server.Start();
                server.Join();
            } catch(Exception e) {
                Console.Error.WriteLine("{0}", Configuration.GetLocalized("An error occurred in the debugger session's host: {0}",
                                                                          e.Message));
                throw;
            }
        }

        /// <summary>
        /// Stops the petri net, detach the debugger and stops the DebugServer (it will not be listening any more after that).
        /// </summary>
        public void StopSession()
        {
            if(_process != null && _document.Settings.CurrentProfile.DebugMode != DebugMode.RunInEditor) {
                if(!_debuggable.ShouldOrphanProcess()) {
                    return;
                }
            }
            DetachWithOptionalStop(true);
        }

        /// <summary>
        /// Detaches from the debug server, and stops it only if <paramref name="stopDebugServer"/> is <c>true</c>.
        /// </summary>
        /// <param name="stopDebugServer">If set to <c>true</c> then the DebugServer is stopped as well as the DebugClient and petri net, whereas only the DebugClient and petri net are stopped if <c>false</c>.</param>
        /// <param name="isDetaching"><c>true</c> if the debigger is already detaching. This breaks infinite loops when an error occurs during the detach process.</param>
        void DetachWithOptionalStop(bool stopDebugServer, bool isDetaching = false)
        {
            if(CurrentSessionState == SessionState.Started) {
                if(CurrentPetriState != PetriState.Stopped && CurrentPetriState != PetriState.Stopped) {
                    StopPetri(isDetaching);
                    CurrentPetriState = PetriState.Stopped;
                }

                try {
                    SendObject(new JObject(new JProperty("type", "flush")));

                    if(stopDebugServer) {
                        SendObject(new JObject(new JProperty("type", "detachAndExit")));
                    } else {
                        SendObject(new JObject(new JProperty("type", "detach")));
                    }
                } catch {
                    // Don't care, probably mean we are already detached
                }
                _process = null;

                var signal = new AutoResetEvent(false);
                var task = Task.Run(() => {
                    // Give a chance to the receiver thread to terminate gracefully for 4s, then force the loop to end by
                    // closing the socket.
                    if(!signal.WaitOne(4000) && _socket != null) {
                        _socket.Close();
                    }
                });
                if(_receiverThread != null && !_receiverThread.Equals(Thread.CurrentThread)) {
                    _receiverThread.Join();
                }
                signal.Set();
            }

            lock(_debuggable.BaseDebugController.StatesInformation) {
                _debuggable.BaseDebugController.StatesInformation.Clear();
            }

            CurrentSessionState = SessionState.Stopped;
        }

        /// <summary>
        /// Tells the DebugServer to run the petri net.
        /// </summary>
        /// <param name="args">The dictionary of arguments to be supplied as the petri net's parameters.
        /// If <c>null</c>, then the last provided arguments will be used.</param>
        /// <param name="verbosity">An optional verbosity parameter that will override the petri net's default.</param>
        public void StartPetri(Dictionary<UInt32, Int64> args, Runtime.PetriNet.LogVerbosity? verbosity = null)
        {
            try {
                // The arguments are cached in case the petri nets needs to be reloaded
                // (when reloading the lib for example).
                if(args == null) {
                    args = _lastArguments;
                } else {
                    _lastArguments = args;
                }

                var jargs = new JObject();
                if(args != null) {
                    foreach(var kvp in args) {
                        jargs.Add(new JProperty(kvp.Key.ToString(), kvp.Value));
                    }
                }

                var jverbosity = new JArray();
                if(verbosity.HasValue) {
                    if(verbosity.Value.HasFlag(Runtime.PetriNet.LogVerbosity.States)) {
                        jverbosity.Add(new JValue("states"));
                    }
                    if(verbosity.Value.HasFlag(Runtime.PetriNet.LogVerbosity.Transitions)) {
                        jverbosity.Add(new JValue("transitions"));
                    }
                    if(verbosity.Value.HasFlag(Runtime.PetriNet.LogVerbosity.ReturnValues)) {
                        jverbosity.Add(new JValue("returnvalues"));
                    }

                    if(jverbosity.Count == 0) {
                        jverbosity.Add(new JValue("nothing"));
                    }
                }

                if(CurrentPetriState == PetriState.Stopped) {
                    CurrentPetriState = PetriState.Starting;
                    SendObject(new JObject(new JProperty("type", "start"),
                                           new JProperty("payload",
                                                         new JObject(new JProperty("hash", _document.Hash),
                                                                     new JProperty("args", jargs),
                                                                     new JProperty("loglevel", jverbosity)))));
                }
            } catch(Exception e) {
                _debuggable.NotifyUnrecoverableError(Configuration.GetLocalized("An error occurred in the debugger when starting the petri net: {0}",
                                                                                e.Message));
                Detach();
            }
        }

        /// <summary>
        /// Tells the DebugServer to stop the execution of the petri net.
        /// </summary>
        public void StopPetri(bool isDetaching = false)
        {
            try {
                if(CurrentPetriState != PetriState.Stopped && CurrentPetriState != PetriState.Stopping) {
                    SendObject(new JObject(new JProperty("type", "stop")));
                    CurrentPetriState = PetriState.Stopping;
                }
            } catch(Exception e) {
                _debuggable.NotifyUnrecoverableError(Configuration.GetLocalized("An error occurred in the debugger when stopping the petri net: {0}",
                                                                                e.Message));
                Detach(isDetaching);
            }
        }

        /// <summary>
        /// Stops the petri net execution, generate and compile the new petri net and load it into the DebugServer.
        /// </summary>
        public async Task ReloadPetri(bool startAfterReload = false)
        {
            _debuggable.NotifyStatusMessage(Configuration.GetLocalized("Reloading the petri net…"));
            StopPetri();

            var result = await _document.GenerateBuildAndDeploy();
            if(!result) {
                return;
            }

            try {
                if(_document.Settings.CurrentProfile.DebugMode == DebugMode.RunInEditor && _document.Settings.Language == Code.Language.CSharp) {
                    Detach();
                    await Attach();
                    if(startAfterReload) {
                        StartPetri(null);
                    }
                } else {
                    _startAfterFix = startAfterReload;
                    SendObject(new JObject(new JProperty("type", "reload")));
                }
            } catch(Exception e) {
                _debuggable.NotifyUnrecoverableError(Configuration.GetLocalized("An error occurred in the debugger when reloading the petri net: {0}",
                                                                                e.Message));
                Detach();
            }
        }

        /// <summary>
        /// Sends the current breakpoints list to the DebugServer.
        /// </summary>
        /// <param name="force">Whether to force the breakpoints update, even if the petri net does not appear to be running.</param>
        public void UpdateBreakpoints(bool force = false)
        {
            if(PetriAlive || force) {
                var breakpoints = new JArray();
                foreach(var bp in _debuggable.BaseDebugController.Breakpoints) {
                    breakpoints.Add(new JValue(bp.GlobalID));
                }
                if(_debuggable.BaseDebugController.TemporaryBreakpoint != null) {
                    var bp = _debuggable.BaseDebugController.TemporaryBreakpoint;
                    breakpoints.Add(new JValue(bp.GlobalID));
                }
                SendObject(new JObject(new JProperty("type", "breakpoints"),
                                       new JProperty("payload",
                                                     breakpoints)));
            }
        }

        /// <summary>
        /// Triggers an asynchronous evaluation of a code expression.
        /// </summary>
        /// <param name="petriNet">The petri net.</param>
        /// <param name="expression">The expression to evaluate.</param>
        /// <param name="userData">Additional and optional user data that will be used to generate the code.</param>
        public void Evaluate(PetriNet petriNet, Code.Expression expression, params object[] userData)
        {
            if(CurrentSessionState != SessionState.Started) {
                throw new InvalidOperationException("The debugger must be attached to evaluate an expression!");
            }

            if(!PetriAlive) {
                var literals = expression.GetLiterals();
                foreach(var l in literals) {
                    if(l is Code.VariableExpression) {
                        throw new NotSupportedException(Configuration.GetLocalized("A variable of the petri net cannot be evaluated when the petri net is not running."));
                    }
                }
            }

            string libName;

            var doc = petriNet is ExternalInnerPetriNet
                                ? ((ExternalInnerPetriNet)petriNet).ExternalDocument
                                : petriNet.Document;

            // A message that may be passed to the debug server, to be returned to us asynchronously to be displayed.
            string userInfo = "";

            if(doc.Settings.Language == Code.Language.Python) {
                // The "path" forwarded to the debug server is in fact the expression to evaluate.
                libName = expression.MakeCode();
            } else {
                string sourceName = null, intermediateName = null;
                var oldOutputType = doc.Settings.OutputType;
                try {
                    sourceName = System.IO.Path.GetTempFileName();

                    var petriGen = CodeGen.PetriGen.PetriGenForDoc(doc);
                    petriGen.WriteExpressionEvaluator(expression, sourceName, userData);

                    intermediateName = System.IO.Path.GetTempFileName();
                    libName = doc.Settings.Language == Code.Language.CSharp
                                        ? intermediateName
                                        : System.IO.Path.GetTempFileName();

                    // Force the output to be a dynamic lib (it would be a static object otherwise for child petri nets).
                    doc.Settings.OutputType = OutputType.DynamicLib;

                    var o = doc.Compiler.CompileSource(
                        new List<Tuple<string, Code.Language>> {
                            new Tuple<string, Code.Language>(sourceName, doc.Settings.Language)
                        },
                        intermediateName,
                        libName
                    );
                    userInfo = o.Item2;
                    if(!o.Item1) {
                        throw new Exception(Configuration.GetLocalized("Compilation errors: {0}", o.Item2));
                    }
                } finally {
                    doc.Settings.OutputType = oldOutputType;

                    try {
                        System.IO.File.Delete(sourceName);
                    } catch {
                        // Don't care…
                    }
                    if(doc.Settings.Language != Code.Language.CSharp) {
                        try {
                            System.IO.File.Delete(intermediateName);
                        } catch {
                            // Don't care…
                        }
                    }
                }
            }

            try {
                SendObject(new JObject(new JProperty("type", "evaluate"),
                                       new JProperty("payload",
                                                     new JObject(new JProperty("lib", libName),
                                                                 new JProperty("id", petriNet.ID),
                                                                 new JProperty("userInfo", userInfo)))));
            } catch(Exception e) {
                Detach();
                throw e;
            }
        }

        /// <summary>
        /// Requests that the debug server changes the specified variable to the fiven value
        /// </summary>
        /// <param name="petriNet">The petri net.</param>
        /// <param name="variable">Variable.</param>
        /// <param name="value">Value.</param>
        public void SetVariable(PetriNet petriNet, Code.VariableExpression variable, Int64 value)
        {
            SendObject(new JObject(new JProperty("type", "setVar"),
                                   new JProperty("payload",
                                                 new JObject(new JProperty("id", petriNet.GlobalID),
                                                             new JProperty("name", variable.MakeUserReadable()),
                                                             new JProperty("value", value)))));
        }

        /// <summary>
        /// Requests a variable update from the debug server.
        /// </summary>
        public void RequestVariables()
        {
            if(!PetriAlive) {
                var variables = new List<Tuple<RootPetriNet, Code.VariableExpression, string>>();

                var vars = _debuggable.BaseDebugController.PetriNet.Variables;
                foreach(var v in vars) {
                    var value = "";
                    if(_debuggable.BaseDebugController.PetriNet.Parameters.Contains(v.Key)) {
                        value = vars[v.Key].ToString();
                    }
                    variables.Add(Tuple.Create(_debuggable.BaseDebugController.PetriNet, v.Key, value));
                }

                _debuggable.NotifyVariables(variables);
            } else {
                try {
                    SendObject(new JObject(new JProperty("type", "vars")));
                } catch(Exception e) {
                    Detach();
                    throw e;
                }
            }
        }

        /// <summary>
        /// Try to connect to a DebugServer.
        /// </summary>
        void Hello()
        {
            try {
                SendObject(new JObject(new JProperty("type", "hello"),
                                       new JProperty("payload",
                                                     new JObject(new JProperty("version",
                                                                               DebugClient.DebuggerAPIVersion)))));

                var ehlo = ReceiveObject();
                if(ehlo != null && ehlo["type"].ToString() == "ehlo") {
                    CurrentSessionState = SessionState.Started;
                    _debuggable.NotifyStatusMessage(Configuration.GetLocalized("Sucessfully connected."));
                    return;
                } else if(ehlo != null && ehlo["type"].ToString() == "error") {
                    throw new Exception(Configuration.GetLocalized("An error was returned by the debugger: {0}",
                                                                   ehlo["payload"]));
                }
                throw new Exception(Configuration.GetLocalized("Invalid message received from debugger (expected ehlo): {0}",
                                                               ehlo));
            } catch(Exception e) {
                _debuggable.NotifyUnrecoverableError(Configuration.GetLocalized("An error occurred in the debugger during the handshake: {0}",
                                                                                e.Message));
                Detach();
            }
        }

        /// <summary>
        /// The method in charge of receiving and dispatching the messages from the DebugServer.
        /// </summary>
        void ReceiveLoop()
        {
            try {
                Hello();

                while(CurrentSessionState != SessionState.Stopped && _socket.Connected) {
                    JObject msg = ReceiveObject();
                    if(msg == null)
                        break;

                    if(msg["type"].ToString() == "ack") {
                        if(msg["payload"].ToString() == "start") {
                            CurrentPetriState = PetriState.Started;
                            UpdateBreakpoints(true);
                            _debuggable.NotifyStatusMessage(Configuration.GetLocalized("The petri net is running…"));
                        } else if(msg["payload"].ToString() == "stopped") {
                            StopPetri();
                        } else if(msg["payload"].ToString() == "stop") {
                            CurrentPetriState = PetriState.Stopped;
                            lock(_debuggable.BaseDebugController.StatesInformation) {
                                _debuggable.BaseDebugController.StatesInformation.Clear();
                            }
                            _debuggable.NotifyActiveStatesChanged();
                            _debuggable.NotifyStatusMessage(Configuration.GetLocalized("The petri net execution has ended."));
                        } else if(msg["payload"].ToString() == "reload") {
                            _debuggable.NotifyStatusMessage(Configuration.GetLocalized("The Petri net has been successfully reloaded."));
                            if(_startAfterFix) {
                                StartPetri(null);
                            }
                        } else if(msg["payload"].ToString() == "pause") {
                            if(CurrentPetriState != PetriState.Paused) {
                                CurrentPetriState = PetriState.Paused;
                                _debuggable.NotifyStatusMessage(Configuration.GetLocalized("Paused."));
                            }
                        } else if(msg["payload"].ToString() == "resume") {
                            CurrentPetriState = PetriState.Started;
                            _debuggable.NotifyStatusMessage(Configuration.GetLocalized("The petri net is running…"));
                        }
                    } else if(msg["type"].ToString() == "error") {
                        _startAfterFix = false;
                        _debuggable.NotifyServerError(msg["payload"].ToString());

                        if(PetriAlive || CurrentPetriState == PetriState.Starting) {
                            StopPetri();
                        }
                    } else if(msg["type"].ToString() == "detach" || msg["type"].ToString() == "detachAndExit") {
                        if(msg["payload"].ToString() == "kbye") {
                            _debuggable.NotifyStatusMessage(Configuration.GetLocalized("Disconnected."));
                            CurrentSessionState = SessionState.Stopped;
                            CurrentPetriState = PetriState.Stopped;
                        } else {
                            CurrentSessionState = SessionState.Stopped;
                            CurrentPetriState = PetriState.Stopped;

                            throw new Exception(Configuration.GetLocalized("Remote debugger requested a session termination for reason: {0}",
                                                                           msg["payload"].ToString()));
                        }
                    } else if(msg["type"].ToString() == "states") {
                        var states = msg["payload"].Select(t => t).ToList();

                        lock(_debuggable.BaseDebugController.StatesInformation) {
                            _debuggable.BaseDebugController.StatesInformation.Clear();
                            foreach(var s in states) {
                                var id = UInt64.Parse(s["id"].ToString());
                                var e = _debuggable.BaseDebugController.PetriNet.EntityFromID(id);
                                if(e == null || !(e is State)) {
                                    throw new Exception(Configuration.GetLocalized("Entity sent from runtime doesn't exist on our side (id: {0})!",
                                                                                   id));
                                }
                                var info = new DebugController.StateInformation(
                                    int.Parse(s["count"].ToString()),
                                    int.Parse(s["tokens"].ToString())
                                );
                                _debuggable.BaseDebugController.StatesInformation[e as State] = info;
                            }

                            _debuggable.BaseDebugController.OnActiveStatesChanged(null);
                        }

                        _debuggable.NotifyActiveStatesChanged();
                    } else if(msg["type"].ToString() == "vars") {
                        var result = new List<Tuple<RootPetriNet, Code.VariableExpression, string>>();
                        foreach(JProperty slots in msg["payload"]) {
                            var id = UInt64.Parse(slots.Name);
                            RootPetriNet petriNet;
                            var e = _debuggable.BaseDebugController.PetriNet.EntityFromID(id);
                            if(e is ExternalInnerPetriNet) {
                                petriNet = ((ExternalInnerPetriNet)e).ExternalDocument.PetriNet;
                            } else {
                                petriNet = e as RootPetriNet;
                            }
                            if(petriNet == null) {
                                throw new Exception(Configuration.GetLocalized("Entity sent from runtime doesn't exist on our side (id: {0})!",
                                                                               id));
                            }

                            foreach(var slot in slots.Value) {
                                foreach(var v in slot) {
                                    result.Add(Tuple.Create(petriNet, new Code.VariableExpression(v["name"].ToString().Substring(1), _document.Settings.Language), v["value"].ToString()));
                                }
                            }
                        }

                        _debuggable.NotifyVariables(result);
                    } else if(msg["type"].ToString() == "evaluation") {
                        var lib = msg["payload"]["lib"].ToString();
                        if(lib != "" && _document.Settings.Language != Code.Language.Python) {
                            System.IO.File.Delete(lib);
                        }
                        _debuggable.NotifyEvaluated(msg["payload"]["eval"].ToString(), msg["payload"]["userInfo"].ToString());
                    } else {
                        _debuggable.NotifyUnrecoverableError(Configuration.GetLocalized("Unknown debugger message type: {0}.",
                                                                                        msg["type"]));
                        Detach();
                    }
                }
                if(CurrentSessionState != SessionState.Stopped) {
                    throw new Exception(Configuration.GetLocalized("Socket unexpectedly disconnected!"));
                }
            } catch(Exception e) {
                _debuggable.NotifyUnrecoverableError(Configuration.GetLocalized("An error occurred in the debugger client: {0}",
                                                                                e.Message));
                Detach();
            }

            try {
                _socket.Close();
            } catch {
                // Means the socket is closed anyway.
            }

            _socket = null;
        }

        /// <summary>
        /// Tries to receive a JSON object from the DebugServer.
        /// </summary>
        /// <returns>The object.</returns>
        JObject ReceiveObject()
        {
            int count = 0;
            while(CurrentSessionState != SessionState.Stopped) {
                string val = ReceiveString();

                if(val.Length > 0)
                    return JObject.Parse(val);

                if(++count > 5) {
                    throw new Exception(Configuration.GetLocalized("Remote debugger isn't available anymore!"));
                }
                Thread.Sleep(1);
            }

            return null;
        }

        /// <summary>
        /// Sends a JSON message to the DebugServer.
        /// </summary>
        /// <param name="o">O.</param>
        void SendObject(JObject o)
        {
            SendString(o.ToString());
        }

        /// <summary>
        /// Low level message reception routine.
        /// </summary>
        /// <returns>The string.</returns>
        string ReceiveString()
        {
            byte[] countBytes = new byte[4];
            byte[] msg;

            lock(_downLock) {
                int len = _socket.GetStream().Read(countBytes, 0, 4);
                if(len != 4) {
                    return "";
                }

                UInt32 count = (UInt32)countBytes[0] | ((UInt32)countBytes[1] << 8) | ((UInt32)countBytes[2] << 16) | ((UInt32)countBytes[3] << 24);
                UInt32 read = 0;

                msg = new byte[count];
                while(read < count) {
                    read += (UInt32)_socket.GetStream().Read(msg, (int)read, (int)(count - read));
                }
            }

            return System.Text.Encoding.UTF8.GetString(msg);
        }

        /// <summary>
        /// Low level message sending routine.
        /// </summary>
        /// <returns>The string.</returns>
        void SendString(string s)
        {
            var msg = System.Text.Encoding.UTF8.GetBytes(s);
            var count = (UInt32)msg.Length;

            byte[] countBytes = {
                (byte)((count >> 0) & 0xFF),
                (byte)((count >> 8) & 0xFF),
                (byte)((count >> 16) & 0xFF),
                (byte)((count >> 24) & 0xFF),
            };

            lock(_upLock) {
                _socket.GetStream().Write(countBytes, 0, 4);
                _socket.GetStream().Write(msg, 0, msg.Length);
            }
        }

        Process _process;

        bool _startAfterFix = false;
        volatile PetriState _petriState;
        volatile SessionState _sessionState;
        Thread _receiverThread;

        volatile TcpClient _socket;
        object _upLock = new object();
        object _downLock = new object();

        /// <summary>
        /// The objects that contains the DebugController.
        /// </summary>
        protected IDebuggable _debuggable;

        /// <summary>
        /// The document.
        /// </summary>
        protected LocalDocument _document;

        Dictionary<UInt32, Int64> _lastArguments;
    }
}
