//
//  DebugController.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2014-11-24.
//

using System;
using System.Collections.Generic;

using Petri.Model;
using Action = Petri.Model.Action;

namespace Petri.Application.Debugger
{
    /// <summary>
    /// The debug controller.
    /// </summary>
    public class DebugController : Controller
    {
        /// <summary>
        /// State information.
        /// </summary>
        public struct StateInformation
        {
            /// <summary>
            /// Initializes a new instance of the
            /// <see cref="T:Petri.Application.Debugger.DebugController.StateInformation"/> struct.
            /// </summary>
            /// <param name="inv">Invocations count.</param>
            /// <param name="tok">Tokens count.</param>
            public StateInformation(int inv, int tok)
            {
                InvocationsCount = inv;
                TokensCount = tok;
            }

            /// <summary>
            /// Gets or sets the number of times a state is active at a given time.
            /// </summary>
            /// <value>The invocations count.</value>
            public int InvocationsCount {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the tokens count of a state at a given time.
            /// </summary>
            /// <value>The tokens count.</value>
            public int TokensCount {
                get;
                set;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.Debugger.DebugController"/> class.
        /// </summary>
        /// <param name="petriNetProvider">The petri net provider.</param>
        /// <param name="client">Client.</param>
        public DebugController(PetriNetProvider petriNetProvider, DebugClient client) : base(petriNetProvider)
        {
            Client = client;
            BreakpointsUpdated += (e) => {
                Client.UpdateBreakpoints();
            };

            StatesInformation = new Dictionary<State, StateInformation>();
            _breakpoints = new HashSet<Action>();
            LastEvaluations = new List<string>();
        }

        /// <summary>
        /// Gets the debug client.
        /// </summary>
        /// <value>The client.</value>
        public DebugClient Client {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets the last evaluated expressions of the debug editor.
        /// </summary>
        /// <value>The last evaluation.</value>
        public List<string> LastEvaluations {
            get;
        }

        /// <summary>
        /// Gets the active states.
        /// </summary>
        /// <value>The currently active states of a running petri net.</value>
        public Dictionary<State, StateInformation> StatesInformation {
            get;
            private set;
        }

        /// <summary>
        /// The delegate mapped to the <see cref="ActiveStatesChanged"/> event.
        /// </summary>
        public delegate void ActiveStatesChangedDel(object sender, EventArgs e);

        /// <summary>
        /// Occurs when there is an update in the set of current active states.
        /// </summary>
        public event ActiveStatesChangedDel ActiveStatesChanged;

        /// <summary>
        /// Triggers the <see cref="ActiveStatesChanged"/> event.
        /// </summary>
        /// <param name="e">E.</param>
        public void OnActiveStatesChanged(EventArgs e)
        {
            ActiveStatesChanged?.Invoke(null, e);
        }

        /// <summary>
        /// The list of breakpoints installed in the currently running petri net.
        /// </summary>
        /// <value>The breakpoints.</value>
        public ReadOnlyCollection<Action> Breakpoints {
            get {
                return new ReadOnlyCollection<Action>(_breakpoints);
            }
        }

        /// <summary>
        /// Attach a breakpoint to the specified petri net state.
        /// </summary>
        /// <returns><c>true</c>, if breakpoint was added, <c>false</c> otherwise.</returns>
        /// <param name="action">The state.</param>
        public bool AddBreakpoint(Action action)
        {
            var ok = _breakpoints.Add(action);
            if(ok) {
                OnBreakpointsUpdated(new UpdatedBreakpointsEventArgs());
            }

            return ok;
        }

        /// <summary>
        /// Remove a breakpoint from the specified petri net state.
        /// </summary>
        /// <returns><c>true</c>, if breakpoint was removed, <c>false</c> otherwise.</returns>
        /// <param name="action">The state.</param>
        public bool RemoveBreakpoint(Action action)
        {
            var ok = _breakpoints.Remove(action);
            if(ok) {
                OnBreakpointsUpdated(new UpdatedBreakpointsEventArgs());
            }

            return ok;
        }

        /// <summary>
        /// Removes all the existing breakpoints.
        /// </summary>
        public void ClearBreakpoints()
        {
            _breakpoints.Clear();
            OnBreakpointsUpdated(new UpdatedBreakpointsEventArgs());
        }

        /// <summary>
        /// Replaces all the existing breakpoints.
        /// </summary>
        /// <param name="breakpoints">The list of new permanent breakpoints.</param>
        /// <param name="temporary">The new temporary breakpoint.</param>
        public void ReplaceBreakpoints(IEnumerable<Action> breakpoints, Action temporary)
        {
            _breakpoints.Clear();
            foreach(var bp in breakpoints) {
                _breakpoints.Add(bp);
            }
            _tempBreakpoint = temporary;

            OnBreakpointsUpdated(new UpdatedBreakpointsEventArgs());
        }

        /// <summary>
        /// Gets or sets the temporary breakpoint.
        /// </summary>
        public Action TemporaryBreakpoint {
            get {
                return _tempBreakpoint;
            }
            set {
                _tempBreakpoint = value;
                OnBreakpointsUpdated(new UpdatedBreakpointsEventArgs());
            }
        }

        /// <summary>
        /// Arguments for the <see cref="BreakpointsUpdated"/> event.
        /// </summary>
        [Serializable]
        public class UpdatedBreakpointsEventArgs : EventArgs
        {

        }

        /// <summary>
        /// The delegate mapped to the <see cref="BreakpointsUpdated"/> event.
        /// </summary>
        public delegate void BreakpointsUpdatedDel(UpdatedBreakpointsEventArgs e);

        /// <summary>
        /// Occurs when there is an update in the set of current breakpoints.
        /// </summary>
        public event BreakpointsUpdatedDel BreakpointsUpdated;

        /// <summary>
        /// Triggers the <see cref="BreakpointsUpdated"/> event.
        /// </summary>
        /// <param name="e">E.</param>
        void OnBreakpointsUpdated(UpdatedBreakpointsEventArgs e)
        {
            BreakpointsUpdated?.Invoke(e);
        }

        Action _tempBreakpoint;

        HashSet<Action> _breakpoints;
    }
}
