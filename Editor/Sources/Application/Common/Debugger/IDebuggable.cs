//
//  IDebuggable.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-02-25.
//

using System;
using System.Collections.Generic;

namespace Petri.Application.Debugger
{
    /// <summary>
    /// Debuggable.
    /// </summary>
    public interface IDebuggable
    {
        /// <summary>
        /// Gets the base debug controller.
        /// </summary>
        /// <value>The base debug controller.</value>
        DebugController BaseDebugController {
            get;
        }

        /// <summary>
        /// Called when the state of the petri net or the session is changed.
        /// </summary>
        void NotifyStateChanged();

        /// <summary>
        /// Asks the user if regenerating the dynamic library should be attempted.
        /// </summary>
        /// <returns><c>true</c>, if we want to reload the lib, <c>false</c> otherwise.</returns>
        System.Threading.Tasks.Task<bool> ShouldAttemptDylibReload();

        /// <summary>
        /// Notifies a new status message.
        /// </summary>
        /// <param name="message">Message.</param>
        void NotifyStatusMessage(string message);

        /// <summary>
        /// Notifies that a code expression was successfully evaluated.
        /// </summary>
        /// <param name="value">The evaluation result.</param>
        /// <param name="userInfo">Additional user info.</param>
        void NotifyEvaluated(string value, string userInfo);

        /// <summary>
        /// Notifies that the value of the petri net's variables have just been retrieved.
        /// </summary>
        /// <param name="variables">The list of key-value pairs.</param>
        void NotifyVariables(List<Tuple<Model.RootPetriNet, Code.VariableExpression, string>> variables);

        /// <summary>
        /// Notifies an error in the debug server.
        /// </summary>
        /// <param name="message">Message.</param>
        void NotifyServerError(string message);

        /// <summary>
        /// Notifies a new state have been activated in the petri net, or a state have been deactivated.
        /// </summary>
        void NotifyActiveStatesChanged();

        /// <summary>
        /// Notifies the user that the current action will cause the debug server's process to be orphaned, and aks him whether to continue or not.
        /// </summary>
        /// <returns><c>true</c>, if the process should be orphaned, <c>false</c> if the current action should be cancelled.</returns>
        bool ShouldOrphanProcess();

        /// <summary>
        /// Notifies the user from an unrecoverable error.
        /// </summary>
        /// <param name="message">The error message.</param>
        void NotifyUnrecoverableError(string message);

        /// <summary>
        /// Invoked when an attempt will be made to attach to a debug host.
        /// </summary>
        void OnStartAttachAttempt();

        /// <summary>
        /// Whether to interrupt a debugger attaching procedure
        /// </summary>
        /// <returns><c>true</c>, if attach was interrupted, <c>false</c> otherwise.</returns>
        bool InterruptAttach();

        /// <summary>
        /// Invoked when a previously start attempt to attach to a debug host comes to and end.
        /// </summary>
        /// <param name="success">If set to <c>true</c> then the attempt was successful.</param>
        void OnAttachAttemptEnd(bool success);
    }
}

