//
//  GeneratedDynamicLibProxy.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-24.
//

using System;

namespace Petri.Application.Debugger
{
    /// <summary>
    /// Factory class to get a dynamic library proxy from a language.
    /// </summary>
    public abstract class GeneratedDynamicLibProxy
    {
        /// <summary>
        /// Gets a new dynamica library proxy for given language.
        /// </summary>
        /// <returns>The proxy for the given language.</returns>
        /// <param name="language">Language.</param>
        /// <param name="libPath">Lib path.</param>
        /// <param name="libName">Lib name.</param>
        /// <param name="className">The class to load from the lib.</param>
        public static Runtime.GeneratedDynamicLibProxy LibProxyForLanguage(Code.Language language,
                                                                           string libPath,
                                                                           string libName,
                                                                           string className)
        {
            if(language == Code.Language.CSharp) {
                return new Runtime.CSharpGeneratedDynamicLibProxy(libPath, libName, className);
            } else if(language == Code.Language.C || language == Code.Language.Cpp) {
                return new CGeneratedDynamicLibProxy(language, libPath, libName, className);
            } else if(language == Code.Language.Python) {
                return new CGeneratedDynamicLibProxy(language, libPath, libName, className);
            }

            throw new Exception("Debugger.GeneratedDynamicLibProxy.LibProxyForLanguage: Should not get there!");
        }
    }

    /// <summary>
    /// A proxy for a library that can be dlopen()ed.
    /// </summary>
    public class CGeneratedDynamicLibProxy : Runtime.GeneratedDynamicLibProxy
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.Debugger.CGeneratedDynamicLibProxy"/> class.
        /// </summary>
        /// <param name="language">Language.</param>
        /// <param name="libPath">Lib path.</param>
        /// <param name="libName">Lib name.</param>
        /// <param name="className">Class name.</param>
        public CGeneratedDynamicLibProxy(Code.Language language,
                                         string libPath,
                                         string libName,
                                         string className) : base(libPath, libName, className)
        {
        }

        /// <summary>
        /// Loads the assembly and extracts the <see cref="Petri.Runtime.IGeneratedDynamicLib"/> instance it encloses.
        /// </summary>
        /// <typeparam name="LibType">The type of the proxy to create. Must be a subclass of <see cref="Petri.Runtime.CGeneratedDynamicLib"/>.</typeparam>
        public override LibType Load<LibType>()
        {
            var filePath = System.IO.Path.Combine(LibPath, LibName);

            LibType dylib = null;

            if(typeof(LibType).IsAssignableFrom(typeof(Runtime.CGeneratedDynamicLib))) {
                IntPtr handle = IntPtr.Zero;
                try {
                    handle = Runtime.Utility.LoadPetriDynamicLib(filePath,
                                                                 ClassName, Configuration.RunningPlatform == Platform.Mac);
                } catch(Exception e) {
                    Console.Error.WriteLine("Exception when loading assembly {0}: {1}.",
                                            filePath,
                                            e);
                }
                if(handle != IntPtr.Zero) {
                    dylib = (LibType)(object)new Runtime.CGeneratedDynamicLib(handle);
                }
            }

            return dylib;
        }

        /// <summary>
        /// Unloads the library.
        /// </summary>
        public override void Unload()
        {
        }
    }
}

