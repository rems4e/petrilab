//
//  LocalDocument.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-01.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

using Petri.Model;

namespace Petri.Application
{
    /// <summary>
    /// A document opened by the user.
    /// </summary>
    public abstract class LocalDocument : Document
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.LocalDocument"/> class.
        /// </summary>
        /// <param name="language">The language the petri net will use.</param>
        internal LocalDocument(Code.Language language) : base(null, language)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.LocalDocument"/> class.
        /// </summary>
        /// <param name="path">Path to the document.</param>
        internal LocalDocument(string path) : base(null, path)
        {
            IssuesChecker.CheckIssues(PetriNet);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.Document"/> class.
        /// </summary>
        /// <param name="path">The path to the document.</param>
        /// <param name="document">The XML descriptor of the document.</param>
        internal LocalDocument(string path, XDocument document) : base(null, path, document)
        {
        }

        /// <summary>
        /// Validates the petri net. Throws an exception if it does not pass validation.
        /// </summary>
        void ValidatePetriNet()
        {
            IssuesChecker.CheckIssues(PetriNet);

            if(_issues.Any((kvp) => {
                return kvp.Value.Any((issue) => issue.IsError && issue.IssueKind != EntityIssue.Kind.CompilationError);
            })) {
                var message = Configuration.GetLocalized("The petri net does not pass validation. Please check issues first.");
                NotifyError(message);
                throw new GenerationException(message);
            }
        }

        /// <summary>
        /// Generates the source code. If no code have ever been generated for this document, meaning we don't know where to save it, then an Exception is thrown.
        /// </summary>
        /// <returns><c>true</c>, if code dont ask was generated, <c>false</c> otherwise.</returns>
        public void GenerateCode()
        {
            ValidatePetriNet();
            GenerateCode(new HashSet<string>());
        }


        /// <summary>
        /// Compiles the document. If some output is made by the invoked compiler, then the method returns false, true otherwise.
        /// </summary>
        /// <param name="verbose">Whether we want verbose compilation output.</param>
        /// <returns>The compilation success status.</returns>
        public virtual async Task<Tuple<Compiler.CompilationStatus, string>> Compile(bool verbose = false)
        {
            RemoveIssue(EntityIssue.Kind.CompilationError);
            RemoveIssue(EntityIssue.Kind.CompilationWarning);

            var result = await Compile(new HashSet<string>(), verbose);

            EchoCompilationStatus(result.Item1, result.Item2);

            return result;
        }

        /// <summary>
        /// Deploy the compilation product of the document.
        /// </summary>
        /// <param name="verbose">Verbose output</param>
        /// <param name="outputPrefix">Prefix to insert before deployment script's output.</param>
        public virtual async Task<bool> Deploy(bool verbose = false, string outputPrefix = "")
        {
            bool result = true;
            string error = null;

            switch(Settings.CurrentProfile.DeployMode) {
            case DeployMode.DoNothing:
                break;
            case DeployMode.Copy:
                try {
                    System.IO.File.Copy(
                        Settings.StandardLibPath,
                        System.IO.Path.Combine(
                            Settings.CurrentProfile.DeployPath,
                            Settings.Name + Settings.LibExtension
                        ),
                        true
                    );
                } catch(Exception e) {
                    result = false;
                    error = e.Message;
                    break;
                }

                break;
            case DeployMode.Script:
                var path = System.IO.Path.GetTempFileName();
                System.IO.File.WriteAllText(path, Settings.CurrentProfile.DeployScript);

                if(Configuration.RunningPlatform != Platform.Windows) {
                    var chmod = Application.CreateProcess("/bin/chmod", "u=rwx,go= -- " + path);

                    chmod.Start();
                    chmod.WaitForExit();
                    if(chmod.ExitCode != 0) {
                        result = false;
                        error = Configuration.GetLocalized("Could not chmod the deployment script!");
                        break;
                    }
                } else {
                    throw new NotImplementedException();
                }

                var script = Application.CreateProcess(path);
                script.StartInfo.WorkingDirectory = System.IO.Directory.GetParent(Path).FullName;
                script.StartInfo.EnvironmentVariables[DEPLOY_ENVVAR_NAME] = Settings.Name;
                script.StartInfo.EnvironmentVariables[DEPLOY_ENVVAR_PROFILE] = Settings.CurrentProfile.Name;
                script.StartInfo.EnvironmentVariables[DEPLOY_ENVVAR_LIB_PATH] = Settings.StandardLibPath;
                script.StartInfo.RedirectStandardOutput = true;
                script.StartInfo.RedirectStandardError = true;

                script.OutputDataReceived += (sender, ev) => {
                    if(verbose && ev.Data != null) {
                        Output.WriteLine(outputPrefix + ev.Data);
                    }
                };
                script.ErrorDataReceived += (sender, ev) => {
                    if(verbose && ev.Data != null) {
                        Output.WriteErrorLine(outputPrefix + ev.Data);
                    }
                };

                script.Start();
                script.BeginOutputReadLine();
                script.BeginErrorReadLine();
                script.WaitForExit();

                System.IO.File.Delete(path);

                result = script.ExitCode == 0;
                break;
            }

            if(!result) {
                if(error != null) {
                    NotifyError(Configuration.GetLocalized("Unable to deploy product: {0}!", error));
                } else {
                    NotifyError(Configuration.GetLocalized("Unable to deploy product!"));
                }
                return await Task.FromResult(false);
            }

            return await Task.FromResult(true);
        }

        /// <summary>
        /// Generates the source code, compile and deploy the lib.
        /// </summary>
        /// <returns>The build and deploy.</returns>
        public async Task<bool> GenerateBuildAndDeploy()
        {
            try {
                GenerateCode();
                if((await Compile()).Item1 == Compiler.CompilationStatus.Error) {
                    return false;
                } else {
                    if(Settings.CurrentProfile.DeployWhenDebug && !(await Deploy())) {
                        return false;
                    }
                }
            } catch {
                return false;
            }

            return true;
        }


        /// <summary>
        /// Shows the compilation status to the user.
        /// </summary>
        /// <param name="status">The compilation success status.</param>
        /// <param name="output">Output.</param>
        public abstract void EchoCompilationStatus(Compiler.CompilationStatus status, string output);
        /// <summary>
        /// Updates the external documents.
        /// </summary>
        /// <returns><c>true</c>, if some of the external documents were updated, <c>false</c> otherwise.</returns>

        public override bool UpdateExternalDocuments()
        {
            var result = base.UpdateExternalDocuments();
            if(result) {
                ReassignIDs();
                IssuesChecker.CheckIssues(PetriNet);
            }

            return result;
        }

        /// <summary>
        /// Clears the entities issues.
        /// </summary>
        public void ClearIssues()
        {
            _issues.Clear();
        }

        /// <summary>
        /// Adds an entity to the list of conflicting entities.
        /// </summary>
        /// <param name="entity">Entity.</param>
        /// <param name="message">Message.</param>
        /// <param name="issue">The kind of issue.</param>
        public void AddIssue(Entity entity, string message, EntityIssue.Kind issue)
        {
            SortedSet<EntityIssue> value;
            if(!_issues.TryGetValue(entity, out value)) {
                value = new SortedSet<EntityIssue>();
                _issues[entity] = value;
            }

            var oldIssue = value.FirstOrDefault((ei) => ei.IssueKind == issue);
            if(oldIssue == null) {
                oldIssue = new EntityIssue(message, issue);
            } else {
                value.RemoveWhere((ei) => ei.IssueKind == issue);
                oldIssue = new EntityIssue(oldIssue.Message + "\n\n" + message, issue);
            }

            value.Add(oldIssue);
        }

        /// <summary>
        /// Removes the issues of a particular kind on an entity.
        /// </summary>
        /// <param name="entity">Entity.</param>
        /// <param name="kind">Kind.</param>
        /// <param name="clean">If set to <c>true</c>, removes entities with 0 issues.
        /// May be useful to set it to <c>false</c> if we remove issues in a loop.</param>
        public void RemoveIssue(Entity entity, EntityIssue.Kind kind, bool clean = true)
        {
            SortedSet<EntityIssue> set = null;
            if(_issues.TryGetValue(entity, out set)) {
                set.RemoveWhere((issue) => {
                    return issue.IssueKind == kind;
                });

                if(clean && set.Count == 0) {
                    _issues.Remove(entity);
                }
            }
        }

        /// <summary>
        /// Removes all issues on an entity.
        /// </summary>
        /// <param name="entity">Entity.</param>
        public void RemoveIssue(Entity entity)
        {
            if(_issues.ContainsKey(entity)) {
                _issues.Remove(entity);
            }
        }

        /// <summary>
        /// Removes all issues of a particular kind on all entities.
        /// </summary>
        /// <param name="kind">Kind.</param>
        public void RemoveIssue(EntityIssue.Kind kind)
        {
            foreach(var kvp in _issues) {
                RemoveIssue(kvp.Key, kind, false);
            }

            foreach(var kvp in _issues.Where((arg) => arg.Value.Count == 0).ToList()) {
                _issues.Remove(kvp.Key);
            }
        }

        /// <summary>
        /// Returns the issues of an entity.
        /// </summary>
        /// <param name="e">The entity to check.</param>
        public SortedSet<EntityIssue> GetIssues(Entity e)
        {
            SortedSet<EntityIssue> issues = null;
            if(_issues.ContainsKey(e)) {
                issues = _issues[e];
            }
            if(e is PetriNet) {
                foreach(var ee in _issues) {
                    bool isNotInnerIssue = ee.Key is RootPetriNet &&
                             ee.Value.All(issue => issue.IssueKind == EntityIssue.Kind.UnusedVariable);
                    if(!isNotInnerIssue && ((PetriNet)e).EntityFromID(ee.Key.GlobalID) != null) {
                        var newIssues = new SortedSet<EntityIssue> {
                            new EntityIssue(Configuration.GetLocalized("Some entities contained in this petri net are causing errors."),
                                            EntityIssue.Kind.ChildIssue)
                        };

                        if(issues != null) {
                            newIssues.UnionWith(issues);
                        }
                        issues = newIssues;
                    }
                }
            }

            return issues ?? new SortedSet<EntityIssue>();
        }

        /// <summary>
        /// Gets the count of issues currently reported by the document.
        /// </summary>
        /// <value>The issues count.</value>
        public int IssuesCount {
            get {
                return _issues.Count;
            }
        }

        /// <summary>
        /// A string writer that does not pollute our files with UTF-8 BOM.
        /// </summary>
        class BOMLessUTF8StringWriter : System.IO.StringWriter
        {
            /// <summary>
            /// Gets the encoding, i.e. UTF-8 without BOM.
            /// </summary>
            /// <value>The encoding.</value>
            public override System.Text.Encoding Encoding {
                get {
                    return new System.Text.UTF8Encoding(false);
                }
            }
        }

        /// <summary>
        /// Save the document to disk, or throw an Exception if the Path is empty.
        /// </summary>
        public virtual void Save()
        {
            InvalidateXml();

            if(Path == "") {
                throw new Exception(Configuration.GetLocalized("Empty path!"));
            }

            // Write to a temporary file to avoid corrupting the existing document on error
            string tempFileName = System.IO.Path.GetTempFileName();

            System.IO.File.WriteAllText(tempFileName, GetXmlString(), new System.Text.UTF8Encoding(false));

            if(System.IO.File.Exists(Path)) {
                System.IO.File.Delete(Path);
            }
            System.IO.File.Move(tempFileName, Path);
        }

        /// <summary>
        /// Fills the document's XML content.
        /// </summary>
        protected virtual void FillXML(XElement root)
        {
            root.SetAttributeValue("APIVersion", DocumentAPIVersion);
            root.Element("Settings")?.Remove();
            var settings = Settings.GetXml();
            root.Add(settings);

            root.Element("PetriNet")?.Remove();
            root.Add(PetriNet.GetXML());
        }

        /// <summary>
        /// Gets the XML descriptor of the document.
        /// </summary>
        /// <returns>The XML descriptor.</returns>
        public XDocument GetXml()
        {
            if(_xmlBackup == null) {
                _xmlBackup = new XDocument();
                var root = new XElement("Document");
                _xmlBackup.Add(root);
            }
            FillXML(_xmlBackup.Element("Document"));

            return _xmlBackup;
        }

        /// <summary>
        /// Gets the XML document as a string.
        /// </summary>
        /// <returns>The xml string.</returns>
        public string GetXmlString()
        {
            if(_xmlStringCache == null) {
                var xml = GetXml();

                XmlWriterSettings xsettings = new XmlWriterSettings();
                xsettings.Indent = true;
                var sw = new BOMLessUTF8StringWriter();
                XmlWriter writer = XmlWriter.Create(sw, xsettings);
                xml.Save(writer);
                writer.Flush();
                writer.Close();

                sw.Write('\n');

                _xmlStringCache = sw.ToString();
            }

            return _xmlStringCache;
        }

        /// <summary>
        /// Invalidates the XML cache.
        /// </summary>
        public virtual void InvalidateXml()
        {
            _xmlStringCache = null;
        }

        /// <summary>
        /// Loads the document from an XML descriptor.
        /// </summary>
        protected override void LoadFromXml(XDocument document)
        {
            base.LoadFromXml(document);
            _xmlBackup = document;
            InvalidateXml();
            ClearIssues();
        }

        /// <summary>
        /// Reassigns the IDs of the entities in this document, starting from 0.
        /// </summary>
        public override void ReassignIDs()
        {
            base.ReassignIDs();
            InvalidateXml();
        }

        /// <summary>
        /// Gets the list of conflicting entities, mapped to the conflict cause and type.
        /// </summary>
        /// <value>The conflicting.</value>
        Dictionary<Entity, SortedSet<EntityIssue>> _issues = new Dictionary<Entity, SortedSet<EntityIssue>>();

        /// <summary>
        /// The XML value of the document, as it was last loaded or generated.
        /// </summary>
        protected XDocument _xmlBackup;
        string _xmlStringCache;
    }
}
