//
//  Configuration.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2014-11-13.
//

using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Xml;
using System.Xml.Linq;

namespace Petri.Application
{
    /// <summary>
    /// The software platform the application is running on.
    /// </summary>
    public enum Platform
    {
        /// <summary>
        /// The Windows operating system.
        /// </summary>
        Windows,

        /// <summary>
        /// A GNU/Linux based OS.
        /// </summary>
        Linux,

        /// <summary>
        /// OS X
        /// </summary>
        Mac,
    }

    /// <summary>
    /// A singleton-like class that embeds access to persistent settings and localization utilities.
    /// </summary>
    public static class Configuration
    {
        /// <summary>
        /// Gets the configuration API version.
        /// </summary>
        /// <value>The configuration API version.</value>
        public static int ConfigurationAPIVersion {
            get {
                return 100;
            }
        }

        /// <summary>
        /// Gets the end date of the trial period.
        /// </summary>
        /// <value>The trial period valid until.</value>
        public static DateTime? TrialPeriodValidUntil => null;

        /// <summary>
        /// Gets a value indicating whether the application is on a trial period.
        /// </summary>
        /// <value><c>true</c> if is on trial; otherwise, <c>false</c>.</value>
        public static bool IsOnTrial => TrialPeriodValidUntil != null;

        /// <summary>
        /// Checks if we are within the trial period.
        /// </summary>
        /// <returns><c>true</c> if we are within the allowed trial period.</returns>
        public static bool CheckTrialPeriod()
        {
            if(IsOnTrial) {
                return RemainingTrialDuration > 0;
            }

            return true;
        }

        /// <summary>
        /// Gets the duration of the remaining trial in days.
        /// </summary>
        /// <value>The duration of the remaining trial.</value>
        public static int RemainingTrialDuration {
            get {
                if(!IsOnTrial) {
                    return int.MaxValue;
                }
                var trialStartDate = DateTime.Now;
                try {
                    trialStartDate = Max(trialStartDate, File.GetCreationTime(ConfigurationFilePath));
                } catch {
                    // Don't care
                }
                try {
                    trialStartDate = Max(trialStartDate, File.GetCreationTime(CLI.Debugger.InputManager.HistoryPath));
                } catch {
                    // Don't care
                }
                try {
                    // This may be deleted as we release in a bundle.
                    trialStartDate = Max(trialStartDate, File.GetCreationTime(Assembly.GetExecutingAssembly().Location));
                } catch {
                    // Don't care
                }
                try {
                    trialStartDate = Max(trialStartDate, File.GetCreationTime("/usr/bin/petrilab"));
                } catch {
                    // Don't care
                }
                try {
                    trialStartDate = Max(trialStartDate, Directory.GetCreationTime("/usr/share/petrilab"));
                } catch {
                    // Don't care
                }

                return Math.Max(0, (int)Math.Ceiling((TrialPeriodValidUntil.Value - trialStartDate).TotalDays));
            }
        }

        /// <summary>
        /// Gets the max between 2 datetime objects.
        /// </summary>
        /// <returns>The max.</returns>
        /// <param name="d1">D1.</param>
        /// <param name="d2">D2.</param>
        static DateTime Max(DateTime d1, DateTime d2)
        {
            return d1 < d2 ? d2 : d1;
        }

        /// <summary>
        /// The error when a key is missing from the localization dictionary
        /// </summary>
        public static readonly string NoLocalizationFormat = "No localization found for locale \"{0}\" and key \"{1}\"";

        /// <summary>
        /// Gets the running platform.
        /// </summary>
        /// <value>The running platform.</value>
        public static Platform RunningPlatform {
            get;
            private set;
        }

        /// <summary>
        /// Gets the path of the directory containing the executable.
        /// </summary>
        /// <value>The path.</value>
        public static string PathToExecutable {
            get {
                if(!Environment.GetEnvironmentVariables().Contains("PETRILAB_DIR")) {
                    return Directory.GetParent(Assembly.GetExecutingAssembly().Location).FullName;
                } else {
                    return (string)Environment.GetEnvironmentVariables()["PETRILAB_DIR"];
                }
            }
        }

        /// <summary>
        /// Gets the path to the directory containing the examples documents.
        /// </summary>
        /// <value>The examples path.</value>
        public static string ExamplesPath {
            get {
                if(Environment.GetEnvironmentVariables().Contains("PETRILAB_DIR")) {
                    var path = (string)Environment.GetEnvironmentVariables()["PETRILAB_DIR"];
                    if(!Path.IsPathRooted(path) && path.StartsWithInv("~/")) {
                        path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile),
                                                      path.Substring(2));
                    }
                    return Path.Combine(path, "Examples");
                } else {
                    // When the app is run in the debugger
                    return PathUtility.GetFullPath(Path.Combine(
                        Assembly.GetExecutingAssembly().Location,
                        "../../../Examples/"
                    ));
                }
            }
        }

        /// <summary>
        /// Gets the python runtime info, in the form of a tupple of (exists, Major.Minor, ABI flags).
        /// </summary>
        /// <returns>The python runtime info.</returns>
        /// <param name="version">If <c>"2"</c>, returns info about python2. Else, python3 info is returned.</param>
        public static Tuple<bool, string, string> GetPythonRuntimeInfo(string version)
        {
            if(version == "2") {
                if(_python2Runtime == null) {
                    var versionString = "'import sys;print(\"{0.major}.{0.minor}\".format(sys.version_info))'";
                    {
                        var process = Application.CreateProcess("python2", "-c " + versionString);
                        process.StartInfo.RedirectStandardOutput = true;
                        process.StartInfo.RedirectStandardError = true;
                        process.Start();
                        var output = process.StandardOutput.ReadToEnd();
                        process.WaitForExit();
                        if(process.ExitCode == 0) {
                            var outputParts = output.Split('\n');
                            if(outputParts[0].StartsWithInv("2.")) {
                                _python2Runtime = Tuple.Create(true, outputParts[0], "");
                            }
                        } else {
                            _python2Runtime = Tuple.Create(false, "", "");
                        }
                    }
                }

                return _python2Runtime;
            } else {
                if(_python3Runtime == null) {
                    var versionString = "'import sys;print(\"{0.major}.{0.minor}\".format(sys.version_info));print(sys.abiflags)'";
                    {
                        var process = Application.CreateProcess("python3", "-c " + versionString);
                        process.StartInfo.RedirectStandardOutput = true;
                        process.StartInfo.RedirectStandardError = true;
                        process.Start();
                        var output = process.StandardOutput.ReadToEnd();
                        process.WaitForExit();
                        if(process.ExitCode == 0) {
                            var outputParts = output.Split('\n');
                            if(outputParts[0].StartsWithInv("3.")) {
                                _python3Runtime = Tuple.Create(true, outputParts[0], outputParts[1]);
                            }
                        } else {
                            _python3Runtime = Tuple.Create(false, "", "");
                        }
                    }
                }

                return _python3Runtime;
            }
        }

        /// <summary>
        /// Gets or sets the GUI language.
        /// </summary>
        /// <value>The language.</value>
        public static string Language {
            get {
                if(_language != null) {
                    return _language;
                }

                return System.Globalization.CultureInfo.CurrentCulture.TwoLetterISOLanguageName;
            }
            set {
                _language = value;
                LoadLanguage();
            }
        }

        /// <summary>
        /// Gets the localized value of the given key, accordind to the current GUI language.
        /// </summary>
        /// <returns>The localized.</returns>
        /// <param name="key">The key.</param>
        public static string GetLocalized(string key)
        {
            if(Configuration._localizedStrings == null) {
                LoadLanguage();
            }

            string val;
            if(!Configuration._localizedStrings.TryGetValue(key, out val)) {
                Console.Error.WriteLine(NoLocalizationFormat, Language, key);
                return key;
            } else {
                return val;
            }
        }

        /// <summary>
        /// Gets the localized value for the given key, and formats it withe the given arguments.
        /// </summary>
        /// <returns>The localized.</returns>
        /// <param name="key">Key.</param>
        /// <param name="args">Arguments.</param>
        public static string GetLocalized(string key, params object[] args)
        {
            return string.Format(GetLocalized(key), args);
        }

        /// <summary>
        /// Gets or sets the path used for the last document save operation.
        /// </summary>
        /// <value>The save path.</value>
        public static string SavePath {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the recent documents list, as a JSON-serialized array of paths and access time.
        /// </summary>
        /// <value>The recent documents.</value>
        public static string RecentDocuments {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the path used for the last debugger host's executable.
        /// </summary>
        /// <value>The last debugger host.</value>
        public static string LastDebuggerHost {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the path used for the last debugger host's arguments.
        /// </summary>
        /// <value>The last debugger host arguments.</value>
        public static string LastDebuggerHostParams {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the default language for new documents.
        /// </summary>
        /// <value>The default programming language.</value>
        public static Code.Language DefaultLanguage {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the number of recent actions in the entity editor.
        /// </summary>
        /// <value>The default programming language.</value>
        public static int EntityEditorMaxRecentActions {
            get;
            set;
        }

        /// <summary>
        /// The max number of recent documents in the File menu.
        /// </summary>
        public static int MaxRecentDocuments {
            get;
            set;
        }

        /// <summary>
        /// Gets the radius of the transitions' handle dot.
        /// </summary>
        /// <value>The transition handle radius.</value>
        public static int TransitionHandleRadius {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the editor and debugger views must stay in sync.
        /// </summary>
        /// <value><c>true</c> if keep editor and debug views are synced; otherwise, <c>false</c>.</value>
        public static bool KeepEditorAndDebugViewsInSync {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the user prefers the use of relative paths for headers, libraries, imported petri nets etc., instead of absolute paths.
        /// </summary>
        /// <value><c>true</c> if prefer relative paths; otherwise, <c>false</c>.</value>
        public static bool PreferRelativePaths {
            get;
            set;
        }

        /// <summary>
        /// Loads the current language's resource file.
        /// </summary>
        static void LoadLanguage()
        {
            string resource;
            if(Language == "fr") {
                resource = "fr.lang";
            } else if(Language == "en") {
                resource = "en.lang";
            } else {
                Console.Error.WriteLine("Language \"{0}\" not supported, defaulting to English locale.",
                                        Language);
                resource = "en.lang";
            }

            _localizedStrings = GetLocalizationsForLanguage(resource);
        }

        /// <summary>
        /// Loads the specified language resource file.
        /// </summary>
        public static Dictionary<string, string> GetLocalizationsForLanguage(string lang)
        {
            var dict = new Dictionary<string, string>();

            try {
                Assembly assembly = Assembly.GetExecutingAssembly();
                Stream objStream = assembly.GetManifestResourceStream(lang);
                var document = XDocument.Load(new StreamReader(objStream));
                var root = document.FirstNode as XElement;
                string key = null;
                foreach(var element in root.Descendants()) {
                    if(key == null) {
                        key = element.Value;
                    } else {
                        if(dict.ContainsKey(key)) {
                            Console.Error.WriteLine("Warning: the key \"{0}\" is already present for the locale \"{1}\".",
                                                    key,
                                                    Language);
                        } else {
                            dict.Add(key, element.Value);
                        }
                        key = null;
                    }
                }
            } catch(Exception e) {
                Console.Error.WriteLine("Could not load language {0}: {1}",
                                        lang,
                                        e.Message);
            }

            return dict;
        }

        /// <summary>
        /// Gets the path of the application's configuration file.
        /// </summary>
        /// <value>The configuration file path.</value>
        public static string ConfigurationFilePath {
            get {
                if(_configPath == null) {
                    switch(RunningPlatform) {
                    case Platform.Mac:
                    case Platform.Linux:
                        // TODO: remove the migration step somewhere in the future #99
                        var newPath = Path.Combine(
                            Environment.GetFolderPath(Environment.SpecialFolder.UserProfile),
                            ".petrilab_config"
                        );

                        var oldPath = Path.Combine(
                            Environment.GetFolderPath(Environment.SpecialFolder.UserProfile),
                            ".petri_config"
                        );

                        try {
                            if(File.Exists(oldPath) && !File.Exists(newPath)) {
                                File.Move(oldPath, newPath);
                            }
                        } catch {
                            // We tried…
                        }

                        _configPath = newPath;
                        break;
                    case Platform.Windows:
                        // TODO: tbd
                        _configPath = "petrilab_config";
                        break;
                    default:
                        throw new Exception("Configuration.get_ConfigurationFilePath: Should not get there!");
                    }

                }

                return _configPath;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.Configuration"/> class.
        /// Must be kept public, otherwise a Save operation will result in an exception.
        /// </summary>
        static Configuration()
        {
            switch(Environment.OSVersion.Platform) {
            case PlatformID.Unix:
                if(Directory.Exists("/Applications") && Directory.Exists("/System") && Directory.Exists("/Users") && Directory.Exists("/Volumes")) {
                    RunningPlatform = Platform.Mac;
                } else {
                    RunningPlatform = Platform.Linux;
                }
                break;
            case PlatformID.MacOSX:
                RunningPlatform = Platform.Mac;
                break;
            default:
                RunningPlatform = Platform.Windows;
                break;
            }

            DefaultLanguage = Code.Language.Cpp;
            LastDebuggerHost = "";
            LastDebuggerHostParams = "";
            RecentDocuments = "";
            SavePath = "";
            EntityEditorMaxRecentActions = 5;
            MaxRecentDocuments = 10;
            KeepEditorAndDebugViewsInSync = true;
            PreferRelativePaths = true;
            TransitionHandleRadius = 7;

            try {
                if(!File.Exists(ConfigurationFilePath)) {
                    Configuration.Save();
                }

                var document = XDocument.Load(ConfigurationFilePath);
                var root = document.FirstNode as XElement;

                int apiVersion = 99; // default API version if nothing specified
                try {
                    apiVersion = int.Parse(root.Attribute("APIVersion").Value);

                } catch {
                }

                if(apiVersion > ConfigurationAPIVersion) {
                    Console.Error.WriteLine("It appears a more recent verison of this application has been run on this computer. The user configuration parsing may break, and some information may be be lost.");
                }

                var node = root.Element("DefaultLanguage");
                if(node != null) {
                    try {
                        DefaultLanguage = (Code.Language)Enum.Parse(typeof(Code.Language), node.Value);
                    } catch {
                        Console.Error.WriteLine("Invalid language value: {0}.",
                                                node.Value);
                    }
                }

                node = root.Element("LastDebuggerHost");
                if(node != null) {
                    LastDebuggerHost = node.Value;
                }

                node = root.Element("LastDebuggerHostParams");
                if(node != null) {
                    LastDebuggerHostParams = node.Value;
                }

                node = root.Element("RecentDocuments");
                if(node != null) {
                    RecentDocuments = node.Value;
                }

                node = root.Element("SavePath");
                if(node != null) {
                    SavePath = node.Value;
                }

                node = root.Element("EntityEditorRecentActions");
                if(node != null) {
                    try {
                        EntityEditorMaxRecentActions = Math.Max(0, int.Parse(node.Value));
                    } catch {
                        Console.Error.WriteLine("Invalid entity editor's recent actions count value: {0}.",
                                                node.Value);
                    }
                }

                node = root.Element("MaxRecentDocuments");
                if(node != null) {
                    try {
                        MaxRecentDocuments = Math.Max(0, int.Parse(node.Value));
                    } catch {
                        Console.Error.WriteLine("Invalid max recent documents count value: {0}.",
                                                node.Value);
                    }
                }

                node = root.Element("KeepEditorAndDebugViewsInSync");
                if(node != null) {
                    try {
                        KeepEditorAndDebugViewsInSync = bool.Parse(node.Value);
                    } catch {
                        Console.Error.WriteLine("Invalid keep editor and debugger views in sync value: {0}.",
                                                node.Value);
                    }
                }

                node = root.Element("PreferRelativePaths");
                if(node != null) {
                    try {
                        PreferRelativePaths = bool.Parse(node.Value);
                    } catch {
                        Console.Error.WriteLine("Invalid prefer relative paths value: {0}.",
                                                node.Value);
                    }
                }

                node = root.Element("TransitionHandleRadius");
                if(node != null) {
                    try {
                        TransitionHandleRadius = Math.Max(1, int.Parse(node.Value));
                    } catch {
                        Console.Error.WriteLine("Invalid transition handle value value: {0}.",
                                                node.Value);
                    }
                }
            } catch(Exception e) {
                Console.Error.WriteLine("Could not load the application's configuration file: {0}",
                                        e.Message);
            }

            LoadLanguage();
        }

        /// <summary>
        /// Save the configuration.
        /// </summary>
        public static void Save()
        {
            var root = new XElement("PetriSettings");
            root.SetAttributeValue("APIVersion", ConfigurationAPIVersion);

            root.Add(new XElement("DefaultLanguage", DefaultLanguage));
            root.Add(new XElement("LastDebuggerHost", LastDebuggerHost));
            root.Add(new XElement("LastDebuggerHostParams", LastDebuggerHostParams));
            root.Add(new XElement("RecentDocuments", RecentDocuments));
            root.Add(new XElement("SavePath", SavePath));
            root.Add(new XElement("EntityEditorRecentActions", EntityEditorMaxRecentActions));
            root.Add(new XElement("MaxRecentDocuments", MaxRecentDocuments));
            root.Add(new XElement("KeepEditorAndDebugViewsInSync", KeepEditorAndDebugViewsInSync));
            root.Add(new XElement("PreferRelativePaths", PreferRelativePaths));
            root.Add(new XElement("TransitionHandleRadius", TransitionHandleRadius));

            var doc = new XDocument();
            doc.Add(root);

            XmlWriterSettings xsettings = new XmlWriterSettings();
            xsettings.Indent = true;
            xsettings.Encoding = new System.Text.UTF8Encoding(false);

            DateTime? lastWrite = null;
            try {
                if(File.Exists(ConfigurationFilePath)) {
                    lastWrite = File.GetLastWriteTime(ConfigurationFilePath);
                }
            } catch {
                // Don't care
            }
            using(var stream = File.CreateText(ConfigurationFilePath)) {
                using(var writer = XmlWriter.Create(stream, xsettings)) {
                    doc.Save(writer);
                }
            }
            if(lastWrite.HasValue) {
                File.SetLastWriteTime(ConfigurationFilePath, lastWrite.Value);
            }
        }

        static string _language = null;
        static string _configPath = null;
        static Dictionary<string, string> _localizedStrings;
        static Tuple<bool, string, string> _python2Runtime, _python3Runtime;
    }
}

