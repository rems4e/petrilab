//
//  Controller.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2014-11-23.
//

using System;
using System.Collections.Generic;

using Petri.Model;

using Action = Petri.Model.Action;

namespace Petri.Application
{
    /// <summary>
    /// The base class of editor and debugger's controllers.
    /// </summary>
    public abstract class Controller
    {
        /// <summary>
        /// The kind of entities that are searched.
        /// </summary>
        [Flags]
        public enum FindWhich : uint
        {
            /// <summary>
            /// All actions.
            /// </summary>
            State = 0x1,

            /// <summary>
            /// All transitions.
            /// </summary>
            Transition = 0x2,

            /// <summary>
            /// All comments.
            /// </summary>
            Comment = 0x4,

            /// <summary>
            /// All kind of entities.
            /// </summary>
            All = 0xFFFFFFFF
        }

        /// <summary>
        /// The fields of the entities that are considered when searching.
        /// </summary>
        [Flags]
        public enum FindWhere : uint
        {
            /// <summary>
            /// The ID of the entities.
            /// </summary>
            ID = 0x1,

            /// <summary>
            /// The name of the entities.
            /// </summary>
            Name = 0x2,

            /// <summary>
            /// The source code of the entity, if applicable.
            /// </summary>
            Code = 0x4,

            /// <summary>
            /// Search in all fields.
            /// </summary>
            Everywhere = 0xFFFFFFFF
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Application.Controller"/> class.
        /// </summary>
        /// <param name="petriNetProvider">The petri net provider.</param>
        protected Controller(PetriNetProvider petriNetProvider)
        {
            _petriNetProvider = petriNetProvider;
        }

        /// <summary>
        /// Gets the petri net.
        /// </summary>
        /// <value>The petri net.</value>
        public RootPetriNet PetriNet {
            get {
                return _petriNetProvider.PetriNet;
            }
        }

        /// <summary>
        /// Find the entities that match the specified criteria.
        /// </summary>
        /// <returns>The entities found.</returns>
        /// <param name="which">The kind of entities to find.</param>
        /// <param name="where">The fields to search in.</param>
        /// <param name="what">The string to search.</param>
        /// <param name="regex">Whether to match fields using a regex.</param>
        public List<Entity> Find(FindWhich which, FindWhere where, string what, bool regex)
        {
            var matcher = new Predicate<string>((obj) => {
                if(regex) {
                    return System.Text.RegularExpressions.Regex.Match(obj, what).Success;
                } else {
                    return obj.Contains(what);
                }
            });

            var result = new List<Entity>();
            var list = PetriNet.BuildAllEntitiesList();
            foreach(var ee in list) {
                if(ee is State && which.HasFlag(FindWhich.State)) {
                    var e = (State)ee;
                    if(where.HasFlag(FindWhere.ID) && matcher.Invoke(e.GlobalID.ToString())) {
                        result.Add(e);
                    }
                    if(where.HasFlag(FindWhere.Name) && matcher.Invoke(e.Name)) {
                        result.Add(e);
                    }
                    if(e is Action && where.HasFlag(FindWhere.Code) && matcher.Invoke(((Action)e).Invocation.MakeUserReadable())) {
                        result.Add(e);
                    }
                }
                if(ee is Transition && which.HasFlag(FindWhich.Transition)) {
                    var e = (Transition)ee;
                    if(where.HasFlag(FindWhere.ID) && matcher.Invoke(e.GlobalID.ToString())) {
                        result.Add(e);
                    }
                    if(where.HasFlag(FindWhere.Code) && matcher.Invoke(e.Condition.MakeUserReadable())) {
                        result.Add(e);
                    }
                    if(where.HasFlag(FindWhere.Name) && matcher.Invoke(e.Name)) {
                        result.Add(e);
                    }
                }
                if(ee is Comment && which.HasFlag(FindWhich.Comment)) {
                    var e = (Comment)ee;
                    if(where.HasFlag(FindWhere.ID) && matcher.Invoke(e.GlobalID.ToString())) {
                        result.Add(e);
                    }
                    if(where.HasFlag(FindWhere.Name) && matcher.Invoke(e.Name)) {
                        result.Add(e);
                    }
                }
            }

            return result;
        }

        PetriNetProvider _petriNetProvider;
    }
}
