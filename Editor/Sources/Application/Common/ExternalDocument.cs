//
//  ExternalDocument.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-01.
//

using Petri.Model;

namespace Petri.Application
{
    /// <summary>
    /// A document inside another document.
    /// </summary>
    public abstract class ExternalDocument : Document
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.ExternalDocument"/> class.
        /// </summary>
        /// <param name="parent">The petri net enclosing this external document.</param>
        /// <param name="path">Path to the document.</param>
        protected ExternalDocument(ExternalInnerPetriNet parent, string path) : base(parent, path)
        {
            Settings.OutputType = OutputType.StaticLib;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.ExternalDocument"/> class.
        /// </summary>
        /// <param name="parent">The petri net enclosing this external document.</param>
        /// <param name="language">Language of the document.</param>
        protected ExternalDocument(ExternalInnerPetriNet parent, Code.Language language) : base(parent, language)
        {
            Settings.OutputType = OutputType.StaticLib;
        }

        /// <summary>
        /// Notifies an error to the user.
        /// </summary>
        /// <param name="error">Error.</param>
        public override void NotifyError(string error)
        {
            Parent.Document.NotifyError(error);
        }

        /// <summary>
        /// Notifies a warning to the user.
        /// </summary>
        /// <param name="warning">The message.</param>
        public override void NotifyWarning(string warning)
        {
            Parent.Document.NotifyWarning(warning);
        }

        /// <summary>
        /// Notifies some information to the user.
        /// </summary>
        /// <param name="info">info.</param>
        public override void NotifyInfo(string info)
        {
            Parent.Document.NotifyInfo(info);
        }
    }

    /// <summary>
    /// A document inside another document, without cyclic dependencies between it and its root document.
    /// </summary>
    public class ValidExternalDocument : ExternalDocument
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.ValidExternalDocument"/> class.
        /// </summary>
        /// <param name="parent">The document's parent document.</param>
        /// <param name="path">Path to the document.</param>
        public ValidExternalDocument(Model.ExternalInnerPetriNet parent, string path) : base(parent, path)
        {
        }
    }

    /// <summary>
    /// A document inside another document, with at least one cyclic dependency between it and its root document.
    /// </summary>
    public class InvalidExternalDocument : ExternalDocument
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.InvalidExternalDocument"/> class.
        /// </summary>
        /// <param name="parent">The document's parent document.</param>
        /// <param name="path">Path to the document.</param>
        /// <param name="message">The error message.</param>
        /// <param name="error">The error code.</param>
        public InvalidExternalDocument(Model.ExternalInnerPetriNet parent,
                                       string path,
                                       string message,
                                       EntityIssue.Kind error) : base(parent,
                                                                 parent.Document.Settings.Language)
        {
            Path = path;
            Message = message;
            Error = error;
        }

        /// <summary>
        /// Gets the error message.
        /// </summary>
        /// <value>The message.</value>
        public string Message {
            get;
            private set;
        }

        /// <summary>
        /// Gets the error code.
        /// </summary>
        /// <value>The error.</value>
        public EntityIssue.Kind Error {
            get;
            private set;
        }
    }
}

