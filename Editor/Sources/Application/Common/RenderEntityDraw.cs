//
//  RenderEntityDraw.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2014-12-09.
//

namespace Petri.Application
{
    /// <summary>
    /// The specialization of the entity draw for a PDF file creation.
    /// </summary>
    public class RenderEntityDraw : EntityDraw
    {
    }
}

