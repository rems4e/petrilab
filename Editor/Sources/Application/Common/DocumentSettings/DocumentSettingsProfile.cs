//
//  DocumentSettingsProfile.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2017-10-13.
//

using System;
using System.Xml.Linq;
using Petri.Code;

namespace Petri.Application
{
    /// <summary>
    /// Deploy mode.
    /// </summary>
    public enum DeployMode
    {
        /// <summary>
        /// Disables deployment.
        /// </summary>
        DoNothing,

        /// <summary>
        /// Copy the product to a custom destination.
        /// </summary>
        Copy,

        /// <summary>
        /// Execute a script.
        /// </summary>
        Script,
    }

    /// <summary>
    /// Debug mode.
    /// </summary>
    public enum DebugMode
    {
        /// <summary>
        /// Attach to an externally managed debug host.
        /// </summary>
        Attach,

        /// <summary>
        /// Attach to a lightweight internal debug host.
        /// </summary>
        RunInEditor,

        /// <summary>
        /// Create a custom external host and attache to it.
        /// </summary>
        CreateHost,
    }

    /// <summary>
    /// A document's settings values.
    /// </summary>
    public class DocumentSettingsProfile
    {
        /// <summary>
        /// Gets the default settings for a new document.
        /// </summary>
        /// <param name="language">The language.</param>
        public static DocumentSettingsProfile GetDefaultProfile(Language language)
        {
            return new DocumentSettingsProfile(language, null);
        }

        /// <summary>
        /// An XML element that may be used for serializing the current settings.
        /// </summary>
        /// <returns>The xml.</returns>
        public XElement GetXml()
        {
            var elem = new XElement("Profile");
            elem.SetAttributeValue("Name", Name);
            elem.SetAttributeValue("SourceOutputPath", RelativeSourceOutputPath);
            elem.SetAttributeValue("LibOutputPath", RelativeLibOutputPath);
            if(MaxConcurrency != null) {
                elem.SetAttributeValue("MaxConcurrency", MaxConcurrency.ToString());
            }
            elem.SetAttributeValue("DeployMode", DeployMode.ToString());
            elem.SetAttributeValue("DeployPath", DeployPath);
            elem.SetAttributeValue("DeployScript", DeployScript);
            elem.SetAttributeValue("DeployWhenDebug", DeployWhenDebug.ToString());
            elem.SetAttributeValue("Port", DebugPort.ToString());
            elem.SetAttributeValue("DebugMode", DebugMode.ToString());
            elem.SetAttributeValue("Hostname", Hostname);
            elem.SetAttributeValue("HostProgram", HostProgram);
            elem.SetAttributeValue("HostProgramArguments", HostProgramArguments);

            var node = new XElement("Compiler");
            node.SetAttributeValue("Invocation", CompilerInfo.Compiler);
            foreach(var f in CompilerInfo.CustomCompilerFlags) {
                var e = new XElement("Flag");
                e.SetAttributeValue("Value", f);
                node.Add(e);
            }
            foreach(var f in CompilerInfo.CustomLinkerFlags) {
                var e = new XElement("LinkerFlag");
                e.SetAttributeValue("Value", f);
                node.Add(e);
            }
            elem.Add(node);

            node = new XElement("IncludePaths");
            foreach(var p in CompilerInfo.IncludePaths) {
                var e = new XElement("IncludePath");
                e.SetAttributeValue("Path", p.Item1);
                e.SetAttributeValue("Recursive", p.Item2);
                node.Add(e);
            }
            elem.Add(node);

            node = new XElement("LibPaths");
            foreach(var p in CompilerInfo.LibPaths) {
                var e = new XElement("LibPath");
                e.SetAttributeValue("Path", p.Item1);
                e.SetAttributeValue("Recursive", p.Item2);
                node.Add(e);
            }
            elem.Add(node);

            node = new XElement("Libs");
            foreach(var p in CompilerInfo.Libs) {
                var e = new XElement("Lib");
                e.SetAttributeValue("Path", p);
                node.Add(e);
            }
            elem.Add(node);

            return elem;
        }

        /// <summary>
        /// Clone this instance, and return the cloned instance.
        /// </summary>
        public DocumentSettingsProfile Clone()
        {
            return new DocumentSettingsProfile(_language, GetXml());
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.DocumentSettingsProfile"/> class with a sensible set of default values if <paramref name="elem"/> is <c>null</c>, and to the values contained in the Xml element otherwise.
        /// </summary>
        /// <param name="language">The document's language.</param>
        /// <param name="elem">An XML element that may be null.</param>
        public DocumentSettingsProfile(Language language, XElement elem)
        {
            AssignFromXml(language, elem);
        }

        /// <summary>
        /// Assigns the values of this instance from a valid XML element obtained from a call to <see cref="Petri.Application.DocumentSettings.GetXml()"/>.
        /// </summary>
        /// <param name="language">The document's language.</param>
        /// <param name="elem">Element.</param>
        public void AssignFromXml(Language language, XElement elem)
        {
            _language = language;

            CompilerInfo = new CompilerInfo();

            Name = "Default";
            RelativeSourceOutputPath = "";
            RelativeLibOutputPath = "";
            MaxConcurrency = null;
            DeployMode = DeployMode.DoNothing;
            DeployPath = "";
            DeployScript = "#!/bin/sh\n\n/bin/cp \"$PETRI_LIB_PATH\" .\n";
            DeployWhenDebug = true;
            DebugPort = 12345;
            DebugMode = DebugMode.Attach;
            Hostname = "localhost";
            HostProgram = "";
            HostProgramArguments = "";

            if(_language == Language.Cpp) {
                CompilerInfo.Compiler = "c++";
            } else if(_language == Language.C || _language == Language.EmbeddedC) {
                CompilerInfo.Compiler = "cc";
            } else if(_language == Language.CSharp) {
                CompilerInfo.Compiler = "csc";
            } else if(_language == Language.Python) {
                CompilerInfo.Compiler = "cc";
            } else {
                throw new Exception("DocumentSettingsProfile.AssignFromXml: Should not get there!");
            }

            if(elem != null) {
                Name = elem.Attribute("Name").Value;

                if(elem.Attribute("SourceOutputPath") != null) {
                    RelativeSourceOutputPath = elem.Attribute("SourceOutputPath").Value;
                }

                if(elem.Attribute("LibOutputPath") != null) {
                    RelativeLibOutputPath = elem.Attribute("LibOutputPath").Value;
                }

                if(elem.Attribute("MaxConcurrency") != null) {
                    MaxConcurrency = UInt64.Parse(elem.Attribute("MaxConcurrency").Value);
                }

                if(elem.Attribute("DeployMode") != null) {
                    try {
                        DeployMode = (DeployMode)System.Enum.Parse(DeployMode.GetType(), elem.Attribute("DeployMode").Value);
                    } catch {
                        Console.Error.WriteLine("Invalid deploy mode value: {0}.", elem.Attribute("DeployMode").Value);
                        throw;
                    }
                }

                if(elem.Attribute("DeployPath") != null) {
                    DeployPath = elem.Attribute("DeployPath").Value;
                }

                if(elem.Attribute("DeployScript") != null) {
                    DeployScript = elem.Attribute("DeployScript").Value;
                }

                if(elem.Attribute("DeployWhenDebug") != null) {
                    DeployWhenDebug = bool.Parse(elem.Attribute("DeployWhenDebug").Value);
                }

                if(elem.Attribute("Port") != null) {
                    DebugPort = UInt16.Parse(elem.Attribute("Port").Value);
                }

                if(elem.Attribute("DebugMode") != null) {
                    try {
                        DebugMode = (DebugMode)System.Enum.Parse(DebugMode.GetType(), elem.Attribute("DebugMode").Value);
                    } catch {
                        Console.Error.WriteLine("Invalid debug mode value: {0}.", elem.Attribute("DebugMode").Value);
                        throw;
                    }
                }

                if(elem.Attribute("Hostname") != null) {
                    Hostname = elem.Attribute("Hostname").Value;
                }

                if(elem.Attribute("HostProgram") != null) {
                    HostProgram = elem.Attribute("HostProgram").Value;
                }

                if(elem.Attribute("HostProgramArguments") != null) {
                    HostProgramArguments = elem.Attribute("HostProgramArguments").Value;
                }

                var node = elem.Element("Compiler");
                if(node != null) {
                    CompilerInfo.Compiler = node.Attribute("Invocation").Value;

                    foreach(var e in node.Elements("Flag")) {
                        CompilerInfo.CustomCompilerFlags.Add(e.Attribute("Value").Value);
                    }
                    foreach(var e in node.Elements("LinkerFlag")) {
                        CompilerInfo.CustomLinkerFlags.Add(e.Attribute("Value").Value);
                    }
                }

                node = elem.Element("IncludePaths");
                if(node != null) {
                    foreach(var e in node.Elements("IncludePath")) {
                        CompilerInfo.IncludePaths.Add(Tuple.Create(
                            e.Attribute("Path").Value,
                           bool.Parse(e.Attribute("Recursive").Value)
                        ));
                    }
                }

                node = elem.Element("LibPaths");
                if(node != null) {
                    foreach(var e in node.Elements("LibPath")) {
                        CompilerInfo.LibPaths.Add(Tuple.Create(
                            e.Attribute("Path").Value,
                            bool.Parse(e.Attribute("Recursive").Value)
                        ));
                    }
                }

                node = elem.Element("Libs");
                if(node != null) {
                    foreach(var e in node.Elements("Lib")) {
                        CompilerInfo.Libs.Add(e.Attribute("Path").Value);
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the name of the profile.
        /// </summary>
        /// <value>The name of the profile.</value>
        public string Name {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the source output path, i.e. the directoty that will contain the source after generation, relative to the document's path.
        /// </summary>
        /// <value>The source output path.</value>
        public string RelativeSourceOutputPath {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the relative lib output path, i.e. the directoty that will contain the library after compilation, relative to the document's path.
        /// </summary>
        /// <value>The lib output path.</value>
        public string RelativeLibOutputPath {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the max concurrency of the petri net.
        /// </summary>
        /// <value>The max concurrency of the petri net.</value>
        public UInt64? MaxConcurrency {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the deploy mode.
        /// </summary>
        /// <value>The deploy mode.</value>
        public DeployMode DeployMode {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the deployment path.
        /// </summary>
        /// <value>The deploy path.</value>
        public string DeployPath {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the deploy script.
        /// </summary>
        /// <value>The deploy script.</value>
        public string DeployScript {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether to deploy when debuggging.
        /// </summary>
        /// <value><c>true</c> to deploy when debugging; otherwise, <c>false</c>.</value>
        public bool DeployWhenDebug {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the port the DebugClient will attempt to connect to upon debugging.
        /// </summary>
        /// <value>The port.</value>
        public UInt16 DebugPort {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the debug mode.
        /// </summary>
        /// <value>The debug mode.</value>
        public DebugMode DebugMode {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the hostname the DebugClient will attempt to connect to upon debugging.
        /// </summary>
        /// <value>The hostname.</value>
        public string Hostname {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the program to launch for debugging.
        /// </summary>
        /// <value>The host program.</value>
        public string HostProgram {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the arguments for the program to launch for debugging.
        /// </summary>
        /// <value>The host program's arguments.</value>
        public string HostProgramArguments {
            get;
            set;
        }

        /// <summary>
        /// Gets the compiler info.
        /// </summary>
        /// <value>The compiler info.</value>
        public CompilerInfo CompilerInfo {
            get;
            private set;
        }

        Language _language;
    }
}
