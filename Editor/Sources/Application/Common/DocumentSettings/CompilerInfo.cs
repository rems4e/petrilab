//
//  CompilerInfo.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2018-01-22.
//

using System;
using System.Collections.Generic;

namespace Petri.Application
{
    /// <summary>
    /// Necessary info for a compiler invocation.
    /// </summary>
    public class CompilerInfo
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Application.CompilerInfo"/> class.
        /// </summary>
        public CompilerInfo()
        {
            CustomCompilerFlags = new List<string>();
            CustomLinkerFlags = new List<string>();
            IncludePaths = new List<Tuple<string, bool>>();
            LibPaths = new List<Tuple<string, bool>>();
            Libs = new List<string>();
        }

        /// <summary>
        /// Gets or sets the compiler's command.
        /// </summary>
        /// <value>The compiler command.</value>
        public string Compiler {
            get;
            set;
        }

        /// <summary>
        /// The additional user provided flags to be passed to the compiler.
        /// </summary>
        /// <value>The compiler flags.</value>
        public List<string> CustomCompilerFlags {
            get;
            private set;
        }

        /// <summary>
        /// The additional user provided flags to be passed to the linker.
        /// </summary>
        /// <value>The linker flags.</value>
        public List<string> CustomLinkerFlags {
            get;
            private set;
        }

        /// <summary>
        /// Gets the include search paths associated with the document.
        /// The first member of each entry is the path to the include search path, and the second member is set to <c>true</c> for a recursive include search path.
        /// </summary>
        /// <value>The include search paths.</value>
        public List<Tuple<string, bool>> IncludePaths {
            get;
            private set;
        }

        /// <summary>
        /// Gets the library search paths associated with the document.
        /// The first member of each entry is the path to the library search path, and the second member is set to <c>true</c> for a recursive library search path.
        /// </summary>
        /// <value>The library search paths.</value>
        public List<Tuple<string, bool>> LibPaths {
            get;
            private set;
        }

        /// <summary>
        /// Gets the library that are needed to compile the document.
        /// </summary>
        /// <value>The libraries.</value>
        public List<string> Libs {
            get;
            private set;
        }
    }
}

