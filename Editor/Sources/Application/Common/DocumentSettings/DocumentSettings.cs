//
//  DocumentSettings.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2014-11-22.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Petri.Code;

namespace Petri.Application
{
    /// <summary>
    /// The compilation product's type
    /// </summary>
    public enum OutputType
    {
        /// <summary>
        /// Create a dynamic library
        /// </summary>
        DynamicLib,

        /// <summary>
        /// Create a static library
        /// </summary>
        StaticLib,
    }

    /// <summary>
    /// A document's settings values.
    /// </summary>
    public class DocumentSettings : Model.ISourceCodeProvider
    {
        /// <summary>
        /// The default Python version.
        /// </summary>
		public static readonly string DefaultPythonVersion = "3";

        /// <summary>
        /// Gets the default settings for a new document.
        /// </summary>
        /// <param name="doc">The document.</param>
        /// <param name="language">The language.</param>
        public static DocumentSettings GetDefaultSettings(Document doc, Language language)
        {
            var settings = new DocumentSettings(doc, null);
            settings.Language = language;
            settings.Enum = GetDefaultEnumForLanguage(language);
            if(settings.Language == Language.Python) {
                settings.AdditionalLanguageInfo = DefaultPythonVersion;
            }
            settings.CurrentProfile = DocumentSettingsProfile.GetDefaultProfile(language);
            settings.Profiles.Add(settings.CurrentProfile);

            return settings;
        }

        /// <summary>
        /// The XML value of the settings, as it was last loaded or generated.
        /// </summary>
        public XElement XMLBackup {
            get;
            private set;
        }

        /// <summary>
        /// An XML element that may be used for serializing the current settings.
        /// </summary>
        /// <returns>The xml.</returns>
        public XElement GetXml()
        {
            XMLBackup = XMLBackup ?? new XElement("Settings");
            XMLBackup.SetAttributeValue("Name", Name);
            XMLBackup.SetAttributeValue("Language", Language.ToString());
            if(AdditionalLanguageInfo != null) {
                XMLBackup.SetAttributeValue("AdditionalLanguageInfo", AdditionalLanguageInfo);
            }
            XMLBackup.SetAttributeValue("Enum", Enum.ToString());
            XMLBackup.SetAttributeValue("DefaultExecutionVerbosity", DefaultExecutionVerbosity.ToString().Replace(" ", "").Replace(",", " | "));
            XMLBackup.SetAttributeValue("OverrideChildCompilationSettings", OverrideChildCompilationSettings.ToString());

            {
                var headers = XMLBackup.Element("Headers");
                if(headers == null) {
                    headers = new XElement("Headers");
                    XMLBackup.Add(headers);
                } else {
                    foreach(var h in new List<XElement>(headers.Elements("Header"))) {
                        h.Remove();
                    }
                }
                foreach(var h in Headers) {
                    var hh = new XElement("Header");
                    hh.SetAttributeValue("File", h);
                    headers.Add(hh);
                }
            }

            {
                var macros = XMLBackup.Element("Macros");
                if(macros == null) {
                    macros = new XElement("Macros");
                    XMLBackup.Add(macros);
                } else {
                    foreach(var m in new List<XElement>(macros.Elements("Macro"))) {
                        m.Remove();
                    }
                }
                foreach(var m in PreprocessorMacros) {
                    var mm = new XElement("Macro");
                    mm.SetAttributeValue("Name", m.Key);
                    mm.SetAttributeValue("Value", m.Value);
                    macros.Add(mm);
                }
            }

            {
                var profiles = XMLBackup.Element("Profiles");
                if(profiles == null) {
                    profiles = new XElement("Profiles");
                    XMLBackup.Add(profiles);
                } else {
                    foreach(var m in new List<XElement>(profiles.Elements("Profile"))) {
                        m.Remove();
                    }
                }
                foreach(var profile in Profiles) {
                    profiles.Add(profile.GetXml());
                }
                profiles.SetAttributeValue("Default", CurrentProfile.Name);
            }

            return XMLBackup;
        }

        /// <summary>
        /// Clone this instance, and return the cloned instance.
        /// </summary>
        public DocumentSettings Clone()
        {
            return new DocumentSettings(_doc, GetXml());
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.DocumentSettings"/> class with a sensible set of default values if <paramref name="elem"/> is <c>null</c>, and to the values contained in the Xml element otherwise.
        /// </summary>
        /// <param name="doc">The document.</param>
        /// <param name="elem">An XML element that may be null.</param>
        public DocumentSettings(Document doc, XElement elem)
        {
            _doc = doc;
            AssignFromXml(elem);
        }

        /// <summary>
        /// Assigns the values of this instance from a valid XML element obtained from a call to <see cref="Petri.Application.DocumentSettings.GetXml()"/>.
        /// </summary>
        /// <param name="elem">Element.</param>
        public void AssignFromXml(XElement elem)
        {
            Profiles = new List<DocumentSettingsProfile>();
            OverrideChildCompilationSettings = false;

            Language = Language.Cpp;
            OutputType = OutputType.DynamicLib;

            Name = "MyPetriNet";
            Enum = GetDefaultEnumForLanguage(Language);

            Headers = new List<string>();
            PreprocessorMacros = new Dictionary<string, string>();

            DefaultExecutionVerbosity = Runtime.PetriNet.LogVerbosity.Nothing;

            if(elem == null) {
                XMLBackup = null;
            } else {
                // Deep copy of the XML
                XMLBackup = new XElement(elem);

                if(elem.Attribute("Name") != null) {
                    Name = elem.Attribute("Name").Value;
                }

                if(elem.Attribute("Language") != null) {
                    try {
                        Language = (Language)System.Enum.Parse(Language.GetType(), elem.Attribute("Language").Value);
                    } catch {
                        Console.Error.WriteLine("Invalid language value: {0}.", elem.Attribute("Language").Value);
                        throw;
                    }
                }

                if(elem.Attribute("AdditionalLanguageInfo") != null) {
                    AdditionalLanguageInfo = elem.Attribute("AdditionalLanguageInfo").Value;
                }

                if(elem.Attribute("Enum") != null) {
                    Enum = new Code.Enum(Language, elem.Attribute("Enum").Value);
                }

                if(elem.Element("Headers") != null) {
                    foreach(var e in elem.Element("Headers").Elements("Header")) {
                        Headers.Add(e.Attribute("File").Value);
                    }
                }

                if(elem.Element("Macros") != null) {
                    foreach(var e in elem.Element("Macros").Elements("Macro")) {
                        PreprocessorMacros.Add(e.Attribute("Name").Value,
                                               e.Attribute("Value").Value);
                    }
                }


                if(elem.Attribute("DefaultExecutionVerbosity") != null) {
                    try {
                        DefaultExecutionVerbosity = (Runtime.PetriNet.LogVerbosity)System.Enum.Parse(
                            typeof(Runtime.PetriNet.LogVerbosity),
                            elem.Attribute("DefaultExecutionVerbosity").Value.Replace(" | ", ",")
                        );
                    } catch {
                        Console.Error.WriteLine("Invalid default execution verbosity value: {0}.", elem.Attribute("DefaultExecutionVerbosity").Value);
                        throw;
                    }
                }

                if(elem.Attribute("OverrideChildCompilationSettings") != null) {
                    OverrideChildCompilationSettings = bool.Parse(elem.Attribute("OverrideChildCompilationSettings").Value);
                }

                var node = elem.Element("Profiles");
                if(node != null) {
                    var names = new HashSet<string>();
                    foreach(var e in node.Elements("Profile")) {
                        var profile = new DocumentSettingsProfile(Language, e);
                        if(!names.Add(profile.Name)) {
                            throw new Exception(Configuration.GetLocalized("A profile with the name {0} has already been loaded!", profile.Name));
                        }
                        Profiles.Add(profile);
                    }
                    if(Profiles.Count == 0) {
                        Profiles.Add(DocumentSettingsProfile.GetDefaultProfile(Language));
                    }
                    var def = node.Attribute("Default").Value;
                    CurrentProfile = Profiles.Find((profile) => {
                        return profile.Name == def;
                    });
                    if(CurrentProfile == null) {
                        CurrentProfile = Profiles[0];
                    }
                }

                if(CurrentProfile == null) {
                    CurrentProfile = DocumentSettingsProfile.GetDefaultProfile(Language);
                    Profiles.Add(CurrentProfile);
                }
            }
        }

        /// <summary>
        /// Gets or sets the name of the document.
        /// </summary>
        /// <value>The name of the document.</value>
        public string Name {
            get {
                return _name;
            }
            set {
                string result = "";
                foreach(char c in value) {
                    if(c < 128 && (char.IsDigit(c) || char.IsLetter(c) || c == '_')) {
                        result += c;
                    }
                }
                if(result.Length == 0) {
                    result = "MyPetriNet";
                } else if(char.IsDigit(result[0])) {
                    result = "_" + result;
                }

                _name = result;
            }
        }

        /// <summary>
        /// Gets the list of settings profiles of the document.
        /// </summary>
        /// <value>The profiles.</value>
        public List<DocumentSettingsProfile> Profiles {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets the current settings profile.
        /// </summary>
        /// <value>The current profile.</value>
        public DocumentSettingsProfile CurrentProfile {
            get;
            set;
        }

        /// <summary>
        /// Gets the profile matched against the root document's current profile.
        /// </summary>
        /// <value>The matched profile.</value>
        public DocumentSettingsProfile MatchedProfile {
            get {
                var profile = Profiles.Find(
                    prof => prof.Name == _doc.RootDocument.Settings.CurrentProfile.Name
                );
                return profile ?? CurrentProfile;
            }
        }

        /// <summary>
        /// Gets or sets the compiler info, matched against the eldest parent that where
        /// its <see cref="OverrideChildCompilationSettings"/> is <c>true</c>.
        /// The second item of the tuple is the document which provide the matched compiler info.
        /// </summary>
        /// <value>The compiler info.</value>
        public Tuple<CompilerInfo, Document> MatchedCompilerInfo {
            get {
                var overridingDocument = _doc;
                var currentDoc = _doc;
                while(currentDoc != null) {
                    if(currentDoc.Settings.OverrideChildCompilationSettings) {
                        overridingDocument = currentDoc;
                    }
                    currentDoc = currentDoc.Parent?.Document ?? null;
                }

                return Tuple.Create(overridingDocument.Settings.MatchedProfile.CompilerInfo, overridingDocument);
            }
        }

        /// <summary>
        /// Gets or sets the enum representing the possible results of the execution of an action.
        /// </summary>
        /// <value>The enum.</value>
        public Code.Enum Enum {
            get;
            set;
        }

        /// <summary>
        /// Gets the headers list of the document.
        /// </summary>
        /// <value>The headers.</value>
        public List<string> Headers {
            get;
            private set;
        }

        /// <summary>
        /// Gets the preprocessor macros of the document.
        /// </summary>
        /// <value>The preprocessor macros.</value>
        public Dictionary<string, string> PreprocessorMacros {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets the default execution verbosity of the petri net.
        /// </summary>
        /// <value>The execution verbosity.</value>
        public Runtime.PetriNet.LogVerbosity DefaultExecutionVerbosity {
            get;
            set;
        }

        /// <summary>
        /// Gets all the known functions.
        /// </summary>
        /// <value>All functions.</value>
        public IEnumerable<Function> AllFunctions {
            get {
                return _doc.AllFunctions.Values.SelectMany(f => f);
            }
        }

        /// <summary>
        /// Gets the default enum for a new document.
        /// </summary>
        /// <value>The default enum.</value>
        /// <param name="language">The language.</param>
        public static Code.Enum GetDefaultEnumForLanguage(Language language)
        {
            return new Code.Enum(language, "ActionResult", new string[] { "OK", "NOK", "TIMEOUT" });
        }

        /// <summary>
        /// Gets or sets a value indicating whether this document's profiles' compilation settings override the
        /// children's profiles' compilation settings.
        /// </summary>
        /// <value><c>true</c> if override children profiles; otherwise, <c>false</c>.</value>
        public bool OverrideChildCompilationSettings {
            get;
            set;
        }

        /// <summary>
        /// Gets the archiver command.
        /// </summary>
        /// <value>The archiver.</value>
        public string Archiver {
            get {
                if(OutputType == OutputType.StaticLib) {
                    return "ar";
                } else if(OutputType == OutputType.DynamicLib) {
                    return MatchedCompilerInfo.Item1.Compiler;
                }

                throw new Exception("DocumentSettings.Archiver: Should not get there!");
            }
        }

        /// <summary>
        /// Gets or sets the language the document uses for generation and compilation.
        /// </summary>
        /// <value>The language.</value>
        public Language Language {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets some additional language info.
        /// </summary>
        /// <value>Some additional language info.</value>
        public string AdditionalLanguageInfo {
            get;
            set;
        }

        /// <summary>
        /// A readable name for the provided language.
        /// </summary>
        /// <returns>The name.</returns>
        /// <param name="language">Language.</param>
        public static string LanguageName(Language language)
        {
            switch(language) {
            case Language.Cpp:
                return "C++";
            case Language.C:
                return "C";
            case Language.CSharp:
                return "C#";
            case Language.EmbeddedC:
                return "Embedded C";
            case Language.Python:
                return "Python";
            }

            throw new Exception("DocumentSettings.LanguageName: Should not get there!");
        }

        /// <summary>
        /// Gets a language from its readable name, given by the <see cref="Petri.Application.DocumentSettings.LanguageName(Language)"/> method.
        /// </summary>
        /// <returns>The language.</returns>
        /// <param name="languageName">The readable name of the language to retrieve.</param>
        public static Language LanguageFromName(string languageName)
        {
            foreach(Language l in System.Enum.GetValues(typeof(Language))) {
                if(LanguageName(l) == languageName) {
                    return l;
                }
            }

            throw new Exception("DocumentSettings.LanguageFromName: Should not get there!");
        }

        /// <summary>
        /// A readable name for the current document's language.
        /// </summary>
        /// <returns>The name.</returns>
        public string LanguageName()
        {
            return LanguageName(Language);
        }

        /// <summary>
        /// Gets the language of the main source file.
        /// </summary>
        /// <value>The main source language.</value>
        public Language MainSourceLanguage {
            get {
                if(Language == Language.Python) {
                    return Language.C;
                }

                return Language;
            }
        }

        /// <summary>
        /// The path to the source file generated by the document.
        /// </summary>
        /// <value>The source path.</value>
        public string RelativeSourcePath {
            get {
                return System.IO.Path.Combine(
                    MatchedProfile.RelativeSourceOutputPath,
                    Name + CodeGen.PetriGen.SourceExtensionFromLanguage(Language)
                );
            }
        }

        /// <summary>
        /// The path to the header file generated by the document, if any.
        /// </summary>
        /// <value>The header path.</value>
        public string RelativeSourceHeaderPath {
            get {
                return System.IO.Path.Combine(
                    MatchedProfile.RelativeSourceOutputPath,
                    Name + CodeGen.PetriGen.HeaderExtensionFromLanguage(Language)
                );
            }
        }

        /// <summary>
        /// The path where the document's dynamic library is to be generated, including the file's name and extension.
        /// </summary>
        /// <value>The lib path.</value>
        public string RelativeLibBaseName {
            get {
                return System.IO.Path.Combine(MatchedProfile.RelativeLibOutputPath, Name);
            }
        }

        /// <summary>
        /// Returns the intermediate compilation file extension according to the document's current language.
        /// </summary>
        /// <value>The intermediate extension.</value>
        public string IntermediateExtension {
            get {
                if(Language == Language.CSharp) {
                    return ".dll";
                } else if(Language == Language.Cpp || Language == Language.C || Language == Language.EmbeddedC || Language == Language.Python) {
                    return ".o";
                }

                throw new Exception("DocumentSettings.IntermediateExtension: Should not get there!");
            }
        }

        /// <summary>
        /// Returns the dynamic library file extension according to the document's current language.
        /// </summary>
        /// <value>The lib extension.</value>
        public string LibExtension {
            get {
                if(Language == Language.CSharp) {
                    return ".dll";
                } else if(Language == Language.Cpp || Language == Language.C || Language == Language.EmbeddedC || Language == Language.Python) {
                    if(OutputType == OutputType.DynamicLib) {
                        return ".so";
                    } else if(OutputType == OutputType.StaticLib) {
                        return ".a";
                    }
                }

                throw new Exception("DocumentSettings.LibExtension: Should not get there!");
            }
        }

        /// <summary>
        /// Gets the complete (path + filename) path to the lib containing the petri net code.
        /// </summary>
        /// <value>The path to the petri net "normal" lib.</value>
        public string StandardLibPath {
            get {
                return System.IO.Path.Combine(
                    System.IO.Directory.GetParent(_doc.Path).FullName,
                    RelativeLibBaseName + LibExtension
                );
            }
        }

        /// <summary>
        /// Gets the complete (path + filename) path to the main source file containing the petri net code.
        /// </summary>
        /// <value>The standard source path.</value>
        public string StandardSourcePath {
            get {
                return System.IO.Path.Combine(
                    System.IO.Directory.GetParent(_doc.Path).FullName,
                    RelativeSourcePath
                );
            }
        }

        /// <summary>
        /// Gets the complete (path + filename) path to the intermediate file containing the petri net code.
        /// </summary>
        /// <value>The standard intermadiate path.</value>
        public string StandardIntermediatePath {
            get {
                return System.IO.Path.Combine(
                    System.IO.Directory.GetParent(_doc.Path).FullName,
                    RelativeLibBaseName + IntermediateExtension
                );
            }
        }

        /// <summary>
        /// Gets or sets the type of the compilation product.
        /// </summary>
        /// <value>The type of the output.</value>
        public OutputType OutputType {
            get {
                return _outputType;
            }
            set {
                _outputType = value;
                if(Language == Language.CSharp) {
                    _outputType = OutputType.DynamicLib;
                }

            }
        }

        /// <summary>
        /// Gets the command line arguments necessary for compiling the provided source file into the requested intermediate file.
        /// </summary>
        /// <returns>The arguments for the compiler invocation.</returns>
        /// <param name="sources">The paths to the source files to be compiled.</param>
        /// <param name="output">The path and filename where the library or intermediate product will be generated.</param>
        /// <param name="compiler">An optional compiler toolchain indicator.</param>
        public string GetCompilerArguments(List<Tuple<string, Language>> sources, string output, Compiler.Toolchain compiler)
        {
            var thisCompiler = MatchedCompilerInfo;
            string val = "";

            if(Language == Language.C || Language == Language.Cpp || Language == Language.Python) {
                val += "-c ";
                if(compiler == Compiler.Toolchain.Clang) {
                    if(Configuration.RunningPlatform == Platform.Mac) {
                        val += "-undefined dynamic_lookup -flat_namespace ";
                    } else {
                        val += "-fPIC -undefined dynamic_lookup -flat_namespace ";
                    }
                } else if(compiler == Compiler.Toolchain.GCC) {
                    val += "-fPIC --no-gnu-unique ";
                }

                val += "-iquote'" + MatchedProfile.RelativeSourceOutputPath + "' ";
                val += "-I'" + MatchedProfile.RelativeSourceOutputPath + "' ";

                if(Language == Language.Python) {
                    var runtime = Configuration.GetPythonRuntimeInfo(AdditionalLanguageInfo);
                    if(runtime.Item1) {
                        var version = runtime.Item2 + runtime.Item3;
                        val += string.Format("-I'/usr/include/python{0}' ", version);
                    }
                }
                val += GetCompilerFlagsForPaths(thisCompiler, hasLibs: false, hasHeaders: true);

                if(Language == Language.Cpp) {
                    val += "-std=c++14 ";
                }

                val += "-o '" + output + "' ";

                foreach(var s in sources) {
                    var flag = CompilerFlagForLanguage(s.Item2);
                    if(flag != null) {
                        val += flag + " '" + s.Item1 + "' ";
                    }
                }
            } else if(Language == Language.CSharp) {
                if(compiler == Compiler.Toolchain.MSVisualCS) {
                    val += "/nologo ";
                }
                val += "-t:library -r:PetriRuntime.dll ";
                val += string.Format(
                    "-lib:'{0}' ",
                    Configuration.PathToExecutable
                );
                val += GetCompilerFlagsForPaths(thisCompiler, hasLibs: true, hasHeaders: true);

                foreach(var h in sources) {
                    val += "'" + h.Item1 + "' ";
                }

                foreach(var path in UniqueDocumentsPaths) {
                    var source = System.IO.Path.Combine(
                        System.IO.Directory.GetParent(path.Key).FullName,
                        path.Value.Settings.RelativeSourcePath
                    );

                    val += "'" + source + "' ";
                    source = System.IO.Path.Combine(
                        System.IO.Directory.GetParent(path.Key).FullName,
                        path.Value.Settings.RelativeSourceHeaderPath
                    );

                    val += "'" + source + "' ";
                }

                val += "-out:'" + output + "' ";
            } else {
                throw new Exception("DocumentSettings.GetCompilerArguments: Should not get there!");
            }

            foreach(var f in thisCompiler.Item1.CustomCompilerFlags) {
                val += f + " ";
            }

            return val;
        }

        /// <summary>
        /// Gets the command line arguments necessary for archiving the provided intermediate compilation file into the requested lib.
        /// </summary>
        /// <returns>The arguments for the archiver invocation.</returns>
        /// <param name="intermediatePath">Intermediate path.</param>
        /// <param name="libPath">Lib path.</param>
        /// <param name="compiler">An optional compiler toolchain indicator.</param>
        public string GetArchiverArguments(string intermediatePath, string libPath, Compiler.Toolchain compiler)
        {
            var thisCompiler = MatchedCompilerInfo;
            string val = "";

            if(Language == Language.C || Language == Language.Cpp || Language == Language.Python) {
                if(_outputType == OutputType.DynamicLib) {
                    val += "-shared ";
                    if(compiler == Compiler.Toolchain.Clang) {
                        if(Configuration.RunningPlatform == Platform.Mac) {
                            val += "-undefined dynamic_lookup -flat_namespace ";
                        } else {
                            val += "-fPIC -undefined dynamic_lookup -flat_namespace ";
                        }
                    } else if(compiler == Compiler.Toolchain.GCC) {
                        val += "-fPIC ";
                    }

                    val += GetCompilerFlagsForPaths(thisCompiler, hasLibs: true, hasHeaders: false);

                    foreach(var path in UniqueDocumentsPaths) {
                        var lib = System.IO.Path.Combine(
                            System.IO.Directory.GetParent(path.Key).FullName,
                            path.Value.Settings.RelativeLibBaseName + path.Value.Settings.IntermediateExtension
                        );

                        val += "'" + lib + "' ";
                    }
                    val += "'" + intermediatePath + "' ";

                    // Need to provide libs after all object files to accomodate for linker dependencies
                    foreach(var l in thisCompiler.Item1.Libs) {
                        val += "-l'" + l + "' ";
                    }
                    if(Language == Language.Python) {
                        var runtime = Configuration.GetPythonRuntimeInfo(AdditionalLanguageInfo);
                        if(runtime.Item1) {
                            var version = runtime.Item2 + runtime.Item3;
                            val += string.Format("-Wl,--export-dynamic -l'python{0}' ", version);
                        }
                    }

                    val += "-o '" + libPath + "' ";

                    foreach(var f in thisCompiler.Item1.CustomLinkerFlags) {
                        val += f + " ";
                    }
                } else {
                    val += "rcs " + libPath + " " + intermediatePath + " ";
                    foreach(var path in UniqueDocumentsPaths) {
                        var lib = System.IO.Path.Combine(
                            System.IO.Directory.GetParent(path.Key).FullName,
                            path.Value.Settings.RelativeLibBaseName + path.Value.Settings.IntermediateExtension
                        );

                        val += "'" + lib + "' ";
                    }
                }
            } else if(Language == Language.CSharp) {
                return null;
            } else {
                throw new Exception("DocumentSettings.GetArchiverArguments: Should not get there!");
            }

            return val;
        }

        /// <summary>
        /// Recursively gets a list of external documents to compile, uniqued by their path.
        /// </summary>
        /// <value>The unique documents paths.</value>
        Dictionary<string, Document> UniqueDocumentsPaths {
            get {
                var allPaths = new Dictionary<string, Document>();
                foreach(var pn in _doc.FirstLevelExternalPetriNets) {
                    if(pn.IsReachable) {
                        var paths = pn.ExternalDocument.AllExternalReachablePaths;
                        foreach(var p in paths) {
                            allPaths[p.Key] = p.Value;
                        }
                        allPaths[pn.ExternalDocument.Path] = pn.ExternalDocument;
                    }
                }

                return allPaths;
            }
        }

        /// <summary>
        /// Returns the compiler flag to specify the language of a source file.
        /// </summary>
        /// <returns>The compiler flag.</returns>
        /// <param name="language">Language.</param>
        string CompilerFlagForLanguage(Language language)
        {
            switch(language) {
            case Language.C:
                return "-x c";
            case Language.Python:
                return null;
            case Language.Cpp:
                return "-x c++";
            }

            throw new Exception("DocumentSettings.CompilerFlagForLanguage: Should not get there!");
        }

        /// <summary>
        /// Returns the list of library and include paths in a string formatted for passing as command line arguments to the compiler.
        /// </summary>
        /// <returns>The paths to be passed to the compiler.</returns>
        /// <param name="compilerInfo">The compiler info.</param>
        /// <param name="hasLibs">Whether to include lib search paths in the flags.</param>
        /// <param name="hasHeaders">Whether to include headers search paths in the flags.</param>
        string GetCompilerFlagsForPaths(Tuple<CompilerInfo, Document> compilerInfo, bool hasHeaders, bool hasLibs)
        {
            string val = "";
            if(hasLibs) {
                foreach(var i in compilerInfo.Item1.LibPaths) {
                    var path = compilerInfo.Item2.GetAbsoluteFromRelativeToDoc(i.Item1);

                    // Recursive?
                    if(i.Item2) {
                        var directories = System.IO.Directory.EnumerateDirectories(
                            path,
                            "*",
                            System.IO.SearchOption.AllDirectories
                        );
                        foreach(var dir in directories) {
                            if(Language == Language.C || Language == Language.Cpp || Language == Language.Python) {
                                val += "-L'" + dir + "' ";
                            } else if(Language == Language.CSharp) {
                                val += "-lib:'" + dir + "' ";
                            }
                        }
                    }
                    if(Language == Language.C || Language == Language.Cpp || Language == Language.Python) {
                        val += "-L'" + path + "' ";
                    } else if(Language == Language.CSharp) {
                        val += "-lib:'" + path + "' ";
                    }
                }
            }

            if(hasHeaders) {
                if(Language == Language.C || Language == Language.Cpp || Language == Language.Python) {
                    var notFound = new List<string>();
                    foreach(var i in compilerInfo.Item1.IncludePaths) {
                        var path = compilerInfo.Item2.GetAbsoluteFromRelativeToDoc(i.Item1);
                        if(System.IO.Directory.Exists(path)) {
                            // Recursive?
                            if(i.Item2) {
                                var directories = System.IO.Directory.EnumerateDirectories(
                                    path,
                                    "*",
                                    System.IO.SearchOption.AllDirectories
                                );
                                foreach(var dir in directories) {
                                    // Do not add dotted files
                                    if(!dir.StartsWithInv(".")) {
                                        val += "-I'" + dir + "' ";
                                    }
                                }
                            }
                        } else {
                            notFound.Add(i.Item1);
                        }
                        val += "-I'" + i.Item1 + "' ";
                    }

                    if(notFound.Count > 0) {
                        _doc.NotifyWarning(Configuration.GetLocalized("Unable to find the folowing include directories: {0}",
                                                                      string.Join("\n", notFound)));
                    }
                }
            }

            return val;
        }

        OutputType _outputType;
        Document _doc;
        string _name;
    }
}
