//
//  RenderView.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2014-12-09.
//

using Petri.Model;

namespace Petri.Application
{
    /// <summary>
    /// A petri view that renders to a PDF file.
    /// </summary>
    public class RenderView : PetriView
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.RenderView"/> class.
        /// </summary>
        /// <param name="doc">Document.</param>
        /// <param name="includeBackground">If <c>false</c>, the PDF document's pages will have a transparent background.</param>
        public RenderView(Document doc, bool includeBackground) : base(new PetriNetProvider(doc))
        {
            EntityDraw = new RenderEntityDraw();

            DrawBackground = includeBackground;
        }

        /// <summary>
        /// Render the petri net as a PDF file saved in the specified location.
        /// </summary>
        /// <param name="exportPath">The export path.</param>
        public void Render(string exportPath)
        {
            using(var surf = new Cairo.PdfSurface(exportPath, 0, 0)) {
                using(var context = new Cairo.Context(surf)) {
                    RenderPetriNet(surf, context, CurrentPetriNet);
                }

                surf.Finish();
            }
        }

        /// <summary>
        /// Recursive method rendering the given petri net and its child petri nets, if any.
        /// </summary>
        /// <param name="surface">The PDF surface.</param>
        /// <param name="context">The drawing context.</param>
        /// <param name="petriNet">Petri net.</param>
        void RenderPetriNet(Cairo.PdfSurface surface,
                            Cairo.Context context,
                            PetriNet petriNet)
        {
            RenderInternal(context, petriNet);
            surface.SetSize(petriNet.Size.X, petriNet.Size.Y);
            RenderInternal(context, petriNet);

            context.ShowPage();
            foreach(State s in petriNet.States) {
                if(s is PetriNet && !(s is ExternalInnerPetriNet && ((ExternalInnerPetriNet)s).ExternalDocument is InvalidExternalDocument)) {
                    PushPetriNet((PetriNet)s);
                    RenderPetriNet(surface, context, (PetriNet)s);
                    PopPetriNet();
                }
            }
        }

        /// <summary>
        /// Gets the offset that will make a position scroll-independent.
        /// </summary>
        /// <value>The fixed offset.</value>
        protected override Vector FixedDrawingOffset {
            get {
                return new Vector(0, 0);
            }
        }

        /// <summary>
        /// Gets the size of the drawing containing the petri net.
        /// </summary>
        /// <returns>The extents of the drawing of the petri net.</returns>
        /// <param name="petriNet">Petri net.</param>
        protected override Vector GetExtents(PetriNet petriNet)
        {
            return new Vector(petriNet.Size);
        }
    }
}

