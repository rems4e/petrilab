//
//  PetriView.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2014-11-13.
//

using System;
using System.Collections.Generic;

using Cairo;

using Petri.Model;

namespace Petri.Application
{
    /// <summary>
    /// This class is responsible for drawing a petri net.
    /// </summary>
    public abstract class PetriView
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.PetriView"/> class.
        /// </summary>
        /// <param name="provider">The petri net provider.</param>
        protected PetriView(PetriNetProvider provider)
        {
            _provider = provider;
            DoPush(PetriNet);

            Zoom = 1.0f;
            DrawBackground = true;
        }

        /// <summary>
        /// Gets the petri net.
        /// </summary>
        /// <value>The petri net.</value>
        protected RootPetriNet PetriNet {
            get {
                return _provider.PetriNet;
            }
        }

        /// <summary>
        /// Gets or sets the object responsible for the actual petri net entities drawing.
        /// </summary>
        /// <value>The entity draw.</value>
        protected EntityDraw EntityDraw {
            get;
            set;
        }

        /// <summary>
        /// Gets the offset that will make a position scroll-independent.
        /// </summary>
        /// <value>The fixed offset.</value>
        protected abstract Vector FixedDrawingOffset {
            get;
        }

        /// <summary>
        /// Gets the path offset's to the upper left corner of the view.
        /// </summary>
        /// <value>The path offset.</value>
        protected double PathOffset {
            get {
                return 15;
            }
        }

        /// <summary>
        /// Gets the size of the drawing containing the petri net.
        /// </summary>
        /// <returns>The extents of the drawing of the petri net.</returns>
        /// <param name="petriNet">Petri net.</param>
        protected abstract Vector GetExtents(PetriNet petriNet);

        /// <summary>
        /// Performs the rendering of the entities of the petri net, and others item like the path.
        /// </summary>
        /// <returns>The minimal size required to fully draw the petri net.</returns>
        /// <param name="context">The drawing context.</param>
        /// <param name="petriNet">The petri net to draw.</param>
        protected virtual Vector RenderInternal(Context context, PetriNet petriNet)
        {
            if(_pathSeparatorWidth < 0) {
                context.SelectFontFace("Arial", FontSlant.Normal, FontWeight.Normal);
                context.SetFontSize(16);
                _pathSeparatorWidth = context.TextExtents(" / ").Width;
                _pathMaxHeight = context.TextExtents("ilL_pqâ/").Height;
            }
            {
                int i = _parentHierarchy.Count - 1;
                context.SelectFontFace("Arial", FontSlant.Normal, FontWeight.Normal);
                context.SetFontSize(16);
                while(i >= 0 && _parentHierarchy[i].Extents == null) {
                    _parentHierarchy[i].Extents = context.TextExtents(_parentHierarchy[i].Text);
                    --i;
                }
            }
            EntityDraw.BeginRendering();
            var extents = GetExtents(petriNet);

            context.LineWidth = 4;
            context.MoveTo(0, 0);
            context.LineTo(extents.X, 0);
            context.LineTo(extents.X, extents.Y);
            context.LineTo(0, extents.Y);
            context.LineTo(0, 0);

            context.SetSourceRGBA(1, 1, 1, 1);
            EntityDraw.InitContextForBackground(context);
            if(DrawBackground) {
                context.Fill();
            }

            double minX = 0, minY = 0;

            foreach(var t in petriNet.Transitions) {
                minX = Math.Max(minX, t.Position.X + t.Width / 2);
                minY = Math.Max(minY, t.Position.Y + t.Height / 2);

                EntityDraw.Draw(t, context);
            }

            foreach(var s in petriNet.States) {
                minX = Math.Max(minX, s.Position.X + s.Radius);
                minY = Math.Max(minY, s.Position.Y + s.Radius);

                EntityDraw.Draw(s, context);
            }

            foreach(var c in petriNet.Comments) {
                minX = Math.Max(minX, c.Position.X + c.Size.X / 2);
                minY = Math.Max(minY, c.Position.Y + c.Size.Y / 2);

                EntityDraw.Draw(c, context);
            }

            {
                PetriNet petri = CurrentPetriNet;

                var pathPosition = FixedDrawingOffset;
                var currX = PathOffset - 5 + pathPosition.X;
                for(int i = _parentHierarchy.Count - 1; i >= 0; --i) {
                    context.SetSourceColor(_colorMapping[_parentHierarchy[i].Color % _colorMapping.Count]);
                    TextExtents exti = _parentHierarchy[i].Extents.Value;
                    context.Rectangle(currX / Zoom,
                                      (pathPosition.Y + 10) / Zoom,
                                      (exti.Width + 10) / Zoom,
                                      (_pathMaxHeight + 12) / Zoom);
                    context.Fill();
                }

                TextExtents ext = _parentHierarchy[_parentHierarchy.Count - 1].Extents.Value;
                context.MoveTo((pathPosition.X + PathOffset - ext.XBearing) / Zoom,
                               (pathPosition.Y + PathOffset - ext.YBearing + 2) / Zoom);

                context.SelectFontFace("Arial", FontSlant.Normal, FontWeight.Normal);
                context.SetFontSize(16 / Zoom);
                context.SetSourceRGBA(0.0, 0.3, 0.1, 1);

                context.TextPath(_parentHierarchy[_parentHierarchy.Count - 1].Text);
                context.Fill();
            }

            SpecializedDrawing(context);

            context.LineWidth = 4 / Zoom;
            context.MoveTo(0, 0);
            context.LineTo(extents.X, 0);
            context.LineTo(extents.X, extents.Y);
            context.LineTo(0, extents.Y);
            context.LineTo(0, 0);
            context.SetSourceRGBA(0.7, 0.7, 0.7, 1);
            context.Stroke();

            minX += 50;
            minY += 50;

            petriNet.Size = new Vector(minX, minY) * Zoom;

            return new Vector(petriNet.Size);
        }

        /// <summary>
        /// Gives a chance to draw custom items in the petri view. The drawing will be made after drawing every entity.
        /// </summary>
        /// <param name="context">The drawing context.</param>
        protected virtual void SpecializedDrawing(Cairo.Context context)
        {
        }

        /// <summary>
        /// If <c>true</c>, a white background is drawn behind the entities. If <c>false</c>, the background will be transparent.
        /// </summary>
        /// <value>If <c>true</c>, the background is drawn.</value>
        public bool DrawBackground {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the zoom level of the petri net in the view.
        /// </summary>
        /// <value>The zoom level.</value>
        public float Zoom {
            get;
            set;
        }

        /// <summary>
        /// Gets the current petri net that is being drawn, which may be the root petri net or one of its children.
        /// </summary>
        /// <value>The current petri net.</value>
        public PetriNet CurrentPetriNet {
            get {
                return _parentHierarchy.Count > 0
                           ? _parentHierarchy[_parentHierarchy.Count - 1].PetriNet
                           : PetriNet;
            }
        }

        /// <summary>
        /// Adds a petri net to the current petri nets path, making it the current petri net <see cref="CurrentPetriNet"/>.
        /// </summary>
        /// <param name="pn">The petri net to push.</param>
        public virtual void PushPetriNet(PetriNet pn)
        {
            DoPush(pn);
        }

        /// <summary>
        /// Removes the current petri net <see cref="CurrentPetriNet"/> from the current petri nets path,
        /// making its predecesssor the current petri net.
        /// </summary>
        public virtual void PopPetriNet()
        {
            _parentHierarchy.RemoveAt(_parentHierarchy.Count - 1);
        }

        /// <summary>
        /// Sets the current petri net to the document's root petri net.
        /// </summary>
        public void GoToRoot()
        {
            var newRoot = _provider.PetriNet;
            while(_parentHierarchy.Count > 0) {
                if(_parentHierarchy.Count == 1 && _parentHierarchy[0].PetriNet == newRoot) {
                    return;
                }
                PopPetriNet();
            }
            PushPetriNet(newRoot);
        }

        /// <summary>
        /// Performs the actual push operation. <see cref="PushPetriNet"/>.
        /// </summary>
        /// <param name="pn">The petri net to push.</param>
        void DoPush(PetriNet pn)
        {
            string previous = _parentHierarchy.Count == 0 ? "" : _parentHierarchy[_parentHierarchy.Count - 1].Text;
            string sep = _parentHierarchy.Count == 0 ? "" : " / ";
            var name = pn.Name == "" ? "(   )" : pn.Name;

            var pStruct = new PathComponent(pn, previous + sep + name, _parentHierarchy.Count == 0 ? 0 : _parentHierarchy[_parentHierarchy.Count - 1].Color + (pn is ExternalInnerPetriNet ? 1 : 0));
            _parentHierarchy.Add(pStruct);
        }

        /// <summary>
        /// A struct that contains a component of the path of the current petri net.
        /// </summary>
        public class PathComponent
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="T:Petri.Application.PetriView.PathComponent"/> class.
            /// </summary>
            /// <param name="pn">The petri net of this component.</param>
            /// <param name="text">The text of this component.</param>
            /// <param name="color">The background color of this component.</param>
            public PathComponent(PetriNet pn, string text, int color)
            {
                PetriNet = pn;
                Text = text;
                Extents = null;
                Color = color;
            }

            /// <summary>
            /// The petri net.
            /// </summary>
            public PetriNet PetriNet {
                get;
                private set;
            }

            /// <summary>
            /// The size of the rendered name of the petri net.
            /// </summary>
            public TextExtents? Extents {
                get;
                set;
            }

            /// <summary>
            /// The text.
            /// </summary>
            public string Text {
                get;
                private set;
            }

            /// <summary>
            /// Gets the background color of this component.
            /// </summary>
            /// <value>The color.</value>
            public int Color {
                get;
                private set;
            }
        }

        /// <summary>
        /// The petri net provider.
        /// </summary>
        protected PetriNetProvider _provider;

        /// <summary>
        /// The path separator width.
        /// </summary>
        protected double _pathSeparatorWidth = -1;

        /// <summary>
        /// The max height of the current petri net's hierarchy text.
        /// </summary>
        protected double _pathMaxHeight = -1;

        /// <summary>
        /// The ancestry of the current petri net.
        /// </summary>
        public List<PathComponent> _parentHierarchy = new List<PathComponent>();

        static List<Color> _colorMapping = new List<Color> {
            new Color(0.9, 0.9, 0.9),
            new Color(0.6, 0.6, 1.0),
            new Color(1.0, 0.6, 0.6),
            new Color(0.6, 1.0, 0.6),
            new Color(1.0, 1.0, 0.6),
        };
    }
}
