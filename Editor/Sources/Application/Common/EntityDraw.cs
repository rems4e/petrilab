//
//  EntityDraw.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2014-12-06.
//

using System;
using System.Collections.Generic;

using Cairo;
using Gdk;

using Petri.Model;

using Point = Petri.Model.Point;

namespace Petri.Application
{
    /// <summary>
    /// The class that is responsible for drawing a petri net's individual parts (actions, transitions, comments…).
    /// </summary>
    public abstract class EntityDraw
    {
        /// <summary>
        /// Initializes the <see cref="T:Petri.Application.EntityDraw"/> class.
        /// </summary>
        static EntityDraw()
        {
            _chainBuf = Pixbuf.LoadFromResource("chain");
        }

        /// <summary>
        /// Begins the rendering.
        /// </summary>
        public virtual void BeginRendering()
        {
            _controlPoints.Clear();
        }

        /// <summary>
        /// Inits the context for the canva's background.
        /// </summary>
        /// <param name="context">Context.</param>
        public virtual void InitContextForBackground(Context context)
        {
            context.SetSourceRGBA(1, 1, 1, 1);
        }

        /// <summary>
        /// Draw the specified comment.
        /// </summary>
        /// <param name="e">The comment to draw.</param>
        /// <param name="context">The graphical context.</param>
        public void Draw(Comment e, Context context)
        {
            InitContextForBackground(e, context);
            DrawBackground(e, context);

            InitContextForBorder(e, context);
            DrawBorder(e, context);

            InitContextForName(e, context);
            DrawText(e, context);
        }

        /// <summary>
        /// Draw the specified state.
        /// </summary>
        /// <param name="e">The state to draw.</param>
        /// <param name="context">The graphical context.</param>
        public void Draw(State e, Context context)
        {
            InitContextForBackground(e, context);
            DrawBackground(e, context);

            InitContextForBorder(e, context);
            DrawBorder(e, context);

            InitContextForName(e, context);
            DrawName(e, context);
            InitContextForTokens(e, context);
            DrawTokens(e, context);
        }

        /// <summary>
        /// Draw the specified transition.
        /// </summary>
        /// <param name="e">The transition to draw.</param>
        /// <param name="context">The graphical context.</param>
        public void Draw(Transition e, Context context)
        {
            InitContextForLine(e, context);
            DrawLine(e, context);

            InitContextForBorder(e, context);
            DrawBorder(e, context);

            InitContextForBackground(e, context);
            DrawBackground(e, context);

            InitContextForText(e, context);
            DrawText(e, context);
        }

        /// <summary>
        /// Inits the context for the comment's background.
        /// </summary>
        /// <param name="c">The comment.</param>
        /// <param name="context">Context.</param>
        protected virtual void InitContextForBackground(Comment c, Context context)
        {
            context.SetSourceRGBA(c.Color.R, c.Color.G, c.Color.B, c.Color.A);
        }

        /// <summary>
        /// Draws the background of the comment.
        /// </summary>
        /// <param name="c">The comment.</param>
        /// <param name="context">Context.</param>
        protected virtual void DrawBackground(Comment c, Context context)
        {
            var screen = Gdk.Screen.Default;
            if(screen != null) {
                Pango.Context pangoContext = Gdk.PangoHelper.ContextGet();
                _commentsLayout = new Pango.Layout(pangoContext);

                _commentsLayout.FontDescription = new Pango.FontDescription();
                _commentsLayout.FontDescription.Family = "Arial";
                _commentsLayout.FontDescription.Size = Pango.Units.FromPixels(12);

                _commentsLayout.SetText(c.Name);

                _commentsLayout.Width = (int)((c.Size.X - 13) * Pango.Scale.PangoScale);
                _commentsLayout.Justify = true;
                int width;
                int height;
                _commentsLayout.GetPixelSize(out width, out height);
                c.Size = new Vector(Math.Max(c.Size.X, width + 13), height + 10);
            }

            var point = new Point(c.Position);
            point.X -= c.Size.X / 2 + context.LineWidth / 2;
            point.Y -= c.Size.Y / 2;
            context.MoveTo(point);
            point.X += c.Size.X;
            context.LineTo(point);
            point.Y += c.Size.Y;
            context.LineTo(point);
            point.X -= c.Size.X;
            context.LineTo(point);
            point.Y -= c.Size.Y;
            context.LineTo(point);

            context.FillPreserve();
        }

        /// <summary>
        /// Inits the context for the comment's border.
        /// </summary>
        /// <param name="c">The comment.</param>
        /// <param name="context">Context.</param>
        protected virtual void InitContextForBorder(Comment c, Context context)
        {
            context.LineWidth = 1;
            context.SetSourceRGBA(c.Color.R * 0.8, c.Color.G * 0.6, c.Color.B * 0.4, c.Color.A);
        }

        /// <summary>
        /// Draws the border of the comment.
        /// </summary>
        /// <param name="c">The comment.</param>
        /// <param name="context">Context.</param>
        protected virtual void DrawBorder(Comment c, Context context)
        {
            context.Stroke();
        }

        /// <summary>
        /// Inits the context for the comment's name.
        /// </summary>
        /// <param name="c">The comment.</param>
        /// <param name="context">Context.</param>
        protected virtual void InitContextForName(Comment c, Context context)
        {
            context.SetSourceRGBA(0, 0, 0, 1);
        }

        /// <summary>
        /// Draws the name of the comment.
        /// </summary>
        /// <param name="c">The comment.</param>
        /// <param name="context">Context.</param>
        protected virtual void DrawText(Comment c, Context context)
        {
            context.MoveTo(c.Position - c.Size / 2 + new Vector(5));
            if(_commentsLayout == null) {
                var xpos = context.CurrentPoint.X;
                var ypos = context.CurrentPoint.Y;
                context.MoveTo(xpos, ypos + 5);
                context.ShowText("Comments' text cannot be drawn");
                context.MoveTo(xpos, ypos + 15);
                context.ShowText("in CLI mode.");
            } else {
                Pango.CairoHelper.ShowLayout(context, _commentsLayout);
            }
            _commentsLayout = null;
        }

        /// <summary>
        /// Inits the context for the state's background.
        /// </summary>
        /// <param name="s">The state.</param>
        /// <param name="context">Context.</param>
        protected virtual void InitContextForBackground(State s, Context context)
        {
            context.SetSourceRGBA(1, 1, 1, 1);
        }

        /// <summary>
        /// Draws the background of the state.
        /// </summary>
        /// <param name="s">The state.</param>
        /// <param name="context">Context.</param>
        protected virtual void DrawBackground(State s, Context context)
        {
            context.Arc(s.Position.X, s.Position.Y, s.Radius, 0, 2 * Math.PI);

            context.FillPreserve();
        }

        /// <summary>
        /// Inits the context for the state's border.
        /// </summary>
        /// <param name="s">The state.</param>
        /// <param name="context">Context.</param>
        protected virtual void InitContextForBorder(State s, Context context)
        {
            context.LineWidth = 3;
            context.SetSourceRGBA(0, 0, 0, 1);
        }

        /// <summary>
        /// Draws the border of the state.
        /// </summary>
        /// <param name="s">The state.</param>
        /// <param name="context">Context.</param>
        protected virtual void DrawBorder(State s, Context context)
        {
            if(s.IsStartState) {
                context.StrokePreserve();

                context.MoveTo(s.Position.X + s.Radius - 5, s.Position.Y);
                context.Arc(s.Position.X, s.Position.Y, s.Radius - 5, 0, 2 * Math.PI);
            }

            context.Stroke();
            if(s is ExternalInnerPetriNet) {
                context.Save();
                var sf = 10.0;
                context.Scale(1 / sf, 1 / sf);
                CairoHelper.SetSourcePixbuf(context, _chainBuf, s.Position.X * sf - 11 * sf, (s.Position.Y - 16 + s.Radius) * sf);
                context.Paint();
                context.Restore();
            }
        }

        /// <summary>
        /// Inits the context for the state's name.
        /// </summary>
        /// <param name="s">The state.</param>
        /// <param name="context">Context.</param>
        protected virtual void InitContextForName(State s, Context context)
        {
            context.SetSourceRGBA(0, 0, 0, 1);
            context.SelectFontFace("Arial", FontSlant.Normal, FontWeight.Normal);
            context.SetFontSize(12);
        }

        /// <summary>
        /// Draws the name of the state.
        /// </summary>
        /// <param name="s">The state.</param>
        /// <param name="context">Context.</param>
        protected virtual void DrawName(State s, Context context)
        {
            int tokenShift = ShouldDisplayTokens(s) ? -3 : 0;

            string val = s.Name;
            TextExtents te = context.TextExtents(val);
            context.MoveTo(s.Position.X - te.Width / 2 - te.XBearing,
                           s.Position.Y - te.Height / 2 - te.YBearing + tokenShift);
            context.TextPath(val);
            context.Fill();
        }

        /// <summary>
        /// Inits the context for the state's tokens.
        /// </summary>
        /// <param name="s">The state.</param>
        /// <param name="context">Context.</param>
        protected virtual void InitContextForTokens(State s, Context context)
        {
            context.SetFontSize(10);
        }

        /// <summary>
        /// Gets the tokens string for the given state.
        /// </summary>
        /// <returns>The tokens string.</returns>
        /// <param name="s">The state.</param>
        protected virtual string GetTokensString(State s)
        {
            return s.RequiredTokens.ToString() + " tok";
        }

        /// <summary>
        /// Whether we should display the token count on a state.
        /// </summary>
        /// <returns><c>true</c>, the tokens should be displayed, <c>false</c> otherwise.</returns>
        /// <param name="s">The state.</param>
        bool ShouldDisplayTokens(State s)
        {
            return s.RequiredTokens > 1;
        }

        /// <summary>
        /// Draws the tokens of the state.
        /// </summary>
        /// <param name="s">The state.</param>
        /// <param name="context">Context.</param>
        protected virtual void DrawTokens(State s, Context context)
        {
            if(ShouldDisplayTokens(s)) {
                string tokNum = GetTokensString(s);
                TextExtents te = context.TextExtents(tokNum);
                context.MoveTo(s.Position.X - te.Width / 2 - te.XBearing,
                               s.Position.Y - te.Height / 2 - te.YBearing + 5);
                context.TextPath(tokNum);
                context.Fill();
            }
        }

        /// <summary>
        /// Gets the default arrow scale.
        /// </summary>
        /// <value>The default arrow scale.</value>
        public static double DefaultArrowScale {
            get {
                return 12;
            }
        }

        /// <summary>
        /// Gets the arrow scale for the transition.
        /// </summary>
        /// <returns>The arrow scale.</returns>
        /// <param name="t">The transition.</param>
        protected virtual double GetArrowScale(Transition t)
        {
            return DefaultArrowScale;
        }

        /// <summary>
        /// Gets the shift between the starting and ending points of a transition that comes from and goes to the same node.
        /// </summary>
        /// <returns>The shift to use with looping transitions.</returns>
        protected static double TransitionToSameShift {
            get {
                return 0.4;
            }
        }

        /// <summary>
        /// Inits the context for the transition's line.
        /// </summary>
        /// <param name="t">The transition.</param>
        /// <param name="context">Context.</param>
        protected virtual void InitContextForLine(Transition t, Context context)
        {
            context.SetSourceRGBA(0.1, 0.6, 1, 1);
            context.LineWidth = 2;
        }

        /// <summary>
        /// Draws the line of a transition.
        /// </summary>
        /// <returns>The Bézier control points of the curve.</returns>
        /// <param name="context">Context.</param>
        /// <param name="origin">Transition's origin.</param>
        /// <param name="destination">Transition's destination.</param>
        /// <param name="position">The position of the transition.</param>
        /// <param name="isSame">Wether the transition's ends are the same state.</param>
        public static ControlPoints DrawLine(Context context, Point origin, Point destination, Point position, bool isSame)
        {
            ControlPoints? cp;
            context.MoveTo(origin);
            if(isSame || (origin - destination).Norm > 1e-3) {
                var middle = new Point((origin.X + destination.X) / 2, (origin.Y + destination.Y) / 2);
                var vec = position - middle;

                var controlMiddle = middle + vec / 0.75;
                var dir = destination - origin;
                if(isSame) {
                    var norm = vec.Normalized;
                    dir = new Vector(-norm.Y, norm.X) * 180;
                }

                var c1 = controlMiddle - dir / 4;
                var c2 = controlMiddle + dir / 4;

                cp = new ControlPoints(c1, c2);

                context.CurveTo(c1, c2, destination);
            } else {
                cp = new ControlPoints(origin, origin);

                context.LineTo(position);
            }
            context.Stroke();

            return cp.Value;
        }

        /// <summary>
        /// Draws the line of the transition.
        /// </summary>
        /// <param name="t">The transition.</param>
        /// <param name="context">Context.</param>
        protected virtual void DrawLine(Transition t, Context context)
        {
            double arrowScale = GetArrowScale(t);

            double radB = t.Before.Radius;
            double radA = t.After.Radius;

            var origin = TransitionOrigin(t);
            if((t.After.Position - t.Position).Norm > radB) {
                var destination = TransitionDestination(t);
                _controlPoints[t] = DrawLine(context, origin, destination, t.Position, t.Before == t.After);

                var pos = BézierAt(origin, destination, _controlPoints[t], arrowScale, -1);
                var direction = (destination - pos).Normalized;
                EntityDraw.DrawArrow(context, direction, destination, arrowScale);
            } else {
                _controlPoints[t] = new ControlPoints(origin, origin);
            }
        }

        /// <summary>
        /// Gets the origin of the transition's line.
        /// </summary>
        /// <returns>The origin.</returns>
        /// <param name="t">The transition.</param>
        public static Point TransitionOrigin(Transition t)
        {
            return TransitionOrigin(t.Position, t.Before, t.Before == t.After);
        }

        /// <summary>
        /// Gets the origin of the transition's line.
        /// </summary>
        /// <returns>The origin.</returns>
        /// <param name="tPos">Position of the transition.</param>
        /// <param name="origin">State before the transition.</param>
        /// <param name="isSame">Wether the transition's ends are the same state.</param>
        public static Point TransitionOrigin(Point tPos, State origin, bool isSame)
        {
            var direction = (tPos - origin.Position).Normalized;
            var result = origin.Position + direction * origin.Radius;

            if(isSame) {
                var alpha = -Math.Atan2(tPos.Y - origin.Position.Y, tPos.X - origin.Position.X);
                var angle = TransitionToSameShift;
                result.X += origin.Radius * (+Math.Cos(+angle + alpha) - Math.Cos(alpha));
                result.Y += origin.Radius * (-Math.Sin(+angle + alpha) + Math.Sin(alpha));
            }

            return result;
        }

        /// <summary>
        /// Gets the destination of the transition's line.
        /// </summary>
        /// <returns>The destination.</returns>
        /// <param name="t">The transition.</param>
        public static Point TransitionDestination(Transition t)
        {
            return TransitionDestination(t.Position, t.After, t.Before == t.After);
        }

        /// <summary>
        /// Gets the destination of the transition's line.
        /// </summary>
        /// <returns>The destination.</returns>
        /// <param name="tPos">Position of the transition.</param>
        /// <param name="destination">State after the transition.</param>
        /// <param name="isSame">Wether the transition's ends are the same state.</param>
        public static Point TransitionDestination(Point tPos, State destination, bool isSame)
        {
            var direction = (destination.Position - tPos).Normalized;
            var result = destination.Position - direction * destination.Radius;

            if(isSame) {
                var alpha = -Math.Atan2(tPos.Y - destination.Position.Y, tPos.X - destination.Position.X);
                var angle = TransitionToSameShift;
                result.X += destination.Radius * (+Math.Cos(-angle + alpha) - Math.Cos(alpha));
                result.Y += destination.Radius * (-Math.Sin(-angle + alpha) + Math.Sin(alpha));
            }

            return result;
        }

        /// <summary>
        /// Inits the context for the transition's border.
        /// </summary>
        /// <param name="t">The transition.</param>
        /// <param name="context">Context.</param>
        protected virtual void InitContextForBorder(Transition t, Context context)
        {

        }

        /// <summary>
        /// Draws the border of the transition.
        /// </summary>
        /// <param name="t">The transition.</param>
        /// <param name="context">Context.</param>
        protected virtual void DrawBorder(Transition t, Context context)
        {
            var point = new Point(t.Position);
            point.X -= t.Width / 2 + context.LineWidth / 2;
            point.Y -= t.Height / 2;
            context.MoveTo(point);
            point.X += t.Width;
            context.LineTo(point);
            point.Y += t.Height;
            context.LineTo(point);
            point.X -= t.Width;
            context.LineTo(point);
            point.Y -= t.Height + context.LineWidth / 2;
            context.LineTo(point);

            context.StrokePreserve();
        }

        /// <summary>
        /// Inits the context for the transition's background.
        /// </summary>
        /// <param name="t">The transition.</param>
        /// <param name="context">Context.</param>
        protected virtual void InitContextForBackground(Transition t, Context context)
        {
            context.SetSourceRGBA(1, 1, 1, 1);
        }

        /// <summary>
        /// Draws the background of the transition.
        /// </summary>
        /// <param name="t">The transition.</param>
        /// <param name="context">Context.</param>
        protected virtual void DrawBackground(Transition t, Context context)
        {
            context.Fill();
        }

        /// <summary>
        /// Inits the context for the transition's text.
        /// </summary>
        /// <param name="t">The transition.</param>
        /// <param name="context">Context.</param>
        protected virtual void InitContextForText(Transition t, Context context)
        {
            context.SetSourceRGBA(0.1, 0.6, 1, 1);
            context.SelectFontFace("Arial", FontSlant.Normal, FontWeight.Normal);
            context.SetFontSize(12);
        }

        /// <summary>
        /// Draws the text of the transition.
        /// </summary>
        /// <param name="t">The transition.</param>
        /// <param name="context">Context.</param>
        protected virtual void DrawText(Transition t, Context context)
        {
            string val = t.Name;
            TextExtents te = context.TextExtents(val);
            context.MoveTo(t.Position.X - te.Width / 2 - te.XBearing,
                           t.Position.Y - te.Height / 2 - te.YBearing);
            context.TextPath(val);
            context.Fill();
        }

        /// <summary>
        /// Draws a triangular arrow head.
        /// </summary>
        /// <param name="context">The graphic context.</param>
        /// <param name="direction">The direction of the arrow.</param>
        /// <param name="position">The position of the arriw.</param>
        /// <param name="scaleAlongAxis">Scale along axis.</param>
        public static void DrawArrow(Context context,
                                     Vector direction,
                                     Point position,
                                     double scaleAlongAxis)
        {
            double angle = 20 * Math.PI / 180;

            double sin = Math.Sin(angle);
            var normal = new Vector(-direction.Y, direction.X) * sin;

            direction *= scaleAlongAxis;
            normal *= scaleAlongAxis;

            context.MoveTo(position);
            context.LineTo(position - direction + normal);
            context.LineTo(position - direction - normal);

            context.Fill();
        }

        /// <summary>
        /// The control points of a cubic Bézier curve.
        /// </summary>
        public struct ControlPoints
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="T:Petri.Application.EntityDraw.ControlPoints"/> struct.
            /// </summary>
            /// <param name="c1">The first control point of the Bézier curve.</param>
            /// <param name="c2">The second control point of the Bézier curve.</param>
            public ControlPoints(Point c1, Point c2)
            {
                this.c1 = c1;
                this.c2 = c2;
            }

            /// <summary>
            /// The first control point of the Bézier curve.
            /// </summary>
            public Point c1;

            /// <summary>
            /// The second control point of the Bézier curve.
            /// </summary>
            public Point c2;
        }

        /// <summary>
        /// Computes a cubic Bézier curve's point.
        /// </summary>
        /// <returns>The point at position t.</returns>
        /// <param name="origin">The origin of the curve.</param>
        /// <param name="destination">The destination of the curve.</param>
        /// <param name="cp">The control points of the curve.</param>
        /// <param name="t">The position where to compute the curve, between 0 and 1.</param>
        protected static Point BézierFormula(Point origin, Point destination, ControlPoints cp, double t)
        {
            var s = 1 - t;
            var x = s * s * s * origin.X + 3 * s * s * t * cp.c1.X + 3 * s * t * t * cp.c2.X + t * t * t * destination.X;
            var y = s * s * s * origin.Y + 3 * s * s * t * cp.c1.Y + 3 * s * t * t * cp.c2.Y + t * t * t * destination.Y;
            return new Point(x, y);
        }

        /// <summary>
        /// Performs a binary search to find the position on a Bézier curve that is approximately at the given distance from the specified end.
        /// </summary>
        /// <returns>The resulting point.</returns>
        /// <param name="origin">The origin of the curve.</param>
        /// <param name="destination">The destination of the curve.</param>
        /// <param name="cp">The control points of the curve.</param>
        /// <param name="length">The distance from the specified end that is wanted.</param>
        /// <param name="dir">The "direction" where to search. Must be 1 when the requested end is the origin, -1 if the requested end is the destination.</param>
        public static Point BézierAt(Point origin, Point destination, ControlPoints cp, double length, int dir)
        {
            double t0 = 0;
            double t1 = 1;
            var middle = 0.5;
            Point target = dir == 1 ? origin : destination;
            Point result = target;
            int i = 0;
            while(i < 12) {
                ++i;
                result = BézierFormula(origin, destination, cp, middle);
                var norm = (result - target).Norm;
                if(dir * norm > dir * (length + 0.25)) {
                    t1 = middle;
                } else if(dir * norm < dir * (length - 0.25)) {
                    t0 = middle;
                } else {
                    break;
                }
                middle = (t0 + t1) / 2;
            }

            return result;
        }

        Pango.Layout _commentsLayout;

        /// <summary>
        /// The control points of the drawn transitions.
        /// </summary>
        protected Dictionary<Transition, ControlPoints> _controlPoints = new Dictionary<Transition, ControlPoints>();

        static Pixbuf _chainBuf;
    }
}
