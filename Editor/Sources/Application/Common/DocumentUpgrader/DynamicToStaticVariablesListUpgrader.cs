//
//  DynamicToStaticVariablesListUpgrader.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2017-01-31.
//

using System.Collections.Generic;
using System.Xml.Linq;

namespace Petri.Application.DocumentUpgrader
{
    /// <summary>
    /// An upgrader module that replaces the old function to print a single variable into with the
    /// new function that can print multiple variables at once.
    /// </summary>
    public class DynamicToStaticVariablesListUpgrader : DocumentUpgrader
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Application.PrintVariablesMonoToMultiUpgrader"/> class.
        /// </summary>
        /// <param name="root">Root.</param>
        /// <param name="apiVersion">The old API version.</param>
        public DynamicToStaticVariablesListUpgrader(XElement root, int apiVersion) : base(root, apiVersion)
        {
        }

        /// <summary>
        /// Upgrades the document.
        /// </summary>
        /// <returns><c>true</c>, if document was upgraded, <c>false</c> otherwise.</returns>
        public override bool UpgradeDocument()
        {
            var variables = new Dictionary<string, string>();
            var parameters = new HashSet<string>();
            foreach(var param in _root.Element("PetriNet").Elements("Parameters").Elements("Parameter")) {
                variables.Add(param.Attribute("Name").Value, param.Attribute("Value").Value);
                parameters.Add(param.Attribute("Name").Value);
            }

            foreach(var state in _root.Element("PetriNet").Descendants("States").Elements()) {
                if(state.Attribute("Function") != null) {
                    var expression = Code.Expression.CreateFromString(state.Attribute("Function").Value, Language);
                    foreach(var v in expression.GetVariables()) {
                        if(!variables.ContainsKey(v.MakeUserReadable())) {
                            variables.Add(v.MakeUserReadable(), "0");
                        }
                    }
                }
            }
            foreach(var transition in _root.Element("PetriNet").Descendants("Transitions").Elements()) {
                if(transition.Attribute("Condition") != null) {
                    var expression = Code.Expression.CreateFromString(transition.Attribute("Condition").Value, Language);
                    foreach(var v in expression.GetVariables()) {
                        if(!variables.ContainsKey(v.MakeUserReadable())) {
                            variables.Add(v.MakeUserReadable(), "0");
                        }
                    }
                }
            }
            foreach(var arg in _root.Element("PetriNet").Descendants("PetriNetRef").Descendants("Argument")) {
                if(arg.Attribute("Value") != null) {
                    var expression = Code.Expression.CreateFromString(arg.Attribute("Value").Value, Language);
                    foreach(var v in expression.GetVariables()) {
                        if(!variables.ContainsKey(v.MakeUserReadable())) {
                            variables.Add(v.MakeUserReadable(), "0");
                        }
                    }
                }
            }

            var xmlVar = new XElement("Variables");
            foreach(var v in variables) {
                var vv = new XElement("Variable");
                vv.SetAttributeValue("Name", v.Key);
                vv.SetAttributeValue("Value", v.Value);
                if(parameters.Contains(v.Key)) {
                    vv.SetAttributeValue("IsParameter", true);
                }
                xmlVar.Add(vv);
            }
            _root.Element("PetriNet").AddFirst(xmlVar);

            return variables.Count > 0;
        }
    }
}
