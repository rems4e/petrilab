//
//  ResultEnumWithTimeoutUpgrader.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2018-01-29.
//

using System.Xml.Linq;

namespace Petri.Application.DocumentUpgrader
{
    /// <summary>
    /// An upgrader module that adds a "TIMEOUT" enum member to the default action result enum.
    /// </summary>
    public class ResultEnumWithTimeoutUpgrader : DocumentUpgrader
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Application.DocumentUpgrader.ResultEnumWithTimeoutUpgrader"/> class.
        /// </summary>
        /// <param name="root">Root.</param>
        /// <param name="apiVersion">The old API version.</param>
        public ResultEnumWithTimeoutUpgrader(XElement root, int apiVersion) : base(root, apiVersion)
        {
        }

        /// <summary>
        /// Upgrades the document.
        /// </summary>
        /// <returns><c>true</c>, if document was upgraded, <c>false</c> otherwise.</returns>
        public override bool UpgradeDocument()
        {
            var oldDefaultEnum = new Code.Enum(Language, "ActionResult", new string[] { "OK", "NOK" });
            var settings = _root.Element("Settings");
            if(settings.Attribute("Enum") != null) {
                var currentEnum = new Code.Enum(Language, settings.Attribute("Enum").Value);
                if(currentEnum.Equals(oldDefaultEnum)) {
                    currentEnum = DocumentSettings.GetDefaultEnumForLanguage(Language);
                    settings.SetAttributeValue("Enum", currentEnum.ToString());

                    return true;
                }
            }

            return false;
        }
    }
}
