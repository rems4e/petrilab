//
//  ResultEnumWithTimeoutUpgrader.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2018-01-29.
//

using System.Xml.Linq;

namespace Petri.Application.DocumentUpgrader
{
    /// <summary>
    /// An upgrader module that moves "Window", "Headers" and "Macros" XML elements to the "Settings" element.
    /// </summary>
    public class ExtraXmlInSettingsUpgrader : DocumentUpgrader
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Application.DocumentUpgrader.ExtraXmlInSettingsUpgrader"/> class.
        /// </summary>
        /// <param name="root">Root.</param>
        /// <param name="apiVersion">The old API version.</param>
        public ExtraXmlInSettingsUpgrader(XElement root, int apiVersion) : base(root, apiVersion)
        {
        }

        /// <summary>
        /// Upgrades the document.
        /// </summary>
        /// <returns><c>true</c>, if document was upgraded, <c>false</c> otherwise.</returns>
        public override bool UpgradeDocument()
        {
            var settings = _root.Element("Settings");
            var window = _root.Element("Window");
            var headers = _root.Element("Headers");
            var macros = _root.Element("Macros");
            if(window != null) {
                window.Remove();
                settings.AddFirst(window);
            }
            if(headers != null) {
                headers.Remove();
                settings.AddFirst(headers);
            }

            if(macros != null) {
                macros.Remove();
                settings.AddFirst(macros);
            }

            return window != null || headers != null || macros != null;
        }
    }
}
