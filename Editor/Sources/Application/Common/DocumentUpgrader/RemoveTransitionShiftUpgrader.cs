//
//  DynamicToStaticVariablesListUpgrader.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2017-01-31.
//

using System.Collections.Generic;
using System.Xml.Linq;

namespace Petri.Application.DocumentUpgrader
{
    /// <summary>
    /// An upgrader module that removes Shit and ShiftAmplitude XML attributes.
    /// </summary>
    public class RemoveTransitionShiftUpgrader : DocumentUpgrader
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Application.RemoveTransitionShiftUpgrader"/> class.
        /// </summary>
        /// <param name="root">Root.</param>
        /// <param name="apiVersion">The old API version.</param>
        public RemoveTransitionShiftUpgrader(XElement root, int apiVersion) : base(root, apiVersion)
        {
        }

        /// <summary>
        /// Upgrades the document.
        /// </summary>
        /// <returns><c>true</c>, if document was upgraded, <c>false</c> otherwise.</returns>
        public override bool UpgradeDocument()
        {
            bool change = false;
            foreach(var transition in _root.Element("PetriNet").Descendants("Transitions").Elements()) {
                if(transition.Attribute("Shift") != null) {
                    transition.Attribute("Shift").Remove();
                    change = true;
                }
                if(transition.Attribute("ShiftAmplitude") != null) {
                    transition.Attribute("ShiftAmplitude").Remove();
                    change = true;
                }
            }

            return change;
        }
    }
}
