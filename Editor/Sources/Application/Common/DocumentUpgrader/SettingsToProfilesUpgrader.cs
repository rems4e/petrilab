//
//  SettingsToProfilesUpgrader.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2017-10-13.
//

using System;
using System.Xml.Linq;

namespace Petri.Application.DocumentUpgrader
{
    /// <summary>
    /// An upgrader module that extracts information from the settings and put them into a settings profile.
    /// </summary>
    public class SettingsToProfilesUpgrader : DocumentUpgrader
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Application.SettingsToProfilesUpgrader"/> class.
        /// </summary>
        /// <param name="root">Root.</param>
        /// <param name="apiVersion">The old API version.</param>
        public SettingsToProfilesUpgrader(XElement root, int apiVersion) : base(root, apiVersion)
        {
        }

        /// <summary>
        /// Upgrades the document.
        /// </summary>
        /// <returns><c>true</c>, if document was upgraded, <c>false</c> otherwise.</returns>
        public override bool UpgradeDocument()
        {
            var profiles = new XElement("Profiles");
            var profile = DocumentSettingsProfile.GetDefaultProfile(Language);

            var settings = _root.Element("Settings");
            var node = settings.Element("Compiler");
            if(node != null) {
                profile.CompilerInfo.Compiler = node.Attribute("Invocation").Value;

                foreach(var e in node.Elements()) {
                    profile.CompilerInfo.CustomCompilerFlags.Add(e.Attribute("Value").Value);
                }
                node.Remove();
            }

            var sourceOutputPath = settings.Attribute("SourceOutputPath");
            if(sourceOutputPath != null) {
                profile.RelativeSourceOutputPath = sourceOutputPath.Value;
                sourceOutputPath.Remove();
            }

            var libOutputPath = settings.Attribute("LibOutputPath");
            if(libOutputPath != null) {
                profile.RelativeLibOutputPath = libOutputPath.Value;
                libOutputPath.Remove();
            }

            var maxConcurrency = settings.Attribute("MaxConcurrency");
            if(maxConcurrency != null) {
                profile.MaxConcurrency = System.UInt64.Parse(maxConcurrency.Value);
                maxConcurrency.Remove();
            }

            var debugPort = settings.Attribute("Port");
            if(debugPort != null) {
                profile.DebugPort = System.UInt16.Parse(debugPort.Value);
                debugPort.Remove();
            }

            var runInEditor = settings.Attribute("RunInEditor");
            if(runInEditor != null) {
                var val = bool.Parse(runInEditor.Value);
                profile.DebugMode = val ? DebugMode.RunInEditor : DebugMode.Attach;
                runInEditor.Remove();
            }

            var hostname = settings.Attribute("Hostname");
            if(hostname != null) {
                profile.Hostname = hostname.Value;
                hostname.Remove();
            }

            node = settings.Element("IncludePaths");
            if(node != null) {
                foreach(var e in node.Elements("IncludePath")) {
                    profile.CompilerInfo.IncludePaths.Add(Tuple.Create(e.Attribute("Path").Value,
                                                          bool.Parse(e.Attribute("Recursive").Value)));
                }
                node.Remove();
            }

            node = settings.Element("LibPaths");
            if(node != null) {
                foreach(var e in node.Elements("LibPath")) {
                    profile.CompilerInfo.LibPaths.Add(Tuple.Create(e.Attribute("Path").Value,
                                                      bool.Parse(e.Attribute("Recursive").Value)));
                }
                node.Remove();
            }

            node = settings.Element("Libs");
            if(node != null) {
                foreach(var e in node.Elements("Lib")) {
                    profile.CompilerInfo.Libs.Add(e.Attribute("Path").Value);
                }
                node.Remove();
            }

            profiles.Add(profile.GetXml());
            profiles.SetAttributeValue("Default", profile.Name);
            settings.Add(profiles);

            return true;
        }
    }
}
