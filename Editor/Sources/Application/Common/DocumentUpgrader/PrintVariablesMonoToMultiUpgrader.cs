//
//  PrintVariablesMonoToMultiUpgrader.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2017-01-31.
//

using System;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace Petri.Application.DocumentUpgrader
{
    /// <summary>
    /// An upgrader module that replaces the old function to print a single variable into with the
    /// new function that can print multiple variables at once.
    /// </summary>
    public class PrintVariablesMonoToMultiUpgrader : DocumentUpgrader
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Application.PrintVariablesMonoToMultiUpgrader"/> class.
        /// </summary>
        /// <param name="root">Root.</param>
        /// <param name="apiVersion">The old API version.</param>
        public PrintVariablesMonoToMultiUpgrader(XElement root, int apiVersion) : base(root, apiVersion)
        {
            if(Language == Code.Language.C) {
                _oldPattern = new Regex("PetriUtility_printVariable\\(\"\\$[a-zA-Z0-9_]+\", \\$([a-zA-Z0-9_]+)\\)");
            } else if(Language == Code.Language.Cpp) {
                _oldPattern = new Regex("Petri::Utility::printVariable\\(\"\\$[a-zA-Z0-9_]+\", \\$([a-zA-Z0-9_]+)\\)");
            } else if(Language == Code.Language.CSharp) {
                _oldPattern = new Regex("Petri.Runtime.Utility.PrintVariable\\(\"\\$[a-zA-Z0-9_]+\", \\$([a-zA-Z0-9_]+)\\)");
            } else if(Language == Code.Language.Python) {
                throw new NotSupportedException(); // Python support did not exist back then…
            }
        }

        /// <summary>
        /// Upgrades the document.
        /// </summary>
        /// <returns><c>true</c>, if document was upgraded, <c>false</c> otherwise.</returns>
        public override bool UpgradeDocument()
        {
            bool result = false;

            if(Language == Code.Language.C || Language == Code.Language.Cpp || Language == Code.Language.CSharp) {
                foreach(var state in _root.Element("PetriNet").Element("States").Elements()) {
                    if(state.Attribute("Function") != null) {
                        result = UpgradeInvocation(state, "Function") || result;
                    }
                }
                foreach(var transition in _root.Element("PetriNet").Element("Transitions").Elements()) {
                    if(transition.Attribute("Condition") != null) {
                        result = UpgradeInvocation(transition, "Condition") || result;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Upgrades the invocation of a state or a transition.
        /// </summary>
        /// <returns><c>true</c>, if the invocation was upgraded, <c>false</c> otherwise.</returns>
        /// <param name="elem">The entity descriptor.</param>
        /// <param name="attribute">The name of the attribute containing the invocation.</param>
        bool UpgradeInvocation(XElement elem, string attribute)
        {
            bool result = false;

            var value = elem.Attribute(attribute).Value;
            var newValue = _oldPattern.Replace(value, (match) => {
                if(Language == Code.Language.C) {
                    return "PetriUtility_printAllVars(_PETRI_PRIVATE_GET_ENTITY_, $" + match.Groups[1] + ")";
                } else if(Language == Code.Language.Cpp) {
                    return "Petri::Utility::printAllVars(_PETRI_PRIVATE_GET_ENTITY_, $" + match.Groups[1] + ")";
                } else if(Language == Code.Language.CSharp) {
                    return "Petri.Runtime.Utility.PrintAllVars(_PETRI_PRIVATE_GET_ENTITY_, $" + match.Groups[1] + ")";
                }

                throw new Exception("PrintVariablesMonoToMultiUpgrader.UpgradeInvocation: Should not get there!");
            });

            if(value != newValue) {
                elem.SetAttributeValue(attribute, newValue);
                result = true;
            }

            return result;
        }

        Regex _oldPattern;
    }
}
