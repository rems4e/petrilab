//
//  DocumentUpgrader.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2017-01-31.
//

using System.Collections.Generic;
using System.Xml.Linq;

namespace Petri.Application.DocumentUpgrader
{
    /// <summary>
    /// The base class of all modules that will upgrade a document's XML to the last document API.
    /// </summary>
    public abstract class DocumentUpgrader
    {
        /// <summary>
        /// The API version where the printVariable(name, value) function was replaced by a function
        /// that can print multiple variables at once.
        /// </summary>
        static readonly int PrintVarsMonoToMultiAPIVersion = 101;

        /// <summary>
        /// The API version where the variables were put into a stack.
        /// </summary>
        static readonly int VariablesInStackAPIVersion = 102;

        /// <summary>
        /// The API version where the variables list became static.
        /// </summary>
        static readonly int StaticVariablesListAPIVersion = 103;

        /// <summary>
        /// The API version the settings profiles were introduced.
        /// </summary>
        static readonly int SettingsProfilesAPIVersion = 104;

        /// <summary>
        /// The API version where timeouts were introduced, and where the Window, Headers and Macros elements were
        /// moved to the Settings element in the XML.
        /// </summary>
        static readonly int TimeoutAndExtraXMLInSettingsAPIVersion = 105;

        /// <summary>
        /// The API version where shift and shift amplitude of transitions were removed.
        /// </summary>
        static readonly int RemoveTransitionShiftAndAmplitudeAPIVersion = 106;

        /// <summary>
        /// Gets the upgrader modules for the specified API version.
        /// </summary>
        /// <returns>The upgrader.</returns>
        /// <param name="root">Root.</param>
        /// <param name="apiVersion">The old API version.</param>
        public static List<DocumentUpgrader> GetUpgrader(XElement root, int apiVersion)
        {
            var result = new List<DocumentUpgrader>();

            if(apiVersion < PrintVarsMonoToMultiAPIVersion) {
                result.Add(new PrintVariablesMonoToMultiUpgrader(root, apiVersion));
            }
            if(apiVersion < VariablesInStackAPIVersion) {
                result.Add(new VariablesInStackUpgrader(root, apiVersion));
            }
            if(apiVersion < StaticVariablesListAPIVersion) {
                result.Add(new DynamicToStaticVariablesListUpgrader(root, apiVersion));
            }
            if(apiVersion < SettingsProfilesAPIVersion) {
                result.Add(new SettingsToProfilesUpgrader(root, apiVersion));
            }
            if(apiVersion < TimeoutAndExtraXMLInSettingsAPIVersion) {
                result.Add(new ResultEnumWithTimeoutUpgrader(root, apiVersion));
                result.Add(new ExtraXmlInSettingsUpgrader(root, apiVersion));
            }
            if(apiVersion < RemoveTransitionShiftAndAmplitudeAPIVersion) {
                result.Add(new RemoveTransitionShiftUpgrader(root, apiVersion));
            }

            return result;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Application.DocumentUpgrader"/> class.
        /// </summary>
        /// <param name="root">Root.</param>
        /// <param name="apiVersion">The old API version.</param>
        protected DocumentUpgrader(XElement root, int apiVersion)
        {
            _root = root;
            _oldAPIVersion = apiVersion;
        }

        /// <summary>
        /// Gets the language a document is written in.
        /// </summary>
        /// <value>The language.</value>
        protected Code.Language Language {
            get {
                if(_language == null) {
                    _language = (Code.Language)System.Enum.Parse(typeof(Code.Language),
                                                                 _root.Element("Settings").Attribute("Language").Value);
                }

                return _language.Value;
            }
        }

        /// <summary>
        /// Performs the document upgrade
        /// </summary>
        /// <returns><c>true</c>, if document was upgraded, <c>false</c> otherwise.</returns>
        public abstract bool UpgradeDocument();

        /// <summary>
        /// The root node of the document's XML descriptor.
        /// </summary>
        protected XElement _root;


        /// <summary>
        /// The old API Version.
        /// </summary>
        protected int _oldAPIVersion;
        Code.Language? _language;
    }
}
