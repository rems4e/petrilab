//
//  IOutput.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-03-12.
//

using System;

namespace Petri.Application
{
    /// <summary>
    /// Text output management.
    /// </summary>
    public interface IOutput
    {
        /// <summary>
        /// Write the specified value and args.
        /// </summary>
        /// <param name="value">Value.</param>
        /// <param name="args">Arguments.</param>
        void Write(string value, params object[] args);

        /// <summary>
        /// Writes the specified value and args, plus a new line character.
        /// </summary>
        /// <param name="value">Value.</param>
        /// <param name="args">Arguments.</param>
        void WriteLine(string value, params object[] args);

        /// <summary>
        /// Writes a new line character.
        /// </summary>
        void WriteLine();

        /// <summary>
        /// Writes the specified value and args, plus a new line character, as a warning message;
        /// </summary>
        /// <param name="value">Value.</param>
        /// <param name="args">Arguments.</param>
        void WriteWarningLine(string value, params object[] args);

        /// <summary>
        /// Writes the specified value and args, as an error message.
        /// </summary>
        /// <param name="value">Value.</param>
        /// <param name="args">Arguments.</param>
        void WriteError(string value, params object[] args);

        /// <summary>
        /// Writes the specified value and args, plus a new line character, as an error message.
        /// </summary>
        /// <param name="value">Value.</param>
        /// <param name="args">Arguments.</param>
        void WriteErrorLine(string value, params object[] args);

        /// <summary>
        /// Writes a new line character as an error message.
        /// </summary>
        void WriteErrorLine();
    }

    /// <summary>
    /// Text output to the console.
    /// </summary>
    public class ConsoleOutput : IOutput
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Application.ConsoleOutput"/> class.
        /// </summary>
        /// <param name="hasColor">The initial value of the <see cref="HasColor"/> property.</param>
        public ConsoleOutput(bool hasColor)
        {
            HasColor = hasColor;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="T:Petri.Application.ConsoleOutput"/> prints
        /// warning and error messages in color.
        /// </summary>
        /// <value><c>true</c> if has color; otherwise, <c>false</c>.</value>
        public bool HasColor {
            get;
            set;
        }

        /// <summary>
        /// Write the specified format and args.
        /// </summary>
        /// <param name="format">Format.</param>
        /// <param name="args">Arguments.</param>
        public void Write(string format, params object[] args)
        {
            Console.Write(format, args);
        }

        /// <summary>
        /// Writes the specified format and args, plus a new line character.
        /// </summary>
        /// <param name="format">Format.</param>
        /// <param name="args">Arguments.</param>
        public void WriteLine(string format, params object[] args)
        {
            Console.WriteLine(format, args);
        }

        /// <summary>
        /// Writes a new line character.
        /// </summary>
        public void WriteLine()
        {
            Console.WriteLine();
        }

        /// <summary>
        /// Writes the specified value and args, plus a new line character, as a warning message.
        /// </summary>
        /// <param name="format">Format.</param>
        /// <param name="args">Arguments.</param>
        public void WriteWarningLine(string format, params object[] args)
        {
            SetColor(ConsoleColor.DarkYellow);
            Console.Error.WriteLine(format, args);
            ResetColor();
        }

        /// <summary>
        /// Writes the specified value and args to the error output stream.
        /// </summary>
        /// <param name="format">Format.</param>
        /// <param name="args">Arguments.</param>
        public void WriteError(string format, params object[] args)
        {
            SetColor(ConsoleColor.Red);
            Console.Error.Write(format, args);
            ResetColor();
        }

        /// <summary>
        /// Writes the specified value and args to the error output stream, plus a new line character.
        /// </summary>
        /// <param name="format">Format.</param>
        /// <param name="args">Arguments.</param>
        public void WriteErrorLine(string format, params object[] args)
        {
            SetColor(ConsoleColor.Red);
            Console.Error.WriteLine(format, args);
            ResetColor();
        }

        /// <summary>
        /// Writes the a new line character to the error output stream.
        /// </summary>
        public void WriteErrorLine()
        {
            Console.Error.WriteLine();
        }

        /// <summary>
        /// Sets the text color of the console if <see cref="HasColor"/> is true.
        /// </summary>
        /// <param name="color">Color.</param>
        void SetColor(ConsoleColor color)
        {
            if(HasColor) {
                Console.ForegroundColor = color;
            }
        }

        /// <summary>
        /// Resets the console color of the console if <see cref="HasColor"/> is true.
        /// </summary>
        void ResetColor()
        {
            if(HasColor) {
                Console.ResetColor();
            }
        }
    }
}

