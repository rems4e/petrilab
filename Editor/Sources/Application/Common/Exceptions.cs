﻿//
//  Exceptions.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2018-02-17.
//

using System;

namespace Petri.Application
{
    /// <summary>
    /// A generic exception type that serves as the base of all the program's specific exceptions.
    /// </summary>
    public class BaseException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Application.BaseException"/> class.
        /// </summary>
        public BaseException() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Application.BaseException"/> class.
        /// </summary>
        /// <param name="message">Message.</param>
        public BaseException(string message) : base(message) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Application.BaseException"/> class.
        /// </summary>
        /// <param name="message">Message.</param>
        /// <param name="innerException">Inner exception.</param>
        public BaseException(string message, Exception innerException) : base(message, innerException) { }
    }

    /// <summary>
    /// An exception that may be thrown in various stages of the generation process.
    /// </summary>
    public class GenerationException : BaseException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Application.GenerationException"/> class.
        /// </summary>
        /// <param name="message">Message.</param>
        public GenerationException(string message) : base(message) { }
    }

    /// <summary>
    /// An exception from the debugger.
    /// </summary>
    public class DebuggerException : BaseException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Application.DebuggerException"/> class.
        /// </summary>
		public DebuggerException() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Application.DebuggerException"/> class.
        /// </summary>
        /// <param name="message">Message.</param>
        public DebuggerException(string message) : base(message) { }
    }

    /// <summary>
    /// The lib could not be loaded when the petri net is to be run by the editor.
    /// </summary>
    public class RunInEditorLoadLibraryDebuggerException : DebuggerException
    {
    }

    /// <summary>
    /// The program hosting the debug server could not be launched or has exited.
    /// </summary>
    public class InvalidProgramStateDebuggerException : DebuggerException
    {
    }

    /// <summary>
    /// The attach process was interrupted by the user.
    /// </summary>
    public class AttachInterruptedDebuggerException : DebuggerException
    {
    }
}
