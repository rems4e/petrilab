//
//  MenuBarWindow.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2014-11-13.
//

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Gtk;
using IgeMacIntegration;

namespace Petri.Application.GUI
{
    /// <summary>
    /// A window that has a menu bar
    /// </summary>
    public abstract class MenuBarWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.MenuBarWindow"/> class.
        /// </summary>
        /// <param name="noMenuBar"><c>true</c> if we don't want a menu bar to be attached to the window.</param>
        protected MenuBarWindow(bool noMenuBar = false) : base(WindowType.Toplevel)
        {
            _noMenuBar = noMenuBar;
            _vbox = new VBox(false, 5);
            // Allows the window to intercept mouse click events to allow it to be dragged by its content.
            var eventBox = new EventBox();
            eventBox.Add(_vbox);
            Add(eventBox);

            QuitSensitive = true;

            BuildMenus();

            FocusInEvent += (o, args) => {
                if(Configuration.RunningPlatform == Platform.Mac) {
                    _menuBar.ShowAll();
                    IgeMacMenu.MenuBar = _menuBar;
                }
                UpdateNew();
                UpdateRecentDocuments();
                UpdateWindowsMenuItems();
            };

            DeleteEvent += OnDeleteEvent;
        }

        /// <summary>
        /// Updates the new document menu item according to the application's default language.
        /// </summary>
        public void UpdateNew()
        {
            if(GUIApplication.IsReadOnly) {
                _newItem.Sensitive = false;
            } else {
                _newItem.Submenu = null;
                _newItems = new Dictionary<MenuItem, Code.Language>();
                if(_newMenu != null) {
                    _newMenu.Destroy();
                }

                _newMenu = new Menu();
                _newItem.Submenu = _newMenu;

                foreach(Code.Language language in Enum.GetValues(typeof(Code.Language))) {
                    MenuItem item = new MenuItem(Configuration.GetLocalized("{0} Petri Net",
                                                                            DocumentSettings.LanguageName(language)));
                    _newItems[item] = language;

                    item.Activated += OnClickMenu;
                    if(language == Configuration.DefaultLanguage) {
                        item.AddAccelerator("activate",
                                            _accelGroup,
                                            new AccelKey(Gdk.Key.n,
                                                         Gdk.ModifierType.ControlMask,
                                                         AccelFlags.Visible));
                    }
                    _newMenu.Append(item);
                    item.Show();
                }

                _newItem.ShowAll();
            }
        }

        /// <summary>
        /// Updates the recent documents menu items.
        /// </summary>
        public virtual void UpdateRecentDocuments()
        {
            _openRecentItem.Submenu = null;
            if(_openRecentMenu != null) {
                _openRecentMenu.Destroy();
            }

            _openRecentMenu = new Menu();
            _openRecentItem.Submenu = _openRecentMenu;

            var recentItems = GUIApplication.RecentDocuments;

            foreach(var pair in recentItems) {
                var item = new MenuItem("");
                ((Label)item.Child).Text = Application.ShortPath(pair.Path);
                item.Activated += OnClickRecentMenu;
                _openRecentMenu.Append(item);
                item.Show();
            }

            if(recentItems.Count > 0) {
                _openRecentMenu.Append(new SeparatorMenuItem());
            }
            _clearRecentItems = new MenuItem(Configuration.GetLocalized("Clear Recent"));
            _clearRecentItems.Activated += OnClickMenu;
            _openRecentMenu.Append(_clearRecentItems);
            _clearRecentItems.Sensitive = recentItems.Count > 0;

            _openRecentItem.ShowAll();
        }

        /// <summary>
        /// Updates the window menu.
        /// </summary>
        public virtual void UpdateWindowsMenuItems()
        {
            var menu = (MenuItem)_windowMenu.AttachWidget;
            menu.Submenu = null;
            _windowMenu.Destroy();

            _windowMenu = new Menu();
            menu.Submenu = _windowMenu;

            var docs = GUIApplication.Documents;

            RadioMenuItem last = null;
            foreach(var doc in docs) {
                RadioMenuItem item = last == null ? new RadioMenuItem("") : new RadioMenuItem(last, "");
                last = item;
                ((Label)item.Child).Text = doc.Settings.Name;
                if(this is DocumentWindow && doc == ((DocumentWindow)this).Document) {
                    item.Active = true;
                }
                item.Activated += OnClickWindowMenu;
                item.Data["related document"] = doc;
                _windowMenu.Append(item);
                item.Show();
            }

            if(docs.Count > 0) {
                _windowMenu.Append(new SeparatorMenuItem());
            }
            _welcomeScreenItem = last == null ? new RadioMenuItem("") : new RadioMenuItem(last, "");
            ((Label)_welcomeScreenItem.Child).Text = Configuration.GetLocalized("MenuItem Welcome Screen");
            last = _welcomeScreenItem;
            if(this is WelcomeScreen) {
                _welcomeScreenItem.Active = true;
            }
            _welcomeScreenItem.Activated += OnClickMenu;
            _windowMenu.Append(_welcomeScreenItem);

            menu.ShowAll();
        }

        /// <summary>
        /// Builds the menus and the menu bar.
        /// </summary>
        protected void BuildMenus()
        {
            _accelGroup = new AccelGroup();
            AddAccelGroup(_accelGroup);
            if(_staticAccelGroup != null) {
                AddAccelGroup(_staticAccelGroup);
            }

            _menuBar = new MenuBar();

            _fileMenu = new Menu();
            _editMenu = new Menu();
            _viewMenu = new Menu();
            _documentMenu = new Menu();
            _debuggerMenu = new Menu();
            _windowMenu = new Menu();
            _helpMenu = new Menu();
            MenuItem file = new MenuItem(Configuration.GetLocalized("File"));
            MenuItem edit = new MenuItem(Configuration.GetLocalized("MenuEdit"));
            MenuItem view = new MenuItem(Configuration.GetLocalized("View"));
            MenuItem document = new MenuItem(Configuration.GetLocalized("Document"));
            MenuItem debugger = new MenuItem(Configuration.GetLocalized("Debug"));
            MenuItem window = new MenuItem(Configuration.GetLocalized("MenuWindow"));
            MenuItem help = new MenuItem(Configuration.GetLocalized("Help"));
            file.Submenu = _fileMenu;
            edit.Submenu = _editMenu;
            view.Submenu = _viewMenu;
            document.Submenu = _documentMenu;
            debugger.Submenu = _debuggerMenu;
            window.Submenu = _windowMenu;
            help.Submenu = _helpMenu;

            _newItem = new MenuItem(Configuration.GetLocalized("New"));

            _openItem = new MenuItem(Configuration.GetLocalized("Open…"));
            _openItem.Activated += OnClickMenu;
            _openItem.AddAccelerator("activate",
                                     _accelGroup,
                                     new AccelKey(Gdk.Key.o,
                                                  Gdk.ModifierType.ControlMask,
                                                  AccelFlags.Visible));

            _openRecentItem = new MenuItem(Configuration.GetLocalized("Open Recent"));

            _closeItem = new MenuItem(Configuration.GetLocalized("Close"));
            _closeItem.Activated += OnClickMenu;
            _closeItem.AddAccelerator("activate",
                                      _accelGroup,
                                      new AccelKey(Gdk.Key.w,
                                                   Gdk.ModifierType.ControlMask,
                                                   AccelFlags.Visible));

            _saveItem = new MenuItem(Configuration.GetLocalized("Save"));
            _saveItem.Activated += OnClickMenu;
            _saveItem.AddAccelerator("activate",
                                     _accelGroup,
                                     new AccelKey(Gdk.Key.s,
                                                  Gdk.ModifierType.ControlMask,
                                                  AccelFlags.Visible));
            _saveItem.Sensitive = false;

            _saveAsItem = new MenuItem(Configuration.GetLocalized("Save As…"));
            _saveAsItem.Activated += OnClickMenu;
            _saveAsItem.AddAccelerator("activate",
                                       _accelGroup,
                                       new AccelKey(Gdk.Key.s,
                                                    Gdk.ModifierType.ControlMask | Gdk.ModifierType.ShiftMask,
                                                    AccelFlags.Visible));
            _saveAsItem.Sensitive = false;

            _exportItem = new MenuItem(Configuration.GetLocalized("Export as PDF…"));
            _exportItem.Activated += OnClickMenu;
            _exportItem.AddAccelerator("activate",
                                       _accelGroup,
                                       new AccelKey(Gdk.Key.e,
                                                    Gdk.ModifierType.ControlMask,
                                                    AccelFlags.Visible));
            _exportItem.Sensitive = false;

            _generateItem = new MenuItem(Configuration.GetLocalized("Generate source code"));
            _generateItem.Activated += OnClickMenu;
            _generateItem.AddAccelerator("activate",
                                         _accelGroup,
                                         new AccelKey(Gdk.Key.g,
                                                      Gdk.ModifierType.ControlMask | Gdk.ModifierType.ShiftMask,
                                                      AccelFlags.Visible));
            _generateItem.Sensitive = false;

            _compileItem = new MenuItem(Configuration.GetLocalized("Compile"));
            _compileItem.Activated += OnClickMenu;
            _compileItem.AddAccelerator("activate",
                                        _accelGroup,
                                        new AccelKey(Gdk.Key.c,
                                                     Gdk.ModifierType.ControlMask | Gdk.ModifierType.ShiftMask,
                                                     AccelFlags.Visible));
            _compileItem.Sensitive = false;

            _deployItem = new MenuItem(Configuration.GetLocalized("Deploy"));
            _deployItem.Activated += OnClickMenu;
            _deployItem.AddAccelerator("activate",
                                       _accelGroup,
                                       new AccelKey(Gdk.Key.d,
                                                    Gdk.ModifierType.ControlMask | Gdk.ModifierType.ShiftMask,
                                                    AccelFlags.Visible));
            _deployItem.Sensitive = false;

            _showDocumentItem = new MenuItem(Configuration.GetLocalized("Show document in file explorer"));
            _showDocumentItem.Activated += OnClickMenu;
            _showDocumentItem.Sensitive = false;

            _showCodeItem = new MenuItem(Configuration.GetLocalized("Show source code in file explorer"));
            _showCodeItem.Activated += OnClickMenu;
            _showCodeItem.Sensitive = false;

            _showCompilerOutputItem = new MenuItem(Configuration.GetLocalized("Show compiler output in file explorer"));
            _showCompilerOutputItem.Activated += OnClickMenu;
            _showCompilerOutputItem.Sensitive = false;

            _revertItem = new MenuItem(Configuration.GetLocalized("Revert…"));
            _revertItem.Activated += OnClickMenu;
            _revertItem.Sensitive = false;

            _fileMenu.Append(_newItem);
            _fileMenu.Append(_openItem);
            _fileMenu.Append(_openRecentItem);
            _fileMenu.Append(new SeparatorMenuItem());
            _fileMenu.Append(_closeItem);
            _fileMenu.Append(_saveItem);
            _fileMenu.Append(_saveAsItem);
            _fileMenu.Append(_revertItem);
            _fileMenu.Append(new SeparatorMenuItem());
            _fileMenu.Append(_exportItem);
            _fileMenu.Append(new SeparatorMenuItem());
            _fileMenu.Append(_generateItem);
            _fileMenu.Append(_compileItem);
            _fileMenu.Append(_deployItem);
            _fileMenu.Append(new SeparatorMenuItem());
            _fileMenu.Append(_showDocumentItem);
            _fileMenu.Append(_showCodeItem);
            _fileMenu.Append(_showCompilerOutputItem);

            _undoItem = new MenuItem(Configuration.GetLocalized("Undo"));
            _undoItem.Activated += OnClickMenu;
            _undoItem.AddAccelerator("activate",
                                     _accelGroup,
                                     new AccelKey(Gdk.Key.z,
                                                  Gdk.ModifierType.ControlMask,
                                                  AccelFlags.Visible));
            _undoItem.Sensitive = false;

            _redoItem = new MenuItem(Configuration.GetLocalized("Redo"));
            _redoItem.Activated += OnClickMenu;
            _redoItem.AddAccelerator("activate",
                                     _accelGroup,
                                     new AccelKey(Gdk.Key.z,
                                                  Gdk.ModifierType.ControlMask | Gdk.ModifierType.ShiftMask,
                                                  AccelFlags.Visible));
            _redoItem.Sensitive = false;

            _cutItem = new MenuItem(Configuration.GetLocalized("Cut"));
            _cutItem.Activated += OnClickMenu;
            _cutItem.AddAccelerator("activate",
                                    _accelGroup,
                                    new AccelKey(Gdk.Key.x,
                                                 Gdk.ModifierType.ControlMask,
                                                 AccelFlags.Visible));

            _copyItem = new MenuItem(Configuration.GetLocalized("Copy"));
            _copyItem.Activated += OnClickMenu;
            _copyItem.AddAccelerator("activate",
                                     _accelGroup,
                                     new AccelKey(Gdk.Key.c,
                                                  Gdk.ModifierType.ControlMask,
                                                  AccelFlags.Visible));

            _selectAllItem = new MenuItem(Configuration.GetLocalized("Select All"));
            _selectAllItem.Activated += OnClickMenu;
            _selectAllItem.AddAccelerator("activate",
                                          _accelGroup,
                                          new AccelKey(Gdk.Key.a,
                                                       Gdk.ModifierType.ControlMask,
                                                       AccelFlags.Visible));

            _pasteItem = new MenuItem(Configuration.GetLocalized("Paste"));
            _pasteItem.Activated += OnClickMenu;
            _pasteItem.AddAccelerator("activate",
                                      _accelGroup,
                                      new AccelKey(Gdk.Key.v,
                                                   Gdk.ModifierType.ControlMask,
                                                   AccelFlags.Visible));

            _findItem = new MenuItem(Configuration.GetLocalized("Find…"));
            _findItem.Activated += OnClickMenu;
            _findItem.AddAccelerator("activate",
                                     _accelGroup,
                                     new AccelKey(Gdk.Key.f,
                                                  Gdk.ModifierType.ControlMask,
                                                  AccelFlags.Visible));
            _findItem.Sensitive = false;

            _embedInMacroItem = new MenuItem(Configuration.GetLocalized("Group in a Macro"));
            _embedInMacroItem.Activated += OnClickMenu;
            _embedInMacroItem.AddAccelerator("activate",
                                             _accelGroup,
                                             new AccelKey(Gdk.Key.e,
                                                          Gdk.ModifierType.ControlMask | Gdk.ModifierType.Mod1Mask,
                                                          AccelFlags.Visible));

            _importPetriNetItem = new MenuItem(Configuration.GetLocalized("Import Petri net…"));
            _importPetriNetItem.Activated += OnClickMenu;
            _importPetriNetItem.AddAccelerator("activate",
                                               _accelGroup,
                                               new AccelKey(Gdk.Key.i,
                                                            Gdk.ModifierType.ControlMask | Gdk.ModifierType.Mod1Mask,
                                                            AccelFlags.Visible));
            _importPetriNetItem.Sensitive = false;
            _embedInMacroItem.Sensitive = false;

            _editMenu.Append(_undoItem);
            _editMenu.Append(_redoItem);
            _editMenu.Append(new SeparatorMenuItem());
            _editMenu.Append(_cutItem);
            _editMenu.Append(_copyItem);
            _editMenu.Append(_pasteItem);
            _editMenu.Append(new SeparatorMenuItem());
            _editMenu.Append(_selectAllItem);
            _editMenu.Append(new SeparatorMenuItem());
            _editMenu.Append(_findItem);
            _editMenu.Append(new SeparatorMenuItem());
            _editMenu.Append(_embedInMacroItem);
            _editMenu.Append(_importPetriNetItem);

            _undoItem.Sensitive = false;
            _redoItem.Sensitive = false;
            _cutItem.Sensitive = false;
            _copyItem.Sensitive = false;
            _pasteItem.Sensitive = false;

            _actualSizeItem = new MenuItem(Configuration.GetLocalized("Actual Size"));
            _actualSizeItem.Activated += OnClickMenu;
            _actualSizeItem.AddAccelerator("activate",
                                           _accelGroup,
                                           new AccelKey(Gdk.Key.Key_0,
                                                        Gdk.ModifierType.ControlMask | Gdk.ModifierType.ShiftMask,
                                                        AccelFlags.Visible));
            _actualSizeItem.Sensitive = false;

            _zoomInItem = new MenuItem(Configuration.GetLocalized("Zoom In"));
            _zoomInItem.Activated += OnClickMenu;
            _zoomInItem.AddAccelerator("activate",
                                       _accelGroup,
                                       new AccelKey(Gdk.Key.plus,
                                                    Gdk.ModifierType.ControlMask,
                                                    AccelFlags.Visible));
            _zoomInItem.Sensitive = false;

            _zoomOutItem = new MenuItem(Configuration.GetLocalized("Zoom Out"));
            _zoomOutItem.Activated += OnClickMenu;
            _zoomOutItem.AddAccelerator("activate",
                                        _accelGroup,
                                        new AccelKey(Gdk.Key.minus,
                                                     Gdk.ModifierType.ControlMask,
                                                     AccelFlags.Visible));
            _zoomOutItem.Sensitive = false;

            _viewMenu.Append(_actualSizeItem);
            _viewMenu.Append(_zoomInItem);
            _viewMenu.Append(_zoomOutItem);

            _showEditorItem = new MenuItem(Configuration.GetLocalized("Show Editor"));
            _showEditorItem.Activated += OnClickMenu;
            _showEditorItem.AddAccelerator("activate",
                                           _accelGroup,
                                           new AccelKey(Gdk.Key.e,
                                                        Gdk.ModifierType.ControlMask | Gdk.ModifierType.Mod1Mask,
                                                        AccelFlags.Visible));
            _showEditorItem.Sensitive = false;

            _showDebuggerItem = new MenuItem(Configuration.GetLocalized("Show Debugger"));
            _showDebuggerItem.Activated += OnClickMenu;
            _showDebuggerItem.AddAccelerator("activate",
                                             _accelGroup,
                                             new AccelKey(Gdk.Key.d,
                                                          Gdk.ModifierType.ControlMask | Gdk.ModifierType.Mod1Mask,
                                                          AccelFlags.Visible));
            _showDebuggerItem.Sensitive = false;

            _knownFunctionsItem = new MenuItem(Configuration.GetLocalized("Show Known Functions…"));
            _knownFunctionsItem.Activated += OnClickMenu;
            _knownFunctionsItem.Sensitive = false;

            _manageHeadersItem = new MenuItem(Configuration.GetLocalized("Manage Headers…"));
            _manageHeadersItem.Activated += OnClickMenu;
            _manageHeadersItem.Sensitive = false;

            _manageMacrosItem = new MenuItem(Configuration.GetLocalized("Manage Macros…"));
            _manageMacrosItem.Activated += OnClickMenu;
            _manageMacrosItem.Sensitive = false;

            _profilesItem = new MenuItem(Configuration.GetLocalized("Profiles"));
            _profilesItem.Sensitive = false;

            _documentSettingsItem = new MenuItem(Configuration.GetLocalized("Settings…"));
            _documentSettingsItem.Activated += OnClickMenu;
            _documentSettingsItem.AddAccelerator("activate",
                                                 _accelGroup,
                                                 new AccelKey(Gdk.Key.comma,
                                                              Gdk.ModifierType.ControlMask | Gdk.ModifierType.Mod1Mask,
                                                              AccelFlags.Visible));
            _documentSettingsItem.Sensitive = false;

            _documentMenu.Append(_showEditorItem);
            _documentMenu.Append(_showDebuggerItem);
            _documentMenu.Append(new SeparatorMenuItem());
            _documentMenu.Append(_knownFunctionsItem);
            _documentMenu.Append(_manageHeadersItem);
            _documentMenu.Append(_manageMacrosItem);
            _documentMenu.Append(new SeparatorMenuItem());
            _documentMenu.Append(_profilesItem);
            _documentMenu.Append(_documentSettingsItem);

            _clearBreakpointsItem = new MenuItem(Configuration.GetLocalized("Clear All Breakpoints"));
            _clearBreakpointsItem.Activated += OnClickMenu;
            _runStopItem = new MenuItem(Configuration.GetLocalized("Execute"));
            _runStopItem.Activated += OnClickMenu;
            _runStopItem.AddAccelerator("activate",
                _accelGroup,
                new AccelKey(Gdk.Key.F5,
                             Gdk.ModifierType.None,
                             AccelFlags.Visible));
            _attachDetachItem = new MenuItem(Configuration.GetLocalized("Connection"));
            _attachDetachItem.Activated += OnClickMenu;
            _createHostItem = new MenuItem(Configuration.GetLocalized("Create Host…"));
            _createHostItem.Activated += OnClickMenu;
            _createHostItem.AddAccelerator("activate",
                                           _accelGroup,
                                           new AccelKey(Gdk.Key.H,
                                                        Gdk.ModifierType.ControlMask | Gdk.ModifierType.ShiftMask,
                                                        AccelFlags.Visible));
            _pauseItem = new MenuItem(Configuration.GetLocalized("Pause"));
            _pauseItem.Activated += OnClickMenu;
            _showVarItem = new MenuItem(Configuration.GetLocalized("Show Variables…"));
            _showVarItem.Activated += OnClickMenu;
            _reloadPetriItem = new MenuItem(Configuration.GetLocalized("Fix"));
            _reloadPetriItem.Activated += OnClickMenu;
            _exitItem = new MenuItem(Configuration.GetLocalized("End Session"));
            _exitItem.Activated += OnClickMenu;

            _clearConsoleItem = new MenuItem(Configuration.GetLocalized("Clear Console"));
            _clearConsoleItem.Activated += OnClickMenu;
            _lockConsoleItem = new MenuItem(Configuration.GetLocalized("Lock Console Scrolling"));
            _lockConsoleItem.Activated += OnClickMenu;

            _syncWithEditorItem = new MenuItem(Configuration.GetLocalized("Sync Debugger With Editor"));
            _syncWithEditorItem.Activated += OnClickMenu;


            _debuggerMenu.Append(_attachDetachItem);
            _debuggerMenu.Append(_createHostItem);
            _debuggerMenu.Append(_exitItem);
            _debuggerMenu.Append(new SeparatorMenuItem());
            _debuggerMenu.Append(_runStopItem);
            _debuggerMenu.Append(_pauseItem);
            _debuggerMenu.Append(_reloadPetriItem);
            _debuggerMenu.Append(_syncWithEditorItem);
            _debuggerMenu.Append(new SeparatorMenuItem());
            _debuggerMenu.Append(_clearConsoleItem);
            _debuggerMenu.Append(_lockConsoleItem);
            _debuggerMenu.Append(new SeparatorMenuItem());
            _debuggerMenu.Append(_showVarItem);
            _debuggerMenu.Append(_clearBreakpointsItem);


            _welcomeScreenItem = new RadioMenuItem(Configuration.GetLocalized("MenuItem Welcome Screen"));
            _welcomeScreenItem.Activated += OnClickMenu;

            _windowMenu.Append(_welcomeScreenItem);


            _showHelpItem = new MenuItem(Configuration.GetLocalized("Help…"));
            _showHelpItem.Activated += OnClickMenu;
            _helpMenu.Append(_showHelpItem);

            var examplesPath = ExamplesPaths;
            if(ExamplesPaths != null) {
                var showExamplesItem = new MenuItem(Configuration.GetLocalized("Examples menu item"));
                showExamplesItem.Submenu = new Menu();

                var submenus = new Stack<Tuple<Menu, List<object>>>();
                submenus.Push(Tuple.Create((Menu)showExamplesItem.Submenu, examplesPath));
                while(submenus.Count > 0) {
                    var currentSubmenu = submenus.Pop();
                    foreach(var item in currentSubmenu.Item2) {
                        if(item is string) {
                            var menuItem = new MenuItem(System.IO.Path.GetFileNameWithoutExtension((string)item));
                            currentSubmenu.Item1.Add(menuItem);
                            menuItem.Activated += (sender, e) => {
                                GUIApplication.OpenDocument((string)item);
                            };
                        } else {
                            var casted = (Tuple<string, List<object>>)item;
                            if(casted.Item2.Count > 0) {
                                var menuItem = new MenuItem(System.IO.Path.GetFileName(casted.Item1));
                                currentSubmenu.Item1.Add(menuItem);
                                menuItem.Submenu = new Menu();
                                submenus.Push(Tuple.Create((Menu)menuItem.Submenu, casted.Item2));
                            }
                        }
                    }
                }
                _helpMenu.Append(showExamplesItem);
            }

            if(Configuration.RunningPlatform != Platform.Mac) {
                _quitItem = new MenuItem(Configuration.GetLocalized("Quit"));
                _quitItem.Activated += OnClickMenu;
                _quitItem.AddAccelerator("activate",
                                         _accelGroup,
                                         new AccelKey(Gdk.Key.q,
                                                      Gdk.ModifierType.ControlMask,
                                                      AccelFlags.Visible));

                _preferencesItem = new MenuItem(Configuration.GetLocalized("Preferences…"));
                _preferencesItem.Activated += OnClickMenu;
                _preferencesItem.AddAccelerator("activate",
                                                _accelGroup,
                                                new AccelKey(Gdk.Key.comma,
                                                             Gdk.ModifierType.ControlMask,
                                                             AccelFlags.Visible));

                _aboutItem = new MenuItem(Configuration.GetLocalized("About PetriLab…"));
                _aboutItem.Activated += OnClickMenu;

                _fileMenu.Append(new SeparatorMenuItem());
                _fileMenu.Append(_quitItem);
                _editMenu.Append(new SeparatorMenuItem());
                _editMenu.Append(_preferencesItem);
                _helpMenu.Append(new SeparatorMenuItem());
                _helpMenu.Append(_aboutItem);
            }

            _menuBar.Append(file);
            _menuBar.Append(edit);
            _menuBar.Append(view);
            _menuBar.Append(document);
            _menuBar.Append(debugger);
            _menuBar.Append(window);
            _menuBar.Append(help);

            foreach(var menuItem in _debuggerMenu.Children) {
                menuItem.Sensitive = false;
            }

            if(!_noMenuBar && Configuration.RunningPlatform != Platform.Mac) {
                _vbox.PackStart(_menuBar, false, false, 0);
            }
        }

        /// <summary>
        /// Inits the static components of GUI. At the moment, it initializes the OS X menu bar and document association stuff.
        /// To be called once at application startup.
        /// </summary>
        public static void InitMenuBar()
        {
            if(Configuration.RunningPlatform == Platform.Mac) {
                _staticAccelGroup = new AccelGroup();

                _staticQuitItem = new MenuItem(Configuration.GetLocalized("Quit"));
                _staticQuitItem.Activated += OnClickMenuStatic;
                _staticQuitItem.AddAccelerator("activate",
                                               _staticAccelGroup,
                                               new AccelKey(Gdk.Key.q,
                                                            Gdk.ModifierType.ControlMask,
                                                            AccelFlags.Visible));

                _staticPreferencesItem = new MenuItem(Configuration.GetLocalized("Preferences…"));
                _staticPreferencesItem.Activated += OnClickMenuStatic;
                _staticPreferencesItem.AddAccelerator("activate",
                                                      _staticAccelGroup,
                                                      new AccelKey(Gdk.Key.comma,
                                                                   Gdk.ModifierType.ControlMask,
                                                                   AccelFlags.Visible));

                _staticAboutItem = new MenuItem(Configuration.GetLocalized("About PetriLab…"));
                _staticAboutItem.Activated += OnClickMenuStatic;

                var aboutGroup = IgeMacMenu.AddAppMenuGroup();
                aboutGroup.AddMenuItem(_staticAboutItem, null);
                var prefsGroup = IgeMacMenu.AddAppMenuGroup();
                prefsGroup.AddMenuItem(_staticPreferencesItem, null);

                IgeMacMenu.QuitMenuItem = _staticQuitItem;

                IgeMacMenu.GlobalKeyHandlerEnabled = true;
            }
        }

        /// <summary>
        /// Called when a recent item menu is clicked.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="args">Arguments.</param>
        protected void OnClickRecentMenu(object sender, EventArgs args)
        {
            var item = ((Label)((MenuItem)sender).Child).Text;
            if(!System.IO.Path.IsPathRooted(item) && item.StartsWithInv("~/")) {
                item = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile),
                                              item.Substring(2));
            }
            GUIApplication.OpenDocument(item);
        }

        /// <summary>
        /// Called when an item the window menu is clicked.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="args">Arguments.</param>
        protected void OnClickWindowMenu(object sender, EventArgs args)
        {
            var item = (RadioMenuItem)sender;
            if(item.Active) {
                var doc = (GUIDocument)item.Data["related document"];
                doc.Window.ShowWindow();
            }
        }

        /// <summary>
        /// Called when any of the non-static menu item is selected from the menu bar.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="args">Arguments.</param>
        async void OnClickMenu(object sender, EventArgs args)
        {
            try {
                await HandleMenuClicked(sender);
            } catch(Exception except) {
                GUIApplication.NotifyUnrecoverableError(this, except.Message);
            }
        }

#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously
        /// <summary>
        /// Called when any of the non-static menu item are selected from the menu bar.
        /// </summary>
        /// <param name="sender">Sender.</param>
        protected virtual async Task HandleMenuClicked(object sender)
#pragma warning restore CS1998 // Async method lacks 'await' operators and will run synchronously
        {
            if(sender == _quitItem && QuitSensitive) {
                GUIApplication.SaveAndQuit();
            } else if(sender == _preferencesItem) {
                GUIApplication.SettingsEditor.ShowWindow();
            } else if(sender == _openItem) {
                GUIApplication.OpenDocument();
            } else if(sender == _clearRecentItems) {
                GUIApplication.ClearRecentDocuments();
                UpdateRecentDocuments();
            } else if(_newItems != null && _newItems.ContainsKey(sender as MenuItem)) {
                GUIApplication.CreateDocument(_newItems[sender as MenuItem]);
            } else if(sender == _copyItem) {
                var w = GUIApplication.CurrentFocusedWindow;
                if(w != null) {
                    if(w == this) {
                        ManageCopy();
                    }

                    if(w.Focus is Editable) {
                        (w.Focus as Editable).CopyClipboard();
                    } else if(w.Focus is TextView) {
                        var v = w.Focus as TextView;
                        v.Buffer.CopyClipboard(v.GetClipboard(Gdk.Selection.Clipboard));
                    }
                }
            } else if(sender == _cutItem) {
                var w = GUIApplication.CurrentFocusedWindow;
                if(w != null) {
                    if(w == this) {
                        ManageCut();
                    }
                    if(w.Focus is Editable) {
                        (w.Focus as Editable).CutClipboard();
                    } else if(w.Focus is TextView) {
                        var v = w.Focus as TextView;
                        v.Buffer.CutClipboard(v.GetClipboard(Gdk.Selection.Clipboard), true);
                    }
                }
            } else if(sender == _pasteItem) {
                var w = GUIApplication.CurrentFocusedWindow;
                if(w != null) {
                    if(w == this) {
                        ManagePaste();
                    }

                    if(w.Focus is Editable) {
                        (w.Focus as Editable).PasteClipboard();
                    } else if(w.Focus is TextView) {
                        var v = w.Focus as TextView;
                        v.Buffer.PasteClipboard(v.GetClipboard(Gdk.Selection.Clipboard));
                    }
                }
            } else if(sender == _selectAllItem) {
                var w = GUIApplication.CurrentFocusedWindow;
                if(w != null) {
                    if(w == this) {
                        ManageSelectAll();
                    }

                    if(w.Focus is Editable) {
                        (w.Focus as Editable).SelectRegion(0, -1);
                    } else if(w.Focus is TextView) {
                        var v = w.Focus as TextView;
                        TextIter start = v.Buffer.GetIterAtOffset(0), end = v.Buffer.GetIterAtOffset(0);
                        end.ForwardToEnd();

                        v.Buffer.SelectRange(start, end);
                    }
                }
            } else if(sender == _closeItem) {
                var w = GUIApplication.CurrentFocusedWindow;
                if(w != null) {
                    if(w == this) {
                        if(TryClose()) {
                            if(!IsCached) {
                                w.Destroy();
                            }
                        }
                    }
                }
            } else if(sender == _welcomeScreenItem) {
                GUIApplication.WelcomeScreen.ShowWindow();
            } else if(sender == _aboutItem) {
                GUIApplication.AboutPanel.ShowWindow();
            }
        }

        /// <summary>
        /// Called when a static menu item is selected.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="args">Arguments.</param>
        protected static void OnClickMenuStatic(object sender, EventArgs args)
        {
            if(sender == _staticQuitItem && QuitSensitive) {
                GUIApplication.SaveAndQuit();
            } else if(sender == _staticPreferencesItem) {
                GUIApplication.SettingsEditor.ShowWindow();
            } else if(sender == _staticAboutItem) {
                GUIApplication.AboutPanel.ShowWindow();
            }
        }

        /// <summary>
        /// Presents the window to the user.
        /// </summary>
        public virtual void ShowWindow()
        {
            ShowAll();
            Present();
        }

        /// <summary>
        /// Tries to close the window. If the window should not close, then this must return <c>false</c>. Else, this must return <c>true</c> and perform cleanup code.
        /// </summary>
        /// <returns><c>true</c>, the window should be closed, <c>false</c> otherwise.</returns>
        public virtual bool TryClose()
        {
            Hide();
            return true;
        }

        /// <summary>
        /// Gets a value indicating whether this <see cref="T:Petri.Application.GUI.MenuBarWindow"/> is cached.
        /// </summary>
        /// <value><c>true</c> if is cached; otherwise, <c>false</c>.</value>
        protected virtual bool IsCached {
            get {
                return true;
            }
        }
        /// <summary>
        /// Called when the window is closed by its close button.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="args">Arguments.</param>
        protected void OnDeleteEvent(object sender, DeleteEventArgs args)
        {
            bool result = TryClose();

            if(IsCached || !result) {
                args.RetVal = true;
            } else {
                args.RetVal = false;
            }
        }

        /// <summary>
        /// Manages the copy operation.
        /// </summary>
        protected virtual void ManageCopy()
        {

        }

        /// <summary>
        /// Manages the cut operation.
        /// </summary>
        protected virtual void ManageCut()
        {

        }

        /// <summary>
        /// Manages the paste operation.
        /// </summary>
        protected virtual void ManagePaste()
        {

        }

        /// <summary>
        /// Manages the select all operation.
        /// </summary>
        protected virtual void ManageSelectAll()
        {

        }

        /// <summary>
        /// Gets or sets a value indicating whether the quit menu item is sensitive.
        /// </summary>
        /// <value><c>true</c> if the quit menu item is sensitive; otherwise, <c>false</c>.</value>
        public static bool QuitSensitive {
            get;
            set;
        }

        /// <summary>
        /// Creates a new tab in a Notebook and returns the VBox that will contain the widgets.
        /// </summary>
        /// <returns>The tab's container.</returns>
        /// <param name="nb">The notebook.</param>
        /// <param name="label">The tab's label.</param>
        public VBox CreateTab(Notebook nb, string label)
        {
            var box = new VBox(false, 5);
            var eb = new EventBox();
            nb.AppendPage(eb, GUIApplication.LabelFromString(label));
            eb.Add(box);

            // This is a fix to what appears to be an awkward GTK bug.
            // Indeed, a notebook's tab has a full white background on creation,
            // and that integrates poorly with other widgets.
            eb.ModifyBg(StateType.Normal, Style.Background(StateType.Normal));

            return box;
        }

        /// <summary>
        /// Recursively gathers all .petri files in the specified path.
        /// Empty directories are not added.
        /// Symbolic links are ignored.
        /// The items are added to the <paramref name="list"/> parameter. Strings are added to represents documents.
        /// <c>Tuple&lt;string, List&lt;object&gt;&gt;</c> are added when a directory is met, with the first element is
        /// the dir name and the second is the filesystem subtree of the dir.
        /// </summary>
        /// <param name="currentDirectory">Current directory.</param>
        /// <param name="list">The documents list.</param>
        static void FetchDocumentsInPath(string currentDirectory, List<object> list)
        {
            foreach(var file in System.IO.Directory.GetFiles(currentDirectory)) {
                if(System.IO.Path.GetExtension(file) == ".petri") {
                    list.Add(file);
                }
            }

            foreach(var dir in System.IO.Directory.GetDirectories(currentDirectory)) {
                // Ignore symlinks
                if(!new System.IO.FileInfo(dir).Attributes.HasFlag(System.IO.FileAttributes.ReparsePoint)) {
                    var subList = new List<object>();
                    FetchDocumentsInPath(dir, subList);
                    // Ignore empty directories
                    if(subList.Count > 0) {
                        list.Add(Tuple.Create(dir, subList));
                    }
                }
            }
        }

        /// <summary>
        /// Gets the examples paths.
        /// </summary>
        /// <value>The examples paths.</value>
        static List<object> ExamplesPaths {
            get {
                if(_examplesPaths == null) {
                    var rootPath = Configuration.ExamplesPath;
                    var examplesPaths = new List<object>();

                    if(System.IO.Directory.Exists(rootPath)) {
                        FetchDocumentsInPath(rootPath, examplesPaths);
                    }

                    if(examplesPaths.Count > 0) {
                        _examplesPaths = examplesPaths;
                    }
                }

                return _examplesPaths;
            }
        }

        /// <summary>
        /// The menu bar.
        /// </summary>
        protected MenuBar _menuBar;

        /// <summary>
        /// The file menu.
        /// </summary>
        protected Menu _fileMenu;

        /// <summary>
        /// The edit menu.
        /// </summary>
        protected Menu _editMenu;

        /// <summary>
        /// The view menu.
        /// </summary>
        protected Menu _viewMenu;

        /// <summary>
        /// The document menu.
        /// </summary>
        protected Menu _documentMenu;

        /// <summary>
        /// The debugger menu.
        /// </summary>
        protected Menu _debuggerMenu;

        /// <summary>
        /// The window menu.
        /// </summary>
        protected Menu _windowMenu;

        /// <summary>
        /// The help menu.
        /// </summary>
        protected Menu _helpMenu;


        //--------------------------------

        /// <summary>
        /// The quit item.
        /// </summary>
        protected MenuItem _quitItem;

        /// <summary>
        /// The about item.
        /// </summary>
        protected MenuItem _aboutItem;

        /// <summary>
        /// The preferences item.
        /// </summary>
        protected MenuItem _preferencesItem;

        /// <summary>
        /// The static quit item.
        /// </summary>
        protected static MenuItem _staticQuitItem;

        /// <summary>
        /// The static about item.
        /// </summary>
        protected static MenuItem _staticAboutItem;

        /// <summary>
        /// The static preferences item.
        /// </summary>
        protected static MenuItem _staticPreferencesItem;

        //--------------------------------

        /// <summary>
        /// The new item.
        /// </summary>
        protected MenuItem _newItem;

        /// <summary>
        /// The new menu.
        /// </summary>
        protected Menu _newMenu;

        /// <summary>
        /// The new items.
        /// </summary>
        protected Dictionary<MenuItem, Code.Language> _newItems;

        //--------------------------------

        /// <summary>
        /// The open item.
        /// </summary>
        protected MenuItem _openItem;

        /// <summary>
        /// The open recent item.
        /// </summary>
        protected MenuItem _openRecentItem;

        /// <summary>
        /// The open recent menu.
        /// </summary>
        protected Menu _openRecentMenu;

        /// <summary>
        /// The clear recent items.
        /// </summary>
        protected MenuItem _clearRecentItems;

        /// <summary>
        /// The close item.
        /// </summary>
        protected MenuItem _closeItem;

        /// <summary>
        /// The save item.
        /// </summary>
        protected MenuItem _saveItem;

        /// <summary>
        /// The save as item.
        /// </summary>
        protected MenuItem _saveAsItem;

        /// <summary>
        /// The export item.
        /// </summary>
        protected MenuItem _exportItem;

        /// <summary>
        /// The revert item.
        /// </summary>
        protected MenuItem _revertItem;

        /// <summary>
        /// The generate item.
        /// </summary>
        protected MenuItem _generateItem;

        /// <summary>
        /// The compile item.
        /// </summary>
        protected MenuItem _compileItem;

        /// <summary>
        /// The deploy item.
        /// </summary>
        protected MenuItem _deployItem;

        /// <summary>
        /// The show document item.
        /// </summary>
        protected MenuItem _showDocumentItem;

        /// <summary>
        /// The show code item.
        /// </summary>
        protected MenuItem _showCodeItem;

        /// <summary>
        /// The show compiler output item.
        /// </summary>
        protected MenuItem _showCompilerOutputItem;

        //--------------------------------

        /// <summary>
        /// The undo item.
        /// </summary>
        protected MenuItem _undoItem;

        /// <summary>
        /// The redo item.
        /// </summary>
        protected MenuItem _redoItem;

        /// <summary>
        /// The cut item.
        /// </summary>
        protected MenuItem _cutItem;

        /// <summary>
        /// The copy item.
        /// </summary>
        protected MenuItem _copyItem;

        /// <summary>
        /// The paste item.
        /// </summary>
        protected MenuItem _pasteItem;

        /// <summary>
        /// The select all item.
        /// </summary>
        protected MenuItem _selectAllItem;

        /// <summary>
        /// The find item.
        /// </summary>
        protected MenuItem _findItem;

        /// <summary>
        /// The embed in macro item.
        /// </summary>
        protected MenuItem _embedInMacroItem;

        /// <summary>
        /// The import petri net item.
        /// </summary>
        protected MenuItem _importPetriNetItem;

        //--------------------------------

        /// <summary>
        /// The show editor item.
        /// </summary>
        protected MenuItem _showEditorItem;

        /// <summary>
        /// The show debugger item.
        /// </summary>
        protected MenuItem _showDebuggerItem;

        /// <summary>
        /// The known functions item.
        /// </summary>
        protected MenuItem _knownFunctionsItem;

        /// <summary>
        /// The manage headers item.
        /// </summary>
        protected MenuItem _manageHeadersItem;

        /// <summary>
        /// The manage macros item.
        /// </summary>
        protected MenuItem _manageMacrosItem;

        /// <summary>
        /// The profiles menu item.
        /// </summary>
        protected MenuItem _profilesItem;

        /// <summary>
        /// The profiles menu.
        /// </summary>
        protected Menu _profilesMenu;


        /// <summary>
        /// The document settings item.
        /// </summary>
        protected MenuItem _documentSettingsItem;

        //--------------------------------

        /// <summary>
        /// The actual size item.
        /// </summary>
        protected MenuItem _actualSizeItem;

        /// <summary>
        /// The zoom in item.
        /// </summary>
        protected MenuItem _zoomInItem;

        /// <summary>
        /// The zoom out item.
        /// </summary>
        protected MenuItem _zoomOutItem;

        //--------------------------------

        /// <summary>
        /// The clear breakpoints item.
        /// </summary>
        protected MenuItem _clearBreakpointsItem;

        /// <summary>
        /// The run stop item.
        /// </summary>
        protected MenuItem _runStopItem;

        /// <summary>
        /// The attach detach item.
        /// </summary>
        protected MenuItem _attachDetachItem;

        /// <summary>
        /// The create host item.
        /// </summary>
        protected MenuItem _createHostItem;

        /// <summary>
        /// The pause item.
        /// </summary>
        protected MenuItem _pauseItem;

        /// <summary>
        /// The show variable item.
        /// </summary>
        protected MenuItem _showVarItem;

        /// <summary>
        /// The exit item.
        /// </summary>
        protected MenuItem _exitItem;

        /// <summary>
        /// The reload petri item.
        /// </summary>
        protected MenuItem _reloadPetriItem;

        /// <summary>
        /// The clear console menu item.
        /// </summary>
        protected MenuItem _clearConsoleItem;

        /// <summary>
        /// The lock/unlock console menu item.
        /// </summary>
        protected MenuItem _lockConsoleItem;

        /// <summary>
        /// The sync with editor menu item.
        /// </summary>
        protected MenuItem _syncWithEditorItem;

        //--------------------------------

        /// <summary>
        /// The welcome screen menu item.
        /// </summary>
        protected RadioMenuItem _welcomeScreenItem;

        //--------------------------------

        /// <summary>
        /// The show help item.
        /// </summary>
        protected MenuItem _showHelpItem;

        /// <summary>
        /// The accel group.
        /// </summary>
        protected AccelGroup _accelGroup;
        static AccelGroup _staticAccelGroup;

        /// <summary>
        /// The vbox of the window.
        /// </summary>
        protected VBox _vbox;

        bool _noMenuBar;

        static List<object> _examplesPaths;
    }
}
