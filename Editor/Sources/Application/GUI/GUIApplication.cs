//
//  GUIApplication.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2014-11-13.
//

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Gtk;
using IgeMacIntegration;
using Newtonsoft.Json;

namespace Petri.Application.GUI
{
    /// <summary>
    /// The entry point class of the application when used in graphical mode.
    /// </summary>
    public static class GUIApplication
    {
        /// <summary>
        /// The entry point of the program when in GUI mode.
        /// </summary>
        /// <returns>The return code of the program.</returns>
        /// <param name="docsToOpen">An optional list of documents to open at launch. May be <c>null</c>.</param>
        /// <param name="player">Whether the app is to be loaded in player mode.</param>
        public static int Main(IEnumerable<string> docsToOpen, bool player)
        {
            _mainThread = System.Threading.Thread.CurrentThread;
            GLib.ExceptionManager.UnhandledException += (GLib.UnhandledExceptionArgs args) => {
                Application.OnExceptionUnhandled(new UnhandledExceptionEventArgs(args.ExceptionObject, false));
            };

            Application.ExceptionUnhandled += (UnhandledExceptionEventArgs e) => {
                CLI.CLIApplication.OnUnhandledException(e);
                NotifyUnrecoverableError(null, Configuration.GetLocalized("Uncaught exception: {0}", Application.GetExceptionMessage(e.ExceptionObject)));
            };

            IsReadOnly = player;

            Gtk.Application.Init();

            if(!Configuration.CheckTrialPeriod()) {
                var d = new MessageDialog(null,
                                          DialogFlags.Modal,
                                          MessageType.Error,
                                          ButtonsType.None,
                                          GUIApplication.SafeMarkupFromLocalized("The trial period is expired. Please contact the support."));
                d.AddButton(Configuration.GetLocalized("OK"), ResponseType.Cancel);
                RunModalDialog(d);
                d.Destroy();
                Gtk.Application.Quit();

                return 0;
            }

            if(Configuration.RunningPlatform == Platform.Mac) {
                MonoDevelop.MacInterop.ApplicationEvents.Quit += (object sender, MonoDevelop.MacInterop.ApplicationQuitEventArgs e) => {
                    if(MenuBarWindow.QuitSensitive) {
                        GUIApplication.SaveAndQuit();
                    }
                    // If we get here, the user has cancelled the action
                    e.UserCancelled = true;
                    e.Handled = true;
                };

                MonoDevelop.MacInterop.ApplicationEvents.OpenDocument += (object sender, MonoDevelop.MacInterop.ApplicationDocumentEventArgs e) => {
                    foreach(var pair in e.Documents) {
                        GUIApplication.OpenDocument(pair.Key);
                    }

                    e.Handled = true;
                };
            }

            MenuBarWindow.InitMenuBar();

            var entries = JsonConvert.DeserializeObject<SerializableRecentDocumentEntry[]>(Configuration.RecentDocuments);
            if(entries != null) {
                foreach(var entry in entries) {
                    if(entry != null) {
                        var e = new RecentDocumentEntry();
                        e.Date = DateTime.Parse(entry.Date);
                        e.Path = entry.Path;
                        _recentDocuments.Add(e);
                    }
                }
            }
            _recentDocuments.Sort((doc1, doc2) => doc2.Date.CompareTo(doc1.Date));
            TrimRecentDocuments();

            WelcomeScreen = new WelcomeScreen();

            if(docsToOpen != null) {
                bool openedOne = false;
                foreach(string path in docsToOpen) {
                    openedOne = OpenDocument(path) || openedOne;
                }

                if(!openedOne) {
                    WelcomeScreen.ShowWindow();
                }
            } else {
                WelcomeScreen.ShowWindow();
            }

            if(Configuration.IsOnTrial) {
                var d = new MessageDialog(null,
                                          DialogFlags.Modal,
                                          MessageType.Info,
                                          ButtonsType.None,
                                          GUIApplication.SafeMarkupFromLocalized("{0} days until trial period expiration.", Configuration.RemainingTrialDuration));
                d.AddButton(Configuration.GetLocalized("OK"), ResponseType.Cancel);
                RunModalDialog(d);
                d.Destroy();
            }

            Gtk.Application.Run();

            return 0;
        }

        /// <summary>
        /// Notifies the user from an unrecoverable error, i.e. an error message with no action
        /// that the user can undertake to fix.
        /// </summary>
        /// <param name="parent">The window to attach the error dialog to.</param>
        /// <param name="message">The error message.</param>
        public static void NotifyUnrecoverableError(Window parent, string message)
        {
            RunOnUIThread(
                () => {
                    var d = new MessageDialog(parent,
                                              DialogFlags.Modal,
                                              MessageType.Error,
                                              ButtonsType.None,
                                              GUIApplication.SafeMarkupFromString(message));
                    d.AddButton(Configuration.GetLocalized("Cancel"), ResponseType.Cancel);
                    RunModalDialog(d);
                    d.Destroy();
                },
                true
            );
        }

        /// <summary>
        /// Runs the specified Action on the GUI thread.
        /// Typically useful for interacting with the GUI widgets on an arbitrary thread without crashing the application.
        /// If an exception occur in the action, then it is caught and outputted on stderr.
        /// </summary>
        /// <param name="action">The Action to execute on the GUI thread.</param>
        /// <param name="forceDelay">If <c>true</c>, forces the Action to be run on the next iteration of the GUI's run loop.</param>
        public static void RunOnUIThread(System.Action action, bool forceDelay = false)
        {
            Application.SwallowAsync(RunOnUIThread(() => {
                action();
                return true;
            }, forceDelay));
        }

        /// <summary>
        /// Runs the specified Func on the GUI thread.
        /// Typically useful for interacting with the GUI widgets on an arbitrary thread without crashing the application.
        /// If an exception occur in the action, then it is caught and outputted on stderr.
        /// </summary>
        /// <param name="action">The Func to execute on the GUI thread.</param>
        /// <param name="forceDelay">If <c>true</c>, forces the Func to be run on the next iteration of the GUI's run loop.</param>
        public static async Task<TResult> RunOnUIThread<TResult>(Func<TResult> action, bool forceDelay = false)
        {
            if(System.Threading.Thread.CurrentThread == _mainThread && !forceDelay) {
                return action();
            }

            var task = new Task<TResult>(action);
            GLib.Timeout.Add(0, () => {
                try {
                    task.RunSynchronously();
                } catch(Exception e) {
                    Console.Error.WriteLine("Exception in Application.RunOnUIThread: {0}.", e);
                }
                return false;

            });

            return await task;
        }

        /// <summary>
        /// Closes and asks for save for each unsaved document. If one of the save dialog is cancelled, then the exit is cancelled.
        /// </summary>
        /// <returns>Whether the app termination should continue.</returns>
        static bool OnExit()
        {
            _exiting = true;
            while(_documents.Count > 0) {
                if(!_documents[_documents.Count - 1].CloseAndConfirm()) {
                    _exiting = false;
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Closes every open document (asks saving if necessary), saves the app's configuration and exits the program.
        /// </summary>
        public static void SaveAndQuit()
        {
            bool exit = OnExit();
            if(!exit) {
                return;
            }

            Configuration.Save();
            Gtk.Application.Quit();
        }

        /// <summary>
        /// Adds a path to the current documents list and updates the recent documents menu item of the opened documents.
        /// </summary>
        /// <param name="path">Path.</param>
        public static void AddRecentDocument(string path)
        {
            int index = _recentDocuments.FindIndex((RecentDocumentEntry obj) => obj.Path == path);
            if(index != -1) {
                _recentDocuments.RemoveAt(index);
            }

            var entry = new RecentDocumentEntry();
            entry.Date = DateTime.UtcNow;
            entry.Path = path;
            _recentDocuments.Insert(0, entry);

            while(_recentDocuments.Count > Configuration.MaxRecentDocuments) {
                _recentDocuments.RemoveAt(_recentDocuments.Count - 1);
            }

            Configuration.RecentDocuments = JsonConvert.SerializeObject(_recentDocuments.ToArray());
        }

        /// <summary>
        /// Adds a document to the list of opened documents.
        /// </summary>
        /// <param name="doc">The document to register.</param>
        public static void AddDocument(GUIDocument doc)
        {
            _documents.Add(doc);
            WelcomeScreen.TryClose();
        }

        /// <summary>
        /// Removes a document from the list of opened documents.
        /// </summary>
        /// <param name="doc">The document to unregister.</param>
        public static void RemoveDocument(GUIDocument doc)
        {
            _documents.Remove(doc);
            if(!_exiting && _documents.Count == 0) {
                WelcomeScreen.ShowWindow();
            }
        }

        /// <summary>
        /// The list of opened documents
        /// </summary>
        /// <value>The documents.</value>
        public static IReadOnlyList<GUIDocument> Documents {
            get {
                return _documents;
            }
        }

        /// <summary>
        /// Gets the current focused window.
        /// </summary>
        /// <value>The current focused window.</value>
        public static Window CurrentFocusedWindow {
            get {
                foreach(Window w in Window.ListToplevels()) {
                    if(w.HasToplevelFocus) {
                        return w;
                    }
                }

                return null;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this <see cref="T:Petri.Application.GUI.GUIApplication"/> is read only.
        /// </summary>
        /// <value><c>true</c> if is read only; otherwise, <c>false</c>.</value>
        public static bool IsReadOnly {
            get;
            private set;
        }

        /// <summary>
        /// Returns a document's path selected with the file chooser dialog.
        /// </summary>
        /// <param name="tunePanel">A callable that is given an opportunity to apply some chanes to the file chooser panel before it is presented to the user.</param>
        /// <param name="retrieveResult">A callable that is invoked right after the user vaidated its choice and can perform some finalization actions on the items changed by <paramref name="tunePanel"/>.</param>
        /// <returns><c>null</c> if the opening was cancelled, the document's path otherwise.</returns>
        public static string SelectDocumentPath(Action<FileChooserDialog> tunePanel = null, Action<FileChooserDialog> retrieveResult = null)
        {
            // Prevents 2 dialogs from being opened at the same time
            if(!_opening) {
                try {
                    _opening = true;
                    var fc = new FileChooserDialog(Configuration.GetLocalized("Open Petri Net…"),
                                                       null,
                                                       FileChooserAction.Open,
                                                       new object[] {Configuration.GetLocalized("Cancel"),
                        ResponseType.Cancel,
                        Configuration.GetLocalized("Open"), ResponseType.Accept
                    });

                    var filter = new FileFilter();
                    filter.Name = "Petri";
                    filter.AddPattern("*.petri");
                    fc.AddFilter(filter);

                    if(tunePanel != null) {
                        tunePanel(fc);
                    }

                    if(GUIApplication.RunModalDialog(fc) == ResponseType.Accept) {
                        string filename = fc.Filename;

                        if(retrieveResult != null) {
                            retrieveResult(fc);
                        }

                        fc.Destroy();

                        return filename;
                    } else {
                        fc.Destroy();
                    }
                } finally {
                    _opening = false;
                }
            }

            return null;
        }

        /// <summary>
        /// Opens a document through the file chooser dialog.
        /// </summary>
        /// <returns><c>false</c> if the opening was cancelled.</returns>
        public static bool OpenDocument()
        {
            var filename = SelectDocumentPath();
            if(filename != null) {
                OpenDocument(filename);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Opens a document by its path. If the document is already opened, then its window is brought to front.
        /// </summary>
        /// <param name="filename">Filename.</param>
        public static bool OpenDocument(string filename)
        {
            AddRecentDocument(filename);

            foreach(var d in _documents) {
                if(d.Path == filename) {
                    d.Window.Present();
                    return true;
                }
            }

            try {
                var doc = new GUIDocument(filename);
                AddDocument(doc);
                return true;
            } catch(Exception e) {
                NotifyUnrecoverableError(null, Configuration.GetLocalized("Unable to open document {0}: {1}", filename, e.GetBaseException().Message));
                return false;
            }
        }

        /// <summary>
        /// Creates an empty document.
        /// </summary>
        /// <param name="language">The language the source code will be generated in.</param>
        /// <param name="runInEditor">If set to <c>true</c>, the petri net will run in editor.</param>
        public static void CreateDocument(Code.Language language, bool runInEditor = false)
        {
            var doc = new GUIDocument(language);
            if(runInEditor && language != Code.Language.EmbeddedC) {
                doc.Settings.CurrentProfile.DebugMode = DebugMode.RunInEditor;
            }
            GUIApplication.AddDocument(doc);

            if(!doc.SaveAs()) {
                doc.CloseAndConfirm();
            }
        }

        /// <summary>
        /// Recent document entry.
        /// </summary>
        public class RecentDocumentEntry
        {
            /// <summary>
            /// The date of last opening of the document.
            /// </summary>
            public DateTime Date;

            /// <summary>
            /// The path of the document.
            /// </summary>
            public string Path;
        }

        class SerializableRecentDocumentEntry
        {
            // This disables false positves: the compiler believes that these fields are never assigned to and will always be null.
            // They are assigned by the JSON deserialization process.
#pragma warning disable 0649
            public string Date;
            public string Path;
#pragma warning restore 0649
        }

        /// <summary>
        /// Remove the documents that do not exist anymore on the filesystem.
        /// </summary>
        public static void TrimRecentDocuments()
        {
            var dict = new List<RecentDocumentEntry>(_recentDocuments);

            for(int i = 0, j = 0; i < dict.Count; ++i) {
                if(!System.IO.File.Exists(dict[i].Path)) {
                    _recentDocuments.RemoveAt(j);
                } else {
                    ++j;
                }
            }
        }

        /// <summary>
        /// Gets the list of recent documents, indexed by their last opening date (UTC).
        /// </summary>
        /// <value>The recent documents.</value>
        public static IReadOnlyList<RecentDocumentEntry> RecentDocuments {
            get {
                return _recentDocuments;
            }
        }

        /// <summary>
        /// Clears the list of recent documents.
        /// </summary>
        public static void ClearRecentDocuments()
        {
            _recentDocuments.Clear();

            Configuration.RecentDocuments = JsonConvert.SerializeObject(_recentDocuments.ToArray());
        }

        /// <summary>
        /// Gets the welcome screen of the application.
        /// </summary>
        /// <value>The welcome screen.</value>
        public static WelcomeScreen WelcomeScreen {
            get;
            private set;
        }

        /// <summary>
        /// Gets the settings editor of the application.
        /// </summary>
        /// <value>The settings editor.</value>
        public static SettingsEditor SettingsEditor {
            get {
                if(_settingsEditor == null) {
                    _settingsEditor = new SettingsEditor();
                }

                return _settingsEditor;
            }
        }

        /// <summary>
        /// Gets the about panel of the application.
        /// </summary>
        /// <value>The about panel.</value>
        public static AboutPanel AboutPanel {
            get {
                if(_aboutPanel == null) {
                    _aboutPanel = new AboutPanel();
                }

                return _aboutPanel;
            }
        }

        /// <summary>
        /// Presents a modal dialog to the user and synchronously returns the user's choice.
        /// </summary>
        /// <returns>The response selected by the user.</returns>
        /// <param name="dialog">Dialog.</param>
        public static async Task<ResponseType> RunModalDialogAsync(Dialog dialog)
        {
            return await RunOnUIThread(() => {
                bool quitActive = MenuBarWindow.QuitSensitive;

                try {
                    dialog.FocusInEvent += (o, args) => {
                        // This takes charge of disabling the menu bar items of the document while the dialog is on screen.
                        if(Configuration.RunningPlatform == Platform.Mac) {
                            IgeMacMenu.MenuBar = WelcomeScreen.MenuBar;
                        }

                        // Cannot quit.
                        MenuBarWindow.QuitSensitive = false;
                    };

                    return (ResponseType)dialog.Run();
                } finally {
                    MenuBarWindow.QuitSensitive = quitActive;
                }
            });
        }

        /// <summary>
        /// Presents a modal dialog to the user and return the user's choice.
        /// </summary>
        /// <returns>The response selected by the user.</returns>
        /// <param name="dialog">Dialog.</param>
        public static ResponseType RunModalDialog(Dialog dialog)
        {
            return RunModalDialogAsync(dialog).Result;
        }

        /// <summary>
        /// Entry validation delegate.
        /// </summary>
        /// <param name="e">The Entry widget.</param>
        /// <param name="focusOut"><c>true</c> if the validation event has been sent because the widget went out of focus.</param>
        /// <param name="args">User-provided arguments.</param>
        public delegate void EntryValidationDel(Entry e, bool focusOut, params object[] args);

        /// <summary>
        /// Registers the specified delegate to be triggered when the Entry is changed (if <c>change == true</c>)
        /// or validated (if <c>change == false</c>; the validation is when the widget loses its focus or the Enter key is pressed).
        /// </summary>
        /// <param name="e">The Entry widget.</param>
        /// <param name="change">If set to <c>true</c> then the delegate will be invoked on each text change in the entry. Otherwise, only on end of input.</param>
        /// <param name="del">The delegate that will be invoked.</param>
        /// <param name="p">The arguments list that will be passed to the delegate upon invocation.</param>
        public static void RegisterValidation(Entry e,
                                              bool change,
                                              EntryValidationDel del,
                                              params object[] p)
        {
            if(change) {
                e.Changed += (obj, eventInfo) => {
                    del(e, false, p);
                };
            } else {
                e.FocusOutEvent += (obj, eventInfo) => {
                    del(e, true, p);
                };
                e.Activated += (obj, args) => {
                    del(e, false, p);
                };
            }
        }

        /// <summary>
        /// Takes a string and escapes it appropriately for usage in different GTK widgets that parses markup language.
        /// </summary>
        /// <returns>The escaped string.</returns>
        /// <param name="s">The string to escape.</param>
        public static string SafeMarkupFromString(string s)
        {
            return System.Net.WebUtility.HtmlEncode(s).Replace("{", "{{").Replace("}", "}}");
        }

        /// <summary>
        /// <c>SafeMarkupFromString(Configuration.GetLocalized(key, args))</c>.
        /// </summary>
        /// <returns>The escaped string.</returns>
        /// <param name="key">The localization key.</param>
        /// <param name="args">The localization arguments.</param>
        public static string SafeMarkupFromLocalized(string key, params object[] args)
        {
            return SafeMarkupFromString(Configuration.GetLocalized(key, args));
        }

        /// <summary>
        /// Creates a GUI label from a string
        /// </summary>
        /// <returns>The label.</returns>
        /// <param name="text">The text of the label.</param>
        /// <param name="centered"><c>true</c> for the text of the label to be centered, <c>false</c> otherwise.</param>
        public static Label LabelFromString(string text, bool centered = false)
        {
            var label = new Label();
            label.Text = text;
            label.LineWrap = true;
            if(!centered) {
                label.SetAlignment(0, 0);
            } else {
                label.Justify = Justification.Center;
            }
            return label;
        }

        /// <summary>
        /// Creates a GUI label from markup
        /// </summary>
        /// <returns>The label.</returns>
        /// <param name="markup">The markup.</param>
        /// <param name="centered"><c>true</c> for the text of the label to be centered, <c>false</c> otherwise.</param>
        public static Label LabelFromMarkup(string markup, bool centered = false)
        {
            var label = new Label();
            label.Markup = markup;
            label.LineWrap = true;
            if(!centered) {
                label.SetAlignment(0, 0);
            } else {
                label.Justify = Justification.Center;
            }
            return label;
        }

        /// <summary>
        /// Creates a ComboBox with the specified text items, and selects the item that matches <paramref name="selectedItem"/>, if any.
        /// </summary>
        /// <returns>The new widget.</returns>
        /// <param name="selectedItem">Selected item.</param>
        /// <param name="items">Items.</param>
        /// <param name="hasEntry"><c>true</c> if the combox box has to have a text field in it.</param>
        public static ComboBox ComboHelper(string selectedItem, IEnumerable<string> items, bool hasEntry = false)
        {
            ComboBox combo;
            if(hasEntry) {
                combo = ComboBoxEntry.NewText();
                ((Entry)combo.Child).Text = selectedItem;
            } else {
                combo = ComboBox.NewText();
            }

            int active = -1;
            int i = 0;
            foreach(string text in items) {
                combo.AppendText(text);
                if(text == selectedItem) {
                    active = i;
                }
                ++i;
            }

            if(active != -1) {
                combo.Active = active;
            }

            return combo;
        }

        /// <summary>
        /// Opens the given folder in the system's file explorer.
        /// </summary>
        /// <param name="folder">Folder.</param>
        public static void OpenFileExplorer(string folder)
        {
            var process = Application.CreateProcess();
            if(Configuration.RunningPlatform == Platform.Linux) {
                process.StartInfo.FileName = "xdg-open";
                process.StartInfo.Arguments = string.Format("'{0}'", folder); ;
            } else if(Configuration.RunningPlatform == Platform.Mac) {
                process.StartInfo.FileName = "open";
                process.StartInfo.Arguments = string.Format("'{0}'", folder); ;
            } else if(Configuration.RunningPlatform == Platform.Windows) {
                process.StartInfo.FileName = "file://";
                process.StartInfo.Arguments = System.Web.HttpUtility.UrlEncode(folder);
            }

            process.Start();
        }

        static List<GUIDocument> _documents = new List<GUIDocument>();
        static List<RecentDocumentEntry> _recentDocuments = new List<RecentDocumentEntry>();
        static bool _opening = false;
        static SettingsEditor _settingsEditor;
        static AboutPanel _aboutPanel;
        static bool _exiting;
        static System.Threading.Thread _mainThread;
    }
}

