//
//  DocumentSettingsEditor.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2014-12-07.
//

using System;
using System.Linq;
using System.Text.RegularExpressions;

using Gtk;

namespace Petri.Application.GUI
{
    /// <summary>
    /// The document's settings' editor.
    /// </summary>
    public class DocumentSettingsEditor : DocumentHelperWindow
    {
        /// <summary>
        /// A page of the tabbed editor.
        /// </summary>
        public enum EditorPage
        {
            /// <summary>
            /// The current tab.
            /// </summary>
            Current = -1,

            /// <summary>
            /// The code gen tab.
            /// </summary>
            Generation = 0,

            /// <summary>
            /// The compilation tab.
            /// </summary>
            Compilation = 1,

            /// <summary>
            /// The compiler tab.
            /// </summary>
            Deployment = 2,

            /// <summary>
            /// The debugging tab.
            /// </summary>
            Debugging = 3
        }

        /// <summary>
        /// Gets or sets the edited profile.
        /// </summary>
        /// <value>The edited profile.</value>
        public DocumentSettingsProfile EditedProfile {
            get;
            set;
        }

        /// <summary>
        /// Gets the index of the edited profile.
        /// </summary>
        /// <value>The index of the edited profile.</value>
        public int EditedProfileIndex {
            get {
                return Settings.Profiles.IndexOf(EditedProfile);
            }
        }

        /// <summary>
        /// Gets the settings.
        /// </summary>
        /// <value>The settings.</value>
        public new DocumentSettings Settings {
            get;
            private set;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.DocumentSettingsEditor"/> class.
        /// </summary>
        /// <param name="doc">Document.</param>
        public DocumentSettingsEditor(GUIDocument doc) : base(doc, 550, 700)
        {
            Settings = doc.Settings;
            _vbox.BorderWidth = 0;

            EditedProfile = Settings.CurrentProfile;

            var hbox = new HBox(false, 5);
            {
                var vbox = new VBox(false, 5);
                FillCommon(vbox);
                vbox.PackStart(new HSeparator(), false, false, 10);
                FillProfilesList(vbox);

                _overrideProfiles = new CheckButton(Configuration.GetLocalized("Override child documents' compiler"));
                _overrideProfiles.Toggled += (sender, e) => {
                    if(_updating) {
                        return;
                    }

                    var newSettings = Settings.Clone();
                    newSettings.OverrideChildCompilationSettings = _overrideProfiles.Active;
                    CommitGuiAction(new ChangeSettingsAction(this, newSettings.GetXml(), _overrideProfiles, null));
                };
                vbox.PackStart(_overrideProfiles, false, false, 5);
                _overrideProfiles.Sensitive = !GUIApplication.IsReadOnly;

                hbox.PackStart(vbox, false, false, 0);
            }

            hbox.PackStart(new VSeparator(), false, false, 10);

            {
                var vbox = new VBox(false, 5);

                _profileNameLabel = GUIApplication.LabelFromString("");
                vbox.PackStart(_profileNameLabel, false, false, 0);

                _notebook = new Notebook();
                vbox.PackStart(_notebook, true, true, 0);

                var codegen = CreateTab(_notebook, Configuration.GetLocalized("Generation document settings tab"));
                FillGenerationTab(codegen);

                var compilation = CreateTab(_notebook, Configuration.GetLocalized("Compilation document settings tab"));
                FillCompilerTab(compilation);

                var deployment = CreateTab(_notebook, Configuration.GetLocalized("Deployment document settings tab"));
                FillDeploymentTab(deployment);

                var debug = CreateTab(_notebook, Configuration.GetLocalized("Debug document settings tab"));
                FillDebuggerTab(debug);

                hbox.PackStart(vbox, true, true, 0);
            }

            _vbox.PackStart(hbox, true, true, 0);
        }

        void FillProfilesList(VBox vbox)
        {
            var label = GUIApplication.LabelFromString(Configuration.GetLocalized("Document's profiles:"));
            vbox.PackStart(label, false, false, 0);

            _profilesList = new TreeView();
            _profilesStore = new ListStore(typeof(string));
            _profilesList.Model = _profilesStore;
            _profilesList.Selection.Changed += (o, args) => {
                if(_updating) {
                    return;
                }

                Focus = _profilesList;
                _profilesList.Selection.GetSelected(out var iter);
                EditedProfile = Settings.Profiles[_profilesStore.GetPath(iter).Indices[0]];
                UpdateUI();
            };

            var cell = new CellRendererText();
            var column = new TreeViewColumn(
                Configuration.GetLocalized("Profile name"),
                cell,
                "text", 0
            );
            _profilesList.AppendColumn(column);

            cell.Editable = !GUIApplication.IsReadOnly;
            cell.Edited += (object sender, EditedArgs args) => {
                if(_updating) {
                    return;
                }

                try {
                    var path = new TreePath(args.Path);
                    var prof = Settings.Profiles[path.Indices[0]];
                    if(prof.Name != args.NewText) {
                        foreach(var p in Settings.Profiles) {
                            if(p.Name == args.NewText) {
                                args.RetVal = false;
                                throw new Exception();
                            }
                        }

                        var newSettings = Settings.Clone();
                        newSettings.Profiles[int.Parse(args.Path)].Name = args.NewText;
                        if(prof == Settings.CurrentProfile) {
                            _profilesStore.GetIter(out var iter, path);
                            _profilesStore.SetValue(iter, 0, Configuration.GetLocalized("{0} (current profile)", prof.Name));
                        }
                        CommitGuiAction(new ChangeSettingsAction(this, newSettings.GetXml(), _profilesList, null));
                    }
                } catch {
                    GUIApplication.NotifyUnrecoverableError(
                        this,
                        Configuration.GetLocalized("A profile named {0} already exists!", args.NewText)
                    );
                }
            };

            cell.EditingStarted += (o, args) => {
                if(_updating) {
                    return;
                }
                _updating = true;
                TreePath path = new TreePath(args.Path);

                var prof = Settings.Profiles[path.Indices[0]];
                if(prof == Settings.CurrentProfile) {
                    _profilesStore.GetIter(out var iter, path);
                    _profilesStore.SetValue(iter, 0, prof.Name);
                }

                GUIApplication.RunOnUIThread(() => {
                    _profilesList.SetCursorOnCell(path, column, cell, true);
                    _updating = false;
                }, true);
            };

            var buttonsBox = new HBox(false, 5);
            var add = new Button(GUIApplication.LabelFromString(Configuration.GetLocalized("Add profile"), true));
            var remove = new Button(GUIApplication.LabelFromString(Configuration.GetLocalized("Remove profile"), true));
            var duplicate = new Button(GUIApplication.LabelFromString(Configuration.GetLocalized("Duplicate profile"), true));
            buttonsBox.PackStart(add, false, false, 0);
            buttonsBox.PackStart(remove, false, false, 0);
            buttonsBox.PackStart(duplicate, false, false, 0);

            add.Clicked += (sender, e) => {
                var name = GetAvailableProfileNameForPattern("", Configuration.GetLocalized("New profile {0}"));
                var profile = DocumentSettingsProfile.GetDefaultProfile(Settings.Language);
                profile.Name = name;
                var newSettings = Settings.Clone();
                newSettings.Profiles.Add(profile);
                CommitGuiAction(new ChangeSettingsAction(this, newSettings.GetXml(), _profilesList, null, Settings.Profiles.Count));
            };

            remove.Clicked += (sender, e) => {
                if(Settings.Profiles.Count > 1) {
                    var index = Settings.Profiles.IndexOf(EditedProfile);

                    var newSettings = Settings.Clone();
                    newSettings.Profiles.RemoveAt(index);
                    if(EditedProfile == Settings.CurrentProfile) {
                        if(!ChangeProfile(newSettings.Profiles[0]).Result) {
                            return;
                        }
                    }
                    CommitGuiAction(new ChangeSettingsAction(this, newSettings.GetXml(), _profilesList, null, 0));
                }
            };

            duplicate.Clicked += (sender, e) => {
                var newSettings = Settings.Clone();
                var name = GetAvailableProfileNameForPattern(EditedProfile.Name, " " + Configuration.GetLocalized("profile (copy {0})"));
                var clone = EditedProfile.Clone();
                clone.Name = name;
                newSettings.Profiles.Add(clone);

                CommitGuiAction(new ChangeSettingsAction(this, newSettings.GetXml(), _profilesList, null, Settings.Profiles.Count));
            };

            vbox.PackStart(_profilesList, true, true, 0);
            if(!GUIApplication.IsReadOnly) {
                vbox.PackStart(buttonsBox, false, false, 0);
            }
        }

        void FillCommon(VBox vbox)
        {
            var labelLanguage = GUIApplication.LabelFromMarkup(
                "<span foreground=\"#505050\"><i>"
                    + Configuration.GetLocalized("Petri net's language: {0}", Settings.LanguageName())
                    + "</i></span>"
            );
            var labelName = GUIApplication.LabelFromString(
                Configuration.GetLocalized("{language} name of the Petri net:", Settings.LanguageName())
            );

            _nameEntry = new Entry();
            _nameEntry.IsEditable = !GUIApplication.IsReadOnly;
            GUIApplication.RegisterValidation(_nameEntry, false, (obj, focusOut, p) => {
                if(_updating) {
                    return;
                }

                Regex name = new Regex(Code.Parser.GetNamePattern(true));
                Match nameMatch = name.Match(obj.Text);

                if(!nameMatch.Success || nameMatch.Value != obj.Text) {
                    GUIApplication.NotifyUnrecoverableError(
                        this,
                        Configuration.GetLocalized(
                            "The Petri net's name is not a valid <language> identifier.",
                            Settings.LanguageName()
                        )
                    );
                    obj.Text = Settings.Name;
                } else {
                    if(Settings.Name != _nameEntry.Text) {
                        var newSettings = Settings.Clone();
                        newSettings.Name = _nameEntry.Text;
                        CommitGuiAction(new ChangeSettingsAction(this, newSettings.GetXml(), _nameEntry, null), focusOut);
                    }
                }
            });

            vbox.PackStart(labelLanguage, false, false, 3);
            if(Settings.Language == Code.Language.Python) {
                vbox.PackStart(GUIApplication.LabelFromString(Configuration.GetLocalized("Language's version:")), false, false, 0);

                _pythonVersionCombo = ComboBox.NewText();
                var runtime2 = Configuration.GetPythonRuntimeInfo("2");
                var runtime3 = Configuration.GetPythonRuntimeInfo("3");
                string python2info = runtime2.Item1
                                         ? Configuration.GetLocalized("(detected version: {0})", runtime2.Item2)
                                         : Configuration.GetLocalized("(not detected)");
                string python3info = runtime3.Item1
                                         ? Configuration.GetLocalized("(detected version: {0})", runtime3.Item2)
                                         : Configuration.GetLocalized("(not detected)");
                _pythonVersionCombo.AppendText("Python 2 " + python2info);
                _pythonVersionCombo.AppendText("Python 3 " + python3info);
                _pythonVersionCombo.Changed += (sender, e) => {
                    if(_updating) {
                        return;
                    }

                    var newSettings = Settings.Clone();
                    newSettings.AdditionalLanguageInfo = _pythonVersionCombo.Active == 0 ? "2" : "3";
                    CommitGuiAction(new ChangeSettingsAction(this, newSettings.GetXml(), _customEnumEditor, null));
                };
                vbox.PackStart(_pythonVersionCombo, false, false, 0);
            }
            vbox.PackStart(new HSeparator(), false, false, 0);
            vbox.PackStart(labelName, false, false, 5);
            vbox.PackStart(_nameEntry, false, false, 0);

            var labelEnum = GUIApplication.LabelFromString(Configuration.GetLocalized("Enum \"Action Result\":"));
            _customEnumEditor = new Entry("");
            _customEnumEditor.IsEditable = !GUIApplication.IsReadOnly;
            GUIApplication.RegisterValidation(_customEnumEditor, false, (obj, focusOut, p) => {
                if(_updating) {
                    return;
                }

                try {
                    if(Settings.Enum.ToString() != _customEnumEditor.Text) {
                        var newSettings = Settings.Clone();
                        newSettings.Enum = new Code.Enum(Settings.Language, _customEnumEditor.Text);
                        CommitGuiAction(new ChangeSettingsAction(this, newSettings.GetXml(), _customEnumEditor, null), focusOut);
                    }
                } catch {
                    obj.Text = Settings.Enum.ToString();
                    if(Settings.Enum.Equals(DocumentSettings.GetDefaultEnumForLanguage(Settings.Language))) {
                        _defaultEnum.Active = true;
                        _customEnum.Active = false;
                        _customEnumEditor.Sensitive = false;
                    }

                    GUIApplication.NotifyUnrecoverableError(
                        this,
                        Configuration.GetLocalized("Invalid name for the enum or one of its values.")
                    );
                }
            });

            var radioVBox = new VBox(true, 2);
            _defaultEnum = new RadioButton(Configuration.GetLocalized("Use the default enum (ActionResult)"));
            _defaultEnum.Sensitive = !GUIApplication.IsReadOnly;
            _defaultEnum.Toggled += (object sender, EventArgs e) => {
                if(_updating) {
                    return;
                }

                if(Settings.Enum.ToString() != DocumentSettings.GetDefaultEnumForLanguage(Settings.Language).ToString()) {
                    if((sender as RadioButton).Active) {
                        _customEnumEditor.Sensitive = false;
                        _customEnumEditor.Text = "";
                        var newSettings = Settings.Clone();
                        newSettings.Enum = DocumentSettings.GetDefaultEnumForLanguage(Settings.Language);

                        CommitGuiAction(new ChangeSettingsAction(this, newSettings.GetXml(), _defaultEnum, null));
                    }
                }
            };
            _customEnum = new RadioButton(
                _defaultEnum,
                Configuration.GetLocalized("Use the following enum (name, value1, value2…):")
            );
            _customEnum.Sensitive = !GUIApplication.IsReadOnly;
            _customEnum.Toggled += (object sender, EventArgs e) => {
                if(_updating) {
                    return;
                }

                if((sender as RadioButton).Active) {
                    _customEnumEditor.Sensitive = true;
                    _customEnumEditor.Text = Settings.Enum.ToString();
                }
            };
            radioVBox.PackStart(_defaultEnum, true, true, 2);
            radioVBox.PackStart(_customEnum, true, true, 2);

            vbox.PackStart(labelEnum, false, false, 0);
            vbox.PackStart(radioVBox, false, false, 0);
            vbox.PackStart(_customEnumEditor, false, false, 0);
        }

        void FillDebuggerTab(VBox vbox)
        {
            var page = EditorPage.Debugging;

            var labelDebugMode = GUIApplication.LabelFromString(Configuration.GetLocalized("Debug mode:"));
            var radioVBox = new VBox(true, 2);
            _debugModeAttach = new RadioButton(Configuration.GetLocalized("Attach to an external debug host"));
            _debugModeAttach.Sensitive = !GUIApplication.IsReadOnly;
            _debugModeAttach.Toggled += (object sender, EventArgs e) => {
                if(_updating) {
                    return;
                }

                if(EditedProfile.DebugMode != DebugMode.Attach) {
                    if((sender as RadioButton).Active) {
                        var newSettings = Settings.Clone();
                        newSettings.Profiles[EditedProfileIndex].DebugMode = DebugMode.Attach;
                        CommitGuiAction(new ChangeSettingsAction(this, newSettings.GetXml(), (Widget)sender, page));
                    }
                }
            };
            _debugModeRunInEditor = new RadioButton(
                _debugModeAttach,
                Configuration.GetLocalized("Run in the editor's debug host")
            );
            _debugModeRunInEditor.Sensitive = !GUIApplication.IsReadOnly;
            _debugModeRunInEditor.Toggled += (object sender, EventArgs e) => {
                if(_updating) {
                    return;
                }

                if(EditedProfile.DebugMode != DebugMode.RunInEditor) {
                    if((sender as RadioButton).Active) {
                        var newSettings = Settings.Clone();
                        newSettings.Profiles[EditedProfileIndex].DebugMode = DebugMode.RunInEditor;
                        CommitGuiAction(new ChangeSettingsAction(this, newSettings.GetXml(), (Widget)sender, page));
                    }
                }
            };
            _debugModeCreate = new RadioButton(
                _debugModeAttach,
                Configuration.GetLocalized("Create an external host and attach to it")
            );
            _debugModeCreate.Sensitive = !GUIApplication.IsReadOnly;
            _debugModeCreate.Toggled += (object sender, EventArgs e) => {
                if(_updating) {
                    return;
                }

                if(EditedProfile.DebugMode != DebugMode.CreateHost) {
                    if((sender as RadioButton).Active) {
                        var newSettings = Settings.Clone();
                        newSettings.Profiles[EditedProfileIndex].DebugMode = DebugMode.CreateHost;
                        CommitGuiAction(new ChangeSettingsAction(this, newSettings.GetXml(), (Widget)sender, page));
                    }
                }
            };
            radioVBox.PackStart(_debugModeAttach, true, true, 2);
            radioVBox.PackStart(_debugModeRunInEditor, true, true, 2);
            radioVBox.PackStart(_debugModeCreate, true, true, 2);

            vbox.PackStart(labelDebugMode, false, false, 0);
            vbox.PackStart(radioVBox, false, false, 0);


            var debuggerInfo = new VBox(false, 5);
            var label = GUIApplication.LabelFromString(Configuration.GetLocalized("Host name for the debugger:"));
            _hostnameEntry = new Entry();
            _hostnameEntry.IsEditable = !GUIApplication.IsReadOnly;
            GUIApplication.RegisterValidation(_hostnameEntry, false, (obj, focusOut, p) => {
                if(_updating) {
                    return;
                }

                if(EditedProfile.Hostname != _hostnameEntry.Text) {
                    var newSettings = Settings.Clone();
                    newSettings.Profiles[EditedProfileIndex].Hostname = _hostnameEntry.Text;
                    CommitGuiAction(new ChangeSettingsAction(this, newSettings.GetXml(), _hostnameEntry, page), focusOut);
                }
            });

            debuggerInfo.PackStart(label, false, false, 0);
            debuggerInfo.PackStart(_hostnameEntry, false, false, 0);

            label = GUIApplication.LabelFromString(Configuration.GetLocalized("TCP Port for the debugger communication:"));
            _portEntry = new Entry();
            _portEntry.IsEditable = !GUIApplication.IsReadOnly;
            GUIApplication.RegisterValidation(_portEntry, false, (obj, focusOut, p) => {
                if(_updating) {
                    return;
                }

                try {
                    if(EditedProfile.DebugPort.ToString() != _portEntry.Text) {
                        var newSettings = Settings.Clone();
                        newSettings.Profiles[EditedProfileIndex].DebugPort = UInt16.Parse(_portEntry.Text);
                        CommitGuiAction(new ChangeSettingsAction(this, newSettings.GetXml(), _portEntry, page), focusOut);
                    }
                } catch {
                    obj.Text = EditedProfile.DebugPort.ToString();
                }
            });

            debuggerInfo.PackStart(label, false, false, 0);
            debuggerInfo.PackStart(_portEntry, false, false, 0);

            label = GUIApplication.LabelFromString(Configuration.GetLocalized("Debugger host program:"));
            _hostProgramEntry = new Entry();
            _hostProgramEntry.IsEditable = !GUIApplication.IsReadOnly;
            GUIApplication.RegisterValidation(_hostProgramEntry, false, (obj, focusOut, p) => {
                if(_updating) {
                    return;
                }

                if(EditedProfile.HostProgram != _hostProgramEntry.Text) {
                    var newSettings = Settings.Clone();
                    newSettings.Profiles[EditedProfileIndex].HostProgram = _hostProgramEntry.Text;
                    CommitGuiAction(new ChangeSettingsAction(this, newSettings.GetXml(), _hostProgramEntry, page), focusOut);
                }
            });

            debuggerInfo.PackStart(label, false, false, 0);
            debuggerInfo.PackStart(_hostProgramEntry, false, false, 0);

            label = GUIApplication.LabelFromString(Configuration.GetLocalized("Debugger host program's argument:"));
            _hostProgramArgumentsEntry = new Entry();
            _hostProgramArgumentsEntry.IsEditable = !GUIApplication.IsReadOnly;
            GUIApplication.RegisterValidation(_hostProgramArgumentsEntry, false, (obj, focusOut, p) => {
                if(_updating) {
                    return;
                }

                if(EditedProfile.HostProgramArguments != _hostProgramArgumentsEntry.Text) {
                    var newSettings = Settings.Clone();
                    newSettings.Profiles[EditedProfileIndex].HostProgramArguments = _hostProgramArgumentsEntry.Text;
                    CommitGuiAction(new ChangeSettingsAction(this, newSettings.GetXml(), _hostProgramArgumentsEntry, page), focusOut);
                }
            });

            debuggerInfo.PackStart(label, false, false, 0);
            debuggerInfo.PackStart(_hostProgramArgumentsEntry, false, false, 0);

            vbox.PackStart(debuggerInfo, false, false, 0);
        }

        void FillDeploymentTab(VBox vbox)
        {
            var page = EditorPage.Deployment;

            var labelDeployMode = GUIApplication.LabelFromString(Configuration.GetLocalized("Deployment action:"));
            _deployNothing = new RadioButton(Configuration.GetLocalized("Do nothing (deployment is disabled)"));
            _deployNothing.Sensitive = !GUIApplication.IsReadOnly;
            _deployNothing.Toggled += (object sender, EventArgs e) => {
                if(_updating) {
                    return;
                }

                if(EditedProfile.DeployMode != DeployMode.DoNothing) {
                    if((sender as RadioButton).Active) {
                        var newSettings = Settings.Clone();
                        newSettings.Profiles[EditedProfileIndex].DeployMode = DeployMode.DoNothing;
                        CommitGuiAction(new ChangeSettingsAction(this, newSettings.GetXml(), (Widget)sender, page));
                    }
                }
            };
            _deployCopy = new RadioButton(
                _deployNothing,
                Configuration.GetLocalized("Copy the generated lib:")
            );
            _deployCopy.Sensitive = !GUIApplication.IsReadOnly;
            _deployCopy.Toggled += (object sender, EventArgs e) => {
                if(_updating) {
                    return;
                }

                if(EditedProfile.DeployMode != DeployMode.Copy) {
                    if((sender as RadioButton).Active) {
                        var newSettings = Settings.Clone();
                        newSettings.Profiles[EditedProfileIndex].DeployMode = DeployMode.Copy;
                        CommitGuiAction(new ChangeSettingsAction(this, newSettings.GetXml(), (Widget)sender, page));
                    }
                }
            };
            _deployScript = new RadioButton(
                _deployNothing,
                Configuration.GetLocalized("Run script:")
            );
            _deployScript.Sensitive = !GUIApplication.IsReadOnly;
            _deployScript.Toggled += (object sender, EventArgs e) => {
                if(_updating) {
                    return;
                }

                if(EditedProfile.DeployMode != DeployMode.Script) {
                    if((sender as RadioButton).Active) {
                        var newSettings = Settings.Clone();
                        newSettings.Profiles[EditedProfileIndex].DeployMode = DeployMode.Script;
                        CommitGuiAction(new ChangeSettingsAction(this, newSettings.GetXml(), (Widget)sender, page));
                    }
                }
            };

            var deployPath = new HBox(false, 5);
            _deployCopyDestination = new Entry();
            _deployCopyDestination.IsEditable = !GUIApplication.IsReadOnly;
            GUIApplication.RegisterValidation(_deployCopyDestination, false, (obj, focusOut, p) => {
                if(_updating) {
                    return;
                }

                if(EditedProfile.DeployPath != _deployCopyDestination.Text) {
                    var newSettings = Settings.Clone();
                    newSettings.Profiles[EditedProfileIndex].DeployPath = _deployCopyDestination.Text;
                    CommitGuiAction(new ChangeSettingsAction(this, newSettings.GetXml(), _deployCopyDestination, page), focusOut);
                }
            });

            _deployDestinationSelector = new Button(GUIApplication.LabelFromString("…"));
            _deployDestinationSelector.Clicked += OnAdd;

            deployPath.PackStart(_deployCopyDestination, true, true, 0);
            if(!GUIApplication.IsReadOnly) {
                deployPath.PackStart(_deployDestinationSelector, false, false, 0);
            }

            _deployScriptBuffer = new TextBuffer(new TextTagTable());
            var scriptScroll = new ScrolledWindow();
            scriptScroll.SetPolicy(PolicyType.Automatic, PolicyType.Automatic);
            var textView = new TextView(_deployScriptBuffer);
            textView.Editable = !GUIApplication.IsReadOnly;
            textView.FocusOutEvent += (o, args) => {
                if(_updating) {
                    return;
                }

                if(_deployScriptBuffer.Text != EditedProfile.DeployScript) {
                    var newSettings = Settings.Clone();
                    newSettings.Profiles[EditedProfileIndex].DeployScript = _deployScriptBuffer.Text;
                    CommitGuiAction(new ChangeSettingsAction(this, newSettings.GetXml(), _deployCopyDestination, page));
                }
            };

            var viewport = new Viewport();
            viewport.Add(textView);
            scriptScroll.Add(viewport);

            var helpLabel = GUIApplication.LabelFromMarkup(string.Format(
                Configuration.GetLocalized(
                    "The script's working directory is the document's parent directory. The following environment variables will be available in the script:\n  • {0} is the name of the petri net.\n  • {1} is the name of the profile used to generate the library.\n  • {2} is the absolute location of the generated library."
                ),
                string.Format("<b>{0}</b>", Petri.Application.Document.DEPLOY_ENVVAR_NAME),
                string.Format("<b>{0}</b>", Petri.Application.Document.DEPLOY_ENVVAR_PROFILE),
                string.Format("<b>{0}</b>", Petri.Application.Document.DEPLOY_ENVVAR_LIB_PATH)
            ));

            AllowShrink = true;
            SetGeometryHints(this, new Gdk.Geometry { MinWidth = 920, MinHeight = 400 }, Gdk.WindowHints.MinSize);
            vbox.SizeAllocated += (object o, SizeAllocatedArgs args) => {
                helpLabel.WidthRequest = vbox.Allocation.Width;
            };

            _deployWhenDebug = new CheckButton(Configuration.GetLocalized("Deploy when debugging requires it"));
            _deployWhenDebug.Sensitive = !GUIApplication.IsReadOnly;
            _deployWhenDebug.Toggled += (sender, e) => {
                if(_updating) {
                    return;
                }

                var newSettings = Settings.Clone();
                newSettings.Profiles[EditedProfileIndex].DeployWhenDebug = _deployWhenDebug.Active;
                CommitGuiAction(new ChangeSettingsAction(this, newSettings.GetXml(), _deployWhenDebug, null));
            };

            vbox.PackStart(_deployWhenDebug, false, false, 5);
            vbox.PackStart(new HSeparator(), false, false, 0);
            vbox.PackStart(labelDeployMode, false, false, 2);
            vbox.PackStart(_deployNothing, false, false, 0);
            vbox.PackStart(_deployCopy, false, false, 0);
            vbox.PackStart(deployPath, false, false, 2);
            vbox.PackStart(_deployScript, false, false, 0);
            vbox.PackStart(scriptScroll, true, true, 0);
            vbox.PackStart(helpLabel, false, false, 0);
        }

        void FillGenerationTab(VBox vbox)
        {
            var page = EditorPage.Generation;

            var labelConcurrency = GUIApplication.LabelFromString(Configuration.GetLocalized("Max concurrency:"));
            _customConcurrencyEditor = new Entry();
            _customConcurrencyEditor.IsEditable = !GUIApplication.IsReadOnly;
            GUIApplication.RegisterValidation(_customConcurrencyEditor, false, (obj, focusOut, p) => {
                if(_updating) {
                    return;
                }

                try {
                    if(EditedProfile.MaxConcurrency.ToString() != _customConcurrencyEditor.Text) {
                        var newSettings = Settings.Clone();
                        newSettings.Profiles[EditedProfileIndex].MaxConcurrency = UInt64.Parse(_customConcurrencyEditor.Text);
                        CommitGuiAction(new ChangeSettingsAction(this, newSettings.GetXml(), _customConcurrencyEditor, page), focusOut);
                    }
                } catch {
                    if(EditedProfile.MaxConcurrency == null) {
                        _defaultConcurrency.Active = true;
                        _customConcurrency.Active = false;
                        _customConcurrencyEditor.Text = "";
                        _customConcurrencyEditor.Sensitive = false;
                    } else {
                        _customConcurrencyEditor.Text = EditedProfile.MaxConcurrency.ToString();
                    }

                    GUIApplication.NotifyUnrecoverableError(
                        this,
                        Configuration.GetLocalized("Invalid max concurrency value.")
                    );
                }
            });

            var radioVBox = new VBox(true, 2);
            _defaultConcurrency = new RadioButton(Configuration.GetLocalized("Use the default max concurrency"));
            _defaultConcurrency.Sensitive = !GUIApplication.IsReadOnly;
            _defaultConcurrency.Toggled += (object sender, EventArgs e) => {
                if(_updating) {
                    return;
                }

                if(EditedProfile.MaxConcurrency != null) {
                    if((sender as RadioButton).Active) {
                        _customConcurrencyEditor.Sensitive = false;
                        _customConcurrencyEditor.Text = "";
                        var newSettings = Settings.Clone();
                        newSettings.Profiles[EditedProfileIndex].MaxConcurrency = null;
                        CommitGuiAction(new ChangeSettingsAction(this, newSettings.GetXml(), (RadioButton)sender, page));
                    }
                }
            };
            _customConcurrency = new RadioButton(
                _defaultConcurrency,
                Configuration.GetLocalized("Use the following max concurrency:")
            );
            _customConcurrency.Sensitive = !GUIApplication.IsReadOnly;
            _customConcurrency.Toggled += (object sender, EventArgs e) => {
                if(_updating) {
                    return;
                }

                if(EditedProfile.MaxConcurrency == null) {
                    if((sender as RadioButton).Active) {
                        _customConcurrencyEditor.Sensitive = true;
                        const int def = 8;
                        _customConcurrencyEditor.Text = EditedProfile.MaxConcurrency?.ToString() ?? def.ToString();
                        var newSettings = Settings.Clone();
                        newSettings.Profiles[EditedProfileIndex].MaxConcurrency = def;
                        CommitGuiAction(new ChangeSettingsAction(this, newSettings.GetXml(), _customConcurrency, page));
                    }
                }
            };
            radioVBox.PackStart(_defaultConcurrency, true, true, 2);
            radioVBox.PackStart(_customConcurrency, true, true, 2);

            vbox.PackStart(labelConcurrency, false, false, 0);
            vbox.PackStart(radioVBox, false, false, 0);
            vbox.PackStart(_customConcurrencyEditor, false, false, 0);

            vbox.PackStart(new HSeparator(), false, false, 5);

            {
                var outputLabel = GUIApplication.LabelFromString(Configuration.GetLocalized("Output path for the generated code:"));
                _sourceOutputPath = new Entry();
                _sourceOutputPath.IsEditable = !GUIApplication.IsReadOnly;
                GUIApplication.RegisterValidation(_sourceOutputPath, false, (obj, focusOut, p) => {
                    if(_updating) {
                        return;
                    }

                    if(EditedProfile.RelativeSourceOutputPath != _sourceOutputPath.Text) {
                        var newSettings = Settings.Clone();
                        newSettings.Profiles[EditedProfileIndex].RelativeSourceOutputPath = _sourceOutputPath.Text;
                        CommitGuiAction(
                            new ChangeSettingsAction(this, newSettings.GetXml(), _sourceOutputPath, page),
                            focusOut
                        );
                    }
                });

                vbox.PackStart(outputLabel, false, false, 0);

                _selectSourceOutputPath = new Button("…");
                _selectSourceOutputPath.Clicked += OnAdd;

                var hbox = new HBox(false, 5);
                hbox.PackStart(_sourceOutputPath, true, true, 0);
                if(!GUIApplication.IsReadOnly) {
                    hbox.PackStart(_selectSourceOutputPath, false, false, 0);
                }
                vbox.PackStart(hbox, false, false, 0);
            }

            {
                var outputLabel = GUIApplication.LabelFromString(Configuration.GetLocalized("Output path for the dynamic library:"));
                _libOutputPath = new Entry();
                _libOutputPath.IsEditable = !GUIApplication.IsReadOnly;
                GUIApplication.RegisterValidation(_libOutputPath, false, (obj, focusOut, p) => {
                    if(_updating) {
                        return;
                    }

                    if(EditedProfile.RelativeLibOutputPath != _libOutputPath.Text) {
                        var newSettings = Settings.Clone();
                        newSettings.Profiles[EditedProfileIndex].RelativeLibOutputPath = _libOutputPath.Text;
                        CommitGuiAction(new ChangeSettingsAction(this, newSettings.GetXml(), _libOutputPath, page), focusOut);
                    }
                });

                vbox.PackStart(outputLabel, false, false, 0);

                _selectLibOutputPath = new Button("…");
                _selectLibOutputPath.Clicked += OnAdd;

                var hbox = new HBox(false, 5);
                hbox.PackStart(_libOutputPath, true, true, 0);
                if(!GUIApplication.IsReadOnly) {
                    hbox.PackStart(_selectLibOutputPath, false, false, 0);
                }
                vbox.PackStart(hbox, false, false, 0);
            }

            var labelVerbosity = GUIApplication.LabelFromString(Configuration.GetLocalized("Default execution verbosity:"));
            _hasStatesVerbosity = new CheckButton(Configuration.GetLocalized("Print states"));
            _hasStatesVerbosity.Sensitive = !GUIApplication.IsReadOnly;
            _hasStatesVerbosity.Toggled += (sender, e) => {
                if(_updating) {
                    return;
                }

                var newSettings = Settings.Clone();
                newSettings.DefaultExecutionVerbosity = newSettings.DefaultExecutionVerbosity ^ Runtime.PetriNet.LogVerbosity.States;
                CommitGuiAction(new ChangeSettingsAction(this, newSettings.GetXml(), _hasStatesVerbosity, page));
            };

            _hasTransitionsVerbosity = new CheckButton(Configuration.GetLocalized("Print transitions"));
            _hasTransitionsVerbosity.Sensitive = !GUIApplication.IsReadOnly;
            _hasTransitionsVerbosity.Toggled += (sender, e) => {
                if(_updating) {
                    return;
                }

                var newSettings = Settings.Clone();
                newSettings.DefaultExecutionVerbosity = newSettings.DefaultExecutionVerbosity ^ Runtime.PetriNet.LogVerbosity.Transitions;
                CommitGuiAction(new ChangeSettingsAction(this, newSettings.GetXml(), _hasTransitionsVerbosity, page));
            };

            _hasReturnValuesVerbosity = new CheckButton(Configuration.GetLocalized("Print return values"));
            _hasReturnValuesVerbosity.Sensitive = !GUIApplication.IsReadOnly;
            _hasReturnValuesVerbosity.Toggled += (sender, e) => {
                if(_updating) {
                    return;
                }

                var newSettings = Settings.Clone();
                newSettings.DefaultExecutionVerbosity = newSettings.DefaultExecutionVerbosity ^ Runtime.PetriNet.LogVerbosity.ReturnValues;
                CommitGuiAction(new ChangeSettingsAction(this, newSettings.GetXml(), _hasReturnValuesVerbosity, page));
            };

            vbox.PackStart(new HSeparator(), false, false, 5);
            vbox.PackStart(_hasStatesVerbosity, false, false, 0);
            vbox.PackStart(_hasTransitionsVerbosity, false, false, 0);
            vbox.PackStart(_hasReturnValuesVerbosity, false, false, 0);
        }

        void FillCompilerTab(VBox vbox)
        {
            var page = EditorPage.Compilation;

            {
                var labelCompiler = GUIApplication.LabelFromString(
                    Configuration.GetLocalized("Path to the <language> compiler:",
                    Settings.LanguageName())
                );

                _compilerEntry = new Entry();
                _compilerEntry.IsEditable = !GUIApplication.IsReadOnly;
                GUIApplication.RegisterValidation(_compilerEntry, false, (obj, focusOut, p) => {
                    if(_updating) {
                        return;
                    }

                    if(EditedProfile.CompilerInfo.Compiler != _compilerEntry.Text) {
                        var newSettings = Settings.Clone();
                        newSettings.Profiles[EditedProfileIndex].CompilerInfo.Compiler = _compilerEntry.Text;
                        CommitGuiAction(
                            new ChangeSettingsAction(this, newSettings.GetXml(), _compilerEntry, page),
                            focusOut
                        );
                    }
                });

                vbox.PackStart(labelCompiler, false, false, 0);
                vbox.PackStart(_compilerEntry, false, false, 0);

                var labelFlags = GUIApplication.LabelFromString(
                    Configuration.GetLocalized("Flags forwarded to the <language> compiler:", Settings.LanguageName())
                );

                _compilerFlags = new Entry();
                _compilerFlags.IsEditable = !GUIApplication.IsReadOnly;
                GUIApplication.RegisterValidation(_compilerFlags, false, (obj, focusOut, p) => {
                    if(_updating) {
                        return;
                    }

                    var values = _compilerFlags.Text.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    if(string.Join(" ", EditedProfile.CompilerInfo.CustomCompilerFlags) != string.Join(" ", values)) {
                        var newSettings = Settings.Clone();
                        var prof = newSettings.Profiles[EditedProfileIndex];
                        prof.CompilerInfo.CustomCompilerFlags.Clear();
                        prof.CompilerInfo.CustomCompilerFlags.AddRange(values);
                        CommitGuiAction(new ChangeSettingsAction(this, newSettings.GetXml(), _compilerFlags, page), focusOut);
                    }
                });

                vbox.PackStart(labelFlags, false, false, 0);
                vbox.PackStart(_compilerFlags, false, false, 0);

                if(Settings.Language != Code.Language.CSharp) {
                    labelFlags = GUIApplication.LabelFromString(
                        Configuration.GetLocalized("Flags forwarded to the <language> linker:", Settings.LanguageName())
                    );

                    _linkerFlags = new Entry();
                    _linkerFlags.IsEditable = !GUIApplication.IsReadOnly;
                    GUIApplication.RegisterValidation(_linkerFlags, false, (obj, focusOut, p) => {
                        if(_updating) {
                            return;
                        }

                        var values = _linkerFlags.Text.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        if(string.Join(" ", EditedProfile.CompilerInfo.CustomLinkerFlags) != string.Join(" ", values)) {
                            var newSettings = Settings.Clone();
                            var prof = newSettings.Profiles[EditedProfileIndex];
                            prof.CompilerInfo.CustomLinkerFlags.Clear();
                            prof.CompilerInfo.CustomLinkerFlags.AddRange(values);
                            CommitGuiAction(new ChangeSettingsAction(this, newSettings.GetXml(), _linkerFlags, page), focusOut);
                        }
                    });

                    vbox.PackStart(labelFlags, false, false, 0);
                    vbox.PackStart(_linkerFlags, false, false, 0);
                }
            }

            if(Settings.Language != Code.Language.CSharp && Settings.Language != Code.Language.Python) {
                var label = GUIApplication.LabelFromString(Configuration.GetLocalized("Headers search paths:"));
                vbox.PackStart(label, false, false, 0);

                _headersSearchPath = new TreeView();
                var c = new TreeViewColumn();
                c.Title = Configuration.GetLocalized("Path");
                var pathCell = new CellRendererText();
                pathCell.Editable = !GUIApplication.IsReadOnly;
                pathCell.Edited += (object sender, EditedArgs args) => {
                    if(_updating) {
                        return;
                    }

                    var tup = EditedProfile.CompilerInfo.IncludePaths[int.Parse(args.Path)];
                    if(tup.Item1 != args.NewText) {
                        var newSettings = Settings.Clone();
                        newSettings.Profiles[EditedProfileIndex].CompilerInfo.IncludePaths[int.Parse(args.Path)] = Tuple.Create(
                            args.NewText,
                            tup.Item2
                        );
                        CommitGuiAction(new ChangeSettingsAction(this, newSettings.GetXml(), _headersSearchPath, page));
                    }
                };

                c.PackStart(pathCell, true);
                c.AddAttribute(pathCell, "text", 0);
                _headersSearchPath.AppendColumn(c);

                c = new TreeViewColumn();
                c.Title = Configuration.GetLocalized("Recursive");
                var recursivityCell = new CellRendererToggle();
                recursivityCell.Sensitive = !GUIApplication.IsReadOnly;
                recursivityCell.Toggled += (object sender, ToggledArgs args) => {
                    if(_updating || GUIApplication.IsReadOnly) {
                        return;
                    }

                    var tup = EditedProfile.CompilerInfo.IncludePaths[int.Parse(args.Path)];

                    var newSettings = Settings.Clone();
                    newSettings.Profiles[EditedProfileIndex].CompilerInfo.IncludePaths[int.Parse(args.Path)] = Tuple.Create(tup.Item1, !tup.Item2);
                    CommitGuiAction(new ChangeSettingsAction(this, newSettings.GetXml(), _headersSearchPath, page));
                };
                c.PackStart(recursivityCell, true);
                c.AddAttribute(recursivityCell, "active", 1);
                _headersSearchPath.AppendColumn(c);

                _headersSearchPathStore = new ListStore(typeof(string), typeof(bool));
                _headersSearchPath.Model = _headersSearchPathStore;

                vbox.PackStart(_headersSearchPath, true, true, 0);

                var hbox = new HBox(false, 5);
                _addHeaderSearchPath = new Button(GUIApplication.LabelFromString("+", true));
                _removeHeaderSearchPath = new Button(GUIApplication.LabelFromString("-", true));
                _addHeaderSearchPath.Clicked += OnAdd;
                _removeHeaderSearchPath.Clicked += OnRemove;
                hbox.PackStart(_addHeaderSearchPath, false, false, 0);
                hbox.PackStart(_removeHeaderSearchPath, false, false, 0);
                if(!GUIApplication.IsReadOnly) {
                    vbox.PackStart(hbox, false, false, 0);
                }
            }

            {
                var label = GUIApplication.LabelFromString(Configuration.GetLocalized("Libraries search paths:"));
                vbox.PackStart(label, false, false, 0);

                _libsSearchPath = new TreeView();
                var c = new TreeViewColumn();
                c.Title = Configuration.GetLocalized("Path");
                var pathCell = new CellRendererText();
                pathCell.Editable = !GUIApplication.IsReadOnly;
                pathCell.Edited += (object sender, EditedArgs args) => {
                    if(_updating) {
                        return;
                    }

                    var tup = EditedProfile.CompilerInfo.LibPaths[int.Parse(args.Path)];
                    if(tup.Item1 != args.NewText) {
                        var newSettings = Settings.Clone();
                        newSettings.Profiles[EditedProfileIndex].CompilerInfo.LibPaths[int.Parse(args.Path)] = Tuple.Create(args.NewText, tup.Item2);
                        CommitGuiAction(new ChangeSettingsAction(this, newSettings.GetXml(), _libsSearchPath, page));
                    }
                };
                c.PackStart(pathCell, true);
                c.AddAttribute(pathCell, "text", 0);
                _libsSearchPath.AppendColumn(c);

                c = new TreeViewColumn();
                c.Title = Configuration.GetLocalized("Recursive");
                var recursivityCell = new CellRendererToggle();
                recursivityCell.Sensitive = !GUIApplication.IsReadOnly;
                recursivityCell.Toggled += (object sender, ToggledArgs args) => {
                    if(_updating || GUIApplication.IsReadOnly) {
                        return;
                    }

                    var tup = EditedProfile.CompilerInfo.LibPaths[int.Parse(args.Path)];

                    var newSettings = Settings.Clone();
                    newSettings.Profiles[EditedProfileIndex].CompilerInfo.LibPaths[int.Parse(args.Path)] = Tuple.Create(tup.Item1, !tup.Item2);
                    CommitGuiAction(new ChangeSettingsAction(this, newSettings.GetXml(), _libsSearchPath, page));
                };
                c.PackStart(recursivityCell, true);
                c.AddAttribute(recursivityCell, "active", 1);
                _libsSearchPath.AppendColumn(c);

                _libsSearchPathStore = new ListStore(typeof(string), typeof(bool));
                _libsSearchPath.Model = _libsSearchPathStore;

                vbox.PackStart(_libsSearchPath, true, true, 0);

                var hbox = new HBox(false, 5);
                _addLibSearchPath = new Button(GUIApplication.LabelFromString("+", true));
                _removeLibSearchPath = new Button(GUIApplication.LabelFromString("-", true));
                _addLibSearchPath.Clicked += OnAdd;
                _removeLibSearchPath.Clicked += OnRemove;
                hbox.PackStart(_addLibSearchPath, false, false, 0);
                hbox.PackStart(_removeLibSearchPath, false, false, 0);
                if(!GUIApplication.IsReadOnly) {
                    vbox.PackStart(hbox, false, false, 0);
                }
            }

            {
                var label = GUIApplication.LabelFromString(Configuration.GetLocalized("Libraries used by the document:"));
                vbox.PackStart(label, false, false, 0);

                _libs = new TreeView();
                TreeViewColumn c = new TreeViewColumn();
                c.Title = Configuration.GetLocalized("Path");
                var pathCell = new CellRendererText();
                pathCell.Editable = !GUIApplication.IsReadOnly;
                pathCell.Edited += (object sender, EditedArgs args) => {
                    if(_updating) {
                        return;
                    }

                    if(EditedProfile.CompilerInfo.Libs[int.Parse(args.Path)] != args.NewText) {
                        var newSettings = Settings.Clone();
                        newSettings.Profiles[EditedProfileIndex].CompilerInfo.Libs[int.Parse(args.Path)] = args.NewText;
                        CommitGuiAction(new ChangeSettingsAction(this, newSettings.GetXml(), _libs, page));
                    }
                };
                c.PackStart(pathCell, true);
                c.AddAttribute(pathCell, "text", 0);
                _libs.AppendColumn(c);

                _libsStore = new ListStore(typeof(string));
                _libs.Model = _libsStore;

                vbox.PackStart(_libs, true, true, 0);

                var hbox = new HBox(false, 5);
                _addLib = new Button(GUIApplication.LabelFromString("+", true));
                _removeLib = new Button(GUIApplication.LabelFromString("-", true));
                _addLib.Clicked += OnAdd;
                _removeLib.Clicked += OnRemove;
                hbox.PackStart(_addLib, false, false, 0);
                hbox.PackStart(_removeLib, false, false, 0);
                if(!GUIApplication.IsReadOnly) {
                    vbox.PackStart(hbox, false, false, 0);
                }
            }
        }

        /// <summary>
        /// Shows the requested page of the editor.
        /// </summary>
        /// <param name="page">Page.</param>
        public void ShowPage(EditorPage? page)
        {
            if(page.HasValue && page != EditorPage.Current) {
                _notebook.Page = (int)page;
            }
            ShowWindow();
        }

        /// <summary>
        /// Gets the string that should be used as the window's title.
        /// </summary>
        /// <value>The current title.</value>
        public override string CurrentTitle {
            get {
                return Configuration.GetLocalized("Settings of {0}", Document.Settings.Name);
            }
        }

        /// <summary>
        /// Changes the editor's controls' value according to the settings values.
        /// </summary>
        public override void UpdateUI()
        {
            bool isReadOnly = GUIApplication.IsReadOnly;

            _updating = true;

            base.UpdateUI();

            if(Settings.Language == Code.Language.Python) {
                _pythonVersionCombo.Active = Settings.AdditionalLanguageInfo == "2" ? 0 : 1;
            }
            _nameEntry.Text = Settings.Name;

            if(Settings.Enum.Equals(DocumentSettings.GetDefaultEnumForLanguage(Settings.Language))) {
                _defaultEnum.Active = true;
                _customEnum.Active = false;
                _customEnumEditor.Sensitive = false;
            } else {
                _defaultEnum.Active = false;
                _customEnum.Active = true;
                _customEnumEditor.Sensitive = true;
                _customEnumEditor.Text = Settings.Enum.ToString();
            }

            if(EditedProfile.MaxConcurrency == null) {
                _defaultConcurrency.Active = true;
                _customConcurrency.Active = false;
                _customConcurrencyEditor.Sensitive = false;
            } else {
                _defaultConcurrency.Active = false;
                _customConcurrency.Active = true;
                _customConcurrencyEditor.Sensitive = true;
                _customConcurrencyEditor.Text = EditedProfile.MaxConcurrency.ToString();
            }
            _hasStatesVerbosity.Active = Settings.DefaultExecutionVerbosity.HasFlag(Runtime.PetriNet.LogVerbosity.States);
            _hasTransitionsVerbosity.Active = Settings.DefaultExecutionVerbosity.HasFlag(Runtime.PetriNet.LogVerbosity.Transitions);
            _hasReturnValuesVerbosity.Active = Settings.DefaultExecutionVerbosity.HasFlag(Runtime.PetriNet.LogVerbosity.ReturnValues);

            _profilesStore.Clear();
            TreeIter? iter = null;
            foreach(var profile in Settings.Profiles) {
                TreeIter? it = null;
                if(profile == Settings.CurrentProfile) {
                    it = _profilesStore.AppendValues(Configuration.GetLocalized("{0} (current profile)", profile.Name));
                } else {
                    it = _profilesStore.AppendValues(profile.Name);
                }

                if(profile == EditedProfile) {
                    iter = it;
                }
            }
            _profilesList.Selection.SelectIter(iter.Value);

            _profileNameLabel.Text = Configuration.GetLocalized("Edited profile ({0}):", EditedProfile.Name);

            _overrideProfiles.Active = Settings.OverrideChildCompilationSettings;

            _compilerEntry.Text = EditedProfile.CompilerInfo.Compiler;
            _compilerFlags.Text = string.Join(" ", EditedProfile.CompilerInfo.CustomCompilerFlags);
            if(_linkerFlags != null) {
                _linkerFlags.Text = string.Join(" ", EditedProfile.CompilerInfo.CustomLinkerFlags);
            }

            _sourceOutputPath.Text = EditedProfile.RelativeSourceOutputPath;
            _libOutputPath.Text = EditedProfile.RelativeLibOutputPath;

            _deployNothing.Active = EditedProfile.DeployMode == DeployMode.DoNothing;
            _deployCopy.Active = EditedProfile.DeployMode == DeployMode.Copy;
            _deployScript.Active = EditedProfile.DeployMode == DeployMode.Script;

            _deployCopyDestination.Text = EditedProfile.DeployPath;
            _deployScriptBuffer.Text = EditedProfile.DeployScript;

            _deployWhenDebug.Active = EditedProfile.DeployWhenDebug;

            _hostnameEntry.Text = EditedProfile.Hostname;
            _portEntry.Text = EditedProfile.DebugPort.ToString();
            _hostProgramEntry.Text = EditedProfile.HostProgram;
            _hostProgramArgumentsEntry.Text = EditedProfile.HostProgramArguments;

            _hostnameEntry.Sensitive = EditedProfile.DebugMode == DebugMode.Attach;
            _hostProgramEntry.Sensitive = EditedProfile.DebugMode == DebugMode.CreateHost;
            _hostProgramArgumentsEntry.Sensitive = EditedProfile.DebugMode == DebugMode.CreateHost;

            _debugModeAttach.Active = EditedProfile.DebugMode == DebugMode.Attach;
            _debugModeRunInEditor.Active = EditedProfile.DebugMode == DebugMode.RunInEditor;
            _debugModeCreate.Active = EditedProfile.DebugMode == DebugMode.CreateHost;

            BuildHeadersSearchPath();
            BuildLibsSearchPath();
            BuildLibs();

            _updating = false;
        }

        /// <summary>
        /// Handles each "+" button press.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Args.</param>
        void OnAdd(object sender, EventArgs e)
        {
            string title = "";
            var page = EditorPage.Current;
            FileChooserAction action = FileChooserAction.Open;

            FileFilter filter = null;

            if(sender == _addHeaderSearchPath) {
                title = Configuration.GetLocalized("Select the directory where to search for the headers…");
                action = FileChooserAction.SelectFolder;
                page = EditorPage.Compilation;
            } else if(sender == _addLibSearchPath) {
                title = Configuration.GetLocalized("Select the directory where to search for the libraries…");
                action = FileChooserAction.SelectFolder;
                page = EditorPage.Compilation;
            } else if(sender == _addLib) {
                title = Configuration.GetLocalized("Select the library…");
                action = FileChooserAction.Open;
                filter = new FileFilter();
                filter.Name = Configuration.GetLocalized("Library");

                filter.AddPattern("*.a");
                filter.AddPattern("*.lib");
                filter.AddPattern("*.so");
                filter.AddPattern("*.dylib");
                filter.AddPattern("*.dll");
                page = EditorPage.Compilation;
            } else if(sender == _selectSourceOutputPath) {
                title = Configuration.GetLocalized("Select the directory where to generate the <language> source code…",
                                                   Settings.LanguageName());
                action = FileChooserAction.SelectFolder;
                page = EditorPage.Generation;
            } else if(sender == _selectLibOutputPath) {
                title = Configuration.GetLocalized("Select the directory where to generate the library…");
                action = FileChooserAction.SelectFolder;
                page = EditorPage.Compilation;
            } else if(sender == _deployDestinationSelector) {
                title = Configuration.GetLocalized("Select a directory to copy the lib to…");
                action = FileChooserAction.SelectFolder;
                page = EditorPage.Deployment;
            }

            var fc = new FileChooserDialog(title, this,
                                           action,
                                           new object[] {
                Configuration.GetLocalized("Cancel"), ResponseType.Cancel,
                Configuration.GetLocalized("Open"), ResponseType.Accept
            });

            CheckButton b = null;

            if(sender == _addHeaderSearchPath
               || sender == _addLibSearchPath
               || sender == _selectSourceOutputPath
               || sender == _selectLibOutputPath
               || sender == _deployDestinationSelector) {
                b = new CheckButton(Configuration.GetLocalized("Relative path"));
                b.Active = Configuration.PreferRelativePaths;
                fc.ActionArea.PackEnd(b);
                b.Show();
            }

            if(filter != null) {
                fc.AddFilter(filter);
            }

            if(GUIApplication.RunModalDialog(fc) == ResponseType.Accept) {
                var filename = fc.Filename;
                if(action == FileChooserAction.SelectFolder && !filename.EndsWithInv("/")) {
                    filename += "/";
                }
                string path = (b?.Active ?? true) ? Document.GetRelativeToDoc(filename) : filename;
                var newSettings = Settings.Clone();

                if(sender == _addHeaderSearchPath) {
                    newSettings.Profiles[EditedProfileIndex].CompilerInfo.IncludePaths.Add(Tuple.Create(path, false));
                } else if(sender == _addLibSearchPath) {
                    newSettings.Profiles[EditedProfileIndex].CompilerInfo.LibPaths.Add(Tuple.Create(path, false));
                } else if(sender == _addLib) {
                    filename = System.IO.Path.GetFileName(filename);
                    if(filename.StartsWithInv("lib")) {
                        filename = filename.Substring(3);
                    }
                    filename = System.IO.Path.GetFileNameWithoutExtension(filename);
                    newSettings.Profiles[EditedProfileIndex].CompilerInfo.Libs.Add(filename);
                } else if(sender == _selectSourceOutputPath) {
                    newSettings.Profiles[EditedProfileIndex].RelativeSourceOutputPath = path;
                } else if(sender == _selectLibOutputPath) {
                    newSettings.Profiles[EditedProfileIndex].RelativeLibOutputPath = path;
                } else if(sender == _deployDestinationSelector) {
                    newSettings.Profiles[EditedProfileIndex].DeployPath = path;
                }

                CommitGuiAction(new ChangeSettingsAction(this, newSettings.GetXml(), (Widget)sender, page));
            }
            fc.Destroy();
        }

        /// <summary>
        /// Handles each "-" button press.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Args.</param>
        void OnRemove(object sender, EventArgs e)
        {
            if(sender == _removeHeaderSearchPath) {
                TreeIter iter;
                TreePath[] treePath = _headersSearchPath.Selection.GetSelectedRows();

                for(int i = treePath.Length; i > 0; --i) {
                    _headersSearchPathStore.GetIter(out iter, treePath[i - 1]);

                    var newSettings = Settings.Clone();
                    newSettings.Profiles[EditedProfileIndex].CompilerInfo.IncludePaths.Remove(
                        Tuple.Create(
                            (string)_headersSearchPathStore.GetValue(iter, 0),
                            (bool)_headersSearchPathStore.GetValue(iter, 1)
                        )
                    );
                    CommitGuiAction(new ChangeSettingsAction(this, newSettings.GetXml(), _headersSearchPath, EditorPage.Compilation));
                }

                BuildHeadersSearchPath();
            } else if(sender == _removeLibSearchPath) {
                TreeIter iter;
                TreePath[] treePath = _libsSearchPath.Selection.GetSelectedRows();

                for(int i = treePath.Length; i > 0; --i) {
                    _libsSearchPathStore.GetIter(out iter, treePath[i - 1]);

                    var newSettings = Settings.Clone();
                    newSettings.Profiles[EditedProfileIndex].CompilerInfo.LibPaths.Remove(
                        Tuple.Create(
                            (string)_libsSearchPathStore.GetValue(iter, 0),
                            (bool)_libsSearchPathStore.GetValue(iter, 1)
                        )
                    );
                    CommitGuiAction(new ChangeSettingsAction(this, newSettings.GetXml(), _libsSearchPath, EditorPage.Compilation));
                }

                BuildLibsSearchPath();
            } else if(sender == _removeLib) {
                TreeIter iter;
                TreePath[] treePath = _libs.Selection.GetSelectedRows();

                for(int i = treePath.Length; i > 0; --i) {
                    _libsStore.GetIter(out iter, treePath[i - 1]);

                    var newSettings = Settings.Clone();
                    newSettings.Profiles[EditedProfileIndex].CompilerInfo.Libs.Remove(_libsStore.GetValue(iter, 0) as string);
                    CommitGuiAction(new ChangeSettingsAction(this, newSettings.GetXml(), _libs, EditorPage.Compilation));
                }

                BuildLibs();
            }
        }

        /// <summary>
        /// Builds the headers search path.
        /// </summary>
        void BuildHeadersSearchPath()
        {
            if(_headersSearchPath != null) {
                _headersSearchPathStore.Clear();
                foreach(var p in EditedProfile.CompilerInfo.IncludePaths) {
                    _headersSearchPathStore.AppendValues(p.Item1, p.Item2);
                }
            }
        }

        /// <summary>
        /// Builds the libs search path.
        /// </summary>
        void BuildLibsSearchPath()
        {
            _libsSearchPathStore.Clear();
            foreach(var p in EditedProfile.CompilerInfo.LibPaths) {
                _libsSearchPathStore.AppendValues(p.Item1, p.Item2);
            }
        }

        /// <summary>
        /// Builds the libs.
        /// </summary>
        void BuildLibs()
        {
            _libsStore.Clear();
            foreach(var p in EditedProfile.CompilerInfo.Libs) {
                _libsStore.AppendValues(p);
            }
        }

        string GetAvailableProfileNameForPattern(string baseName, string suffix)
        {
            var regex = string.Format(".*({0})", Regex.Escape(suffix).Replace(Regex.Escape("{0}"), "\\d+"));
            var match = Regex.Match(baseName, regex);
            if(match.Success) {
                baseName = baseName.Remove(baseName.Length - match.Groups[1].Length);
            }

            suffix = baseName + suffix;

            int i = 0;
            while(true) {
                var candidate = string.Format(suffix, i);
                bool good = Settings.Profiles.All((prof) => prof.Name != candidate);
                if(good) {
                    return candidate;
                }

                if(i == int.MaxValue) {
                    throw new NotSupportedException();
                }
                ++i;
            }
        }

        ComboBox _pythonVersionCombo;

        TreeView _profilesList;
        ListStore _profilesStore;

        CheckButton _overrideProfiles;

        Label _profileNameLabel;
        Notebook _notebook;

        RadioButton _defaultEnum, _customEnum;
        Entry _customEnumEditor;

        RadioButton _defaultConcurrency, _customConcurrency;
        Entry _customConcurrencyEditor;

        CheckButton _hasStatesVerbosity;
        CheckButton _hasTransitionsVerbosity;
        CheckButton _hasReturnValuesVerbosity;

        Entry _libOutputPath, _sourceOutputPath;
        Button _selectLibOutputPath, _selectSourceOutputPath;

        TreeView _headersSearchPath;
        ListStore _headersSearchPathStore;
        Button _addHeaderSearchPath;
        Button _removeHeaderSearchPath;

        TreeView _libsSearchPath;
        ListStore _libsSearchPathStore;
        Button _addLibSearchPath;
        Button _removeLibSearchPath;

        TreeView _libs;
        ListStore _libsStore;
        Button _addLib;
        Button _removeLib;

        Entry _nameEntry;
        Entry _compilerEntry;
        Entry _compilerFlags;
        Entry _linkerFlags;

        RadioButton _deployNothing, _deployCopy, _deployScript;
        Entry _deployCopyDestination;
        Button _deployDestinationSelector;
        TextBuffer _deployScriptBuffer;
        CheckButton _deployWhenDebug;

        RadioButton _debugModeAttach, _debugModeCreate, _debugModeRunInEditor;
        Entry _hostnameEntry;
        Entry _portEntry;
        Entry _hostProgramEntry;
        Entry _hostProgramArgumentsEntry;

        bool _updating;
    }
}
