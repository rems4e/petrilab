//
//  GUIDocument.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2014-11-13.
//

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Xml.Linq;

using Gtk;

using Petri.Application.Debugger;

namespace Petri.Application.GUI
{
    /// <summary>
    /// A document with all the GUI attached to it, including the editor and the debugger.
    /// </summary>
    public class GUIDocument : LocalDocument
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.GUIDocument"/> class.
        /// </summary>
        /// <param name="path">The path ot the document to load, or a empty string if new document.</param>
        public GUIDocument(string path) : base(path)
        {
            Init();
        }

        /// <summary>
        /// Creates a new empty document.
        /// </summary>
        /// <param name="language">Language.</param>
        public GUIDocument(Code.Language language) : base(language)
        {
            Init();
        }

        /// <summary>
        /// Initializes the document's internals along with the window' state.
        /// </summary>
        void Init()
        {
            IsClosing = false;

            AssociatedWindows = new HashSet<DocumentWindow>();

            DebugDocument = new Debugger.DebuggableGUIDocument(this);

            EditorController = new Editor.EditorController(this);
            DebugController = new Debugger.DebugController(DebugDocument);

            Window = new MainWindow(this);
            Window.EditorGui.View.FocusChanged += (GUIPetriView view, GUIPetriView.FocusChangedEventArgs e) => {
                if(e.IsFocusIn) {
                    ReloadHeadersIfNecessary();
                    CheckForExternalDocumentsUpdate();
                }
            };
            Window.DebugGui.View.FocusChanged += (GUIPetriView view, GUIPetriView.FocusChangedEventArgs e) => {
                if(e.IsFocusIn) {
                    CheckForExternalDocumentsUpdate();
                }
            };

            var winConf = Settings.XMLBackup.Element("Window");
            if(winConf != null) {
                Window.Resize(int.Parse(winConf.Attribute("W").Value), int.Parse(winConf.Attribute("H").Value));
                Window.Move(int.Parse(winConf.Attribute("X").Value), int.Parse(winConf.Attribute("Y").Value));
            }

            DebugController.BreakpointsUpdated += (e) => {
                Window.DebugGui.View.Redraw();
            };

            Window.SwitchToEditor();

            Window.ShowWindow();
            Window.EditorGui.HPaned.Position = Window.Allocation.Width - 260;
            Window.DebugGui.HPaned.Position = Window.Allocation.Width - 260;

            Output = new GUIConsoleOutput(Window);

            UpdateFunctions();
        }

        /// <summary>
        /// Gets the debug document.
        /// </summary>
        /// <value>The debug document.</value>
		public Debugger.DebuggableGUIDocument DebugDocument {
            get;
            private set;
        }

        /// <summary>
        /// Gets the associated windows of the document, such as the document settings window, the header/macro managers and the search dialog.
        /// </summary>
        /// <value>The associated windows.</value>
        public HashSet<DocumentWindow> AssociatedWindows {
            get;
            private set;
        }

        /// <summary>
        /// The window of the document.
        /// </summary>
        /// <value>The window.</value>
        public MainWindow Window {
            get;
            private set;
        }

        /// <summary>
        /// Gets the current controller.
        /// </summary>
        /// <value>The current controller.</value>
        public Controller CurrentController {
            get;
            set;
        }

        /// <summary>
        /// Gets the editor controller.
        /// </summary>
        /// <value>The editor controller.</value>
        public Editor.EditorController EditorController {
            get;
            private set;
        }

        /// <summary>
        /// Gets the debug controller.
        /// </summary>
        /// <value>The debug controller.</value>
        public Debugger.DebugController DebugController {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets the time the headers were updated last.
        /// </summary>
        /// <value>The last headers update.</value>
        protected DateTime LastHeadersUpdate {
            get;
            set;
        } = DateTime.MinValue;

        /// <summary>
        /// Loads the document from an XML descriptor.
        /// </summary>
        protected override void LoadFromXml(XDocument document)
        {
            base.LoadFromXml(document);
            LastHeadersUpdate = DateTime.Now;
        }

        /// <summary>
        /// Removes a header and removes the functions it contains from the known functions.
        /// </summary>
        /// <param name="header">The header's path.</param>
        public void RemoveHeaderNoUpdate(string header)
        {
            string filename = header;

            // If path is relative, then make it absolute
            if(!System.IO.Path.IsPathRooted(header)) {
                filename = System.IO.Path.Combine(System.IO.Directory.GetParent(Path).FullName,
                                                  filename);
            }

            var toRemove = new List<string>();
            foreach(var list in AllFunctions) {
                list.Value.RemoveAll(s => s.Header == filename);
                if(list.Value.Count == 0) {
                    toRemove.Add(list.Key);
                }
            }
            foreach(var key in toRemove) {
                AllFunctions.Remove(key);
            }

            Settings.Headers.Remove(header);

            if(_functionsBrowser != null) {
                _functionsBrowser.UpdateUI();
            }
        }

        /// <summary>
        /// Adds a header to the document.
        /// </summary>
        /// <param name="header">Header.</param>
        public override void AddHeader(string header)
        {
            base.AddHeader(header);
            LastHeadersUpdate = DateTime.Now;
        }

        /// <summary>
        /// Reloads the document's headers if necessary, i.e. if they have been updated since the last time we read them.
        /// </summary>
        public void ReloadHeadersIfNecessary()
        {
            bool needsToReload = false;
            foreach(string h in Settings.Headers) {
                string hh = GetAbsoluteFromRelativeToDoc(h);
                if(!System.IO.File.Exists(hh) || System.IO.File.GetLastWriteTime(hh) > LastHeadersUpdate) {
                    needsToReload = true;
                    break;
                }
            }

            if(needsToReload) {
                var backup = new List<string>(Settings.Headers);

                LastHeadersUpdate = DateTime.Now;

                foreach(var h in backup) {
                    RemoveHeaderNoUpdate(h);
                }

                foreach(var h in backup) {
                    AddHeader(h);
                }
            }
        }

        /// <summary>
        /// Checks for external documents update, change status line if there is any.
        /// </summary>
        void CheckForExternalDocumentsUpdate()
        {
            if(UpdateExternalDocuments()) {
                Window.Gui.Status = Configuration.GetLocalized("Some imported petri nets have been updated.");
                Window.UpdateUI();
            }
        }

        /// <summary>
        /// Updates the external documents.
        /// </summary>
        /// <returns><c>true</c>, if some of the external documents were updated, <c>false</c> otherwise.</returns>
        public override bool UpdateExternalDocuments()
        {
            var result = base.UpdateExternalDocuments();

            if(result) {
                Window.Gui.Status = Configuration.GetLocalized("Some imported petri nets have been updated.");
                Window.EditorGui.View.GoToRoot();
                DebugDocument.ExternalsInSync = false;
                Window.Gui.BaseView.Redraw();
                Window.EditorGui.PropertiesPanel.Reset();
            }

            return result;
        }

        /// <summary>
        /// Updates the functions.
        /// </summary>
        public void UpdateFunctions()
        {
            if(_functionsBrowser != null) {
                _functionsBrowser.UpdateUI();
            }
        }

        /// <summary>
        /// Saves the document to disk, and asks for a save path if the document has never been saved.
        /// </summary>
        public override void Save()
        {
            CommitAllChanges();

            try {
                base.Save();

                // We require the current undo stack to represent an unmodified state
                foreach(var w in AssociatedWindows) {
                    w.ForceUnmodified();
                }
                _upgraded = false;
                foreach(var w in AssociatedWindows) {
                    w.UpdateUI();
                }
            } catch(Exception e) {
                NotifyError(Configuration.GetLocalized("Unable to save document: {0}", e.Message));
            }
        }

        /// <summary>
        /// Gets a value indicating whether this <see cref="Petri.Application.GUI.GUIDocument"/> has been  modified since its last save.
        /// </summary>
        /// <value><c>true</c> if modified; otherwise, <c>false</c>.</value>
        public bool Modified {
            get {
                bool modified = false;
                foreach(var w in AssociatedWindows) {
                    modified = modified || w.Modified;
                }
                return modified || _upgraded;
            }
        }

        /// <summary>
        /// Commits all changes accross all windows.
        /// </summary>
        public void CommitAllChanges()
        {
            foreach(var w in AssociatedWindows) {
                w.CommitCurrentWidget();
            }
        }

        /// <summary>
        /// Upgrades the document to the last API version.
        /// </summary>
        /// <returns><c>true</c>, if document was upgraded, <c>false</c> otherwise.</returns>
        /// <param name="root">The root element of the document's XML descriptor.</param>
        /// <param name="apiVersion">The old API version.</param>
        protected override bool UpgradeDoc(XElement root, int apiVersion)
        {
            _upgraded = base.UpgradeDoc(root, apiVersion);
            return _upgraded;
        }

        /// <summary>
        /// Closes the document and asks a confirmation first, so the user can review his change and cancel the process.
        /// </summary>
        /// <returns><c>true</c>, if the document was allowed to be closed, <c>false</c> otherwise.</returns>
        public bool CloseAndConfirm()
        {
            CommitAllChanges();

            if(DebugController.Client.CurrentSessionState != DebugClient.SessionState.Stopped) {
                Window.Present();
                var d = new MessageDialog(Window,
                                          DialogFlags.Modal,
                                          MessageType.Question,
                                          ButtonsType.None,
                                          Configuration.GetLocalized("A debugger session is still running. Do you want to stop it?"));
                d.AddButton(Configuration.GetLocalized("Cancel"), ResponseType.Cancel);
                d.AddButton(Configuration.GetLocalized("Stop the session"), ResponseType.Yes).HasDefault = true;

                var result = GUIApplication.RunModalDialog(d);

                if(result == ResponseType.Yes) {
                    IsClosing = true;
                    DebugController.Client.Detach();
                    d.Destroy();
                } else {
                    d.Destroy();
                    return false;
                }
            }

            if(Modified) {
                Window.Present();
                var d = new MessageDialog(
                    Window,
                    DialogFlags.Modal,
                    MessageType.Question,
                    ButtonsType.None,
                    Configuration.GetLocalized("Do you want to save the changes made to '{0}'? If you don't save, all changes will be permanently lost.", Settings.Name)
                );
                d.AddButton(Configuration.GetLocalized("Don't save"), ResponseType.No);
                d.AddButton(Configuration.GetLocalized("Cancel"), ResponseType.Cancel);
                d.AddButton(Configuration.GetLocalized("Save"), ResponseType.Yes).HasDefault = true;

                var result = GUIApplication.RunModalDialog(d);

                if(result == ResponseType.Yes) {
                    Save();
                    d.Destroy();
                    if(Modified) {
                        return false;
                    }
                } else if(result == ResponseType.No) {
                    d.Destroy();
                } else {
                    d.Destroy();
                    return false;
                }
            }

            Close();

            return true;
        }

        /// <summary>
        /// Closes the document without conformation.
        /// </summary>
        void Close()
        {
            foreach(Window w in AssociatedWindows) {
                w.Destroy();
            }

            GUIApplication.RemoveDocument(this);
        }

        /// <summary>
        /// Exports the petri net as a PDF document. It asks the user for the save path, and the creates a multipage document, with each page corresponding to instances of the PetriNet class.
        /// So, the root petri net is exported as well as each inner petri nets, each one on its page.
        /// </summary>
        public void ExportAsPDF()
        {
            CommitAllChanges();

            string exportPath = "";

            var fc = new FileChooserDialog(
                Configuration.GetLocalized("Export the PDF as…"),
                Window,
                FileChooserAction.Save,
                new object[] {
                    Configuration.GetLocalized("Cancel"),
                    ResponseType.Cancel,
                    Configuration.GetLocalized("Save"),
                    ResponseType.Accept
                }
            );

            if(System.IO.Directory.Exists(Configuration.SavePath)) {
                fc.SetCurrentFolder(Configuration.SavePath);
            }
            fc.CurrentName = Settings.Name + ".pdf";

            fc.DoOverwriteConfirmation = true;

            var b = new CheckButton(Configuration.GetLocalized("Include background"));
            b.Active = true;
            fc.ActionArea.PackEnd(b);
            b.Show();

            if(GUIApplication.RunModalDialog(fc) == ResponseType.Accept) {
                exportPath = fc.Filename;
                Configuration.SavePath = System.IO.Directory.GetParent(exportPath).FullName;
                if(!exportPath.EndsWithInv(".pdf")) {
                    exportPath += ".pdf";
                }
            }
            fc.Destroy();

            if(exportPath != "") {
                var renderView = new RenderView(this, b.Active);

                renderView.Render(exportPath);
            }
        }

        /// <summary>
        /// Saves the document as a new document, at a path asked to the user.
        /// </summary>
        /// <returns><c>false</c>, if the process was cancelled, <c>true</c> otherwise.</returns>
        public bool SaveAs()
        {
            // Do no recursively enter this method
            if(!_saving) {
                try {
                    _saving = true;

                    var fc = new FileChooserDialog(
                        Configuration.GetLocalized("Save the petri net as…"),
                        Window,
                        FileChooserAction.Save,
                        new object[] {
                            Configuration.GetLocalized("Cancel"), ResponseType.Cancel,
                            Configuration.GetLocalized("Save"), ResponseType.Accept
                        }
                    );

                    if(System.IO.Directory.Exists(Configuration.SavePath)) {
                        fc.SetCurrentFolder(Configuration.SavePath);
                    }
                    var fn = Settings.Name;
                    if(Path != "") {
                        fn += " - " + Configuration.GetLocalized("filename Copy") + ".petri";
                    }
                    fc.CurrentName = fn;

                    fc.DoOverwriteConfirmation = true;

                    if(GUIApplication.RunModalDialog(fc) == ResponseType.Accept) {
                        var oldPath = Path == "" ? null : System.IO.Directory.GetParent(Path).FullName;
                        Path = fc.Filename;
                        fc.Destroy();

                        Configuration.SavePath = System.IO.Directory.GetParent(Path).FullName;

                        if(!Path.EndsWithInv(".petri")) {
                            Path += ".petri";
                        }

                        Settings.Name = System.IO.Path.GetFileName(Path).Split(
                            new string[] { ".petri" },
                            StringSplitOptions.None
                        )[0];

                        if(oldPath != null) {
                            var pathUpgrader = new Action<List<Tuple<string, bool>>>((collection) => {
                                for(int i = 0; i < collection.Count; ++i) {
                                    if(!System.IO.Path.IsPathRooted(collection[i].Item1)) {
                                        var newPath = GetRelativeToDoc(
                                            System.IO.Path.Combine(oldPath, collection[i].Item1)
                                        );

                                        collection[i] = Tuple.Create(
                                            newPath,
                                            collection[i].Item2
                                        );
                                    }
                                }
                            });
                            foreach(var profile in Settings.Profiles) {
                                pathUpgrader.Invoke(profile.CompilerInfo.IncludePaths);
                                pathUpgrader.Invoke(profile.CompilerInfo.LibPaths);
                            }
                        }

                        InvalidateHash();
                        InvalidateXml();

                        Save();

                        GUIApplication.AddRecentDocument(Path);
                        Window.UpdateRecentDocuments();
                        Window.UpdateTitle();

                        return true;
                    } else {
                        fc.Destroy();
                        return false;
                    }
                } finally {
                    _saving = false;
                }
            }

            return false;
        }

        /// <summary>
        /// Reverts/Restores the document to the content of its file.
        /// If the <see cref="Petri.Application.Document.Path"/> is empty, then this method sets up a new untitled document.
        /// On error, the error message is presented to the user, and the document is left in the state as before this call (strong exception guarantee).
        /// </summary>
        public void Restore()
        {
            if(Modified) {
                var result = Window.PromptYesNo(
                    Configuration.GetLocalized("Do you want to revert the petri net to the last opened version? ? All changes will be permanently lost."),
                    Configuration.GetLocalized("Revert")
                ).Result;

                if(!result) {
                    return;
                }

                Close();
                GUIApplication.OpenDocument(Path);
            }
        }

        /// <summary>
        /// Generates the document's code.
        /// If the code has never been generated for this document, a file save dialog asks for where the code is to be saved.
        /// Presents an error in case the document has not been saved for the first time.
        /// </summary>
        public bool GenerateCodeEnsureOutputPath()
        {
            CommitAllChanges();

            if(Settings.CurrentProfile.RelativeSourceOutputPath != "") {
                try {
                    GenerateCode();
                    Window.EditorGui.Status = Configuration.GetLocalized("The <language> code has been sucessfully generated.",
                                                                         Settings.LanguageName());

                    return true;
                } catch(GenerationException) {
                    return false;
                }
            } else {
                var fc = new FileChooserDialog(Configuration.GetLocalized("Save the generated code as…"), Window,
                                               FileChooserAction.SelectFolder,
                                               new object[] {
                                                   Configuration.GetLocalized("Cancel"), ResponseType.Cancel,
                                                   Configuration.GetLocalized("Save"), ResponseType.Accept
                                               }
                );

                if(GUIApplication.RunModalDialog(fc) == ResponseType.Accept) {
                    var filename = fc.Filename;
                    if(!filename.EndsWithInv("/")) {
                        filename += "/";
                    }
                    var newPath = GetRelativeToDoc(filename);
                    fc.Destroy();

                    // We bypass the CommitGUIAction/undo stack on purpose, it would be strange otherwise.
                    Settings.CurrentProfile.RelativeSourceOutputPath = newPath;

                    return GenerateCodeEnsureOutputPath();
                } else {
                    fc.Destroy();
                    return false;
                }
            }
        }

        /// <summary>
        /// Compiles the document. If some output is made by the invoked compiler, then the method returns false, true otherwise.
        /// </summary>
        /// <returns>The compilation success status.</returns>
        /// <param name="verbose">Whether we want verbose compilation output.</param>
        public override async Task<Tuple<Compiler.CompilationStatus, string>> Compile(bool verbose = false)
        {
            CommitAllChanges();

            try {
                if(_flatPetriNet == null) {
                    if(!GenerateCodeEnsureOutputPath()) {
                        return Tuple.Create(Compiler.CompilationStatus.Error, "");
                    }
                }

                Window.EditorGui.Compilation = true;
                Window.DebugGui.IsCompiling = true;

                GUIApplication.RunOnUIThread(() => {
                    Window.Gui.Status = Configuration.GetLocalized("Compiling…");
                });

                return await base.Compile(verbose);
            } catch {
                Window.Gui.Status = Configuration.GetLocalized("The compilation has failed.");
                throw;
            } finally {
                Window.EditorGui.Compilation = false;
                Window.DebugGui.IsCompiling = false;
            }
        }

        /// <summary>
        /// Deploy the compilation product of the document.
        /// </summary>
        /// <param name="verbose">Verbose output</param>
        /// <param name="outputPrefix">Prefix to insert before deployment script's output.</param>
        public override async Task<bool> Deploy(bool verbose = true, string outputPrefix = "")
        {
            CommitAllChanges();

            if(!IsCompiled) {
                await Compile(verbose);
            }

            return await base.Deploy(
                verbose,
                ANSIColorParser.LightGrayText + Configuration.GetLocalized("(deployment) ") + ANSIColorParser.ResetText);
        }

        /// <summary>
        /// Shows the compilation status to the user.
        /// </summary>
        /// <param name="status">The compilation success status.</param>
        /// <param name="output">Output.</param>
        public override void EchoCompilationStatus(Compiler.CompilationStatus status, string output)
        {
            GUIApplication.RunOnUIThread(() => {
                if(IssuesCount > 1) {
                    Window.EditorGui.Status = Configuration.GetLocalized("{0} entities with issue.",
                                                                         IssuesCount);
                } else if(IssuesCount == 1) {
                    Window.EditorGui.Status = Configuration.GetLocalized("1 entity with issue.");
                } else {
                    Window.EditorGui.Status = Configuration.GetLocalized("No entity issue.");
                }

                if(status != Compiler.CompilationStatus.Success) {
                    string header = status == Compiler.CompilationStatus.Error
                                                      ? Configuration.GetLocalized("Compilation errors: {0}", output)
                                                      : Configuration.GetLocalized("Warnings: {0}", output);
                    Window.SetIssuesContent(header);
                } else {
                    Window.SetIssuesContent(null);
                }

                Window.EditorGui.PropertiesPanel.Reset();

                if(status == Compiler.CompilationStatus.Error) {
                    Window.Gui.Status = Configuration.GetLocalized("The compilation has failed.");
                } else {
                    Window.Gui.Status = Configuration.GetLocalized("The compilation was successful.");
                }
            });
        }

        /// <summary>
        /// Shows the document's preprocessor macros' manager.
        /// </summary>
        public void ManageMacros()
        {
            if(_macrosManager == null) {
                _macrosManager = new MacrosManager(this);
            }

            _macrosManager.ShowWindow();
        }

        /// <summary>
        /// Show the document's headers editor, or presents an error in case the document has not been saved for the first time.
        /// </summary>
        public void ManageHeaders()
        {
            if(_headersManager == null) {
                _headersManager = new HeadersManager(this);
            }

            _headersManager.ShowWindow();
        }

        /// <summary>
        /// Shows a list of the functions known to the editor.
        /// </summary>
        public void ShowKnownFunctions()
        {
            if(_functionsBrowser == null) {
                _functionsBrowser = new FunctionsBrowser(this);
            }

            _functionsBrowser.ShowWindow();
        }

        /// <summary>
        /// Show the document's settings editor, or presents an error in case the document has not been saved for the first time.
        /// </summary>
        public void EditSettings(DocumentSettingsEditor.EditorPage page = DocumentSettingsEditor.EditorPage.Current)
        {
            if(_settingsEditor == null) {
                _settingsEditor = new DocumentSettingsEditor(this);
            }

            _settingsEditor.ShowPage(page);
        }

        /// <summary>
        /// Shows the find panel.
        /// </summary>
        public void Find()
        {
            if(_findPanel == null) {
                _findPanel = new FindPanel(this);
            }

            _findPanel.ShowWindow();
        }

        /// <summary>
        /// Notifies an error to the user.
        /// </summary>
        /// <param name="error">Error.</param>
        public override void NotifyError(string error)
        {
            GUIApplication.RunOnUIThread(() => {
                GUIApplication.NotifyUnrecoverableError(Window, error);
            });
        }

        /// <summary>
        /// Notifies a warning message to the user.
        /// </summary>
        /// <param name="warning">The message.</param>
        public override void NotifyWarning(string warning)
        {
            GUIApplication.RunOnUIThread(() => {
                var d = new MessageDialog(Window,
                                          DialogFlags.Modal,
                                          MessageType.Warning,
                                          ButtonsType.None,
                                          GUIApplication.SafeMarkupFromString(warning));
                d.AddButton(Configuration.GetLocalized("OK"), ResponseType.Cancel);
                GUIApplication.RunModalDialog(d);
                d.Destroy();
            });
        }

        /// <summary>
        /// Notifies some information to the user.
        /// </summary>
        /// <param name="info">info.</param>
        public override void NotifyInfo(string info)
        {
            GUIApplication.RunOnUIThread(() => {
                var d = new MessageDialog(Window,
                                          DialogFlags.Modal,
                                          MessageType.Info,
                                          ButtonsType.None,
                                          GUIApplication.SafeMarkupFromString(info));
                d.AddButton(Configuration.GetLocalized("OK"), ResponseType.Cancel);
                GUIApplication.RunModalDialog(d);
                d.Destroy();
            });
        }

        /// <summary>
        /// Returns whether the document is currently being closed.
        /// </summary>
        /// <value><c>true</c> if is closing; otherwise, <c>false</c>.</value>
        public bool IsClosing {
            get;
            private set;
        }

        /// <summary>
        /// Fills the document's XML content.
        /// </summary>
        protected override void FillXML(XElement root)
        {
            base.FillXML(root);

            // If Window is null, the document is currenty loading and we have no interest in the window properties.
            if(Window != null) {
                var winConf = Settings.XMLBackup.Element("Window");
                if(winConf == null) {
                    winConf = new XElement("Window");
                    Settings.XMLBackup.Add(winConf);
                }
                Window.GetPosition(out int x, out int y);
                Window.GetSize(out int w, out int h);
                winConf.SetAttributeValue("X", x.ToString());
                winConf.SetAttributeValue("Y", y.ToString());
                winConf.SetAttributeValue("W", w.ToString());
                winConf.SetAttributeValue("H", h.ToString());
            }
        }

        /// <summary>
        /// Invalidates the XML cache.
        /// </summary>
        public override void InvalidateXml()
        {
            base.InvalidateXml();
            DebugDocument?.InvalidateXml();
        }

        FunctionsBrowser _functionsBrowser;
        HeadersManager _headersManager;
        MacrosManager _macrosManager;
        DocumentSettingsEditor _settingsEditor;
        FindPanel _findPanel;
        bool _saving = false;
        bool _upgraded;
    }
}
