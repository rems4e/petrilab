//
//  ANSIColorParser.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2017-12-05.
//

using System.Linq;

using Gtk;

namespace Petri.Application.GUI
{
    /// <summary>
    /// A class that parses ANSI color escape sequences.
    /// </summary>
    public class ANSIColorParser
    {
        enum TermAttr
        {
            TERM_RESET = 0,
            TERM_BRIGHT = 1,
            TERM_DIM = 2,
            TERM_UNDERLINE = 4,
            TERM_BLINK = 5,
            TERM_REVERSE = 7,
        };

        enum TermColor
        {
            TERM_BLACK = 0,
            TERM_RED,
            TERM_GREEN,
            TERM_YELLOW,
            TERM_BLUE,
            TERM_MAGENTA,
            TERM_CYAN,
            TERM_WHITE,
            TERM_NONE
        }

        /// <summary>
        /// The ANSI escape sequence we are interested in.
        /// </summary>
        static readonly string[] SequenceDelimiter = { new string((char)0x1B, 1) + "[" };

        /// <summary>
        /// The escape sequence that resets the text
        /// </summary>
        public static readonly string ResetText = SequenceDelimiter[0] + "0;0;0m";

        /// <summary>
        /// The escape sequence that resets the text
        /// </summary>
        public static readonly string LightGrayText = SequenceDelimiter[0] + "2;37;48m";

        /// <summary>
        /// Gets an RGB representation from the given color, attribute and destination
        /// </summary>
        /// <returns>RGB representation of the ANSI escape sequence.</returns>
        /// <param name="color">The color.</param>
        /// <param name="attr">The attribute.</param>
        /// <param name="background">If set to <c>true</c> we are handling a background color, else a foreground color.</param>
        static string RGBFromANSI(TermColor color, TermAttr attr, bool background)
        {
            string result;
            switch(color) {
            case TermColor.TERM_RED:
                result = "#FF0000";
                break;
            case TermColor.TERM_BLUE:
                result = "#0000FF";
                break;
            case TermColor.TERM_GREEN:
                result = "#00FF00";
                break;
            case TermColor.TERM_YELLOW:
                result = "#FFFF00";
                break;
            case TermColor.TERM_MAGENTA:
                result = "#FF00FF";
                break;
            case TermColor.TERM_CYAN:
                result = "#00FFFF";
                break;
            case TermColor.TERM_WHITE:
                result = "#FFFFFF";
                break;
            case TermColor.TERM_NONE:
                return background ? "#FFFFFF" : "#000000";
            case TermColor.TERM_BLACK:
            default:
                result = "#000000";
                break;
            }

            if(attr == TermAttr.TERM_DIM) {
                result = result.Replace("FF", "99");
            }

            if(background) {
                result = result.Replace("FF", "BB").Replace("99", "66");
            }

            return result;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Editor.GUI.ANSIColorParser"/> class.
        /// </summary>
        /// <param name="buf">The text buffer to apply the formatting to.s</param>
        public ANSIColorParser(TextBuffer buf)
        {
            _buffer = buf;
        }

        /// <summary>
        /// Parses ANSI colors escape sequences, remove them from the text and transform them into text formatting.
        /// </summary>
        /// <param name="text">Text.</param>
        public void ParseANSIColors(string text)
        {
            var parts = text.Split(SequenceDelimiter, System.StringSplitOptions.RemoveEmptyEntries);

            foreach(var part in parts) {
                var a = part.Split(';');
                int colorSequenceLength = 0;
                try {
                    if(a.Length > 2) {
                        if((a[2].Length >= 1 && a[2][0] == 'm') ||
                           (a[2].Length >= 2 && a[2][1] == 'm') ||
                           (a[2].Length >= 3 && a[2][2] == 'm')) {
                            int attr = int.Parse(a[0]);
                            if(attr == (int)TermAttr.TERM_RESET) {
                                _lastForeground = "#000000";
                                _lastBackground = "#FFFFFF";
                            } else {
                                int front = int.Parse(a[1]) - 30;
                                int back = int.Parse(new string(a[2].TakeWhile((char c) => {
                                    return char.IsDigit(c);
                                }).ToArray())) - 40;

                                _lastForeground = RGBFromANSI((TermColor)front, (TermAttr)attr, false);
                                _lastBackground = RGBFromANSI((TermColor)back, (TermAttr)attr, true);
                            }

                            colorSequenceLength = a[0].Length + 1 + a[1].Length + 1 + (a[2][0] == 'm' ? 1 : a[2][1] == 'm' ? 2 : 3);
                        }
                    }
                } catch(System.Exception) {
                    // Malformed sequence, just pass through.
                }

                var name = _lastBackground + _lastForeground;
                var tag = _buffer.TagTable.Lookup(name);
                if(tag == null) {
                    tag = new TextTag(name);
                    _buffer.TagTable.Add(tag);
                    tag.Background = _lastBackground;
                    tag.Foreground = _lastForeground;
                }

                string cleaned = part.Substring(colorSequenceLength);
                var iter = _buffer.EndIter;
                _buffer.InsertWithTags(ref iter, cleaned, tag);
            }
        }

        TextBuffer _buffer;
        string _lastForeground = null;
        string _lastBackground = null;
    }
}
