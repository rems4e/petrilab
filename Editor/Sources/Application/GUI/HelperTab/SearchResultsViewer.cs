//
//  SearchResultsViewer.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2017-06-18.
//

using System.Collections.Generic;

using Gtk;

namespace Petri.Application.GUI
{
    /// <summary>
    /// A helper tab that present the search results to the user.
    /// </summary>
    public class SearchResultsViewer : HelperTab
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.SearchResultsViewer"/> class.
        /// </summary>
        /// <param name="doc">Document.</param>
        /// <param name="nb">The notebook to add this tab to.</param>
        public SearchResultsViewer(GUIDocument doc, Notebook nb) : base(nb, Configuration.GetLocalized("Search results"))
        {
            _document = doc;

            _findView = new TreeView();
            _findView.RowActivated += (o, args) => {
                TreeIter iter;
                _findStore.GetIter(out iter, args.Path);
                var entity = _findStore.GetValue(iter, 4) as Model.Entity;
                if(_editorOrDebugger) {
                    _document.Window.SwitchToEditor();
                } else {
                    _document.Window.SwitchToDebug();
                }
                _document.Window.Gui.BaseView.SetSelection(entity);
            };

            var c0 = new TreeViewColumn();
            c0.Title = Configuration.GetLocalized("ID");
            var idCell = new CellRendererText();
            c0.PackStart(idCell, true);
            c0.AddAttribute(idCell, "text", 0);

            var c1 = new TreeViewColumn();
            c1.Title = Configuration.GetLocalized("Kind");
            var typeCell = new CellRendererText();
            c1.PackStart(typeCell, true);
            c1.AddAttribute(typeCell, "text", 1);

            var c2 = new TreeViewColumn();
            c2.Title = Configuration.GetLocalized("Name");
            var nameCell = new CellRendererText();
            c2.PackStart(nameCell, true);
            c2.AddAttribute(nameCell, "text", 2);

            var c3 = new TreeViewColumn();
            c3.Title = Configuration.GetLocalized("Value");
            var valueCell = new CellRendererText();
            c3.PackStart(valueCell, true);
            c3.AddAttribute(valueCell, "text", 3);

            _findView.AppendColumn(c0);
            _findView.AppendColumn(c1);
            _findView.AppendColumn(c2);
            _findView.AppendColumn(c3);
            _findStore = new ListStore(typeof(string),
                                       typeof(string),
                                       typeof(string),
                                       typeof(string),
                                       typeof(Model.Entity));

            _findView.Model = _findStore;

            var findScroll = new ScrolledWindow();
            findScroll.SetPolicy(PolicyType.Automatic, PolicyType.Automatic);

            findScroll.Add(_findView);
            Add(findScroll);
        }


        /// <summary>
        /// Performs a find action.
        /// </summary>
        /// <param name="what">The text to search.</param>
        /// <param name="which">The kind of entities to search.</param>
        /// <param name="where">The fields to search into.</param>
        /// <param name="regex">Whether to search with a regex pattern.</param>
        public void PerformFind(string what, Controller.FindWhich which, Controller.FindWhere where, bool regex)
        {
            try {
                _editorOrDebugger = _document.CurrentController == _document.EditorController;
                var list = _document.CurrentController.Find(which, where, what, regex);
                _findStore.Clear();

                foreach(var ee in list) {
                    if(ee is Model.State) {
                        var e = (Model.State)ee;
                        _findStore.AppendValues(e.GlobalID.ToString(),
                                                Configuration.GetLocalized("State"),
                                                e.Name,
                                                (e is Model.Action) ? ((Model.Action)e).Invocation.MakeUserReadable() : "-",
                                                e);
                    } else if(ee is Model.Transition) {
                        var e = (Model.Transition)ee;
                        _findStore.AppendValues(e.GlobalID.ToString(),
                                                Configuration.GetLocalized("Transition"),
                                                e.Name,
                                                e.Condition.MakeUserReadable(),
                                                e);
                    } else if(ee is Model.Comment) {
                        var e = (Model.Comment)ee;
                        _findStore.AppendValues(e.GlobalID.ToString(),
                                                Configuration.GetLocalized("Comment"),
                                                "-",
                                                e.Name,
                                                e);
                    }
                }

                Present();
                _document.Window.Gui.Status = Configuration.GetLocalized("{0} occurrences found.", list.Count);
            } catch(System.ArgumentException exc) {
                if(regex) {
                    _document.NotifyError(Configuration.GetLocalized("The given regular expression is invalid ({0})!",
                                                                     exc.Message));
                } else {
                    // Should never happen.
                    throw;
                }
            }
        }

        bool _editorOrDebugger;
        List<Model.Entity> _findResults = new List<Model.Entity>();
        TreeView _findView;
        ListStore _findStore;
        GUIDocument _document;
    }
}

