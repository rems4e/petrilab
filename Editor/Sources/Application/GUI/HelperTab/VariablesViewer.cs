//
//  VariablesViewer.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2015-01-20.
//

using System;
using System.Collections.Generic;
using System.Linq;

using Gtk;

using Petri.Application.Debugger;

namespace Petri.Application.GUI
{
    /// <summary>
    /// A helper tab that allows the user to view or set a petri net's variables' values.
    /// </summary>
    public class VariablesViewer : HelperTab
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.VariablesViewer"/> class.
        /// </summary>
        /// <param name="controller">The debug controller.</param>
        /// <param name="nb">The notebook to add this tab to.</param>
        public VariablesViewer(DebugController controller, Notebook nb) : base(nb, Configuration.GetLocalized("Variables"))
        {
            _controller = controller;

            var scrolledWindow = new ScrolledWindow();
            scrolledWindow.SetPolicy(PolicyType.Automatic, PolicyType.Automatic);

            _table = new TreeView();
            scrolledWindow.Add(_table);

            {
                var c = new TreeViewColumn();
                c.Title = Configuration.GetLocalized("Name");
                var nameCell = new VariableCellRenderer();
                c.PackStart(nameCell, true);
                c.AddAttribute(nameCell, "tuple", 0);
                _table.AppendColumn(c);
            }

            {
                var c = new TreeViewColumn();
                c.Title = Configuration.GetLocalized("Value");
                var valueCell = new ParameterCellRenderer();
                valueCell.EditingStarted += (o, args) => {
                    if(_editing) {
                        return;
                    }
                    _editing = true;
                    TreePath path = new TreePath(args.Path);
                    _dataStore.GetIter(out var iter, path);
                    _editIter = iter;

                    var current = (string)_dataStore.GetValue(_editIter.Value, 3);
                    _dataStore.SetValue(_editIter.Value, 1, current);
                    GUIApplication.RunOnUIThread(() => {
                        _table.SetCursorOnCell(path, c, valueCell, true);
                        _editing = false;
                    }, true);
                };
                valueCell.Edited += (object o, EditedArgs args) => {
                    if(_editing) {
                        return;
                    }
                    try {
                        var value = Int64.Parse(args.NewText);
                        var tup = (Tuple<Model.RootPetriNet, Code.VariableExpression>)_dataStore.GetValue(_editIter.Value, 0);
                        if(_controller.Client.PetriAlive) {
                            _controller.Client.SetVariable(tup.Item1, tup.Item2, value);
                        }

                        if(tup.Item1.Parameters.Contains(tup.Item2)) {
                            OverridenValues[tup.Item2] = value.ToString();
                        }

                        EndEditing(c, valueCell, value.ToString());
                    } catch {
                        args.RetVal = false;
                        EndEditing(c, valueCell, (string)_dataStore.GetValue(_editIter.Value, 1));
                    }
                };
                valueCell.EditingCanceled += (sender, e) => {
                    EndEditing(c, valueCell, (string)_dataStore.GetValue(_editIter.Value, 1));
                };
                c.PackStart(valueCell, true);
                c.AddAttribute(valueCell, "text", 1);
                c.AddAttribute(valueCell, "editable", 2);
                c.AddAttribute(valueCell, "when_editing", 3);
                _table.AppendColumn(c);
            }

            _dataStore = new ListStore(typeof(Tuple<Model.PetriNet, Code.VariableExpression>), typeof(string), typeof(bool), typeof(string));
            _table.Model = _dataStore;

            var update = new Button(GUIApplication.LabelFromString(Configuration.GetLocalized("Update", true)));
            update.Clicked += (object sender, EventArgs e) => {
                Update();
            };
            var actionBox = new VBox(false, 5);
            actionBox.PackStart(update, false, false, 0);

            var hbox = new HBox(false, 5);
            hbox.PackStart(scrolledWindow, true, true, 0);
            hbox.PackStart(actionBox, false, true, 0);
            Add(hbox);

            OverridenValues = new Dictionary<Code.VariableExpression, string>();
        }

        /// <summary>
        /// Sets the value of a cell to the correct formatted value.
        /// </summary>
        /// <param name="column">Column.</param>
        /// <param name="cell">Cell.</param>
        /// <param name="value">The value to put in the formatted text.</param>
        void EndEditing(TreeViewColumn column, CellRenderer cell, string value)
        {
            if(_editing) {
                return;
            }
            _editing = true;
            var tup = (Tuple<Model.RootPetriNet, Code.VariableExpression>)_dataStore.GetValue(_editIter.Value, 0);
            var res = GetDisplayValues(tup.Item2, tup.Item1, value);
            string formattedText = res.Item1;
            string whenEditing = res.Item2;
            bool isParameter = res.Item3;

            _dataStore.SetValue(_editIter.Value, 1, formattedText);
            _dataStore.SetValue(_editIter.Value, 3, whenEditing);

            GUIApplication.RunOnUIThread(() => {
                var path = _dataStore.GetPath(_editIter.Value);
                _table.SetCursorOnCell(path, column, cell, false);
                _editing = false;
            }, true);
        }

        /// <summary>
        /// Gets the overriden values of the petri net parameters.
        /// </summary>
        /// <value>The overriden values.</value>
        public Dictionary<Code.VariableExpression, string> OverridenValues {
            get;
            private set;
        }

        /// <summary>
        /// Show the variable viewer.
        /// </summary>
        public override void Present()
        {
            Update();
            base.Present();
        }

        /// <summary>
        /// Updates the variables list.
        /// </summary>
        public void Update()
        {
            _controller.Client.RequestVariables();
        }

        /// <summary>
        /// Updates the variables list with the given variables names and values.
        /// </summary>
        /// <param name="variables">Variables.</param>
        public void UpdateWithVars(List<Tuple<Model.RootPetriNet, Code.VariableExpression, string>> variables)
        {
            _dataStore.Clear();

            // This removes overriden values that are not parameters any more
            foreach(var s in OverridenValues.Where((kvp) => {
                return !_controller.PetriNet.Parameters.Contains(kvp.Key);
            }).ToList()) {
                OverridenValues.Remove(s.Key);
            }

            foreach(var p in _controller.PetriNet.Parameters) {
                if(!OverridenValues.ContainsKey(p) || OverridenValues[p] == _controller.PetriNet.Variables[p].ToString()) {
                    OverridenValues[p] = null;
                }
            }

            foreach(var v in variables) {
                var res = GetDisplayValues(v.Item2, v.Item1, v.Item3);
                string formattedText = res.Item1;
                string whenEditing = res.Item2;
                bool isParameter = res.Item3;

                bool isEditable = _controller.Client.CurrentPetriState == DebugClient.PetriState.Paused
                          || (_controller.Client.CurrentPetriState == DebugClient.PetriState.Stopped
                             && isParameter);

                _dataStore.AppendValues(Tuple.Create(v.Item1, v.Item2), formattedText, isEditable, whenEditing);
            }

            ShowAll();
        }

        /// <summary>
        /// Gets the contents of a variables row. The first item is the formatted value (either a number or text
        /// containing the number. The second item is the current value of the variable. The thirs item is whether the
        /// variable is a parameter.
        /// </summary>
        /// <returns>The display values.</returns>
        /// <param name="name">The variable.</param>
        /// <param name="pn">The petri net enclosing the variable.</param>
        /// <param name="currentValue">The current value of the variable.</param>
        Tuple<string, string, bool> GetDisplayValues(Code.VariableExpression name, Model.RootPetriNet pn, string currentValue)
        {
            bool isParameter = OverridenValues.ContainsKey(name) && pn == _controller.PetriNet;

            string formattedText;
            string whenEditing;
            if(!isParameter) {
                formattedText = currentValue;
                whenEditing = currentValue;
            } else {
                var def = pn.Variables[name];

                if(OverridenValues[name] == null || OverridenValues[name] == def.ToString()) {
                    formattedText = Configuration.GetLocalized("{0} (default value)", currentValue);
                    whenEditing = currentValue;
                } else {
                    formattedText = Configuration.GetLocalized("{0} (default value is {1})", OverridenValues[name], def);
                    whenEditing = OverridenValues[name];
                }
            }

            return Tuple.Create(formattedText, whenEditing, isParameter);
        }

        bool _editing = false;
        TreeIter? _editIter;

        TreeView _table;
        ListStore _dataStore;
        DebugController _controller;
    }

    class VariableCellRenderer : CellRendererText
    {
        [GLib.Property("tuple")]
        public Tuple<Model.RootPetriNet, Code.VariableExpression> Tuple {
            get {
                return _tuple;
            }
            set {
                _tuple = value;
                Text = _tuple.Item1.VariableFullName(_tuple.Item2);
            }
        }

        Tuple<Model.RootPetriNet, Code.VariableExpression> _tuple;
    }

    class ParameterCellRenderer : CellRendererText
    {
        [GLib.Property("when_editing")]
        public string WhenEditing {
            get;
            set;
        }
    }
}
