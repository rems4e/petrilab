//
//  IssuesViewer.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2017-06-18.
//

using Gtk;

namespace Petri.Application.GUI
{
    /// <summary>
    /// A helper tab that present the compilation issues to the user.
    /// </summary>
    public class IssuesViewer : HelperTab
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.IssuesViewer"/> class.
        /// </summary>
        /// <param name="nb">The notebook to add this tab to.</param>
        public IssuesViewer(Notebook nb) : base(nb, Configuration.GetLocalized("Issues"))
        {
            var issuesScroll = new ScrolledWindow();
            issuesScroll.SetPolicy(PolicyType.Automatic, PolicyType.Automatic);

            _issuesBuffer = new TextBuffer(new TextTagTable());
            var textView = new TextView(_issuesBuffer);
            textView.WrapMode = WrapMode.WordChar;
            textView.Editable = false;

            var fontDescription = new Pango.FontDescription();
            fontDescription.Family = "monospace";
            textView.ModifyFont(fontDescription);

            var viewport = new Viewport();
            viewport.Add(textView);
            issuesScroll.Add(viewport);
            Add(issuesScroll);
        }

        /// <summary>
        /// Sets the content of the compilation issues view.
        /// May be <c>null</c>, causing the issues text to be cleared.
        /// </summary>
        /// <param name="text">Text.</param>
        public void SetIssuesContent(string text)
        {
            if(text == null) {
                _issuesBuffer.Text = "";
            } else {
                _issuesBuffer.Text = text;
                Present();
            }
        }

        TextBuffer _issuesBuffer;
    }
}

