//
//  HelperTab.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2017-06-18.
//

using Gtk;

namespace Petri.Application.GUI
{
    /// <summary>
    /// A base class for a tab to put in a Gtk.Notebook.
    /// </summary>
    public abstract class HelperTab : VBox
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.HelperTab"/> class.
        /// </summary>
        /// <param name="nb">The notebook that will contain this tab.</param>
        /// <param name="label">The name of this tab.</param>
        protected HelperTab(Notebook nb, string label) : base(false, 5)
        {
            _isPresenting = true;

            _notebook = nb;
            _mypage = _notebook.NPages;
            _notebook.SwitchPage += (object o, SwitchPageArgs args) => {
                if(!_isPresenting && args.PageNum == _mypage) {
                    Present();
                }
            };

            var eb = new EventBox();
            _notebook.AppendPage(eb, GUIApplication.LabelFromString(label));
            eb.Add(this);

            // This is a fix to what appears to be an awkward GTK bug.
            // Indeed, a notebook's tab has a full white background on creation,
            // and that integrates poorly with other widgets.
            eb.ModifyBg(StateType.Normal, Style.Background(StateType.Normal));

            _isPresenting = false;
        }

        /// <summary>
        /// Show the notebook page.
        /// </summary>
        public virtual void Present()
        {
            _isPresenting = true;
            _notebook.Page = _mypage;
            _isPresenting = false;
        }

        Notebook _notebook;
        int _mypage;
        bool _isPresenting;
    }
}

