//
//  ConsoleOutputViewer.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2017-06-18.
//

using Gtk;

namespace Petri.Application.GUI
{
    /// <summary>
    /// A helper tab that present the console output to the user.
    /// </summary>
    public class ConsoleOutputViewer : HelperTab
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.ConsoleOutputViewer"/> class.
        /// </summary>
        /// <param name="nb">The notebook to add this tab to.</param>
        /// <param name="lockMenuLabel">The console lock menu item's label.</param>
        public ConsoleOutputViewer(Notebook nb,
                                   Label lockMenuLabel) : base(nb, Configuration.GetLocalized("Console output"))
        {
            _lockMenuLabel = lockMenuLabel;

            _consoleScroll = new ScrolledWindow();
            _consoleScroll.SetPolicy(PolicyType.Automatic, PolicyType.Automatic);

            _consoleBuffer = new TextBuffer(new TextTagTable());
            var textView = new TextView(_consoleBuffer);
            textView.WrapMode = WrapMode.WordChar;
            textView.Editable = false;

            var clearConsole = new Button(GUIApplication.LabelFromString(Configuration.GetLocalized("cons_Clear"), true));
            _lockConsole = new Button(GUIApplication.LabelFromString(Configuration.GetLocalized("cons_Unlock"), true));
            clearConsole.Clicked += (sender, e) => {
                ClearConsole();
            };
            _lockConsole.Clicked += (sender, e) => {
                ToggleConsoleLock();
            };

            var consoleActionBox = new VBox(false, 0);
            consoleActionBox.PackStart(clearConsole, false, false, 0);
            consoleActionBox.PackStart(_lockConsole, false, false, 0);

            var consoleBox = new HBox(false, 5);
            consoleBox.PackStart(_consoleScroll, true, true, 0);
            consoleBox.PackStart(consoleActionBox, false, true, 0);
            var viewport = new Viewport();
            viewport.Add(textView);
            _consoleScroll.Add(viewport);
            Add(consoleBox);

            _colorParser = new ANSIColorParser(_consoleBuffer);

            ClearConsole();
        }

        /// <summary>
        /// Clears the console's content.
        /// </summary>
        public void ClearConsole()
        {
            _consoleBuffer.Text = "";
            AppendConsoleContent(ANSIColorParser.LightGrayText
                                 + Configuration.GetLocalized("Text output by a petri net execution will go there.")
                                 + ANSIColorParser.ResetText + "\n");
            _emptyConsole = true;
        }

        /// <summary>
        /// Toggles the console scroll lock.
        /// </summary>
        public void ToggleConsoleLock()
        {
            _consoleLocked = !_consoleLocked;
            if(_consoleLocked) {
                ((Label)_lockConsole.Child).Text = Configuration.GetLocalized("cons_Unlock");
                _lockMenuLabel.Text = Configuration.GetLocalized("Unlock Console Scrolling");
            } else {
                ((Label)_lockConsole.Child).Text = Configuration.GetLocalized("cons_Lock");
                _lockMenuLabel.Text = Configuration.GetLocalized("Lock Console Scrolling");
            }
        }

        /// <summary>
        /// Appends text to the console view.
        /// </summary>
        /// <param name="text">Text.</param>
        public void AppendConsoleContent(string text)
        {
            if(_emptyConsole) {
                _consoleBuffer.Text = "";
                _emptyConsole = false;
            }
            _colorParser.ParseANSIColors(text);
            if(_consoleLocked) {
                _consoleScroll.Vadjustment.Value = _consoleScroll.Vadjustment.Upper;
            }
        }

        TextBuffer _consoleBuffer;
        bool _emptyConsole = true;
        ScrolledWindow _consoleScroll;
        bool _consoleLocked = true;

        Button _lockConsole;
        Label _lockMenuLabel;

        ANSIColorParser _colorParser;
    }
}

