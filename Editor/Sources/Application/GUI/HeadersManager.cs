//
//  HeadersManager.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2014-11-13.
//

using System;

using Gtk;

namespace Petri.Application.GUI
{
    /// <summary>
    /// The window that allows the user to add or remove headers to the document.
    /// </summary>
    public class HeadersManager : DocumentHelperWindow
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.HeadersManager"/> class.
        /// </summary>
        /// <param name="doc">Document.</param>
        public HeadersManager(GUIDocument doc) : base(doc, 300, 300)
        {
            _vbox.BorderWidth = 0;

            var scrolledWindow = new ScrolledWindow();
            scrolledWindow.SetPolicy(PolicyType.Automatic, PolicyType.Automatic);

            _vbox.Add(scrolledWindow);

            _table = new TreeView();
            scrolledWindow.Add(_table);
            var c = new TreeViewColumn();
            c.Title = Configuration.GetLocalized("File HeadersManager");
            var fileCell = new CellRendererText();
            c.PackStart(fileCell, true);
            c.AddAttribute(fileCell, "text", 0);

            _table.AppendColumn(c);
            _headersStore = new ListStore(typeof(string));
            _table.Model = _headersStore;

            var hbox = new HBox(false, 5);
            var plus = new Button(GUIApplication.LabelFromString("+", true));
            var minus = new Button(GUIApplication.LabelFromString("-", true));
            plus.Clicked += OnAdd;
            minus.Clicked += OnRemove;
            hbox.PackStart(plus, false, false, 0);
            hbox.PackStart(minus, false, false, 0);
            if(!GUIApplication.IsReadOnly) {
                _vbox.PackStart(hbox, false, false, 0);
            }
        }

        /// <summary>
        /// Gets the string that should be used as the window's title.
        /// </summary>
        /// <value>The current title.</value>
        public override string CurrentTitle {
            get {
                return Configuration.GetLocalized("Headers of {0}", Document.Settings.Name);
            }
        }

        /// <summary>
        /// Builds the list of headers.
        /// </summary>
        public override void UpdateUI()
        {
            base.UpdateUI();
            _headersStore.Clear();
            foreach(string h in Document.Settings.Headers) {
                _headersStore.AppendValues(h);
            }
        }

        /// <summary>
        /// Called when a header is to be removed.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">E.</param>
        void OnRemove(object sender, EventArgs e)
        {
            TreeIter iter;
            TreePath[] treePath = _table.Selection.GetSelectedRows();

            for(int i = treePath.Length; i > 0; i--) {
                _headersStore.GetIter(out iter, treePath[(i - 1)]);
                CommitGuiAction(new RemoveHeaderAction(Document, _headersStore.GetValue(iter, 0) as string));
            }
        }

        /// <summary>
        /// Called when a header is to be added.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">E.</param>
        void OnAdd(object sender, EventArgs e)
        {
            var fc = new FileChooserDialog(
                Configuration.GetLocalized("Choose a file containing the <language> declarations…", Document.Settings.LanguageName()),
                this,
                FileChooserAction.Open,
                new object[] {
                    Configuration.GetLocalized("Cancel"),
                    ResponseType.Cancel,
                    Configuration.GetLocalized("Open"),
                    ResponseType.Accept
                }
            );

            var b = new CheckButton(Configuration.GetLocalized("Relative path"));
            b.Active = Configuration.PreferRelativePaths;
            fc.ActionArea.PackEnd(b);
            b.Show();

            if(GUIApplication.RunModalDialog(fc) == ResponseType.Accept) {
                string filename = fc.Filename;
                if(b.Active) {
                    filename = Document.GetRelativeToDoc(filename);
                }
                if(filename.Length > 0) {
                    CommitGuiAction(new AddHeaderAction(Document, filename));
                }
            }
            fc.Destroy();
        }

        TreeView _table;
        ListStore _headersStore;
    }
}

