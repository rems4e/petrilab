//
//  DocumentHelperGUIAction.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2017-02-18.
//

using System.Xml.Linq;

using Petri.Model;

namespace Petri.Application.GUI
{
    /// <summary>
    /// Change the settings of a document.
    /// </summary>
    public class ChangeSettingsAction : GuiAction
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.ChangeSettingsAction"/> class.
        /// </summary>
        /// <param name="editor">The settings editor.</param>
        /// <param name="newSettingsXML">The new settings' XML representation.</param>
        /// <param name="widget">The widget of the settings to display when focused.</param>
        /// <param name="page">The page of the settings to display when focused.</param>
        /// <param name="newSelectedProfile">The new index of the profile to select.</param>
        /// <param name="oldSelectedProfile">The index of the old selected profile.</param>
        public ChangeSettingsAction(DocumentSettingsEditor editor,
                                    XElement newSettingsXML,
                                    Gtk.Widget widget,
                                    DocumentSettingsEditor.EditorPage? page,
                                    int newSelectedProfile = -1,
                                    int oldSelectedProfile = -1)
        {
            if(oldSelectedProfile == -1) {
                oldSelectedProfile = editor.EditedProfileIndex;
            }
            if(newSelectedProfile == -1) {
                newSelectedProfile = oldSelectedProfile;
            }
            _editor = editor;
            _newSettingsXML = newSettingsXML;
            _oldSettingsXML = _editor.Settings.GetXml();
            _widget = widget;
            _page = page;
            _newSelectedProfile = newSelectedProfile;
            _oldSelectedProfile = oldSelectedProfile;
        }

        /// <summary>
        /// Apply this instance, actually applying the Gui change it encapsulates.
        /// </summary>
        public override void Apply()
        {
            _editor.Settings.AssignFromXml(_newSettingsXML);
            if(_newSelectedProfile != -1) {
                _editor.EditedProfile = _editor.Document.Settings.Profiles[_newSelectedProfile];
            }
        }

        /// <summary>
        /// Gets an instance having the opposite effect of <c>this</c>.
        /// </summary>
        public override GuiAction Reverse()
        {
            return new ChangeSettingsAction(_editor, _oldSettingsXML, _widget, _page, _oldSelectedProfile, _newSelectedProfile);
        }

        /// <summary>
        /// The object that is meant to gain the user's focus upon Apply()ing.
        /// </summary>
        /// <value>The focus.</value>
        public override Focusable Focus {
            get {
                return new FocusableSettings(_widget, _page);
            }
        }

        /// <summary>
        /// Gets the description of the GUI action's effect.
        /// </summary>
        /// <value>The description.</value>
        public override string Description {
            get {
                return Configuration.GetLocalized("Change document settings");
            }
        }

        Gtk.Widget _widget;
        DocumentSettingsEditor.EditorPage? _page;
        int _newSelectedProfile;
        int _oldSelectedProfile;
        DocumentSettingsEditor _editor;
        XElement _newSettingsXML;
        XElement _oldSettingsXML;
    }

    /// <summary>
    /// Change a preprocessor macro's value.
    /// </summary>
    public class ChangeMacroAction : GuiAction
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.ChangeMacroAction"/> class.
        /// </summary>
        /// <param name="doc">Document.</param>
        /// <param name="key">The name of the macro.</param>
        /// <param name="newValue">The new value of the macro.</param>
        public ChangeMacroAction(GUIDocument doc, string key, string newValue)
        {
            _document = doc;
            _key = key;
            if(doc.Settings.PreprocessorMacros.ContainsKey(key)) {
                _oldValue = doc.Settings.PreprocessorMacros[key];
            }
            _newValue = newValue;
        }

        /// <summary>
        /// Apply this instance, actually applying the Gui change it encapsulates.
        /// </summary>
        public override void Apply()
        {
            _document.Settings.PreprocessorMacros[_key] = _newValue;
        }

        /// <summary>
        /// Gets an instance having the opposite effect of <c>this</c>.
        /// </summary>
        public override GuiAction Reverse()
        {
            if(_oldValue != null) {
                return new ChangeMacroAction(_document, _key, _oldValue);
            } else {
                return new RemoveMacroAction(_document, _key);
            }
        }

        /// <summary>
        /// The object that is meant to gain the user's focus upon Apply()ing.
        /// </summary>
        /// <value>The focus.</value>
        public override Focusable Focus {
            get {
                return new FocusableMacroEditor(_document);
            }
        }

        /// <summary>
        /// Gets the description of the GUI action's effect.
        /// </summary>
        /// <value>The description.</value>
        public override string Description {
            get {
                return Configuration.GetLocalized("Change preprocessor macro's value");
            }
        }

        string _key;
        string _oldValue;
        string _newValue;
        GUIDocument _document;
    }

    /// <summary>
    /// Remove a preprocessor macro.
    /// </summary>
    public class RemoveMacroAction : GuiAction
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.RemoveMacroAction"/> class.
        /// </summary>
        /// <param name="doc">Document.</param>
        /// <param name="key">The name of the macro to remove.</param>
        public RemoveMacroAction(GUIDocument doc, string key)
        {
            _document = doc;
            _key = key;
            _oldValue = doc.Settings.PreprocessorMacros[key];
        }

        /// <summary>
        /// Apply this instance, actually applying the Gui change it encapsulates.
        /// </summary>
        public override void Apply()
        {
            _document.Settings.PreprocessorMacros.Remove(_key);
        }

        /// <summary>
        /// Gets an instance having the opposite effect of <c>this</c>.
        /// </summary>
        public override GuiAction Reverse()
        {
            return new ChangeMacroAction(_document, _key, _oldValue);
        }

        /// <summary>
        /// The object that is meant to gain the user's focus upon Apply()ing.
        /// </summary>
        /// <value>The focus.</value>
        public override Focusable Focus {
            get {
                return new FocusableMacroEditor(_document);
            }
        }

        /// <summary>
        /// Gets the description of the GUI action's effect.
        /// </summary>
        /// <value>The description.</value>
        public override string Description {
            get {
                return Configuration.GetLocalized("Remove preprocessor macro");
            }
        }

        string _key;
        string _oldValue;
        GUIDocument _document;
    }

    /// <summary>
    /// Add a new header to the document.
    /// </summary>
    public class AddHeaderAction : IssueUpdaterAction<RootPetriNet>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.AddHeaderAction"/> class.
        /// </summary>
        /// <param name="doc">Document.</param>
        /// <param name="path">The path to the new header.</param>
        public AddHeaderAction(GUIDocument doc, string path) : base(doc.RootDocument.PetriNet)
        {
            _document = doc;
            _path = path;
        }

        /// <summary>
        /// Apply this instance, actually applying the Gui change it encapsulates.
        /// </summary>
        public override void ApplyBeforeCheck()
        {
            _document.AddHeader(_path);
            _document.UpdateFunctions();
            _document.Window.EditorGui.View.ClearSelection();
        }

        /// <summary>
        /// Gets an instance having the opposite effect of <c>this</c>.
        /// </summary>
        public override GuiAction Reverse()
        {
            return new RemoveHeaderAction(_document, _path);
        }

        /// <summary>
        /// The object that is meant to gain the user's focus upon Apply()ing.
        /// </summary>
        /// <value>The focus.</value>
        public override Focusable Focus {
            get {
                return new FocusableHeadersEditor(_document);
            }
        }

        /// <summary>
        /// Gets the description of the GUI action's effect.
        /// </summary>
        /// <value>The description.</value>
        public override string Description {
            get {
                return Configuration.GetLocalized("Add a header");
            }
        }

        string _path;
        GUIDocument _document;
    }

    /// <summary>
    /// Remove a header from the document.
    /// </summary>
    public class RemoveHeaderAction : IssueUpdaterAction<RootPetriNet>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.RemoveHeaderAction"/> class.
        /// </summary>
        /// <param name="doc">Document.</param>
        /// <param name="path">The path to the document to remove from the document.</param>
        public RemoveHeaderAction(GUIDocument doc, string path) : base(doc.RootDocument.PetriNet)
        {
            _document = doc;
            _path = path;
        }

        /// <summary>
        /// Apply this instance, actually applying the Gui change it encapsulates.
        /// </summary>
        public override void ApplyBeforeCheck()
        {
            _document.RemoveHeaderNoUpdate(_path);
            _document.UpdateFunctions();
            _document.Window.EditorGui.View.ClearSelection();
        }

        /// <summary>
        /// Gets an instance having the opposite effect of <c>this</c>.
        /// </summary>
        public override GuiAction Reverse()
        {
            return new AddHeaderAction(_document, _path);
        }

        /// <summary>
        /// The object that is meant to gain the user's focus upon Apply()ing.
        /// </summary>
        /// <value>The focus.</value>
        public override Focusable Focus {
            get {
                return new FocusableHeadersEditor(_document);
            }
        }

        /// <summary>
        /// Gets the description of the GUI action's effect.
        /// </summary>
        /// <value>The description.</value>
        public override string Description {
            get {
                return Configuration.GetLocalized("Remove a header");
            }
        }

        string _path;
        GUIDocument _document;
    }
}
