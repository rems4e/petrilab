//
//  EditorGUIAction.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2014-11-13.
//

using System;
using System.Collections.Generic;

using Petri.Model;

using Action = Petri.Model.Action;

namespace Petri.Application.GUI
{
    /// <summary>
    /// Change the Entity's parent, removing it from its previous and adding it to the new.
    /// </summary>
    public class ChangeParentAction : IssueUpdaterAction<Entity>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.ChangeParentAction"/> class.
        /// </summary>
        /// <param name="e">The entity</param>
        /// <param name="parent">The new parent.</param>
        public ChangeParentAction(Entity e, PetriNet parent) : base(e)
        {
            _newParent = parent;
            _oldParent = _entity.Parent;
        }

        /// <summary>
        /// Apply this instance, actually applying the Gui change it encapsulates.
        /// </summary>
        public override void ApplyBeforeCheck()
        {
            _entity.Parent = _newParent;
            _entity.Document.RootDocument.ClearReachability();
        }

        /// <summary>
        /// Gets an instance having the opposite effect of <c>this</c>.
        /// </summary>
        public override GuiAction Reverse()
        {
            return new ChangeParentAction(_entity, _oldParent);
        }

        /// <summary>
        /// Gets the description of the GUI action's effect.
        /// </summary>
        /// <value>The description.</value>
        public override string Description {
            get {
                return Configuration.GetLocalized("Change Parent");
            }
        }

        PetriNet _newParent;
        PetriNet _oldParent;
    }

    /// <summary>
    /// Change the name of an entity.
    /// </summary>
    public class ChangeNameAction : GuiAction
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.ChangeNameAction"/> class.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="newName">The new name.</param>
        public ChangeNameAction(Entity entity, string newName)
        {
            _entity = entity;
            _newName = newName;
            _oldName = _entity.Name;
        }

        /// <summary>
        /// Apply this instance, actually applying the Gui change it encapsulates.
        /// </summary>
        public override void Apply()
        {
            _entity.Name = _newName;
        }

        /// <summary>
        /// Gets an instance having the opposite effect of <c>this</c>.
        /// </summary>
        public override GuiAction Reverse()
        {
            return new ChangeNameAction(_entity, _oldName);
        }

        /// <summary>
        /// The object that is meant to gain the user's focus upon Apply()ing.
        /// </summary>
        /// <value>The focus.</value>
        public override Focusable Focus {
            get {
                return new FocusableEntity(_entity);
            }
        }

        /// <summary>
        /// Gets the description of the GUI action's effect.
        /// </summary>
        /// <value>The description.</value>
        public override string Description {
            get {
                if(_entity is Comment) {
                    return Configuration.GetLocalized("Change Comment");
                } else {
                    return Configuration.GetLocalized("Change Name");
                }
            }
        }

        Entity _entity;
        string _newName;
        string _oldName;
    }

    /// <summary>
    /// Change the required tokens count of a state.
    /// </summary>
    public class ChangeRequiredTokensAction : IssueUpdaterAction<State>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.ChangeRequiredTokensAction"/> class.
        /// </summary>
        /// <param name="state">State.</param>
        /// <param name="newCount">The new token count.</param>
        public ChangeRequiredTokensAction(State state, UInt32 newCount) : base(state)
        {
            _newCount = newCount;
            _oldCount = state.RequiredTokens;
        }

        /// <summary>
        /// Apply this instance, actually applying the Gui change it encapsulates.
        /// </summary>
        public override void ApplyBeforeCheck()
        {
            _entity.RequiredTokens = _newCount;
        }


        /// <summary>
        /// Gets an instance having the opposite effect of <c>this</c>.
        /// </summary>
        public override GuiAction Reverse()
        {
            return new ChangeRequiredTokensAction(_entity, _oldCount);
        }

        /// <summary>
        /// Gets the description of the GUI action's effect.
        /// </summary>
        /// <value>The description.</value>
        public override string Description {
            get {
                return Configuration.GetLocalized("Change required tokens count");
            }
        }

        UInt32 _newCount;
        UInt32 _oldCount;
    }

    /// <summary>
    /// Moves an petri net entity in the view from a specified amount.
    /// </summary>
    public class MoveEntityAction : GuiAction
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.MoveEntityAction"/> class.
        /// The opposite action of this instance will not put the entity back aligned to the grid.
        /// </summary>
        /// <param name="entity">The entity to move.</param>
        /// <param name="delta">The position delta to move the entity from.</param>
        /// <param name="grid">If set to <c>true</c>, the new position will be adapted to fit on the grid.</param>
        public MoveEntityAction(Entity entity, Vector delta, bool grid) : this(entity,
                                                                          delta,
                                                                          grid,
                                                                          false)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.MoveEntityAction"/> class.
        /// </summary>
        /// <param name="entity">The entity to move.</param>
        /// <param name="delta">The position delta to move the entity from.</param>
        /// <param name="grid">If set to <c>true</c>, the new position will be adapted to fit on the grid.</param>
        /// <param name="oldGrid">Tells whether the opposite action will put the entity back on the grid or not.</param>
        MoveEntityAction(Entity entity, Vector delta, bool grid, bool oldGrid)
        {
            _oldGrid = oldGrid;
            _grid = grid;
            _entity = entity;
            _delta = new Vector(delta);

            if(_grid) {
                var pos = _entity.Position + _delta;
                pos.X = Math.Round(pos.X / Entity.GridSize) * Entity.GridSize;
                pos.Y = Math.Round(pos.Y / Entity.GridSize) * Entity.GridSize;
                _delta = pos - _entity.Position;
            }
        }

        /// <summary>
        /// Apply this instance, actually applying the Gui change it encapsulates.
        /// </summary>
        public override void Apply()
        {
            _entity.Position = _entity.Position + _delta;
        }

        /// <summary>
        /// Gets an instance having the opposite effect of <c>this</c>.
        /// </summary>
        public override GuiAction Reverse()
        {
            return new MoveEntityAction(_entity, -_delta, _oldGrid, _grid);
        }

        /// <summary>
        /// The object that is meant to gain the user's focus upon Apply()ing.
        /// </summary>
        /// <value>The focus.</value>
        public override Focusable Focus {
            get {
                return new FocusableEntity(_entity);
            }
        }

        /// <summary>
        /// Gets the description of the GUI action's effect.
        /// </summary>
        /// <value>The description.</value>
        public override string Description {
            get {
                string entity;
                if(_entity is State) {
                    entity = Configuration.GetLocalized("the state");
                } else if(_entity is Transition) {
                    entity = Configuration.GetLocalized("the transition");
                } else if(_entity is Comment) {
                    entity = Configuration.GetLocalized("the comment");
                } else {// Too lazy to search for a counter example but should probably never happen
                    entity = Configuration.GetLocalized("the entity");
                }

                return Configuration.GetLocalized("Move {0}", entity);
            }
        }

        bool _grid, _oldGrid;
        Entity _entity;
        Vector _delta;
    }

    /// <summary>
    /// Toggles if the specified state is active upon the petri net's launch or not.
    /// </summary>
    public class ToggleActiveAction : IssueUpdaterAction<State>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.ToggleActiveAction"/> class.
        /// </summary>
        /// <param name="s">The state.</param>
        public ToggleActiveAction(State s) : base(s)
        {
        }

        /// <summary>
        /// Apply this instance, actually applying the Gui change it encapsulates.
        /// </summary>
        public override void ApplyBeforeCheck()
        {
            _entity.IsStartState = !_entity.IsStartState;
            _entity.Document.RootDocument.ClearReachability();
        }

        /// <summary>
        /// Gets an instance having the opposite effect of <c>this</c>.
        /// </summary>
        public override GuiAction Reverse()
        {
            return new ToggleActiveAction(_entity);
        }

        /// <summary>
        /// Gets the description of the GUI action's effect.
        /// </summary>
        /// <value>The description.</value>
        public override string Description {
            get {
                return Configuration.GetLocalized("Change the state");
            }
        }
    }

    /// <summary>
    /// Changes the expression associated to the condition of a transition.
    /// </summary>
    public class ConditionChangeAction : IssueUpdaterAction<Transition>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.ConditionChangeAction"/> class.
        /// </summary>
        /// <param name="transition">Transition.</param>
        /// <param name="newCondition">New condition.</param>
        public ConditionChangeAction(Transition transition, Code.Expression newCondition) : base(transition)
        {
            _oldCondition = transition.Condition;
            _newCondition = newCondition;
        }

        /// <summary>
        /// Apply this instance, actually applying the Gui change it encapsulates.
        /// </summary>
        public override void ApplyBeforeCheck()
        {
            _entity.Condition = _newCondition;
        }

        /// <summary>
        /// Gets an instance having the opposite effect of <c>this</c>.
        /// </summary>
        public override GuiAction Reverse()
        {
            return new ConditionChangeAction(_entity, _oldCondition);
        }

        /// <summary>
        /// Gets the description of the GUI action's effect.
        /// </summary>
        /// <value>The description.</value>
        public override string Description {
            get {
                return Configuration.GetLocalized("Change the condition");
            }
        }

        Code.Expression _newCondition, _oldCondition;
    }

    /// <summary>
    /// Changes the expression associated to the invocation of a petri net action.
    /// </summary>
    public class InvocationChangeAction : IssueUpdaterAction<Action>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.InvocationChangeAction"/> class.
        /// </summary>
        /// <param name="action">Action.</param>
        /// <param name="invocation">The new invocation.</param>
        public InvocationChangeAction(Action action, Code.FunctionInvocation invocation) : base(action)
        {
            _oldInvocation = action.Invocation;
            _newInvocation = invocation;
        }

        /// <summary>
        /// Apply this instance, actually applying the Gui change it encapsulates.
        /// </summary>
        public override void ApplyBeforeCheck()
        {
            _entity.Invocation = _newInvocation;
        }

        /// <summary>
        /// Gets an instance having the opposite effect of <c>this</c>.
        /// </summary>
        public override GuiAction Reverse()
        {
            return new InvocationChangeAction(_entity, _oldInvocation);
        }

        /// <summary>
        /// Gets the description of the GUI action's effect.
        /// </summary>
        /// <value>The description.</value>
        public override string Description {
            get {
                return Configuration.GetLocalized("Change the function");
            }
        }

        Code.FunctionInvocation _newInvocation, _oldInvocation;
    }

    /// <summary>
    /// Changes a parameter of an invocation.
    /// </summary>
    public class ChangeInvocationArgumentAction : IssueUpdaterAction<Entity>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.ChangeInvocationArgumentAction"/> class.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="invocation">The invocation.</param>
        /// <param name="index">The argument index.</param>
        /// <param name="argument">The new value.</param>
        public ChangeInvocationArgumentAction(Entity entity,
                                              Code.FunctionInvocation invocation,
                                              int index,
                                              Code.Expression argument) : base(entity)
        {
            _invocation = invocation;
            _index = index;
            _oldValue = _invocation.Arguments[_index];
            _newValue = argument;
        }

        /// <summary>
        /// Apply this instance, actually applying the Gui change it encapsulates.
        /// </summary>
        public override void ApplyBeforeCheck()
        {
            _invocation.Arguments[_index] = _newValue;
        }

        /// <summary>
        /// Gets an instance having the opposite effect of <c>this</c>.
        /// </summary>
        public override GuiAction Reverse()
        {
            return new ChangeInvocationArgumentAction(_entity, _invocation, _index, _oldValue);
        }

        /// <summary>
        /// Gets the description of the GUI action's effect.
        /// </summary>
        /// <value>The description.</value>
        public override string Description {
            get {
                return Configuration.GetLocalized("Change argument's value");
            }
        }

        Code.FunctionInvocation _invocation;
        Code.Expression _oldValue, _newValue;
        int _index;
    }

    /// <summary>
    /// Adds a parameter to an invocation.
    /// </summary>
    public class AddInvocationArgumentAction : IssueUpdaterAction<Entity>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.AddInvocationArgumentAction"/> class.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="invocation">The invocation.</param>
        /// <param name="index">The argument index.</param>
        /// <param name="argument">The new value.</param>
        public AddInvocationArgumentAction(Entity entity,
                                           Code.FunctionInvocation invocation,
                                           int index,
                                           Code.Expression argument) : base(entity)
        {
            _invocation = invocation;
            _index = index;
            _value = argument;
        }

        /// <summary>
        /// Apply this instance, actually applying the Gui change it encapsulates.
        /// </summary>
        public override void ApplyBeforeCheck()
        {
            _invocation.Arguments.Insert(_index, _value);
        }

        /// <summary>
        /// Gets an instance having the opposite effect of <c>this</c>.
        /// </summary>
        public override GuiAction Reverse()
        {
            return new RemoveInvocationArgumentAction(_entity, _invocation, _index);
        }

        /// <summary>
        /// Gets the description of the GUI action's effect.
        /// </summary>
        /// <value>The description.</value>
        public override string Description {
            get {
                return Configuration.GetLocalized("Add an argument");
            }
        }

        Code.FunctionInvocation _invocation;
        Code.Expression _value;
        int _index;
    }

    /// <summary>
    /// Removes a parameter associated with an invocation.
    /// </summary>
    public class RemoveInvocationArgumentAction : IssueUpdaterAction<Entity>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.AddInvocationArgumentAction"/> class.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="invocation">The invocation.</param>
        /// <param name="index">The argument index.</param>
        public RemoveInvocationArgumentAction(Entity entity,
                                              Code.FunctionInvocation invocation,
                                              int index) : base(entity)
        {
            _invocation = invocation;
            _index = index;
            _value = _invocation.Arguments[index];
        }

        /// <summary>
        /// Apply this instance, actually applying the Gui change it encapsulates.
        /// </summary>
        public override void ApplyBeforeCheck()
        {
            _invocation.Arguments.RemoveAt(_index);
        }

        /// <summary>
        /// Gets an instance having the opposite effect of <c>this</c>.
        /// </summary>
        public override GuiAction Reverse()
        {
            return new AddInvocationArgumentAction(_entity, _invocation, _index, _value);
        }

        /// <summary>
        /// Gets the description of the GUI action's effect.
        /// </summary>
        /// <value>The description.</value>
        public override string Description {
            get {
                return Configuration.GetLocalized("Remove the argument");
            }
        }

        Code.FunctionInvocation _invocation;
        Code.Expression _value;
        int _index;
    }

    /// <summary>
    /// Changes the timeout associated to a petri net action.
    /// </summary>
    public class ChangeTimeoutAction : IssueUpdaterAction<Action>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.ChangeTimeoutAction"/> class.
        /// </summary>
        /// <param name="action">Action.</param>
        /// <param name="timeout">The new timeout.</param>
        public ChangeTimeoutAction(Action action, uint timeout) : base(action)
        {
            _oldTimeout = action.TimeoutMS;
            _newTimeout = timeout;
        }

        /// <summary>
        /// Apply this instance, actually applying the Gui change it encapsulates.
        /// </summary>
        public override void ApplyBeforeCheck()
        {
            _entity.TimeoutMS = _newTimeout;
        }

        /// <summary>
        /// Gets an instance having the opposite effect of <c>this</c>.
        /// </summary>
        public override GuiAction Reverse()
        {
            return new ChangeTimeoutAction(_entity, _oldTimeout);
        }

        /// <summary>
        /// Gets the description of the GUI action's effect.
        /// </summary>
        /// <value>The description.</value>
        public override string Description {
            get {
                return Configuration.GetLocalized("Change the timeout");
            }
        }

        uint _newTimeout, _oldTimeout;
    }

    /// <summary>
    /// Add an entity action to the petri net.
    /// </summary>
    public class AddCommentAction : IssueUpdaterAction<Comment>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.AddCommentAction"/> class.
        /// </summary>
        /// <param name="entity">Entity.</param>
        public AddCommentAction(Comment entity) : base(entity)
        {
        }

        /// <summary>
        /// Apply this instance, actually applying the Gui change it encapsulates.
        /// </summary>
        public override void ApplyBeforeCheck()
        {
            _entity.Parent.AddComment(_entity);
        }

        /// <summary>
        /// Gets an instance having the opposite effect of <c>this</c>.
        /// </summary>
        public override GuiAction Reverse()
        {
            return new RemoveCommentAction(_entity);
        }

        /// <summary>
        /// Gets the description of the GUI action's effect.
        /// </summary>
        /// <value>The description.</value>
        public override string Description {
            get {
                return Configuration.GetLocalized("Add a comment");
            }
        }
    }

    /// <summary>
    /// Removes a comment from the petri net.
    /// </summary>
    public class RemoveCommentAction : IssueUpdaterAction<Comment>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.RemoveCommentAction"/> class.
        /// </summary>
        /// <param name="entity">Entity.</param>
        public RemoveCommentAction(Comment entity) : base(entity, true)
        {
        }

        /// <summary>
        /// Apply this instance, actually applying the Gui change it encapsulates.
        /// </summary>
        public override void ApplyBeforeCheck()
        {
            _entity.Parent.RemoveComment(_entity);
        }

        /// <summary>
        /// Gets an instance having the opposite effect of <c>this</c>.
        /// </summary>
        public override GuiAction Reverse()
        {
            return new AddCommentAction(_entity);
        }

        /// <summary>
        /// The object that is meant to gain the user's focus upon Apply()ing.
        /// </summary>
        /// <value>The focus.</value>
        public override Focusable Focus {
            get {
                return new FocusableEntity(_entity, true);
            }
        }

        /// <summary>
        /// Gets the description of the GUI action's effect.
        /// </summary>
        /// <value>The description.</value>
        public override string Description {
            get {
                return Configuration.GetLocalized("Remove the comment");
            }
        }
    }

    /// <summary>
    /// Changes a petri net comment's background color.
    /// </summary>
    public class ChangeCommentColorAction : GuiAction
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.ChangeCommentColorAction"/> class.
        /// </summary>
        /// <param name="comment">The comment.</param>
        /// <param name="color">The new color.</param>
        public ChangeCommentColorAction(Comment comment, Cairo.Color color)
        {
            _comment = comment;
            _oldColor = new Cairo.Color(comment.Color.R,
                                        comment.Color.G,
                                        comment.Color.B,
                                        comment.Color.A);
            _newColor = color;
        }

        /// <summary>
        /// Apply this instance, actually applying the Gui change it encapsulates.
        /// </summary>
        public override void Apply()
        {
            _comment.Color = new Cairo.Color(_newColor.R, _newColor.G, _newColor.B, _newColor.A);
        }

        /// <summary>
        /// Gets an instance having the opposite effect of <c>this</c>.
        /// </summary>
        public override GuiAction Reverse()
        {
            return new ChangeCommentColorAction(_comment, _oldColor);
        }

        /// <summary>
        /// The object that is meant to gain the user's focus upon Apply()ing.
        /// </summary>
        /// <value>The focus.</value>
        public override Focusable Focus {
            get {
                return new FocusableEntity(_comment);
            }
        }

        /// <summary>
        /// Gets the description of the GUI action's effect.
        /// </summary>
        /// <value>The description.</value>
        public override string Description {
            get {
                return Configuration.GetLocalized("Change the comment's color");
            }
        }

        Comment _comment;
        Cairo.Color _oldColor, _newColor;
    }

    /// <summary>
    /// Resize a petri net comment.
    /// </summary>
    public class ResizeCommentAction : GuiAction
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.ResizeCommentAction"/> class.
        /// </summary>
        /// <param name="comment">The comment.</param>
        /// <param name="size">The new size.</param>
        public ResizeCommentAction(Comment comment, Vector size)
        {
            _comment = comment;
            _oldSize = new Vector(comment.Size);
            _newSize = size;
        }

        /// <summary>
        /// Apply this instance, actually applying the Gui change it encapsulates.
        /// </summary>
        public override void Apply()
        {
            _comment.Size = new Vector(_newSize);
        }

        /// <summary>
        /// Gets an instance having the opposite effect of <c>this</c>.
        /// </summary>
        public override GuiAction Reverse()
        {
            return new ResizeCommentAction(_comment, _oldSize);
        }

        /// <summary>
        /// The object that is meant to gain the user's focus upon Apply()ing.
        /// </summary>
        /// <value>The focus.</value>
        public override Focusable Focus {
            get {
                return new FocusableEntity(_comment);
            }
        }

        /// <summary>
        /// Gets the description of the GUI action's effect.
        /// </summary>
        /// <value>The description.</value>
        public override string Description {
            get {
                return Configuration.GetLocalized("Resize the comment");
            }
        }

        Comment _comment;
        Vector _oldSize, _newSize;
    }

    /// <summary>
    /// Adds a transition to the petri net.
    /// </summary>
    public class AddTransitionAction : IssueUpdaterAction<Transition>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.AddTransitionAction"/> class.
        /// </summary>
        /// <param name="t">The transition.</param>
        /// <param name="incrementTokenCount">If set to <c>true</c> increment token count.</param>
        public AddTransitionAction(Transition t, bool incrementTokenCount) : base(t)
        {
            _incrementTokenCount = incrementTokenCount;
        }

        /// <summary>
        /// Apply this instance, actually applying the Gui change it encapsulates.
        /// </summary>
        public override void ApplyBeforeCheck()
        {
            _entity.Before.Parent.AddTransition(_entity);

            _entity.Before.AddTransitionAfter(_entity);
            _entity.After.AddTransitionBefore(_entity);
            if(_incrementTokenCount) {
                ++_entity.After.RequiredTokens;
            }
            _entity.Document.RootDocument.ClearReachability();
        }

        /// <summary>
        /// Gets an instance having the opposite effect of <c>this</c>.
        /// </summary>
        public override GuiAction Reverse()
        {
            return new RemoveTransitionAction(_entity, _incrementTokenCount);
        }

        /// <summary>
        /// Gets the description of the GUI action's effect.
        /// </summary>
        /// <value>The description.</value>
        public override string Description {
            get {
                return Configuration.GetLocalized("Add a transition");
            }
        }

        bool _incrementTokenCount;
    }

    /// <summary>
    /// Detach one of the transition's end and attach it to a new state (possibly the same).
    /// </summary>
    public class ChangeTransitionEndAction : GuiAction
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.ChangeTransitionEndAction"/> class.
        /// It sets <c>true</c> to the last but one parameter of the other constructor of this class iff:
        /// <list type="bullet">
        /// <item><description><paramref name="destination"/> is set to <c>true</c></description></item>
        /// <item><description><c>transition.After.RequiredTokens == transition.After.TransitionsBefore.Count</c></description></item>
        /// </list>
        /// It sets <c>true</c> to the last parameter of the other constructor of this class iff:
        /// <list type="bullet">
        /// <item><description><paramref name="destination"/> is set to <c>true</c></description></item>
        /// <item><description><c>newEnd.TransitionsBefore.Count == 0</c> or <c>newEnd.TransitionsBefore == {transition}</c></description></item>
        /// <item><description><c>newEnd.RequiredTokens == newEnd.Transitions.Count</c></description></item>
        /// </list>
        /// </summary>
        /// <param name="transition">The transition.</param>
        /// <param name="newEnd">The new state at which the transition's end will be attached</param>
        /// <param name="destination">If set to <c>true</c>, the transition's end which is moved is the destination end (pointed towards by the arrow). Otherwise, it is the start state of the transition.</param>
        public ChangeTransitionEndAction(Transition transition,
                                         State newEnd,
                                         bool destination) : this(transition,
                                                                  newEnd,
                                                                  destination,
                                                                  destination
                                                                      && transition.After.RequiredTokens >= transition.After.TransitionsBefore.Count,
                                                                  destination
                                                                      && newEnd.TransitionsBefore.Count == newEnd.RequiredTokens
                                                                      && (newEnd.TransitionsBefore.Count == 0
                                                                          || newEnd.TransitionsBefore.Count == 1 && newEnd.TransitionsBefore.Contains(transition)))
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.ChangeTransitionEndAction"/> class.
        /// </summary>
        /// <param name="transition">The transition.</param>
        /// <param name="newEnd">The new state at which the transition's end will be attached</param>
        /// <param name="destination">If set to <c>true</c>, the transition's end which is moved is the destination end (pointed towards by the arrow). Otherwise, it is the start state of the transition.</param>
        /// <param name="decrementOld">Whether the transitions' detached end sees its required token count decremented. Only effective is <c>destination</c> is set to true.</param>
        /// <param name="incrementNew">Whether the transitions' newly attached end sees its required token count incremented. Only effective is <c>destination</c> is set to true.</param>
        ChangeTransitionEndAction(Transition transition,
                                  State newEnd,
                                  bool destination,
                                  bool decrementOld,
                                  bool incrementNew)
        {
            _transition = transition;
            _newEnd = newEnd;
            _destination = destination;
            _decrementOld = decrementOld;
            _incrementNew = incrementNew;
            if(destination) {
                _oldEnd = _transition.After;
            } else {
                _oldEnd = _transition.Before;
            }
        }

        /// <summary>
        /// Apply this instance, actually applying the Gui change it encapsulates.
        /// </summary>
        public override void Apply()
        {
            if(_destination) {
                _transition.After.RemoveTransitionBefore(_transition);
                if(_decrementOld) {
                    --_transition.After.RequiredTokens;
                }
                _transition.After = _newEnd;
                _newEnd.AddTransitionBefore(_transition);
            } else {
                _transition.Before.RemoveTransitionAfter(_transition);
                if(_decrementOld) {
                    --_transition.Before.RequiredTokens;
                }
                _transition.Before = _newEnd;
                _newEnd.AddTransitionAfter(_transition);
            }

            if(_destination && _incrementNew) {
                ++_newEnd.RequiredTokens;
            }
            _transition.Document.RootDocument.ClearReachability();
        }

        /// <summary>
        /// Gets an instance having the opposite effect of <c>this</c>.
        /// </summary>
        public override GuiAction Reverse()
        {
            return new ChangeTransitionEndAction(_transition,
                                                 _oldEnd,
                                                 _destination,
                                                 _incrementNew,
                                                 _decrementOld);
        }

        /// <summary>
        /// The object that is meant to gain the user's focus upon Apply()ing.
        /// </summary>
        /// <value>The focus.</value>
        public override Focusable Focus {
            get {
                return new FocusableEntity(_transition);
            }
        }

        /// <summary>
        /// Gets the description of the GUI action's effect.
        /// </summary>
        /// <value>The description.</value>
        public override string Description {
            get {
                return Configuration.GetLocalized("Change the transition's end");
            }
        }

        Transition _transition;
        State _newEnd, _oldEnd;
        bool _destination, _decrementOld, _incrementNew;
    }

    /// <summary>
    /// Removes a transition from its petri net.
    /// </summary>
    public class RemoveTransitionAction : IssueUpdaterAction<Transition>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.RemoveTransitionAction"/> class.
        /// </summary>
        /// <param name="transtition">Transtition.</param>
        /// <param name="decrementTokenCount">If set to <c>true</c> then decrement token count of the destination.</param>
        public RemoveTransitionAction(Transition transtition,
                                      bool decrementTokenCount) : base(transtition, true)
        {
            _decrementTokenCount = decrementTokenCount;
        }

        /// <summary>
        /// Apply this instance, actually applying the Gui change it encapsulates.
        /// </summary>
        public override void ApplyBeforeCheck()
        {
            if(_decrementTokenCount) {
                --_entity.After.RequiredTokens;
            }
            _entity.Before.Parent.RemoveTransition(_entity);
            _entity.Document.RootDocument.ClearReachability();
        }

        /// <summary>
        /// Gets an instance having the opposite effect of <c>this</c>.
        /// </summary>
        public override GuiAction Reverse()
        {
            return new AddTransitionAction(_entity, _decrementTokenCount);
        }

        /// <summary>
        /// The object that is meant to gain the user's focus upon Apply()ing.
        /// </summary>
        /// <value>The focus.</value>
        public override Focusable Focus {
            get {
                return new FocusableEntity(_entity, true);
            }
        }

        /// <summary>
        /// Gets the description of the GUI action's effect.
        /// </summary>
        /// <value>The description.</value>
        public override string Description {
            get {
                return Configuration.GetLocalized("Remove the transition");
            }
        }

        bool _decrementTokenCount;
    }

    /// <summary>
    /// Adds a state to its petri net.
    /// </summary>
    public class AddStateAction : IssueUpdaterAction<State>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.AddStateAction"/> class.
        /// </summary>
        /// <param name="s">S.</param>
        public AddStateAction(State s) : base(s)
        {
        }

        /// <summary>
        /// Apply this instance, actually applying the Gui change it encapsulates.
        /// </summary>
        public override void ApplyBeforeCheck()
        {
            _entity.Parent.AddState(_entity);

            if(_entity is PetriNet) {
                var allEntities = ((PetriNet)_entity).BuildLocalEntitiesList();
                foreach(var entity in allEntities) {
                    if(entity is ExternalInnerPetriNet) {
                        _entity.Document.FirstLevelExternalPetriNets.Add((ExternalInnerPetriNet)entity);
                    }
                }
            }
        }

        /// <summary>
        /// Gets an instance having the opposite effect of <c>this</c>.
        /// </summary>
        public override GuiAction Reverse()
        {
            return new RemoveStateAction(_entity);
        }

        /// <summary>
        /// Gets the description of the GUI action's effect.
        /// </summary>
        /// <value>The description.</value>
        public override string Description {
            get {
                return Configuration.GetLocalized("Add a state");
            }
        }
    }

    /// <summary>
    /// Removes a state from its petri net.
    /// </summary>
    public class RemoveStateAction : IssueUpdaterAction<State>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.RemoveStateAction"/> class.
        /// </summary>
        /// <param name="s">S.</param>
        public RemoveStateAction(State s) : base(s, true)
        {
        }

        /// <summary>
        /// Apply this instance, actually applying the Gui change it encapsulates.
        /// </summary>
        public override void ApplyBeforeCheck()
        {
            if(_entity is PetriNet) {
                var allEntities = ((PetriNet)_entity).BuildLocalEntitiesList();
                foreach(var entity in allEntities) {
                    if(entity is ExternalInnerPetriNet) {
                        _entity.Document.FirstLevelExternalPetriNets.Remove((ExternalInnerPetriNet)entity);
                    }
                }
            }

            _entity.Parent.RemoveState(_entity);
            _entity.Document.RootDocument.ClearReachability();
        }

        /// <summary>
        /// Gets an instance having the opposite effect of <c>this</c>.
        /// </summary>
        public override GuiAction Reverse()
        {
            return new AddStateAction(_entity);
        }

        /// <summary>
        /// The object that is meant to gain the user's focus upon Apply()ing.
        /// </summary>
        /// <value>The focus.</value>
        public override Focusable Focus {
            get {
                return new FocusableEntity(_entity, true);
            }
        }

        /// <summary>
        /// Gets the description of the GUI action's effect.
        /// </summary>
        /// <value>The description.</value>
        public override string Description {
            get {
                return Configuration.GetLocalized("Remove the state");
            }
        }
    }

    /// <summary>
    /// Change the source document of an imported petri net.
    /// </summary>
    public class RepointReferenceAction : IssueUpdaterAction<ExternalInnerPetriNet>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.RepointReferenceAction"/> class.
        /// </summary>
        /// <param name="pn">The petri net.</param>
        /// <param name="newPath">The new path.</param>
        public RepointReferenceAction(ExternalInnerPetriNet pn, string newPath) : base(pn)
        {
            _newPath = newPath;
            _oldPath = pn.ExternalDocument.Path;
        }

        /// <summary>
        /// Apply this instance, actually applying the Gui change it encapsulates.
        /// </summary>
        public override void ApplyBeforeCheck()
        {
            _entity.ExternalPetriSource.RepointReference(_newPath);
            _entity.Document.RootDocument.ClearReachability();
        }

        /// <summary>
        /// Gets an instance having the opposite effect of <c>this</c>.
        /// </summary>
        public override GuiAction Reverse()
        {
            return new RepointReferenceAction(_entity, _oldPath);
        }

        /// <summary>
        /// Gets the description of the GUI action's effect.
        /// </summary>
        /// <value>The description.</value>
        public override string Description {
            get {
                return Configuration.GetLocalized("Repoint reference");
            }
        }

        string _newPath;
        string _oldPath;
    }

    /// <summary>
    /// Toggle between relatively- and absolutely-referenced petri nets.
    /// </summary>
    public class ToggleRelativeReferenceAction : GuiAction
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.ToggleRelativeReferenceAction"/> class.
        /// </summary>
        /// <param name="pn">The petri net.</param>
        public ToggleRelativeReferenceAction(ExternalInnerPetriNet pn)
        {
            _pn = pn;
        }

        /// <summary>
        /// Apply this instance, actually applying the Gui change it encapsulates.
        /// </summary>
        public override void Apply()
        {
            _pn.ExternalPetriSource.RelativePathReference = !_pn.ExternalPetriSource.RelativePathReference;
        }

        /// <summary>
        /// Gets an instance having the opposite effect of <c>this</c>.
        /// </summary>
        public override GuiAction Reverse()
        {
            return new ToggleRelativeReferenceAction(_pn);
        }

        /// <summary>
        /// The object that is meant to gain the user's focus upon Apply()ing.
        /// </summary>
        /// <value>The focus.</value>
        public override Focusable Focus {
            get {
                return new FocusableEntity(_pn);
            }
        }

        /// <summary>
        /// Gets the description of the GUI action's effect.
        /// </summary>
        /// <value>The description.</value>
        public override string Description {
            get {
                return Configuration.GetLocalized("Toggle relative reference");
            }
        }

        ExternalInnerPetriNet _pn;
    }

    /// <summary>
    /// Acts on a key-value pair.
    /// </summary>
    public abstract class KVPAction<KeyType, ValueType> : IssueUpdaterAction<Entity>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Application.GUI.KVPAction`2"/> class.
        /// </summary>
        /// <param name="dictionary">The dictionary containing the key-value pairs</param>
        /// <param name="entity">The related entity.</param>
        /// <param name="key">The argument name.</param>
        /// <param name="description">The description of the action.</param>
        protected KVPAction(IDictionary<KeyType, ValueType> dictionary,
                                   Entity entity,
                                   KeyType key,
                                   string description) : base(entity)
        {
            _dictionary = dictionary;
            _key = key;
            _description = description;
        }

        /// <summary>
        /// Gets the description of the GUI action's effect.
        /// </summary>
        /// <value>The description.</value>
        public override string Description {
            get {
                return _description;
            }
        }

        /// <summary>
        /// The dictionary to operate on.
        /// </summary>
        protected IDictionary<KeyType, ValueType> _dictionary;

        /// <summary>
        /// The key.
        /// </summary>
        protected KeyType _key;

        /// <summary>
        /// The description of the GUI action.
        /// </summary>
        protected string _description;
    }

    /// <summary>
    /// Change the name of the key-value pair.
    /// </summary>
    public class ChangeKVPNameAction<KeyType, ValueType> : KVPAction<KeyType, ValueType>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Application.GUI.ChangeKVPNameAction`2"/> class.
        /// </summary>
        /// <param name="dictionary">The dictionary containing the key-value pairs</param>
        /// <param name="entity">The related entity.</param>
        /// <param name="key">The argument name.</param>
        /// <param name="newName">The argument's new name.</param>
        /// <param name="description">The description of the action.</param>
        /// <param name="action">An additional action to perform when the key is renamed.</param>
        public ChangeKVPNameAction(IDictionary<KeyType, ValueType> dictionary,
                                   Entity entity,
                                   KeyType key,
                                   KeyType newName,
                                   string description,
                                   Action<KeyType, KeyType> action) : base(dictionary, entity, key, description)
        {
            _newName = newName;
            _action = action;
        }

        /// <summary>
        /// Apply this instance, actually applying the Gui change it encapsulates.
        /// </summary>
        public override void ApplyBeforeCheck()
        {
            var oldValue = _dictionary[_key];
            _dictionary.Remove(_key);
            _dictionary[_newName] = oldValue;
            _action(_key, _newName);
        }

        /// <summary>
        /// Gets an instance having the opposite effect of <c>this</c>.
        /// </summary>
        public override GuiAction Reverse()
        {
            return new ChangeKVPNameAction<KeyType, ValueType>(_dictionary, _entity, _newName, _key, _description, _action);
        }

        KeyType _newName;
        Action<KeyType, KeyType> _action;
    }

    /// <summary>
    /// Change the value of the key-value pair.
    /// </summary>
    public class ChangeKVPValueAction<KeyType, ValueType> : KVPAction<KeyType, ValueType>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Application.GUI.ChangeKVPValueAction`2"/> class.
        /// </summary>
        /// <param name="dictionary">The dictionary containing the key-value pairs</param>
        /// <param name="entity">The related entity.</param>
        /// <param name="key">The argument name.</param>
        /// <param name="newValue">The argument's new value.</param>
        /// <param name="description">The description of the action.</param>
        public ChangeKVPValueAction(IDictionary<KeyType, ValueType> dictionary,
                                   Entity entity,
                                   KeyType key,
                                   ValueType newValue,
                                   string description) : base(dictionary, entity, key, description)
        {
            _newValue = newValue;
            _oldValue = _dictionary[_key];
        }

        /// <summary>
        /// Apply this instance, actually applying the Gui change it encapsulates.
        /// </summary>
        public override void ApplyBeforeCheck()
        {
            _dictionary[_key] = _newValue;
        }

        /// <summary>
        /// Gets an instance having the opposite effect of <c>this</c>.
        /// </summary>
        public override GuiAction Reverse()
        {
            return new ChangeKVPValueAction<KeyType, ValueType>(_dictionary, _entity, _key, _oldValue, _description);
        }

        ValueType _newValue;
        ValueType _oldValue;
    }

    /// <summary>
    /// Adds key-value pair.
    /// </summary>
    public class AddKVPAction<KeyType, ValueType> : KVPAction<KeyType, ValueType>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Application.GUI.AddKVPAction`2"/> class.
        /// </summary>
        /// <param name="dictionary">The dictionary containing the key-value pairs</param>
        /// <param name="entity">The related entity.</param>
        /// <param name="key">The argument name.</param>
        /// <param name="value">The argument's new value.</param>
        /// <param name="description">The description of the action.</param>
        /// <param name="addAction">An additional action to perform when the key is added.</param>
        /// <param name="removeAction">An additional action to perform when the key is removed.</param>
        public AddKVPAction(IDictionary<KeyType, ValueType> dictionary,
                            Entity entity,
                            KeyType key,
                            ValueType value,
                            string description,
                            Action<KeyType> addAction,
                            Action<KeyType> removeAction) : base(dictionary, entity, key, description)
        {
            _value = value;
            _addAction = addAction;
            _removeAction = removeAction;
        }

        /// <summary>
        /// Apply this instance, actually applying the Gui change it encapsulates.
        /// </summary>
        public override void ApplyBeforeCheck()
        {
            _dictionary[_key] = _value;
            _addAction(_key);
        }

        /// <summary>
        /// Gets an instance having the opposite effect of <c>this</c>.
        /// </summary>
        public override GuiAction Reverse()
        {
            return new RemoveKVPAction<KeyType, ValueType>(_dictionary, _entity, _key, _description, _addAction, _removeAction);
        }

        ValueType _value;
        Action<KeyType> _addAction;
        Action<KeyType> _removeAction;
    }

    /// <summary>
    /// Removes a key-value pair.
    /// </summary>
    public class RemoveKVPAction<KeyType, ValueType> : KVPAction<KeyType, ValueType>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Application.GUI.AddKVPAction`2"/> class.
        /// </summary>
        /// <param name="dictionary">The dictionary containing the key-value pairs</param>
        /// <param name="entity">The related entity.</param>
        /// <param name="key">The argument name.</param>
        /// <param name="description">The description of the action.</param>
        /// <param name="addAction">An additional action to perform when the key is added.</param>
        /// <param name="removeAction">An additional action to perform when the key is removed.</param>
        public RemoveKVPAction(IDictionary<KeyType, ValueType> dictionary,
                               Entity entity,
                               KeyType key,
                               string description,
                               Action<KeyType> addAction,
                               Action<KeyType> removeAction) : base(dictionary, entity, key, description)
        {
            _oldValue = _dictionary[_key];
            _addAction = addAction;
            _removeAction = removeAction;
        }

        /// <summary>
        /// Apply this instance, actually applying the Gui change it encapsulates.
        /// </summary>
        public override void ApplyBeforeCheck()
        {
            _dictionary.Remove(_key);
            _removeAction(_key);
        }

        /// <summary>
        /// Gets an instance having the opposite effect of <c>this</c>.
        /// </summary>
        public override GuiAction Reverse()
        {
            return new AddKVPAction<KeyType, ValueType>(_dictionary, _entity, _key, _oldValue, _description, _addAction, _removeAction);
        }

        ValueType _oldValue;
        Action<KeyType> _addAction;
        Action<KeyType> _removeAction;
    }

    /// <summary>
    /// Add a parameter.
    /// </summary>
    public class AddParameterAction : GuiAction
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.AddParameterAction"/> class.
        /// </summary>
        /// <param name="pn">The petri net.</param>
        /// <param name="var">The variable to set as a parameter</param>
        public AddParameterAction(RootPetriNet pn, Code.VariableExpression var)
        {
            _pn = pn;
            _var = var;
        }

        /// <summary>
        /// Apply this instance, actually applying the Gui change it encapsulates.
        /// </summary>
        public override void Apply()
        {
            _pn.Parameters.Add(_var);
        }

        /// <summary>
        /// Gets an instance having the opposite effect of <c>this</c>.
        /// </summary>
        public override GuiAction Reverse()
        {
            return new RemoveParameterAction(_pn, _var);
        }

        /// <summary>
        /// The object that is meant to gain the user's focus upon Apply()ing.
        /// </summary>
        /// <value>The focus.</value>
        public override Focusable Focus {
            get {
                return new FocusableEntity(_pn);
            }
        }

        /// <summary>
        /// Gets the description of the GUI action's effect.
        /// </summary>
        /// <value>The description.</value>
        public override string Description {
            get {
                return Configuration.GetLocalized("Add a parameter");
            }
        }

        RootPetriNet _pn;
        Code.VariableExpression _var;
    }

    /// <summary>
    /// Remove a parameter.
    /// </summary>
    public class RemoveParameterAction : GuiAction
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.RemoveParameterAction"/> class.
        /// </summary>
        /// <param name="pn">The petri net.</param>
        /// <param name="var">The variable to remove from the petri net's parameters</param>
        public RemoveParameterAction(RootPetriNet pn, Code.VariableExpression var)
        {
            _pn = pn;
            _var = var;
        }

        /// <summary>
        /// Apply this instance, actually applying the Gui change it encapsulates.
        /// </summary>
        public override void Apply()
        {
            _pn.Parameters.Remove(_var);
        }

        /// <summary>
        /// Gets an instance having the opposite effect of <c>this</c>.
        /// </summary>
        public override GuiAction Reverse()
        {
            return new AddParameterAction(_pn, _var);
        }

        /// <summary>
        /// The object that is meant to gain the user's focus upon Apply()ing.
        /// </summary>
        /// <value>The focus.</value>
        public override Focusable Focus {
            get {
                return new FocusableEntity(_pn);
            }
        }

        /// <summary>
        /// Gets the description of the GUI action's effect.
        /// </summary>
        /// <value>The description.</value>
        public override string Description {
            get {
                return Configuration.GetLocalized("Remove the parameter");
            }
        }

        RootPetriNet _pn;
        Code.VariableExpression _var;
    }
}
