//
//  UndoManager.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2014-11-13.
//

using System;
using System.Collections.Generic;

using Petri.Model;

namespace Petri.Application.GUI
{
    /// <summary>
    /// The undo manager of the application, one instance per document.
    /// </summary>
    public class UndoManager
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.UndoManager"/> class.
        /// </summary>
        public UndoManager()
        {
            _undoStack = new Stack<ActionDescription>();
            _redoStack = new Stack<ActionDescription>();
        }

        /// <summary>
        /// Arguments for the <see cref="ActionApplied"/> event.
        /// </summary>
        [Serializable]
        public class ActionAppliedEventArgs : EventArgs
        {
            /// <summary>
            /// Initializes a new instance of the
            /// <see cref="T:Petri.Application.GUI.UndoManager.ActionAppliedEventArgs"/> class.
            /// </summary>
            /// <param name="action">Action.</param>
            /// <param name="isReplay">Whether the action is replayed.</param>
            public ActionAppliedEventArgs(GuiAction action, bool isReplay)
            {
                Action = action;
                IsReplay = isReplay;
            }

            /// <summary>
            /// Gets the GUI action that has been applied.
            /// </summary>
            /// <value>The action.</value>
            public GuiAction Action {
                get;
                private set;
            }

            /// <summary>
            /// Gets a value indicating whether the action is replayed.
            /// </summary>
            /// <value><c>false</c> if is first time the action is applied; otherwise, <c>true</c>
            /// (when it is applied because of an undo or redo operation).</value>
            public bool IsReplay {
                get;
                private set;
            }
        }

        /// <summary>
        /// The delegate mapped to the <see cref="ActionApplied"/> event.
        /// </summary>
        public delegate void ActionAppliedDel(UndoManager manager, ActionAppliedEventArgs e);

        /// <summary>
        /// Occurs when a GUI action is applied.
        /// </summary>
        public event ActionAppliedDel ActionApplied;

        /// <summary>
        /// Triggers the <see cref="ActionApplied"/> event.
        /// </summary>
        /// <param name="e">E.</param>
        void OnActionApplied(ActionAppliedEventArgs e)
        {
            ActionApplied?.Invoke(this, e);
        }

        /// <summary>
        /// Registers the GUI action into the undo manager's stacks, and commit the action so that its effect are visible.
        /// </summary>
        /// <param name="action">Action.</param>
        /// <param name="doNotFocus">If <c>true</c>, then no focus is given to the action.</param>
        public void CommitGuiAction(GuiAction action, bool doNotFocus = false)
        {
            _redoStack.Clear();
            _undoStack.Push(new ActionDescription(action, action.Description));
            action.Apply();
            OnActionApplied(new ActionAppliedEventArgs(action, false));

            if(!doNotFocus) {
                action.Focus.Focus();
            }
        }

        /// <summary>
        /// Undoes the last committed GUI action and pushes it onto the redo stack.
        /// </summary>
        /// <returns>The GUI action that has just been reverted.</returns>
        public GuiAction Undo()
        {
            return SwapAndApply(_undoStack, _redoStack);
        }

        /// <summary>
        /// Undoes the last undone GUI action and pushes it onto the undo stack.
        /// </summary>
        /// <returns>The GUI action that has just been restored.</returns>
        public GuiAction Redo()
        {
            return SwapAndApply(_redoStack, _undoStack);
        }

        /// <summary>
        /// Clear the undo/redo stacks, meaning that no action can be undone/redone after this call.
        /// </summary>
        public void Clear()
        {
            _undoStack.Clear();
            _redoStack.Clear();
        }

        /// <summary>
        /// Gets the GUI action on top of the undo stack.
        /// </summary>
        /// <value>The next undo action, or <c>null</c> if the stack is empty.</value>
        public GuiAction NextUndo {
            get {
                if(_undoStack.Count > 0) {
                    return _undoStack.Peek()._action;
                } else {
                    return null;
                }
            }
        }

        /// <summary>
        /// Gets the description of the GUI action on top of the undo stack.
        /// Do not call when the stack is empty, or be prepared to get a <c>NullReferenceException</c>.
        /// </summary>
        /// <value>The description of the next undo action, or <c>null</c> if the stack is empty.</value>
        public string NextUndoDescription {
            get {
                return _undoStack.Peek()._description;
            }
        }

        /// <summary>
        /// Gets the GUI action on top of the redo stack.
        /// </summary>
        /// <value>The next redo action, or <c>null</c> if the stack is empty.</value>
        public GuiAction NextRedo {
            get {
                if(_redoStack.Count > 0)
                    return _redoStack.Peek()._action;
                else
                    return null;
            }
        }

        /// <summary>
        /// Gets the description of the GUI action on top of the red stack.
        /// Do not call when the stack is empty, or be prepared to get a <c>NullReferenceException</c>.
        /// </summary>
        /// <value>The description of the next redo action, or <c>null</c> if the stack is empty.</value>
        public string NextRedoDescription {
            get {
                return _redoStack.Peek()._description;
            }
        }

        struct ActionDescription
        {
            public ActionDescription(GuiAction a, string d)
            {
                _action = a;
                _description = d;
            }

            public GuiAction _action;

            /// <summary>
            /// The description of the action. When we undo an action, we reverse the effect of the action,
            /// but we have to keep the original description string, hence having this field.
            /// </summary>
            public string _description;
        }

        /// <summary>
        /// Removes the top Gui action from <paramref name="toPop"/>, pushes its opposite to <paramref name="toPush"/> and commits this opposite action.
        /// </summary>
        /// <param name="toPop">To stack to pop from.</param>
        /// <param name="toPush">To stack to push to.</param>
        /// <returns>The GUI action that has just been applied.</returns>
        GuiAction SwapAndApply(Stack<ActionDescription> toPop, Stack<ActionDescription> toPush)
        {
            if(toPop.Count > 0) {
                var actionDescription = toPop.Pop();
                actionDescription._action = actionDescription._action.Reverse();
                toPush.Push(new ActionDescription(actionDescription._action,
                                                  actionDescription._description));
                actionDescription._action.Apply();
                OnActionApplied(new ActionAppliedEventArgs(actionDescription._action, true));
                actionDescription._action.Focus.Focus();

                return actionDescription._action;
            }

            return null;
        }

        Stack<ActionDescription> _undoStack;
        Stack<ActionDescription> _redoStack;
    }

    /// <summary>
    /// GUI action.
    /// </summary>
    public abstract class GuiAction
    {
        /// <summary>
        /// Apply this instance, actually applying the Gui change it encapsulates.
        /// </summary>
        public abstract void Apply();

        /// <summary>
        /// Gets the opposite instance of <c>this</c>. The opposite of moving and entity 10px to the right is moving it 10px to the left.
        /// </summary>
        public abstract GuiAction Reverse();

        /// <summary>
        /// The object that is meant to gain the user's focus upon Apply()ing.
        /// </summary>
        /// <value>The focus.</value>
        public abstract Focusable Focus {
            get;
        }

        /// <summary>
        /// Gets the description of the GUI action's effect.
        /// </summary>
        /// <value>The description.</value>
        public abstract string Description {
            get;
        }
    }

    /// <summary>
    /// A simple wrapper that allows to change the description of a GUI action but keep its effect.
    /// </summary>
    public class GuiActionWrapper : GuiAction
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.GuiActionWrapper"/> class.
        /// </summary>
        /// <param name="action">The action to wrap.</param>
        /// <param name="description">The description of the wrapper.</param>
        public GuiActionWrapper(GuiAction action, string description)
        {
            _action = action;
            _description = description;
        }

        /// <summary>
        /// Apply this instance, actually applying the Gui change it encapsulates.
        /// </summary>
        public override void Apply()
        {
            _action.Apply();
        }

        /// <summary>
        /// Gets an instance having the opposite effect of <c>this</c>.
        /// </summary>
        public override GuiAction Reverse()
        {
            return new GuiActionWrapper(_action.Reverse(), _description);
        }

        /// <summary>
        /// The object that is meant to gain the user's focus upon Apply()ing.
        /// </summary>
        /// <value>The focus.</value>
        public override Focusable Focus {
            get {
                return _action.Focus;
            }
        }

        /// <summary>
        /// Gets the description of the GUI action's effect.
        /// </summary>
        /// <value>The description.</value>
        public override string Description {
            get {
                return _description;
            }
        }

        GuiAction _action;
        string _description;
    }

    /// <summary>
    /// A GUI action that wraps around a list of GUI actions, by applying then in the order they are given, and undoing them in the reverse order they are given.
    /// </summary>
    public class GuiActionList : GuiAction
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.GuiActionList"/> class.
        /// </summary>
        /// <param name="actions">The actions list, considered in the enumeration's natural order. Must not be empty.</param>
        /// <param name="description">The description for the wrapper action.</param>
        public GuiActionList(IEnumerable<GuiAction> actions, string description)
        {
            _actions = new List<GuiAction>(actions);
            _description = description;

            // Strange use case anyway
            if(_actions.Count == 0) {
                throw new ArgumentException(Configuration.GetLocalized("The action list is empty!"));
            }
        }

        /// <summary>
        /// Apply this instance, actually applying the Gui change it encapsulates.
        /// </summary>
        public override void Apply()
        {
            foreach(var a in _actions) {
                a.Apply();
            }
        }

        /// <summary>
        /// The opposite action correctly reverses the order each GUI action's opposite has to be applied.
        /// </summary>
        public override GuiAction Reverse()
        {
            var l = new List<GuiAction>();
            foreach(var a in _actions) {
                l.Insert(0, a.Reverse());
            }

            return new GuiActionList(l, _description);
        }

        /// <summary>
        /// The object that is meant to gain the user's focus upon Apply()ing.
        /// Here, the object is a list of the inner action's Focus objects.
        /// When an inner object's Focus returns a List&lt;object&gt;, then the list is flattened into the return value.
        /// </summary>
        /// <value>The focus.</value>
        public override Focusable Focus {
            get {
                var l = new List<Focusable>();
                foreach(var a in _actions) {
                    var f = a.Focus;
                    if(f is IEnumerable<Focusable>) {
                        l.AddRange(f as IEnumerable<Focusable>);
                    } else {
                        l.Add(f);
                    }
                }

                return new FocusableList(l);
            }
        }

        /// <summary>
        /// Gets the description of the GUI action's effect.
        /// </summary>
        /// <value>The description.</value>
        public override string Description {
            get {
                return _description;
            }
        }

        List<GuiAction> _actions;
        string _description;
    }

    /// <summary>
    /// A dumb GUI action that only remembers an object as the return value of the Focus getter.
    /// </summary>
    public class DoNothingAction : GuiAction
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.DoNothingAction"/> class.
        /// </summary>
        /// <param name="focus">Focus.</param>
        public DoNothingAction(Focusable focus)
        {
            _focus = focus;
        }

        /// <summary>
        /// Apply this instance, actually applying the Gui change it encapsulates.
        /// </summary>
        public override void Apply()
        {
        }

        /// <summary>
        /// Gets an instance having the opposite effect of <c>this</c>.
        /// </summary>
        public override GuiAction Reverse()
        {
            return new DoNothingAction(_focus);
        }

        /// <summary>
        /// The object that is meant to gain the user's focus upon Apply()ing.
        /// </summary>
        /// <value>The focus.</value>
        public override Focusable Focus {
            get {
                return _focus;
            }
        }

        /// <summary>
        /// Gets the description of the GUI action's effect.
        /// </summary>
        /// <value>The description.</value>
        public override string Description {
            get {
                return "";
            }
        }

        Focusable _focus;
    }

    /// <summary>
    /// A GuiAction that updates the issues of a particular entity.
    /// </summary>
    public abstract class IssueUpdaterAction<EntityType> : GuiAction where EntityType : Entity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Application.GUI.IssueUpdaterAction`1"/> class.
        /// </summary>
        /// <param name="entity">Entity.</param>
        /// <param name="remove">If set to <c>true</c>, recursively remove all issues from the entity.</param>
        protected IssueUpdaterAction(EntityType entity, bool remove = false)
        {
            _entity = entity;
            _remove = remove;
        }

        /// <summary>
        /// Apply this instance, actually applying the Gui change it encapsulates.
        /// </summary>
        public sealed override void Apply()
        {
            ApplyBeforeCheck();
            if(_remove) {
                var root = _entity.Document.RootDocument;
                root.RemoveIssue(_entity);
                if(_entity is PetriNet) {
                    var list = ((PetriNet)(Entity)_entity).BuildAllEntitiesList();
                    foreach(var e in list) {
                        root.RemoveIssue(e);
                    }
                }
            } else {
                IssuesChecker.CheckIssues(_entity);
            }
        }

        /// <summary>
        /// The object that is meant to gain the user's focus upon Apply()ing.
        /// </summary>
        /// <value>The focus.</value>
        public override Focusable Focus {
            get {
                return new FocusableEntity(_entity);
            }
        }

        /// <summary>
        /// Apply this instance, actually applying the Gui change it encapsulates.
        /// </summary>
        public abstract void ApplyBeforeCheck();

        /// <summary>
        /// The entity.
        /// </summary>
        protected readonly EntityType _entity;
        readonly bool _remove;
    }
}
