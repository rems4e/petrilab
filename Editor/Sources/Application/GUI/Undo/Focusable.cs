//
//  Focusable.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-02-20.
//

using System.Collections.Generic;
using System.Linq;

using Petri.Model;

namespace Petri.Application.GUI
{
    /// <summary>
    /// An object that can gain the users focus.
    /// </summary>
    public abstract class Focusable
    {
        /// <summary>
        /// Gets the user's focus.
        /// </summary>
        public abstract void Focus();
    }

    /// <summary>
    /// A list of IFocusable. If all of the IFocusable instances are FocusableEntity, then the corresponding Entity instances are set as the document's selection.
    /// Else, they are all focused, one after the other.
    /// </summary>
    public class FocusableList : Focusable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.FocusableList"/> class.
        /// </summary>
        /// <param name="focus">Focus.</param>
        public FocusableList(List<Focusable> focus)
        {
            _focus = new HashSet<Focusable>(focus);
        }

        /// <summary>
        /// Gets the user's focus.
        /// </summary>
        public override void Focus()
        {
            bool isEntitiesList = true;
            foreach(Focusable f in _focus) {
                if(!(f is FocusableEntity)) {
                    isEntitiesList = false;
                    break;
                }
            }
            if(isEntitiesList && _focus.Count > 1) {
                GUIDocument doc = null;
                Entity entity = null;
                foreach(var f in _focus) {
                    doc = (GUIDocument)((FocusableEntity)f).Entity.Document;
                    entity = ((FocusableEntity)f).Entity;
                    break;
                }
                doc.Window.EditorGui.View.GoToParent(entity);
                doc.Window.EditorGui.View.SetSelection(_focus.Where(e => !((FocusableEntity)e).IsRemove).Select(e => ((FocusableEntity)e).Entity));
            } else {
                foreach(Focusable f in _focus) {
                    f.Focus();
                }
            }
        }

        HashSet<Focusable> _focus;
    }

    /// <summary>
    /// Focus on an Entity instance of a petri net
    /// </summary>
    public class FocusableEntity : Focusable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.FocusableEntity"/> class.
        /// </summary>
        /// <param name="focus">Focus.</param>
        /// <param name="remove"><c>true</c> if the focus is on an entity that is being deleted.</param>
        public FocusableEntity(Entity focus, bool remove = false)
        {
            Entity = focus;
            IsRemove = remove;
        }

        /// <summary>
        /// Gets the user's focus.
        /// </summary>
        public override void Focus()
        {
            var doc = ((GUIDocument)Entity.Document);
            if(IsRemove) {
                doc.Window.EditorGui.View.GoToParent(Entity);
                doc.Window.EditorGui.View.ClearSelection();
            } else {
                doc.Window.EditorGui.View.SetSelection(Entity);
            }
        }

        /// <summary>
        /// Gets the entity to focus on.
        /// </summary>
        /// <value>The entity.</value>
        public Entity Entity {
            get;
            private set;
        }

        /// <summary>
        /// Gets a value indicating whether we are removing the entity.
        /// </summary>
        /// <value><c>true</c> if removing; otherwise, <c>false</c>.</value>
        public bool IsRemove {
            get;
            private set;
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object"/> is equal to the current <see cref="Petri.Application.GUI.FocusableEntity"/>.
        /// </summary>
        /// <param name="o">The <see cref="System.Object"/> to compare with the current <see cref="Petri.Application.GUI.FocusableEntity"/>.</param>
        /// <returns><c>true</c> if the specified <see cref="System.Object"/> is equal to the current
        /// <see cref="Petri.Application.GUI.FocusableEntity"/>; otherwise, <c>false</c>.</returns>
        public override bool Equals(object o)
        {
            return o is FocusableEntity && ((FocusableEntity)o).Entity == Entity;
        }

        /// <summary>
        /// Serves as a hash function for a <see cref="Petri.Application.GUI.FocusableEntity"/> object.
        /// </summary>
        /// <returns>A hash code for this instance that is suitable for use in hashing algorithms and data structures such as a
        /// hash table.</returns>
        public override int GetHashCode()
        {
            return Entity.GetHashCode();
        }
    }

    /// <summary>
    /// Focus on the document's settings
    /// </summary>
    public class FocusableSettings : Focusable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.FocusableSettings"/> class.
        /// </summary>
        /// <param name="widget">The widget to focus on.</param>
        /// <param name="page">The page to focus on.</param>
        public FocusableSettings(Gtk.Widget widget, DocumentSettingsEditor.EditorPage? page)
        {
            _widget = widget;
            _page = page;
        }

        /// <summary>
        /// Gets the user's focus.
        /// </summary>
        public override void Focus()
        {
            var editor = ((DocumentSettingsEditor)_widget.Toplevel);
            editor.ShowPage(_page);
            if(_widget != null) {
                editor.Focus = _widget;
            }
        }

        Gtk.Widget _widget;
        DocumentSettingsEditor.EditorPage? _page;
    }

    /// <summary>
    /// Focus on the document's macros editor
    /// </summary>
    public class FocusableMacroEditor : Focusable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.FocusableMacroEditor"/> class.
        /// </summary>
        /// <param name="doc">Document.</param>
        public FocusableMacroEditor(GUIDocument doc)
        {
            _document = doc;
        }

        /// <summary>
        /// Gets the user's focus.
        /// </summary>
        public override void Focus()
        {
            _document.ManageMacros();
        }

        GUIDocument _document;
    }

    /// <summary>
    /// Focus on the document's headers editor
    /// </summary>
    public class FocusableHeadersEditor : Focusable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.FocusableHeadersEditor"/> class.
        /// </summary>
        /// <param name="doc">Document.</param>
        public FocusableHeadersEditor(GUIDocument doc)
        {
            _document = doc;
        }

        /// <summary>
        /// Gets the user's focus.
        /// </summary>
        public override void Focus()
        {
            _document.ManageHeaders();
        }

        GUIDocument _document;
    }
}

