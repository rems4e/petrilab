//
//  AboutPanel.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2015-01-20.
//

using System.IO;
using System.Reflection;

using Gtk;

namespace Petri.Application.GUI
{
    /// <summary>
    /// A window that allows the user to customize some application global settings.
    /// </summary>
    public class AboutPanel : MenuBarWindow
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.SettingsEditor"/> class.
        /// </summary>
        public AboutPanel() : base(true)
        {
            DefaultWidth = 260;
            DefaultHeight = 300;

            KeyPressEvent += (object o, KeyPressEventArgs args) => {
                if(args.Event.Key == Gdk.Key.Escape) {
                    TryClose();
                }
            };

            SetPosition(WindowPosition.Center);
            int x, y;
            GetPosition(out x, out y);
            Move(x, 2 * y / 3);
            BorderWidth = 15;

            var buf = Gdk.Pixbuf.LoadFromResource("splash");
            buf = buf.ScaleSimple(buf.Width / 4, buf.Height / 4, Gdk.InterpType.Bilinear);
            var splash = new Image(buf);

            var label = GUIApplication.LabelFromMarkup("<span foreground=\"#505050\" size=\"xx-large\">"
                                                       + "PetriLab"
                                                       + "</span>", true);
            var version = GUIApplication.LabelFromMarkup("<span foreground=\"#999999\" size=\"large\">"
                                                         + GUIApplication.SafeMarkupFromLocalized("Version {0}", Application.Version)
                                                         + "</span>", true);

            var copyright = GUIApplication.LabelFromMarkup("<span foreground=\"#999999\" size=\"small\">"
                                                           + GUIApplication.SafeMarkupFromString("Copyright © 2014-2018 Sigilence Technologies")
                                                           + "</span>", true);

            var website = GUIApplication.LabelFromMarkup(GUIApplication.SafeMarkupFromString(Application.WebSiteInfo), true);
            website.Selectable = true;

            var licences = GUIApplication.LabelFromString(Configuration.GetLocalized("This software uses open-source libraries. Their licences are reproduced below."));
            var licencesScroll = new ScrolledWindow();
            licencesScroll.SetPolicy(PolicyType.Automatic, PolicyType.Automatic);

            var licencesBuffer = new TextBuffer(new TextTagTable());
            var textView = new TextView(licencesBuffer);
            textView.WrapMode = WrapMode.WordChar;
            textView.Editable = false;
            var viewport = new Viewport();
            viewport.Add(textView);
            licencesScroll.Add(viewport);

            var titleTag = new TextTag("title");
            titleTag.Scale = 1.5;
            licencesBuffer.TagTable.Add(titleTag);

            var siteTag = new TextTag("site");
            siteTag.Foreground = "#666666";
            siteTag.Underline = Pango.Underline.Single;
            licencesBuffer.TagTable.Add(siteTag);

            var iter = licencesBuffer.EndIter;
            licencesBuffer.InsertWithTags(ref iter, "Json.NET", titleTag);
            licencesBuffer.InsertWithTags(ref iter, "\n");
            licencesBuffer.InsertWithTags(ref iter, "https://www.newtonsoft.com/json", siteTag);
            licencesBuffer.InsertWithTags(ref iter, "\n\n");
            licencesBuffer.InsertWithTags(ref iter, GetLicence("json.net.txt"));

            licencesBuffer.InsertWithTags(ref iter, "\n\n");

            iter = licencesBuffer.EndIter;
            licencesBuffer.InsertWithTags(ref iter, "JsonCpp", titleTag);
            licencesBuffer.InsertWithTags(ref iter, "\n");
            licencesBuffer.InsertWithTags(ref iter, "https://github.com/open-source-parsers/jsoncpp", siteTag);
            licencesBuffer.InsertWithTags(ref iter, "\n\n");
            licencesBuffer.InsertWithTags(ref iter, GetLicence("jsoncpp.txt"));

            licencesBuffer.InsertWithTags(ref iter, "\n\n");

            iter = licencesBuffer.EndIter;
            licencesBuffer.InsertWithTags(ref iter, "Boost", titleTag);
            licencesBuffer.InsertWithTags(ref iter, "\n");
            licencesBuffer.InsertWithTags(ref iter, "http://www.boost.org/", siteTag);
            licencesBuffer.InsertWithTags(ref iter, "\n\n");
            licencesBuffer.InsertWithTags(ref iter, GetLicence("boost.txt"));

            licencesBuffer.InsertWithTags(ref iter, "\n\n");

            iter = licencesBuffer.EndIter;
            licencesBuffer.InsertWithTags(ref iter, "MonoDevelop", titleTag);
            licencesBuffer.InsertWithTags(ref iter, "\n");
            licencesBuffer.InsertWithTags(ref iter, "https://github.com/mono/monodevelop/tree/master/main/src/addins/MacPlatform/MacInterop", siteTag);
            licencesBuffer.InsertWithTags(ref iter, "\n\n");
            licencesBuffer.InsertWithTags(ref iter, GetLicence("novell.txt"));

            licences.WidthRequest = 450;
            licencesScroll.WidthRequest = 450;
            licencesScroll.HeightRequest = 200;

            _vbox.PackStart(splash, false, false, 0);
            _vbox.PackStart(label, false, false, 5);
            _vbox.PackStart(version, false, false, 10);
            _vbox.PackStart(website, false, false, 0);
            _vbox.PackStart(copyright, false, false, 5);
            _vbox.PackStart(new HSeparator(), false, false, 5);
            _vbox.PackStart(licences, false, false, 5);
            _vbox.PackStart(licencesScroll, false, false, 5);
        }

        /// <summary>
        /// Show the settings editor.
        /// </summary>
        public override void ShowWindow()
        {
            Title = Configuration.GetLocalized("About PetriLab");
            base.ShowWindow();
        }

        /// <summary>
        /// Gets the licence.
        /// </summary>
        /// <returns>The licence.</returns>
        /// <param name="name">Name.</param>
        string GetLicence(string name)
        {
            var assembly = Assembly.GetExecutingAssembly();
            Stream objStream = assembly.GetManifestResourceStream("licence_" + name);
            return new StreamReader(objStream).ReadToEnd();
        }
    }
}
