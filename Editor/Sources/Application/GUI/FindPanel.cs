//
//  FindPanel.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2017-12-05.
//

using System;

using Gtk;

namespace Petri.Application.GUI
{
    /// <summary>
    /// A panel with a form to find entities.
    /// </summary>
    public class FindPanel : DocumentHelperWindow
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.FindPanel"/> class.
        /// </summary>
        /// <param name="doc">Document.</param>
        public FindPanel(GUIDocument doc) : base(doc, 400, 100, true)
        {
            FindWhich = Controller.FindWhich.State | Controller.FindWhich.Comment | Controller.FindWhich.Transition;
            FindWhere = Controller.FindWhere.Name | Controller.FindWhere.Code;

            var hbox = new HBox();
            var label = GUIApplication.LabelFromString(Configuration.GetLocalized("Find among the entities of kind:"));
            hbox.PackStart(label, false, false, 0);
            _vbox.PackStart(hbox, false, false, 0);

            var combo = ComboBox.NewText();
            combo.AppendText(Configuration.GetLocalized("All entities"));
            combo.AppendText(Configuration.GetLocalized("States"));
            combo.AppendText(Configuration.GetLocalized("Transitions"));
            combo.AppendText(Configuration.GetLocalized("Comments"));
            combo.Active = 0;

            var regex = new CheckButton(Configuration.GetLocalized("Regular expression"));
            regex.Active = false;
            var searchInID = new CheckButton(Configuration.GetLocalized("Search in entities' id"));
            searchInID.Active = false;
            var searchInName = new CheckButton(Configuration.GetLocalized("Search in entities' name"));
            searchInName.Active = true;
            var searchInCode = new CheckButton(Configuration.GetLocalized("Search in entities' code"));
            searchInCode.Active = true;

            hbox = new HBox();
            hbox.PackStart(combo, false, false, 0);
            _vbox.PackStart(hbox, false, false, 0);

            _what = new Entry();
            _what.Activated += OnFind;

            _vbox.PackStart(_what, true, true, 0);
            _vbox.PackStart(regex, false, false, 0);
            _vbox.PackStart(searchInID, false, false, 0);
            _vbox.PackStart(searchInName, false, false, 0);
            _vbox.PackStart(searchInCode, false, false, 0);

            hbox = new HBox(false, 5);
            var cancel = new Button(GUIApplication.LabelFromString(Configuration.GetLocalized("Cancel"), true));
            var find = new Button(GUIApplication.LabelFromString(Configuration.GetLocalized("Find"), true));
            cancel.Clicked += (sender, e) => {
                Hide();
            };
            find.Clicked += OnFind;
            find.Sensitive = false;

            hbox.PackStart(cancel, false, false, 0);
            hbox.PackStart(find, false, false, 0);
            _vbox.PackStart(hbox, false, false, 0);

            combo.Changed += (object sender, EventArgs e) => {
                if(combo.Active == 0) {
                    FindWhich = Controller.FindWhich.State | Controller.FindWhich.Comment | Controller.FindWhich.Transition;
                    searchInCode.Sensitive = true;
                } else if(combo.Active == 1) {
                    FindWhich = Controller.FindWhich.State;
                    searchInCode.Sensitive = true;
                } else if(combo.Active == 2) {
                    FindWhich = Controller.FindWhich.Transition;
                    searchInCode.Sensitive = true;
                } else if(combo.Active == 3) {
                    FindWhich = Controller.FindWhich.Comment;
                    searchInName.Active = true;
                    searchInCode.Sensitive = false;
                    FindWhere |= Controller.FindWhere.Name;
                    find.Sensitive = FindWhere != 0 && _what.Text != "";
                }
            };
            regex.Toggled += (sender, e) => {
                Regex = regex.Active;
            };
            searchInID.Toggled += (sender, e) => {
                FindWhere ^= Controller.FindWhere.ID;
                find.Sensitive = FindWhere != 0 && _what.Text != "";
            };
            searchInName.Toggled += (sender, e) => {
                FindWhere ^= Controller.FindWhere.Name;
                find.Sensitive = _what.Text != "" && (FindWhich != Controller.FindWhich.Comment && FindWhere != 0
                                                      || FindWhere.HasFlag(Controller.FindWhere.Name)
                                                      || FindWhere.HasFlag(Controller.FindWhere.ID));
            };
            searchInCode.Toggled += (sender, e) => {
                FindWhere ^= Controller.FindWhere.Code;
                find.Sensitive = FindWhere != 0 && _what.Text != "";
            };
            _what.Changed += (sender, e) => {
                find.Sensitive = FindWhere != 0 && _what.Text != "";
            };
        }

        /// <summary>
        /// Gets the string that should be used as the window's title.
        /// </summary>
        /// <value>The current title.</value>
        public override string CurrentTitle {
            get {
                return Configuration.GetLocalized("Find in the document {0}", Document.Settings.Name);
            }
        }

        /// <summary>
        /// Presents the find panel to the user.
        /// </summary>
        public override void ShowWindow()
        {
            _what.GrabFocus();
            base.ShowWindow();
        }

        /// <summary>
        /// Asks the editor GUI to find the requested text.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">E.</param>
        protected void OnFind(object sender, EventArgs e)
        {
            Document.Window.PerformFind(_what.Text, FindWhich, FindWhere, Regex);
            Hide();
        }

        /// <summary>
        /// Gets or sets the kind of search that is performed.
        /// </summary>
        /// <value>The type.</value>
        Controller.FindWhich FindWhich {
            get;
            set;
        }

        Controller.FindWhere FindWhere {
            get;
            set;
        }

        bool Regex {
            get;
            set;
        }

        Entry _what;
    }
}
