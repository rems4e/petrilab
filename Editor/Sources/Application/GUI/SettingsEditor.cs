//
//  SettingsEditor.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2015-01-20.
//

using System;
using System.Collections.Generic;

using Gtk;

namespace Petri.Application.GUI
{
    /// <summary>
    /// A window that allows the user to customize some application global settings.
    /// </summary>
    public class SettingsEditor : MenuBarWindow
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.SettingsEditor"/> class.
        /// </summary>
        public SettingsEditor()
        {
            DefaultWidth = 200;
            DefaultHeight = 100;

            KeyPressEvent += (object o, KeyPressEventArgs args) => {
                if(args.Event.Key == Gdk.Key.Escape) {
                    TryClose();
                }
            };

            SetPosition(WindowPosition.Center);
            int x, y;
            GetPosition(out x, out y);
            Move(x, 2 * y / 3);
            BorderWidth = 15;

            // Default language
            {
                var combo = ComboBox.NewText();

                foreach(Code.Language l in Enum.GetValues(typeof(Code.Language))) {
                    combo.AppendText(DocumentSettings.LanguageName(l));
                }

                TreeIter iter;
                combo.Model.GetIterFirst(out iter);
                do {
                    if((combo.Model.GetValue(iter, 0) as string).Equals(DocumentSettings.LanguageName(Configuration.DefaultLanguage))) {
                        combo.SetActiveIter(iter);
                        break;
                    }
                } while(combo.Model.IterNext(ref iter));

                combo.Changed += (object sender, EventArgs e) => {
                    TreeIter it;

                    if(combo.GetActiveIter(out it)) {
                        Configuration.DefaultLanguage = (Code.Language)int.Parse(combo.Model.GetStringFromIter(it));
                        UpdateNew();
                    }
                };

                var hbox = new HBox(false, 5);
                hbox.PackStart(GUIApplication.LabelFromString(Configuration.GetLocalized("Default language:")),
                               false,
                               false,
                               5);
                _vbox.PackStart(hbox, false, false, 5);
                _vbox.PackStart(combo, false, false, 0);
            }

            // Recent documents count
            {
                var combo = ComboBox.NewText();

                var values = new SortedSet<int>();
                values.Add(Configuration.MaxRecentDocuments);
                values.Add(0);
                values.Add(3);
                values.Add(5);
                values.Add(10);
                values.Add(15);
                values.Add(20);

                foreach(int val in values) {
                    combo.AppendText(val.ToString());
                }

                TreeIter iter;
                combo.Model.GetIterFirst(out iter);
                do {
                    var currVal = int.Parse((string)combo.Model.GetValue(iter, 0));
                    if(currVal == Configuration.MaxRecentDocuments) {
                        combo.SetActiveIter(iter);
                        break;
                    }
                } while(combo.Model.IterNext(ref iter));

                combo.Changed += (object sender, EventArgs e) => {
                    TreeIter it;

                    if(combo.GetActiveIter(out it)) {
                        Configuration.MaxRecentDocuments = int.Parse((string)combo.Model.GetValue(it, 0));
                    }
                };

                var hbox = new HBox(false, 5);
                hbox.PackStart(GUIApplication.LabelFromString(Configuration.GetLocalized("Max recent documents:")),
                               false,
                               false,
                               5);
                _vbox.PackStart(hbox, false, false, 5);
                _vbox.PackStart(combo, false, false, 0);
            }

            // Recent actions count
            {
                var combo = ComboBox.NewText();

                var values = new SortedSet<int>();
                values.Add(Configuration.EntityEditorMaxRecentActions);
                values.Add(0);
                values.Add(1);
                values.Add(2);
                values.Add(3);
                values.Add(4);
                values.Add(5);
                values.Add(10);

                foreach(int val in values) {
                    combo.AppendText(val.ToString());
                }

                TreeIter iter;
                combo.Model.GetIterFirst(out iter);
                do {
                    var currVal = int.Parse((string)combo.Model.GetValue(iter, 0));
                    if(currVal == Configuration.EntityEditorMaxRecentActions) {
                        combo.SetActiveIter(iter);
                        break;
                    }
                } while(combo.Model.IterNext(ref iter));

                combo.Changed += (object sender, EventArgs e) => {
                    TreeIter it;

                    if(combo.GetActiveIter(out it)) {
                        Configuration.EntityEditorMaxRecentActions = int.Parse((string)combo.Model.GetValue(it, 0));
                    }
                };

                var hbox = new HBox(false, 5);
                hbox.PackStart(GUIApplication.LabelFromString(Configuration.GetLocalized("Max recently used function invocations in the editor:")),
                               false,
                               false,
                               5);
                _vbox.PackStart(hbox, false, false, 5);
                _vbox.PackStart(combo, false, false, 0);
            }

            // Keep the views in sync
            /*{
                CheckButton sync = new CheckButton(Configuration.GetLocalized("Keep the editor and debugger's views in sync"));
                sync.Active = Configuration.KeepEditorAndDebugViewsInSync;
                sync.Toggled += (object sender, EventArgs e) => {
                    Configuration.KeepEditorAndDebugViewsInSync = sync.Active;
                };
                _vbox.PackStart(sync, false, false, 0);
            }*/

            // Prefer relative paths
            {
                CheckButton relative = new CheckButton(Configuration.GetLocalized("Prefer relative paths for referenced files"));
                relative.Active = Configuration.PreferRelativePaths;
                relative.Toggled += (object sender, EventArgs e) => {
                    Configuration.PreferRelativePaths = relative.Active;
                };
                _vbox.PackStart(relative, false, false, 0);
            }

            // Transitions handles size
            {
                var button = new SpinButton(1, 12, 1);
                button.Value = Configuration.TransitionHandleRadius;
                button.Changed += (object sender, EventArgs e) => {
                    Configuration.TransitionHandleRadius = (int)button.Value;
                };

                var hbox = new HBox(false, 5);
                hbox.PackStart(GUIApplication.LabelFromString(Configuration.GetLocalized("Radius in pixels of the transitions' handles:")),
                               false,
                               false,
                               5);
                _vbox.PackStart(hbox, false, false, 5);

                hbox = new HBox(false, 5);
                hbox.PackStart(button, false, false, 5);
                _vbox.PackStart(hbox, false, false, 0);
            }
        }

        /// <summary>
        /// Show the settings editor.
        /// </summary>
        public override void ShowWindow()
        {
            Title = Configuration.GetLocalized("Preferences");
            base.ShowWindow();
        }
    }
}

