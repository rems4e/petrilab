//
//  MacrosManager.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2015-01-20.
//

using System;

using Gtk;

namespace Petri.Application.GUI
{
    /// <summary>
    /// A window that allows the user to add, change or remove macros to the document.
    /// </summary>
    public class MacrosManager : DocumentHelperWindow
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.MacrosManager"/> class.
        /// </summary>
        /// <param name="doc">Document.</param>
        public MacrosManager(GUIDocument doc) : base(doc, 400, 300)
        {
            var scrolledWindow = new ScrolledWindow();
            scrolledWindow.SetPolicy(PolicyType.Automatic, PolicyType.Automatic);
            _vbox.PackStart(scrolledWindow, true, true, 0);

            _table = new TreeView();
            scrolledWindow.Add(_table);

            {
                var c = new TreeViewColumn();
                c.Title = Configuration.GetLocalized("Name");
                var nameCell = new CellRendererText();
                c.PackStart(nameCell, true);
                c.AddAttribute(nameCell, "text", 0);
                _table.AppendColumn(c);
            }

            {
                var c = new TreeViewColumn();
                c.Title = Configuration.GetLocalized("Value");
                var valueCell = new CellRendererText();
                valueCell.Editable = !GUIApplication.IsReadOnly;
                valueCell.Edited += (object o, EditedArgs args) => {
                    TreeIter iter;
                    _dataStore.GetIterFromString(out iter, args.Path);
                    CommitGuiAction(new ChangeMacroAction(Document,
                                                          _dataStore.GetValue(iter, 0) as string,
                                                          args.NewText));
                };
                c.PackStart(valueCell, true);
                c.AddAttribute(valueCell, "text", 1);
                _table.AppendColumn(c);
            }

            _dataStore = new ListStore(typeof(string), typeof(string));
            _table.Model = _dataStore;

            var hbox = new HBox(false, 5);
            var plus = new Button(GUIApplication.LabelFromString("+", true));
            var minus = new Button(GUIApplication.LabelFromString("-", true));
            plus.Clicked += OnAdd;
            minus.Clicked += OnRemove;
            hbox.PackStart(plus, false, false, 0);
            hbox.PackStart(minus, false, false, 0);
            if(!GUIApplication.IsReadOnly) {
                _vbox.PackStart(hbox, false, false, 0);
            }
        }

        /// <summary>
        /// Gets the string that should be used as the window's title.
        /// </summary>
        /// <value>The current title.</value>
        public override string CurrentTitle {
            get {
                return Configuration.GetLocalized("Macros of {0}", Document.Settings.Name);
            }
        }

        /// <summary>
        /// Builds the list of macros.
        /// </summary>
        public override void UpdateUI()
        {
            base.UpdateUI();
            _dataStore.Clear();
            foreach(var m in Document.Settings.PreprocessorMacros) {
                _dataStore.AppendValues(m.Key, m.Value);
            }
        }

        /// <summary>
        /// Calles when a macro is to be removed.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">E.</param>
        protected void OnRemove(object sender, EventArgs e)
        {
            TreeIter iter;
            TreePath[] treePath = _table.Selection.GetSelectedRows();

            for(int i = treePath.Length; i > 0; i--) {
                _dataStore.GetIter(out iter, treePath[(i - 1)]);
                var result = PromptYesNo(
                    Configuration.GetLocalized("Removing a macro used in the document will make it inconsistent. Be careful!"),
                    Configuration.GetLocalized("Remove")
                ).Result;

                if(result) {
                    var key = _dataStore.GetValue(iter, 0) as string;
                    CommitGuiAction(new RemoveMacroAction(Document, key));
                }
            }
        }

        /// <summary>
        /// Called when a macro is to be added.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">E.</param>
        void OnAdd(object sender, EventArgs e)
        {
            var d = new MessageDialog(this,
                                      DialogFlags.Modal,
                                      MessageType.Question,
                                      ButtonsType.None,
                                      Configuration.GetLocalized("Please write down the macro name (cannot be changed later):"));
            d.AddButton(Configuration.GetLocalized("Cancel"), ResponseType.Cancel);
            d.AddButton(Configuration.GetLocalized("Add the macro"), ResponseType.Accept);
            Entry entry = new Entry(Configuration.GetLocalized("Name"));
            d.VBox.PackEnd(entry, true, true, 0);
            d.ShowAll();
            if(GUIApplication.RunModalDialog(d) == ResponseType.Accept) {
                CommitGuiAction(new ChangeMacroAction(Document,
                                                      entry.Text,
                                                      Configuration.GetLocalized("Value")));
            }
            d.Destroy();
        }

        TreeView _table;
        ListStore _dataStore;
    }
}

