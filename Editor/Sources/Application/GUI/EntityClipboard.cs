//
//  EntityClipboard.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2014-12-09.
//

using System.Collections.Generic;

using Petri.Model;

namespace Petri.Application.GUI
{
    /// <summary>
    /// The clipboard that manages petri net entities.
    /// </summary>
    public static class EntityClipboard
    {
        /// <summary>
        /// Gets or sets the application's clipboard's content.
        /// </summary>
        /// <value>The clipboard.</value>
        public static HashSet<Entity> Clipboard {
            get;
            set;
        }

        /// <summary>
        /// The number of times a group of entity which have been copied have been pasted. This is intended to suffix new the entities with an increasing number.
        /// </summary>
        /// <value>The paste count.</value>
        public static int PasteCount {
            get;
            set;
        }

        static EntityClipboard()
        {
            Clipboard = new HashSet<Entity>();
        }
    }
}
