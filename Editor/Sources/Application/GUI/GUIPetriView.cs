//
//  GUIPetriView.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2014-11-13.
//

using System;
using System.Collections.Generic;

using Cairo;
using Gtk;

using Petri.Model;

using Point = Petri.Model.Point;

namespace Petri.Application.GUI
{
    /// <summary>
    /// A specialized petri view that handles input events.
    /// </summary>
    public abstract class GUIPetriView : PetriView
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.GUIPetriView"/> class.
        /// </summary>
        /// <param name="provider">The petri net provider.</param>
        /// <param name="window">The document's window.</param>
        protected GUIPetriView(PetriNetProvider provider, MainWindow window) : base(provider)
        {
            _window = window;

            DrawingArea = new DrawingArea();
            DrawingArea.ButtonPressEvent += OnButtonPressEvent;
            DrawingArea.ButtonReleaseEvent += OnButtonReleaseEvent;
            DrawingArea.MotionNotifyEvent += OnMotionNotifyEvent;
            DrawingArea.ExposeEvent += OnExposeEvent;

            DrawingArea.CanFocus = true;
            DrawingArea.CanDefault = true;
            DrawingArea.AddEvents((int)(Gdk.EventMask.ButtonPressMask
                | Gdk.EventMask.ButtonReleaseMask
                | Gdk.EventMask.KeyPressMask
                | Gdk.EventMask.PointerMotionMask));

            _deltaClick = new Point();
            _originalPosition = new Point();
            _lastClickDate = DateTime.Now;
            _lastClickPosition = new Point();

            _editPixBuf = DrawingArea.RenderIcon(Stock.Edit, IconSize.LargeToolbar, null);

            window.FocusInEvent += (o, args) => {
                OnFocusChanged(new FocusChangedEventArgs(true));
            };
            window.FocusOutEvent += (o, args) => {
                OnFocusChanged(new FocusChangedEventArgs(false));
            };

            FocusChanged += (GUIPetriView view, FocusChangedEventArgs e) => {
                Redraw();
                if(e.IsFocusIn) {
                    _lastFocus = DateTime.Now;
                }
            };
            SelectionUpdated += e => {
                Redraw();
            };

        }

        /// <summary>
        /// Clears the selection.
        /// </summary>
        public virtual void ClearSelection()
        {
            _selection.Clear();
            OnSelectionUpdated(new SelectionUpdatedEventArgs());
        }

        /// <summary>
        /// Sets the selected entity.
        /// </summary>
        /// <param name="e">The entity.</param>
        public void SetSelection(Entity e)
        {
            _selection.Clear();

            GoToParent(e);

            // We don't want to focus on the root petri net, ever. Nothing interesting here.
            if(e != null && !(e is RootPetriNet)) {
                _selection.Add(e);
            }

            OnSelectionUpdated(new SelectionUpdatedEventArgs());
        }

        /// <summary>
        /// Gets the selected entities.
        /// </summary>
        /// <value>The selected entities.</value>
        public ReadOnlyCollection<Entity> SelectedEntities {
            get {
                return new ReadOnlyCollection<Entity>(_selection);
            }
        }

        /// <summary>
        /// Gets the selected entity if there is no multiple selection, else <c>null</c>.
        /// </summary>
        /// <value>The selected entity if single selection, else <c>null</c>.</value>
        public Entity SelectedEntity {
            get {
                if(SelectedEntities.Count == 1) {
                    foreach(var e in SelectedEntities) {
                        return e;
                    }
                }

                return null;
            }
        }

        /// <summary>
        /// Gets a value indicating whether ther is multiple selected entity.
        /// </summary>
        /// <value><c>true</c> if multiple selection; otherwise, <c>false</c>.</value>
        public bool MultipleSelection {
            get {
                return SelectedEntities.Count > 1;
            }
        }

        /// <summary>
        /// Arguments for the <see cref="SelectionUpdated"/> event.
        /// </summary>
        [Serializable]
        public class SelectionUpdatedEventArgs : EventArgs
        {

        }

        /// <summary>
        /// The delegate mapped to the <see cref="SelectionUpdated"/> event.
        /// </summary>
        public delegate void SelectionUpdatedDel(SelectionUpdatedEventArgs e);

        /// <summary>
        /// Occurs when there is an update in the set of selected entities.
        /// </summary>
        public event SelectionUpdatedDel SelectionUpdated;

        /// <summary>
        /// Triggers the <see cref="SelectionUpdated"/> event.
        /// </summary>
        /// <param name="e">E.</param>
        protected void OnSelectionUpdated(SelectionUpdatedEventArgs e)
        {
            SelectionUpdated?.Invoke(e);
        }

        /// <summary>
        /// Gets the drawing area where the petri net will be drawn into.
        /// </summary>
        /// <value>The drawing area.</value>
        public Gtk.DrawingArea DrawingArea {
            get;
            private set;
        }

        /// <summary>
        /// Notifies the view that it has to be redrawn.
        /// </summary>
        public void Redraw()
        {
            if(_window.Gui == null || _window.Gui.BaseView == this) {
                if(_needsRedraw == false) {
                    _needsRedraw = true;
                    GUIApplication.RunOnUIThread(() => {
                        DrawingArea.QueueDraw();
                    });
                }
            }
        }

        /// <summary>
        /// Arguments for the <see cref="FocusChanged"/> event.
        /// </summary>
        [Serializable]
        public class FocusChangedEventArgs : EventArgs
        {
            /// <summary>
            /// Initializes a new instance of the
            /// <see cref="T:Petri.Application.GUI.GUIPetriView.FocusChangedEventArgs"/> class.
            /// </summary>
            /// <param name="isFocusIn">If set to <c>true</c> is focus in.</param>
            public FocusChangedEventArgs(bool isFocusIn)
            {
                IsFocusIn = isFocusIn;
            }

            /// <summary>
            /// Gets a value indicating whether the event is triggered by a focus in event.
            /// </summary>
            /// <value><c>true</c> if focus in; otherwise, <c>false</c>.</value>
            public bool IsFocusIn {
                get;
                private set;
            }

            /// <summary>
            /// Gets a value indicating whether the event is triggered by a focus out event.
            /// </summary>
            /// <value><c>true</c> if focus out; otherwise, <c>false</c>.</value>
            public bool IsFocusOut {
                get {
                    return !IsFocusIn;
                }
            }
        }

        /// <summary>
        /// The delegate mapped to the <see cref="FocusChanged"/> event.
        /// </summary>
        public delegate void FocusChangededDel(GUIPetriView view, FocusChangedEventArgs e);

        /// <summary>
        /// Occurs when the focus of the petri view is changed.
        /// </summary>
        public event FocusChangededDel FocusChanged;

        /// <summary>
        /// Triggers the <see cref="FocusChanged"/> event.
        /// </summary>
        /// <param name="e">E.</param>
        public void OnFocusChanged(FocusChangedEventArgs e)
        {
            FocusChanged?.Invoke(this, e);
        }

        /// <summary>
        /// Adds a petri net to the current petri nets path, making it the current petri net
        /// <see cref="Petri.Application.PetriView.CurrentPetriNet"/>.
        /// This also clears the selection as we just changed from content to display.
        /// </summary>
        /// <param name="pn">The petri net to push.</param>
        public override void PushPetriNet(PetriNet pn)
        {
            base.PushPetriNet(pn);
            ClearSelection();
            _nextPetriNet = -1;
            Redraw();
        }

        /// <summary>
        /// Removes the current petri net <see cref="Petri.Application.PetriView.CurrentPetriNet"/>
        /// from the current petri nets path, making its predecesssor the current petri net.
        /// This also clears the selection as we just changed from content to display.
        /// </summary>
        public override void PopPetriNet()
        {
            base.PopPetriNet();
            ClearSelection();
            _nextPetriNet = -1;
            Redraw();
        }

        /// <summary>
        /// Manages a double click made at the specified position.
        /// </summary>
        /// <param name="button">The button that made the click. 1 for the left button, 2 for the right button, 3 for the middle button.</param>
        /// <param name="mouse">The coordinates of the mouse when the click was made.</param>
        protected virtual void ManageTwoButtonPress(uint button, Point mouse)
        {
        }

        /// <summary>
        /// Manages a double click made at the specified position.
        /// </summary>
        /// <param name="button">The button that made the click. 1 for the left button, 2 for the right button, 3 for the middle button.</param>
        /// <param name="mouse">The coordinates of the mouse when the click was made.</param>
        protected virtual void ManageOneButtonPress(uint button, Point mouse)
        {
        }

        /// <summary>
        /// Manages a click release at the specified position.
        /// </summary>
        /// <param name="button">The button that was released. 1 for the left button, 2 for the right button, 3 for the middle button.</param>
        /// <param name="mouse">The coordinates of the mouse when the click was made.</param>
        protected virtual void ManageButtonRelease(uint button, Point mouse)
        {
        }

        /// <summary>
        /// Manages a mouse movement.
        /// </summary>
        /// <param name="mouse">The new position of the pointer.</param>
        protected virtual void ManageMotion(Point mouse)
        {
        }

        /// <summary>
        /// Receives an event when a btton is pressed in the petri view.
        /// </summary>
        /// <param name="o">O.</param>
        /// <param name="ev">Ev.</param>
        void OnButtonPressEvent(object o, ButtonPressEventArgs ev)
        {
            {
                // This ignores clicks inside the petri view when those clicks are the one which brought focus to the window.
                // This prevents unwanted selection changes.
                var now = DateTime.Now;
                if((now - _lastFocus).TotalMilliseconds < 200) {
                    // This allows for the second click to be taken into account, even if it happens in less than 200 ms.
                    // Indeed, a double click in this context probably means that the user really want to select something.
                    _lastFocus = now - new TimeSpan(0, 0, 1);

                    return;
                }
            }

            _window.Gui.Status = "";
            DrawingArea.HasFocus = true;

            if(ev.Event.Type == Gdk.EventType.ButtonPress) {
                // The Windows version of GTK# currently doesn't detect TwoButtonPress events, so here is a lame simulation of it.
                if(/*ev.Type == Gdk.EventType.TwoButtonPress || */(_lastClickPosition.X == ev.Event.X && _lastClickPosition.Y == ev.Event.Y && (DateTime.Now - _lastClickDate).TotalMilliseconds < 500)) {
                    _lastClickPosition.X = -12345;

                    ManageTwoButtonPress(ev.Event.Button,
                                         new Point(ev.Event.X / Zoom,
                                                   ev.Event.Y / Zoom));
                } else {
                    _lastClickDate = DateTime.Now;
                    _lastClickPosition.X = ev.Event.X;
                    _lastClickPosition.Y = ev.Event.Y;

                    var scrolled = _window.Gui.ScrolledWindow;

                    if(ev.Event.X >= PathOffset + scrolled.Hadjustment.Value && ev.Event.X < _parentHierarchy[_parentHierarchy.Count - 1].Extents.Value.Width + PathOffset + 5 + scrolled.Hadjustment.Value
                       && ev.Event.Y >= PathOffset + scrolled.Vadjustment.Value && ev.Event.Y < _parentHierarchy[_parentHierarchy.Count - 1].Extents.Value.Height + PathOffset + scrolled.Vadjustment.Value) {
                        for(int i = 0; i < _parentHierarchy.Count; ++i) {
                            if(ev.Event.X < PathOffset + _parentHierarchy[i].Extents.Value.Width + 5 + scrolled.Hadjustment.Value) {
                                _nextPetriNet = i;
                                break;
                            }
                        }
                    } else {
                        var doc = CurrentPetriNet.Document is ExternalDocument ? CurrentPetriNet.Document : CurrentPetriNet is ExternalInnerPetriNet ? ((ExternalInnerPetriNet)CurrentPetriNet).ExternalDocument : null;
                        bool click = false;
                        if(doc != null) {
                            if(ev.Event.X >= _editPosition.X * Zoom && ev.Event.X < _editPosition.X * Zoom + _editPixBuf.Width
                               && ev.Event.Y >= _editPosition.Y * Zoom && ev.Event.Y < _editPosition.Y * Zoom + _editPixBuf.Height) {
                                GUIApplication.OpenDocument(doc.Path);
                                click = true;
                            }
                        }
                        if(!click) {
                            ManageOneButtonPress(ev.Event.Button,
                                                 new Point(ev.Event.X / Zoom,
                                                 ev.Event.Y / Zoom));
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Receives an event when a button is released in the petri view.
        /// </summary>
        /// <param name="o">O.</param>
        /// <param name="ev">Ev.</param>
        void OnButtonReleaseEvent(object o, ButtonReleaseEventArgs ev)
        {
            if(_nextPetriNet != -1) {
                var npn = _nextPetriNet;
                while(_parentHierarchy.Count > npn + 1) {
                    PopPetriNet();
                }
                Redraw();
            } else {
                ManageButtonRelease(ev.Event.Button,
                                    new Point(ev.Event.X / Zoom,
                                              ev.Event.Y / Zoom));
            }
        }

        /// <summary>
        /// Does so that the next click will not be forming a double click.
        /// </summary>
        protected void ResetDoubleClick()
        {
            _lastClickPosition.X = Double.MinValue;
        }

        /// <summary>
        /// Manages a mouse motion.
        /// </summary>
        /// <param name="o">The emitter of the event.</param>
        /// <param name="ev">The event.</param>
        void OnMotionNotifyEvent(object o, MotionNotifyEventArgs ev)
        {
            _nextPetriNet = -1;
            ManageMotion(new Point(ev.Event.X / Zoom, ev.Event.Y / Zoom));
        }

        /// <summary>
        /// Gets the offset that will make a position scroll-independent.
        /// </summary>
        /// <value>The fixed offset.</value>
        protected override Vector FixedDrawingOffset {
            get {
                return new Vector(_window.Gui.ScrolledWindow.Hadjustment.Value,
                                  _window.Gui.ScrolledWindow.Vadjustment.Value);
            }
        }

        /// <summary>
        /// Gets the size of the drawing containing the petri net.
        /// </summary>
        /// <returns>The extents of the drawing of the petri net.</returns>
        /// <param name="petriNet">Petri net.</param>
        protected override Vector GetExtents(PetriNet petriNet)
        {
            var extents = new Vector();
            extents.X = Math.Max(petriNet.Size.X, DrawingArea.Allocation.Size.Width / Zoom);
            extents.Y = Math.Max(petriNet.Size.Y, DrawingArea.Allocation.Size.Height / Zoom);

            return extents;
        }


        /// <summary>
        /// Performs the rendering of the entities of the petri net, and others item like the path.
        /// If the petri net doesn't fit in the view, then it is redrawn after the view have been enlarged.
        /// </summary>
        /// <returns>The minimal size required to fully draw the petri net.</returns>
        /// <param name="context">The drawing context.</param>
        /// <param name="petriNet">The petri net to draw.</param>
        protected override Vector RenderInternal(Context context, PetriNet petriNet)
        {
            var scrolled = _window.Gui.ScrolledWindow;

            var min = base.RenderInternal(context, petriNet);

            min.X += scrolled.Hadjustment.PageSize / 2;
            min.Y += scrolled.Vadjustment.PageSize / 2;

            int prevX, prevY;
            DrawingArea.GetSizeRequest(out prevX, out prevY);
            DrawingArea.SetSizeRequest((int)min.X, (int)min.Y);
            petriNet.Size = new Vector(min);
            if(Math.Abs(min.X - prevX) > 10 || Math.Abs(min.Y - prevY) > 10) {
                RenderInternal(context, petriNet);
            }

            _needsRedraw = false;

            return min;
        }

        /// <summary>
        /// Gives a chance to draw custom items in the petri view. The drawing will be made after drawing every entity.
        /// </summary>
        /// <param name="context">The drawing context.</param>
        protected override void SpecializedDrawing(Context context)
        {
            base.SpecializedDrawing(context);
            var doc = CurrentPetriNet is ExternalInnerPetriNet ? ((ExternalInnerPetriNet)CurrentPetriNet).ExternalDocument : CurrentPetriNet.Document as ExternalDocument;
            if(doc != null) {
                // Display the imported petri net's path
                var text = Application.ShortPath(((ExternalPetriNetSource)(doc.Parent).PetriSource).Path);
                context.SelectFontFace("Arial", FontSlant.Normal, FontWeight.Normal);
                context.SetFontSize(14 / Zoom);

                context.SetSourceRGBA(0.9, 0.9, 0.9, 1);
                var extents = context.TextExtents(text);
                var szX = _window.Gui.ScrolledWindow.Hadjustment.PageSize;
                var pathPosition = FixedDrawingOffset;

                var posX = pathPosition.X + szX - extents.Width * Zoom - PathOffset - extents.XBearing * Zoom;
                context.Rectangle((posX - 5) / Zoom,
                                  (pathPosition.Y + 10) / Zoom,
                                  (extents.Width * Zoom + 10) / Zoom,
                                  (extents.Height * Zoom + 10) / Zoom);
                context.Fill();

                context.SetSourceRGBA(0, 0, 0, 1);
                context.MoveTo((posX) / Zoom, (pathPosition.Y + PathOffset - extents.YBearing * Zoom) / Zoom);
                context.TextPath(text);
                context.Fill();

                // Display the imported petri net's edit button
                _editPosition = new Point((posX - 5 - _editPixBuf.Width - 5) / Zoom, (pathPosition.Y + 10) / Zoom);
                context.Save();
                context.Scale(1 / Zoom, 1 / Zoom);
                Gdk.CairoHelper.SetSourcePixbuf(context, _editPixBuf, _editPosition.X * Zoom, _editPosition.Y * Zoom);
                context.Rectangle(_editPosition.X * Zoom, _editPosition.Y * Zoom, _editPixBuf.Width, _editPixBuf.Height);
                context.Fill();
                context.Restore();
            }
        }

        /// <summary>
        /// Called when the DrawingArea needs to be redrawn. Redraws the petri net.
        /// </summary>
        /// <param name="o">O.</param>
        /// <param name="ev">Ev.</param>
        void OnExposeEvent(object o, ExposeEventArgs ev)
        {
            using(Cairo.Context context = Gdk.CairoHelper.Create(DrawingArea.GdkWindow)) {
                context.Scale(Zoom, Zoom);
                RenderInternal(context, CurrentPetriNet);
            }
        }

        /// <summary>
        /// Sets the current petri net to the parent of the specified entity.
        /// </summary>
        /// <param name="entity">Entity.</param>
        public void GoToParent(Entity entity)
        {
            if(entity != null && CurrentPetriNet != entity.Parent // If we are not in the right petri net
                   && !(entity.Parent is RootPetriNet && CurrentPetriNet == ((RootPetriNet)entity.Parent).Parent)) { // But we allow being in the ExternalInnerPetriNet when we meant to be in its RootPetriNet
                if(entity is RootPetriNet) { // This occurs when we undo the creation of an entity at the document's root.
                    while(_parentHierarchy.Count > 1) {
                        PopPetriNet();
                    }
                } else {
                    // Go back to the common root…
                    var root = Entity.GetCommonRoot(entity.Parent, CurrentPetriNet);
                    while(CurrentPetriNet != root) {
                        PopPetriNet();
                    }

                    // Then push until we are at the desired selection's parent.
                    var stack = new Stack<PetriNet>();
                    var v = entity.Parent;
                    while(v != root) {
                        stack.Push(v);
                        v = v.Parent;
                    }
                    while(stack.Count > 0) {
                        PushPetriNet(stack.Peek());
                        stack.Pop();
                    }
                }
            }
        }

        /// <summary>
        /// The document's window.
        /// </summary>
        protected MainWindow _window;

        /// <summary>
        /// The delta click.
        /// </summary>
        protected Point _deltaClick;

        /// <summary>
        /// The original position of the entities when they are moved.
        /// </summary>
        protected Point _originalPosition;

        /// <summary>
        /// The selection set.
        /// </summary>
        protected HashSet<Entity> _selection = new HashSet<Entity>();

        Point _lastClickPosition;
        DateTime _lastClickDate;

        DateTime _lastFocus = DateTime.Now;

        bool _needsRedraw;
        int _nextPetriNet;
        Point _editPosition;
        Gdk.Pixbuf _editPixBuf;
    }
}
