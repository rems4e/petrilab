//
//  EditorGui.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2014-11-24.
//

using System;
using System.Collections.Generic;

using Gdk;
using Gtk;

namespace Petri.Application.GUI.Editor
{
    /// <summary>
    /// The GUI of the petri net editor.
    /// </summary>
    public class EditorGui : Gui
    {
        /// <summary>
        /// Initializes the <see cref="T:Petri.Application.GUI.Editor.EditorGui"/> class.
        /// </summary>
        static EditorGui()
        {
            Pixbuf buf = Pixbuf.LoadFromResource("cpp");
            IconTheme.AddBuiltinIcon("CppGen", buf.Width, buf);

            buf = Pixbuf.LoadFromResource("build");
            IconTheme.AddBuiltinIcon("Build", (int)(buf.Width / 0.8), buf);

            buf = Pixbuf.LoadFromResource("arrow");
            IconTheme.AddBuiltinIcon("Arrow", (int)(buf.Width / 0.8), buf);

            buf = Pixbuf.LoadFromResource("action");
            IconTheme.AddBuiltinIcon("Action", (int)(buf.Width / 0.8), buf);

            buf = Pixbuf.LoadFromResource("transition");
            IconTheme.AddBuiltinIcon("Transition", (int)(buf.Width / 0.8), buf);

            buf = Pixbuf.LoadFromResource("comment");
            IconTheme.AddBuiltinIcon("Comment", (int)(buf.Width / 0.8), buf);

            buf = Pixbuf.LoadFromResource("bug");
            IconTheme.AddBuiltinIcon("Debug", (int)(buf.Width / 0.8), buf);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.Editor.EditorGui"/> class.
        /// </summary>
        /// <param name="doc">Document.</param>
        /// <param name="window">The document's window.</param>
        public EditorGui(GUIDocument doc, MainWindow window)
        {
            _document = doc;
            _save = new ToolButton(Stock.Save);
            _save.TooltipMarkup = GUIApplication.SafeMarkupFromLocalized("Save");

            _generateCode = new ToolButton("CppGen");
            _generateCode.IconName = "CppGen";
            _generateCode.TooltipMarkup = GUIApplication.SafeMarkupFromLocalized(
                "Generate <language> short", DocumentSettings.LanguageName(_document.Settings.Language)
            );

            _compile = new ToolButton("Build");
            _compile.IconName = "Build";
            _compile.TooltipMarkup = GUIApplication.SafeMarkupFromLocalized("Compile");

            _deploy = new ToolButton(Stock.Convert);
            _deploy.TooltipMarkup = GUIApplication.SafeMarkupFromLocalized("Deploy");

            _arrow = new ToggleToolButton("Arrow");
            _arrow.Active = true;
            _arrow.IconName = "Arrow";
            _arrow.TooltipMarkup = GUIApplication.SafeMarkupFromLocalized("Select");

            _action = new ToggleToolButton("Action");
            _action.IconName = "Action";
            _action.TooltipMarkup = GUIApplication.SafeMarkupFromLocalized("State toolbar");

            _transition = new ToggleToolButton("Transition");
            _transition.IconName = "Transition";
            _transition.TooltipMarkup = GUIApplication.SafeMarkupFromLocalized("Transition toolbar");

            _comment = new ToggleToolButton("Comment");
            _comment.IconName = "Comment";
            _comment.TooltipMarkup = GUIApplication.SafeMarkupFromLocalized("Comment toolbar");

            _switchToDebug = new ToolButton("Debug");
            _switchToDebug.IconName = "Debug";
            _switchToDebug.TooltipMarkup = GUIApplication.SafeMarkupFromString(Configuration.GetLocalized("Debug"));

            _zoomIn = new ToolButton(Stock.ZoomIn);
            _zoomIn.TooltipMarkup = GUIApplication.SafeMarkupFromLocalized("Zoom In toolbar");
            _zoomOut = new ToolButton(Stock.ZoomOut);
            _zoomOut.TooltipMarkup = GUIApplication.SafeMarkupFromLocalized("Zoom Out toolbar");

            _findTool = new ToolButton(Stock.Find);
            _findTool.TooltipMarkup = GUIApplication.SafeMarkupFromLocalized("Find");

            _save.Clicked += OnClick;
            _generateCode.Clicked += OnClick;
            _compile.Clicked += OnClick;
            _deploy.Clicked += OnClick;
            _switchToDebug.Clicked += OnClick;

            _zoomIn.Clicked += OnClick;
            _zoomOut.Clicked += OnClick;
            _arrow.Clicked += OnClick;
            _action.Clicked += OnClick;
            _transition.Clicked += OnClick;
            _comment.Clicked += OnClick;
            _findTool.Clicked += OnClick;

            _toolbar.Insert(_save, -1);
            _toolbar.Insert(_generateCode, -1);
            if(doc.Settings.Language != Code.Language.EmbeddedC) {
                _toolbar.Insert(_compile, -1);
                _toolbar.Insert(_deploy, -1);
            }

            _toolbar.Insert(new SeparatorToolItem(), -1);

            _toolbar.Insert(_arrow, -1);
            _toolbar.Insert(_action, -1);
            _toolbar.Insert(_transition, -1);
            _toolbar.Insert(_comment, -1);

            _toolbar.Insert(new SeparatorToolItem(), -1);

            _toolbar.Insert(_zoomOut, -1);
            _toolbar.Insert(_zoomIn, -1);

            _toolbar.Insert(new SeparatorToolItem(), -1);

            _toolbar.Insert(_findTool, -1);

            if(doc.Settings.Language != Code.Language.EmbeddedC) {
                _toolbar.Insert(new SeparatorToolItem(), -1);
                _toolbar.Insert(_switchToDebug, -1);
            }

            HPaned.SizeRequested += (object o, SizeRequestedArgs args) => {
                PropertiesPanel.Resize((o as HPaned).Child2.Allocation.Width);
            };

            _petriView = new EditorView(doc, window);
            PropertiesPanel = new EditorPropertiesPanel(null, doc, window, this);
            _petriView.SelectionUpdated += e => {
                // The properties panel is likely to trigger new selection (CommitGuiAction, FocusableEntity).
                // We don't want to recreate the panel if the selection doesn't change.
                // But we need to recreate it if the selected entity is null, as it means we have to handle the current
                // enclosing petri net.
                if(PropertiesPanel.Entity != _petriView.SelectedEntity || _petriView.MultipleSelection || _petriView.SelectedEntity == null) {
                    PropertiesPanel = new EditorPropertiesPanel(_petriView.SelectedEntity, doc, window, this);
                }
            };

            var viewport = new Viewport();
            viewport.Add(_petriView.DrawingArea);

            _petriView.DrawingArea.SizeRequested += (o, args) => {
                viewport.WidthRequest = viewport.Child.Requisition.Width;
                viewport.HeightRequest = viewport.Child.Requisition.Height;
            };

            _scroll.Add(viewport);
            VPaned.Pack1(_scroll, true, true);
        }

        /// <summary>
        /// Gets the entity editor.
        /// </summary>
        /// <value>The editor properties panel.</value>
        public EditorPropertiesPanel PropertiesPanel {
            get;
            private set;
        }

        /// <summary>
        /// Gets the properties panel.
        /// </summary>
        /// <value>The base properties panel.</value>
        public override PropertiesPanel.PropertiesPanel BasePropertiesPanel {
            get {
                return PropertiesPanel;
            }
        }

        /// <summary>
        /// Manages a toolbar click.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">E.</param>
        protected async void OnClick(object sender, EventArgs e)
        {
            if(_toggling) {
                // This is necessary, as when setting the Active property of one of the edition tools buttons, it triggers the click event again.
                // This avoids a stack overflow.
                return;
            }
            _toggling = true;

            try {
                if(sender == _save) {
                    _document.Save();
                } else if(sender == _generateCode) {
                    _document.GenerateCodeEnsureOutputPath();
                } else if(sender == _compile) {
                    await _document.Compile();
                } else if(sender == _deploy) {
                    await _document.Deploy();
                } else if(sender == _switchToDebug) {
                    _document.Window.SwitchToDebug();
                } else if(sender == _zoomIn) {
                    _petriView.Zoom /= 0.8f;
                    if(_petriView.Zoom > 8f) {
                        _petriView.Zoom = 8f;
                    }
                    _petriView.Redraw();
                } else if(sender == _zoomOut) {
                    _petriView.Zoom *= 0.8f;
                    if(_petriView.Zoom < 0.01f) {
                        _petriView.Zoom = 0.01f;
                    }
                    _petriView.Redraw();
                } else if(sender == _arrow) {
                    _petriView.CurrentTool = EditorView.EditorTool.Arrow;
                    _arrow.Active = true;
                    _action.Active = false;
                    _transition.Active = false;
                    _comment.Active = false;
                } else if(sender == _action) {
                    _petriView.CurrentTool = EditorView.EditorTool.Action;
                    _arrow.Active = false;
                    _action.Active = true;
                    _transition.Active = false;
                    _comment.Active = false;
                } else if(sender == _transition) {
                    _petriView.CurrentTool = EditorView.EditorTool.Transition;
                    _arrow.Active = false;
                    _action.Active = false;
                    _transition.Active = true;
                    _comment.Active = false;
                } else if(sender == _comment) {
                    _petriView.CurrentTool = EditorView.EditorTool.Comment;
                    _arrow.Active = false;
                    _action.Active = false;
                    _transition.Active = false;
                    _comment.Active = true;
                } else if(sender == _findTool) {
                    _document.Find();
                }
            } catch(Exception except) {
                _document.NotifyError(except.Message);
            }

            _toggling = false;
        }

        /// <summary>
        /// Gets the code generation toolbar item.
        /// </summary>
        /// <value>The code gen.</value>
        public ToolButton CodeGen {
            get {
                return _generateCode;
            }
        }

        /// <summary>
        /// Gets the editor's petri view.
        /// </summary>
        /// <value>The view.</value>
        public EditorView View {
            get {
                return _petriView;
            }
        }

        /// <summary>
        /// Gets the petri view.
        /// </summary>
        /// <value>The base view.</value>
        public override GUIPetriView BaseView {
            get {
                return View;
            }
        }

        /// <summary>
        /// Sets a value indicating whether this <see cref="Petri.Application.GUI.Editor.EditorGui"/> is compiling the petri net.
        /// </summary>
        /// <value><c>true</c> if compilation; otherwise, <c>false</c>.</value>
        public bool Compilation {
            set {
                GUIApplication.RunOnUIThread(() => {
                    if(value) {
                        _compile.Sensitive = false;
                    } else {
                        _compile.Sensitive = true;
                    }
                });
            }
        }

        /// <summary>
        /// Updates the GUI (mainly menu items and toolbars).
        /// </summary>
        public override void Update()
        {
            base.Update();

            _document.Window.CopyItem.Sensitive = View.SelectedEntities.Count > 0;
            _document.Window.CutItem.Sensitive = View.SelectedEntities.Count > 0 && !View.IsReadonly;
            _document.Window.PasteItem.Sensitive = EntityClipboard.Clipboard.Count > 0 && !View.IsReadonly;
            _document.Window.EmbedItem.Sensitive = View.SelectedEntities.Count > 0 && !View.IsReadonly;
            _document.Window.ImportPetriNetItem.Sensitive = !View.IsReadonly;

            _deploy.Sensitive = _document.Settings.CurrentProfile.DeployMode != DeployMode.DoNothing;
        }

        /// <summary>
        /// Copies something to the clipboard.
        /// </summary>
        public override void Copy()
        {
            _document.EditorController.Copy();
        }

        /// <summary>
        /// Cuts something to the clipboard.
        /// </summary>
        public override void Cut()
        {
            _document.EditorController.Cut();
        }

        /// <summary>
        /// Pastes something from the clipboard.
        /// </summary>
        public override void Paste()
        {
            _document.EditorController.Paste();
        }

        /// <summary>
        /// Selects all the current petri net's entities.
        /// </summary>
        public override void SelectAll()
        {
            var selected = new List<Model.Entity>();
            selected.AddRange(View.CurrentPetriNet.States);
            selected.AddRange(View.CurrentPetriNet.Transitions);
            selected.AddRange(View.CurrentPetriNet.Comments);
            View.SetSelection(selected);
        }

        EditorView _petriView;

        ToolButton _save, _generateCode, _compile, _deploy, _switchToDebug, _zoomIn, _zoomOut, _findTool;
        ToggleToolButton _arrow, _action, _transition, _comment;

        bool _toggling = false;

        GUIDocument _document;
    }
}
