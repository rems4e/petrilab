//
//  EditorPropertiesPanel.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2014-11-20.
//

using Petri.Model;

namespace Petri.Application.GUI.Editor
{
    /// <summary>
    /// An editor which goal is to edit a selected entity.
    /// </summary>
    public class EditorPropertiesPanel : PropertiesPanel.PropertiesPanel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.Editor.EditorPropertiesPanel"/> class.
        /// </summary>
        /// <param name="selected">The edited entity.</param>
        /// <param name="doc">Document.</param>
        /// <param name="window">The main document's window.</param>
        /// <param name="gui">The editor GUI.</param>
        public EditorPropertiesPanel(Entity selected,
                                     LocalDocument doc,
                                     MainWindow window,
                                     EditorGui gui) : base(doc, window, gui.PropertiesPanelView)
        {
            Entity = selected;

            if(selected != null) {
                AddModule(new PropertiesPanel.IdentifierModule(this, selected));
            }

            if(selected is State) {
                if(selected is ExitPoint) {
                    AddModule(new PropertiesPanel.ExitPointModule(this, (ExitPoint)selected));
                }
                AddModule(new PropertiesPanel.EntityNameModule(this, selected));
                if(!(selected is ExitPoint)) {
                    AddModule(new PropertiesPanel.InitialStateModule(this, (State)selected));
                }
                AddModule(new PropertiesPanel.RequiredTokensModule(this, (State)selected));
                if(selected is Action) {
                    if(!(selected is ExitPoint)) {
                        // FIXME: re-enable
                        // AddModule(new PropertiesPanel.ActionTimeoutModule(this, (Action)selected));
                    }
                    AddModule(new PropertiesPanel.InvocationModule(this, (Action)selected, doc.EntityFactory, gui.View.RecentFunctions));
                } else if(selected is InnerPetriNet) {
                    if(selected is ExternalInnerPetriNet) {
                        AddModule(new PropertiesPanel.PetriNetReferenceModule(this, (ExternalInnerPetriNet)selected));
                        AddModule(new PropertiesPanel.PetriNetArgumentsModule(this, (ExternalInnerPetriNet)selected));
                        AddModule(new PropertiesPanel.PetriNetValueRetrievalModule(this, (ExternalInnerPetriNet)selected));
                    }
                }
                if(selected is ExitPoint) {
                    AddModule(new PropertiesPanel.PetriNetReturnValuesModule(this, (ExitPoint)selected));
                }
                AddModule(new PropertiesPanel.EntityErrorModule(this, selected));
            } else if(selected is Transition) {
                AddModule(new PropertiesPanel.EntityNameModule(this, selected));
                AddModule(new PropertiesPanel.TransitionConditionModule(this, (Transition)selected));
                AddModule(new PropertiesPanel.EntityErrorModule(this, selected));
            } else if(selected is Comment) {
                AddModule(new PropertiesPanel.CommentModule(this, (Comment)selected));
            } else if(gui.View.MultipleSelection) {
                AddModule(new PropertiesPanel.MultipleSelectionModule(this));
            } else {
                var current = gui.View.CurrentPetriNet;
                if(current is ExternalInnerPetriNet) {
                    current = ((ExternalInnerPetriNet)current).ExternalDocument.PetriNet;
                }
                if(current is RootPetriNet) {
                    AddModule(new PropertiesPanel.PetriNetVariablesModule(this, (RootPetriNet)current));
                    AddModule(new PropertiesPanel.EntityErrorModule(this, current));
                } else {
                    AddModule(new PropertiesPanel.EmptySelectionModule(this));
                }
            }

            Reset();
        }
    }
}
