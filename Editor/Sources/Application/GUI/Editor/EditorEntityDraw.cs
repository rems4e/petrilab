//
//  EditorEntityDraw.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2014-12-06.
//

using System;
using System.Linq;

using Cairo;

using Petri.Model;

using Point = Petri.Model.Point;

namespace Petri.Application.GUI.Editor
{
    /// <summary>
    /// The object responsible for drawing the editor's petri net parts.
    /// </summary>
    public class EditorEntityDraw : EntityDraw
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EditorEntityDraw"/> class.
        /// </summary>
        /// <param name="document">The document.</param>
        /// <param name="editor">Editor.</param>
        public EditorEntityDraw(LocalDocument document, EditorView editor)
        {
            _document = document;
            _editor = editor;
        }

        /// <summary>
        /// Inits the context for the comment's border.
        /// </summary>
        /// <param name="c">The comment.</param>
        /// <param name="context">Context.</param>
        protected override void InitContextForBorder(Comment c, Context context)
        {
            base.InitContextForBorder(c, context);
            if(_editor.IsEntitySelected(c)) {
                context.LineWidth = 2;
            }
            context.SetSourceRGBA(0.8, 0.6, 0.4, 1);
        }

        /// <summary>
        /// Draws the border of the comment.
        /// </summary>
        /// <param name="c">The comment.</param>
        /// <param name="context">Context.</param>
        protected override void DrawBorder(Comment c, Context context)
        {
            base.DrawBorder(c, context);
            if(_editor.IsEntitySelected(c)) {
                var point = new Point(c.Position.X - c.Size.X / 2 - 2, c.Position.Y - 2);
                context.MoveTo(point);
                point.X += 6;
                context.LineTo(point);
                point.Y += 6;
                context.LineTo(point);
                point.X -= 6;
                context.LineTo(point);
                point.Y -= 6;
                context.LineTo(point);

                point.X = c.Position.X + c.Size.X / 2 - 7;
                context.MoveTo(point);
                point.X += 6;
                context.LineTo(point);
                point.Y += 6;
                context.LineTo(point);
                point.X -= 6;
                context.LineTo(point);
                point.Y -= 6;
                context.LineTo(point);
                context.Fill();
            }
        }

        /// <summary>
        /// Inits the context for the state's background.
        /// </summary>
        /// <param name="s">The state.</param>
        /// <param name="context">Context.</param>
        protected override void InitContextForBackground(State s, Context context)
        {
            Color color;

            bool reachable = s.IsReachable;

            var issues = _document.GetIssues(s);
            if(issues.Count > 0) {
                // Error is in child or we have a warning
                bool showAsWarning = issues.All((issue) => !issue.IsError);

                if(s is PetriNet && showAsWarning) {
                    if(reachable) {
                        color = new Color(1, 0.6, 0.2);
                    } else {
                        color = new Color(1, 0.85, 0.65);
                    }
                } else {
                    if(reachable) {
                        color = new Color(1, 0.52, 0.52);
                    } else {
                        color = new Color(1, 0.8, 0.8);
                    }
                }
            } else {
                if(reachable) {
                    color = new Color(1, 1, 1);
                } else {
                    color = new Color(0.9, 0.9, 0.9);
                }
            }

            context.SetSourceRGBA(color.R, color.G, color.B, color.A);
        }

        /// <summary>
        /// Inits the context for the state's border.
        /// </summary>
        /// <param name="s">The state.</param>
        /// <param name="context">Context.</param>
        protected override void InitContextForBorder(State s, Context context)
        {
            Color color;
            if(s.IsReachable) {
                color = new Color(0, 0, 0, 1);
            } else {
                color = new Color(0.6, 0.6, 0.6, 1);
            }

            double lineWidth = 3;

            if(_editor.IsEntitySelected(s)) {
                color.R = 1;
                color.G = 0;
                color.B = 0;
            }
            context.LineWidth = lineWidth;
            context.SetSourceRGBA(color.R, color.G, color.B, color.A);

            if(s == _editor.HoveredItem && _editor.CurrentAction == EditorView.EditorAction.CreatingTransition) {
                lineWidth += 2;
            }

            context.LineWidth = lineWidth;
        }

        /// <summary>
        /// Inits the context for the state's name.
        /// </summary>
        /// <param name="s">The state.</param>
        /// <param name="context">Context.</param>
        protected override void InitContextForName(State s, Context context)
        {
            base.InitContextForName(s, context);
            if(_editor.IsEntitySelected(s)) {
                context.SetSourceRGBA(1, 0, 0, 1);
            }
        }

        /// <summary>
        /// Gets the arrow scale for the transition.
        /// </summary>
        /// <returns>The arrow scale.</returns>
        /// <param name="t">The transition.</param>
        protected override double GetArrowScale(Transition t)
        {
            if(_editor.IsEntitySelected(t)) {
                return 18;
            } else {
                return base.GetArrowScale(t);
            }
        }

        /// <summary>
        /// Inits the context for the transition's line.
        /// </summary>
        /// <param name="t">The transition.</param>
        /// <param name="context">Context.</param>
        protected override void InitContextForLine(Transition t, Context context)
        {
            Color c;
            if(t.Before.IsReachable) {
                c = new Color(0.1, 0.6, 1, 1);
            } else {
                c = new Color(0.6, 0.6, 0.6, 1);
            }
            double lineWidth = 2;

            if(_editor.IsEntitySelected(t)) {
                if(_editor.CurrentAction == EditorView.EditorAction.ChangingTransitionDestination || _editor.CurrentAction == EditorView.EditorAction.ChangingTransitionOrigin) {
                    c.R = 0.6;
                    c.G = 0.6;
                    c.B = 0.6;
                } else {
                    c.R = 0.3;
                    c.G = 0.8;
                    c.B = 1.0;
                    lineWidth += 2;
                }
            }
            context.SetSourceRGBA(c.R, c.G, c.B, c.A);
            context.LineWidth = lineWidth;
        }

        /// <summary>
        /// Inits the context for the transition's background.
        /// </summary>
        /// <param name="t">The transition.</param>
        /// <param name="context">Context.</param>
        protected override void InitContextForBackground(Transition t, Context context)
        {
            Color color = new Color(1, 1, 1, 1);

            if(_document.GetIssues(t).Count > 0) {
                color.R = 1;
                color.G = 0.6;
                color.B = 0.6;
            }

            context.SetSourceRGBA(color.R, color.G, color.B, color.A);
        }

        /// <summary>
        /// Draws the line of the transition.
        /// </summary>
        /// <param name="t">The transition.</param>
        /// <param name="context">Context.</param>
        protected override void DrawLine(Transition t, Context context)
        {
            base.DrawLine(t, context);

            if(_editor.IsEntitySelected(t) && !_editor.MultipleSelection) {
                if((t.After.Position - t.Position).Norm > t.After.Radius) {

                    var pos = GetOriginHandle(t);
                    context.Arc(pos.X,
                                pos.Y,
                                Configuration.TransitionHandleRadius,
                                0,
                                2 * Math.PI);

                    pos = GetDestinationHandle(t);
                    context.Arc(pos.X,
                                pos.Y,
                                Configuration.TransitionHandleRadius,
                                0,
                                2 * Math.PI);

                    context.Fill();
                }
            }
        }

        /// <summary>
        /// Gets the origin handle's position of the transition.
        /// </summary>
        /// <returns>The origin handle.</returns>
        /// <param name="t">The transition.</param>
        public Point GetOriginHandle(Transition t)
        {
            var origin = TransitionOrigin(t);
            var destination = TransitionDestination(t);

            return BézierAt(origin, destination, _controlPoints[t], Configuration.TransitionHandleRadius, 1);
        }

        /// <summary>
        /// Gets the destination handle's position of the transition.
        /// </summary>
        /// <returns>The destination handle.</returns>
        /// <param name="t">The transition.</param>
        public Point GetDestinationHandle(Transition t)
        {
            var origin = TransitionOrigin(t);
            var destination = TransitionDestination(t);

            return BézierAt(origin, destination, _controlPoints[t], Configuration.TransitionHandleRadius, -1);
        }

        /// <summary>
        /// Determines if the specified point is on the transition's origin handle.
        /// </summary>
        /// <returns><c>true</c> if the point is on the origin handle of the transition; otherwise, <c>false</c>.</returns>
        /// <param name="point">Point.</param>
        /// <param name="transition">The transition.</param>
        public bool IsOnOriginHandle(Point point, Transition transition)
        {
            var pos = GetOriginHandle(transition);
            if(Math.Abs(point.X - pos.X) < Configuration.TransitionHandleRadius && Math.Abs(point.Y - pos.Y) < Configuration.TransitionHandleRadius) {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Determines if the specified point is on the transition's origin handle.
        /// </summary>
        /// <returns><c>true</c> if the point is on the origin handle of the transition; otherwise, <c>false</c>.</returns>
        /// <param name="point">Point.</param>
        /// <param name="transition">The transition.</param>
        public bool IsOnDestinationHandle(Point point, Transition transition)
        {
            var pos = GetDestinationHandle(transition);
            if(Math.Abs(point.X - pos.X) < Configuration.TransitionHandleRadius && Math.Abs(point.Y - pos.Y) < Configuration.TransitionHandleRadius) {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Inits the context for the transition's text.
        /// </summary>
        /// <param name="t">The transition.</param>
        /// <param name="context">Context.</param>
        protected override void InitContextForText(Transition t, Context context)
        {
            base.InitContextForText(t, context);
            if(_editor.IsEntitySelected(t) && (_editor.CurrentAction == EditorView.EditorAction.ChangingTransitionDestination || _editor.CurrentAction == EditorView.EditorAction.ChangingTransitionOrigin)) {
                context.SetSourceRGBA(0.6, 0.6, 0.6, 1);
            }
        }

        LocalDocument _document;
        EditorView _editor;
    }
}

