//
//  EditorView.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2014-11-23.
//

using System;
using System.Collections.Generic;
using System.Linq;

using Cairo;
using Gtk;

using Petri.Model;

using Action = Petri.Model.Action;
using Point = Petri.Model.Point;

namespace Petri.Application.GUI.Editor
{
    /// <summary>
    /// The petri view of the petri net editor.
    /// </summary>
    public class EditorView : GUIPetriView
    {
        /// <summary>
        /// A tool of the editor.
        /// </summary>
        public enum EditorTool
        {
            /// <summary>
            /// The tool that allows the selection of items.
            /// </summary>
            Arrow,

            /// <summary>
            /// The tool that allows the creation of states.
            /// </summary>
            Action,

            /// <summary>
            /// The tool that allows the creation of transitions.
            /// </summary>
            Transition,

            /// <summary>
            /// The tool that allows the creation of comments.
            /// </summary>
            Comment
        }

        /// <summary>
        /// The editor current user's action.
        /// </summary>
        public enum EditorAction
        {
            /// <summary>
            /// The user is doing nothing.
            /// </summary>
            None,

            /// <summary>
            /// The user is moving a state.
            /// </summary>
            MovingAction,

            /// <summary>
            /// The user is moving a comment.
            /// </summary>
            MovingComment,

            /// <summary>
            /// The user is moving a transition.
            /// </summary>
            MovingTransition,

            /// <summary>
            /// The user is creating a transition.
            /// </summary>
            CreatingTransition,

            /// <summary>
            /// The user is changing a transition's origin.
            /// </summary>
            ChangingTransitionOrigin,

            /// <summary>
            /// The user is changing a transition's destination.
            /// </summary>
            ChangingTransitionDestination,

            /// <summary>
            /// The user is drawing a selection rectangle.
            /// </summary>
            SelectionRect,

            /// <summary>
            /// The user is resizing a comment.
            /// </summary>
            ResizingComment
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EditorView"/> class.
        /// </summary>
        /// <param name="doc">Document.</param>
        /// <param name="window">The document's window.</param>
        public EditorView(GUIDocument doc, MainWindow window) : base(new PetriNetProvider(doc), window)
        {
            _editorController = doc.EditorController;
            _entityFactory = doc.EntityFactory;

            CurrentAction = EditorAction.None;
            CurrentTool = EditorTool.Arrow;
            EntityDraw = new EditorEntityDraw(doc, this);

            RecentFunctions = new List<Petri.Code.Function>();

            DrawingArea.KeyPressEvent += OnKeyPressEvent;
            DrawingArea.KeyReleaseEvent += OnKeyReleaseEvent;

            FocusChanged += (GUIPetriView view, FocusChangedEventArgs e) => {
                _shiftDown = false;
                _ctrlDown = false;
                _altDown = false;
                CurrentAction = EditorAction.None;
                if(e.IsFocusIn) {
                    _hoveredItem = null;
                }
            };
        }

        /// <summary>
        /// Gets action that is undertook by the user.
        /// </summary>
        /// <value>The current action.</value>
        public EditorAction CurrentAction {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets the current editing tool.
        /// </summary>
        /// <value>The current tool.</value>
        public EditorTool CurrentTool {
            get;
            set;
        }

        /// <summary>
        /// Gets the entity that is hovered by the cursor.
        /// </summary>
        /// <value>The hovered item.</value>
        public Entity HoveredItem {
            get {
                return _hoveredItem;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the current petri net is readonly.
        /// </summary>
        /// <value><c>true</c> if readonly; otherwise, <c>false</c>.</value>
        public bool IsReadonly {
            get {
                return CurrentPetriNet.Document is ExternalDocument || CurrentPetriNet is ExternalInnerPetriNet;
            }
        }

        /// <summary>
        /// Manages a double click made at the specified position.
        /// </summary>
        /// <param name="button">The button that made the click. 1 for the left button, 2 for the right button, 3 for the middle button.</param>
        /// <param name="mouse">The coordinates of the mouse when the click was made.</param>
        protected override void ManageTwoButtonPress(uint button, Point mouse)
        {
            if(button == 1) {
                // Add new action
                if(SelectedEntities.Count == 0 && !IsReadonly) {
                    _window.CommitGuiAction(new AddStateAction(new Action(CurrentPetriNet.Document.EntityFactory,
                                                               CurrentPetriNet,
                                                               false,
                                                               mouse)));
                    _hoveredItem = SelectedEntity;
                } else if(SelectedEntities.Count == 1) {
                    CurrentAction = EditorAction.None;

                    var selected = SelectedEntity as State;

                    // Change type from Action to InnerPetriNet
                    if(selected != null && selected is Action && !IsReadonly) {
                        selected = _editorController.ActionToInnerPetriNet((Action)selected) ?? selected;
                    }

                    if(selected is InnerPetriNet && !(selected is ExternalInnerPetriNet && ((ExternalInnerPetriNet)selected).ExternalDocument is InvalidExternalDocument)) {
                        PushPetriNet((InnerPetriNet)selected);
                    }
                }
            } else if(button == 3) {
                if(SelectedEntities.Count == 0 && !IsReadonly) {
                    _window.CommitGuiAction(new AddCommentAction(new Comment(_entityFactory,
                                                                 CurrentPetriNet,
                                                                 Configuration.GetLocalized("DefaultCommentName"),
                                                                 mouse)));
                    _hoveredItem = SelectedEntity;
                }
            }
        }

        /// <summary>
        /// Manages a double click made at the specified position.
        /// </summary>
        /// <param name="button">The button that made the click. 1 for the left button, 2 for the right button, 3 for the middle button.</param>
        /// <param name="mouse">The coordinates of the mouse when the click was made.</param>
        protected override void ManageOneButtonPress(uint button, Point mouse)
        {
            if(button == 1) {
                _deltaClick = mouse;

                if(CurrentAction == EditorAction.None) {
                    if(SelectedEntity is Transition
                       && ((EditorEntityDraw)EntityDraw).IsOnOriginHandle(mouse, (Transition)SelectedEntity)
                       && !IsReadonly) {
                        CurrentAction = EditorAction.ChangingTransitionOrigin;
                    } else if(SelectedEntity is Transition
                              && ((EditorEntityDraw)EntityDraw).IsOnDestinationHandle(mouse, (Transition)SelectedEntity)
                              && !IsReadonly) {
                        CurrentAction = EditorAction.ChangingTransitionDestination;
                    } else {
                        _hoveredItem = CurrentPetriNet.StateAtPosition(_deltaClick);

                        if(_hoveredItem == null) {
                            _hoveredItem = CurrentPetriNet.TransitionAtPosition(_deltaClick);

                            if(_hoveredItem == null) {
                                _hoveredItem = CurrentPetriNet.CommentAtPosition(_deltaClick);
                            }
                        }

                        if(_hoveredItem != null) {
                            if(_shiftDown || _ctrlDown) {
                                if(IsEntitySelected(_hoveredItem)) {
                                    RemoveFromSelection(_hoveredItem);
                                    _hoveredItem = null;
                                } else {
                                    AddToSelection(_hoveredItem);
                                }
                            } else if(!IsEntitySelected(_hoveredItem)) {
                                SetSelection(_hoveredItem);
                            }

                            if(_hoveredItem != null) {
                                _motionReference = _hoveredItem;
                                _originalPosition.X = _motionReference.Position.X;
                                _originalPosition.Y = _motionReference.Position.Y;

                                if(_motionReference is State && !IsReadonly) {
                                    CurrentAction = EditorAction.MovingAction;
                                } else if(_motionReference is Transition && !IsReadonly) {
                                    CurrentAction = EditorAction.MovingTransition;
                                } else if(_motionReference is Comment && !IsReadonly) {
                                    var c = _motionReference as Comment;
                                    if(Math.Abs(mouse.X - _motionReference.Position.X - c.Size.X / 2) < 8 || Math.Abs(mouse.X - _motionReference.Position.X + (_motionReference as Comment).Size.X / 2) < 8) {
                                        CurrentAction = EditorAction.ResizingComment;
                                        _beforeResize.X = c.Size.X;
                                        _beforeResize.Y = c.Size.Y;
                                    } else {
                                        CurrentAction = EditorAction.MovingComment;
                                    }
                                }
                                _deltaClick.X = mouse.X - _originalPosition.X;
                                _deltaClick.Y = mouse.Y - _originalPosition.Y;
                            }
                        }
                    }
                }

                if(CurrentAction == EditorAction.None) {
                    if(CurrentTool == EditorTool.Arrow) {
                        if(_hoveredItem == null) {
                            if(!(_ctrlDown || _shiftDown)) {
                                ClearSelection();
                            } else {
                                _selectedFromRect = new HashSet<Entity>(SelectedEntities);
                            }
                            CurrentAction = EditorAction.SelectionRect;
                            _originalPosition = mouse;
                        }
                    } else if(CurrentTool == EditorTool.Action) {
                        if(_hoveredItem == null && !IsReadonly) {
                            _window.CommitGuiAction(new AddStateAction(new Action(_entityFactory,
                                                                                  CurrentPetriNet,
                                                                                  false,
                                                                                  mouse)));
                        }
                    } else if(CurrentTool == EditorTool.Transition) {
                        if(_hoveredItem is State && !(_hoveredItem is ExitPoint) && !IsReadonly) {
                            SetSelection(_hoveredItem);
                            CurrentAction = EditorAction.CreatingTransition;
                            _deltaClick = mouse;
                        }
                    } else if(CurrentTool == EditorTool.Comment) {
                        if(_hoveredItem == null && !IsReadonly) {
                            _window.CommitGuiAction(new AddCommentAction(new Comment(_entityFactory,
                                                                                     CurrentPetriNet,
                                                                                     Configuration.GetLocalized("DefaultCommentName"),
                                                                                     mouse)));
                        }
                    }
                }
            } else if(button == 3) {
                if(_hoveredItem is State && !(_hoveredItem is ExitPoint) && !IsReadonly) {
                    SetSelection(_hoveredItem);
                    CurrentAction = EditorAction.CreatingTransition;
                    _deltaClick = mouse;
                } else if(_hoveredItem == null) {
                    CurrentAction = EditorAction.None;
                    ClearSelection();
                }
            }
        }

        /// <summary>
        /// Manages a click release at the specified position.
        /// </summary>
        /// <param name="button">The button that was released. 1 for the left button, 2 for the right button, 3 for the middle button.</param>
        /// <param name="mouse">The coordinates of the mouse when the click was made.</param>
        protected override void ManageButtonRelease(uint button, Point mouse)
        {
            if((CurrentAction == EditorAction.MovingAction
               || CurrentAction == EditorAction.MovingTransition
                || CurrentAction == EditorAction.MovingComment) && !IsReadonly) {
                var backToPrevious = _originalPosition - _motionReference.Position;
                if(backToPrevious.X != 0 || backToPrevious.Y != 0) {
                    var actions = new List<GuiAction>();
                    foreach(var e in SelectedEntities) {
                        e.Position = e.Position + backToPrevious;
                        actions.Add(new MoveEntityAction(e,
                                                         -backToPrevious,
                                                         (!_altDown && e.StickToGrid) || (_altDown && !e.StickToGrid)));
                    }
                    _window.CommitGuiAction(new GuiActionList(actions,
                                                              actions.Count > 1 ? Configuration.GetLocalized("Move the entities") : Configuration.GetLocalized("Move the entity")));
                }
                CurrentAction = EditorAction.None;
            } else if(CurrentAction == EditorAction.CreatingTransition && button == 1 && !IsReadonly) {
                CurrentAction = EditorAction.None;
                if(_hoveredItem != null && _hoveredItem is State) {
                    _window.CommitGuiAction(new AddTransitionAction(new Transition(_entityFactory,
                                                                                   CurrentPetriNet,
                                                                                   SelectedEntity as State,
                                                                                   _hoveredItem as State),
                                            (_hoveredItem as State).RequiredTokens == 0));
                }
            } else if(CurrentAction == EditorAction.SelectionRect) {
                CurrentAction = EditorAction.None;

                SetSelection(_selectedFromRect);

                _selectedFromRect.Clear();
            } else if(CurrentAction == EditorAction.ResizingComment && !IsReadonly) {
                CurrentAction = EditorAction.None;
                var newSize = new Vector((_hoveredItem as Comment).Size);
                (_hoveredItem as Comment).Size = _beforeResize;
                _window.CommitGuiAction(new ResizeCommentAction(_hoveredItem as Comment, newSize));
            } else if(CurrentAction == EditorAction.ChangingTransitionOrigin && !IsReadonly) {
                CurrentAction = EditorAction.None;
                if(_hoveredItem is State && !(_hoveredItem is ExitPoint)) {
                    _window.CommitGuiAction(new ChangeTransitionEndAction((Transition)SelectedEntity,
                                                                          (State)_hoveredItem,
                                                                          false));
                }
            } else if(CurrentAction == EditorAction.ChangingTransitionDestination && !IsReadonly) {
                CurrentAction = EditorAction.None;
                if(_hoveredItem is State) {
                    _window.CommitGuiAction(new ChangeTransitionEndAction((Transition)SelectedEntity,
                                                                          (State)_hoveredItem,
                                                                          true));
                }
            }

            Redraw();
        }

        /// <summary>
        /// Manages a mouse movement.
        /// </summary>
        /// <param name="mouse">The new position of the pointer.</param>
        protected override void ManageMotion(Point mouse)
        {
            if((CurrentAction == EditorAction.MovingAction
                || CurrentAction == EditorAction.MovingTransition
                || CurrentAction == EditorAction.MovingComment) && !IsReadonly) {
                if(CurrentAction == EditorAction.MovingAction || CurrentAction == EditorAction.MovingComment) {
                    RemoveFromSelection(new List<Entity>(SelectedEntities.Where(e => e is Transition)));
                } else {
                    SetSelection(_motionReference);
                }
                var delta = new Vector(mouse.X - _deltaClick.X - _motionReference.Position.X,
                                       mouse.Y - _deltaClick.Y - _motionReference.Position.Y);
                foreach(var e in SelectedEntities) {
                    var pos = e.Position + delta;
                    if((!_altDown && e.StickToGrid) || (_altDown && !e.StickToGrid)) {
                        pos.X = Math.Round(pos.X / Entity.GridSize) * Entity.GridSize;
                        pos.Y = Math.Round(pos.Y / Entity.GridSize) * Entity.GridSize;
                    }

                    e.Position = pos;
                }
                Redraw();
            } else if(CurrentAction == EditorAction.SelectionRect) {
                _deltaClick = mouse;

                _deltaUpdated = true;
                ManageScrolling();

                var oldSet = new HashSet<Entity>(SelectedEntities);
                _selectedFromRect = new HashSet<Entity>();

                double xm = Math.Min(_deltaClick.X, _originalPosition.X);
                double ym = Math.Min(_deltaClick.Y, _originalPosition.Y);
                double xM = Math.Max(_deltaClick.X, _originalPosition.X);
                double yM = Math.Max(_deltaClick.Y, _originalPosition.Y);

                foreach(State s in CurrentPetriNet.States) {
                    if(xm < s.Position.X + s.Radius && xM > s.Position.X - s.Radius && ym < s.Position.Y + s.Radius && yM > s.Position.Y - s.Radius)
                        _selectedFromRect.Add(s);
                }

                foreach(Transition t in CurrentPetriNet.Transitions) {
                    if(xm < t.Position.X + t.Width / 2 && xM > t.Position.X - t.Width / 2 && ym < t.Position.Y + t.Height / 2 && yM > t.Position.Y - t.Height / 2)
                        _selectedFromRect.Add(t);
                }

                foreach(Comment c in CurrentPetriNet.Comments) {
                    if(xm < c.Position.X + c.Size.X / 2 && xM > c.Position.X - c.Size.X / 2 && ym < c.Position.Y + c.Size.Y / 2 && yM > c.Position.Y - c.Size.Y / 2)
                        _selectedFromRect.Add(c);
                }

                _selectedFromRect.SymmetricExceptWith(oldSet);

                Redraw();
            } else if(CurrentAction == EditorAction.ResizingComment && !IsReadonly) {
                var comment = _hoveredItem as Comment;
                double w = Math.Abs(mouse.X - comment.Position.X) * 2;
                comment.Size = new Vector(w, 0);
                Redraw();
            } else {
                _deltaClick = mouse;

                _hoveredItem = CurrentPetriNet.StateAtPosition(_deltaClick);

                if(_hoveredItem == null) {
                    _hoveredItem = CurrentPetriNet.TransitionAtPosition(_deltaClick);

                    if(_hoveredItem == null) {
                        _hoveredItem = CurrentPetriNet.CommentAtPosition(_deltaClick);
                    }
                }

                if(CurrentAction != EditorAction.None) {
                    Redraw();
                }
            }
        }

        /// <summary>
        /// Manages a key press event.
        /// </summary>
        /// <param name="o">O.</param>
        /// <param name="ev">Ev.</param>
        protected void OnKeyPressEvent(object o, KeyPressEventArgs ev)
        {
            _window.Gui.Status = "";
            ResetDoubleClick();

            if(ev.Event.Key == Gdk.Key.Escape) {
                if(CurrentAction == EditorAction.CreatingTransition) {
                    CurrentAction = EditorAction.None;
                    Redraw();
                } else if(CurrentAction == EditorAction.None) {
                    if(SelectedEntities.Count > 0) {
                        ClearSelection();
                    } else if(_parentHierarchy.Count > 1) {
                        PopPetriNet();
                    }
                    Redraw();
                } else if(CurrentAction == EditorAction.SelectionRect) {
                    CurrentAction = EditorAction.None;
                    Redraw();
                }
            } else if((ev.Event.Key == Gdk.Key.Delete || ev.Event.Key == Gdk.Key.BackSpace)
                      && SelectedEntities.Count > 0
                      && CurrentAction == EditorAction.None
                      && !IsReadonly) {
                var action = _editorController.RemoveSelectionAction();
                if(action != null) {
                    _window.CommitGuiAction(action);
                }
            } else if(ev.Event.Key == Gdk.Key.Shift_L || ev.Event.Key == Gdk.Key.Shift_R) {
                _shiftDown = true;
            } else if(ev.Event.Key == Gdk.Key.Alt_L || ev.Event.Key == Gdk.Key.Alt_R) {
                _altDown = true;
            } else if(((Configuration.RunningPlatform == Platform.Mac) && (ev.Event.Key == Gdk.Key.Meta_L || ev.Event.Key == Gdk.Key.Meta_R)) || ((Configuration.RunningPlatform != Platform.Mac) && (ev.Event.Key == Gdk.Key.Control_L || ev.Event.Key == Gdk.Key.Control_R))) {
                _ctrlDown = true;
            }
        }

        /// <summary>
        /// Manages a key release event.
        /// </summary>
        /// <param name="o">O.</param>
        /// <param name="ev">Ev.</param>
        protected void OnKeyReleaseEvent(object o, KeyReleaseEventArgs ev)
        {
            if(ev.Event.Key == Gdk.Key.Shift_L || ev.Event.Key == Gdk.Key.Shift_R) {
                _shiftDown = false;
            } else if(ev.Event.Key == Gdk.Key.Alt_L || ev.Event.Key == Gdk.Key.Alt_R) {
                _altDown = false;
            } else if(((Configuration.RunningPlatform == Platform.Mac) && (ev.Event.Key == Gdk.Key.Meta_L || ev.Event.Key == Gdk.Key.Meta_R)) || ((Configuration.RunningPlatform != Platform.Mac) && (ev.Event.Key == Gdk.Key.Control_L || ev.Event.Key == Gdk.Key.Control_R))) {
                _ctrlDown = false;
            }
        }

        /// <summary>
        /// Manages the scrolling when the user is drawing a selection rectangle and the mouse's cursor reaches one edge of the scrolling window.
        /// </summary>
        protected void ManageScrolling()
        {
            if(!_scrolling) {
                GLib.Timeout.Add(100, () => {
                    _scrolling = true;

                    var scrolled = _window.EditorGui.ScrolledWindow;
                    const double margin = 30, powCoef = 0.6;

                    if(_deltaUpdated) {
                        _scrollingDelta.X = _deltaClick.X * Zoom - scrolled.Hadjustment.Value;
                        _scrollingDelta.Y = _deltaClick.Y * Zoom - scrolled.Vadjustment.Value;
                        _deltaUpdated = false;
                    }

                    if(_scrollingDelta.X > scrolled.Allocation.Width - margin) {
                        scrolled.Hadjustment.Value = Math.Min(scrolled.Hadjustment.Upper - scrolled.Hadjustment.PageSize,
                                                              scrolled.Hadjustment.Value
                                                                  + Math.Pow((_scrollingDelta.X - (scrolled.Allocation.Width - margin)), powCoef));
                    } else if(_scrollingDelta.X < margin) {
                        scrolled.Hadjustment.Value = Math.Max(scrolled.Hadjustment.Lower,
                                                              scrolled.Hadjustment.Value - Math.Pow(margin - _scrollingDelta.X, powCoef));
                    }

                    if(_scrollingDelta.Y > scrolled.Allocation.Height - margin) {
                        scrolled.Vadjustment.Value = Math.Min(scrolled.Vadjustment.Upper - scrolled.Vadjustment.PageSize,
                                                              scrolled.Vadjustment.Value
                                                                  + Math.Pow((_scrollingDelta.Y - (scrolled.Allocation.Height - margin)), powCoef));
                    } else if(_scrollingDelta.Y < margin) {
                        scrolled.Vadjustment.Value = Math.Max(scrolled.Vadjustment.Lower,
                                                              scrolled.Vadjustment.Value - Math.Pow(margin - _scrollingDelta.Y, powCoef));
                    }

                    if(CurrentAction != EditorAction.SelectionRect) {
                        _scrolling = false;
                    }

                    return CurrentAction == EditorAction.SelectionRect;
                });
            }
        }

        /// <summary>
        /// Gives a chance to draw custom items in the petri view. The drawing will be made after drawing every entity.
        /// This will draw the selection rectangle and the changing of one of a transition's ends, among others.
        /// </summary>
        /// <param name="context">The drawing context.</param>
        protected override void SpecializedDrawing(Context context)
        {
            base.SpecializedDrawing(context);
            if(CurrentAction == EditorAction.CreatingTransition || CurrentAction == EditorAction.ChangingTransitionDestination || CurrentAction == EditorAction.ChangingTransitionOrigin) {
                var color = new Color(1, 0, 0, 1);
                double lineWidth = 2;

                State end = null;
                // Acknowledge a valid transition change unless we want to make a transition come out of an ExitPoint
                if(_hoveredItem is State && !(_hoveredItem is ExitPoint && CurrentAction == EditorAction.ChangingTransitionOrigin)) {
                    color.R = 0;
                    color.G = 1;
                    end = (State)_hoveredItem;
                }

                State transitionEnd = null;
                if(CurrentAction == EditorAction.CreatingTransition) {
                    transitionEnd = (State)SelectedEntity;
                } else if(CurrentAction == EditorAction.ChangingTransitionDestination) {
                    transitionEnd = ((Transition)SelectedEntity).Before;
                } else if(CurrentAction == EditorAction.ChangingTransitionOrigin) {
                    transitionEnd = ((Transition)SelectedEntity).After;
                }

                var direction = _deltaClick - transitionEnd.Position;
                if(transitionEnd == _hoveredItem || direction.Norm > transitionEnd.Radius) {
                    context.LineWidth = lineWidth;
                    context.SetSourceRGBA(color.R, color.G, color.B, color.A);
                    var endPos = _deltaClick;
                    if(end != null) {
                        endPos = end.Position;
                    }
                    var tPos = Transition.GetPosition(
                        transitionEnd.Position,
                        endPos
                    ) + Transition.GetDefaultShift(transitionEnd == _hoveredItem);

                    // Invert origin and destinations when we change a transition's origin to avoid drawing the arrow on the wrong side of the line.
                    // We cannot simply swap the 2 variables or on a looping transition, the ends will be crossed.
                    Point origin, destination;
                    if(CurrentAction == EditorAction.ChangingTransitionOrigin) {
                        destination = EntityDraw.TransitionDestination(tPos, transitionEnd, transitionEnd == _hoveredItem);
                        origin = _deltaClick;
                        if(end != null) {
                            origin = EntityDraw.TransitionOrigin(tPos, end, transitionEnd == _hoveredItem);
                        }
                    } else {
                        origin = EntityDraw.TransitionOrigin(tPos, transitionEnd, transitionEnd == _hoveredItem);
                        destination = _deltaClick;
                        if(end != null) {
                            destination = EntityDraw.TransitionDestination(tPos, end, transitionEnd == _hoveredItem);
                        }
                    }

                    var control = EntityDraw.DrawLine(
                        context,
                        origin,
                        destination,
                        tPos,
                        transitionEnd == _hoveredItem
                    );

                    var pos = EntityDraw.BézierAt(origin, destination, control, EntityDraw.DefaultArrowScale, -1);
                    var arrowDir = (destination - pos).Normalized;
                    EntityDraw.DrawArrow(context, arrowDir, destination, EntityDraw.DefaultArrowScale);
                }
            } else if(CurrentAction == EditorAction.SelectionRect) {
                double xm = Math.Min(_deltaClick.X, _originalPosition.X);
                double ym = Math.Min(_deltaClick.Y, _originalPosition.Y);
                double xM = Math.Max(_deltaClick.X, _originalPosition.X);
                double yM = Math.Max(_deltaClick.Y, _originalPosition.Y);

                context.LineWidth = 1;
                context.MoveTo(xm, ym);
                context.SetSourceRGBA(0.4, 0.4, 0.4, 0.6);
                context.Rectangle(xm, ym, xM - xm, yM - ym);
                context.StrokePreserve();
                context.SetSourceRGBA(0.8, 0.8, 0.8, 0.3);
                context.Fill();
            }
        }

        /// <summary>
        /// Checks whether the specified entity is selected.
        /// </summary>
        /// <returns><c>true</c>, if the entity is selected, <c>false</c> otherwise.</returns>
        /// <param name="e">The entity.</param>
        public bool IsEntitySelected(Entity e)
        {
            if(CurrentAction == EditorAction.SelectionRect) {
                return _selectedFromRect.Contains(e);
            }
            return SelectedEntities.Contains(e);
        }

        /// <summary>
        /// Clears the selection.
        /// </summary>
        public override void ClearSelection()
        {
            base.ClearSelection();
            _hoveredItem = null;
        }

        /// <summary>
        /// Adds the entity to the selection.
        /// </summary>
        /// <param name="e">The entity.</param>
        public void AddToSelection(Entity e)
        {
            _selection.Add(e);
            OnSelectionUpdated(new SelectionUpdatedEventArgs());
        }

        /// <summary>
        /// Adds to the entity list to the selection.
        /// </summary>
        /// <param name="e">The entities list.</param>
        public void AddToSelection(IEnumerable<Entity> e)
        {
            _selection.IntersectWith(e);
            OnSelectionUpdated(new SelectionUpdatedEventArgs());
        }

        /// <summary>
        /// Removes the entity from the selection.
        /// </summary>
        /// <param name="e">The entity.</param>
        public void RemoveFromSelection(Entity e)
        {
            _selection.Remove(e);
            OnSelectionUpdated(new SelectionUpdatedEventArgs());
        }

        /// <summary>
        /// Removes each entity of the list from the selection.
        /// </summary>
        /// <param name="e">The entities list.</param>
        public void RemoveFromSelection(IEnumerable<Entity> e)
        {
            _selection.ExceptWith(e);
            OnSelectionUpdated(new SelectionUpdatedEventArgs());
        }

        /// <summary>
        /// Sets the selection to the given list of entities.
        /// </summary>
        /// <param name="selection">The list of entities.</param>
        public void SetSelection(IEnumerable<Entity> selection)
        {
            foreach(var e in selection) {
                GoToParent(e);
                break;
            }

            _selection.Clear();
            _selection.UnionWith(selection);

            OnSelectionUpdated(new SelectionUpdatedEventArgs());
        }

        /// <summary>
        /// Gets the list of recent functions used in the Actions editor.
        /// </summary>
        /// <value>The recent functions.</value>
        public List<Code.Function> RecentFunctions {
            get;
            private set;
        }

        EditorController _editorController;
        EntityFactory _entityFactory;

        Entity _motionReference;
        HashSet<Entity> _selectedFromRect = new HashSet<Entity>();
        Vector _beforeResize = new Vector();
        Entity _hoveredItem;
        bool _shiftDown;
        bool _altDown;
        bool _ctrlDown;

        bool _scrolling = false, _deltaUpdated = false;
        Vector _scrollingDelta = new Vector();
    }
}
