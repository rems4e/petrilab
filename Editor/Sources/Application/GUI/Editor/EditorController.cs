//
//  EditorController.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2014-11-13.
//

using System;
using System.Collections.Generic;
using System.Linq;

using Gtk;

using Petri.Model;

namespace Petri.Application.GUI.Editor
{
    /// <summary>
    /// The controller of the petri net editor.
    /// </summary>
    public class EditorController : Controller
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.Editor.EditorController"/> class.
        /// </summary>
        /// <param name="doc">Document.</param>
        public EditorController(GUIDocument doc) : base(new PetriNetProvider(doc))
        {
            _document = doc;
        }

        /// <summary>
        /// Gets or sets the document.
        /// </summary>
        /// <value>The document.</value>
        MainWindow Window {
            get {
                return _document.Window;
            }
        }

        /// <summary>
        /// Copies something to the clipboard.
        /// </summary>
        public void Copy()
        {
            if(Window.EditorGui.View.SelectedEntities.Count > 0) {
                EntityClipboard.Clipboard = new HashSet<Entity>(CloneEntities(Window.EditorGui.View.SelectedEntities,
                                                                              Window.EditorGui.View.CurrentPetriNet));
                EntityClipboard.PasteCount = 0;

                Window.EditorGui.Update();
            }
        }

        /// <summary>
        /// Pastes something from the clipboard.
        /// </summary>
        public void Paste()
        {
            if(EntityClipboard.Clipboard.Count > 0 && !Window.EditorGui.View.IsReadonly) {
                ++EntityClipboard.PasteCount;

                var action = PasteAction();
                Window.CommitGuiAction(action);
            }
        }

        /// <summary>
        /// Cuts something to the clipboard.
        /// </summary>
        public void Cut()
        {
            if(Window.EditorGui.View.SelectedEntities.Count > 0 && !Window.EditorGui.View.IsReadonly) {
                Copy();

                var action = RemoveSelectionAction();
                if(action != null) {
                    Window.CommitGuiAction(new GuiActionWrapper(action, Configuration.GetLocalized("Cut the entities")));
                }
            }
        }

        /// <summary>
        /// Embeds the currently selected items in an inner petri net.
        /// </summary>
        public void EmbedInInnerPetriNet()
        {
            var selected = Window.EditorGui.View.SelectedEntities;
            if(selected.Count > 0 && !Window.EditorGui.View.IsReadonly) {
                var toRemove = new List<Entity>();
                foreach(var e in selected) {
                    if(e is Transition) {
                        var t = (Transition)e;
                        if(!selected.Contains(t.Before) || !selected.Contains(t.After))
                            toRemove.Add(t);
                    } else if(e is State) {
                        var s = (State)e;
                        foreach(var t in s.TransitionsBefore) {
                            if(!selected.Contains(t)) {
                                toRemove.Add(t);
                            }
                        }
                        foreach(var t in s.TransitionsAfter) {
                            if(!selected.Contains(t)) {
                                toRemove.Add(t);
                            }
                        }
                    }
                }

                if(toRemove.Count > 0) {
                    _document.NotifyError(
                        Configuration.GetLocalized("Unable to wrap the selection into a macro: some entities linked to the selection are not selected.")
                    );
                    return;
                }


                var actions = new List<GuiAction>();

                var pos = new Point(double.MaxValue, double.MaxValue);
                foreach(var e in selected) {
                    if(e is State || e is Comment) {
                        pos.X = Math.Min(pos.X, e.Position.X);
                        pos.Y = Math.Min(pos.Y, e.Position.Y);
                    }
                }
                var macro = new LocalInnerPetriNet(Window.EditorGui.View.CurrentPetriNet.Document.EntityFactory,
                                                   Window.EditorGui.View.CurrentPetriNet,
                                                   false,
                                                   pos);

                actions.Add(new AddStateAction(macro));

                foreach(var e in selected) {
                    if(e is Comment) {
                        actions.Add(new RemoveCommentAction(e as Comment));
                    } else if(e is State) {
                        actions.Add(new RemoveStateAction(e as State));
                    } else if(e is Transition) {
                        actions.Add(new RemoveTransitionAction(e as Transition, false));
                    }
                }

                foreach(var e in selected) {
                    actions.Add(new ChangeParentAction(e, macro));

                    if(e is Comment) {
                        actions.Add(new AddCommentAction(e as Comment));
                    } else if(e is State) {
                        actions.Add(new AddStateAction(e as State));
                    } else if(e is Transition) {
                        actions.Add(new AddTransitionAction(e as Transition, false));
                    }

                    actions.Add(new MoveEntityAction(e,
                                                     new Point(50, 50) - pos,
                                                     true));
                }

                Window.CommitGuiAction(new GuiActionList(actions, Configuration.GetLocalized("Wrap into a macro")));
            }
        }

        /// <summary>
        /// Transforms an action into an inner petri net.
        /// </summary>
        /// <returns>The inner petri net if the user agreed on the action, or <c>null</c> if it was cancelled.</returns>
        /// <param name="action">The action to transform.</param>
        public InnerPetriNet ActionToInnerPetriNet(Model.Action action)
        {
            var ok = _document.Window.PromptYesNo(
                Configuration.GetLocalized("Do you really want to make the selected state into a macro?"),
                Configuration.GetLocalized("Yes"),
                Configuration.GetLocalized("No")
            ).Result;

            if(ok) {
                Window.EditorGui.View.ClearSelection();
                var inner = new LocalInnerPetriNet(_document.EntityFactory,
                                                   Window.EditorGui.View.CurrentPetriNet,
                                                   action.IsStartState,
                                                   action.Position);
                inner.Name = action.Name;

                var guiActionList = new List<GuiAction>();

                var statesTable = new Dictionary<UInt64, State>();
                foreach(Transition t in action.TransitionsAfter) {
                    statesTable[t.After.ID] = t.After;
                    statesTable[t.Before.ID] = inner;
                    guiActionList.Add(new RemoveTransitionAction(t,
                                                                 t.After.RequiredTokens == t.After.TransitionsBefore.Count));

                    var newTransition = new Transition(_document.EntityFactory,
                                                       Window.EditorGui.View.CurrentPetriNet,
                                                       t.GetXML(),
                                                       statesTable);
                    newTransition.ID = _document.EntityFactory.ConsumeID();
                    guiActionList.Add(new AddTransitionAction(newTransition,
                                                              newTransition.After.RequiredTokens == newTransition.After.TransitionsBefore.Count));
                }
                foreach(Transition t in action.TransitionsBefore) {
                    statesTable[t.Before.ID] = t.Before;
                    statesTable[t.After.ID] = inner;
                    guiActionList.Add(new RemoveTransitionAction(t,
                                                                 t.After.RequiredTokens == t.After.TransitionsBefore.Count));

                    var newTransition = new Transition(_document.EntityFactory,
                                                       Window.EditorGui.View.CurrentPetriNet,
                                                       t.GetXML(),
                                                       statesTable);
                    newTransition.ID = _document.EntityFactory.ConsumeID();
                    guiActionList.Add(new AddTransitionAction(newTransition,
                                                              newTransition.After.RequiredTokens == newTransition.After.TransitionsBefore.Count));
                }

                // It is important to put the action that adds the new inner petri net first in the list.
                // Indeed, it will later be retrieved to set the focus of the GUI, and we want to focus on the newly created petri net.
                guiActionList.Add(new AddStateAction(inner));
                guiActionList.Add(new RemoveStateAction(action));
                guiActionList.Add(new ChangeRequiredTokensAction(inner, action.RequiredTokens));
                var guiAction = new GuiActionList(guiActionList,
                                                  Configuration.GetLocalized("Change the state into a macro"));
                Window.CommitGuiAction(guiAction);

                return inner;
            }

            return null;
        }

        /// <summary>
        /// Asks the user to select a petri document, and imports this document in the current view as an external document.
        /// </summary>
        public void ImportPetriNet()
        {
            if(!Window.EditorGui.View.IsReadonly) {
                var b = new CheckButton(Configuration.GetLocalized("Relative path"));
                b.Active = Configuration.PreferRelativePaths;

                var filename = GUIApplication.SelectDocumentPath((fc) => {
                    fc.Title = Configuration.GetLocalized("Import Petri net…");
                    fc.ActionArea.PackEnd(b);
                    b.Show();
                });

                if(filename != null) {
                    if(b.Active) {
                        filename = Window.EditorGui.View.CurrentPetriNet.Document.GetRelativeToDoc(filename);
                    }

                    var inner = new ExternalInnerPetriNet(
                        Window.EditorGui.View.CurrentPetriNet.Document.EntityFactory,
                        Window.EditorGui.View.CurrentPetriNet,
                        filename,
                        new Point(100, 100)
                    );
                    var action = new AddStateAction(inner);
                    Window.CommitGuiAction(action);
                    // Needs to be after the action was commited.
                    Window.EditorGui.View.CurrentPetriNet.Document.ReassignIDs();
                }
            }
        }

        /// <summary>
        /// Gets the GUI action that causes all entities of the selection to be removed from the current petri net.
        /// </summary>
        /// <returns>The selection action.</returns>
        public GuiAction RemoveSelectionAction()
        {
            var states = new List<State>();
            var comments = new List<Comment>();
            var transitions = new HashSet<Transition>();
            foreach(var e in Window.EditorGui.View.SelectedEntities) {
                if(e is State) {
                    if(!(e is ExitPoint)) { // Do not erase exit point!
                        states.Add(e as State);
                    }

                    // Removes all transitions attached to the deleted states
                    foreach(var t in (e as State).TransitionsAfter) {
                        transitions.Add(t);
                    }
                    foreach(var t in (e as State).TransitionsBefore) {
                        transitions.Add(t);
                    }
                } else if(e is Transition) {
                    transitions.Add(e as Transition);
                } else if(e is Comment) {
                    comments.Add(e as Comment);
                }
            }

            var deleteEntities = new List<GuiAction>();
            foreach(var t in transitions) {
                deleteEntities.Add(new RemoveTransitionAction(t,
                                                              t.After.RequiredTokens == t.After.TransitionsBefore.Count));
            }
            foreach(State s in states) {
                deleteEntities.Add(new RemoveStateAction(s));
            }
            foreach(Comment c in comments) {
                deleteEntities.Add(new RemoveCommentAction(c));
            }

            if(deleteEntities.Count > 0) {
                return new GuiActionList(deleteEntities,
                                         Configuration.GetLocalized("Remove the entities"));
            }

            return null;
        }

        /// <summary>
        /// Gets the GUI action that inserts the entities contained in the clipboard into the current petri net.
        /// </summary>
        /// <returns>The action.</returns>
        GuiAction PasteAction()
        {
            var actionList = new List<GuiAction>();

            var newEntities = CloneEntities(EntityClipboard.Clipboard,
                                            Window.EditorGui.View.CurrentPetriNet);
            var states = from e in newEntities
                         where e is State
                         select (e as State);
            var transitions = new HashSet<Transition>(from e in newEntities
                                                      where e is Transition
                                                      select (e as Transition));
            var comments = from e in newEntities
                           where e is Comment
                           select (e as Comment);

            foreach(State s in states) {
                // Change entity's owner
                s.Parent = Window.EditorGui.View.CurrentPetriNet;
                s.Position = s.Position + new Vector(20 * EntityClipboard.PasteCount);
                actionList.Add(new AddStateAction(s));
            }
            foreach(Comment c in comments) {
                // Change entity's owner
                c.Parent = Window.EditorGui.View.CurrentPetriNet;
                c.Position = c.Position + new Vector(20 * EntityClipboard.PasteCount);
                actionList.Add(new AddCommentAction(c));
            }

            foreach(Transition t in transitions) {
                // Change entity's owner
                t.Parent = Window.EditorGui.View.CurrentPetriNet;
                actionList.Add(new AddTransitionAction(t, false));
            }

            return new GuiActionList(actionList, Configuration.GetLocalized("Paste the entities"));
        }

        /// <summary>
        /// Clones the given entities.
        /// </summary>
        /// <returns>The new entities.</returns>
        /// <param name="entities">The entities to clone.</param>
        /// <param name="newParent">The petri net that will contain the new entities.</param>
        public static List<Entity> CloneEntities(IEnumerable<Entity> entities, PetriNet newParent)
        {
            var destination = newParent.Document;

            var cloned = new List<Entity>();

            var states = from e in entities
                         where (e is State && !(e is ExitPoint))
                         select (e as State);
            var exitPoints = from e in entities
                             where (e is ExitPoint)
                             select (e as ExitPoint);
            var comments = from e in entities
                           where (e is Comment)
                           select (e as Comment);

            var transitions = new List<Transition>(from e in entities
                                                   where e is Transition
                                                   select (e as Transition));

            // We cannot clone a transition without its 2 ends being cloned too
            transitions.RemoveAll(t => !states.Concat(exitPoints).Contains(t.After) || !states.Concat(exitPoints).Contains(t.Before));

            // Basic cloning strategy: serialization/deserialization to XElement, with the save/restore mechanism
            var statesTable = new Dictionary<UInt64, State>();
            foreach(State s in states) {
                var xml = s.GetXML();
                if(s is ExternalInnerPetriNet && destination.Path != s.Document.Path) {
                    var path = xml.Attribute("Path").Value;
                    path = PathUtility.GetFullPath(System.IO.Path.Combine(System.IO.Path.Combine(
                        System.IO.Directory.GetParent(s.Parent.Document.Path).FullName,
                        path
                    )));

                    xml.SetAttributeValue("Path", path);
                }
                var newState = State.FromXml(destination.EntityFactory,
                                             xml,
                                             newParent,
                                             false);
                statesTable.Add(newState.ID, newState);
            }
            foreach(ExitPoint s in exitPoints) {
                var action = s.ToAction();
                statesTable.Add(action.ID, action);
            }
            foreach(Comment c in comments) {
                var xml = c.GetXML();
                var newComment = new Comment(destination.EntityFactory, newParent, xml);
                newComment.ID = destination.EntityFactory.ConsumeID();
                cloned.Add(newComment);
            }

            foreach(Transition t in transitions) {
                var xml = t.GetXML();
                var newTransition = new Transition(destination.EntityFactory, newParent, xml, statesTable);

                // Reassigning an ID to the transitions to keep a unique one for each entity
                newTransition.ID = destination.EntityFactory.ConsumeID();
                cloned.Add(newTransition);

                newTransition.Before.AddTransitionAfter(newTransition);
                newTransition.After.AddTransitionBefore(newTransition);
            }

            foreach(State s in statesTable.Values) {
                // Same as with the transitions. Could not do that before, as we needed the ID to remain the same for the states for the deserialization to work
                UpdateID(s, destination);
                cloned.Add(s);

                // If some transitions were removed as they didn't fully belong to the cloned set, we have to take account of the posibly too big required tokens count.
                // Didn't think of a better way than this.
                if(s.RequiredTokens > s.TransitionsBefore.Count) {
                    s.RequiredTokens = (UInt32)s.TransitionsBefore.Count;
                }
            }

            return cloned;
        }

        /// <summary>
        /// Updates the IDs of all of the entities of the state to be compatible with the provided document.
        /// </summary>
        /// <param name="s">The state (which may be a PetriNet).</param>
        /// <param name="d">The document.</param>
        static void UpdateID(State s, Document d)
        {
            s.ID = s.Document.EntityFactory.ConsumeID();
            var petriNet = s as PetriNet;
            if(petriNet != null) {
                foreach(Comment c in petriNet.Comments) {
                    c.ID = c.Document.EntityFactory.ConsumeID();
                }
                foreach(Transition t in petriNet.Transitions) {
                    t.ID = t.Document.EntityFactory.ConsumeID();
                }
                foreach(State s2 in petriNet.States) {
                    UpdateID(s2, d);
                }
            }
        }

        GUIDocument _document;
    }
}
