//
//  DebugView.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2014-11-23.
//

using Gtk;
using Cairo;

using Petri.Application.Debugger;
using Petri.Model;

using Action = Petri.Model.Action;
using Point = Petri.Model.Point;

namespace Petri.Application.GUI.Debugger
{
    /// <summary>
    /// The petri view of the debugger.
    /// </summary>
    public class DebugView : GUIPetriView
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.Debugger.DebugView"/> class.
        /// </summary>
        /// <param name="doc">Document.</param>
        /// <param name="controller">The debug controller.</param>
        /// <param name="window">The document's window.</param>
        public DebugView(DebuggableGUIDocument doc,
                         DebugController controller,
                         MainWindow window) : base(new PetriNetProvider(doc), window)
        {
            _document = doc;
            _debugController = controller;
            EntityDraw = new DebugEntityDraw(doc, controller, this);
            DrawingArea.KeyPressEvent += OnKeyPressEvent;
        }

        /// <summary>
        /// Manages a double click made at the specified position.
        /// </summary>
        /// <param name="button">The button that made the click. 1 for the left button, 2 for the right button, 3 for the middle button.</param>
        /// <param name="mouse">The coordinates of the mouse when the click was made.</param>
        protected override void ManageTwoButtonPress(uint button, Point mouse)
        {
            if(button == 1) {
                var entity = CurrentPetriNet.StateAtPosition(mouse);
                if(entity is InnerPetriNet && !(entity is ExternalInnerPetriNet && ((ExternalInnerPetriNet)entity).ExternalDocument is InvalidExternalDocument)) {
                    PushPetriNet((InnerPetriNet)entity);
                    Redraw();
                } else if(entity is Action) {
                    var a = entity as Action;
                    if(_debugController.Breakpoints.Contains(a)) {
                        _debugController.RemoveBreakpoint(a);
                    } else if(a.IsReachable) {
                        _debugController.AddBreakpoint(a);
                    }

                    SetSelection(entity);

                    Redraw();
                }
            } else if(button == 3) {
                var state = CurrentPetriNet.StateAtPosition(mouse);
                if(state is Action && state.IsReachable) {
                    _debugController.TemporaryBreakpoint = (Action)state;
                    if(_debugController.Client.CurrentSessionState == DebugClient.SessionState.Started) {
                        if(_debugController.Client.CurrentPetriState == DebugClient.PetriState.Paused) {
                            _debugController.Client.SetPause(false);
                        }
                        if(_debugController.Client.CurrentPetriState == DebugClient.PetriState.Stopped) {
                            _debugController.Client.StartPetri(_debugController.PetriNetArgs);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Manages a double click made at the specified position.
        /// </summary>
        /// <param name="button">The button that made the click. 1 for the left button, 2 for the right button, 3 for the middle button.</param>
        /// <param name="mouse">The coordinates of the mouse when the click was made.</param>
        protected override void ManageOneButtonPress(uint button, Point mouse)
        {
            if(button == 1) {
                _deltaClick = mouse;

                Entity hovered = CurrentPetriNet.StateAtPosition(_deltaClick);

                if(hovered == null) {
                    hovered = CurrentPetriNet.TransitionAtPosition(_deltaClick);

                    if(hovered == null) {
                        hovered = CurrentPetriNet.CommentAtPosition(_deltaClick);
                    }
                }

                SetSelection(hovered);
                Redraw();
            } else if(button == 3) {

            }
        }

        /// <summary>
        /// Manages a key press event.
        /// </summary>
        /// <param name="o">O.</param>
        /// <param name="ev">Ev.</param>
        void OnKeyPressEvent(object o, KeyPressEventArgs ev)
        {
            if(ev.Event.Key == Gdk.Key.Escape) {
                if(_parentHierarchy.Count > 1) {
                    PopPetriNet();
                }
                Redraw();
            }
        }

        /// <summary>
        /// Gives a chance to draw custom items in the petri view. The drawing will be made after drawing every entity.
        /// </summary>
        /// <param name="context">The drawing context.</param>
        protected override void SpecializedDrawing(Context context)
        {
            base.SpecializedDrawing(context);

            if(_document.NeedsSync) {
                var offset = FixedDrawingOffset;

                string text;
                if(!_document.FirstLevelInSync || !_document.ExternalsInSync) {
                    text = Configuration.GetLocalized("Breaking changes; sync to reflect them");
                    context.SetSourceRGBA(1, 1, 1, 1);
                } else {
                    text = Configuration.GetLocalized("Layout changes; synchronize to reflect them");
                    context.SetSourceRGBA(0.9, 0.9, 0.9, 1);
                }

                var szX = _window.Gui.ScrolledWindow.Hadjustment.PageSize;
                var szY = _window.Gui.ScrolledWindow.Vadjustment.PageSize;

                context.SelectFontFace("Arial", FontSlant.Normal, FontWeight.Normal);
                context.SetFontSize(16 / Zoom);

                var extents = context.TextExtents(text);

                var posX = ((szX - extents.Width * Zoom) / 2 + offset.X) / Zoom;
                var posY = (szY - extents.Height * Zoom + offset.Y - PathOffset) / Zoom;

                context.Rectangle(posX - 5 / Zoom,
                                  posY - 5 / Zoom,
                                  (extents.Width * Zoom + 10) / Zoom,
                                  (extents.Height * Zoom + 10) / Zoom);
                context.Fill();

                context.SetSourceRGBA(0, 0, 0, 1);
                context.MoveTo(posX, posY - extents.YBearing);
                context.TextPath(text);
                context.Fill();
            }
        }

        DebuggableGUIDocument _document;
        DebugController _debugController;
    }
}

