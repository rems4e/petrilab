//
//  DebugPropertiesPanel.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2015-01-25.
//

using Petri.Model;

using Action = Petri.Model.Action;

namespace Petri.Application.GUI.Debugger
{
    /// <summary>
    /// The debugger's editor panel. It contains some basic info on the selected entities, and can evaluate expressions.
    /// </summary>
    public class DebugPropertiesPanel : PropertiesPanel.PropertiesPanel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.Debugger.DebugPropertiesPanel"/> class.
        /// </summary>
        /// <param name="doc">Document.</param>
        /// <param name="selected">Selected.</param>
        /// <param name="controller">The debug controller.</param>
        /// <param name="window">The main document's window.</param>
        /// <param name="gui">The debug GUI.</param>
        public DebugPropertiesPanel(DebuggableGUIDocument doc,
                                    Entity selected,
                                    DebugController controller,
                                    MainWindow window,
                                    DebugGui gui) : base(doc, window, gui.PropertiesPanelView)
        {
            Entity = selected;

            if(selected != null) {
                AddModule(new PropertiesPanel.IdentifierModule(this, selected));
            }

            if(selected is Transition) {
                AddModule(new PropertiesPanel.EntityNameModule(this, selected));
                AddModule(new PropertiesPanel.TransitionConditionModule(this, (Transition)selected));
            } else if(selected is State) {
                AddModule(new PropertiesPanel.EntityNameModule(this, selected));
                if(!(selected is ExitPoint)) {
                    AddModule(new PropertiesPanel.InitialStateModule(this, (State)selected));
                }
                AddModule(new PropertiesPanel.RequiredTokensModule(this, (State)selected));

                if(selected is Action) {
                    AddModule(new PropertiesPanel.BreakpointModule(this, (Action)selected, controller));
                    if(!(selected is ExitPoint)) {
                        // FIXME: re-enable
                        //AddModule(new PropertiesPanel.ActionTimeoutModule(this, (Action)selected));
                    }
                    AddModule(new PropertiesPanel.InvocationModule(this, (Action)selected, doc.EntityFactory));
                    if(selected is ExitPoint) {
                        AddModule(new PropertiesPanel.PetriNetReturnValuesModule(this, (ExitPoint)selected));
                    }
                } else if(selected is InnerPetriNet) {
                    if(selected is ExternalInnerPetriNet) {
                        AddModule(new PropertiesPanel.PetriNetReferenceModule(this, (ExternalInnerPetriNet)selected));
                        AddModule(new PropertiesPanel.PetriNetArgumentsModule(this, (ExternalInnerPetriNet)selected));
                        AddModule(new PropertiesPanel.PetriNetValueRetrievalModule(this, (ExternalInnerPetriNet)selected));
                    }
                }
            } else if(selected == null) {
                if(gui.View.CurrentPetriNet is RootPetriNet) {
                    AddModule(new PropertiesPanel.PetriNetVariablesModule(this, (RootPetriNet)gui.View.CurrentPetriNet));
                }
            }

            _evaluateModule = new PropertiesPanel.EvaluateModule(this, doc, controller);
            AddModule(_evaluateModule);

            Reset();
        }

        /// <summary>
        /// Updates the expression evaluation result when it is received.
        /// </summary>
        /// <param name="result">Result.</param>
        /// <param name="userInfo">Additional user info.</param>
        public void OnEvaluate(string result, string userInfo)
        {
            _evaluateModule.OnEvaluate(result, userInfo);
        }

        /// <summary>
        /// Sets the "evaluate" button's sensitivity.
        /// </summary>
        /// <value>The evaluate button's sensitivity.</value>
        public bool EvaluateSensitive {
            set {
                if(_evaluateModule.Evaluate != null) {
                    _evaluateModule.Evaluate.Sensitive = value;
                }
            }
        }

        PropertiesPanel.EvaluateModule _evaluateModule;
    }
}
