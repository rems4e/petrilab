//
//  DebugEntityDraw.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2014-12-06.
//

using Cairo;

using Petri.Application.Debugger;
using Petri.Model;

namespace Petri.Application.GUI.Debugger
{
    /// <summary>
    /// Draw a petri net during debugging.
    /// </summary>
    public class DebugEntityDraw : EntityDraw
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.Debugger.DebugEntityDraw"/> class.
        /// </summary>
        /// <param name="document">The document.</param>
        /// <param name="controller">The debug controller.</param>
        /// <param name="view">The debug view.</param>
        public DebugEntityDraw(DebuggableGUIDocument document, DebugController controller, DebugView view)
        {
            _document = document;
            _controller = controller;
            _view = view;
        }

        /// <summary>
        /// Inits the context for the canva's background.
        /// </summary>
        /// <param name="context">Context.</param>
        public override void InitContextForBackground(Context context)
        {
            base.InitContextForBackground(context);
            if(!_document.FirstLevelInSync || !_document.ExternalsInSync) {
                context.SetSourceRGBA(1, 0.9, 0.9, 1);
            }

        }

        /// <summary>
        /// Inits the context for the state's background.
        /// </summary>
        /// <param name="s">The state.</param>
        /// <param name="context">Context.</param>
        protected override void InitContextForBackground(State s, Context context)
        {

            if(!s.IsReachable) {
                context.SetSourceColor(new Color(0.9, 0.9, 0.9));
            } else {
                Color color = new Color(1, 1, 1, 1);

                var temp = _controller.TemporaryBreakpoint;
                if(s == temp) {
                    color.R = 0.6;
                    color.G = 0.6;
                    color.B = 1;
                } else {
                    lock(_controller.StatesInformation) {
                        if(_controller.StatesInformation.TryGetValue(s as State, out var info) == true && info.InvocationsCount > 0) {
                            if(_controller.Client.CurrentPetriState == DebugClient.PetriState.Paused) {
                                color.R = 0.4;
                                color.G = 0.7;
                                color.B = 0.4;
                            } else {
                                color.R = 0.6;
                                color.G = 1;
                                color.B = 0.6;
                            }
                        } else if(s is InnerPetriNet) {
                            foreach(var a in _controller.StatesInformation) {
                                if(a.Value.InvocationsCount > 0 && ((InnerPetriNet)s).EntityFromID(a.Key.GlobalID) != null) {
                                    if(_controller.Client.CurrentPetriState == DebugClient.PetriState.Paused) {
                                        color.R = 0.7;
                                        color.G = 0.4;
                                        color.B = 0.7;
                                    } else {
                                        color.R = 1;
                                        color.G = 0.7;
                                        color.B = 1;
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }

                context.SetSourceRGBA(color.R, color.G, color.B, color.A);
            }
        }

        /// <summary>
        /// Inits the context for the state's border.
        /// </summary>
        /// <param name="s">The state.</param>
        /// <param name="context">Context.</param>
        protected override void InitContextForBorder(State s, Context context)
        {
            base.InitContextForBorder(s, context);

            Color color;
            if(s.IsReachable) {
                color = new Color(0, 0, 0, 1);
            } else {
                color = new Color(0.6, 0.6, 0.6, 1);
            }

            if(s is Action) {
                if(_controller.Breakpoints.Contains(s as Action)) {
                    color = new Color(1, 0, 0, 1);
                    context.LineWidth = 4;
                }
            }
            if(s == _view.SelectedEntity) {
                context.LineWidth += 2;
            }

            context.SetSourceColor(color);
        }

        /// <summary>
        /// Gets the tokens string for the given state.
        /// </summary>
        /// <returns>The tokens string.</returns>
        /// <param name="s">The state.</param>
        protected override string GetTokensString(State s)
        {
            var str = base.GetTokensString(s);
            lock(_controller.StatesInformation) {
                if(_controller.StatesInformation.TryGetValue(s as State, out var info) == true) {
                    str = info.TokensCount + "/" + str;
                }
            }
            return str;
        }

        /// <summary>
        /// Inits the context for the transition's border.
        /// </summary>
        /// <param name="t">The transition.</param>
        /// <param name="context">Context.</param>
        protected override void InitContextForBorder(Transition t, Context context)
        {
            base.InitContextForBorder(t, context);
            if(t == _view.SelectedEntity) {
                context.LineWidth += 1;
            }
        }

        /// <summary>
        /// Inits the context for the transition's line.
        /// </summary>
        /// <param name="t">The transition.</param>
        /// <param name="context">Context.</param>
        protected override void InitContextForLine(Transition t, Context context)
        {
            base.InitContextForLine(t, context);
            if(t == _view.SelectedEntity) {
                context.LineWidth += 1;
            }
        }

        DebuggableGUIDocument _document;
        DebugController _controller;
        DebugView _view;
    }
}

