//
//  DebugGui.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2014-11-24.
//

using System;
using System.Threading.Tasks;

using Gdk;
using Gtk;

using Petri.Application.Debugger;

namespace Petri.Application.GUI.Debugger
{
    /// <summary>
    /// The GUI of the petri net debugger.
    /// </summary>
    public class DebugGui : Gui
    {
        /// <summary>
        /// Initializes the <see cref="T:Petri.Application.GUI.Debugger.DebugGui"/> class.
        /// </summary>
        static DebugGui()
        {
            Pixbuf buf = Pixbuf.LoadFromResource("fix");
            IconTheme.AddBuiltinIcon("Fix", buf.Width, buf);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.Debugger.DebugGui"/> class.
        /// </summary>
        /// <param name="doc">Document.</param>
        /// <param name="controller">The debug controller.</param>
        /// <param name="window">The document's window.</param>
        public DebugGui(DebuggableGUIDocument doc, DebugController controller, MainWindow window)
        {
            _document = doc;
            _window = window;
            _controller = controller;

            _attachDetach = new MenuToolButton(Stock.Network);
            var menu = new Menu();
            _attachInternal = new MenuItem(Configuration.GetLocalized("Attach to the editor"));
            _attachInternal.Activated += async (sender, e) => await PerformAttach(DebugMode.RunInEditor);
            _attachExternal = new MenuItem(Configuration.GetLocalized("Attach to an external debugger"));
            _attachExternal.Activated += async (sender, e) => await PerformAttach(DebugMode.Attach);
            _createHost = new MenuItem(Configuration.GetLocalized("Create an external host and attach"));
            _createHost.Activated += async (sender, e) => await PerformAttach(DebugMode.CreateHost);
            menu.Append(_attachInternal);
            menu.Append(_attachExternal);
            menu.Append(_createHost);
            menu.ShowAll();
            _attachDetach.Menu = menu;
            _attachDetach.ShowMenu += (o, args) => {
                _attachInternal.Sensitive = Client.CurrentSessionState == DebugClient.SessionState.Stopped;
                _attachExternal.Sensitive = Client.CurrentSessionState == DebugClient.SessionState.Stopped;
                _createHost.Sensitive = Client.CurrentSessionState == DebugClient.SessionState.Stopped;
            };

            _startStopPetri = new ToolButton(Stock.MediaPlay);
            _playPause = new ToolButton(Stock.MediaPause);
            _reload = new ToolButton("Fix");
            _reload.IconName = "Fix";
            _reload.TooltipMarkup = GUIApplication.SafeMarkupFromLocalized("Fix");
            _syncWithEditor = new ToolButton(Stock.Refresh);
            _syncWithEditor.TooltipMarkup = GUIApplication.SafeMarkupFromLocalized("Sync");
            _exit = new ToolButton(Stock.Quit);
            _exit.TooltipMarkup = GUIApplication.SafeMarkupFromLocalized("End Session");
            _switchToEditor = new ToolButton(Stock.Edit);
            _switchToEditor.TooltipMarkup = GUIApplication.SafeMarkupFromLocalized("Editor");
            _zoomIn = new ToolButton(Stock.ZoomIn);
            _zoomIn.TooltipMarkup = GUIApplication.SafeMarkupFromLocalized("Zoom In");
            _zoomOut = new ToolButton(Stock.ZoomOut);
            _zoomOut.TooltipMarkup = GUIApplication.SafeMarkupFromLocalized("Zoom Out");

            _attachDetach.Clicked += OnClick;
            _startStopPetri.Clicked += OnClick;
            _playPause.Clicked += OnClick;
            _reload.Clicked += OnClick;
            _syncWithEditor.Clicked += OnClick;
            _exit.Clicked += OnClick;
            _switchToEditor.Clicked += OnClick;
            _zoomIn.Clicked += OnClick;
            _zoomOut.Clicked += OnClick;

            _toolbar.Insert(_attachDetach, -1);
            _toolbar.Insert(_exit, -1);

            _toolbar.Insert(new SeparatorToolItem(), -1);

            _toolbar.Insert(_startStopPetri, -1);
            _toolbar.Insert(_playPause, -1);
            _toolbar.Insert(_reload, -1);
            _toolbar.Insert(_syncWithEditor, -1);

            _toolbar.Insert(new SeparatorToolItem(), -1);

            _toolbar.Insert(_zoomOut, -1);
            _toolbar.Insert(_zoomIn, -1);

            if(!GUIApplication.IsReadOnly) {
                _toolbar.Insert(new SeparatorToolItem(), -1);
                _toolbar.Insert(_switchToEditor, -1);
            }

            HPaned.SizeRequested += (object o, SizeRequestedArgs args) => {
                HPaned p = (HPaned)o;
                if(p.Child2.Allocation.Width != 1) {
                    PropertiesPanel.Resize(p.Child2.Allocation.Width);
                } else {
                    // Account for some kind of size issues with this GUI not being shown at application startup
                    int x, y;
                    _window.GetSize(out x, out y);
                    p.Position = x - 250;
                    PropertiesPanel.Resize(200);
                }
            };

            _petriView = new DebugView(_document, _controller, window);
            PropertiesPanel = new DebugPropertiesPanel(_document, null, _controller, window, this);
            _petriView.SelectionUpdated += e => {
                PropertiesPanel = new DebugPropertiesPanel(_document,
                                                           _petriView.SelectedEntity,
                                                           _controller,
                                                           window,
                                                           this);
            };

            var viewport = new Viewport();
            viewport.Add(_petriView.DrawingArea);

            _petriView.DrawingArea.SizeRequested += (o, args) => {
                viewport.WidthRequest = viewport.Child.Requisition.Width;
                viewport.HeightRequest = viewport.Child.Requisition.Height;
            };

            _scroll.Add(viewport);
            VPaned.Pack1(_scroll, true, true);
        }

        /// <summary>
        /// Attaches to the debugger, temporarily overriding the debug mode.
        /// </summary>
        /// <param name="mode">The debug mode.</param>
        async Task PerformAttach(DebugMode mode)
        {
            var old = _document.Settings.CurrentProfile.DebugMode;
            try {
                _document.Settings.CurrentProfile.DebugMode = mode;
                await Client.Attach();

            } catch(Exception e) {
                _document.NotifyUnrecoverableError(e.Message);
            }
            _document.Settings.CurrentProfile.DebugMode = old;
        }

        /// <summary>
        /// Gets or sets the debug editor.
        /// </summary>
        /// <value>The debug editor.</value>
        public DebugPropertiesPanel PropertiesPanel {
            get;
            set;
        }

        /// <summary>
        /// Gets the properties panel.
        /// </summary>
        /// <value>The base properties panel.</value>
        public override PropertiesPanel.PropertiesPanel BasePropertiesPanel {
            get {
                return PropertiesPanel;
            }
        }

        /// <summary>
        /// Gets the petri view of the debugger.
        /// </summary>
        /// <value>The view.</value>
        public DebugView View {
            get {
                return _petriView;
            }
        }

        /// <summary>
        /// Gets the petri view.
        /// </summary>
        /// <value>The base view.</value>
        public override GUIPetriView BaseView {
            get {
                return View;
            }
        }

        /// <summary>
        /// Called when an expression has been evaluated
        /// </summary>
        /// <param name="result">Result.</param>
        /// <param name="userInfo">Additional user info.</param>
        public void OnEvaluate(string result, string userInfo)
        {
            PropertiesPanel.OnEvaluate(result, userInfo);
        }

        /// <summary>
        /// Manages a toolbar click.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">E.</param>
        public async void OnClick(object sender, EventArgs e)
        {
            try {
                if(sender == _attachDetach || sender == _window.AttachDetachItem) {
                    if(Client.CurrentSessionState != DebugClient.SessionState.Stopped) {
                        Client.Detach();
                    } else {
                        await Client.Attach();
                    }
                } else if(sender == _window.CreateHostItem) {
                    await CreateHost();
                } else if(sender == _startStopPetri || sender == _window.StartStopPetriItem) {
                    if(Client.PetriAlive) {
                        Client.StopPetri();
                    } else if(Client.CurrentPetriState == DebugClient.PetriState.Stopped) {
                        Client.StartPetri(_controller.PetriNetArgs);
                    }
                } else if(sender == _playPause || sender == _window.PlayPauseItem) {
                    Client.SetPause(Client.CurrentPetriState == DebugClient.PetriState.Started);
                } else if(sender == _reload || sender == _window.ReloadItem) {
                    await Client.ReloadPetri();
                } else if(sender == _syncWithEditor || sender == _window.SyncWithEditorItem) {
                    await _controller.SyncPetriNets();
                } else if(sender == _switchToEditor) {
                    _window.SwitchToEditor();
                } else if(sender == _exit || sender == _window.ExitItem) {
                    Client.StopSession();
                } else if(sender == _zoomIn) {
                    _petriView.Zoom /= 0.8f;
                    if(_petriView.Zoom > 8f) {
                        _petriView.Zoom = 8f;
                    }
                    _petriView.Redraw();
                } else if(sender == _zoomOut) {
                    _petriView.Zoom *= 0.8f;
                    if(_petriView.Zoom < 0.01f) {
                        _petriView.Zoom = 0.01f;
                    }
                    _petriView.Redraw();
                } else if(sender == _window.ClearBreakpointsItem) {
                    _controller.ClearBreakpoints();
                }
            } catch(Exception except) {
                _document.NotifyUnrecoverableError(except.Message);
            }
        }

        /// <summary>
        /// Creates an external host for the debugger.
        /// </summary>
        async Task CreateHost()
        {
            var d = new MessageDialog(_window,
                                      DialogFlags.Modal,
                                      MessageType.Question,
                                      ButtonsType.None,
                                      Configuration.GetLocalized("Please enter the required information to create a new host:"));
            d.AddButton(Configuration.GetLocalized("Cancel"), ResponseType.Cancel);
            var create = d.AddButton(Configuration.GetLocalized("Create host"), ResponseType.Accept);
            create.GrabFocus();

            var vbox = new VBox(false, 5);
            d.VBox.PackStart(vbox, false, false, 0);
            var exec = new Entry(Configuration.LastDebuggerHost);
            var args = new Entry(Configuration.LastDebuggerHostParams);

            var hbox = new HBox(false, 5);
            hbox.PackStart(GUIApplication.LabelFromString(Configuration.GetLocalized("Executable's path:")),
                           false,
                           false,
                           0);
            vbox.PackStart(hbox, false, false, 0);
            hbox = new HBox(false, 5);
            hbox.PackStart(exec, true, true, 0);
            var choose = new Button(GUIApplication.LabelFromString(Configuration.GetLocalized("Choose…"), true));
            hbox.PackStart(choose, false, false, 0);
            vbox.PackStart(hbox, false, false, 0);
            hbox = new HBox(false, 5);
            hbox.PackStart(GUIApplication.LabelFromString(Configuration.GetLocalized("Executable's arguments:")),
                           false,
                           false,
                           0);
            vbox.PackStart(hbox, false, false, 0);
            vbox.PackEnd(args, false, false, 0);

            choose.Clicked += (object sender, EventArgs e) => {
                var fc = new FileChooserDialog(Configuration.GetLocalized("Choose an executable…"), _window,
                                               FileChooserAction.Open,
                                               new object[] {Configuration.GetLocalized("Cancel"), ResponseType.Cancel,
                    Configuration.GetLocalized("Open"), ResponseType.Accept
                });

                if(GUIApplication.RunModalDialog(fc) == ResponseType.Accept) {
                    exec.Text = fc.Filename;
                }
                fc.Destroy();
            };

            d.ShowAll();

            if(GUIApplication.RunModalDialog(d) == ResponseType.Accept) {
                string name = exec.Text;
                string arguments = args.Text;

                d.Destroy();

                Configuration.LastDebuggerHost = name;
                Configuration.LastDebuggerHostParams = arguments;

                await Client.CreateHostAndAttach(name, arguments);
            } else {
                d.Destroy();
            }
        }

        /// <summary>
        /// Gets the debug client.
        /// </summary>
        /// <value>The client.</value>
        protected DebugClient Client {
            get {
                return _controller.Client;
            }
        }

        /// <summary>
        /// Sets a value indicating whethera petri net is undergoing a compilation process.
        /// </summary>
        /// <value><c>true</c> if a compilation process is ongoing; otherwise, <c>false</c>.</value>
        public bool IsCompiling {
            set {
                GUIApplication.RunOnUIThread(() => {
                    if(value) {
                        _reload.Sensitive = false;
                    } else {
                        _reload.Sensitive = true;
                    }
                });
            }
        }

        /// <summary>
        /// Updates the menu items and toolbar items.
        /// </summary>
        public override void Update()
        {
            base.Update();

            _window.EmbedItem.Sensitive = false;
            _window.ImportPetriNetItem.Sensitive = false;
            _window.SyncWithEditorItem.Sensitive = _document.NeedsSync;

            GUIApplication.RunOnUIThread(() => {
                if(_controller.Client.CurrentSessionState == DebugClient.SessionState.Started) {
                    _attachDetach.StockId = Stock.Disconnect;
                    if(_controller.Client.PetriAlive) {
                        PropertiesPanel.EvaluateSensitive = false;
                        _startStopPetri.StockId = Stock.Stop;
                    }

                    switch(_controller.Client.CurrentPetriState) {
                    case DebugClient.PetriState.Stopping:
                        break;
                    case DebugClient.PetriState.Starting:
                    case DebugClient.PetriState.Stopped:
                        _startStopPetri.StockId = Stock.MediaPlay;
                        _playPause.StockId = Stock.MediaPause;
                        PropertiesPanel.EvaluateSensitive = true;
                        break;
                    case DebugClient.PetriState.Pausing:
                        goto case DebugClient.PetriState.Started;
                    case DebugClient.PetriState.Started:
                        PropertiesPanel.EvaluateSensitive = false;
                        _playPause.StockId = Stock.MediaPause;
                        break;
                    case DebugClient.PetriState.Paused:
                        PropertiesPanel.EvaluateSensitive = true;
                        _playPause.StockId = Stock.MediaPlay;
                        break;
                    }
                } else {
                    _attachDetach.StockId = Stock.Network;
                    _startStopPetri.StockId = Stock.MediaPlay;
                    PropertiesPanel.EvaluateSensitive = false;
                    _playPause.StockId = Stock.MediaPause;
                }

                Action<MenuItem, ToolButton, bool> reflectMenuItem = (menu, button, updateName) => {
                    button.Sensitive = menu.Sensitive;
                    if(updateName) {
                        button.TooltipMarkup = GUIApplication.SafeMarkupFromString(
                            (menu.Child as Label).Text
                        );
                    }
                };

                reflectMenuItem(_window.AttachDetachItem, _attachDetach, true);
                reflectMenuItem(_window.StartStopPetriItem, _startStopPetri, true);
                reflectMenuItem(_window.PlayPauseItem, _playPause, true);
                reflectMenuItem(_window.ExitItem, _exit, true);
                reflectMenuItem(_window.ReloadItem, _reload, true);
                reflectMenuItem(_window.SyncWithEditorItem, _syncWithEditor, false);
            });
        }

        DebugView _petriView;
        MenuToolButton _attachDetach;
        ToolButton _startStopPetri, _playPause, _reload, _syncWithEditor, _exit, _zoomIn, _zoomOut, _switchToEditor;
        MenuItem _attachInternal, _attachExternal, _createHost;

        DebuggableGUIDocument _document;
        MainWindow _window;
        DebugController _controller;
    }
}

