//
//  DebuggableGUIDocument.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2017-08-18.
//

using System.Collections.Generic;
using System.Threading.Tasks;

using Gtk;

using Petri.Application.Debugger;

using VariablesList = System.Collections.Generic.List<System.Tuple<Petri.Model.RootPetriNet, Petri.Code.VariableExpression, string>>;

namespace Petri.Application.GUI.Debugger
{
    /// <summary>
    /// A document with all the GUI attached to it, including the editor and the debugger.
    /// </summary>
    public class DebuggableGUIDocument : LocalDocument, IDebuggable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.Debugger.DebuggableGUIDocument"/> class.
        /// </summary>
        /// <param name="mainDocument">The GUI document.</param>
        public DebuggableGUIDocument(GUIDocument mainDocument) : base(mainDocument.Settings.Language)
        {
            MainDocument = mainDocument;
            Sync();
        }

        /// <summary>
        /// Gets the base debug controller.
        /// </summary>
        /// <value>The base debug controller.</value>
        public Petri.Application.Debugger.DebugController BaseDebugController {
            get {
                return MainDocument.DebugController;
            }
        }

        /// <summary>
        /// Gets or sets the text output manager.
        /// </summary>
        /// <value>The output.</value>
        public override IOutput Output {
            get {
                return MainDocument.Output;
            }
            protected set {

            }
        }

        /// <summary>
        /// Called when the state of the petri net or the session is changed.
        /// </summary>
        public void NotifyStateChanged()
        {
            GUIApplication.RunOnUIThread(() => {
                if(MainDocument.Window.Visible) {
                    MainDocument.Window.UpdateUI();
                    MainDocument.Window.VariablesViewer.Update();
                    MainDocument.Window.DebugGui.View.Redraw();

                    if(BaseDebugController.Client.CurrentSessionState == DebugClient.SessionState.Started) {
                        MainDocument.Window.SwitchToDebug();
                    }
                }
            });
        }

        /// <summary>
        /// Notifies the user from an unrecoverable error.
        /// </summary>
        /// <param name="message">The error message.</param>
        public void NotifyUnrecoverableError(string message)
        {
            GUIApplication.RunOnUIThread(() => {
                MainDocument.NotifyError(message);
            });
        }

        /// <summary>
        /// Notifies a new status message.
        /// </summary>
        /// <param name="message">Message.</param>
        public void NotifyStatusMessage(string message)
        {
            if(!MainDocument.IsClosing) {
                GUIApplication.RunOnUIThread(() => {
                    MainDocument.Window.AppendConsoleContent(ANSIColorParser.LightGrayText
                                                + message
                                                + ANSIColorParser.ResetText + "\n");
                });
            }
        }

        /// <summary>
        /// Notifies that a code expression was successfully evaluated.
        /// </summary>
        /// <param name="value">The evaluation result.</param>
        /// <param name="userInfo">Additional user info.</param>
        public void NotifyEvaluated(string value, string userInfo)
        {
            GUIApplication.RunOnUIThread(() => {
                MainDocument.Window.DebugGui.OnEvaluate(value, userInfo);
            });
        }

        /// <summary>
        /// Notifies that the value of the petri net's variables have just been retrieved.
        /// </summary>
        /// <param name="variables">The list of key-value pairs.</param>
        public void NotifyVariables(VariablesList variables)
        {
            GUIApplication.RunOnUIThread(() => {
                MainDocument.Window.VariablesViewer.UpdateWithVars(variables);
            });
        }

        /// <summary>
        /// Notifies an error in the debug server.
        /// </summary>
        /// <param name="message">Message.</param>
        public void NotifyServerError(string message)
        {
            GUIApplication.RunOnUIThread(() => {
                var d = new MessageDialog(MainDocument.Window,
                                          DialogFlags.Modal,
                                          MessageType.Error,
                                          ButtonsType.None,
                                          GUIApplication.SafeMarkupFromLocalized("An error occurred in the debugger: {0}",
                                                                                 message));
                d.AddButton(Configuration.GetLocalized("Cancel"), ResponseType.Cancel);
                if(message == "You are trying to run a Petri net that is different from the one which is compiled!") {
                    d.AddButton(Configuration.GetLocalized("Fix and run"), ResponseType.Accept);
                }
                if(GUIApplication.RunModalDialog(d) == ResponseType.Accept) {
                    Application.SwallowAsync(BaseDebugController.Client.ReloadPetri(true));
                }
                d.Destroy();
            });
        }

        /// <summary>
        /// Notifies the user that the current action will cause the debug server's process to be orphaned, and aks him
        /// whether to continue or not.
        /// </summary>
        /// <returns><c>true</c>, if the process should be orphaned, <c>false</c> if the current action should be cancelled.</returns>
        public bool ShouldOrphanProcess()
        {
            return MainDocument.Window.PromptYesNo(
                Configuration.GetLocalized("The current action will cause the debug server's process to be orphaned. It will keep running, even after this debugger exits, and will not be reachable by the petri net debugger.")
                    + "\n"
                    + Configuration.GetLocalized("Do you want the debug server process to be orphaned anyway?"),
                Configuration.GetLocalized("Continue")
            ).Result;
        }

        /// <summary>
        /// Notifies a new state have been activated in the petri net, or a state have been deactivated.
        /// </summary>
        public void NotifyActiveStatesChanged()
        {
            MainDocument.Window.DebugGui.View.Redraw();
        }

        /// <summary>
        /// Asks the user if regenerating the dynamic library should be attempted.
        /// </summary>
        /// <returns><c>true</c>, if we want to reload the lib, <c>false</c> otherwise.</returns>
        public async Task<bool> ShouldAttemptDylibReload()
        {
            GUIApplication.RunOnUIThread(() => {
                lock(this) {
                    if(_attachCancelDialog != null) {
                        _attachCancelDialog.Respond(ResponseType.Ok);
                        _attachCancelDialog.Destroy();
                        _attachCancelDialog = null;
                    }
                }
            });

            return await MainDocument.Window.PromptYesNo(
                Configuration.GetLocalized("Unable to load the dynamic library! Try to compile it again.")
                    + "\n"
                    + Configuration.GetLocalized("Do you want to try to recompile the petri net?"),
                Configuration.GetLocalized("Fix")
            );
        }

        /// <summary>
        /// Invoked when an attempt will be made to attach to a debug host.
        /// </summary>
        public void OnStartAttachAttempt()
        {
            _attachInterrupted = false;

            Application.SwallowAsync(Task.Factory.StartNew(() => {
                GUIApplication.RunOnUIThread(() => {
                    _attachCancelDialog = new MessageDialog(MainDocument.Window,
                                            DialogFlags.Modal,
                                            MessageType.Info,
                                            ButtonsType.None,
                                            GUIApplication.SafeMarkupFromLocalized("Waiting for the process to launch the debug server…"));
                    _attachCancelDialog.AddButton(Configuration.GetLocalized("Cancel"), ResponseType.Cancel);


                    if(GUIApplication.RunModalDialog(_attachCancelDialog) == ResponseType.Cancel) {
                        lock(this) {
                            _attachInterrupted = true;
                            _attachCancelDialog.Destroy();
                            _attachCancelDialog = null;
                        }
                    }
                });
            }));
        }

        /// <summary>
        /// Invoked when a previously start attempt to attach to a debug host comes to and end.
        /// </summary>
        /// <param name="success">If set to <c>true</c> then the attempt was successful.</param>
        public void OnAttachAttemptEnd(bool success)
        {
            Application.SwallowAsync(() => {
                GUIApplication.RunOnUIThread(() => {
                    lock(this) {
                        if(_attachCancelDialog != null) {
                            _attachCancelDialog.Respond(ResponseType.Ok);
                            _attachCancelDialog.Destroy();
                            _attachCancelDialog = null;
                        }
                    }
                });
            });
        }

        /// <summary>
        /// Whether to interrupt a debugger attaching procedure
        /// </summary>
        /// <returns><c>true</c>, if attach was interrupted, <c>false</c> otherwise.</returns>
        public bool InterruptAttach()
        {
            return _attachInterrupted;
        }

        /// <summary>
        /// Shows the compilation status to the user.
        /// </summary>
        /// <param name="status">The compilation success status.</param>
        /// <param name="output">Output.</param>
        public override void EchoCompilationStatus(Compiler.CompilationStatus status, string output)
        {
            MainDocument.EchoCompilationStatus(status, output);
        }

        /// <summary>
        /// Notifies an error to the user.
        /// </summary>
        /// <param name="error">Error.</param>
        public override void NotifyError(string error)
        {
            MainDocument.NotifyError(error);
        }

        /// <summary>
        /// Notifies a warning message to the user.
        /// </summary>
        /// <param name="warning">The message.</param>
        public override void NotifyWarning(string warning)
        {
            MainDocument.NotifyWarning(warning);
        }

        /// <summary>
        /// Notifies some information to the user.
        /// </summary>
        /// <param name="info">info.</param>
        public override void NotifyInfo(string info)
        {
            MainDocument.NotifyInfo(info);
        }

        /// <summary>
        /// Gets the main document, i.e. the document containing the GUI.
        /// </summary>
        /// <value>The main document.</value>
        public GUIDocument MainDocument {
            get;
            private set;
        }

        /// <summary>
        /// Syncs this document with the content of its main document.
        /// </summary>
        public void Sync()
        {
            GUIApplication.RunOnUIThread(() => {
                Path = MainDocument.Path;
                LoadFromXml(MainDocument.GetXml());
                Settings = MainDocument.Settings;
                ExternalsInSync = true;
                IssuesChecker.CheckIssues(PetriNet);

                // init
                if(BaseDebugController != null) {
                    var breakpoints = new List<Model.Action>(BaseDebugController.Breakpoints);
                    var temp = BaseDebugController.TemporaryBreakpoint;
                    var newBreakpoints = new List<Model.Action>();
                    foreach(var bp in breakpoints) {
                        var newBp = PetriNet.EntityFromID(bp.GlobalID);
                        if(newBp is Model.Action) {
                            newBreakpoints.Add((Model.Action)newBp);
                        }
                    }

                    BaseDebugController.ReplaceBreakpoints(
                        newBreakpoints,
                        temp != null ? PetriNet.EntityFromID(temp.GlobalID) as Model.Action : null
                    );
                }

                if(MainDocument.Window != null) {
                    MainDocument.Window.DebugGui.View.GoToRoot();
                    MainDocument.Window.VariablesViewer.Update();
                    MainDocument.Window.DebugGui.Update();
                }
            });
        }

        /// <summary>
        /// Returns whether this debug document has the same contents as its main document.
        /// </summary>
        /// <returns><c>true</c>, if in sync, <c>false</c> otherwise.</returns>
        public bool FirstLevelInSync {
            get {
                return MainDocument.Hash == Hash;
            }
        }

        /// <summary>
        /// Returns whether this document's external petri nets are in sync with the main document's.
        /// </summary>
        /// <returns><c>true</c>, if in sync, <c>false</c> otherwise.</returns>
        public bool ExternalsInSync {
            get;
            set;
        }

        /// <summary>
        /// Returns whether this debug document has been updated from the main document, even if it does not require a sync.
        /// </summary>
        /// <returns><c>true</c>, if updated, <c>false</c> otherwise.</returns>
        public bool NeedsSync {
            get {
                return !FirstLevelInSync || !ExternalsInSync || MainDocument.GetXmlString() != GetXmlString();
            }
        }

        volatile bool _attachInterrupted;
        MessageDialog _attachCancelDialog;
    }
}
