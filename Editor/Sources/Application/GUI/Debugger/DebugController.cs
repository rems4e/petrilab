//
//  DebugController.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-01.
//

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Petri.Application.Debugger;

namespace Petri.Application.GUI.Debugger
{
    /// <summary>
    /// The GUI debug controller.
    /// </summary>
    public class DebugController : Petri.Application.Debugger.DebugController
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.Debugger.DebugController"/> class.
        /// </summary>
        /// <param name="doc">Document.</param>
        public DebugController(DebuggableGUIDocument doc) : base(new PetriNetProvider(doc), new DebugClient(doc, doc))
        {
            _document = doc;

            ActiveStatesChanged += (sender, e) => {
                if(DateTime.Now - _lastVarRequest > new TimeSpan(0, 0, 0, 0, 50)) {
                    _lastVarRequest = DateTime.Now;
                    Client.RequestVariables();
                }
            };
        }

        /// <summary>
        /// Gets the arguments that should be passed as parameters when the petri net is run.
        /// </summary>
        /// <value>The petri net arguments.</value>
        public Dictionary<UInt32, Int64> PetriNetArgs {
            get {
                var overriden = _document.MainDocument.Window.VariablesViewer.OverridenValues;
                var result = new Dictionary<UInt32, Int64>();

                foreach(var kvp in overriden) {
                    if(kvp.Value != null) {
                        int index = 0;
                        foreach(var k in PetriNet.Parameters) {
                            if(k == kvp.Key) {
                                break;
                            }
                            ++index;
                        }
                        result.Add((UInt32)index, Int64.Parse(kvp.Value));
                    }
                }

                return result;
            }
        }

        /// <summary>
        /// Syncs the debug petri net with the editor's petri net.
        /// </summary>
        public async Task SyncPetriNets()
        {
            bool doSync = true;

            var window = _document.MainDocument.Window;
            if(Client.CurrentPetriState != DebugClient.PetriState.Stopped) {
                doSync = await window.PromptYesNo(
                    Configuration.GetLocalized("Synchronizing the petri net requires to stop the current petri net's execution.")
                        + "\n"
                        + Configuration.GetLocalized("Do you want to stop execution and sync anyway?"),
                    Configuration.GetLocalized("Sync")
                );

                if(doSync) {
                    Client.StopPetri();
                    doSync = await window.InterruptibleWaitFor(
                        Configuration.GetLocalized("Waiting for the petri net execution to stop…"),
                        () => {
                            return Client.CurrentPetriState == DebugClient.PetriState.Stopped;
                        }
                    );
                }
            }

            if(doSync) {
                _document.Sync();
            }
        }

        DebuggableGUIDocument _document;
        DateTime _lastVarRequest = DateTime.MinValue;
    }
}
