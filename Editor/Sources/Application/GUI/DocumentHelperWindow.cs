//
//  DocumentHelperWindow.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-04-28.
//

using Gtk;

namespace Petri.Application.GUI
{
    /// <summary>
    /// A window that is attached to a document.
    /// </summary>
    public abstract class DocumentHelperWindow : DocumentWindow
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.DocumentHelperWindow"/> class.
        /// </summary>
        /// <param name="doc">The document of the window.</param>
        /// <param name="width">The initial width of the window.</param>
        /// <param name="height">The initial height of the window.</param>
        /// <param name="noMenuBar"><c>true</c> if we don't want a menu bar to be attached to the window.</param>
        protected DocumentHelperWindow(GUIDocument doc, int width, int height, bool noMenuBar = false) : base(doc, noMenuBar)
        {
            DefaultWidth = width;
            DefaultHeight = height;

            KeyPressEvent += (object o, KeyPressEventArgs args) => {
                if(args.Event.Key == Gdk.Key.Escape) {
                    TryClose();
                }
            };

            SetPosition(WindowPosition.Center);
            int x, y;
            GetPosition(out x, out y);
            Move(x, 2 * y / 3);
        }
    }
}

