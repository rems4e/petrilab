//
//  FunctionsBrowser.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2015-01-20.
//

using Gtk;

namespace Petri.Application.GUI
{
    /// <summary>
    /// The list of known functions of the document.
    /// </summary>
    public class FunctionsBrowser : DocumentHelperWindow
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.FunctionsBrowser"/> class.
        /// </summary>
        /// <param name="doc">Document.</param>
        public FunctionsBrowser(GUIDocument doc) : base(doc, 600, 600)
        {
            var scrolledWindow = new ScrolledWindow();
            scrolledWindow.SetPolicy(PolicyType.Automatic, PolicyType.Automatic);

            _vbox.Add(scrolledWindow);

            _table = new TreeView();
            scrolledWindow.Add(_table);

            {
                var c = new TreeViewColumn();
                c.Title = Configuration.GetLocalized("Function");
                var nameCell = new CellRendererText();
                c.PackStart(nameCell, true);
                c.AddAttribute(nameCell, "text", 0);
                _table.AppendColumn(c);
            }

            _dataStore = new ListStore(typeof(string), typeof(string));
            _table.Model = _dataStore;
        }

        /// <summary>
        /// Gets the string that should be used as the window's title.
        /// </summary>
        /// <value>The current title.</value>
        public override string CurrentTitle {
            get {
                return Configuration.GetLocalized("Functions of {0}", Document.Settings.Name);
            }
        }

        /// <summary>
        /// Updates this list of known functions.
        /// </summary>
        public override void UpdateUI()
        {
            base.UpdateUI();
            _dataStore.Clear();
            foreach(var section in Document.AllFunctions) {
                foreach(var f in section.Value) {
                    var h = "";
                    if(f.Header != null) {
                        h = System.IO.Path.GetFileName(f.Header);
                        h = " - " + h;
                    }
                    _dataStore.AppendValues(f.Prototype + h);
                }
            }
        }

        TreeView _table;
        ListStore _dataStore;
    }
}
