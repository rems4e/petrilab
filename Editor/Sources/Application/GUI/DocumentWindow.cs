//
//  DocumentWindow.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-01.
//

using System.Threading.Tasks;

using Gtk;

using Petri.Application.Debugger;

namespace Petri.Application.GUI
{
    /// <summary>
    /// A window that is attached to a document.
    /// </summary>
    public abstract class DocumentWindow : MenuBarWindow
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.DocumentWindow"/> class.
        /// </summary>
        /// <param name="doc">The document of the window.</param>
        /// <param name="noMenuBar"><c>true</c> if we don't want a menu bar to be attached to the window.</param>
        protected DocumentWindow(GUIDocument doc, bool noMenuBar = false) : base(noMenuBar)
        {
            Document = doc;

            if(!GUIApplication.IsReadOnly) {
                _saveItem.Sensitive = true;
                _saveAsItem.Sensitive = true;
            }
            foreach(var item in _documentMenu.Children) {
                item.Sensitive = true;
            }

            if(GUIApplication.IsReadOnly) {
                _showEditorItem.Sensitive = false;
                _showDebuggerItem.Sensitive = false;
            }

            _showVarItem.Sensitive = true;
            _profilesItem.Sensitive = true;
            BorderWidth = 15;

            UndoManager = new UndoManager();
            UndoManager.ActionApplied += (manager, e) => {
                Document.InvalidateHash();
                Document.InvalidateXml();
                UpdateUI();
            };

            FocusInEvent += (o, args) => {
                UpdateUI();
            };

            Document.AssociatedWindows.Add(this);
        }

        /// <summary>
        /// Gets the document of the window.
        /// </summary>
        /// <value>The document.</value>
        public GUIDocument Document {
            get;
            private set;
        }

        /// <summary>
        /// Gets the string that should be used as the window's title.
        /// </summary>
        /// <value>The current title.</value>
        public abstract string CurrentTitle {
            get;
        }

        /// <summary>
        /// The window's undo manager.
        /// </summary>
        /// <value>The undo manager.</value>
        public UndoManager UndoManager {
            get;
            private set;
        }

        /// <summary>
        /// Gets a value indicating whether this <see cref="Petri.Application.GUI.DocumentWindow"/> has been  modified since its last save.
        /// </summary>
        /// <value><c>true</c> if modified; otherwise, <c>false</c>.</value>
        public bool Modified {
            get {
                return _guiActionToMatchSave != UndoManager.NextUndo;
            }
        }

        /// <summary>
        /// Sets the undo stack in a state that appears to be unmodified since last save.
        /// </summary>
        public void ForceUnmodified()
        {
            _guiActionToMatchSave = UndoManager.NextUndo;
        }

        /// <summary>
        /// Commits the currently focused widget's value.
        /// </summary>
        public void CommitCurrentWidget()
        {
            if(Focus is Entry) {
                Focus = DefaultFocus;
            }
        }

        /// <summary>
        /// Gets the default focused widget when we want to change selection.
        /// </summary>
        /// <value>The default focus.</value>
        protected virtual Widget DefaultFocus => null;

        /// <summary>
        /// Called when any of the non-static menu item is selected from the menu bar.
        /// </summary>
        /// <param name="sender">Sender.</param>
        protected override async Task HandleMenuClicked(object sender)
        {
            await base.HandleMenuClicked(sender);

            if(sender == _saveItem) {
                Document.Save();
            } else if(sender == _saveAsItem) {
                Document.SaveAs();
            } else if(sender == _exportItem) {
                Document.ExportAsPDF();
            } else if(sender == _revertItem) {
                Document.Restore();
            } else if(sender == _generateItem) {
                Document.GenerateCodeEnsureOutputPath();
            } else if(sender == _compileItem) {
                await Document.Compile();
            } else if(sender == _deployItem) {
                await Document.Deploy();
            } else if(sender == _showDocumentItem) {
                GUIApplication.OpenFileExplorer(System.IO.Directory.GetParent(Document.Path).FullName);
            } else if(sender == _showCodeItem) {
                GUIApplication.OpenFileExplorer(
                    PathUtility.GetFullPath(
                        System.IO.Path.Combine(
                            System.IO.Directory.GetParent(Document.Path).FullName,
                            Document.Settings.CurrentProfile.RelativeSourceOutputPath
                        )
                    )
                );
            } else if(sender == _showCompilerOutputItem) {
                GUIApplication.OpenFileExplorer(
                    PathUtility.GetFullPath(
                        System.IO.Path.Combine(
                            System.IO.Directory.GetParent(Document.Path).FullName,
                            Document.Settings.CurrentProfile.RelativeLibOutputPath
                        )
                    )
                );
            } else if(sender == _undoItem) {
                CommitCurrentWidget();
                UndoManager.Undo();
            } else if(sender == _redoItem) {
                CommitCurrentWidget();
                UndoManager.Redo();
            } else if(sender == _knownFunctionsItem) {
                Document.ShowKnownFunctions();
            } else if(sender == _manageHeadersItem) {
                Document.ManageHeaders();
            } else if(sender == _manageMacrosItem) {
                Document.ManageMacros();
            } else if(sender == _documentSettingsItem) {
                Document.EditSettings();
            } else if(sender == _showVarItem) {
                Document.Window.ShowVariables();
            }
        }

        /// <summary>
        /// Updates the state of the user interface (mainly menu items and toolbars).
        /// </summary>
        public virtual void UpdateUI()
        {
            _revertItem.Sensitive = Modified && Document.Path.Length > 0;
            _undoItem.Sensitive = UndoManager.NextUndo != null;
            _redoItem.Sensitive = UndoManager.NextRedo != null;
            (_undoItem.Child as Label).Text = UndoManager.NextUndo != null
                                                  ? Configuration.GetLocalized("Undo {0}", UndoManager.NextUndoDescription)
                                                  : Configuration.GetLocalized("Undo");
            (_redoItem.Child as Label).Text = UndoManager.NextRedo != null
                                                  ? Configuration.GetLocalized("Redo {0}", UndoManager.NextRedoDescription)
                                                  : Configuration.GetLocalized("Redo");
            _findItem.Sensitive = false;
            _showDocumentItem.Sensitive = true;
            _showCodeItem.Sensitive = true;
            _showCompilerOutputItem.Sensitive = true;
            _deployItem.Sensitive = Document.Settings.CurrentProfile.DeployMode != DeployMode.DoNothing;

            UpdateProfilesMenuItems();
            UpdateTitle();
        }

        /// <summary>
        /// Updates the profiles menu items.
        /// </summary>
        void UpdateProfilesMenuItems()
        {
            _profilesItem.Submenu = null;
            if(_profilesMenu != null) {
                _profilesMenu.Destroy();
            }

            _profilesMenu = new Menu();
            _profilesItem.Submenu = _profilesMenu;

            int i = 0;
            RadioMenuItem last = null;
            foreach(var profile in Document.Settings.Profiles) {
                int index = i++;
                RadioMenuItem item = last == null ? new RadioMenuItem("") : new RadioMenuItem(last, "");
                last = item;
                if(profile == Document.Settings.CurrentProfile) {
                    item.Active = true;
                }
                ((Label)item.Child).Text = profile.Name;
                item.Activated += (sender, e) => {
                    if(item.Active) {
                        if(Document.Settings.CurrentProfile != Document.Settings.Profiles[index]) {
                            ChangeProfile(Document.Settings.Profiles[index]).Wait();
                        }
                    }
                };
                _profilesMenu.Append(item);
                item.Show();
            }

            _profilesItem.ShowAll();
        }

        /// <summary>
        /// Enforces that the debug session is stopped if the user confirms, and change the profile to the argument.
        /// </summary>
        /// <param name="profile">The new profile.</param>
        /// <returns><c>true</c> if a change was performed, <c>false</c> otherwise.</returns>
        public async Task<bool> ChangeProfile(DocumentSettingsProfile profile)
        {
            bool doChange = true;

            var client = Document.DebugController.Client;
            if(client.CurrentSessionState != DebugClient.SessionState.Stopped) {
                doChange = await PromptYesNo(
                    Configuration.GetLocalized("Changing the current profile requires to stop the debugging session.")
                        + "\n"
                        + Configuration.GetLocalized("Do you want to stop the session and change profile anyway?"),
                    Configuration.GetLocalized("Change profile")
                );

                if(doChange) {
                    client.Detach();
                }
            }

            if(doChange) {
                Document.Settings.CurrentProfile = profile;
                UpdateUI();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Commits the GUI action and update the GUI accordingly (undo/redo submenus etc.).
        /// </summary>
        /// <param name="action">The action.</param>
        /// <param name="doNotFocus">If <c>true</c>, then no focus is given to the action.</param>
        public void CommitGuiAction(GuiAction action, bool doNotFocus = false)
        {
            UndoManager.CommitGuiAction(action, doNotFocus);
        }

        /// <summary>
        /// Prompts a yes-no question to the user using the given parameters.
        /// </summary>
        /// <returns>The user's choice.</returns>
        /// <param name="message">The question.</param>
        /// <param name="validateString">The label of the "yes" button.</param>
        /// <param name="cancelString">The label of the "no" button. <c>null</c> if a default is to be used.</param>
        public async Task<bool> PromptYesNo(string message,
                                            string validateString,
                                            string cancelString = null)
        {
            return await GUIApplication.RunOnUIThread(() => {
                cancelString = cancelString ?? Configuration.GetLocalized("Cancel");

                var d = new MessageDialog(this,
                                          DialogFlags.Modal,
                                          MessageType.Question,
                                          ButtonsType.None,
                                          GUIApplication.SafeMarkupFromString(message));
                d.AddButton(cancelString, ResponseType.Cancel);
                d.AddButton(validateString, ResponseType.Accept);

                var resp = GUIApplication.RunModalDialogAsync(d).Result;
                d.Destroy();

                return resp == ResponseType.Accept;
            });
        }

        /// <summary>
        /// Wait for a task to complete, but gives the ability to cancel the blocking wait.
        /// </summary>
        /// <returns><c>true</c> if the condition was met, or <c>false</c> if interrupted.</returns>
        /// <param name="message">The dialog's message.</param>
        /// <param name="condition">The condition to check.</param>
        public async Task<bool> InterruptibleWaitFor(string message, System.Func<bool> condition)
        {
            var ev = new System.Threading.ManualResetEvent(false);

            var dialog = new MessageDialog(this,
                                           DialogFlags.Modal,
                                           MessageType.Info,
                                           ButtonsType.None,
                                           GUIApplication.SafeMarkupFromString(message));
            dialog.AddButton(Configuration.GetLocalized("Cancel"), ResponseType.Cancel);

            var waitTask = Task.Factory.StartNew(() => {
                while(true) {
                    if(condition()) {
                        GUIApplication.RunOnUIThread(() => {
                            dialog.Destroy();
                        });
                        return true;
                    } else if(ev.WaitOne(0)) {
                        return false;
                    }

                    System.Threading.Thread.Sleep(50);
                }
            });

            Application.SwallowAsync(() => {
                GUIApplication.RunModalDialog(dialog);
                ev.Set();
            });

            return await waitTask;
        }

        /// <summary>
        /// Updates the title of the window to the <see cref="CurrentTitle"/> value.
        /// </summary>
        public void UpdateTitle()
        {
            var result = (Document.Modified ? "*" : "") + CurrentTitle;
            if(GUIApplication.IsReadOnly) {
                result += Configuration.GetLocalized(" - Read Only");
            }

            Title = result + string.Format(" ({0})", System.IO.Path.GetFileName(Document.Path));
        }

        GuiAction _guiActionToMatchSave = null;
    }
}

