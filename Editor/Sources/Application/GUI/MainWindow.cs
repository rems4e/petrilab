//
//  MainWindow.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2014-11-13.
//

using System.Threading.Tasks;

using Gtk;

using Petri.Application.Debugger;

namespace Petri.Application.GUI
{
    /// <summary>
    /// The main window of the document.
    /// </summary>
    public class MainWindow : DocumentWindow
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.MainWindow"/> class.
        /// </summary>
        /// <param name="doc">Document.</param>
        public MainWindow(GUIDocument doc) : base(doc)
        {
            UndoManager.ActionApplied += (manager, e) => {
                Document.InvalidateFlatPetriNet();

                SwitchToEditor();

                if(e.IsReplay) {
                    _editorGui.PropertiesPanel.Reset();
                }
            };

            WindowPosition = WindowPosition.CenterOnParent;
            AllowShrink = true;
            DefaultWidth = 1168;
            DefaultHeight = 730;

            if(GUIApplication.Documents.Count > 0) {
                GUIApplication.Documents[GUIApplication.Documents.Count - 1].Window.GetPosition(out int x, out int y);
                Move(x + 20, y + 42);
            } else {
                SetPosition(WindowPosition.Center);
                GetPosition(out int x, out int y);
                Move(x, 2 * y / 3);
            }

            _editorGui = new Editor.EditorGui(Document, this);
            _debugGui = new Debugger.DebugGui(Document.DebugDocument, doc.DebugController, this);

            _editorGui.View.SelectionUpdated += e => {
                GUIApplication.RunOnUIThread(() => {
                    // We need to perform a redraw event first as it computes the parent hierarchy there, and it is needed inside this call.
                    // It is expected that the view performs a redraw itself on this event.
                    UpdateUI();
                }, true);
            };

            if(Document.Settings.Language != Code.Language.EmbeddedC) {
                foreach(var menuItem in _debuggerMenu.Children) {
                    menuItem.Sensitive = true;
                }
            }

            _exportItem.Sensitive = true;

            ((AccelLabel)_generateItem.Child).Text = Configuration.GetLocalized("Generate <language> long",
                                                                                DocumentSettings.LanguageName(Document.Settings.Language));
            _generateItem.Sensitive = true;
            _compileItem.Sensitive = true;
            _deployItem.Sensitive = Document.Settings.CurrentProfile.DeployMode != DeployMode.DoNothing;

            _findItem.Sensitive = true;
            _actualSizeItem.Sensitive = true;
            _zoomInItem.Sensitive = true;
            _zoomOutItem.Sensitive = true;

            _clearConsoleItem.Sensitive = true;
            _lockConsoleItem.Sensitive = true;
            ((Label)_lockConsoleItem.Child).Text = Configuration.GetLocalized("Unlock Console Scrolling");

            _helperTabs = new Notebook();
            ConsoleOutputViewer = new ConsoleOutputViewer(_helperTabs, (Label)_lockConsoleItem.Child);
            SearchResultsViewer = new SearchResultsViewer(Document, _helperTabs);
            IssuesViewer = new IssuesViewer(_helperTabs);
            VariablesViewer = new VariablesViewer(Document.DebugController, _helperTabs);
        }

        /// <summary>
        /// Gets the string that should be used as the window's title.
        /// </summary>
        /// <value>The current title.</value>
        public override string CurrentTitle {
            get {
                return Document.Settings.Name + " - " + (Gui == EditorGui ?
                                                         Configuration.GetLocalized("Editor") :
                                                         Configuration.GetLocalized("Debugger"));
            }
        }

        /// <summary>
        /// Gets or sets the currently installed main view of the window (debugger, editor…).
        /// </summary>
        /// <value>The GUI.</value>
        public Gui Gui {
            get {
                return _gui;
            }
            set {
                if(_gui != null) {
                    _gui.BaseView.OnFocusChanged(new GUIPetriView.FocusChangedEventArgs(false));
                    _gui.Hide();
                    _vbox.Remove(_gui);
                    _gui.VPaned.Remove(_helperTabs);

                    value.VPaned.Position = _gui.VPaned.Position;
                    value.ShouldUpdatePosition = false;
                    Task.Run(async () => {
                        // Dirty hack… This is to acctually enforce common position between GUI VPaned, over the
                        // complicated GTK's requisition/allocation scheme.
                        await Task.Delay(500);
                        GUIApplication.RunOnUIThread(() => {
                            value.ShouldUpdatePosition = true;
                        });
                    });
                } else {
                    GetSize(out var _, out var h);
                    value.VPaned.Position = h - 300;
                }
                _gui = value;

                _vbox.PackEnd(_gui);

                _gui.BaseView.Redraw();
                _gui.BaseView.OnFocusChanged(new GUIPetriView.FocusChangedEventArgs(true));
                UpdateUI();
                _gui.ShowAll();

                _gui.Status = "";

                _gui.VPaned.Pack2(_helperTabs, false, false);
                _gui.BasePropertiesPanel.Reset();
                _gui.BaseView.Redraw();
            }
        }

        /// <summary>
        /// Gets the editor GUI.
        /// </summary>
        /// <value>The editor GUI.</value>
        public Editor.EditorGui EditorGui {
            get {
                return _editorGui;
            }
        }

        /// <summary>
        /// Gets the debug GUI.
        /// </summary>
        /// <value>The debug GUI.</value>
        public Debugger.DebugGui DebugGui {
            get {
                return _debugGui;
            }
        }

        /// <summary>
        /// Gets the "Cut" menu item of the menu bar.
        /// </summary>
        /// <value>The cut item.</value>
        public MenuItem CutItem {
            get {
                return _cutItem;
            }
        }

        /// <summary>
        /// Gets the "Copy" menu item of the menu bar.
        /// </summary>
        /// <value>The copy item.</value>
        public MenuItem CopyItem {
            get {
                return _copyItem;
            }
        }

        /// <summary>
        /// Gets the "Paste" menu item of the menu bar.
        /// </summary>
        /// <value>The paste item.</value>
        public MenuItem PasteItem {
            get {
                return _pasteItem;
            }
        }

        /// <summary>
        /// Gets the "Embed in macro" menu item of the menu bar.
        /// </summary>
        /// <value>The embed item.</value>
        public MenuItem EmbedItem {
            get {
                return _embedInMacroItem;
            }
        }

        /// <summary>
        /// Gets the import petri net menu item of the menu bar.
        /// </summary>
        /// <value>The import petri net item.</value>
        public MenuItem ImportPetriNetItem {
            get {
                return _importPetriNetItem;
            }
        }

        /// <summary>
        /// Gets the debugger's attach/detach menu item of the menu bar.
        /// </summary>
        /// <value>The attach/detach item.</value>
        public MenuItem AttachDetachItem {
            get {
                return _attachDetachItem;
            }
        }

        /// <summary>
        /// Gets the debugger's create host menu item of the menu bar.
        /// </summary>
        /// <value>The create host item.</value>
        public MenuItem CreateHostItem {
            get {
                return _createHostItem;
            }
        }

        /// <summary>
        /// Gets the debugger's start/stop menu item of the menu bar.
        /// </summary>
        /// <value>The start/stop item.</value>
        public MenuItem StartStopPetriItem {
            get {
                return _runStopItem;
            }
        }

        /// <summary>
        /// Gets the debugger's pause menu item of the menu bar.
        /// </summary>
        /// <value>The pause item.</value>
        public MenuItem PlayPauseItem {
            get {
                return _pauseItem;
            }
        }

        /// <summary>
        /// Gets the debugger's fix menu item of the menu bar.
        /// </summary>
        /// <value>The reload item.</value>
        public MenuItem ReloadItem {
            get {
                return _reloadPetriItem;
            }
        }

        /// <summary>
        /// Gets the sync with editor menu item of the menu bar.
        /// </summary>
        /// <value>The reload item.</value>
        public MenuItem SyncWithEditorItem {
            get {
                return _syncWithEditorItem;
            }
        }

        /// <summary>
        /// Gets the debugger's exit menu item of the menu bar.
        /// </summary>
        /// <value>The exit item.</value>
        public MenuItem ExitItem {
            get {
                return _exitItem;
            }
        }

        /// <summary>
        /// Gets the clear breakpoints menu item.
        /// </summary>
        /// <value>The clear breakpoints item.</value>
        public MenuItem ClearBreakpointsItem {
            get {
                return _clearBreakpointsItem;
            }
        }

        /// <summary>
        /// Gets the lock console menu item.
        /// </summary>
        /// <value>The lock console menu item.</value>
        public MenuItem LockConsoleMenuItem {
            get {
                return _lockConsoleItem;
            }
        }
        /// <summary>
        /// Called when any of the non-static menu item are selected from the menu bar.
        /// </summary>
        /// <param name="sender">Sender.</param>

        protected override async Task HandleMenuClicked(object sender)
        {
            await base.HandleMenuClicked(sender);

            if(sender == _embedInMacroItem) {
                if(Document.CurrentController == Document.EditorController) {
                    Document.EditorController.EmbedInInnerPetriNet();
                }
            } else if(sender == _importPetriNetItem) {
                Document.EditorController.ImportPetriNet();
            } else if(sender == _showEditorItem) {
                SwitchToEditor();
            } else if(sender == _showDebuggerItem) {
                SwitchToDebug();
            } else if(sender == _actualSizeItem) {
                Gui.BaseView.Zoom = 1;
                Gui.BaseView.Redraw();
            } else if(sender == _zoomInItem) {
                Gui.BaseView.Zoom /= 0.8f;
                if(Gui.BaseView.Zoom > 8f) {
                    Gui.BaseView.Zoom = 8f;
                }
                Gui.BaseView.Redraw();
            } else if(sender == _zoomOutItem) {
                Gui.BaseView.Zoom *= 0.8f;
                if(Gui.BaseView.Zoom < 0.01f) {
                    Gui.BaseView.Zoom = 0.01f;
                }
                Gui.BaseView.Redraw();
            } else if(sender == _findItem) {
                Document.Find();
            } else if(sender == _clearBreakpointsItem) {
                DebugGui.OnClick(sender, null);
            } else if(sender == _runStopItem) {
                DebugGui.OnClick(sender, null);
            } else if(sender == _attachDetachItem) {
                DebugGui.OnClick(sender, null);
            } else if(sender == _createHostItem) {
                DebugGui.OnClick(sender, null);
            } else if(sender == _pauseItem) {
                DebugGui.OnClick(sender, null);
            } else if(sender == _showVarItem) {
                DebugGui.OnClick(sender, null);
            } else if(sender == _reloadPetriItem) {
                DebugGui.OnClick(sender, null);
            } else if(sender == _syncWithEditorItem) {
                DebugGui.OnClick(sender, null);
            } else if(sender == _exitItem) {
                DebugGui.OnClick(sender, null);
            } else if(sender == _clearConsoleItem) {
                ConsoleOutputViewer.ClearConsole();
            } else if(sender == _lockConsoleItem) {
                ConsoleOutputViewer.ToggleConsoleLock();
            }
        }

        /// <summary>
        /// Updates the state of the user interface (mainly menu items and toolbars).
        /// </summary>
        public override void UpdateUI()
        {
            base.UpdateUI();

            _syncWithEditorItem.Sensitive = false;
            _findItem.Sensitive = true;

            if(_executeHasShortcut) {
                _executeHasShortcut = false;
                _runStopItem.RemoveAccelerator(_accelGroup, (uint)Gdk.Key.F5, Gdk.ModifierType.None);
            }
            if(_continueHasShortcut) {
                _continueHasShortcut = false;
                _pauseItem.RemoveAccelerator(_accelGroup, (uint)Gdk.Key.F5, Gdk.ModifierType.None);
            }

            if(Document.DebugController.Client.CurrentSessionState == DebugClient.SessionState.Started) {
                _attachDetachItem.Sensitive = true;
                _createHostItem.Sensitive = true;
                _runStopItem.Sensitive = Document.DebugController.Client.CurrentPetriState == DebugClient.PetriState.Stopped
                                             || Document.DebugController.Client.PetriAlive;
                _reloadPetriItem.Sensitive = true;
                _exitItem.Sensitive = true;

                (_attachDetachItem.Child as Label).Text = Configuration.GetLocalized("Disconnect");

                if(Document.DebugController.Client.PetriAlive) {
                    DebugGui.PropertiesPanel.EvaluateSensitive = false;
                    (_runStopItem.Child as Label).Text = Configuration.GetLocalized("Stop");
                    _pauseItem.Sensitive = true;
                }

                switch(Document.DebugController.Client.CurrentPetriState) {
                case DebugClient.PetriState.Stopping:
                    break;
                case DebugClient.PetriState.Starting:
                case DebugClient.PetriState.Stopped:
                    (_runStopItem.Child as Label).Text = Configuration.GetLocalized("Execute");
                    _runStopItem.AddAccelerator("activate",
                           _accelGroup,
                           new AccelKey(Gdk.Key.F5,
                                        Gdk.ModifierType.None,
                                        AccelFlags.Visible));
                    _executeHasShortcut = true;

                    _pauseItem.Sensitive = false;
                    (_pauseItem.Child as Label).Text = Configuration.GetLocalized("Pause");
                    DebugGui.PropertiesPanel.EvaluateSensitive = true;
                    break;
                case DebugClient.PetriState.Pausing:
                    _pauseItem.Sensitive = false;
                    goto case DebugClient.PetriState.Started;
                case DebugClient.PetriState.Started:
                    (_pauseItem.Child as Label).Text = Configuration.GetLocalized("Pause");
                    DebugGui.PropertiesPanel.EvaluateSensitive = false;
                    break;
                case DebugClient.PetriState.Paused:
                    DebugGui.PropertiesPanel.EvaluateSensitive = true;
                    (_pauseItem.Child as Label).Text = Configuration.GetLocalized("Continue");
                    _pauseItem.AddAccelerator("activate",
                                               _accelGroup,
                                               new AccelKey(Gdk.Key.F5,
                                                            Gdk.ModifierType.None,
                                                            AccelFlags.Visible));
                    _continueHasShortcut = true;

                    DebugGui.PropertiesPanel.EvaluateSensitive = true;
                    break;
                }
            } else {
                if(Document.Settings.Language != Code.Language.EmbeddedC) {
                    _attachDetachItem.Sensitive = true;
                    _createHostItem.Sensitive = Document.DebugController.Client.CurrentSessionState == DebugClient.SessionState.Stopped;
                }
                if(Document.DebugController.Client.CurrentSessionState == DebugClient.SessionState.Stopped) {
                    (_attachDetachItem.Child as Label).Text = Configuration.GetLocalized("Connection");
                } else {
                    (_attachDetachItem.Child as Label).Text = Configuration.GetLocalized("Abort connection");
                }
                (_runStopItem.Child as Label).Text = Configuration.GetLocalized("Execute");
                _runStopItem.Sensitive = false;
                _runStopItem.AddAccelerator("activate",
                    _accelGroup,
                    new AccelKey(Gdk.Key.F5,
                                 Gdk.ModifierType.None,
                                 AccelFlags.Visible));
                _executeHasShortcut = true;

                _reloadPetriItem.Sensitive = false;
                _pauseItem.Sensitive = false;
                _exitItem.Sensitive = false;
                DebugGui.PropertiesPanel.EvaluateSensitive = false;
                (_pauseItem.Child as Label).Text = Configuration.GetLocalized("Pause");
            }

            // The GUI must be updated after the menu bar, especially for the DebugGui which relies on menu items sensitiveness for its Update method.
            Gui.Update();
            Gui.BaseView.Redraw();
        }

        /// <summary>
        /// Manages the copy operation.
        /// </summary>
        protected override void ManageCopy()
        {
            if(Focus == Gui.BaseView.DrawingArea) {
                Gui.Copy();
            }
        }

        /// <summary>
        /// Manages the cut operation.
        /// </summary>
        protected override void ManageCut()
        {
            if(Focus == Gui.BaseView.DrawingArea) {
                Gui.Cut();
            }
        }

        /// <summary>
        /// Manages the paste operation.
        /// </summary>
        protected override void ManagePaste()
        {
            if(Focus == Gui.BaseView.DrawingArea) {
                Gui.Paste();
            }
        }

        /// <summary>
        /// Manages the select all operation.
        /// </summary>
        protected override void ManageSelectAll()
        {
            if(Focus == Gui.BaseView.DrawingArea) {
                Gui.SelectAll();
            }
        }

        /// <summary>
        /// Tries to close the window. If the window should not close, then this must return <c>false</c>. Else, this must return <c>true</c> and perform cleanup code.
        /// </summary>
        /// <returns><c>true</c>, the window should be closed, <c>false</c> otherwise.</returns>
        public override bool TryClose()
        {
            return Document.CloseAndConfirm();
        }

        /// <summary>
        /// Gets the default focused widget when we want to change selection.
        /// </summary>
        /// <value>The default focus.</value>
        protected override Widget DefaultFocus => Gui.BaseView.DrawingArea;

        /// <summary>
        /// Gets a value indicating whether this <see cref="T:Petri.Application.GUI.MainWindow"/> is cached.
        /// </summary>
        /// <value><c>false</c>.</value>
        protected override bool IsCached {
            get {
                return false;
            }
        }

        /// <summary>
        /// Switchs to the debug view.
        /// </summary>
        public void SwitchToDebug()
        {
            if(Document.CurrentController != Document.DebugController) {
                Document.CurrentController = Document.DebugController;
                Gui = DebugGui;
            }
        }

        /// <summary>
        /// Switchs to the editor view.
        /// </summary>
        public void SwitchToEditor()
        {
            if(GUIApplication.IsReadOnly) {
                SwitchToDebug();
            } else if(Document.CurrentController != Document.EditorController) {
                Document.CurrentController = Document.EditorController;
                Gui = EditorGui;
            }
        }

        /// <summary>
        /// Performs a find action.
        /// </summary>
        /// <param name="what">The text to search.</param>
        /// <param name="which">The kind of entities to search.</param>
        /// <param name="where">The fields to search into.</param>
        /// <param name="regex">Whether to search with a regex pattern.</param>
        public void PerformFind(string what, Controller.FindWhich which, Controller.FindWhere where, bool regex)
        {
            SearchResultsViewer.PerformFind(what, which, where, regex);
        }

        /// <summary>
        /// Sets the content of the compilation issues view.
        /// May be <c>null</c>, causing the issues text to be cleared.
        /// </summary>
        /// <param name="text">Text.</param>
        public void SetIssuesContent(string text)
        {
            IssuesViewer.SetIssuesContent(text);
        }

        /// <summary>
        /// Appends text to the console view.
        /// </summary>
        /// <param name="text">Text.</param>
        public void AppendConsoleContent(string text)
        {
            ConsoleOutputViewer.AppendConsoleContent(text);
        }

        /// <summary>
        /// Shows the variables list of the petri net.
        /// </summary>
        public void ShowVariables()
        {
            VariablesViewer.Present();
        }

        /// <summary>
        /// Gets the console output viewer.
        /// </summary>
        /// <value>The console output viewer.</value>
        public ConsoleOutputViewer ConsoleOutputViewer {
            get;
            private set;
        }

        /// <summary>
        /// Gets the search results viewer.
        /// </summary>
        /// <value>The search results viewer.</value>
        public SearchResultsViewer SearchResultsViewer {
            get;
            private set;
        }

        /// <summary>
        /// Gets the issues viewer.
        /// </summary>
        /// <value>The console output viewer.</value>
        public IssuesViewer IssuesViewer {
            get;
            private set;
        }
        /// <summary>
        /// Gets the variables viewer tab of the document.
        /// </summary>
        /// <value>The variables viewer.</value>

        public VariablesViewer VariablesViewer {
            get;
            private set;
        }

        Notebook _helperTabs;

        bool _executeHasShortcut = true;
        bool _continueHasShortcut = false;

        Gui _gui;
        Editor.EditorGui _editorGui;
        Debugger.DebugGui _debugGui;
    }
}
