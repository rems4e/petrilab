//
//  TransitionConditionModule.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-12-29.
//

using System;

using Gtk;

using Petri.Model;

namespace Petri.Application.GUI.PropertiesPanel
{
    /// <summary>
    /// A properties panel that allows to view or edit the condition of a petri net's transition.
    /// </summary>
    public class TransitionConditionModule : PropertiesPanelModule
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Application.GUI.PropertiesPanel.TransitionConditionModule"/> class.
        /// </summary>
        /// <param name="pane">The properties panel.</param>
        /// <param name="transition">Transition.</param>
        public TransitionConditionModule(PropertiesPanel pane, Transition transition) : base(pane)
        {
            _transition = transition;
        }

        /// <summary>
        /// Adds the widgets composing the panel module to the panel.
        /// </summary>
        public override void Fill()
        {
            if(_transition.Document is ExternalDocument || _panel is Debugger.DebugPropertiesPanel) {
                CreateLabel(0, Configuration.GetLocalized("Transition's condition:"));
                var e = CreateWidget<Entry>(true,
                                            0,
                                            _transition.Condition.MakeUserReadable());
                e.IsEditable = false;
            } else {
                CreateLabel(0, Configuration.GetLocalized("Transition's condition:"));
                string userReadable;
                if(_transition.Condition.NeedsExpansion) {
                    userReadable = _transition.Condition.Unexpanded;
                } else {
                    userReadable = _transition.Condition.MakeUserReadable();
                }
                var condition = CreateWidget<Entry>(true, 0, userReadable);
                GUIApplication.RegisterValidation(condition, false, (obj, focusOut, p) => {
                    if(_panel.IsResetting) {
                        return;
                    }

                    try {
                        var issuesCount = _panel.Document.GetIssues(_transition).Count;

                        var cond = new ConditionChangeAction(_transition,
                                                             _panel.Document.EntityFactory.CreateExpressionFromString(obj.Text));
                        CommitGuiAction(cond, focusOut);

                        // This will get rid of the compilation errors, if any are displayed.
                        if(issuesCount > 0) {
                            _panel.Document.RemoveIssue(_transition, EntityIssue.Kind.CompilationError);
                            _panel.Document.RemoveIssue(_transition, EntityIssue.Kind.CompilationWarning);
                            _panel.Reset();
                        }
                    } catch(Exception e) {
                        _panel.NotifyError(Configuration.GetLocalized("The specified condition is invalid ({0}).",
                                                                      e.Message));
                        obj.Text = userReadable;
                    }
                });
            }
        }

        Transition _transition;
    }
}
