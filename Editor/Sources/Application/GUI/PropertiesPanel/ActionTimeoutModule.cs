//
//  ActionTimeoutModule.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2018-01-14.
//

using Gtk;

namespace Petri.Application.GUI.PropertiesPanel
{
    /// <summary>
    /// A properties panel module that allows to view/change the timeout of an action.
    /// </summary>
    public class ActionTimeoutModule : PropertiesPanelModule
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Application.GUI.PropertiesPanel.EntityNameModule"/> class.
        /// </summary>
        /// <param name="pane">The properties panel.</param>
        /// <param name="action">Action.</param>
        public ActionTimeoutModule(PropertiesPanel pane, Model.Action action) : base(pane)
        {
            _action = action;
        }

        /// <summary>
        /// Adds the widgets composing the panel module to the panel.
        /// </summary>
        public override void Fill()
        {
            if(_action.Document is ExternalDocument || _panel is Debugger.DebugPropertiesPanel) {
                if(_action.TimeoutMS > 0) {
                    CreateLabel(0, Configuration.GetLocalized("Timeout: {0} ms", _action.TimeoutMS));
                }
            } else {
                bool hasTimeout = _action.TimeoutMS > 0;

                var hasTimeoutButton = CreateWidget<CheckButton>(true, 0, Configuration.GetLocalized("Has timeout"));
                CreateLabel(10, Configuration.GetLocalized("Timeout (ms)"));
                var timeout = CreateWidget<Entry>(true, 10, _action.TimeoutMS > 0 ? _action.TimeoutMS.ToString() : "");
                hasTimeoutButton.Active = hasTimeout;
                timeout.Sensitive = hasTimeout;

                hasTimeoutButton.Toggled += (sender, e) => {
                    hasTimeout = hasTimeoutButton.Active;
                    if(hasTimeout) {
                        timeout.Sensitive = true;
                        CommitGuiAction(new ChangeTimeoutAction(_action, 1000));
                        timeout.Text = "1000";
                    } else {
                        timeout.Sensitive = false;
                        CommitGuiAction(new ChangeTimeoutAction(_action, 0));
                        timeout.Text = "";
                    }
                };

                GUIApplication.RegisterValidation(timeout, false, (obj, focusOut, p) => {
                    try {
                        var newVal = uint.Parse(timeout.Text);
                        if(newVal == 0) {
                            throw new System.ArgumentException();
                        }
                        CommitGuiAction(new ChangeTimeoutAction(_action, newVal), focusOut);
                    } catch {
                        _panel.NotifyError(Configuration.GetLocalized("The timeout value must be a number of milliseconds greater than 0!"));
                        timeout.Text = _action.TimeoutMS.ToString();
                    }
                });
            }
        }

        Model.Action _action;
    }
}
