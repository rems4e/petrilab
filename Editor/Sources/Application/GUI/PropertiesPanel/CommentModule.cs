//
//  CommentModule.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-12-29.
//

using System;
using System.Collections.Generic;

using Gtk;

using Petri.Model;

namespace Petri.Application.GUI.PropertiesPanel
{
    /// <summary>
    /// A properties module that handles a petri net comment.
    /// </summary>
    public class CommentModule : PropertiesPanelModule
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Application.GUI.PropertiesPanel.CommentModule"/> class.
        /// </summary>
        /// <param name="pane">The properties panel.</param>
        /// <param name="comment">The comment.</param>
        public CommentModule(PropertiesPanel pane, Comment comment) : base(pane)
        {
            _comment = comment;
        }

        /// <summary>
        /// Adds the widgets composing the panel module to the panel.
        /// </summary>
        public override void Fill()
        {
            if(_comment.Document is ExternalDocument) {
                CreateLabel(0, Configuration.GetLocalized("Comment:"));

                var buf = new TextBuffer(new TextTagTable());
                buf.Text = _comment.Name;
                var name = CreateWidget<TextView>(true, 0, buf);
                name.SetSizeRequest(200, 400);
                name.WrapMode = WrapMode.Word;
                name.Editable = false;
            } else {
                CreateLabel(0, Configuration.GetLocalized("Color:"));
                _colors = new List<Cairo.Color> {
                    new Cairo.Color(1, 1, 0.7),
                    new Cairo.Color(1, 0.7, 0.7),
                    new Cairo.Color(0.7, 1, 0.7),
                    new Cairo.Color(0.5, 0.8, 1),
                    new Cairo.Color(1, 0.7, 1)
                };
                _colorNames = new List<string> {
                    Configuration.GetLocalized("Yellow"),
                    Configuration.GetLocalized("Red"),
                    Configuration.GetLocalized("Green"),
                    Configuration.GetLocalized("Blue"),
                    Configuration.GetLocalized("Pink"),
                    Configuration.GetLocalized("Manual…")
                };

                int colorIndex = _colors.FindIndex(((Cairo.Color obj) => {
                    return obj.R == _comment.Color.R && obj.G == _comment.Color.G && obj.B == _comment.Color.B;
                }));
                if(colorIndex == -1) {
                    colorIndex = _colorNames.Count - 1;
                }

                var colorList = GUIApplication.ComboHelper(_colorNames[colorIndex], _colorNames);
                colorList.Changed += (object sender, EventArgs e) => {
                    ComboBox combo = sender as ComboBox;

                    TreeIter iter;

                    if(combo.GetActiveIter(out iter)) {
                        var val = combo.Model.GetValue(iter, 0) as string;
                        EditColor(_comment, val, true);
                    }
                };
                AddWidget(colorList, false, 0);

                _button = new ColorButton();
                _button.ColorSet += (object sender, EventArgs e) => {
                    var newColor = (sender as ColorButton).Color;
                    CommitGuiAction(new ChangeCommentColorAction(_comment,
                                                                 new Cairo.Color(newColor.Red / 65535.0,
                                                                                 newColor.Green / 65535.0,
                                                                                 newColor.Blue / 65535.0)));
                };

                EditColor(_comment, _colorNames[colorIndex], false);

                CreateLabel(0, Configuration.GetLocalized("Comment:"));

                var buf = new TextBuffer(new TextTagTable());
                buf.Text = _comment.Name;
                var comment = CreateWidget<TextView>(true, 0, buf);
                comment.SetSizeRequest(200, 400);
                comment.WrapMode = WrapMode.Word;

                comment.FocusOutEvent += (obj, eventInfo) => {
                    CommitGuiAction(new ChangeNameAction(_comment, (obj as TextView).Buffer.Text));
                };
            }
        }

        /// <summary>
        /// Adapts the editor's widget to be able to edit the color of the comment.
        /// </summary>
        /// <param name="comment">Comment.</param>
        /// <param name="color">Color.</param>
        /// <param name="changed">If set to <c>true</c> changed.</param>
        protected void EditColor(Comment comment, string color, bool changed)
        {
            var widgets = _panel.Widgets.Find((obj) => obj.Item1 == this).Item2;
            if(color == Configuration.GetLocalized("Manual…")) {
                int index = widgets.FindIndex(obj => {
                    return obj.Item1 is Label && (obj.Item1 as Label).Text == Configuration.GetLocalized("Color:");
                });
                widgets.Insert(index + 2, Tuple.Create(_button as Widget, false, 20));
                _button.Color = new Gdk.Color((byte)(comment.Color.R * 255),
                                              (byte)(comment.Color.G * 255),
                                              (byte)(comment.Color.B * 255));
            } else {
                int index = widgets.FindIndex(obj => {
                    return obj.Item1 == _button;
                });
                if(index != -1) {
                    widgets.RemoveAt(index);
                }

                if(changed) {
                    CommitGuiAction(new ChangeCommentColorAction(comment,
                                                                 _colors[_colorNames.IndexOf(color)]));
                }
            }

            _panel.FormatAndShow();
        }

        List<string> _colorNames;
        List<Cairo.Color> _colors;
        ColorButton _button;
        Comment _comment;
    }
}
