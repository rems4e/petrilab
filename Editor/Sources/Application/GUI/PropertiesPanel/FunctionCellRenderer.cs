//
//  FunctionCellRenderer.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-12-29.
//

using Gtk;

namespace Petri.Application.GUI.PropertiesPanel
{
    /// <summary>
    /// A cell renderer that prints the signature of a function.
    /// </summary>
    class FunctionCellRenderer : CellRendererText
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.PropertiesPanel.FunctionCellRenderer"/> class.
        /// </summary>
        /// <param name="factory">Entity factory.</param>
        public FunctionCellRenderer(Model.EntityFactory factory)
        {
            _factory = factory;
        }

        /// <summary>
        /// Gets a readable name from the given function.
        /// </summary>
        /// <returns>The readable name of the.</returns>
        /// <param name="factory">Entity factory.</param>
        /// <param name="func">Function.</param>
        public static string ReadableNameForFunction(Model.EntityFactory factory, Code.Function func)
        {
            if(func.Signature == factory.DoNothingFunction.Signature) {
                return Configuration.GetLocalized("Do Nothing");
            } else if(func.Signature == factory.PauseFunction?.Signature) {
                return Configuration.GetLocalized("Sleep");
            } else if(func.Signature == factory.PrintActionFunction?.Signature) {
                return Configuration.GetLocalized("Show state's ID and Name");
            } else if(func.Signature == factory.PrintVariablesFunction?.Signature) {
                return Configuration.GetLocalized("Print variables");
            } else if(func.Signature == factory.PrintTextFunction?.Signature) {
                return Configuration.GetLocalized("Print text");
            } else {
                return func.Signature;
            }
        }

        /// <summary>
        /// Gets or sets the function invocation.
        /// </summary>
        /// <value>The function invocation.</value>
        [GLib.Property("invocation")]
        public Code.FunctionInvocation Invocation {
            get {
                return _invocation;
            }
            set {
                if(value != null) {
                    Text = ReadableNameForFunction(_factory, value.Function);
                }
                _invocation = value;
            }
        }

        Code.FunctionInvocation _invocation;
        Model.EntityFactory _factory;
    }
}
