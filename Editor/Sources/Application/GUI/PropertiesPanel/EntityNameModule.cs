//
//  EntityNameModule.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-01.
//

using Gtk;

using Petri.Model;

namespace Petri.Application.GUI.PropertiesPanel
{
    /// <summary>
    /// A properties panel module that allows to view/change a petri net state or transition's name.
    /// </summary>
    public class EntityNameModule : PropertiesPanelModule
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Application.GUI.PropertiesPanel.EntityNameModule"/> class.
        /// </summary>
        /// <param name="pane">The properties panel.</param>
        /// <param name="entity">Entity.</param>
        public EntityNameModule(PropertiesPanel pane, Entity entity) : base(pane)
        {
            _entity = entity;
        }

        /// <summary>
        /// Adds the widgets composing the panel module to the panel.
        /// </summary>
        public override void Fill()
        {
            var key = _entity is Transition ? Configuration.GetLocalized("Transition's name:") : Configuration.GetLocalized("State's name:");

            CreateLabel(0, key);
            var name = CreateWidget<Entry>(true, 0, _entity.Name);

            if(_entity is ExitPoint || _entity.Document is ExternalDocument || _panel is Debugger.DebugPropertiesPanel) {
                name.IsEditable = false;
            } else {
                GUIApplication.RegisterValidation(name, true, (obj, focusOut, p) => {
                    CommitGuiAction(new ChangeNameAction(_entity, obj.Text), focusOut);
                });
            }
        }

        Entity _entity;
    }
}
