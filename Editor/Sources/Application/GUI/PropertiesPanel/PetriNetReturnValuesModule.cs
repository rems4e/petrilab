//
//  PetriNetReturnValuesModule.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2017-04-17.
//

using System;
using System.Collections.Generic;

using Petri.Code;
using Petri.Model;

namespace Petri.Application.GUI.PropertiesPanel
{
    /// <summary>
    /// A properties panel module that allows to view/change the return values of a petri net.
    /// </summary>
    public class PetriNetReturnValuesModule : AKeyValueEditorModule<string, Expression>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Application.GUI.PropertiesPanel.ReturnValuesModule"/> class.
        /// </summary>
        /// <param name="pane">The properties panel.</param>
        /// <param name="exitPoint">The exit point.</param>
        public PetriNetReturnValuesModule(PropertiesPanel pane, ExitPoint exitPoint) : base(pane, exitPoint)
        {
            _exitPoint = exitPoint;
        }

        /// <summary>
        /// Gets the title of the module.
        /// </summary>
        /// <value>The title.</value>
        protected override string Title => Configuration.GetLocalized("Petri net's return values:");

        /// <summary>
        /// Gets the key-value pairs' source.
        /// </summary>
        /// <value>The data source.</value>
        protected override IDictionary<string, Expression> DataSource => _exitPoint.Document.PetriNet.ReturnValues;

        /// <summary>
        /// Gets the key set.
        /// </summary>
        /// <value>The key set.</value>
        protected override string[] KeySet => null;

        /// <summary>
        /// Gets the key description.
        /// </summary>
        /// <value>The key description.</value>
        protected override string KeyDescription => Configuration.GetLocalized("Name");

        /// <summary>
        /// Gets the value description.
        /// </summary>
        /// <value>The value description.</value>
        protected override string ValueDescription => Configuration.GetLocalized("Value");

        /// <summary>
        /// Gets the message for when a non-unique key is being inserted.
        /// </summary>
        /// <value>The non unique key message.</value>
        protected override string GetNonUniqueKeyMessage(string key)
        {
            return Configuration.GetLocalized("A return value named {0} already exists!", key.ToString());
        }

        /// <summary>
        /// Gets the description used when creating a key-value pair.
        /// </summary>
        /// <value>The add KVP description.</value>
        protected override string AddKVPDescription => Configuration.GetLocalized("Add a return value");

        /// <summary>
        /// Gets the description used when removing a key-value pair.
        /// </summary>
        /// <value>The remove KVP description.</value>
        protected override string RemoveKVPDescription => Configuration.GetLocalized("Remove the return value");

        /// <summary>
        /// Gets the description used when changing a key-value pair's name.
        /// </summary>
        /// <value>The change KVP name description.</value>
        protected override string ChangeKVPNameDescription => Configuration.GetLocalized("Change return value's name");

        /// <summary>
        /// Gets the description used when changing a key-value pair's value.
        /// </summary>
        /// <value>The change KVP value description.</value>
        protected override string ChangeKVPValueDescription => Configuration.GetLocalized("Change return value");

        /// <summary>
        /// Gets the default key format.
        /// </summary>
        /// <value>The default key format.</value>
        protected override string DefaultKeyFormat => "ret{0}";

        /// <summary>
        /// Gets the default value.
        /// </summary>
        /// <value>The default value.</value>
        protected override Expression DefaultValue => Expression.CreateFromString("0", _panel.Document.Settings.Language);

        /// <summary>
        /// Creates a key from the given value.
        /// </summary>
        /// <returns>The key.</returns>
        /// <param name="name">The key name.</param>
        protected override string CreateKeyFromString(string name)
        {
            return name;
        }

        /// <summary>
        /// Creates a key from a string.
        /// </summary>
        /// <param name="value">the value.</param>
        protected override Expression CreateValueFromString(string value)
        {
            try {
                var expr = Expression.CreateFromString(value, _panel.Document.Settings.Language);
                if(expr is ExpressionList) {
                    throw new Exception();
                }
                return expr;
            } catch {
                throw new Exception(Configuration.GetLocalized("Invalid expression!"));
            }
        }

        /// <summary>
        /// The read-only representation of the given key-value pair.
        /// </summary>
        /// <returns>The pair's read-only representation.</returns>
        /// <param name="key">Key.</param>
        /// <param name="value">Value.</param>
        protected override string ReadOnlyKVP(string key, Expression value)
        {
            return Configuration.GetLocalized("{0}, value {1}", key, value.ToString());
        }

        ExitPoint _exitPoint;
    }
}
