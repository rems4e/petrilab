//
//  BreakpointModule.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-01.
//

using Gtk;

using Action = Petri.Model.Action;

namespace Petri.Application.GUI.PropertiesPanel
{
    /// <summary>
    /// A properties module that allows to set/unset a breakpoint on a petri net state in the debugger.
    /// </summary>
    public class BreakpointModule : PropertiesPanelModule
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Application.GUI.PropertiesPanel.BreakpointModule"/> class.
        /// </summary>
        /// <param name="pane">The properties panel.</param>
        /// <param name="action">The state to put/remove a breakpoint on.</param>
        /// <param name="controller">The debug controller.</param>
        public BreakpointModule(PropertiesPanel pane,
                                Action action,
                                Petri.Application.Debugger.DebugController controller) : base(pane)
        {
            _action = action;
            _controller = controller;
        }

        /// <summary>
        /// Adds the widgets composing the panel module to the panel.
        /// </summary>
        public override void Fill()
        {
            var active = CreateWidget<CheckButton>(false,
                                                   0,
                                                   Configuration.GetLocalized("Breakpoint on the state"));
            active.Active = _controller.Breakpoints.Contains(_action);
            active.Toggled += (sender, e) => {
                if(_controller.Breakpoints.Contains(_action)) {
                    _controller.RemoveBreakpoint(_action);
                } else {
                    _controller.AddBreakpoint(_action);
                }
            };
        }

        Petri.Application.Debugger.DebugController _controller;
        Action _action;
    }
}
