//
//  InvocationModule.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-12-29.
//

using System;
using System.Collections.Generic;
using System.Linq;

using Gtk;

using Petri.Code;

using Action = Petri.Model.Action;

namespace Petri.Application.GUI.PropertiesPanel
{
    /// <summary>
    /// A properties panel that allows to view/change the function invocation of a petri net state.
    /// </summary>
    public class InvocationModule : PropertiesPanelModule
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Application.GUI.PropertiesPanel.InvocationModule"/> class.
        /// </summary>
        /// <param name="pane">The properties panel.</param>
        /// <param name="action">The action.</param>
        /// <param name="factory">The entity factory.</param>
        /// <param name="recentFunctions">The list of recent functions.</param>
        public InvocationModule(PropertiesPanel pane,
                                Action action,
                                Model.EntityFactory factory,
                                List<Code.Function> recentFunctions = null) : base(pane)
        {
            _action = action;
            _factory = factory;
            _recentFunctions = recentFunctions;
            _manual = false;
        }

        /// <summary>
        /// Adds the widgets composing the panel module to the panel.
        /// </summary>
        public override void Fill()
        {
            if(_action.Document is ExternalDocument || _panel is Debugger.DebugPropertiesPanel) {
                CreateLabel(0, Configuration.GetLocalized("State's action:"));
                var label = CreateLabel(10, ReadableNameForInvocation(_action.Document, _action.Invocation));
                label.Selectable = true;
            } else {
                _label = GUIApplication.LabelFromString(Configuration.GetLocalized("Associated action:"));

                _funcList = new ComboBox();
                _funcList.RowSeparatorFunc = (TreeModel mod, TreeIter iter) => {
                    return (string)mod.GetValue(iter, 0) == "" && mod.GetValue(iter, 1) == null;
                };

                var renderer = new FunctionCellRenderer(_factory);
                _funcList.PackStart(renderer, true);
                _funcList.AddAttribute(renderer, "text", 0);
                _funcList.AddAttribute(renderer, "invocation", 1);
                _funcList.AddAttribute(renderer, "sensitive", 2);

                var model = new TreeStore(typeof(string),
                                          typeof(Code.FunctionInvocation),
                                          typeof(bool));
                _funcList.Model = model;

                model.AppendValues(Configuration.GetLocalized("Manual…"), null, true);
                model.AppendValues("", null, false);

                // Recent functions
                {
                    var allFunctions = new HashSet<Code.Function>(_panel.Document.AllFunctions.Values.SelectMany(f => f));
                    if(_recentFunctions.Count > Configuration.EntityEditorMaxRecentActions) {
                        _recentFunctions.RemoveRange(Configuration.EntityEditorMaxRecentActions,
                                                     _recentFunctions.Count - Configuration.EntityEditorMaxRecentActions);
                    }
                    for(int i = 0; i < _recentFunctions.Count;) {
                        if(allFunctions.Contains(_recentFunctions[i])) {
                            ++i;
                        } else {
                            _recentFunctions.RemoveAt(i);
                        }
                    }
                    if(_recentFunctions.Count > 0) {
                        model.AppendValues(Configuration.GetLocalized("Recent functions"),
                                           null,
                                           false);
                        foreach(var f in _recentFunctions) {
                            var invocation = _factory.DefaultInvocationForFunction(f);
                            model.AppendValues(FunctionCellRenderer.ReadableNameForFunction(_factory, f),
                                               invocation,
                                               true);
                        }

                        model.AppendValues("", null, false);
                    }
                }

                bool hasCustom = (_panel.Document.AllFunctions.Count - 1 - (_panel.Document.AllFunctions.ContainsKey("") ? 1 : 0)) > 0;

                foreach(var sect in _panel.Document.AllFunctions) {
                    bool builtins = sect.Key == Configuration.GetLocalized("Built-in functions");
                    if(sect.Key != "") {
                        var iter = model.AppendValues(sect.Key, null, false);
                        foreach(Code.Function func in sect.Value) {
                            if(func.ReturnType.Equals(_panel.Document.Settings.Enum.Type) || (!builtins && _panel.Document.Settings.Language == Language.Python)) {
                                var invocation = _factory.DefaultInvocationForFunction(func);
                                if(builtins) {
                                    model.AppendValues(FunctionCellRenderer.ReadableNameForFunction(_factory, func),
                                                       invocation,
                                                       true);
                                } else {
                                    model.AppendValues(iter, func.Signature, invocation, true);
                                }
                            }
                        }
                    }
                    if(sect.Key == Configuration.GetLocalized("Built-in functions") && _panel.Document.AllFunctions.Count > 1 + (hasCustom ? 1 : 0)) {
                        model.AppendValues("", null, true);
                    }
                }

                if(_panel.Document.AllFunctions.ContainsKey("")) {
                    var otherFunctions = _panel.Document.AllFunctions[""].Where(f => (f.ReturnType.Equals(_panel.Document.Settings.Enum.Type) || _panel.Document.Settings.Language == Language.Python));
                    if(otherFunctions.Any()) {
                        if(hasCustom) {
                            model.AppendValues("", null, false);
                        }
                        model.AppendValues(Configuration.GetLocalized("Other functions"),
                                           null,
                                           false);

                        foreach(var func in otherFunctions) {
                            var invocation = _factory.DefaultInvocationForFunction(func);

                            model.AppendValues(func.Signature, invocation, true);
                        }
                    }
                }

                {
                    TreeIter iter;
                    _funcList.Model.GetIterFirst(out iter);
                    if(!_manual) {
                        _funcList.Model.GetIterFirst(out iter);
                        var found = PropertiesPanel.GetMatchingIter(_funcList.Model,
                                                    iter,
                                                    (it) => {
                                                        return (model.GetValue(it, 1) != null)
                                                                    && ((FunctionInvocation)model.GetValue(it,
                                                                                                           1)).Function.Signature == _action.Invocation.Function.Signature;
                                                    });
                        if(found.HasValue) {
                            iter = found.Value;
                        } else {
                            _manual = true;
                        }
                    }
                    _funcList.SetActiveIter(iter);
                }

                _funcList.Changed += (object sender, EventArgs e) => {
                    if(_panel.IsResetting) {
                        return;
                    }

                    TreeIter iter;

                    var combo = sender as ComboBox;
                    if(combo.GetActiveIter(out iter)) {
                        var f = (FunctionInvocation)model.GetValue(iter, 1);
                        if(f == null) {
                            _manual = true;
                            EditInvocation();
                        } else if(_action.Invocation.Function != f.Function) {
                            // Recent functions
                            if(!_panel.Document.AllFunctions[Configuration.GetLocalized("Built-in functions")].Contains(f.Function)) {
                                _recentFunctions.Remove(f.Function);
                                _recentFunctions.Insert(0, f.Function);
                                if(_recentFunctions.Count > Configuration.EntityEditorMaxRecentActions) {
                                    _recentFunctions.RemoveRange(Configuration.EntityEditorMaxRecentActions,
                                                                 _recentFunctions.Count - Configuration.EntityEditorMaxRecentActions);
                                }
                            }

                            _manual = false;
                            CommitGuiAction(new InvocationChangeAction(_action, f), false, true);
                        } else {
                            // Go from manual to builtin, same function
                            _manual = false;
                            EditInvocation();
                        }
                    }
                };

                EditInvocation();
            }
        }

        /// <summary>
        /// Commits the GUI action.
        /// </summary>
        /// <param name="a">The action to commit.</param>
        /// <param name="doNotFocus">If <c>true</c>, then no focus is given to the action.</param>
        /// <param name="reset">Wether to reformat the widgets of the module.</param>
        void CommitGuiAction(GuiAction a, bool doNotFocus, bool reset)
        {
            var issuesCount = _panel.Document.GetIssues(_action).Count;
            if(a != null) {
                CommitGuiAction(a, doNotFocus);
            }

            if(issuesCount > 0) {
                _panel.Reset();
            } else if(reset) {
                EditInvocation();
            } else {
                _panel.FormatAndShow();
            }
        }

        /// <summary>
        /// Updates the widgets to allow for editing the action with the specified kind of function.
        /// </summary>
        void EditInvocation()
        {
            var widgets = _panel.Widgets.Find((obj) => obj.Item1 == this).Item2;
            widgets.Clear();
            AddWidget(_label, true, 0);
            AddWidget(_funcList, true, 0);

            if(_manual) {
                ManualEdit();
            } else {
                AssistedEdit();
            }

            _panel.FormatAndShow();
        }

        /// <summary>
        /// Edits a manual invocation.
        /// </summary>
        void ManualEdit()
        {
            CreateLabel(0, Configuration.GetLocalized("State's invocation:"));
            string userReadable;
            if(_action.Invocation.NeedsExpansion) {
                userReadable = _action.Invocation.Unexpanded;
            } else {
                userReadable = _action.Invocation.MakeUserReadable();
            }

            var invocation = CreateWidget<Entry>(true, 0, userReadable);
            GUIApplication.RegisterValidation(invocation, false, (obj, focusOut, p) => {
                if(_panel.IsResetting) {
                    return;
                }

                try {
                    FunctionInvocation expr;
                    if(obj.Text == "") {
                        expr = _factory.DefaultInvocationForFunction(_factory.DoNothingFunction);
                    } else {
                        expr = _factory.TryGetFunction(obj.Text);
                    }
                    CommitGuiAction(new InvocationChangeAction(_action, expr),
                                    focusOut,
                                    false);
                } catch(Exception ex) {
                    _panel.NotifyError(Configuration.GetLocalized("The specified expression is invalid ({0}).",
                                                                  ex.Message));
                    obj.Text = userReadable;
                }
            });
        }

        /// <summary>
        /// Adapts the widgets to allow for editing one parameter of an invocation.
        /// </summary>
        /// <param name="i">The index of the parameter.</param>
        void EditParameter(int i)
        {
            var p = _action.Invocation.Function.Parameters[i];
            CreateLabel(20, Configuration.GetLocalized("Parameter {0} {1}:", p.Type, p.Name));

            var valueEditor = CreateWidget<Entry>(true, 20, _action.Invocation.Arguments[i].MakeUserReadable());
            GUIApplication.RegisterValidation(valueEditor, false, (obj, focusOut, ii) => {
                if(_panel.IsResetting) {
                    return;
                }

                try {
                    var args = new List<Expression>();
                    var widgets = _panel.Widgets.Find((o) => o.Item1 == this).Item2;
                    for(int j = (_action.Invocation.Function is Method) ? 4 : 2; j < widgets.Count; ++j) {
                        Widget w = widgets[j].Item1;
                        if(w.GetType() == typeof(Entry)) {
                            args.Add(_factory.CreateExpressionFromString((w as Entry).Text));
                        }
                    }
                    FunctionInvocation invocation;
                    if(_action.Invocation.Function is Method) {
                        invocation = new MethodInvocation(_action.Invocation.Function as Method,
                                                          _factory.CreateExpressionFromString((widgets[3].Item1 as Entry).Text),
                                                          false,
                                                          args.ToArray());
                    } else {
                        invocation = new FunctionInvocation(_action.Invocation.Function,
                                                            args.ToArray());
                    }
                    CommitGuiAction(new InvocationChangeAction(_action, invocation), focusOut, false);
                } catch(Exception ex) {
                    _panel.NotifyError(Configuration.GetLocalized("The specified expression is invalid ({0}).",
                                                                  ex.Message));
                    obj.Text = _action.Invocation.Arguments[(int)(ii[0])].MakeUserReadable();
                }
            }, new object[] { i });
        }

        /// <summary>
        /// Edits an action's invocation by providing entries for each of the function's arguments.
        /// </summary>
        void AssistedEdit()
        {
            if(_action.Invocation.Function.Signature == _factory.PrintVariablesFunction?.Signature) {
                var readableArgs = (from arg in _action.Invocation.Arguments
                                    select arg.MakeUserReadable()).Skip(1);

                int i = 0;
                foreach(var arg in readableArgs) {
                    int idx = i++ + 1;
                    var combo = GUIApplication.ComboHelper(
                        arg,
                        from var in _panel.Document.PetriNet.Variables select var.Key.MakeUserReadable(),
                        true
                    );

                    var remove = new Button(GUIApplication.LabelFromString("-", true));
                    remove.Clicked += (sender, e) => {
                        CommitGuiAction(new RemoveInvocationArgumentAction(
                            _action,
                            _action.Invocation,
                            idx
                        ), false, true);
                    };
                    var box = CreateWidget<HBox>(true, 20, false, 5);
                    box.PackStart(combo, true, true, 0);
                    box.PackStart(remove, false, false, 0);
                    box.ShowAll();


                    var updateArg = new Action<string>((val) => {
                        CommitGuiAction(new ChangeInvocationArgumentAction(
                            _action,
                            _action.Invocation,
                            idx,
                            Expression.CreateFromString<VariableExpression>(val, _panel.Document.Settings.Language)
                        ));
                    });

                    combo.Changed += (object sender, EventArgs e) => {
                        if(combo.GetActiveIter(out TreeIter iter)) {
                            var val = combo.Model.GetValue(iter, 0) as string;
                            updateArg(val);
                        }
                    };
                    GUIApplication.RegisterValidation((Entry)combo.Child, false, (e, focusOut, args) => {
                        try {
                            updateArg(e.Text);
                        } catch {
                            _panel.NotifyError(Configuration.GetLocalized("The specified expression must be a variable!"));
                            e.Text = _action.Invocation.Arguments[idx].MakeUserReadable();
                        }
                    });
                }
                var create = new Button(GUIApplication.LabelFromString("+", true));
                create.Clicked += (sender, e) => {
                    CommitGuiAction(new AddInvocationArgumentAction(
                        _action,
                        _action.Invocation,
                        _action.Invocation.Arguments.Count,
                        new EmptyExpression(_panel.Document.Settings.Language, true)
                    ), false, true);
                };

                var buttonBox = CreateWidget<HBox>(false, 20, false, 5);
                buttonBox.PackStart(create, false, false, 0);
                buttonBox.ShowAll();
            } else if(_action.Invocation.Function.Signature != _factory.PrintActionFunction?.Signature) {
                if(_action.Invocation.Function is Method) {
                    var method = _action.Invocation as MethodInvocation;
                    CreateLabel(20, Configuration.GetLocalized("{1} object of type {0}:",
                                                               method.Function.Enclosing.ToString(),
                                                               method.MethodObjectName));

                    var valueEditor = CreateWidget<Entry>(true, 20, method.This.MakeUserReadable());
                    GUIApplication.RegisterValidation(valueEditor, false, (obj, focusOut, p) => {
                        if(_panel.IsResetting) {
                            return;
                        }

                        var widgets = _panel.Widgets.Find((o) => o.Item1 == this).Item2;
                        try {
                            var args = new List<Expression>();
                            for(int j = 4; j < widgets.Count; ++j) {
                                Widget w = widgets[j].Item1;
                                if(w.GetType() == typeof(Entry)) {
                                    args.Add(_factory.CreateExpressionFromString((w as Entry).Text));
                                }
                            }
                            CommitGuiAction(new InvocationChangeAction(_action,
                                                                       new MethodInvocation(method.Function as Method,
                                                                                            _factory.CreateExpressionFromString((widgets[3].Item1 as Entry).Text),
                                                                                            false,
                                                                                            args.ToArray())),
                                            focusOut,
                                            false);
                        } catch(Exception ex) {
                            _panel.NotifyError(Configuration.GetLocalized("The specified expression is invalid ({0}).",
                                                                          ex.Message));
                            obj.Text = method.This.MakeUserReadable();
                        }
                    });
                }
                for(int i = 0; i < _action.Invocation.Function.Parameters.Count; ++i) {
                    EditParameter(i);
                }
            }
        }

        /// <summary>
        /// Returns a pretty string for a function invocation, that manages the special built-in functions.
        /// </summary>
        /// <returns>The representation for the invocation.</returns>
        /// <param name="doc">Document.</param>
        /// <param name="invocation">Invocation.</param>
        public static string ReadableNameForInvocation(Document doc, Code.FunctionInvocation invocation)
        {
            if(invocation.Function.Signature == doc.EntityFactory.DoNothingFunction.Signature) {
                return Configuration.GetLocalized("Do Nothing");
            } else if(invocation.Function.Signature == doc.EntityFactory.PauseFunction?.Signature) {
                return Configuration.GetLocalized("Sleep for {0}", invocation.Arguments[0].MakeUserReadable());
            } else if(invocation.Function.Signature == doc.EntityFactory.PrintActionFunction?.Signature) {
                return Configuration.GetLocalized("Show state's ID and Name");
            } else if(invocation.Function.Signature == doc.EntityFactory.PrintVariablesFunction?.Signature) {
                var readableArgs = (from arg in invocation.Arguments
                                    select arg.MakeUserReadable()).Skip(1);

                return Configuration.GetLocalized("Print variables {0}", string.Join(";", readableArgs));
            } else if(invocation.Function.Signature == doc.EntityFactory.PrintTextFunction?.Signature) {
                return Configuration.GetLocalized("Print \"{0}\"", invocation.Arguments[0].MakeUserReadable());
            } else {
                return invocation.MakeUserReadable();
            }
        }

        Action _action;
        Model.EntityFactory _factory;
        List<Code.Function> _recentFunctions;

        Label _label;
        ComboBox _funcList;
        bool _manual;
    }
}
