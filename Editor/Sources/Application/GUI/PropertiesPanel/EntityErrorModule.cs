//
//  EntityErrorModule.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-01.
//

using Gtk;

using Petri.Model;

namespace Petri.Application.GUI.PropertiesPanel
{
    /// <summary>
    /// A properties module that displays compilation errors/warnings on the selected entity, if any.
    /// </summary>
    public class EntityErrorModule : PropertiesPanelModule
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Application.GUI.PropertiesPanel.CompilationErrorModule"/> class.
        /// </summary>
        /// <param name="pane">The properties panel.</param>
        /// <param name="entity">The entity.</param>
        public EntityErrorModule(PropertiesPanel pane, Entity entity) : base(pane)
        {
            _entity = entity;
        }

        /// <summary>
        /// Adds the widgets composing the panel module to the panel.
        /// </summary>
        public override void Fill()
        {
            var issues = _panel.Document.GetIssues(_entity);
            if(issues.Count > 0) {
                CreateLabel(0, Configuration.GetLocalized("Errors/warnings:"));

                var buf = new TextBuffer(new TextTagTable());
                foreach(var issue in issues) {
                    buf.Text += issue.Message + "\n\n";
                }

                var error = CreateWidget<TextView>(true, 0, buf);
                error.SetSizeRequest(200, 100);
                error.WrapMode = WrapMode.Word;
                error.Editable = false;
            }
        }

        Entity _entity;
    }
}
