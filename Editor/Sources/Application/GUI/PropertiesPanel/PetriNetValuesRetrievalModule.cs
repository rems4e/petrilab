//
//  PetriNetValuesRetrievalModule.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2017-04-17.
//

using System;
using System.Linq;
using System.Collections.Generic;

using Petri.Model;
using Petri.Code;

namespace Petri.Application.GUI.PropertiesPanel
{
    /// <summary>
    /// A properties panel module that allows to retrieve a petri net's return values.
    /// </summary>
    public class PetriNetValueRetrievalModule : AKeyValueEditorModule<string, VariableExpression>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Application.GUI.PropertiesPanel.PetriNetValueRetrievalModule"/> class.
        /// </summary>
        /// <param name="pane">The properties panel.</param>
        /// <param name="petriNet">The petri net.</param>
        public PetriNetValueRetrievalModule(PropertiesPanel pane, ExternalInnerPetriNet petriNet) : base(pane, petriNet)
        {
            _petriNet = petriNet;
        }

        /// <summary>
        /// Gets the title of the module.
        /// </summary>
        /// <value>The title.</value>
        protected override string Title => Configuration.GetLocalized("Petri net's return values' destination:");

        /// <summary>
        /// Gets the key-value pairs' source.
        /// </summary>
        /// <value>The data source.</value>
        protected override IDictionary<string, VariableExpression> DataSource => _petriNet.ReturnValuesDestination;

        /// <summary>
        /// Gets the key set.
        /// </summary>
        /// <value>The key set.</value>
        protected override string[] KeySet => (from ret in _petriNet.ExternalDocument.PetriNet.ReturnValues
                                               select ret.Key).ToArray();

        /// <summary>
        /// Gets the key description.
        /// </summary>
        /// <value>The key description.</value>
        protected override string KeyDescription => Configuration.GetLocalized("Return value");

        /// <summary>
        /// Gets the value description.
        /// </summary>
        /// <value>The value description.</value>
        protected override string ValueDescription => Configuration.GetLocalized("Destination");

        /// <summary>
        /// Gets the message for when a non-unique key is being inserted.
        /// </summary>
        /// <value>The non unique key message.</value>
        protected override string GetNonUniqueKeyMessage(string key)
        {
            return Configuration.GetLocalized("The return value named {0} is already retrieved!", key.ToString());
        }

        /// <summary>
        /// Gets the description used when creating a key-value pair.
        /// </summary>
        /// <value>The add KVP description.</value>
        protected override string AddKVPDescription => Configuration.GetLocalized("Add a return value destination");

        /// <summary>
        /// Gets the description used when removing a key-value pair.
        /// </summary>
        /// <value>The remove KVP description.</value>
        protected override string RemoveKVPDescription => Configuration.GetLocalized("Remove the return value destination");

        /// <summary>
        /// Gets the description used when changing a key-value pair's name.
        /// </summary>
        /// <value>The change KVP name description.</value>
        protected override string ChangeKVPNameDescription => Configuration.GetLocalized("Change return value destination's name");

        /// <summary>
        /// Gets the description used when changing a key-value pair's value.
        /// </summary>
        /// <value>The change KVP value description.</value>
        protected override string ChangeKVPValueDescription => Configuration.GetLocalized("Change return value destination's value");

        /// <summary>
        /// Gets the default key format.
        /// </summary>
        /// <value>The default key format.</value>
        protected override string DefaultKeyFormat => "ret{0}";

        /// <summary>
        /// Gets the default value.
        /// </summary>
        /// <value>The default value.</value>
        protected override VariableExpression DefaultValue => new VariableExpression("$var", _petriNet.Document.Settings.Language);

        /// <summary>
        /// Creates a key from the given value.
        /// </summary>
        /// <returns>The key.</returns>
        /// <param name="name">The key name.</param>
        protected override string CreateKeyFromString(string name)
        {
            return name;
        }

        /// <summary>
        /// Creates a key from the given value.
        /// </summary>
        /// <returns>The key.</returns>
        /// <param name="value">The key name.</param>
        protected override VariableExpression CreateValueFromString(string value)
        {
            try {
                return Expression.CreateFromString<VariableExpression>(value, _petriNet.Document.Settings.Language);
            } catch {
                throw new Exception(Configuration.GetLocalized("The return value's destination must be a variable!"));
            }
        }

        /// <summary>
        /// The read-only representation of the given key-value pair.
        /// </summary>
        /// <returns>The pair's read-only representation.</returns>
        /// <param name="key">Key.</param>
        /// <param name="value">Value.</param>
        protected override string ReadOnlyKVP(string key, VariableExpression value)
        {
            return Configuration.GetLocalized("{0}, destination {1}", key, value.ToString());
        }

        ExternalInnerPetriNet _petriNet;
    }
}
