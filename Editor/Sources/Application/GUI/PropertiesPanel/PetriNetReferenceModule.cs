//
//  PetriNetReferenceModule.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-12-29.
//

using Gtk;

using Petri.Model;

namespace Petri.Application.GUI.PropertiesPanel
{
    /// <summary>
    /// A properties module that shows information about an external petri net.
    /// </summary>
    public class PetriNetReferenceModule : PropertiesPanelModule
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Application.GUI.PropertiesPanel.PetriNetReferenceModule"/> class.
        /// </summary>
        /// <param name="pane">The properties panel.</param>
        /// <param name="petriNet">The petri net.</param>
        public PetriNetReferenceModule(PropertiesPanel pane, ExternalInnerPetriNet petriNet) : base(pane)
        {
            _petriNet = petriNet;
            _pathLabel = GUIApplication.LabelFromMarkup("");
        }

        /// <summary>
        /// Adds the widgets composing the panel module to the panel.
        /// </summary>
        public override void Fill()
        {
            var edoc = _petriNet.ExternalDocument;
            if(edoc is ValidExternalDocument) {
                CreateLabel(0, Configuration.GetLocalized("This is a reference to:"));

                AddWidget(_pathLabel, true, 0);

                var edit = CreateWidget<Button>(false, 20, Configuration.GetLocalized("Open petri net…"));
                edit.Clicked += (sender, e) => {
                    GUIApplication.OpenDocument(edoc.Path);
                };
            } else {
                var label = GUIApplication.LabelFromMarkup(string.Format("<span color=\"red\">{0}</span>",
                                                                         GUIApplication.SafeMarkupFromLocalized("This is an invalid reference to:")));
                AddWidget(label, true, 0);
                AddWidget(_pathLabel, true, 0);
            }

            UpdateUI();

            if(!(_petriNet.Document is ExternalDocument || _panel is Debugger.DebugPropertiesPanel)) {
                var relative = CreateWidget<CheckButton>(false, 20, Configuration.GetLocalized("Relative reference"));
                relative.Active = _petriNet.ExternalPetriSource.RelativePathReference;
                relative.Toggled += (sender, e) => {
                    CommitGuiAction(new ToggleRelativeReferenceAction(_petriNet));
                    UpdateUI();
                };

                var change = CreateWidget<Button>(false, 20, Configuration.GetLocalized("Change location…"));
                change.Clicked += (sender, e) => {
                    var b = new CheckButton(Configuration.GetLocalized("Relative path"));
                    b.Active = Configuration.PreferRelativePaths;

                    var filename = GUIApplication.SelectDocumentPath((fc) => {
                        fc.Title = Configuration.GetLocalized("Change location…");
                        fc.ActionArea.PackEnd(b);
                        b.Show();
                    });
                    if(filename != null) {
                        if(b.Active) {
                            filename = _panel.Document.GetRelativeToDoc(filename);
                        }
                        CommitGuiAction(new RepointReferenceAction(_petriNet, filename));
                        _panel.Reset();
                    }
                };
            }
        }

        /// <summary>
        /// Updates the user interface elements.
        /// </summary>
        void UpdateUI()
        {
            if(_petriNet.ExternalDocument is ValidExternalDocument) {
                _pathLabel.Markup = string.Format("<span color=\"blue\">{0}</span>",
                                                  GUIApplication.SafeMarkupFromString(Application.ShortPath(_petriNet.ExternalPetriSource.Path)));
            } else {
                _pathLabel.Markup = string.Format("<span color=\"red\">{0}</span>",
                                                  GUIApplication.SafeMarkupFromString(Application.ShortPath(_petriNet.ExternalPetriSource.Path)));
            }

        }

        Label _pathLabel;
        ExternalInnerPetriNet _petriNet;
    }
}
