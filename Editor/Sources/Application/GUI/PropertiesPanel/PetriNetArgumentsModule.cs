//
//  PetriNetArgumentsModule.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2017-04-17.
//

using System;
using System.Linq;
using System.Collections.Generic;

using Petri.Model;
using Petri.Code;

namespace Petri.Application.GUI.PropertiesPanel
{
    /// <summary>
    /// A properties panel module that allows to view/change the parameters of a petri net.
    /// </summary>
    public class PetriNetArgumentsModule : AKeyValueEditorModule<VariableExpression, Expression>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Application.GUI.PropertiesPanel.PetriNetArgumentsModule"/> class.
        /// </summary>
        /// <param name="pane">The properties panel.</param>
        /// <param name="petriNet">The petri net.</param>
        public PetriNetArgumentsModule(PropertiesPanel pane, ExternalInnerPetriNet petriNet) : base(pane, petriNet)
        {
            _petriNet = petriNet;
        }

        /// <summary>
        /// Gets the title of the module.
        /// </summary>
        /// <value>The title.</value>
        protected override string Title => Configuration.GetLocalized("Petri net's arguments:");

        /// <summary>
        /// Gets the key-value pairs' source.
        /// </summary>
        /// <value>The data source.</value>
        protected override IDictionary<VariableExpression, Expression> DataSource => _petriNet.Arguments;

        /// <summary>
        /// Gets the key set.
        /// </summary>
        /// <value>The key set.</value>
        protected override string[] KeySet => (from param in _petriNet.ExternalDocument.PetriNet.Variables
                                               where _petriNet.ExternalDocument.PetriNet.Parameters.Contains(param.Key)
                                               select param.Key.MakeUserReadable()).ToArray();

        /// <summary>
        /// Gets the key description.
        /// </summary>
        /// <value>The key description.</value>
        protected override string KeyDescription => Configuration.GetLocalized("Parameter");

        /// <summary>
        /// Gets the value description.
        /// </summary>
        /// <value>The value description.</value>
        protected override string ValueDescription => Configuration.GetLocalized("Value");

        /// <summary>
        /// Gets the message for when a non-unique key is being inserted.
        /// </summary>
        /// <value>The non unique key message.</value>
        protected override string GetNonUniqueKeyMessage(VariableExpression key)
        {
            return Configuration.GetLocalized("An argument named {0} already exists!", key.ToString());
        }

        /// <summary>
        /// Gets the description used when creating a key-value pair.
        /// </summary>
        /// <value>The add KVP description.</value>
        protected override string AddKVPDescription => Configuration.GetLocalized("Add an argument");

        /// <summary>
        /// Gets the description used when removing a key-value pair.
        /// </summary>
        /// <value>The remove KVP description.</value>
        protected override string RemoveKVPDescription => Configuration.GetLocalized("Remove the argument");

        /// <summary>
        /// Gets the description used when changing a key-value pair's name.
        /// </summary>
        /// <value>The change KVP name description.</value>
        protected override string ChangeKVPNameDescription => Configuration.GetLocalized("Change argument's name");

        /// <summary>
        /// Gets the description used when changing a key-value pair's value.
        /// </summary>
        /// <value>The change KVP value description.</value>
        protected override string ChangeKVPValueDescription => Configuration.GetLocalized("Change argument's value");

        /// <summary>
        /// Gets the default key format.
        /// </summary>
        /// <value>The default key format.</value>
        protected override string DefaultKeyFormat => "$arg{0}";

        /// <summary>
        /// Gets the default value.
        /// </summary>
        /// <value>The default value.</value>
        protected override Expression DefaultValue => Expression.CreateFromString("0", _petriNet.Document.Settings.Language);

        /// <summary>
        /// Creates a key from the given value.
        /// </summary>
        /// <returns>The key.</returns>
        /// <param name="name">The key name.</param>
        protected override VariableExpression CreateKeyFromString(string name)
        {
            try {
                return Expression.CreateFromString<VariableExpression>(name, _petriNet.Document.Settings.Language);
            } catch {
                throw new Exception(Configuration.GetLocalized("The argument's name must be a variable!"));
            }
        }

        /// <summary>
        /// Creates a key from a string.
        /// </summary>
        /// <param name="value">the value.</param>
        protected override Expression CreateValueFromString(string value)
        {
            try {
                var expr = Expression.CreateFromString(value, _panel.Document.Settings.Language);
                if(expr is ExpressionList) {
                    throw new Exception();
                }
                return expr;
            } catch {
                throw new Exception(Configuration.GetLocalized("Invalid expression!"));
            }
        }

        /// <summary>
        /// The read-only representation of the given key-value pair.
        /// </summary>
        /// <returns>The pair's read-only representation.</returns>
        /// <param name="key">Key.</param>
        /// <param name="value">Value.</param>
        protected override string ReadOnlyKVP(VariableExpression key, Expression value)
        {
            return Configuration.GetLocalized("{0}, value {1}", key.ToString(), value.ToString());
        }

        ExternalInnerPetriNet _petriNet;
    }
}
