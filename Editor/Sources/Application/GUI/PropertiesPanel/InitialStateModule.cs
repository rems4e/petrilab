//
//  InitialStateModule.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-01.
//

using Gtk;

using Petri.Model;

namespace Petri.Application.GUI.PropertiesPanel
{
    /// <summary>
    /// A properties module that indicates if the current petri net state is to be active at t=0.
    /// </summary>
    public class InitialStateModule : PropertiesPanelModule
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Application.GUI.PropertiesPanel.InitialStateModule"/> class.
        /// </summary>
        /// <param name="pane">The properties panel.</param>
        /// <param name="state">State.</param>
        public InitialStateModule(PropertiesPanel pane, State state) : base(pane)
        {
            _state = state;
        }

        /// <summary>
        /// Adds the widgets composing the panel module to the panel.
        /// </summary>
        public override void Fill()
        {
            if(_state.Document is ExternalDocument || _panel is Debugger.DebugPropertiesPanel) {
                if(_state.IsStartState) {
                    CreateLabel(0, Configuration.GetLocalized("Active on t=0"));
                }
            } else {
                var active = CreateWidget<CheckButton>(false, 0, Configuration.GetLocalized("Active on t=0"));
                active.Active = _state.IsStartState;
                active.Toggled += (sender, e) => {
                    CommitGuiAction(new ToggleActiveAction(_state));
                };
            }
        }

        State _state;
    }
}
