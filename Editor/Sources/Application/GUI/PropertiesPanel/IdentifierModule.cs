//
//  IdentifierModule.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-02.
//

using Petri.Model;

namespace Petri.Application.GUI.PropertiesPanel
{
    /// <summary>
    /// A properties panel that displays the current petri net entity's identifier.
    /// </summary>
    public class IdentifierModule : PropertiesPanelModule
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Application.GUI.PropertiesPanel.IdentifierModule"/> class.
        /// </summary>
        /// <param name="pane">The properties panel.</param>
        /// <param name="entity">Entity.</param>
        public IdentifierModule(PropertiesPanel pane, Entity entity) : base(pane)
        {
            _entity = entity;
        }

        /// <summary>
        /// Adds the widgets composing the panel module to the panel.
        /// </summary>
        public override void Fill()
        {
            var label = GUIApplication.LabelFromMarkup("<span color=\"grey\">"
                                                       + Configuration.GetLocalized("Entity's ID:")
                                                       + " "
                                                       + _entity.GlobalID.ToString()
                                                       + "</span>");
            AddWidget(label, true, 0);
        }

        Entity _entity;
    }
}
