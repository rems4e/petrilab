//
//  PropertiesPanel.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2015-01-25.
//

using System;
using System.Collections.Generic;
using System.Linq;

using Gtk;

using WidgetTuple = System.Tuple<Gtk.Widget, bool, int>;

namespace Petri.Application.GUI.PropertiesPanel
{
    /// <summary>
    /// An entity editor for the debugger's view or the editor's view.
    /// </summary>
    public abstract class PropertiesPanel
    {
        /// <summary>
        /// The list of widgets. Item1 contains the widget,
        /// Item2 contains whether the widget has to resize with the pane.
        /// Item3 is the widget indentation
        /// </summary>
        public List<Tuple<PropertiesPanelModule, List<WidgetTuple>>> Widgets {
            get;
            private set;
        } = new List<Tuple<PropertiesPanelModule, List<WidgetTuple>>>();

        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.PropertiesPanel"/> class.
        /// </summary>
        /// <param name="doc">Document.</param>
        /// <param name="window">The GUI window of the panel.</param>
        /// <param name="view">The view to display widgets into.</param>
        protected PropertiesPanel(LocalDocument doc, MainWindow window, Fixed view)
        {
            Document = doc;
            _window = window;
            _view = view;
        }

        /// <summary>
        /// Gets the entity currently edited.
        /// </summary>
        /// <value>The entity.</value>
        public Model.Entity Entity {
            get;
            protected set;
        }

        /// <summary>
        /// Notifies an error to the user.
        /// </summary>
        /// <param name="error">Error.</param>
        public void NotifyError(string error)
        {
            GUIApplication.NotifyUnrecoverableError(_window, error);
        }

        /// <summary>
        /// Gets the document.
        /// </summary>
        /// <value>The document.</value>
        public LocalDocument Document {
            get;
            private set;
        }

        /// <summary>
        /// Adds a module to the properties panel.
        /// </summary>
        /// <param name="module">Module.</param>
        protected void AddModule(PropertiesPanelModule module)
        {
            _modules.Add(module);
            Widgets.Add(Tuple.Create(module, new List<WidgetTuple>()));
        }

        /// <summary>
        /// Adds the widget to the view.
        /// </summary>
        /// <param name="module">The module the widget belongs to.</param>
        /// <param name="w">The widget.</param>
        /// <param name="resizeable">If set to <c>true</c> then the widhet is resizeable.</param>
        /// <param name="indentation">The left indentation of the widget, in pixels.</param>
        public void AddWidget(PropertiesPanelModule module, Widget w, bool resizeable, int indentation)
        {
            var tup = Widgets.Find((o) => {
                return o.Item1 == module;
            });
            tup.Item2.Add(Tuple.Create(w, resizeable, indentation));
        }

        /// <summary>
        /// Resizes the view to a new width.
        /// </summary>
        /// <param name="width">The new width.</param>
        public void Resize(int width)
        {
            foreach(var list in Widgets) {
                foreach(var tuple in list.Item2) {
                    if(tuple.Item2) {
                        tuple.Item1.WidthRequest = Math.Max(0, width - 20 - tuple.Item3);
                    }
                }
            }

            if(_lastWidth != width) {
                GUIApplication.RunOnUIThread(() => {
                    FormatAndShow(false);
                }, true);
                _lastWidth = width;
            }
        }

        /// <summary>
        /// Removes all the widgets from the view, adds them again and format them against the indentation rules.
        /// </summary>
        public void FormatAndShow(bool reset = true)
        {
            var currentSet = new HashSet<Widget>(_view.AllChildren.Cast<Widget>());
            var toKeepSet = new HashSet<Widget>();
            foreach(var module in Widgets) {
                foreach(var tup in module.Item2) {
                    if(currentSet.Contains(tup.Item1)) {
                        toKeepSet.Add(tup.Item1);
                    }
                }
            }
            if(reset) {
                foreach(Widget w in currentSet.Except(toKeepSet)) {
                    _view.Remove(w);
                }
                _lastWidth = -1;
                _isResetting = false;
            }

            WidgetTuple lastWidget = null;
            int lastX = 20, lastY = 0;

            foreach(var module in Widgets) {
                foreach(var tup in module.Item2) {
                    Widget w = tup.Item1;
                    if(reset && !toKeepSet.Contains(w)) {
                        _view.Add(w);
                    }
                    if(w is Label) {
                        if(lastWidget != null && lastWidget.Item3 < tup.Item3) {
                            lastY += 5;
                        } else {
                            lastY += 15;
                        }
                    }

                    var w2 = (Fixed.FixedChild)_view[w];
                    w2.X = lastX + tup.Item3;
                    w2.Y = lastY;

                    lastY += w.Allocation.Height;
                    w.Show();
                    lastWidget = tup;
                }
            }

            if(_window.Gui != null) {
                _window.Gui.BaseView.Redraw();
            }
        }

        /// <summary>
        /// Resets the view by removing widgets, and populating the view module by module.
        /// </summary>
        public void Reset()
        {
            if(_isResetting) {
                return;
            }

            _isResetting = true;
            GUIApplication.RunOnUIThread(() => {
                foreach(var list in Widgets) {
                    list.Item2.Clear();
                }

                foreach(var m in _modules) {
                    m.Fill();
                }

                FormatAndShow();
            }, true);
        }

        /// <summary>
        /// Gets a value indicating whether this <see cref="T:Petri.Application.GUI.PropertiesPanel.PropertiesPanel"/>
        /// is resetting.
        /// </summary>
        /// <value><c>true</c> if is resetting; otherwise, <c>false</c>.</value>
        public bool IsResetting {
            get {
                return _isResetting;
            }
        }

        /// <summary>
        /// Gets the TreeIter, sibling or child of <paramref name="iter"/>, that satisfies the given predicate.
        /// </summary>
        /// <returns>The iter.</returns>
        /// <param name="store">Store.</param>
        /// <param name="iter">Iter.</param>
        /// <param name="pred">Pred.</param>
        public static TreeIter? GetMatchingIter(TreeModel store,
                                                TreeIter iter,
                                                Predicate<TreeIter> pred)
        {
            do {
                if(pred(iter)) {
                    return iter;
                }
                if(store.IterHasChild(iter)) {
                    TreeIter child;
                    if(store.IterChildren(out child, iter)) {
                        var result = GetMatchingIter(store, child, pred);
                        if(result != null) {
                            return result;
                        }
                    }
                }
            } while(store.IterNext(ref iter));

            return null;
        }

        /// <summary>
        /// Commits the GUI action and update the GUI accordingly (undo/redo submenus etc.).
        /// </summary>
        /// <param name="action">The action.</param>
        /// <param name="doNotFocus">If <c>true</c>, then no focus is given to the action.</param>
        public void CommitGuiAction(GuiAction action, bool doNotFocus)
        {
            _window.CommitGuiAction(action, doNotFocus);
        }

        /// <summary>
        /// The view to create widgets into.
        /// </summary>
        protected Fixed _view;

        /// <summary>
        /// The list of modules contained in the panel.
        /// </summary>
        protected List<PropertiesPanelModule> _modules = new List<PropertiesPanelModule>();

        /// <summary>
        /// The document window containing the panel.
        /// </summary>
        MainWindow _window;

        /// <summary>
        /// The last known width of the panel.
        /// </summary>
        int _lastWidth = 0;

        volatile bool _isResetting = false;
    }
}

