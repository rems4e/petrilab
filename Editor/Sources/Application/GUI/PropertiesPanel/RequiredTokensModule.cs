//
//  RequiredTokensModule.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-12-29.
//

using System;
using System.Collections.Generic;

using Gtk;

using Petri.Model;

namespace Petri.Application.GUI.PropertiesPanel
{
    /// <summary>
    /// A properties module that allows to view or change the count of tokens needed to enable a petri net's state.
    /// </summary>
    public class RequiredTokensModule : PropertiesPanelModule
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Application.GUI.PropertiesPanel.RequiredTokensModule"/> class.
        /// </summary>
        /// <param name="pane">The properties panel.</param>
        /// <param name="state">State.</param>
        public RequiredTokensModule(PropertiesPanel pane, State state) : base(pane)
        {
            _state = state;
        }

        /// <summary>
        /// Adds the widgets composing the panel module to the panel.
        /// </summary>
        public override void Fill()
        {
            if(_state.TransitionsBefore.Count > 0) {
                CreateLabel(0, Configuration.GetLocalized("Required tokens to enter the state:"));
                if(_state.Document is ExternalDocument || _panel is Debugger.DebugPropertiesPanel || _state is ExitPoint) {
                    var ee = CreateLabel(20, _state.RequiredTokens.ToString());
                } else {
                    var name = CreateWidget<Entry>(true, 0, _state.RequiredTokens.ToString());

                    GUIApplication.RegisterValidation(name, false, (obj, focusOut, p) => {
                        try {
                            var val = uint.Parse(obj.Text);
                            if(val <= 0) {
                                throw new Exception();
                            }
                            CommitGuiAction(new ChangeRequiredTokensAction(_state, val));
                        } catch {
                            _panel.NotifyError(Configuration.GetLocalized("The required tokens count must be a strictly positive integer."));
                            obj.Text = _state.RequiredTokens.ToString();
                        }
                    });
                }
            }
        }

        State _state;
    }
}
