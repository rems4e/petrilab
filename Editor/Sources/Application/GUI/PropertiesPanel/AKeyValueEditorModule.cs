//
//  AKeyValueEditorModule.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2017-09-05.
//

using System.Collections.Generic;

using Gtk;

namespace Petri.Application.GUI.PropertiesPanel
{
    /// <summary>
    /// A properties panel module that allows to view/change the parameters of a petri net.
    /// </summary>
    public abstract class AKeyValueEditorModule<KeyType, ValueType> : PropertiesPanelModule where KeyType : class
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Application.GUI.PropertiesPanel.AKeyValueEditorModule"/> class.
        /// </summary>
        /// <param name="pane">The properties panel.</param>
        /// <param name="entity">The entity.</param>
        public AKeyValueEditorModule(PropertiesPanel pane, Model.Entity entity) : base(pane)
        {
            _entity = entity;
        }

        /// <summary>
        /// Gets a value indicating whether this
        /// <see cref="T:Petri.Application.GUI.PropertiesPanel.AKeyValueEditorModule"/> is read only.
        /// </summary>
        /// <value><c>true</c> if is read only; otherwise, <c>false</c>.</value>
        protected bool IsReadOnly => _entity.Document is ExternalDocument || _panel is Debugger.DebugPropertiesPanel;

        /// <summary>
        /// Gets the title of the module.
        /// </summary>
        /// <value>The title.</value>
        protected abstract string Title {
            get;
        }

        /// <summary>
        /// Gets the key-value pairs' source.
        /// </summary>
        /// <value>The data source.</value>
        protected abstract IDictionary<KeyType, ValueType> DataSource {
            get;
        }

        /// <summary>
        /// Gets the key set. May be <c>null</c>, and in that case the keys are unrestricted.
        /// </summary>
        /// <value>The key set.</value>
        protected abstract string[] KeySet {
            get;
        }

        /// <summary>
        /// Gets the key description.
        /// </summary>
        /// <value>The key description.</value>
        protected abstract string KeyDescription { get; }

        /// <summary>
        /// Gets the value description.
        /// </summary>
        /// <value>The value description.</value>
        protected abstract string ValueDescription { get; }

        /// <summary>
        /// Gets the message for when a non-unique key is being inserted.
        /// </summary>
        /// <value>The non unique key message.</value>
        protected abstract string GetNonUniqueKeyMessage(KeyType key);

        /// <summary>
        /// Gets the description used when creating a key-value pair.
        /// </summary>
        /// <value>The add KVP description.</value>
        protected abstract string AddKVPDescription {
            get;
        }

        /// <summary>
        /// Gets the description used when removing a key-value pair.
        /// </summary>
        /// <value>The remove KVP description.</value>
        protected abstract string RemoveKVPDescription {
            get;
        }

        /// <summary>
        /// Gets the description used when changing a key-value pair's name.
        /// </summary>
        /// <value>The change KVP name description.</value>
        protected abstract string ChangeKVPNameDescription {
            get;
        }

        /// <summary>
        /// Gets the description used when changing a key-value pair's value.
        /// </summary>
        /// <value>The change KVP value description.</value>
        protected abstract string ChangeKVPValueDescription {
            get;
        }

        /// <summary>
        /// Gets the default key format.
        /// </summary>
        /// <value>The default key format.</value>
        protected abstract string DefaultKeyFormat {
            get;
        }

        /// <summary>
        /// Gets the default value.
        /// </summary>
        /// <value>The default value.</value>
        protected abstract ValueType DefaultValue {
            get;
        }

        /// <summary>
        /// Creates a key from the given value.
        /// </summary>
        /// <returns>The key.</returns>
        /// <param name="name">The key name.</param>
        protected abstract KeyType CreateKeyFromString(string name);

        /// <summary>
        /// Creates a key from a string.
        /// </summary>
        /// <param name="value">the value.</param>
        protected abstract ValueType CreateValueFromString(string value);

        /// <summary>
        /// The read-only representation of the given key-value pair.
        /// </summary>
        /// <returns>The pair's read-only representation.</returns>
        /// <param name="key">Key.</param>
        /// <param name="value">Value.</param>
        protected abstract string ReadOnlyKVP(KeyType key, ValueType value);

        /// <summary>
        /// Gets the action that is triggered after a key is removed.
        /// </summary>
        /// <value>The remove action.</value>
        protected virtual System.Action<KeyType> GetAddAction(KeyType key)
        {
            return new System.Action<KeyType>((kk) => { });
        }

        /// <summary>
        /// Gets the action that is triggered after a key is removed.
        /// </summary>
        /// <value>The remove action.</value>
        protected virtual System.Action<KeyType> GetRemoveAction(KeyType key)
        {
            return new System.Action<KeyType>((kk) => { });
        }

        /// <summary>
        /// Gets the action that is triggered after a key is removed.
        /// </summary>
        /// <value>The remove action.</value>
        protected virtual System.Action<KeyType, KeyType> RenameAction {
            get {
                return new System.Action<KeyType, KeyType>((oldName, newName) => { });
            }
        }

        /// <summary>
        /// Gets the column types.
        /// </summary>
        /// <value>The column types.</value>
        protected virtual System.Type[] ColumnTypes {
            get {
                return new System.Type[] { typeof(KeyType), typeof(ValueType) };
            }
        }

        /// <summary>
        /// Adds a new row representing the given key.
        /// </summary>
        /// <param name="key">Key.</param>
        protected virtual void AddRow(KeyType key)
        {
            _store.AppendValues(key, DataSource[key]);
        }

        /// <summary>
        /// Adds the columns to the table.
        /// </summary>
        /// <param name="readOnly">If set to <c>true</c>, the columns are read only.</param>
        /// <returns>The next attribute index.</returns>
        protected virtual int AddColumns(bool readOnly)
        {
            int valIndex = 0;
            {
                var changeKeyAction = new System.Action<object, EditedArgs>((object sender, EditedArgs args) => {
                    try {
                        var expr = CreateKeyFromString(args.NewText);
                        _store.GetIterFromString(out TreeIter iter, args.Path);

                        var key = _store.GetValue(iter, 0) as KeyType;
                        if(expr.ToString() != key.ToString()) {
                            if(!DataSource.ContainsKey(expr)) {
                                CommitGuiAction(new ChangeKVPNameAction<KeyType, ValueType>(DataSource,
                                                                                            _entity,
                                                                                            key,
                                                                                            expr,
                                                                                            ChangeKVPNameDescription,
                                                                                            RenameAction));
                                // Panel resetting is already handled by the selection change algorithm, which
                                // forces resets when the selected entity is null (i.e. RootPetriNet selected).
                                if(!(_entity is Model.RootPetriNet)) {
                                    _panel.Reset();
                                }
                            } else {
                                throw new System.Exception(GetNonUniqueKeyMessage(expr));
                            }
                        }
                    } catch(System.Exception e) {
                        _panel.NotifyError(e.Message);
                        args.RetVal = false;
                    }
                });

                var column = new TreeViewColumn();
                column.Title = KeyDescription;

                if(KeySet == null) {
                    var cell = new StringValueCellRenderer<KeyType>();
                    cell.Editable = !readOnly;
                    cell.Edited += (object sender, EditedArgs args) => {
                        if(_panel.IsResetting) {
                            return;
                        }

                        changeKeyAction(sender, args);
                    };

                    column.PackStart(cell, true);
                    column.AddAttribute(cell, "value", valIndex++);
                } else {
                    var cell = new ComboStringValueCellRenderer<KeyType>();
                    cell.Editable = !readOnly;
                    cell.HasEntry = true;
                    cell.TextColumn = 0;
                    var store = new ListStore(typeof(string));
                    cell.Model = store;
                    foreach(var key in KeySet) {
                        store.AppendValues(key);
                    }
                    cell.Edited += (object sender, EditedArgs args) => {
                        if(_panel.IsResetting) {
                            return;
                        }

                        changeKeyAction(sender, args);
                    };
                    column.PackStart(cell, true);
                    column.AddAttribute(cell, "value", valIndex++);
                }
                _view.AppendColumn(column);
            }
            {
                var column = new TreeViewColumn();
                column.Title = ValueDescription;
                var cell = new StringValueCellRenderer<ValueType>();
                cell.Editable = !readOnly;
                cell.Edited += (object sender, EditedArgs args) => {
                    if(_panel.IsResetting) {
                        return;
                    }

                    _store.GetIterFromString(out TreeIter iter, args.Path);
                    var key = _store.GetValue(iter, 0) as KeyType;

                    try {
                        var newVal = CreateValueFromString(args.NewText);
                        CommitGuiAction(new ChangeKVPValueAction<KeyType, ValueType>(DataSource,
                                                                                     _entity,
                                                                                     key,
                                                                                     newVal,
                                                                                     ChangeKVPValueDescription));
                        _store.SetValue(iter, 1, newVal);
                    } catch(System.Exception e) {
                        _panel.NotifyError(e.Message);
                        args.RetVal = false;
                    }
                };
                column.PackStart(cell, true);
                column.AddAttribute(cell, "value", valIndex++);
                _view.AppendColumn(column);
            }

            return valIndex;
        }

        /// <summary>
        /// Adds the widgets composing the panel module to the panel.
        /// </summary>
        public override void Fill()
        {
            bool readOnly = IsReadOnly;
            if(readOnly && DataSource.Count == 0) {
                return;
            }

            CreateLabel(0, Title);

            _view = CreateWidget<TreeView>(true, 0);

            AddColumns(readOnly);

            _store = new ListStore(ColumnTypes);
            _view.Model = _store;

            foreach(var arg in DataSource) {
                AddRow(arg.Key);
            }
            if(!readOnly) {
                var create = new Button(GUIApplication.LabelFromString("+", true));
                create.Clicked += (sender, ev) => {
                    var id = DataSource.Count;
                    KeyType result;
                    do {
                        result = CreateKeyFromString(string.Format(DefaultKeyFormat, id));
                        ++id;
                    } while(DataSource.ContainsKey(result));

                    CommitGuiAction(new AddKVPAction<KeyType, ValueType>(DataSource,
                                                                         _entity,
                                                                         result,
                                                                         DefaultValue,
                                                                         AddKVPDescription,
                                                                         GetAddAction(result),
                                                                         GetRemoveAction(result)));
                    // Panel resetting is already handled by the selection change algorithm, which
                    // forces resets when the selected entity is null (i.e. RootPetriNet selected).
                    if(!(_entity is Model.RootPetriNet)) {
                        _panel.Reset();
                    }
                };
                var remove = new Button(GUIApplication.LabelFromString("-", true));
                remove.Clicked += (sender, ev) => {
                    TreeIter iter;
                    TreePath[] treePath = _view.Selection.GetSelectedRows();

                    for(int i = treePath.Length; i > 0; --i) {
                        _store.GetIter(out iter, treePath[i - 1]);

                        var key = _store.GetValue(iter, 0) as KeyType;

                        CommitGuiAction(new RemoveKVPAction<KeyType, ValueType>(DataSource,
                                                                                _entity,
                                                                                key,
                                                                                RemoveKVPDescription,
                                                                                GetAddAction(key),
                                                                                GetRemoveAction(key)));
                        // Panel resetting is already handled by the selection change algorithm, which
                        // forces resets when the selected entity is null (i.e. RootPetriNet selected).
                        if(!(_entity is Model.RootPetriNet)) {
                            _panel.Reset();
                        }
                    }
                };

                var buttonBox = CreateWidget<HBox>(false, 0, false, 5);
                buttonBox.PackStart(create, false, false, 0);
                buttonBox.PackStart(remove, false, false, 0);
                buttonBox.ShowAll();
            }
        }

        class StringValueCellRenderer<Type> : CellRendererText
        {
            [GLib.Property("value")]
            public Type Key {
                get {
                    return _value;
                }
                set {
                    _value = value;
                    Text = _value.ToString();
                }
            }

            Type _value;
        }

        class ComboStringValueCellRenderer<Type> : CellRendererCombo
        {
            [GLib.Property("value")]
            public Type Key {
                get {
                    return _value;
                }
                set {
                    _value = value;
                    Text = _value.ToString();
                }
            }

            Type _value;
        }

        Model.Entity _entity;

        /// <summary>
        /// The list view
        /// </summary>
        protected TreeView _view;

        /// <summary>
        /// The data store of the editor
        /// </summary>
        protected ListStore _store;
    }
}
