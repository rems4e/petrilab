//
//  EvaluateModule.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-12-29.
//

using System;

using Gtk;

using Petri.Application.Debugger;

namespace Petri.Application.GUI.PropertiesPanel
{
    /// <summary>
    /// A properties module that allows to evaluate a code expression from within the debugger.
    /// </summary>
    public class EvaluateModule : PropertiesPanelModule
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Application.GUI.PropertiesPanel.EvaluateModule"/> class.
        /// </summary>
        /// <param name="pane">The properties panel.</param>
        /// <param name="document">Document.</param>
        /// <param name="controller">The debug controller.</param>
        public EvaluateModule(PropertiesPanel pane, Debugger.DebuggableGUIDocument document, DebugController controller) : base(pane)
        {
            _document = document;
            _controller = controller;
        }

        /// <summary>
        /// Adds the widgets composing the panel module to the panel.
        /// </summary>
        public override void Fill()
        {
            CreateLabel(0, Configuration.GetLocalized("Evaluate expression:"));
            string[] lastEvaluations = (_document.BaseDebugController != null) ? _document.BaseDebugController.LastEvaluations.ToArray() : new string[] { };
            var combo = CreateWidget<ComboBoxEntry>(true, 0, new object[] { lastEvaluations });
            if(_controller.LastEvaluations.Count > 0) {
                combo.Entry.Text = _document.BaseDebugController.LastEvaluations[0];
            }

            if(_document.Settings?.Language == Code.Language.C) {
                CreateLabel(0, Configuration.GetLocalized("With printf format:"));
                _formatEntry = CreateWidget<Entry>(true, 0, "%d");
            }
            Evaluate = CreateWidget<Button>(false, 0, Configuration.GetLocalized("Evaluate"));
            Evaluate.Sensitive = (_controller.Client.CurrentSessionState == DebugClient.SessionState.Started)
            && (_controller.Client.CurrentPetriState == DebugClient.PetriState.Stopped || _controller.Client.CurrentPetriState == DebugClient.PetriState.Paused);

            CreateLabel(0, Configuration.GetLocalized("Result:"));

            _buf = new TextBuffer(new TextTagTable());
            _buf.Text = "";
            var result = CreateWidget<TextView>(true, 0, _buf);
            result.Editable = false;
            result.WrapMode = WrapMode.Word;

            combo.Entry.Activated += (object sender, EventArgs e) => {
                Evaluate.Click();
            };

            Evaluate.Clicked += (sender, ev) => {
                if((_document.BaseDebugController.Client.CurrentSessionState == DebugClient.SessionState.Started)
                   && (_document.BaseDebugController.Client.CurrentPetriState == DebugClient.PetriState.Stopped || _document.BaseDebugController.Client.CurrentPetriState == DebugClient.PetriState.Paused)) {
                    string str = combo.Entry.Text;

                    int pos = _document.BaseDebugController.LastEvaluations.IndexOf(str);
                    if(pos != -1) {
                        _document.BaseDebugController.LastEvaluations.RemoveAt(pos);
                        combo.RemoveText(pos);
                    }
                    _document.BaseDebugController.LastEvaluations.Insert(0, str);
                    combo.PrependText(str);

                    try {
                        Code.Expression expr = _document.EntityFactory.CreateExpressionFromString(str);

                        object[] userData = null;
                        if(_document.Settings.Language == Code.Language.C) {
                            userData = new object[] { _formatEntry.Text };
                        }
                        _document.BaseDebugController.Client.Evaluate(
                            _document.MainDocument.Window.DebugGui.BaseView.CurrentPetriNet,
                            expr,
                            userData
                        );
                    } catch(Exception e) {
                        _buf.Text = e.Message;
                    }
                }
            };
        }

        /// <summary>
        /// Updates the expression evaluation result when it is received.
        /// </summary>
        /// <param name="result">Result.</param>
        /// <param name="userInfo">Additional user info.</param>
        public void OnEvaluate(string result, string userInfo)
        {
            var message = result;
            if(!string.IsNullOrEmpty(userInfo)) {
                message = Configuration.GetLocalized("{0}nnAdditional message: '{1}'", message, userInfo);
            }

            _buf.Text = message;
        }

        /// <summary>
        /// Gets the "evaluate" button.
        /// </summary>
        /// <value>The evaluate.</value>
        public Button Evaluate {
            get;
            private set;
        }

        TextBuffer _buf;
        Entry _formatEntry;
        Debugger.DebuggableGUIDocument _document;
        DebugController _controller;

    }
}
