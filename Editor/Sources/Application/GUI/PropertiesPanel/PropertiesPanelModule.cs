//
//  PropertiesPanelModule.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-12-29.
//

using System;

using Gtk;

using Petri.Model;

namespace Petri.Application.GUI.PropertiesPanel
{
    /// <summary>
    /// A module that represents a section of a properties editor.
    /// </summary>
    public abstract class PropertiesPanelModule
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Application.GUI.PropertiesPanel.PropertiesPanelModule"/> class.
        /// </summary>
        /// <param name="panel">The properties panel.</param>
        protected PropertiesPanelModule(PropertiesPanel panel)
        {
            _panel = panel;
        }

        /// <summary>
        /// Commits the GUI action and update the GUI accordingly (undo/redo submenus etc.).
        /// </summary>
        /// <param name="action">The action.</param>
        /// <param name="doNotFocus">If <c>true</c>, then no focus is given to the action.</param>
        public void CommitGuiAction(GuiAction action, bool doNotFocus = false)
        {
            _panel.CommitGuiAction(action, doNotFocus);
        }

        /// <summary>
        /// Adds the widgets composing the panel module to the panel.
        /// </summary>
        public abstract void Fill();

        /// <summary>
        /// Creates the a label widget with the given indentation (in pixels), the specified text.
        /// The widget is added to the view unless the last parameter is set to <c>true</c>.
        /// </summary>
        /// <returns>The label.</returns>
        /// <param name="indentation">The left indentation of the widget, in pixels.</param>
        /// <param name="text">Text.</param>
        public Label CreateLabel(int indentation, string text)
        {
            var label = GUIApplication.LabelFromString(text);
            AddWidget(label, true, indentation);

            return label;
        }

        /// <summary>
        /// Creates a widget of the given type parameter with the given attributes, and adds it to the view.
        /// </summary>
        /// <returns>The widget.</returns>
        /// <param name="resizeable">If set to <c>true</c> then the widget is resizeable.</param>
        /// <param name="indentation">The left indentation of the widget, in pixels.</param>
        /// <param name="widgetConstructionArgs">Widget construction arguments.</param>
        /// <typeparam name="WidgetType">The type of the widget to be created.</typeparam>
        public WidgetType CreateWidget<WidgetType>(bool resizeable,
                                                   int indentation,
                                                   params object[] widgetConstructionArgs) where WidgetType : Widget
        {
            var w = (WidgetType)Activator.CreateInstance(typeof(WidgetType),
                                                         widgetConstructionArgs);
            _panel.AddWidget(this, w, resizeable, indentation);
            return w;
        }

        /// <summary>
        /// Adds the widget to the view.
        /// </summary>
        /// <param name="w">The widget.</param>
        /// <param name="resizeable">If set to <c>true</c> then the widhet is resizeable.</param>
        /// <param name="indentation">The left indentation of the widget, in pixels.</param>
        public void AddWidget(Widget w, bool resizeable, int indentation)
        {
            _panel.AddWidget(this, w, resizeable, indentation);
        }


        /// <summary>
        /// The properties panel.
        /// </summary>
        protected PropertiesPanel _panel;
    }

    /// <summary>
    /// A properties module that handles exit points.
    /// </summary>
    public class ExitPointModule : PropertiesPanelModule
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.PropertiesPanel.ExitPointModule"/> class.
        /// </summary>
        /// <param name="panel">The properties panel.</param>
        /// <param name="e">The exit point.</param>
        public ExitPointModule(PropertiesPanel panel, ExitPoint e) : base(panel)
        {
        }

        /// <summary>
        /// Adds the widgets composing the panel module to the panel.
        /// </summary>
        public override void Fill()
        {
            CreateLabel(0, Configuration.GetLocalized("Petri net's exit point"));
        }
    }

    /// <summary>
    /// A properties module that handles multiple entities selection.
    /// </summary>
    public class MultipleSelectionModule : PropertiesPanelModule
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.PropertiesPanel.MultipleSelectionModule"/> class.
        /// </summary>
        /// <param name="panel">The properties panel.</param>
        public MultipleSelectionModule(PropertiesPanel panel) : base(panel)
        {
        }

        /// <summary>
        /// Adds the widgets composing the panel module to the panel.
        /// </summary>
        public override void Fill()
        {
            CreateLabel(0, Configuration.GetLocalized("Select only one object"));
        }
    }

    /// <summary>
    /// A properties panel that handles empty selection.
    /// </summary>
    public class EmptySelectionModule : PropertiesPanelModule
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.PropertiesPanel.EmptySelectionModule"/> class.
        /// </summary>
        /// <param name="panel">The properties panel.</param>
        public EmptySelectionModule(PropertiesPanel panel) : base(panel)
        {
        }

        /// <summary>
        /// Adds the widgets composing the panel module to the panel.
        /// </summary>
        public override void Fill()
        {
            CreateLabel(0, Configuration.GetLocalized("Select an object to edit"));
        }
    }

}
