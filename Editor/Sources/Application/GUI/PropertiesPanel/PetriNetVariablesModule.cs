//
//  PetriNetVariablesModule.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2017-04-17.
//

using System;
using System.Collections.Generic;

using Gtk;

using Petri.Code;
using Petri.Model;

namespace Petri.Application.GUI.PropertiesPanel
{
    /// <summary>
    /// A properties panel module that allows to view/change the variables of a petri net.
    /// </summary>
    public class PetriNetVariablesModule : AKeyValueEditorModule<VariableExpression, Int64>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Application.GUI.PropertiesPanel.PetriNetVariablesModule"/> class.
        /// </summary>
        /// <param name="pane">The properties panel.</param>
        /// <param name="petriNet">The petri net.</param>
        public PetriNetVariablesModule(PropertiesPanel pane, RootPetriNet petriNet) : base(pane, petriNet)
        {
            _petriNet = petriNet;
        }

        /// <summary>
        /// Gets the title of the module.
        /// </summary>
        /// <value>The title.</value>
        protected override string Title => Configuration.GetLocalized("Petri net's variables:");

        /// <summary>
        /// Gets the key-value pairs' source.
        /// </summary>
        /// <value>The data source.</value>
        protected override IDictionary<VariableExpression, Int64> DataSource => _petriNet.Variables;

        /// <summary>
        /// Gets the key set.
        /// </summary>
        /// <value>The key set.</value>
        protected override string[] KeySet => null;

        /// <summary>
        /// Gets the key description.
        /// </summary>
        /// <value>The key description.</value>
        protected override string KeyDescription => Configuration.GetLocalized("Name");

        /// <summary>
        /// Gets the value description.
        /// </summary>
        /// <value>The value description.</value>
        protected override string ValueDescription => Configuration.GetLocalized("Default value");

        /// <summary>
        /// Gets the message for when a non-unique key is being inserted.
        /// </summary>
        /// <value>The non unique key message.</value>
        protected override string GetNonUniqueKeyMessage(VariableExpression key)
        {
            return Configuration.GetLocalized("A variable named {0} already exists!", key.ToString());
        }

        /// <summary>
        /// Gets the description used when creating a key-value pair.
        /// </summary>
        /// <value>The add KVP description.</value>
        protected override string AddKVPDescription => Configuration.GetLocalized("Add a variable");

        /// <summary>
        /// Gets the description used when removing a key-value pair.
        /// </summary>
        /// <value>The remove KVP description.</value>
        protected override string RemoveKVPDescription => Configuration.GetLocalized("Remove the variable");

        /// <summary>
        /// Gets the description used when changing a key-value pair's name.
        /// </summary>
        /// <value>The change KVP name description.</value>
        protected override string ChangeKVPNameDescription => Configuration.GetLocalized("Change variable's name");

        /// <summary>
        /// Gets the description used when changing a key-value pair's value.
        /// </summary>
        /// <value>The change KVP value description.</value>
        protected override string ChangeKVPValueDescription => Configuration.GetLocalized("Change variable's value");

        /// <summary>
        /// Gets the default key format.
        /// </summary>
        /// <value>The default key format.</value>
        protected override string DefaultKeyFormat => "$var{0}";

        /// <summary>
        /// Gets the default value.
        /// </summary>
        /// <value>The default value.</value>
        protected override Int64 DefaultValue => 0;

        /// <summary>
        /// Creates a key from the given value.
        /// </summary>
        /// <returns>The key.</returns>
        /// <param name="name">The key name.</param>
        protected override VariableExpression CreateKeyFromString(string name)
        {
            try {
                return Expression.CreateFromString<VariableExpression>(name, _petriNet.Document.Settings.Language);
            } catch {
                throw new Exception(Configuration.GetLocalized("The variable's name is invalid!"));
            }
        }

        /// <summary>
        /// Creates a key from a string.
        /// </summary>
        /// <param name="value">the value.</param>
        protected override Int64 CreateValueFromString(string value)
        {
            try {
                return System.Int64.Parse(value);
            } catch {
                throw new Exception(Configuration.GetLocalized("Invalid value!"));
            }
        }

        /// <summary>
        /// The read-only representation of the given key-value pair.
        /// </summary>
        /// <returns>The pair's read-only representation.</returns>
        /// <param name="key">Key.</param>
        /// <param name="value">Value.</param>
        protected override string ReadOnlyKVP(VariableExpression key, Int64 value)
        {
            return Configuration.GetLocalized("{0}, default value {1}", key.ToString(), value.ToString());
        }

        /// <summary>
        /// Gets the action that is triggered after a key is removed.
        /// </summary>
        /// <value>The remove action.</value>
        protected override Action<VariableExpression> GetAddAction(VariableExpression key)
        {
            var isParameter = _petriNet.Parameters.Contains(key);
            return new Action<VariableExpression>((kk) => {
                if(isParameter) {
                    _petriNet.Parameters.Add(key);
                }
            });
        }

        /// <summary>
        /// Gets the action that is triggered after a key is removed.
        /// </summary>
        /// <value>The remove action.</value>
        protected override Action<VariableExpression> GetRemoveAction(VariableExpression key)
        {
            var isParameter = _petriNet.Parameters.Contains(key);
            return new Action<VariableExpression>((kk) => {
                if(isParameter) {
                    _petriNet.Parameters.Remove(key);
                }
            });
        }

        /// <summary>
        /// Gets the action that is triggered after a key is removed.
        /// </summary>
        /// <value>The remove action.</value>
        protected override Action<VariableExpression, VariableExpression> RenameAction {
            get {
                return new Action<VariableExpression, VariableExpression>((oldName, newName) => {
                    if(_petriNet.Parameters.Contains(oldName)) {
                        _petriNet.Parameters.Remove(oldName);
                        _petriNet.Parameters.Add(newName);
                    }
                });
            }
        }

        /// <summary>
        /// Gets the column types.
        /// </summary>
        /// <value>The column types.</value>
        protected override System.Type[] ColumnTypes {
            get {
                return new System.Type[] { typeof(VariableExpression), typeof(Int64), typeof(bool) };
            }
        }

        /// <summary>
        /// Adds a new row representing the given key.
        /// </summary>
        /// <param name="key">Key.</param>
        protected override void AddRow(VariableExpression key)
        {
            _store.AppendValues(key, DataSource[key], _petriNet.Parameters.Contains(key));
        }

        /// <summary>
        /// Adds the columns to the table.
        /// </summary>
        /// <param name="readOnly">If set to <c>true</c>, the columns are read only.</param>
        /// <returns>The next attribute index.</returns>
        protected override int AddColumns(bool readOnly)
        {
            var valIndex = base.AddColumns(readOnly);
            {
                int idx = valIndex;
                var column = new TreeViewColumn();
                column.Title = Configuration.GetLocalized("Parameter");
                var cell = new CellRendererToggle();
                cell.Sensitive = !readOnly;
                cell.Toggled += (object o, ToggledArgs args) => {
                    if(_panel.IsResetting) {
                        return;
                    }

                    _store.GetIterFromString(out TreeIter iter, args.Path);
                    var key = _store.GetValue(iter, 0) as VariableExpression;

                    if(_petriNet.Parameters.Contains(key)) {
                        CommitGuiAction(new RemoveParameterAction(_petriNet, key));
                        _store.SetValue(iter, idx, false);
                    } else {
                        CommitGuiAction(new AddParameterAction(_petriNet, key));
                        _store.SetValue(iter, idx, true);
                    }

                };
                column.PackStart(cell, true);
                column.AddAttribute(cell, "active", valIndex++);
                _view.AppendColumn(column);
            }

            return valIndex;
        }

        RootPetriNet _petriNet;
    }
}
