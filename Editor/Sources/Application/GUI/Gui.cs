//
//  Gui.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2014-11-24.
//

using System;

using Gtk;

namespace Petri.Application.GUI
{
    /// <summary>
    /// The base the application's GUIs.
    /// </summary>
    public abstract class Gui : VBox
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.Gui"/> class.
        /// </summary>
        protected Gui()
        {
            _toolbar = new Toolbar();
            _toolbar.ToolbarStyle = ToolbarStyle.Icons;
            PackStart(_toolbar, false, false, 0);

            _status = GUIApplication.LabelFromString("");
            PackEnd(_status, false, true, 5);

            _scroll = new ScrolledWindow();
            _scroll.Hadjustment.ValueChanged += (object sender, EventArgs e) => {
                BaseView.Redraw();
            };
            _scroll.Vadjustment.ValueChanged += (object sender, EventArgs e) => {
                BaseView.Redraw();
            };
            _scroll.SetPolicy(PolicyType.Automatic, PolicyType.Automatic);

            HPaned = new HPaned();
            PackStart(HPaned, true, true, 0);

            VPaned = new VPaned();
            HPaned.Pack1(VPaned, true, true);

            _editor = new Fixed();
            HPaned.Pack2(_editor, false, true);

            // Dirty hack… This is to acctually enforce common position between GUI VPaned, over the
            // complicated GTK's requisition/allocation scheme.
            VPaned.AddNotification("position", (o, args) => {
                if(ShouldUpdatePosition) {
                    _lastValidPosition = VPaned.Position;
                } else {
                    VPaned.Position = _lastValidPosition;
                }
            });
        }

        /// <summary>
        /// <c>false</c> if a modification to the <c>Prosition</c> property should be discarded.
        /// </summary>
        /// <value><c>true</c> if a position update should be kept; otherwise, <c>false</c>.</value>
        public bool ShouldUpdatePosition {
            get;
            set;
        } = true;

        /// <summary>
        /// The scroll view enclosing the petri net.
        /// </summary>
        /// <value>The scrolled window.</value>
        public ScrolledWindow ScrolledWindow {
            get {
                return _scroll;
            }
        }

        /// <summary>
        /// Gets the petri view.
        /// </summary>
        /// <value>The base view.</value>
        public abstract GUIPetriView BaseView {
            get;
        }

        /// <summary>
        /// Gets the properties panel.
        /// </summary>
        /// <value>The base properties panel.</value>
        public abstract PropertiesPanel.PropertiesPanel BasePropertiesPanel {
            get;
        }

        /// <summary>
        /// Gets the contextual editor.
        /// </summary>
        /// <value>The properties panel's view.</value>
        public Fixed PropertiesPanelView {
            get {
                return _editor;
            }
        }

        /// <summary>
        /// Gets the paned view containing the properties editor on its right pane.
        /// </summary>
        /// <value>The paned.</value>
        public HPaned HPaned {
            get;
            protected set;
        }

        /// <summary>
        /// Gets the paned view containing the find results on its bottom pane.
        /// </summary>
        /// <value>The paned.</value>
        public VPaned VPaned {
            get;
            protected set;
        }

        /// <summary>
        /// Sets the status message of the application.
        /// </summary>
        /// <value>The status.</value>
        public string Status {
            set {
                _status.Text = value;
            }
        }

        /// <summary>
        /// Updates the GUI (mainly menu items and toolbars).
        /// </summary>
        public virtual void Update()
        {

        }

        /// <summary>
        /// Copies something to the clipboard.
        /// </summary>
        public virtual void Copy()
        {

        }

        /// <summary>
        /// Cuts something to the clipboard.
        /// </summary>
        public virtual void Cut()
        {

        }

        /// <summary>
        /// Pastes something from the clipboard.
        /// </summary>
        public virtual void Paste()
        {

        }

        /// <summary>
        /// Selects all.
        /// </summary>
        public virtual void SelectAll()
        {

        }

        /// <summary>
        /// The status label of the editor view.
        /// </summary>
        protected Label _status;

        /// <summary>
        /// The selected entity's properties editor's view.
        /// </summary>
        protected Fixed _editor;

        /// <summary>
        /// The scrolled window enclosing the petri net's view.
        /// </summary>
        protected ScrolledWindow _scroll;

        /// <summary>
        /// The window's toolbar.
        /// </summary>
        protected Toolbar _toolbar;

        int _lastValidPosition;
    }
}

