//
//  GUIConsoleOutput.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2017-12-05.
//

using System;

namespace Petri.Application.GUI
{
    /// <summary>
    /// Text output to the GUI console.
    /// </summary>
    public class GUIConsoleOutput : IOutput
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Editor.GUI.GUIConsoleOutput"/> class.
        /// </summary>
        /// <param name="window">Document.</param>
        public GUIConsoleOutput(MainWindow window)
        {
            _window = window;
        }

        /// <summary>
        /// Write the specified format and args.
        /// </summary>
        /// <param name="format">Format.</param>
        /// <param name="args">Arguments.</param>
        public void Write(string format, params object[] args)
        {
            var result = string.Format(format, args);
            Console.Write(result);
            GUIApplication.RunOnUIThread(() => {
                _window.AppendConsoleContent(result);
            });
        }

        /// <summary>
        /// Writes the specified value and args to the error output stream.
        /// </summary>
        /// <param name="format">Format.</param>
        /// <param name="args">Arguments.</param>
        public void WriteError(string format, params object[] args)
        {
            var result = string.Format(format, args);
            Console.Error.Write(result);
            GUIApplication.RunOnUIThread(() => {
                _window.AppendConsoleContent(result);
            });
        }

        /// <summary>
        /// Writes the specified format and args, plus a new line character.
        /// </summary>
        /// <param name="format">Format.</param>
        /// <param name="args">Arguments.</param>
        public void WriteLine(string format, params object[] args)
        {
            Write(format, args);
            WriteLine();
        }

        /// <summary>
        /// Writes a new line character.
        /// </summary>
        public void WriteLine()
        {
            Write("\n");
        }

        /// <summary>
        /// Writes the specified value and args to the error output stream, plus a new line character.
        /// </summary>
        /// <param name="format">Format.</param>
        /// <param name="args">Arguments.</param>
        public void WriteErrorLine(string format, params object[] args)
        {
            WriteError(format, args);
            WriteErrorLine();
        }

        /// <summary>
        /// Writes the specified value and args as a warning, plus a new line character.
        /// </summary>
        /// <param name="format">Format.</param>
        /// <param name="args">Arguments.</param>
        public void WriteWarningLine(string format, params object[] args)
        {
            Write(format, args);
        }

        /// <summary>
        /// Writes a new line character to the error output stream.
        /// </summary>
        public void WriteErrorLine()
        {
            WriteError("\n");
        }

        readonly MainWindow _window;
    }
}

