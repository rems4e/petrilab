//
//  WelcomeScreen.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-04-23.
//

using System;
using System.Collections.Generic;
using System.Linq;

using Gtk;

namespace Petri.Application.GUI
{
    /// <summary>
    /// A welcome window that opens at application startup and allows fast document creation and recent documents opening.
    /// </summary>
    public class WelcomeScreen : MenuBarWindow
    {
        /// <summary>
        /// The width, in pixels, of the recent documents' table.
        /// </summary>
        public static readonly int RecentWidth = 350;

        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.GUI.WelcomeScreen"/> class.
        /// </summary>
        public WelcomeScreen()
        {
            Decorated = false;

            DefaultWidth = 600;
            DefaultHeight = 400;

            // Allows the window to be dragged by dragging its content.
            ButtonPressEvent += (object o, ButtonPressEventArgs args) => {
                BeginMoveDrag(0, (int)args.Event.XRoot, (int)args.Event.YRoot, 0);
            };

            KeyPressEvent += (object o, KeyPressEventArgs args) => {
                if(args.Event.Key == Gdk.Key.Escape) {
                    TryClose();
                }
            };

            SetPosition(WindowPosition.Center);
            int x, y;
            GetPosition(out x, out y);
            Move(x, 2 * y / 3);

            _vbox.BorderWidth = 15;

            var hbox = new HBox(false, 20);
            _vbox.PackStart(hbox);

            // New documents pane
            {
                var vbox2 = new VBox(false, 5);
                hbox.PackStart(vbox2, false, false, 0);

                var vbox3 = new VBox(false, 0);

                var buf = Gdk.Pixbuf.LoadFromResource("splash");
                buf = buf.ScaleSimple(buf.Width / 4, buf.Height / 4, Gdk.InterpType.Bilinear);
                var splash = new Image(buf);

                var label = GUIApplication.LabelFromMarkup("<span foreground=\"#505050\" size=\"xx-large\">"
                                                           + GUIApplication.SafeMarkupFromLocalized("Welcome to PetriLab")
                                                           + "</span>", true);
                var version = GUIApplication.LabelFromMarkup("<span foreground=\"#999999\" size=\"large\">"
                                                             + GUIApplication.SafeMarkupFromLocalized("Version {0}",
                                                                                                      Application.Version)
                                                             + "</span>", true);
                vbox3.PackStart(splash, false, false, 0);
                vbox3.PackStart(label, false, false, 5);
                vbox3.PackStart(version, false, false, 10);
                vbox2.PackStart(vbox3, true, true, 0);

                if(!GUIApplication.IsReadOnly) {
                    var createLabel = GUIApplication.LabelFromString(Configuration.GetLocalized("Create a new petri net:"));

                    var table = new TreeView();
                    table.HeadersVisible = false;

                    TreeViewColumn c = new TreeViewColumn();
                    var languageCell = new CellRendererText();
                    c.PackStart(languageCell, true);
                    c.AddAttribute(languageCell, "text", 0);

                    table.AppendColumn(c);
                    var store = new ListStore(typeof(string));
                    table.Model = store;

                    foreach(Code.Language language in Enum.GetValues(typeof(Code.Language))) {
                        store.AppendValues(DocumentSettings.LanguageName(language));
                    }

                    var path = new TreePath(new int[] { new List<Code.Language>(Enum.GetValues(typeof(Code.Language)).Cast<Code.Language>()).FindIndex(
                        (Code.Language language) => language == Configuration.DefaultLanguage)
                    });
                    table.Selection.SelectPath(path);

                    var hbox3 = new HBox(false, 0);
                    var toggle = new CheckButton(Configuration.GetLocalized("Hosted by the editor"));
                    table.CursorChanged += (object sender, EventArgs e) => {
                        var selection = table.Selection;
                        TreeIter iter;
                        selection.GetSelected(out iter);
                        var language = DocumentSettings.LanguageFromName(store.GetValue(iter, 0) as string);
                        if(language == Code.Language.EmbeddedC) {
                            toggle.Sensitive = false;
                        } else {
                            toggle.Sensitive = true;
                        }
                    };

                    var create = new Button(Configuration.GetLocalized("Create"));
                    create.Clicked += (object sender, EventArgs e) => {
                        var selection = table.Selection;
                        TreeIter iter;
                        selection.GetSelected(out iter);
                        var language = DocumentSettings.LanguageFromName(store.GetValue(iter, 0) as string);
                        GUIApplication.CreateDocument(language, toggle.Active);
                    };

                    table.RowActivated += (object o, RowActivatedArgs args) => {
                        TreeIter iter;
                        store.GetIter(out iter, args.Path);
                        var language = DocumentSettings.LanguageFromName(store.GetValue(iter, 0) as string);
                        GUIApplication.CreateDocument(language, toggle.Active);
                    };


                    hbox3.PackStart(toggle, false, false, 0);
                    hbox3.PackEnd(create, false, false, 0);

                    vbox2.PackStart(createLabel, false, false, 0);
                    vbox2.PackStart(table, false, false, 0);
                    vbox2.PackStart(hbox3, false, false, 0);
                } else {
                    var readOnlyLabel = GUIApplication.LabelFromMarkup("<i><span foreground=\"#505050\" size=\"x-large\">"
                       + GUIApplication.SafeMarkupFromLocalized("Read Only mode")
                       + "</span></i>", true);
                    vbox3.PackStart(readOnlyLabel, false, false, 5);
                }
            }

            // Recent documents pane
            {
                var vbox2 = new VBox(false, 5);
                hbox.PackStart(vbox2, false, false, 0);

                var label = GUIApplication.LabelFromString(Configuration.GetLocalized("Recent documents"));

                var table = new TreeView();
                table.SetSizeRequest(RecentWidth, -1);
                table.HeadersVisible = false;

                TreeViewColumn c = new TreeViewColumn();
                var docCell = new DocumentCellRenderer();

                c.PackStart(docCell, true);
                c.AddAttribute(docCell, "text", 0);

                table.AppendColumn(c);
                _recentDocumentsStore = new ListStore(typeof(string));
                table.Model = _recentDocumentsStore;

                table.RowActivated += (object o, RowActivatedArgs args) => {
                    TreeIter iter;
                    _recentDocumentsStore.GetIter(out iter, args.Path);
                    GUIApplication.OpenDocument(_recentDocumentsStore.GetValue(iter, 0) as string);
                };

                var hbox2 = new HBox(false, 0);
                var clearRecents = new Button(GUIApplication.LabelFromString(Configuration.GetLocalized("Clear recent documents", true)));
                clearRecents.Clicked += (sender, e) => {
                    GUIApplication.ClearRecentDocuments();
                    UpdateRecentDocuments();
                };
                hbox2.PackStart(clearRecents, false, false, 0);

                var open = new Button(GUIApplication.LabelFromString(Configuration.GetLocalized("Open…", true)));
                hbox2.PackEnd(open, false, false, 0);
                open.Clicked += (sender, e) => {
                    GUIApplication.OpenDocument();
                };

                vbox2.PackStart(label, false, false, 0);
                vbox2.PackStart(table, true, true, 0);
                vbox2.PackStart(hbox2, false, false, 0);
            }
        }

        /// <summary>
        /// Tries to close the window. If the window should not close, then this must return <c>false</c>. Else, this must return <c>true</c> and perform cleanup code.
        /// </summary>
        /// <returns><c>true</c>, the window should be closed, <c>false</c> otherwise.</returns>
        public override bool TryClose()
        {
            Hide();
            if(GUIApplication.Documents.Count == 0) {
                GUIApplication.SaveAndQuit();
            }
            return true;
        }

        /// <summary>
        /// Gets the menu bar of the welcome screen.
        /// </summary>
        /// <value>The menu bar.</value>
        public MenuBar MenuBar {
            get {
                return _menuBar;
            }
        }

        /// <summary>
        /// Updates the recent documents.
        /// </summary>
        public override void UpdateRecentDocuments()
        {
            base.UpdateRecentDocuments();
            _recentDocumentsStore.Clear();
            foreach(var doc in GUIApplication.RecentDocuments) {
                _recentDocumentsStore.AppendValues(doc.Path);
            }
        }

        Gtk.ListStore _recentDocumentsStore;
    }

    /// <summary>
    /// A cell rendere that draw a document name and its path.
    /// </summary>
    class DocumentCellRenderer : CellRenderer
    {
        /// <summary>
        /// Gets the size.
        /// </summary>
        /// <param name="widget">Widget.</param>
        /// <param name="cell_area">Cell area.</param>
        /// <param name="x_offset">X offset.</param>
        /// <param name="y_offset">Y offset.</param>
        /// <param name="width">Width.</param>
        /// <param name="height">Height.</param>
        public override void GetSize(Widget widget,
                                     ref Gdk.Rectangle cell_area,
                                     out int x_offset,
                                     out int y_offset,
                                     out int width,
                                     out int height)
        {
            base.GetSize(widget, ref cell_area, out x_offset, out y_offset, out width, out height);

            width = WelcomeScreen.RecentWidth;
            height = 32;
        }

        /// <summary>
        /// Render the specified window, widget, background_area, cell_area, expose_area and flags.
        /// </summary>
        /// <param name="window">Window.</param>
        /// <param name="widget">Widget.</param>
        /// <param name="background_area">Background area.</param>
        /// <param name="cell_area">Cell area.</param>
        /// <param name="expose_area">Expose area.</param>
        /// <param name="flags">Flags.</param>
        protected override void Render(Gdk.Drawable window,
                                       Widget widget,
                                       Gdk.Rectangle background_area,
                                       Gdk.Rectangle cell_area,
                                       Gdk.Rectangle expose_area,
                                       CellRendererState flags)
        {
            base.Render(window, widget, background_area, cell_area, expose_area, flags);

            using(var layout = new Pango.Layout(widget.PangoContext)) {
                layout.Alignment = Pango.Alignment.Left;
                var text = GUIApplication.SafeMarkupFromString(System.IO.Path.GetFileNameWithoutExtension(Text)) + "\n<span foreground=\"grey\" size=\"small\">"
                               + GUIApplication.SafeMarkupFromString(Application.ShortPath(Text)) + "</span>";
                layout.SetMarkup(text);

                StateType state = flags.HasFlag(CellRendererState.Selected) ?
                    widget.IsFocus ? StateType.Selected : StateType.Active : StateType.Normal;

                window.DrawLayout(widget.Style.TextGC(state), cell_area.X, cell_area.Y, layout);
            }
        }

        /// <summary>
        /// Gets or sets the text of the cell.
        /// </summary>
        /// <value>The text.</value>
        [GLib.Property("text")]
        public string Text {
            get;
            set;
        }
    }
}

