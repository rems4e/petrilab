//
//  DebuggableHeadlessDocument.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-02-25.
//

using System;
using System.Threading.Tasks;

using VariablesList = System.Collections.Generic.List<System.Tuple<Petri.Model.RootPetriNet, Petri.Code.VariableExpression, string>>;

namespace Petri.Application.CLI
{
    /// <summary>
    /// A headless document that has a debug controller.
    /// </summary>
    public class DebuggableHeadlessDocument : HeadlessDocument, Petri.Application.Debugger.IDebuggable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.CLI.DebuggableHeadlessDocument"/> class.
        /// </summary>
        /// <param name="path">The path to the document to debug.</param>
        /// <param name="inputManager">The input manager.</param>
        public DebuggableHeadlessDocument(string path, Debugger.InputManager inputManager) : base(path)
        {
            DebugController = new Debugger.DebugController(this, inputManager);
            Output = DebugController;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Application.CLI.DebuggableHeadlessDocument"/> class.
        /// </summary>
        /// <param name="inputManager">The input manager.</param>
        public DebuggableHeadlessDocument(Debugger.InputManager inputManager) : base(Code.Language.Cpp)
        {
            DebugController = new Debugger.DebugController(this, inputManager);
            Output = DebugController;
        }

        /// <summary>
        /// Gets the base debug controller.
        /// </summary>
        /// <value>The base debug controller.</value>
        public Petri.Application.Debugger.DebugController BaseDebugController {
            get {
                return DebugController;
            }
        }

        /// <summary>
        /// Gets the CLI debug controller.
        /// </summary>
        /// <value>The debug controller.</value>
        public Debugger.DebugController DebugController {
            get;
            private set;
        }

        /// <summary>
        /// Launches the debugger's loop, and reloads a document if asked.
        /// </summary>
        /// <param name="path">The path to the document to load, or <c>null</c> if no document is to be loaded.</param>
        public static int DebuggerLoop(string path)
        {
            var inputManager = new Debugger.InputManager();
            while(true) {
                DebuggableHeadlessDocument toDebug;
                if(path != "") {
                    try {
                        toDebug = new DebuggableHeadlessDocument(path, inputManager);
                    } catch(Exception e) {
                        toDebug = new DebuggableHeadlessDocument(inputManager);
                        toDebug.DebugController.WriteErrorLine("{0}", Configuration.GetLocalized("Could not load the document: {0}", e.Message));
                    }
                } else {
                    toDebug = new DebuggableHeadlessDocument(inputManager);
                }

                var result = toDebug.DebugController.Debug();

                if(result.Item2 != null) {
                    path = result.Item2;
                } else {
                    return result.Item1;
                }
            }
        }

        /// <summary>
        /// Invoked when an attempt will be made to attach to a debug host.
        /// </summary>
        public void OnStartAttachAttempt()
        {
        }

        /// <summary>
        /// Whether to interrupt a debugger attaching procedure
        /// </summary>
        /// <returns><c>true</c>, if attach was interrupted, <c>false</c> otherwise.</returns>
        public bool InterruptAttach()
        {
            return DebugController.Interrupt;
        }

        /// <summary>
        /// Invoked when a previously start attempt to attach to a debug host comes to and end.
        /// </summary>
        /// <param name="success">If set to <c>true</c> then the attempt was successful.</param>
        public void OnAttachAttemptEnd(bool success)
        {

        }

        /// <summary>
        /// Called when the state of the petri net or the session is changed.
        /// </summary>
        public void NotifyStateChanged()
        {
            if(DebugController.Client.CurrentSessionState == Petri.Application.Debugger.DebugClient.SessionState.Stopped) {
                DebugController.NotifyDetached();
            }
        }

        /// <summary>
        /// Asks the user if regenerating the dynamic library should be attempted.
        /// </summary>
        /// <returns><c>true</c>, if we want to reload the lib, <c>false</c> otherwise.</returns>
        public Task<bool> ShouldAttemptDylibReload()
        {
            Output.WriteLine("{0}", Configuration.GetLocalized("Unable to load the dynamic library! Try to compile it again."));
            return Task.FromResult(
                DebugController.PromptYesNo(Configuration.GetLocalized("Do you want to try to recompile the petri net?"))
            );
        }

        /// <summary>
        /// Notifies the user from an unrecoverable error.
        /// </summary>
        /// <param name="message">The error message.</param>
        public void NotifyUnrecoverableError(string message)
        {
            DebugController.NotifyUnrecoverableError(Configuration.GetLocalized("Error: {0}", message));
        }

        /// <summary>
        /// Notifies a new status message.
        /// </summary>
        /// <param name="message">Message.</param>
        public void NotifyStatusMessage(string message)
        {
            Output.WriteLine("{0}", Configuration.GetLocalized("Status message incoming: {0}", message));
        }

        /// <summary>
        /// Notifies that a code expression was successfully evaluated.
        /// </summary>
        /// <param name="value">The evaluation result.</param>
        /// <param name="userInfo">Additional user info.</param>
        public void NotifyEvaluated(string value, string userInfo)
        {
            DebugController.EvalResult = Tuple.Create(value, userInfo);
        }

        /// <summary>
        /// Notifies that the value of the petri net's variables have just been retrieved.
        /// </summary>
        /// <param name="variables">The list of key-value pairs.</param>
        public void NotifyVariables(VariablesList variables)
        {
            DebugController.Vars = variables;
        }

        /// <summary>
        /// Notifies an error in the debug server.
        /// </summary>
        /// <param name="message">Message.</param>
        public void NotifyServerError(string message)
        {
            Output.WriteErrorLine("{0}", Configuration.GetLocalized("An error occurred in the debugger: {0}",
                                                                    message));
            if(message == "You are trying to run a Petri net that is different from the one which is compiled!") {
                if(DebugController.PromptYesNo(Configuration.GetLocalized("Do you want to try to recompile the petri net and run it afterwards?"))) {
                    Application.SwallowAsync(DebugController.Client.ReloadPetri(true));
                } else {
                    DebugController.Client.StopPetri();
                }
            }
        }

        /// <summary>
        /// Notifies the user that the current action will cause the debug server's process to be orphaned, and aks him
        /// whether to continue or not.
        /// </summary>
        /// <returns><c>true</c>, if the process should be orphaned, <c>false</c> if the current action should be cancelled.</returns>
        public bool ShouldOrphanProcess()
        {
            // Always allow the process' release, as it will not survive to the debugger, and we assume the CLI user knows his actions.
            return true;
        }

        /// <summary>
        /// Notifies a new state have been activated in the petri net, or a state have been deactivated.
        /// </summary>
        public void NotifyActiveStatesChanged()
        {
        }

    }
}
