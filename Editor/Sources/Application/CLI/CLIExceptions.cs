//
//  CLIExceptions.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-06-11.
//

using System;

namespace Petri.Application.CLI
{
    /// <summary>
    /// An exception used to convey information when the app is run in a terminal
    /// </summary>
    public class CLIException : BaseException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Application.CLI.CLIException"/> class.
        /// </summary>
        /// <param name="message">The message of the exception.</param>
        public CLIException(string message = "") : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Application.CLI.CLIException"/> class.
        /// </summary>
        /// <param name="message">The message of the exception.</param>
        /// <param name="inner">The inner exception.</param>
        public CLIException(string message, Exception inner) : base(message, inner)
        {
        }
    }

    /// <summary>
    /// Wrong invocation of the program.
    /// </summary>
    public class UsageException : CLIException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Application.CLI.UsageException"/> class.
        /// </summary>
        /// <param name="id">An id used to distinguish the place where the exception was thrown.</param>
        /// <param name="message">Message.</param>
        /// <param name="args">Arguments.</param>
        public UsageException(int id, string message, params object[] args) : base(string.Format(message, args))
        {
            ReturnCode = CLIApplication.ArgumentError + id;
        }

        /// <summary>
        /// Gets the return code.
        /// </summary>
        /// <value>The return code.</value>
        public int ReturnCode {
            get;
            private set;
        }
    }

    /// <summary>
    /// Unable to load the document.
    /// </summary>
    public class DocumentLoadException : CLIException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Application.CLI.DocumentLoadException"/> class.
        /// </summary>
        /// <param name="code">Code.</param>
        /// <param name="e">The inner exception.</param>
        public DocumentLoadException(int code, Exception e) : base(string.Format("Could not load the document: {0}.", e.Message), e)
        {
            ReturnCode = code;
        }

        /// <summary>
        /// Gets the return code.
        /// </summary>
        /// <value>The return code.</value>
        public int ReturnCode {
            get;
            private set;
        }
    }

    /// <summary>
    /// Compilation failure exception.
    /// </summary>
    public class CompilationFailureException : CLIException
    {
    }

    /// <summary>
    /// Deployment failure exception.
    /// </summary>
    public class DeploymentFailureException : CLIException
    {
    }

    /// <summary>
    /// Execution failure exception.
    /// </summary>
    public class ExecutionFailureException : CLIException
    {
    }

    /// <summary>
    /// Unable to load the dynamic library.
    /// </summary>
    public class LoadLibFailureException : CLIException
    {
    }
}

