//
//  HeadlessDocument.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-04-09.
//

namespace Petri.Application.CLI
{
    /// <summary>
    /// A headless petri net document that outputs to the console.
    /// </summary>
    public class HeadlessDocument : LocalDocument
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.CLI.HeadlessDocument"/> class.
        /// </summary>
        /// <param name="language">The language the petri net will use.</param>
        public HeadlessDocument(Code.Language language) : base(language)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.CLI.HeadlessDocument"/> class.
        /// </summary>
        /// <param name="path">Path to the document.</param>
        public HeadlessDocument(string path) : base(path)
        {
        }

        /// <summary>
        /// Shows the compilation status to the user.
        /// </summary>
        /// <param name="status">The compilation success status.</param>
        /// <param name="output">Output.</param>
        public override void EchoCompilationStatus(Compiler.CompilationStatus status, string output)
        {
            if(status == Compiler.CompilationStatus.Error) {
                Output.WriteErrorLine("{0}\n{1}",
                                      Configuration.GetLocalized("Compilation failed."),
                                      output);
            } else if(status == Compiler.CompilationStatus.Warning) {
                Output.WriteLine("{0}\n{1}",
                                 Configuration.GetLocalized("Some warnings were generated."),
                                 output);
            }
        }

        /// <summary>
        /// Notifies an error to the user.
        /// </summary>
        /// <param name="error">Error.</param>
        public override void NotifyError(string error)
        {
            Output.WriteErrorLine("{0}", Configuration.GetLocalized("An error occurred: {0}",
                                                                    error));
        }

        /// <summary>
        /// Notifies a warning to the user.
        /// </summary>
        /// <param name="warning">The message.</param>
        public override void NotifyWarning(string warning)
        {
            Output.WriteErrorLine("{0}", Configuration.GetLocalized("Warning: {0}", warning));
        }

        /// <summary>
        /// Notifies some information to the user.
        /// </summary>
        /// <param name="info">info.</param>
        public override void NotifyInfo(string info)
        {
            Output.WriteLine("{0}", info);
        }

    }
}
