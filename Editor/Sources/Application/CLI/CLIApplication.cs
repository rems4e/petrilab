//
//  CLIApplication.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-31.
//

using System;
using System.Collections.Generic;
using System.Linq;

namespace Petri.Application.CLI
{
    /// <summary>
    /// The entry point class for the application when used in command-line mode.
    /// </summary>
    public class CLIApplication
    {
        /// <summary>
        /// The usage string.
        /// </summary>
        public static readonly string UsageString = Configuration.GetLocalized("Usage: petrilab [--version] [--player] [--profile <profile-name>] [--generate|-g] [--compile|-c] [--deploy] [--update|-u] [--run|-r [--args '$arg0:value1 $arg1:value2'] [--log-nothing] [--log-states] [--log-transitions] [--log-return-values]] [--clean|-k] [--verbose|-v] [--debug|-d] [--exportPDF path.pdf] [--list-params] [--list-profiles] [--] <path-to-document.petri>");

        internal static readonly string MissingPetriDocument = Configuration.GetLocalized("The path to the document must be specified as the last program argument!");

        internal static readonly string WrongShortOption = Configuration.GetLocalized("Invalid short option {0}!");

        internal static readonly string WrongProfileName = Configuration.GetLocalized("Invalid profile name {0}! The list of declared profiles can be obtained with the {1} option.");

        internal static readonly int ArgumentError = 4;
        internal static readonly int GenerationFailure = 28;
        internal static readonly int CompilationFailure = 32;
        internal static readonly int DeploymentFailure = 36;
        internal static readonly int RunFailure = 40;
        internal static readonly int LoadLibFailure = 72;
        internal static readonly int UnexpectedError = 80;

        /// <summary>
        /// Prints the application's usage when invoked from a terminal, either when requested by the user with a help flag, or when a wrong flag is passed as an argument.
        /// </summary>
        /// <returns>The expected return code for the application</returns>
        /// <param name="output">The output stream.</param>
        /// <param name="returnCode">The return code that the function must return. If ≠ 0, then the output is done on stderr. Otherwise, the output is made on stdout.</param>
        static int PrintUsage(IOutput output, int returnCode)
        {
            if(returnCode == 0) {
                output.WriteLine("{0}", UsageString);
            } else {
                output.WriteErrorLine("{0}", UsageString);
            }
            return returnCode;
        }

        /// <summary>
        /// Prints the detailed usage information for the CLI application, as requested by the --help command line option.
        /// </summary>
        /// <param name="output">The output stream.</param>
        public static void PrintHelp(IOutput output)
        {
            output.WriteLine("{0}", UsageString);
            output.WriteLine();

            output.WriteLine("{0}", Configuration.GetLocalized("Possible use cases:"));
            var usages = new string[] {
                "--version                           ~ "
                    + Configuration.GetLocalized("Prints the application's version with some additional info."),
                "[--player] <path-to-doc.petri> [<path-to-other-doc-1.petri> …] ~ "
                    + Configuration.GetLocalized("Opens the specified documents list in the petri net editor. If the {0} option is specified, then an execute/debug only version of the app is opened.", "--player"),
                "--exportPDF <path-to-new-PDF-doc> <path-to-doc.petri>          ~ "
                    + Configuration.GetLocalized("Creates a PDF representation of the petri net at the specified path."),
                "",
                "--generate|-g <path-to-doc.petri>   ~ "
                    + Configuration.GetLocalized("Generates the petri net's source code."),
                "--compile|-c <path-to-doc.petri>    ~ "
                    + Configuration.GetLocalized("Compiles the petri net's source code. If the source code is outdated, it is generated as if the {0} option was passed as well.",
                                                 "--generate"),
                "--deploy    <path-to-doc.petri>     ~ "
                    + Configuration.GetLocalized("Deploys the dynamic library."),
                "--update|-c <path-to-doc.petri>     ~ "
                    + Configuration.GetLocalized("Recompiles and deploys the dynamic library if it is outdated only. If the source code is outdated, it is generated as if the {0} option was passed as well.",
                                                 "--generate"),
                "--clean|-k <path-to-doc.petri>      ~ "
                    + Configuration.GetLocalized("Cleans the artifacts of a petri net, i.e removes the generated source code files and compiled dynamic libraries."),

                "",
                "--list-params <path-to-doc.petri>   ~ "
                    + Configuration.GetLocalized("Displays the list of parameters the petri net accepts."),
                "--list-profiles <path-to-doc.petri> ~ "
                    + Configuration.GetLocalized("Displays the list of profiles the document declares."),
                "",
                "--run|-r [--args '$arg0:value $arg1:value'] [--log-nothing] [--log-states] [--log-transitions] [--log-return-values] <path-to-doc.petri>      ~ "
                    + Configuration.GetLocalized("Runs the petri net with the provided arguements, if any. The --log-nothing, --log-states, --log-transitions and --log-return-values options override the default verbosity of the petri net. The petri net must be able to run embedded in the editor, i.e. not require hosting from an external application. If the dynamic library is outdated or nonexistent, it is generated as if the {0} option was passed.",
                                                 "--compile"),
                "--debug|-d <path-to-doc.petri>      ~ "
                    + Configuration.GetLocalized("Load the command-line debugger and starts a debugging session in it."),
            };

            foreach(var u in usages) {
                output.WriteLine("  {0}", u);
            }

            output.WriteLine("\n{0}", Configuration.GetLocalized("The {0} option is to be followed by a profile name, which can be obtained from the {1} option. If present, it will be used by the following options: {2}.",
                                                                 "--profile",
                                                                 "--list-profiles",
                                                                 "--generate --compile --update --deploy --run"));

            output.WriteLine();
            output.WriteLine("{0}", Configuration.GetLocalized("Options that can be combined:  {0} (or equivalently, {1}, {2}, or any mix of short and long options format).",
                                                               "--generate --compile --update --run --clean, --deploy",
                                                               "-gcrku",
                                                               "-g -c -r -k, -u"));
            output.WriteLine("{0}", Configuration.GetLocalized("{0} can always be specified with every other option and causes some information output to be generated.",
                                                               "--verbose|-v"));

            output.WriteLine();
            output.WriteLine("{0}", Configuration.GetLocalized("Specifying the {0} option and giving it a 2 letter ISO country name code argument will cause the application to load the appropriate locale. Currently supported locales are:",
                                                               "--lang"));
            output.WriteLine("  en - English");
            output.WriteLine("  fr - Français");
        }

        /// <summary>
        /// Returns whether the argument is a short CLI option.
        /// </summary>
        /// <returns><c>true</c> if opt is a short option; otherwise, <c>false</c>.</returns>
        /// <param name="opt">Option.</param>
        static bool IsShortOption(string opt)
        {
            return System.Text.RegularExpressions.Regex.Match(opt, "^-[a-zA-Z0-9]+$").Success;
        }

        /// <summary>
        /// The entry point of the program when in command line mode. The execution of this method will not throw any exception.
        /// </summary>
        /// <returns>The return code of the program.</returns>
        /// <param name="args">Arguments.</param>
        public static int Main(string[] args)
        {
            Application.ExceptionUnhandled += OnUnhandledException;

            IOutput output = new ConsoleOutput(false);

            try {
                return RawMain(output, args);
            } catch(UsageException e) {
                output.WriteErrorLine("{0}", e.Message);
                output.WriteErrorLine("{0}", UsageString);
                return e.ReturnCode;
            } catch(GenerationException) {
                output.WriteErrorLine("{0}", Configuration.GetLocalized("Generation failed, aborting!"));
                return GenerationFailure;
            } catch(CompilationFailureException) {
                output.WriteErrorLine("{0}", Configuration.GetLocalized("Compilation failed, aborting!"));
                return CompilationFailure;
            } catch(DeploymentFailureException) {
                output.WriteErrorLine("{0}", Configuration.GetLocalized("Deployment failed, aborting!"));
                return DeploymentFailure;
            } catch(ExecutionFailureException) {
                output.WriteErrorLine("{0}", Configuration.GetLocalized("The petri net could not be run because of an error."));
                return RunFailure;
            } catch(LoadLibFailureException) {
                output.WriteErrorLine("{0}", Configuration.GetLocalized("The dynamic library could not be loaded."));
                return LoadLibFailure;
            } catch(DocumentLoadException e) {
                output.WriteErrorLine("{0}", e.Message);
                return e.ReturnCode;
            } catch(Exception e) {
                output.WriteErrorLine("{0}", Configuration.GetLocalized("An exception occurred: {0} {1}.", e, e.Message));
                return UnexpectedError;
            }
        }

        /// <summary>
        /// The entry point of the program when in command line mode. The execution of this method may throw an exception.
        /// </summary>
        /// <returns>The return code of the program.</returns>
        /// <param name="output">The output stream.</param>
        /// <param name="args">Arguments.</param>
        public static int RawMain(IOutput output, string[] args)
        {
            if(Configuration.IsOnTrial) {
                output.WriteLine("{0}\n", Configuration.GetLocalized("{0} days until trial period expiration.", Configuration.RemainingTrialDuration));
            }

            bool generate = false;
            bool compile = false;
            bool run = false;
            bool clean = false;
            bool verbose = false;
            bool update = false;
            bool deploy = false;
            Runtime.PetriNet.LogVerbosity? verbosity = null;

            if(args[0] == "--help" || args[0] == "-h") {
                PrintHelp(output);
                return 0;
            } else if(args[0] == "--version") {
                output.WriteLine("PetriLab");
                output.WriteLine("{0}", Configuration.GetLocalized("Version {0}", Application.Version));

                output.WriteLine("Copyright © 2014-2018 Sigilence Technologies");

                output.WriteLine(Application.WebSiteInfo);

                return 0;
            }

            if(!Configuration.CheckTrialPeriod()) {
                output.WriteLine(Configuration.GetLocalized("The trial period is expired. Please contact the support."));
                return UnexpectedError;
            }

            if(args[0] == "--player" || args[0] == "--") {
                if(args.Length < 1) {
                    throw new UsageException(0, MissingPetriDocument);
                }

                bool player = args[0] == "--player";
                bool endOfArgs = args[0] == "--";
                var docs = args.Skip(1);

                if(!endOfArgs) {
                    if(docs.FirstOrDefault() == "--player") {
                        player = true;
                        docs = docs.Skip(1);
                    }
                    if(docs.FirstOrDefault() == "--") {
                        docs = docs.Skip(1);
                    }
                }

                // Registering the full path of the document, so that we cannot open twice the same document later.
                docs = docs.Select((path) => PathUtility.GetFullPath(path));
                return GUI.GUIApplication.Main(docs, player);
            } else if(args[0] == "--debug" || args[0] == "-d") {
                return DebuggableHeadlessDocument.DebuggerLoop(args.Length == 2 ? args[1] : "");
            } else if(args[0] == "--debug-host") {
                if(args.Length != 2) {
                    throw new UsageException(2, MissingPetriDocument);
                }

                // This is to prevent this process, as a child process of the petri net debugger, to be terminated when the parent process receives ^C.
                using(var signal = new Mono.Unix.UnixSignal(Mono.Unix.Native.Signum.SIGINT)) {
                    HeadlessDocument toDebug;
                    try {
                        toDebug = new HeadlessDocument(args[1]);
                    } catch(Exception e) {
                        throw new DocumentLoadException(RunFailure, e);
                    }

                    Petri.Application.Debugger.DebugClient.DebugSession(toDebug);
                }
                return 0;
            } else if(args[0] == "--run-host") {
                if(args.Length != 5) {
                    throw new UsageException(3, MissingPetriDocument);
                }

                var verbosityInt = int.Parse(args[2].Substring(5));
                Runtime.PetriNet.LogVerbosity? runVerbosity = verbosityInt == -1 ? null : (Runtime.PetriNet.LogVerbosity?)verbosityInt;
                verbose = args[3] == "-v1";

                HeadlessDocument toRun;
                try {
                    toRun = new HeadlessDocument(args[1]);
                } catch(Exception e) {
                    throw new DocumentLoadException(RunFailure, e);
                }

                toRun.Settings.CurrentProfile.DebugMode = DebugMode.RunInEditor;

                var param = ParsePetriNetArguments(args[4], toRun.PetriNet);
                var returnValues = RunDocument(output, toRun, param, runVerbosity, verbose, true);
                return 0;
            } else if(args[0] == "--exportPDF") {
                if(args.Length != 3) {
                    throw new UsageException(4, Configuration.GetLocalized("Missing document and/or export path"));
                }

                var doc = new HeadlessDocument(args[2]);

                if(Gdk.Screen.Default == null) {
                    output.WriteLine("{0}", Configuration.GetLocalized("Comments' text cannot be drawn in CLI mode, placeholders will be inserted."));
                }

                var renderView = new RenderView(doc, true);
                renderView.Render(args[1]);

                return 0;
            } else if(args[0] == "--upgrade-doc") {
                if(args.Length < 2) {
                    throw new UsageException(5, Configuration.GetLocalized("Missing document"));
                }

                for(int i = 1; i < args.Length; ++i) {
                    var doc = new HeadlessDocument(args[i]);
                    output.WriteLine("{0}", Configuration.GetLocalized("Processing petri net \"{0}\"…", doc.Settings.Name));
                    doc.Save();
                }

                return 0;
            } else if(args[0] == "--list-params") {
                if(args.Length != 2) {
                    throw new UsageException(6, Configuration.GetLocalized("Missing document"));
                }

                var doc = new HeadlessDocument(args[1]);
                if(doc.PetriNet.Parameters.Count > 0) {
                    output.WriteLine("{0}", Configuration.GetLocalized("This petri net takes {0} parameters:", doc.PetriNet.Parameters.Count));
                    output.WriteLine(
                        "\n{0}{1}",
                        Configuration.GetLocalized("cli param Name:").PadRight(40),
                        Configuration.GetLocalized("cli param Default value:")
                    );
                    foreach(var param in doc.PetriNet.Parameters) {
                        output.WriteLine("{0}{1}", param.MakeUserReadable().PadRight(40), doc.PetriNet.Variables[param]);
                    }
                } else {
                    output.WriteLine("{0}", Configuration.GetLocalized("This petri net does not take any parameter."));
                }

                return 0;
            } else if(args[0] == "--list-profiles") {
                if(args.Length != 2) {
                    throw new UsageException(7, Configuration.GetLocalized("Missing document"));
                }

                var doc = new HeadlessDocument(args[1]);
                output.WriteLine("{0}", Configuration.GetLocalized("This document declares {0} profiles:", doc.Settings.Profiles.Count));
                foreach(var profile in doc.Settings.Profiles) {
                    output.WriteLine("  {0}", profile.Name);
                }

                return 0;
            }

            string argsString = null;
            string profileString = null;

            int lastArg = 0;
            for(; lastArg < args.Length; ++lastArg) {
                bool used = false;
                // A getopt-like options/file separator that allows the hypothetical processing of petri net files named "--help" or "--compile" and so on.
                if(args[lastArg] == "--") {
                    ++lastArg;
                } else if(IsShortOption(args[lastArg])) {
                    int count = 0;
                    if(args[lastArg].Contains("v")) {
                        verbose = true;
                        ++count;
                    }
                    if(args[lastArg].Contains("g")) {
                        generate = true;
                        ++count;
                    }
                    if(args[lastArg].Contains("c")) {
                        compile = true;
                        ++count;
                    }
                    if(args[lastArg].Contains("r")) {
                        run = true;
                        ++count;
                    }
                    if(args[lastArg].Contains("k")) {
                        clean = true;
                        ++count;
                    }
                    if(args[lastArg].Contains("u")) {
                        update = true;
                        ++count;
                    }

                    // The argument is used if all of the short options have been consumed.
                    if(count != (args[lastArg].Length - 1)) {
                        throw new UsageException(8, WrongShortOption, args[lastArg]);
                    }
                    used = true;
                } else if(args[lastArg] == "--verbose") {
                    verbose = true;
                    used = true;
                } else if(args[lastArg] == "--log-nothing") {
                    verbosity = Runtime.PetriNet.LogVerbosity.Nothing;
                    used = true;
                } else if(args[lastArg] == "--log-states") {
                    if(!verbosity.HasValue) {
                        verbosity = Runtime.PetriNet.LogVerbosity.Nothing;
                    }
                    verbosity |= Runtime.PetriNet.LogVerbosity.States;
                    used = true;
                } else if(args[lastArg] == "--log-transitions") {
                    if(!verbosity.HasValue) {
                        verbosity = Runtime.PetriNet.LogVerbosity.Nothing;
                    }
                    verbosity |= Runtime.PetriNet.LogVerbosity.Transitions;
                    used = true;
                } else if(args[lastArg] == "--log-return-values") {
                    if(!verbosity.HasValue) {
                        verbosity = Runtime.PetriNet.LogVerbosity.Nothing;
                    }
                    verbosity |= Runtime.PetriNet.LogVerbosity.ReturnValues;
                    used = true;
                } else if(args[lastArg] == "--generate") {
                    generate = true;
                    used = true;
                } else if(args[lastArg] == "--compile") {
                    compile = true;
                    used = true;
                } else if(args[lastArg] == "--run") {
                    run = true;
                    used = true;
                } else if(args[lastArg] == "--args" && lastArg < args.Length - 1) {
                    ++lastArg;
                    argsString = args[lastArg];
                    used = true;
                } else if(args[lastArg] == "--clean") {
                    clean = true;
                    used = true;
                } else if(args[lastArg] == "--update") {
                    update = true;
                    used = true;
                } else if(args[lastArg] == "--deploy") {
                    deploy = true;
                    used = true;
                } else if(args[lastArg] == "--profile" && lastArg < args.Length - 1) {
                    used = true;
                    ++lastArg;
                    profileString = args[lastArg];
                }

                if(!used) {
                    break;
                }
            }

            if(!compile && !generate && !run && !clean && !update && !deploy) {
                // Registering the full path of the document, so that we cannot open twice the same document later.
                var docs = args.Select((path) => PathUtility.GetFullPath(path));
                return GUI.GUIApplication.Main(docs, false);
            }

            if(lastArg == args.Length) {
                // Did not specify document path
                throw new UsageException(9, MissingPetriDocument);
            }

            for(; lastArg < args.Length; ++lastArg) {
                string documentPath = args[lastArg];

                HeadlessDocument document;
                try {
                    document = new HeadlessDocument(documentPath);
                } catch(Exception e) {
                    throw new DocumentLoadException(UnexpectedError, e);
                }

                if(profileString != null) {
                    var profile = document.Settings.Profiles.Find(prof => prof.Name == profileString);
                    if(profile == null) {
                        throw new UsageException(10, WrongProfileName, profileString, "--list-profiles");
                    }

                    document.Settings.CurrentProfile = profile;
                }

                if(verbose) {
                    output.WriteLine("{0}", Configuration.GetLocalized("Processing document \"{0}\" with profile \"{1}\"…", document.Settings.Name, document.Settings.CurrentProfile.Name));
                }

                if(run) {
                    document.Settings.CurrentProfile.DebugMode = DebugMode.RunInEditor;
                }

                if(clean) {
                    if(verbose) {
                        output.Write("{0}", Configuration.GetLocalized("Cleaning artifacts of petri net \"{0}\"…",
                                                                       document.Settings.Name));
                    }
                    System.IO.File.Delete(document.Settings.StandardSourcePath);
                    System.IO.File.Delete(document.Settings.StandardIntermediatePath);

                    var outputType = document.Settings.OutputType;

                    document.Settings.OutputType = OutputType.DynamicLib;
                    System.IO.File.Delete(document.Settings.StandardLibPath);
                    document.Settings.OutputType = OutputType.StaticLib;
                    System.IO.File.Delete(document.Settings.StandardLibPath);

                    document.Settings.OutputType = outputType;

                    if(document.Settings.Language != Code.Language.EmbeddedC) {
                        string headerPath = PathUtility.GetFullPath(
                            System.IO.Path.Combine(
                                System.IO.Directory.GetParent(document.Path).FullName,
                                document.Settings.RelativeSourceHeaderPath
                            )
                        );
                        System.IO.File.Delete(headerPath);
                    }
                    if(verbose) {
                        output.WriteLine(" {0}", Configuration.GetLocalized("Done."));
                    }
                }

                bool forceGeneration = false, forceCompilation = false;
                if(!generate && (compile || run || update || deploy)) {
                    var sourcePath = document.Settings.StandardSourcePath;
                    if(!System.IO.File.Exists(sourcePath)
                       || System.IO.File.GetLastWriteTime(sourcePath) < System.IO.File.GetLastWriteTime(document.Path)) {
                        generate = true;
                        forceGeneration = true;
                    } else if(verbose) {
                        output.WriteLine("{0}", Configuration.GetLocalized("The previously generated {0} code is up to date, no need for code generation.",
                                                                           document.Settings.LanguageName()));
                    }
                }

                if(generate) {
                    if(forceGeneration && verbose) {
                        output.WriteLine("{0}", Configuration.GetLocalized("The previously generated {0} code is outdated or nonexistent, generating new code…",
                                                                           document.Settings.LanguageName()));
                    }
                    document.GenerateCode();
                    if(verbose) {
                        output.WriteLine("{0}", Configuration.GetLocalized("Successfully generated the {0} code.",
                                                                           document.Settings.LanguageName()));
                    }
                }

                // Runs the embedded C document, and discard other work like compilation, as they will not work anyway.
                if(document.Settings.Language == Code.Language.EmbeddedC) {
                    if(run) {
                        RunEmbeddedCDocument(output, document, verbose);
                        return 0;
                    } else if(compile || update || deploy) {
                        throw new UsageException(11, Configuration.GetLocalized("An EmbeddedC petri net cannot be compiled."));
                    }
                }

                if(!compile && (run || update || deploy)) {
                    var libPath = document.Settings.StandardLibPath;
                    if(!System.IO.File.Exists(libPath) || System.IO.File.GetLastWriteTime(libPath) < System.IO.File.GetLastWriteTime(document.Settings.StandardSourcePath)) {
                        compile = true;
                        forceCompilation = true;
                    } else if(verbose) {
                        output.WriteLine("{0}", Configuration.GetLocalized("The previously compiled library is up to date, no need for recompilation."));
                    }
                }

                if(compile) {
                    if(forceCompilation && verbose) {
                        output.WriteLine("{0}", Configuration.GetLocalized("The previously compiled library is outdated or nonexistent, compiling…"));
                    }
                    var res = document.Compile(verbose).Result.Item1;
                    if(res == Compiler.CompilationStatus.Error) {
                        throw new CompilationFailureException();
                    } else if(verbose) {
                        output.WriteLine("{0}", Configuration.GetLocalized("Compilation successful!"));
                    }
                }

                if(!deploy && (run || update)) {
                    deploy = true;
                }

                if(deploy) {
                    if(verbose && document.Settings.CurrentProfile.DeployMode != DeployMode.DoNothing) {
                        output.WriteLine("{0}", Configuration.GetLocalized("Deploying…"));
                    }

                    var result = document.Deploy(verbose).Result;
                    if(!result) {
                        throw new DeploymentFailureException();
                    } else if(verbose && document.Settings.CurrentProfile.DeployMode != DeployMode.DoNothing) {
                        output.WriteLine("{0}", Configuration.GetLocalized("Deployment successful!"));
                    }
                }

                if(run) {
                    Dictionary<UInt32, Int64> param = null;
                    try {
                        param = ParsePetriNetArguments(argsString, document.PetriNet);
                    } catch(KeyNotFoundException e) {
                        output.WriteLine(Configuration.GetLocalized("Invalid petri net argument's name '{0}'!", e.Message));
                    } catch(ArgumentException e) {
                        output.WriteLine(Configuration.GetLocalized("Invalid petri net argument's value '{0}'!", e.ParamName));
                    }

                    if(param == null) {
                        throw new ExecutionFailureException();
                    }

                    StartRunHostAndWait(output, document, param, verbosity, verbose);
                }
            }

            return 0;
        }

        /// <summary>
        /// Parses a string in the form "$var1:4 $var2:-7" and converts it to key-value pairs of petri net arguments.
        /// If an argument is a number instead of a variable's name, it is kept as is.
        /// </summary>
        /// <returns>The petri net arguments' key value pairs.</returns>
        /// <param name="argsString">The arguments.</param>
        /// <param name="petriNet">Petri net.</param>
        public static Dictionary<UInt32, Int64> ParsePetriNetArguments(string argsString, Model.RootPetriNet petriNet)
        {
            if(argsString != null) {
                return ParsePetriNetArguments(argsString.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries), petriNet);
            }

            return new Dictionary<UInt32, Int64>();
        }

        /// <summary>
        /// Parses a list of strings in the form "$var1:4" and converts it to key-value pairs of petri net arguments.
        /// If an argument is a number instead of a variable's name, it is kept as is.
        /// </summary>
        /// <returns>The petri net arguments' key value pairs.</returns>
        /// <param name="args">The arguments list.</param>
        /// <param name="petriNet">Petri net.</param>
        public static Dictionary<UInt32, Int64> ParsePetriNetArguments(IEnumerable<string> args, Model.RootPetriNet petriNet)
        {
            var result = new Dictionary<UInt32, Int64>();
            foreach(var pp in args) {
                var p = pp.Split(':');
                if(p.Length != 2) {
                    throw new KeyNotFoundException(pp);
                }

                string name = p[0].Trim();
                Int64 index = 0;
                if(name.StartsWithInv("$")) {
                    foreach(var k in petriNet.Parameters) {
                        if(k.MakeUserReadable() == name) {
                            break;
                        }
                        ++index;
                    }
                    if(index == petriNet.Parameters.Count) {
                        throw new KeyNotFoundException(name);
                    }
                } else {
                    try {
                        index = UInt32.Parse(name);
                    } catch {
                        throw new KeyNotFoundException(name);
                    }
                }

                Int64 value;
                try {
                    value = Int64.Parse(p[1].Trim());
                } catch {
                    throw new ArgumentException("", p[1].Trim());
                }
                result[(UInt32)index] = value;
            }

            return result;
        }

        /// <summary>
        /// Runs the petri net described by the document. It must have been already compiled.
        /// The function will invoke our process again (sort of a fork() call).
        /// This process will host the petri net's runtime.
        /// </summary>
        /// <param name="output">The output stream.</param>
        /// <param name="doc">The document to run.</param>
        /// <param name="param">The dictionary of custom parameters indexes and values.</param>
        /// <param name="verbosity">The petri net execution verbosity.</param>
        /// <param name="verbose">Whether to output info messages.</param>
        static void StartRunHostAndWait(IOutput output, LocalDocument doc, Dictionary<UInt32, Int64> param, Runtime.PetriNet.LogVerbosity? verbosity, bool verbose)
        {
            var strargs = "";
            foreach(var kvp in param) {
                strargs += kvp.Key + ":" + kvp.Value + " ";
            }

            var copyProg = "petrilab";
            var copyArgs = " --run-host \"" + PathUtility.GetFullPath(doc.Path) + "\" " + string.Format("--log{0}", (verbosity.HasValue ? (int)verbosity.Value : -1)) + (verbose ? " -v1" : " v0") + " '" + strargs + "'";

            // Launch a copy of this program that will host the petri net's runtime.
            if(!Environment.GetEnvironmentVariables().Contains("PETRILAB_AM_I_IN_BUNDLE")) {
                copyProg = "mono";
                copyArgs = System.Reflection.Assembly.GetExecutingAssembly().Location + copyArgs;
            }
            var process = Application.CreateProcess(
                copyProg,
                copyArgs
            );

            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.RedirectStandardError = true;
            process.StartInfo.WorkingDirectory = System.IO.Directory.GetParent(doc.Path).FullName;

            process.OutputDataReceived += (sender, ev) => {
                if(ev.Data != null) {
                    output.WriteLine("{0}", ev.Data);
                }
            };
            process.ErrorDataReceived += (sender, ev) => {
                if(ev.Data != null) {
                    output.WriteErrorLine("{0}", ev.Data);
                }
            };

            process.Start();
            process.BeginOutputReadLine();
            process.BeginErrorReadLine();

            process.WaitForExit();

            if(process.ExitCode != 0) {
                throw new ExecutionFailureException();
            }
        }

        /// <summary>
        /// Runs the petri net described by the document. It must have been already compiled.
        /// </summary>
        /// <param name="output">The output stream.</param>
        /// <param name="doc">The document to run.</param>
        /// <param name="param">The dictionary of custom parameters indexes and values.</param>
        /// <param name="verbosity">The level of additional info to be ouput upon execution.</param>
        /// <param name="verbose">Whether to ouptut info messages</param>
        /// <param name="allowRetry">Whether a failure to load the dynamic lib triggers a recompilation attempt.</param>
        public static Dictionary<string, Int64> RunDocument(IOutput output, LocalDocument doc, Dictionary<UInt32, Int64> param, Runtime.PetriNet.LogVerbosity? verbosity, bool verbose, bool allowRetry)
        {
            // This is called in the invoked process and will load and run the petri net.
            if(verbose) {
                output.WriteLine("{0}\n", Configuration.GetLocalized("Preparing for the petri net's execution…"));
                output.Write("{0}", Configuration.GetLocalized("Loading the library… "));
            }
            var proxy = Petri.Application.Debugger.GeneratedDynamicLibProxy.LibProxyForLanguage(
                doc.Settings.Language,
                System.IO.Directory.GetParent(doc.Path).FullName,
                doc.Settings.RelativeLibBaseName + doc.Settings.LibExtension,
                doc.Settings.Name
            );

            var dylib = proxy.Load<Runtime.IGeneratedDynamicLib>();

            if(dylib == null) {
                if(allowRetry) {
                    output.Write("\n{0}", Configuration.GetLocalized("Could not load the dynamic lib. Attempting recompilation… "));

                    var res = doc.Compile(verbose).Result.Item1;
                    if(res == Compiler.CompilationStatus.Error) {
                        throw new CompilationFailureException();
                    }
                    output.WriteLine("{0}", Configuration.GetLocalized("Done."));
                    output.WriteLine("{0}", Configuration.GetLocalized("Attempting execution again…"));
                    return RunDocument(output, doc, param, verbosity, verbose, false);
                }

                throw new ExecutionFailureException();
            }
            if(verbose) {
                output.WriteLine("{0}", Configuration.GetLocalized("Library loaded."));
                output.Write("{0}", Configuration.GetLocalized("Extracting the dynamic library… "));
            }

            var dynamicLib = dylib.Lib;

            if(verbose) {
                output.WriteLine("{0}", Configuration.GetLocalized("OK."));
                output.Write("{0}", Configuration.GetLocalized("Loading the petri net… "));
            }

            Runtime.PetriNet pn = dynamicLib.CreatePetriNet();
            if(verbosity.HasValue) {
                pn.SetLogVerbosity(verbosity.Value);
            }
            if(verbose) {
                output.WriteLine("{0}", Configuration.GetLocalized("OK."));
                output.WriteLine("{0}\n", Configuration.GetLocalized("Ready to go! The application will automatically close when/if the petri net execution completes."));
            }

            foreach(var arg in param) {
                pn.Variables[arg.Key].Value = arg.Value;
            }

            pn.Run();
            pn.Join();

            var variables = new Dictionary<string, Int64>();
            if(pn.Variables.IsReturnValues && pn.Variables.Size > 0) {
                foreach(var v in pn.Variables) {
                    variables[v.Name] = v.Value;
                }
            }

            if(verbose) {
                output.Write("\n{0}", Configuration.GetLocalized("Execution complete. Unloading the library… "));
            }
            proxy.Unload();
            if(verbose) {
                output.WriteLine("{0}", Configuration.GetLocalized("Done, will now exit."));
            }

            return variables;
        }

        /// <summary>
        /// Runs the embedded C document.
        /// </summary>
        /// <returns>The embedded C document.</returns>
        /// <param name="output">The output stream.</param>
        /// <param name="document">Document.</param>
        /// <param name="verbose">Whether some additional info is to be ouput upon execution.</param>
        static void RunEmbeddedCDocument(IOutput output, Document document, bool verbose)
        {
            var source = document.Settings.StandardSourcePath;
            var sourceList = string.Join(" ", document.Sources.Select(s => "\"" + s.Item1 + "\""));

            var prog = System.IO.Path.GetTempFileName();
            var includeList = string.Join(
                " ",
                document.Settings.CurrentProfile.CompilerInfo.IncludePaths.Select(i => "-I\"" + i.Item1 + "\"")
            );

            var arguments = includeList + " -DGENERATED_CODE_PATH=\"\\\"" + source + "\\\"\" -DPETRI_NET_TO_RUN=\"" + document.Settings.Name + "\" " + sourceList + " -o \"" + prog + "\"";

            if(verbose) {
                output.WriteLine("{0}", Configuration.GetLocalized("Starting compilation."));
            }
            // Compile
            {
                var process = Application.CreateProcess("cc", arguments);

                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.RedirectStandardError = true;
                process.StartInfo.WorkingDirectory = System.IO.Directory.GetParent(document.Path).FullName;

                var outBuilder = new System.Text.StringBuilder();
                var errBuilder = new System.Text.StringBuilder();

                process.OutputDataReceived += (sender, ev) => {
                    if(ev.Data != null) {
                        outBuilder.AppendLine(ev.Data);
                    }
                };
                process.ErrorDataReceived += (sender, ev) => {
                    if(ev.Data != null) {
                        errBuilder.AppendLine(ev.Data);
                    }
                };

                process.Start();
                process.BeginOutputReadLine();
                process.BeginErrorReadLine();

                process.WaitForExit();

                if(process.ExitCode != 0) {
                    output.WriteLine("{0}", outBuilder);
                    output.WriteLine("{0}", errBuilder);

                    output.WriteErrorLine("{0}", Configuration.GetLocalized("The compilation failed with status code {0}!",
                                                                            process.ExitCode));
                    output.WriteErrorLine("{0}", Configuration.GetLocalized("Compiler invocation: cc {0}.", arguments));
                    throw new CompilationFailureException();
                }
            }

            if(verbose) {
                output.WriteLine("{0}", Configuration.GetLocalized("Compilation successful."));
                output.WriteLine("{0}", Configuration.GetLocalized("Starting execution."));
            }

            // Run
            {
                var process = Application.CreateProcess(prog);

                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.RedirectStandardError = true;
                process.StartInfo.WorkingDirectory = System.IO.Directory.GetParent(document.Path).FullName;

                process.OutputDataReceived += (sender, ev) => {
                    if(ev.Data != null) {
                        output.WriteLine("{0}", ev.Data);
                    }
                };
                process.ErrorDataReceived += (sender, ev) => {
                    if(ev.Data != null) {
                        output.WriteLine("{0}", ev.Data);
                    }
                };

                process.Start();
                process.BeginOutputReadLine();
                process.BeginErrorReadLine();

                process.WaitForExit();

                System.IO.File.Delete(prog);

                if(process.ExitCode != 0) {
                    output.WriteErrorLine("{0}", Configuration.GetLocalized("The petri net execution failed with status code {0}!",
                                                                            process.ExitCode));

                    throw new ExecutionFailureException();
                }

                if(verbose) {
                    output.WriteLine("{0}", Configuration.GetLocalized("Execution successful."));
                }
            }
        }

        /// <summary>
        /// The application's unhandled exceptions handler.
        /// Presents an the exception's message as an error to the user.
        /// </summary>
        /// <param name="args">The exception wrapper.</param>
        public static void OnUnhandledException(UnhandledExceptionEventArgs args)
        {
            lock(_outLock) {
                var message = Application.GetExceptionMessage(args.ExceptionObject);
                if(message != null) {
                    Console.Error.WriteLine(Configuration.GetLocalized("Uncaught exception: {0}", message));
                } else {
                    Console.Error.WriteLine(Configuration.GetLocalized("Uncaught exception, but unknown exception object."));
                }
                Console.Error.WriteLine("{0}", args.ExceptionObject);
            }
        }

        static object _outLock = new object();
    }
}
