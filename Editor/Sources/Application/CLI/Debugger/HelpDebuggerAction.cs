//
//  HelpDebuggerAction.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-04-16.
//

using System;
using System.Collections.Generic;

namespace Petri.Application.CLI.Debugger
{
    /// <summary>
    /// The help command of the debugger.
    /// </summary>
    public class HelpDebuggerAction : BasicDebuggerAction
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.CLI.Debugger.HelpDebuggerAction"/> class.
        /// </summary>
        /// <param name="output">Output.</param>
        public HelpDebuggerAction(IOutput output) : base(false, output, (List<string> s) => {
            HelpDebuggerAction.Help(s);
            return true;
        },
                                                         Configuration.GetLocalized("Show help."),
                                                         Configuration.GetLocalized("Shows a list of all debugger commands, or give details about specific commands or subcommand."),
                                                         Configuration.GetLocalized("[<command name>] [<subcommand name] [--usage-only]"),
                                                         "help",
                                                         new string[] { "--usage-only" })
        {
        }

        /// <summary>
        /// Displays help for the specified action/subaction.
        /// </summary>
        /// <param name="args">Arguments.</param>
        static void Help(List<string> args)
        {
            Root.PrintHelp(args);
        }

        /// <summary>
        /// Executes the specified action with the specified args.
        /// This is this method's responsibility to block if the underlying action is asynchronous but the side effects
        /// should be synchronous.
        /// </summary>
        /// <param name="args">Arguments.</param>
        public override bool Execute(List<string> args)
        {
            return Action(args);
        }

        /// <summary>
        /// Returns a list of autocompletion values for the current user input. The first item of the tuple is the base
        /// string on which the autocompletion was based.
        /// </summary>
        /// <param name="args">The string on which the autocompletion is based.</param>
        public override Tuple<string, List<string>> Autocomplete(string args)
        {
            return Root.Autocomplete(args);
        }

        /// <summary>
        /// Gets or sets the root debugger action.
        /// </summary>
        /// <value>The root.</value>
        public static RootDebuggerAction Root {
            get;
            set;
        }
    }
}

