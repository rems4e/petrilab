//
//  DebuggerActionGroup.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-03-13.
//

using System;
using System.Collections.Generic;
using System.Linq;

namespace Petri.Application.CLI.Debugger
{
    /// <summary>
    /// A debugger action that gives access to one or several more debugger actions.
    /// </summary>
    public class DebuggerActionGroup : DebuggerAction
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.CLI.Debugger.DebuggerActionGroup"/> class.
        /// </summary>
        /// <param name="output">The IOutput the action can write to.</param>
        /// <param name="actions">The list of sub-actions.</param>
        /// <param name="description">Maps to <see cref="Petri.Application.CLI.Debugger.DebuggerAction.Description"/>.</param>
        /// <param name="invocation">Maps to <see cref="Petri.Application.CLI.Debugger.DebuggerAction.Invocation"/>.</param>
        public DebuggerActionGroup(IOutput output, IEnumerable<DebuggerAction> actions,
                                   string description,
                                   string invocation) : base(output)
        {
            Description = description;
            Invocation = invocation;

            _sortedActions = new List<DebuggerAction>(actions);
            _sortedActions.Sort((a1, a2) => {
                return string.Compare(a1.Invocation, a2.Invocation, StringComparison.InvariantCulture);
            });

            foreach(var a in _sortedActions) {
                _maxHelpWidth = Math.Max(_maxHelpWidth, a.Invocation.Length);
                _actionsMapping[a.Invocation] = a;
                a.Parent = this;
            }
        }

        /// <summary>
        /// Executes the specified action with the specified args.
        /// This is this method's responsibility to block if the underlying action is asynchronous but the side effects
        /// should be synchronous.
        /// </summary>
        /// <param name="args">Arguments.</param>
        public override bool Execute(List<string> args)
        {
            if(args.Count == 0) {
                PrintHelp(args);
                return true;
            } else {
                return RecursiveInvoke((DebuggerAction action, List<string> a) => {
                    if(action.NeedsDocumentFirst) {
                        Output.WriteErrorLine("{0}", Configuration.GetLocalized("A document needs to be loaded to execute this action!"));
                        return true;
                    } else {
                        if(!action.Execute(a)) {
                            Output.WriteErrorLine("{0}", Configuration.GetLocalized("Invalid invocation."));
                            a.Add("--usage-only");
                            action.PrintHelp(a);
                        }
                        return true;
                    }
                }, args);
            }
        }

        /// <summary>
        /// Prints an help text for the action, including usage information, aliases if necessary etc.
        /// <param name="args">Arguments.</param>
        /// </summary>
        public override void PrintHelp(List<string> args)
        {
            if(args.Count == 0) {
                Output.WriteLine("{0}", HelpHeader);
                foreach(var a in _sortedActions) {
                    Output.WriteLine("  {0} -- {1}",
                                     a.Invocation.PadRight(_maxHelpWidth),
                                     a.Description);
                }

                Output.WriteLine("{0}", HelpFooter);
            } else {
                RecursiveInvoke((DebuggerAction action, List<string> a) => {
                    action.PrintHelp(a);
                    return true;
                }, args);
            }
        }

        /// <summary>
        /// Gets the help header.
        /// </summary>
        /// <value>The help header.</value>
        protected virtual string HelpHeader {
            get {
                return Configuration.GetLocalized("The following subcommands are supported:");
            }
        }

        /// <summary>
        /// Gets the help footer.
        /// </summary>
        /// <value>The help footer.</value>
        protected virtual string HelpFooter {
            get {
                return "\n" + Configuration.GetLocalized("For more help on any particular subcommand, type 'help <command> <subcommand>'.");
            }
        }

        /// <summary>
        /// A delegate that is meant to be bound to PrintHelp and Execute.
        /// </summary>
        delegate bool RecursiveInvocation(DebuggerAction action, List<string> args);

        /// <summary>
        /// Executes the given delegate with the provided arguments.
        /// </summary>
        /// <param name="invocation">Invocation.</param>
        /// <param name="args">Arguments.</param>
        bool RecursiveInvoke(RecursiveInvocation invocation, List<string> args)
        {
            args = Application.SpaceSplit(args[0], 1);
            List<string> nextArgs = args.Count == 2 && args[1].Length > 0 ? new List<string> { args[1] } : new List<string>();

            DebuggerAction action;
            _actionsMapping.TryGetValue(args[0], out action);
            if(action == null) {
                if(Parent == null) {
                    Output.WriteLine("{0}", Configuration.GetLocalized("Invalid command '{0}'.",
                                                                     args[0]));
                } else {
                    Output.WriteLine("{0}", Configuration.GetLocalized("Invalid subcommand '{0}' for command '{1}'.",
                                                                     args[0],
                                                                     FullInvocation));
                }

                return true;
            } else {
                return invocation(action, nextArgs);
            }
        }

        /// <summary>
        /// Gets the action mapped to the specified invocation.
        /// </summary>
        /// <returns>The action for specified invocation.</returns>
        /// <param name="invocation">The invocation.</param>
        public DebuggerAction ActionForInvocation(string invocation)
        {
            return _actionsMapping[invocation];
        }

        /// <summary>
        /// Returns a list of autocompletion values for the current user input. The first item of the tuple is the base string on which the autocompletion was based.
        /// </summary>
        /// <param name="args">The string on which the autocompletion is based.</param>
        public override Tuple<string, List<string>> Autocomplete(string args)
        {
            var cmdAndArgs = Application.SpaceSplit(args, 1);
            if(cmdAndArgs.Count < 2) {
                return base.Autocomplete(cmdAndArgs.Count == 1 ? cmdAndArgs[0] : "");
            } else if(_actionsMapping.ContainsKey(cmdAndArgs[0])) {
                return _actionsMapping[cmdAndArgs[0]].Autocomplete(cmdAndArgs.Count == 2 ? cmdAndArgs[1] : "");
            }

            return Tuple.Create(args, new List<string>());
        }

        /// <summary>
        /// Gets the values available for completion.
        /// </summary>
        /// <returns>The completion values.</returns>
        /// <param name="prefix">Prefix.</param>
        public override IEnumerable<string> GetCompletionValues(string prefix)
        {
            return from action in _sortedActions
                   select action.Invocation;
        }

        /// <summary>
        /// Gets the index of the first parameter that is in raw form, or <c>int.MaxValue</c> if none exist.
        /// </summary>
        /// <value>The first raw input.</value>
        public override int FirstRawInput {
            get {
                return int.MaxValue;
            }
        }

        List<DebuggerAction> _sortedActions;
        /// <summary>
        /// The mapping between invocations and their actions.
        /// </summary>
        protected Dictionary<string, DebuggerAction> _actionsMapping = new Dictionary<string, DebuggerAction>();
        int _maxHelpWidth = 0;
    }
}

