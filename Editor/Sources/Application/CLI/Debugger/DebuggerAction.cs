//
//  DebuggerAction.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2014-12-09.
//

using System;
using System.Collections.Generic;
using System.Linq;

namespace Petri.Application.CLI.Debugger
{
    /// <summary>
    /// A debugger action, that can be executed and which usage can be queried.
    /// </summary>
    public abstract class DebuggerAction
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.CLI.Debugger.DebuggerAction"/> class.
        /// </summary>
        /// <param name="output">The IOutput the action can write to.</param>
        protected DebuggerAction(IOutput output)
        {
            Output = output;
            NeedsDocumentFirst = false;
        }

        /// <summary>
        /// Executes the specified action with the specified args.
        /// This is this method's responsibility to block if the underlying action is asynchronous but the side effects should be synchronous.
        /// </summary>
        /// <param name="args">Arguments.</param>
        public abstract bool Execute(List<string> args);

        /// <summary>
        /// Gets the description of the action, as a short string explaining what the action does.
        /// It is displayed for each command when the help command is called.
        /// </summary>
        /// <value>The description.</value>
        public string Description {
            get;
            protected set;
        }

        /// <summary>
        /// The string that when entered by the user in the debugger's prompt, will invoke the action.
        /// </summary>
        /// <value>The invocation.</value>
        public string Invocation {
            get;
            protected set;
        }

        /// <summary>
        /// Prints an help text for the action, including usage information, aliases if necessary etc.
        /// <param name="args">Arguments.</param>
        /// </summary>
        public abstract void PrintHelp(List<string> args);

        /// <summary>
        /// Gets the IOutput the action can write to.
        /// </summary>
        /// <value>The output.</value>
        protected IOutput Output {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets the action's parent action.
        /// </summary>
        /// <value>The parent.</value>
        public DebuggerAction Parent {
            get;
            set;
        }

        /// <summary>
        /// Gets a value indicating whether this action cannot execute due to a missing document
        /// </summary>
        /// <value><c>true</c> no document is loaded and we need one; otherwise, <c>false</c>.</value>
        public bool NeedsDocumentFirst {
            get;
            protected set;
        }

        /// <summary>
        /// Gets the full invocation of the action, including the ancestor's name.
        /// </summary>
        /// <value>The full invocation.</value>
        public string FullInvocation {
            get {
                var builder = new System.Text.StringBuilder();
                var current = this;

                while(current.Parent != null) {
                    builder.Insert(0, current.Invocation + (current != this ? " " : ""));
                    current = current.Parent;
                }

                return builder.ToString();
            }
        }

        /// <summary>
        /// Returns a list of autocompletion values for the current user input. The first item of the tuple is the base string on which the autocompletion was based.
        /// </summary>
        /// <param name="args">The string on which the autocompletion is based.</param>
        public virtual Tuple<string, List<string>> Autocomplete(string args)
        {
            return Tuple.Create(args, GetCompletionsWithPrefix(args));
        }

        /// <summary>
        /// Gets the values available for completion.
        /// </summary>
        /// <returns>The completion values.</returns>
        /// <param name="prefix">Prefix.</param>
        public abstract IEnumerable<string> GetCompletionValues(string prefix);

        /// <summary>
        /// Gets the index of the first parameter that is in raw form, or <c>int.MaxValue</c> if none exist.
        /// </summary>
        /// <value>The first raw input.</value>
        public abstract int FirstRawInput {
            get;
        }

        /// <summary>
        /// Gets the completions values matching the given prefix.
        /// </summary>
        /// <returns>The completions list.</returns>
        /// <param name="prefix">Prefix.</param>
        protected List<string> GetCompletionsWithPrefix(string prefix)
        {
            return new List<string>(from v in GetCompletionValues(prefix)
                                    where v.StartsWithInv(prefix)
                                    select v);
        }
    }
}

