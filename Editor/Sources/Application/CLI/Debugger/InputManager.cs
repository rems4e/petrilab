//
//  InputManager.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-02-27.
//

using System;
using System.Collections.Generic;
using System.IO;

namespace Petri.Application.CLI.Debugger
{
    /// <summary>
    /// The input manager of the CLI debugger.
    /// This class is responsible for managing all user input, and formatting the text output to the console.
    /// It also handles the command history of the debugger, and its persistence.
    /// </summary>
    public class InputManager
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.CLI.Debugger.InputManager"/> class.
        /// </summary>
        public InputManager()
        {
            Console.CancelKeyPress += InterruptHandler;

            LoadHistory();
            _commandHistory.AddLast("");
            _historyPtr = _commandHistory.Last;

            System.Threading.Tasks.Task.Run(() => {
                while(true) {
                    var key = Console.ReadKey(true);
                    lock(_keys) {
                        _keys.Enqueue(key);
                    }
                }
            });
        }

        /// <summary>
        /// Tries to read a line of text from the user.
        /// If the specified prompt is null, then the default prompt is used, and the obtained line of text is added to the command history.
        /// If the specified prompt is not null, then it is used instead of the default one, and nothing is added to the command history.
        /// </summary>
        /// <returns>The line read from the user, or null if the operation was interrupted by Ctrl-C.</returns>
        /// <param name="prompt">The prompt displayed at the beginning of the input. If null, then a default one is used.</param>
        public string TryReadLine(string prompt = null)
        {
            bool hasHistory = prompt == null;
            if(prompt == null) {
                prompt = PromptString;
            }
            _promptString = prompt;
            _prompting = true;
            _interruptFlag = false;
            Prompt();
            CursorPosition = 0;

            try {
                while(!Interrupt) {
                    ConsoleKeyInfo? k = null;
                    lock(_keys) {
                        if(_keys.Count > 0) {
                            k = _keys.Dequeue();
                        }
                    }
                    if(k != null) {
                        var key = (ConsoleKeyInfo)k;
                        if(hasHistory && key.Key == ConsoleKey.UpArrow) {
                            ClearN(_historyPtr.Value.Length);

                            _historyPtr.Value = _lastHistoryValue;

                            if(_historyPtr.Previous != null) {
                                _historyPtr = _historyPtr.Previous;
                            } else {
                                Console.Beep();
                            }

                            _lastHistoryValue = _historyPtr.Value;
                            Console.Write("{0}", _historyPtr.Value);
                            CursorPosition = _historyPtr.Value.Length;
                        } else if(hasHistory && key.Key == ConsoleKey.DownArrow) {
                            ClearN(_historyPtr.Value.Length);

                            _historyPtr.Value = _lastHistoryValue;

                            if(_historyPtr.Next != null) {
                                _historyPtr = _historyPtr.Next;
                            } else {
                                Console.Beep();
                            }

                            _lastHistoryValue = _historyPtr.Value;
                            Console.Write("{0}", _historyPtr.Value);
                            CursorPosition = _historyPtr.Value.Length;
                        } else if(key.Key == ConsoleKey.LeftArrow) {
                            --CursorPosition;
                        } else if(key.Key == ConsoleKey.RightArrow) {
                            ++CursorPosition;
                        } else if(key.Key == ConsoleKey.Enter) {
                            Console.Write("{0}", key.KeyChar);

                            if(_historyPtr.Value.Length == 0) {
                                return "";
                            } else {
                                var command = _historyPtr.Value;
                                if(hasHistory) {
                                    if(command.StartsWithInv(" ")) {
                                        // Do not store history entries starting with a space
                                        _historyPtr.Value = "";
                                    } else if(_commandHistory.Count >= 2 && _historyPtr == _commandHistory.Last && _historyPtr.Previous.Value == _historyPtr.Value) {
                                        // We try to avoid duplicate history entries here.
                                        _historyPtr.Value = "";
                                    } else {
                                        if(_historyPtr != _commandHistory.Last) {
                                            _historyPtr.Value = _lastHistoryValue;
                                            _commandHistory.Last.Value = command;
                                        }
                                        if(_commandHistory.Count > MaxHistorySize) {
                                            _commandHistory.RemoveFirst();
                                        }
                                        _commandHistory.AddLast("");
                                    }
                                } else {
                                    _historyPtr.Value = "";
                                }
                                _historyPtr = _commandHistory.Last;
                                CursorPosition = _historyPtr.Value.Length;
                                _lastHistoryValue = "";

                                return command;
                            }
                        } else if(key.Key == ConsoleKey.Backspace) {
                            if(CursorPosition > 0) {
                                var pos = CursorPosition - 1;

                                ClearN(_historyPtr.Value.Length);
                                _historyPtr.Value = _historyPtr.Value.Substring(0, pos) + _historyPtr.Value.Substring(pos + 1);
                                Console.Write("{0}", _historyPtr.Value);

                                CursorPosition = pos;
                            } else {
                                Console.Beep();
                            }
                        } else if(key.Key == ConsoleKey.Delete) {
                            if(CursorPosition < _historyPtr.Value.Length) {
                                var pos = CursorPosition;
                                ClearN(_historyPtr.Value.Length);
                                _historyPtr.Value = _historyPtr.Value.Substring(0, pos) + _historyPtr.Value.Substring(pos + 1);
                                Console.Write("{0}", _historyPtr.Value);

                                CursorPosition = pos;
                            } else {
                                Console.Beep();
                            }
                        } else if(key.Key == ConsoleKey.Tab) {
                            var pos = CursorPosition;
                            string args = _historyPtr.Value.Substring(0, pos);
                            var completions = DebugController.Actions.Autocomplete(args);

                            if(completions.Item2.Count > 0) {
                                // Searching the common prefix of all the completions.
                                int prefixLength = 0;
                                if(completions.Item2.Count == 1) {
                                    prefixLength = completions.Item2[0].Length - completions.Item1.Length;
                                } else {
                                    foreach(char c in completions.Item2[0].Substring(completions.Item1.Length)) {
                                        for(int i = 1; i < completions.Item2.Count; ++i) {
                                            if(completions.Item2[i].Length - completions.Item1.Length <= prefixLength || completions.Item2[i][prefixLength + completions.Item1.Length] != c) {
                                                goto EndPrefix;
                                            }
                                        }
                                        ++prefixLength;
                                    }
                                EndPrefix:
                                    ;
                                }

                                string commonPrefix = completions.Item2[0].Substring(completions.Item1.Length,
                                                                                     prefixLength);
                                if(commonPrefix.Length > 0 || (completions.Item2.Count == 1 && completions.Item2[0] == completions.Item1)) {
                                    if(completions.Item2.Count == 1 && !_historyPtr.Value.Substring(pos).StartsWithInv(" ") && !commonPrefix.EndsWithInv("/")) {
                                        commonPrefix += " ";
                                    }

                                    ClearN(_historyPtr.Value.Length);
                                    _historyPtr.Value = _historyPtr.Value.Substring(0, pos) + commonPrefix + _historyPtr.Value.Substring(pos);
                                    Console.Write("{0}", _historyPtr.Value);

                                    CursorPosition = pos + commonPrefix.Length;
                                } else {
                                    Console.WriteLine();
                                    DebugController.WriteLine("{0}", Configuration.GetLocalized("Available completions:"));
                                    foreach(var comp in completions.Item2) {
                                        DebugController.WriteLine("    {0}", comp);
                                    }
                                }
                            } else {
                                Console.Beep();
                            }
                        } else if(key.Key == ConsoleKey.A && key.Modifiers == ConsoleModifiers.Control) {
                            CursorPosition = 0;
                        } else if(key.Key == ConsoleKey.E && key.Modifiers == ConsoleModifiers.Control) {
                            CursorPosition = _historyPtr.Value.Length;
                        } else if(key.Key == ConsoleKey.D && key.Modifiers == ConsoleModifiers.Control) {
                            if(_prompting && _historyPtr.Value.Length == 0) {
                                DebugController.Exit(new List<string> { "--force" });
                                Console.Write("^D");
                                return null;
                            }
                        } else if(!char.IsControl(key.KeyChar)) {
                            var pos = CursorPosition;

                            ClearN(_historyPtr.Value.Length);
                            _historyPtr.Value = _historyPtr.Value.Substring(0, pos) + key.KeyChar + _historyPtr.Value.Substring(pos);
                            Console.Write("{0}", _historyPtr.Value);

                            CursorPosition = pos + 1;
                        }
                    } else {
                        System.Threading.Thread.Sleep(50);
                    }
                }

                _commandHistory.Last.Value = "";
                _historyPtr = _commandHistory.Last;
                _lastHistoryValue = "";

                return null;
            } finally {
                _prompting = false;
                _promptString = PromptString;
                Console.CursorLeft = 0;
            }
        }

        /// <summary>
        /// Writes a formatted string to the console.
        /// If a TryReadLine() operation is ongoing, then the text is ouput just before the current prompt.
        /// </summary>
        /// <param name="type">The type of output.</param>
        /// <param name="newLine">If set to <c>true</c> then a new line character is added after the output text.</param>
        /// <param name="output">The format text to output.</param>
        /// <param name="args">The format arguments.</param>
        public void WriteOutput(OutputType type, bool newLine, string output, params object[] args)
        {
            int cursorPosition = CursorPosition;
            if(_prompting) {
                ClearN(_promptString.Length + _historyPtr.Value.Length);
            }

            if(type == OutputType.Warning) {
                Console.ForegroundColor = ConsoleColor.DarkYellow;
            } else if(type == OutputType.Error) {
                Console.ForegroundColor = ConsoleColor.Red;
            }

            Console.Write(output, args);
            if(newLine) {
                Console.WriteLine();
            }

            Console.ResetColor();

            if(_prompting) {
                Prompt();
                Console.Write("{0}", _historyPtr.Value);
                CursorPosition = cursorPosition;
            }
        }

        /// <summary>
        /// Asks a binary question to the user with the specified prompt.
        /// </summary>
        /// <returns>The user's answer to the question.</returns>
        /// <param name="prompt">Prompt.</param>
        /// <param name="defaultsToYes">If set to <c>true</c>, then an empty answer from the user will be considered as <c>true</c>.</param>
        public bool PromptYesNo(string prompt, bool defaultsToYes = true)
        {
            if(_prompting) {
                ClearN(_promptString.Length + _historyPtr.Value.Length);
            }

            bool result = false;
            var answer = TryReadLine(prompt + string.Format(" [{0}/{1}] ",
                                                            Configuration.GetLocalized("Uppercase short yes answer"),
                                                            Configuration.GetLocalized("Lowercase short no answer")));
            if(answer == Configuration.GetLocalized("Lowercase short yes answer") || answer == Configuration.GetLocalized("Uppercase short yes answer") || (answer == "" && defaultsToYes)) {
                result = true;
            }

            return result;
        }

        /// <summary>
        /// Gets a value indicating whether an interrupt flag was raised by the user (Ctrl-C).
        /// </summary>
        /// <value><c>true</c> if an interruption is pending; otherwise, <c>false</c>.</value>
        public bool Interrupt {
            get {
                bool i = _interruptFlag;
                _interruptFlag = false;
                return i;
            }
        }

        /// <summary>
        /// Outputs the current prompt string to the console.
        /// </summary>
        void Prompt()
        {
            if(_promptString == PromptString) {
                Console.ForegroundColor = ConsoleColor.DarkGray;
            }
            Console.Write("{0}", _promptString);
            _promptColumn = Console.CursorLeft;

            Console.ResetColor();
        }

        /// <summary>
        /// Clears n characters from the console, starting from the end of the currently edited command.
        /// </summary>
        /// <param name="n">The number of characters to delete.</param>
        void ClearN(int n)
        {
            CursorPosition = _historyPtr.Value.Length;

            while(n > 0) {
                int toDelete = Math.Min(n, Console.CursorLeft);
                Console.Write(new String('\b', toDelete));
                n -= toDelete;

                if(n > 0) {
                    var newTop = --Console.CursorTop;
                    var newLeft = Console.BufferWidth - 1;
                    Console.CursorLeft = newLeft;
                    Console.Write(' ');
                    Console.CursorLeft = newLeft;
                    Console.CursorTop = newTop;
                    --n;
                }
            }
        }

        /// <summary>
        /// Gets the default prompt string.
        /// </summary>
        /// <value>The prompt string.</value>
        static string PromptString {
            get {
                return "(petri) ";
            }
        }

        /// <summary>
        /// The Ctrl-C handler.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="args">Arguments.</param>
        void InterruptHandler(object sender, ConsoleCancelEventArgs args)
        {
            Console.WriteLine("^C");
            args.Cancel = true;
            _interruptFlag = true;
        }

        /// <summary>
        /// Loads the history from a persistent location.
        /// </summary>
        void LoadHistory()
        {
            var historyPath = HistoryPath;
            try {
                int num = 0;
                if(File.Exists(historyPath)) {
                    foreach(string line in File.ReadLines(historyPath, System.Text.Encoding.UTF8)) {
                        _commandHistory.AddFirst(line);
                        ++num;
                        if(num == MaxHistorySize) {
                            break;
                        }
                    }
                }
            } catch(Exception e) {
                Console.Error.WriteLine("{0}", Configuration.GetLocalized("Could not load the debugger command's history from file at path {0}. Reason: {1}.",
                                                                          historyPath,
                                                                          e.Message));
            }
        }

        /// <summary>
        /// Saves the history to a persistent location.
        /// </summary>
        public void SaveHistory()
        {
            var historyPath = HistoryPath;
            DateTime? lastWrite = null;
            try {
                if(File.Exists(historyPath)) {
                    lastWrite = File.GetLastWriteTime(historyPath);
                }
            } catch {
                // Don't care
            }
            try {
                using(var file = File.Open(historyPath, FileMode.Create)) {
                    using(var writer = new StreamWriter(file)) {
                        var link = _commandHistory.Last.Previous;
                        while(link != null) {
                            writer.WriteLine("{0}", link.Value);
                            link = link.Previous;
                        }
                    }
                }
                if(lastWrite.HasValue) {
                    File.SetLastWriteTime(historyPath, lastWrite.Value);
                }
            } catch(Exception e) {
                Console.Error.WriteLine("{0}", Configuration.GetLocalized("Could not save the debugger command's history to path {0}. Reason: {1}.",
                                                                          historyPath,
                                                                          e.Message));
            }
        }

        /// <summary>
        /// Clears the commands history.
        /// </summary>
        public void ClearHistory()
        {
            _commandHistory.Clear();
            _commandHistory.AddLast("");
            _historyPtr = _commandHistory.Last;
        }

        /// <summary>
        /// Gets or sets the debug controller.
        /// </summary>
        /// <value>The debug controller.</value>
        public DebugController DebugController {
            get;
            set;
        }

        /// <summary>
        /// Gets the filesystem path where the commands history is to be persisted and retrieved.
        /// </summary>
        /// <value>The history path.</value>
        public static string HistoryPath {
            get {
                // TODO: remove the migration step somewhere in the future #99
                var newPath = Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.UserProfile),
                    ".petrilab_history"
                );

                var oldPath = Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.UserProfile),
                    ".petri_history"
                );

                try {
                    if(File.Exists(oldPath) && !File.Exists(newPath)) {
                        File.Move(oldPath, newPath);
                    }
                } catch {
                    // We tried…
                }

                return newPath;
            }
        }

        /// <summary>
        /// Gets the max number of entries in the commands history.
        /// </summary>
        /// <value>The size of the max history.</value>
        public static int MaxHistorySize {
            get {
                return 1000;
            }
        }

        /// <summary>
        /// Sets the cursor position relative to the end of the currently edited command.
        /// </summary>
        int CursorPosition {
            get {
                return _cursorPosition;
            }
            set {
                if(value >= 0 && value <= _historyPtr.Value.Length && Console.BufferWidth > 0) {
                    var oldLeft = Console.CursorLeft;
                    _cursorPosition = value;
                    Console.CursorLeft = (_promptColumn + value) % Console.BufferWidth;
                    if(oldLeft == Console.BufferWidth - 1 && Console.CursorLeft == 0) {
                        ++Console.CursorTop;
                    } else if(oldLeft == 0 && Console.CursorLeft == Console.BufferWidth - 1) {
                        --Console.CursorTop;
                    }
                }
            }
        }

        int _promptColumn;
        int _cursorPosition = 0;

        Queue<ConsoleKeyInfo> _keys = new Queue<ConsoleKeyInfo>();
        string _promptString = PromptString;
        LinkedList<string> _commandHistory = new LinkedList<string>();
        LinkedListNode<string> _historyPtr;
        string _lastHistoryValue = "";
        volatile bool _interruptFlag = false;
        volatile bool _prompting = false;
    }
}

