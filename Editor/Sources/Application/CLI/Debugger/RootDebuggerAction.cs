//
//  RootDebuggerAction.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-03-13.
//

using System;
using System.Collections.Generic;
using System.Linq;

namespace Petri.Application.CLI.Debugger
{
    /// <summary>
    /// The root debugger action.
    /// </summary>
    public class RootDebuggerAction : DebuggerActionGroup
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.CLI.Debugger.RootDebuggerAction"/> class.
        /// </summary>
        /// <param name="output">The output the action can write to.</param>
        /// <param name="actions">The sub-actions.</param>
        public RootDebuggerAction(IOutput output, IEnumerable<DebuggerAction> actions) : base(output,
                                                                                              actions.Union(new List<DebuggerAction> { new HelpDebuggerAction(output) }),
                                                                                              "",
                                                                                              "")
        {
            HelpDebuggerAction.Root = this;
            _actionsMapping[""] = this;

            var aliases = new Dictionary<string, string[]>();
            aliases.Add("exit", new string[] { "quit" });
            aliases.Add("q", new string[] { "quit" });
            aliases.Add("h", new string[] { "help" });
            aliases.Add("man", new string[] { "help" });
            aliases.Add("r", new string[] { "run" });
            aliases.Add("a", new string[] { "host attach" });
            aliases.Add("attach", new string[] { "host attach" });
            aliases.Add("ai",
                        new string[] {
                "host attach --internal",
                Configuration.GetLocalized("Attach to the debug server, hosted in this debugger's instance.")
            });
            aliases.Add("ae",
                        new string[] {
                "host attach --external",
                Configuration.GetLocalized("Attach to the debug server, hosted in an external application.")
            });
            aliases.Add("d", new string[] { "host detach" });
            aliases.Add("detach", new string[] { "host detach" });
            aliases.Add("eval", new string[] { "print" });
            aliases.Add("status", new string[] { "host status" });
            aliases.Add("resume", new string[] { "continue" });
            aliases.Add("c", new string[] { "continue" });
            aliases.Add("b", new string[] { "breakpoint add" });
            aliases.Add("find", new string[] { "search" });

            foreach(var pair in aliases) {
                _maxHelpAliasWidth = Math.Max(_maxHelpAliasWidth, pair.Key.Length);

                List<string> cmdAndArgs = new List<string> { "", pair.Value[0] };
                DebuggerAction action = this;
                do {
                    action = ((DebuggerActionGroup)action).ActionForInvocation(cmdAndArgs[0]);
                    cmdAndArgs = Application.SpaceSplit(cmdAndArgs.Count > 1 ? cmdAndArgs[1] : "", 1);
                } while(action is DebuggerActionGroup && cmdAndArgs.Count > 0);

                DebuggerAlias alias = new DebuggerAlias(Output,
                                                        pair.Key,
                                                        action,
                                                        pair.Value.Length > 1 ? pair.Value[1] : action.Description,
                                                        cmdAndArgs.Count == 0 ? "" : cmdAndArgs[0]);
                _actionsMapping[alias.Invocation] = alias;
                _sortedAliases.Add(alias);
            }

            _sortedAliases.Sort((a1, a2) => {
                return string.Compare(a1.Invocation, a2.Invocation, StringComparison.InvariantCulture);
            });
        }

        /// <summary>
        /// Prints the help text of the action.
        /// </summary>
        /// <param name="args">Arguments.</param>
        public override void PrintHelp(List<string> args)
        {
            base.PrintHelp(args);
            if(args.Count == 0) {
                Output.WriteLine("{0}", Configuration.GetLocalized("Command aliases:"));
                foreach(var a in _sortedAliases) {
                    Output.WriteLine("  {0} -- ({1}) {2}",
                                     a.Invocation.PadRight(_maxHelpAliasWidth),
                                     a.Aliased,
                                     a.Description);
                }

                Output.WriteLine();
                Output.WriteLine("{0}", Configuration.GetLocalized("For more information on any command, type 'help <command-name>'."));
            }
        }

        /// <summary>
        /// Gets the help header.
        /// </summary>
        /// <value>The help header.</value>
        protected override string HelpHeader {
            get {
                return Configuration.GetLocalized("Debugger commands:");
            }
        }

        /// <summary>
        /// Gets the help footer.
        /// </summary>
        /// <value>The help footer.</value>
        protected override string HelpFooter {
            get {
                return "";
            }
        }

        List<DebuggerAlias> _sortedAliases = new List<DebuggerAlias>();
        int _maxHelpAliasWidth = 0;
    }
}

