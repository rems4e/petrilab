//
//  BasicDebuggerAction.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2014-12-09.
//

using System;
using System.Collections.Generic;
using System.Linq;

namespace Petri.Application.CLI.Debugger
{
    /// <summary>
    /// A debugger action that wraps around a delegate call.
    /// </summary>
    public class BasicDebuggerAction : DebuggerAction
    {
        /// <summary>
        /// Enum representing the path completion policy of a <see cref="BasicDebuggerAction"/>.
        /// </summary>
        public enum PathCompletion
        {
            /// <summary>
            /// No path completion at all.
            /// </summary>
            Disabled,

            /// <summary>
            /// Only completing paths for directories.
            /// </summary>
            DirectoriesOnly,

            /// <summary>
            /// Path completion for every items, files or directories.
            /// </summary>
            All
        }

        /// <summary>
        /// A delagate type that gets called when a DebuggerAction is invoked. The delegate should
        /// return <c>false</c> only when the invocation was malformed.
        /// <param name="args">The arguments passed to the action.</param>">
        /// </summary>
        ///
        public delegate bool DebuggerActionDel(List<string> args);

        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.CLI.Debugger.BasicDebuggerAction"/> class.
        /// </summary>
        /// <param name="needsDocument"><c>true</c> if the action needs a document to execute; otherwise, <c>false</c></param>
        /// <param name="output">The object in charge of outputting text to the console.</param>
        /// <param name="action">The delegate that will be called when the action is invoked.
        /// This is the delegate's responsibility to block if the underlying action is asynchronous but the side effects should be synchronous.</param>
        /// <param name="description">Maps to <see cref="Petri.Application.CLI.Debugger.DebuggerAction.Description"/>.</param>
        /// <param name="help">A help string part of the output of the <c>PrintHelp</c> call.</param>
        /// <param name="syntax">Maps to <c>DebuggerAction.Description</c>.</param>
        /// <param name="invocation">Maps to <see cref="Petri.Application.CLI.Debugger.DebuggerAction.Invocation"/>.</param>
        /// <param name="arguments">The arguments that are accepted by the action.</param>
        /// <param name="pathCompletion">The path completion policy for this action.</param>
        /// <param name="firstRawInput">The action will not split its input arguments and will treat them as a whole, starting from the argument at this index.</param>
        public BasicDebuggerAction(bool needsDocument,
                                   IOutput output,
                                   DebuggerActionDel action,
                                   string description,
                                   string help,
                                   string syntax,
                                   string invocation,
                                   IEnumerable<string> arguments = null,
                                   PathCompletion pathCompletion = PathCompletion.Disabled,
                                   int firstRawInput = int.MaxValue) : base(output)
        {
            Description = description;
            Action = action;
            Invocation = invocation;

            _help = help;
            _syntax = syntax;

            _arguments = new List<string>(arguments ?? new string[] { });
            _arguments.Sort();

            _pathCompletion = pathCompletion;

            _firstRawInput = firstRawInput;

            NeedsDocumentFirst = needsDocument;
        }

        /// <summary>
        /// Executes the specified action with the specified args.
        /// This is this method's responsibility to block if the underlying action is asynchronous but the side effects
        /// should be synchronous.
        /// </summary>
        /// <param name="args">Arguments.</param>
        public override bool Execute(List<string> args)
        {
            if(args.Count > 0) {
                args = Application.SpaceSplit(args[0], FirstRawInput);
            }
            return Action(args);
        }

        /// <summary>
        /// The delegate that will be called when the action is invoked.
        /// </summary>
        /// <value>The action.</value>
        public DebuggerActionDel Action {
            get;
            private set;
        }

        /// <summary>
        /// A string explaining the usage of the command, with examples if necessary.
        /// </summary>
        /// <value>The syntax.</value>
        public string Syntax {
            get {
                return _syntax;
            }
        }

        /// <summary>
        /// Prints the help text of the action.
        /// </summary>
        /// <param name="args">Arguments.</param>
        public override void PrintHelp(List<string> args)
        {
            var syntaxes = Syntax.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
            if(!args.Contains("--usage-only")) {
                Output.WriteLine("{0}", _help);
                if(syntaxes.Length > 0) {
                    Output.WriteLine();
                }
            }
            var prefix = Configuration.GetLocalized("Syntax:");
            foreach(var syntax in syntaxes) {
                Output.WriteLine("{0} {1}", prefix, FullInvocation + " " + syntax);
                if(!prefix.StartsWithInv(" ")) {
                    prefix = "".PadLeft(prefix.Length);
                }
            }
        }

        /// <summary>
        /// Returns a list of autocompletions values for the current user input. The first item of the tuple is the base string on which the autocompletion was based.
        /// </summary>
        /// <param name="args">The string on which the autocompletion is based.</param>
        public override Tuple<string, List<string>> Autocomplete(string args)
        {
            var opt = new List<string> { "", args };
            while(true) {
                var opt2 = Application.SpaceSplit(opt[1], 1);
                if(opt2.Count < 2) {
                    return base.Autocomplete(opt[1]);
                }
                opt = opt2;
            }
        }

        /// <summary>
        /// Gets the index of the first parameter that is in raw form, or <c>int.MaxValue</c> if none exist.
        /// </summary>
        /// <value>The first raw input.</value>
        public override int FirstRawInput {
            get {
                return _firstRawInput;
            }
        }

        /// <summary>
        /// Gets the values available for completion. This lists the items of the specified directory, if the arguments is a path and path autocomplete is enabled.
        /// </summary>
        /// <returns>The completion values.</returns>
        /// <param name="prefix">Prefix.</param>
        public override IEnumerable<string> GetCompletionValues(string prefix)
        {
            if(_pathCompletion != PathCompletion.Disabled) {
                var list = new List<string>(_arguments);

                try {
                    bool quote = prefix.StartsWithInv("\"");
                    if(quote) {
                        prefix = prefix.Substring(1);
                    }
                    bool tilde = prefix.StartsWithInv("~");
                    string home = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
                    if(tilde) {
                        if(prefix.Length > 2) {
                            prefix = System.IO.Path.Combine(home, prefix.Substring(2));
                        } else if(prefix.Length == 2) {
                            prefix = home + System.IO.Path.DirectorySeparatorChar;
                        } else {
                            prefix = home;
                        }
                    }
                    if(!quote) {
                        prefix = prefix.Replace("\\ ", " ");
                    }
                    var path = prefix == "" ? Environment.CurrentDirectory : (prefix == "/" ? "/" : System.IO.Directory.GetParent(prefix).FullName);
                    if(System.IO.Directory.Exists(path)) {
                        var entries = System.IO.Directory.GetFileSystemEntries(path);
                        for(int i = 0; i < entries.Length; ++i) {
                            // Remove files if we only want directories
                            if(_pathCompletion == PathCompletion.DirectoriesOnly && !System.IO.Directory.Exists(entries[i])) {
                                entries[i] = "";
                                continue;
                            }

                            // Ignore dotted elements unless explicitely requested.
                            if(((!System.IO.Path.IsPathRooted(prefix) && !prefix.StartsWithInv(".")) ||
                                (System.IO.Path.IsPathRooted(prefix) && !prefix.Substring(path.Length).StartsWithInv("."))) && System.IO.Path.GetFileName(entries[i]).StartsWithInv(".")) {
                                entries[i] = "";
                                continue;
                            }

                            // Strip the absolute prefix to match the current directory if the string to complete is not an absolute path.
                            if(!System.IO.Path.IsPathRooted(prefix)) {
                                if(Environment.CurrentDirectory == "/") {
                                    entries[i] = entries[i].Substring(1);
                                } else {
                                    entries[i] = PathUtility.GetRelativePath(entries[i], Environment.CurrentDirectory);
                                }
                            }

                            if(System.IO.File.GetAttributes(entries[i]).HasFlag(System.IO.FileAttributes.Directory)) {
                                entries[i] = entries[i] + "/";
                            }
                            if(!entries[i].StartsWithInv(prefix)) {
                                // This also removes non-canonical paths completions, such as a/../a
                                // However, such non-canonical completions are currently disabled by the InputManager as well.
                                entries[i] = "";
                                continue;
                            }
                            if(tilde) {
                                entries[i] = "~" + entries[i].Substring(home.Length);
                            }
                        }

                        var nonEmpty = from e in entries
                                       where e.Length > 0
                                       select quote ? "\"" + e : e.Replace(" ",
                                                                           "\\ ");

                        list.AddRange(nonEmpty);
                    }
                } catch {
                    // Filesystem access related exceptions, such as System.UnauthorizedAccessException or System.UriFormatException.
                }

                return list;
            } else {
                return _arguments;
            }
        }

        int _firstRawInput;
        string _syntax;
        string _help;
        List<string> _arguments;
        PathCompletion _pathCompletion;
    }
}

