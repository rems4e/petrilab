//
//  DebugController.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-01.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

using Petri.Application.Debugger;
using Petri.Model;

using Action = Petri.Model.Action;

namespace Petri.Application.CLI.Debugger
{
    /// <summary>
    /// Output type.
    /// </summary>
    public enum OutputType
    {
        /// <summary>
        /// Normal output
        /// </summary>
        Normal,

        /// <summary>
        /// Warning output
        /// </summary>
        Warning,

        /// <summary>
        /// Error output
        /// </summary>
        Error,
    }

    /// <summary>
    /// The controller of the CLI debugger.
    /// </summary>
    public class DebugController : Petri.Application.Debugger.DebugController, IOutput
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.CLI.Debugger.DebugController"/> class.
        /// </summary>
        /// <param name="doc">Document.</param>
        /// <param name="inputManager">The input manager.</param>
        public DebugController(DebuggableHeadlessDocument doc, InputManager inputManager) : base(new PetriNetProvider(doc),
                                                                                                 new DebugClient(doc, doc))
        {
            _documentPath = doc.Path;
            _documentSettings = doc.Settings;

            _inputManager = inputManager;
            _inputManager.DebugController = this;

            var actions = new List<DebuggerAction>();

            bool disableForNoDocumentLoaded = _documentPath == "";

            actions.Add(new BasicDebuggerAction(false, this, Exit,
                                                Configuration.GetLocalized("Quit."),
                                                Configuration.GetLocalized("Quits out of the petri net debugger."),
                                                "",
                                                "quit", new string[] { "--force" }));

            actions.Add(new BasicDebuggerAction(false, this, Load,
                                                Configuration.GetLocalized("Load a document."),
                                                Configuration.GetLocalized("Loads a document into the debugger."),
                                                Configuration.GetLocalized("<document path>"),
                                                "load", null, BasicDebuggerAction.PathCompletion.All));

            actions.Add(new BasicDebuggerAction(false, this, Unload,
                                                Configuration.GetLocalized("Unload the document."),
                                                Configuration.GetLocalized("Unloads the document from the debugger."),
                                                "",
                                                "unload", new string[] { "--force" }));

            {
                var runCompletions = new List<string> { "--log-states", "--log-transitions", "--log-nothing", "--log-return-values" };
                if(!disableForNoDocumentLoaded) {
                    foreach(var param in PetriNet.Parameters) {
                        runCompletions.Add(param.MakeUserReadable() + ":");
                    }
                }
                actions.Add(new BasicDebuggerAction(disableForNoDocumentLoaded, this, Run,
                                                    Configuration.GetLocalized("Run the petri net."),
                                                    Configuration.GetLocalized("Runs the petri net. If some parameters are specified, their values override the default. If {0}, {1}, {2} and/or {3} is specified, the default log level of the petri net is overriden.", "--log-nothing", "--log-states", "--log-transitions", "--log-return-values"),
                                                    Configuration.GetLocalized("[{0}] [{1}] [{2}] [{3}] [<$param1>: <value1> [<$param2>: <value2> […]]]", "--log-nothing", "--log-states", "--log-transitions", "--log-return-values"),
                                                    "run", runCompletions));
            }

            string[] evalArgs = null;
            string evalSyntax = Configuration.GetLocalized("<expression>");
            string evalHelp = Configuration.GetLocalized("Evaluates an expression.");
            int evalFirstRaw = 0;
            if(_documentSettings.Language == Code.Language.C) {
                evalArgs = new string[] { "-f" };
                evalSyntax = Configuration.GetLocalized("-f <printf-like-formatter> <expression>");
                evalHelp += " " + Configuration.GetLocalized("The value of the -f parameter is mandatory, and must be a valid printf-like formatter. For example, is the expression to compute evaluates to an int, the formatter must be \"%d\", with or without the quotes.");
                evalFirstRaw = 2;
            }
            actions.Add(new BasicDebuggerAction(disableForNoDocumentLoaded, this, Print,
                                                Configuration.GetLocalized("Evaluate an expression."),
                                                evalHelp,
                                                evalSyntax,
                                                "print",
                                                evalArgs,
                                                BasicDebuggerAction.PathCompletion.Disabled, evalFirstRaw));

            actions.Add(new BasicDebuggerAction(disableForNoDocumentLoaded, this, Stop,
                                                Configuration.GetLocalized("Stop the petri net's execution."),
                                                Configuration.GetLocalized("Stops the petri net's execution."),
                                                "",
                                                "stop"));

            actions.Add(new BasicDebuggerAction(disableForNoDocumentLoaded, this, Continue,
                                                Configuration.GetLocalized("Resume the petri net's execution."),
                                                Configuration.GetLocalized("Resumes the petri net's execution."),
                                                "",
                                                "continue"));

            actions.Add(new BasicDebuggerAction(false, this, ChangeDirectory,
                                                Configuration.GetLocalized("Change the current working directory."),
                                                Configuration.GetLocalized("Changes the current working directory to the given argument, or to the parent directory of the debugged petri net if no path is given."),
                                                Configuration.GetLocalized("[path-to-directory]"),
                                                "cd", null, BasicDebuggerAction.PathCompletion.DirectoriesOnly));

            actions.Add(new BasicDebuggerAction(false, this, PrintDirectory,
                                                Configuration.GetLocalized("Print the current working directory."),
                                                Configuration.GetLocalized("Prints the current working directory."),
                                                "",
                                                "pwd"));

            {
                var breakpoints = new List<DebuggerAction> {
                    new BasicDebuggerAction(disableForNoDocumentLoaded, this, AddBreakpoints,
                                            Configuration.GetLocalized("Add new breakpoints."),
                                            Configuration.GetLocalized("Adds new breakpoints to some states by their IDs."),
                                            Configuration.GetLocalized("<state ID 1> [<state ID 2> […]]"),
                                            "add"),

                    new BasicDebuggerAction(disableForNoDocumentLoaded, this, RemoveBreakpoints,
                                            Configuration.GetLocalized("Delete some breakpoints."),
                                            Configuration.GetLocalized("Deletes some breakpoints by the ID of their states."),
                                            Configuration.GetLocalized("<state ID 1> [<state ID 2> […]]"),
                                            "delete"),

                    new BasicDebuggerAction(disableForNoDocumentLoaded, this, RemoveBreakpoints,
                                            Configuration.GetLocalized("Delete some breakpoints."),
                                            Configuration.GetLocalized("Deletes some breakpoints by the ID of their states."),
                                            Configuration.GetLocalized("<state ID 1> [<state ID 2> […]]"),
                                            "remove"),

                    new BasicDebuggerAction(disableForNoDocumentLoaded, this, ClearBreakpoints,
                                            Configuration.GetLocalized("Delete all the breakpoints."),
                                            Configuration.GetLocalized("Deletes all of the existing breakpoints."),
                                            "",
                                            "clear"),

                    new BasicDebuggerAction(disableForNoDocumentLoaded, this, ListBreakpoints,
                                            Configuration.GetLocalized("List the breakpoints."),
                                            Configuration.GetLocalized("Prints a list of the currently installed breakpoints."),
                                            "",
                                            "list")
                };
                actions.Add(new DebuggerActionGroup(this, breakpoints, Configuration.GetLocalized("Manage breakpoints."),
                                                    "breakpoint"));
            }

            actions.Add(new BasicDebuggerAction(disableForNoDocumentLoaded, this, SearchEntities,
                                                Configuration.GetLocalized("List the entities containing the given text or pattern."),
                                                Configuration.GetLocalized("Lists the entities that contain a given text (by default) or pattern (if the \"--regex\" argument is given). By default the search is made on all entities (states, transitions and comments) on the \"ID\", \"Name\" and \"Code\" fields, but it can be restricted to one or more of them with the arguments \"--state\", \"--transition\" and \"--comment\" to search only for one or more of states, transitions and comments, in their ID (\"--id\"), name (\"--name\"), and/or code (\"code\")."),
                                                Configuration.GetLocalized("[--state|-s] [--transition|-t] [--comment|-c] [--id] [--name] [--code] [--regex] <text>"),
                                                "search",
                                                new string[] { "--state", "--transition", "--comment", "--id", "--name", "--code", "--regex" }));

            actions.Add(new BasicDebuggerAction(disableForNoDocumentLoaded, this, ListVariables,
                                                Configuration.GetLocalized("List the petri net's variables with their value."),
                                                Configuration.GetLocalized("Lists the petri net's variables with their value.\nIf the petri net is not running, the values of the variables will not be retrieved."),
                                                "",
                                                "lsvar"));

            actions.Add(new BasicDebuggerAction(disableForNoDocumentLoaded, this, ListParameters,
                                                Configuration.GetLocalized("List the petri net's parameters with their default value."),
                                                Configuration.GetLocalized("Lists the petri net's parameters with their default value."),
                                                "",
                                                "lsparam"));

            actions.Add(new BasicDebuggerAction(disableForNoDocumentLoaded, this, ListActiveStates,
                                                Configuration.GetLocalized("List the petri net's active states."),
                                                Configuration.GetLocalized("Lists the petri net's active states."),
                                                "",
                                                "lsstates"));

            actions.Add(new BasicDebuggerAction(false, this, PrintVersion,
                                                Configuration.GetLocalized("Print the debugger's version."),
                                                Configuration.GetLocalized("Prints the debugger's version."),
                                                "",
                                                "version"));

            {
                var host = new List<DebuggerAction>();
                host.Add(new BasicDebuggerAction(disableForNoDocumentLoaded, this, CreateHost,
                                                 Configuration.GetLocalized("Create an external host."),
                                                 Configuration.GetLocalized("Creates an external host by launching a new executable, passing it command-line arguments if they are specified."),
                                                 Configuration.GetLocalized("<executable> [-- <host-args>]"),
                                                 "create", null, BasicDebuggerAction.PathCompletion.All));

                host.Add(new BasicDebuggerAction(disableForNoDocumentLoaded, this,
                                                 Attach,
                                                 Configuration.GetLocalized("Attach to the debug server."),
                                                 Configuration.GetLocalized("Attaches this client to an active debug server.\nWhether the petri net execution is hosted by the debugger or by an external application is retrieved from the document's settings.\nIf the --internal option is specified, the petri net will be forced to be hosted by the debugger, whereas specifying --external will force the debugger to attempt to connect to an external application.\nThe --create option will create an host using the petri net's settings, or the next argument as executable, and the rest as the executable's arguments if they are specified."),
                                                 Configuration.GetLocalized("--internal\n--external\n--create [<executable> [<host-args>]"),
                                                 "attach",
                                                 new string[] {
                    "--create",
                    "--internal",
                    "--external"
                }, BasicDebuggerAction.PathCompletion.All, 2));

                host.Add(new BasicDebuggerAction(disableForNoDocumentLoaded, this, Detach,
                                                 Configuration.GetLocalized("Detach from the debug server."),
                                                 Configuration.GetLocalized("Detaches this client from its debug server."),
                                                 "",
                                                 "detach"));

                host.Add(new BasicDebuggerAction(disableForNoDocumentLoaded, this, Release,
                                                 Configuration.GetLocalized("Stop the debug server and let its process execute."),
                                                 Configuration.GetLocalized("Stops the debug server and let its process execute. The process will not be reachable from the debug server."),
                                                 "",
                                                 "release"));

                host.Add(new BasicDebuggerAction(false, this, PrintStatus,
                                                 Configuration.GetLocalized("Print the debug server's host's status."),
                                                 Configuration.GetLocalized("Outputs whether the debugger is attached, and the state of the petri net."),
                                                 "",
                                                 "status"));

                actions.Add(new DebuggerActionGroup(this, host, Configuration.GetLocalized("Manage the petri net's host."),
                                                    "host"));
            }

            {
                var tokens = new List<DebuggerAction>();
                tokens.Add(new BasicDebuggerAction(disableForNoDocumentLoaded, this, ShowTokens,
                                                 Configuration.GetLocalized("Display the current tokens of each state of the petri net."),
                                                 Configuration.GetLocalized("Displays the petri net's states with their tokens count."),
                                                 "",
                                                 "show"));

                actions.Add(new DebuggerActionGroup(this, tokens, Configuration.GetLocalized("Manage the petri net's states' tokens."),
                                                    "token"));
            }

            {
                var history = new List<DebuggerAction>();
                history.Add(new BasicDebuggerAction(false, this, ClearHistory,
                                                    Configuration.GetLocalized("Clear the history."),
                                                    Configuration.GetLocalized("Removes all the saved commands from the history."),
                                                    "",
                                                    "clear"));

                actions.Add(new DebuggerActionGroup(this, history, Configuration.GetLocalized("Manage the debugger's commands history."),
                                                    "history"));
            }

            {
                var profiles = new List<DebuggerAction> {
                    new BasicDebuggerAction(disableForNoDocumentLoaded, this, GetCurrentProfile,
                                            Configuration.GetLocalized("Get the current settings profile."),
                                            Configuration.GetLocalized("Prints the name of the document's current settings profile."),
                                            "",
                                            "get"),

                    new BasicDebuggerAction(disableForNoDocumentLoaded, this, SetCurrentProfile,
                                            Configuration.GetLocalized("Set the current settings profile."),
                                            Configuration.GetLocalized("Sets the document's current settings profile by name."),
                                            Configuration.GetLocalized("<profile name>"),
                                            "set",
                                            (from prof in _documentSettings.Profiles select prof.Name).ToArray()),

                    new BasicDebuggerAction(disableForNoDocumentLoaded, this, ListProfiles,
                                            Configuration.GetLocalized("List the available settings profiles."),
                                            Configuration.GetLocalized("Lists the document's settings profiles' names."),
                                            "",
                                            "list"),
                };
                actions.Add(new DebuggerActionGroup(this, profiles, Configuration.GetLocalized("Manage profiles."),
                                                    "profile"));
            }

            _actions = new RootDebuggerAction(this, actions);
        }

        /// <summary>
        /// The CLI debugger's main loop.
        /// </summary>
        public Tuple<int, string> Debug()
        {
            if(_documentPath == "") {
                WriteLine("{0}", Configuration.GetLocalized("No document loaded."));
            } else {
                WriteLine("{0}", Configuration.GetLocalized("Current petri net set to '{0}'",
                                                            _documentSettings.Name));
                _oldDebugMode = _documentSettings.CurrentProfile.DebugMode;
                if(_documentSettings.CurrentProfile.DebugMode == DebugMode.RunInEditor) {
                    WriteLine("{0}", Configuration.GetLocalized("The petri net will be hosted by the debugger."));
                } else if(_documentSettings.CurrentProfile.DebugMode == DebugMode.CreateHost) {
                    WriteLine("{0}", Configuration.GetLocalized(
                        "The petri net will be hosted by the program '{0}', launched with arguments '{1}'.",
                        _documentSettings.CurrentProfile.HostProgram,
                        _documentSettings.CurrentProfile.HostProgramArguments
                    ));
                } else {
                    WriteLine("{0}", Configuration.GetLocalized("The petri net will be hosted by an external application."));
                }
            }

            while(_alive) {
                try {
                    string line = _inputManager.TryReadLine();
                    if(!string.IsNullOrEmpty(line)) {
                        _actions.Execute(new List<string> { line });
                    }
                } catch(Exception e) {
                    if(e is AggregateException) {
                        e = e.InnerException ?? e;
                    }
                    WriteLine("{0}", Configuration.GetLocalized("An error occurred in the debugger loop: {0} - {1}",
                                                                e.GetType(),
                                                                e.Message));
                }
            }

            _inputManager.SaveHistory();

            return Tuple.Create(_returnCode, _nextDocument);
        }

        /// <summary>
        /// Gets the actions available from the debugger's command prompt.
        /// </summary>
        /// <value>The actions.</value>
        public DebuggerActionGroup Actions {
            get {
                return _actions;
            }
        }

        /// <summary>
        /// Gets a value indicating whether an interrupt flag was raised by the user (Ctrl-C).
        /// </summary>
        /// <value><c>true</c> if an interruption is pending; otherwise, <c>false</c>.</value>
        public bool Interrupt {
            get {
                return _inputManager.Interrupt;
            }
        }

        /// <summary>
        /// Notifies the user from an unrecoverable error.
        /// </summary>
        /// <param name="error">The error message.</param>
        public void NotifyUnrecoverableError(string error)
        {
            WriteErrorLine("{0}", error);
        }

        /// <summary>
        /// Notifies when the debug client detaches from the debug server.
        /// </summary>
        public void NotifyDetached()
        {
            _documentSettings.CurrentProfile.DebugMode = _oldDebugMode;
        }

        /// <summary>
        /// Write the specified value and args.
        /// </summary>
        /// <param name="value">Value.</param>
        /// <param name="args">Arguments.</param>
        public void Write(string value, params object[] args)
        {
            _inputManager.WriteOutput(OutputType.Normal, false, value, args);
        }

        /// <summary>
        /// Writes the line.
        /// </summary>
        /// <param name="value">Value.</param>
        /// <param name="args">Arguments.</param>
        public void WriteLine(string value, params object[] args)
        {
            _inputManager.WriteOutput(OutputType.Normal, true, value, args);
        }

        /// <summary>
        /// Writes the line.
        /// </summary>
        public void WriteLine()
        {
            _inputManager.WriteOutput(OutputType.Normal, true, "");
        }

        /// <summary>
        /// Writes the warning line.
        /// </summary>
        /// <param name="value">Value.</param>
        /// <param name="args">Arguments.</param>
        public void WriteWarningLine(string value, params object[] args)
        {
            _inputManager.WriteOutput(OutputType.Warning, true, value, args);
        }


        /// <summary>
        /// Writes the error.
        /// </summary>
        /// <param name="value">Value.</param>
        /// <param name="args">Arguments.</param>
        public void WriteError(string value, params object[] args)
        {
            _inputManager.WriteOutput(OutputType.Error, false, value, args);
        }

        /// <summary>
        /// Writes the error line.
        /// </summary>
        /// <param name="value">Value.</param>
        /// <param name="args">Arguments.</param>
        public void WriteErrorLine(string value, params object[] args)
        {
            _inputManager.WriteOutput(OutputType.Error, true, value, args);
        }

        /// <summary>
        /// Writes the error line.
        /// </summary>
        public void WriteErrorLine()
        {
            _inputManager.WriteOutput(OutputType.Error, true, "");
        }
        /// <summary>
        /// Asks a binary question to the user with the specified prompt.
        /// </summary>
        /// <returns>The user's answer to the question.</returns>
        /// <param name="prompt">Prompt.</param>
        /// <param name="defaultsToYes">If set to <c>true</c>, then an empty answer from the user will be considered as <c>true</c>.</param>
        public bool PromptYesNo(string prompt, bool defaultsToYes = true)
        {
            return _inputManager.PromptYesNo(prompt, defaultsToYes);
        }

        /// <summary>
        /// Loads the specified document from the debugger.
        /// </summary>
        /// <param name="args">The command's arguments.</param>
        public bool Load(List<string> args)
        {
            if(_documentPath != "") {
                NotifyUnrecoverableError(Configuration.GetLocalized("A document is already loaded."));
                if(_inputManager.PromptYesNo(Configuration.GetLocalized("Do you want to unload it?"))) {
                    Unload(new List<string>());
                    if(_alive != false) {
                        return true;
                    }
                } else {
                    return true;
                }
            }

            _nextDocument = args[0];
            _alive = false;

            return true;
        }

        /// <summary>
        /// Unloads the specified document from the debugger.
        /// </summary>
        /// <param name="args">The command's arguments.</param>
        public bool Unload(List<string> args)
        {
            Exit(args);
            _nextDocument = "";

            return true;
        }

        /// <summary>
        /// Exits from the debugger.
        /// </summary>
        /// <param name="args">The command's arguments.</param>
        public bool Exit(List<string> args)
        {
            if(args.Contains("--force")) {
                Client.StopSession();

                _returnCode = 0;
                _alive = false;
            } else {
                if(Client.CurrentPetriState != DebugClient.PetriState.Stopped) {
                    NotifyUnrecoverableError(Configuration.GetLocalized("The petri net is running."));
                    if(_inputManager.PromptYesNo(Configuration.GetLocalized("Do you want to stop it and exit anyway?"))) {
                        Stop(new List<string>());
                    }
                }

                if(Client.CurrentPetriState == DebugClient.PetriState.Stopped) {
                    Client.Detach();

                    _returnCode = 0;
                    _alive = false;
                }
            }

            return true;
        }

        /// <summary>
        /// Attaches to the debug server.
        /// </summary>
        /// <param name="args">The command's arguments.</param>
        bool Attach(List<string> args)
        {
            if(Client.CurrentSessionState != DebugClient.SessionState.Stopped) {
                NotifyUnrecoverableError(Configuration.GetLocalized("The debugger is already attached."));
            } else {
                int intern = args.LastIndexOf("--internal");
                int external = args.LastIndexOf("--external");
                int create = args.LastIndexOf("--create");
                if(intern != external && create != -1) {
                    NotifyUnrecoverableError(Configuration.GetLocalized("--create cannot be specified with either --internal or --external."));
                    return false;
                }
                if(intern > external) {
                    if(_documentSettings.CurrentProfile.DebugMode != DebugMode.RunInEditor) {
                        _documentSettings.CurrentProfile.DebugMode = DebugMode.RunInEditor;
                        WriteLine("{0}", Configuration.GetLocalized("The petri net will be hosted by the debugger."));
                    }
                } else if(external > intern) {
                    if(_documentSettings.CurrentProfile.DebugMode != DebugMode.Attach) {
                        _documentSettings.CurrentProfile.DebugMode = DebugMode.Attach;
                        WriteLine("{0}", Configuration.GetLocalized("The petri net will be hosted by an external application."));
                    }
                } else if(create != -1) {
                    if(create < args.Count - 1) {
                        _documentSettings.CurrentProfile.HostProgram = args[create + 1];
                        if(create < args.Count - 2) {
                            _documentSettings.CurrentProfile.HostProgramArguments = string.Join(" ", args.ToArray(), create + 2, args.Count - (create + 2));
                        }
                    }
                    _documentSettings.CurrentProfile.DebugMode = DebugMode.CreateHost;
                    WriteLine("{0}", Configuration.GetLocalized(
                        "The petri net will be hosted by the program '{0}', launched with arguments '{1}'.",
                        _documentSettings.CurrentProfile.HostProgram,
                        _documentSettings.CurrentProfile.HostProgramArguments
                    ));
                }

                Client.Attach().Wait();
            }

            return true;
        }

        /// <summary>
        /// Detaches from the debug server.
        /// </summary>
        /// <param name="args">The command's arguments.</param>
        bool Detach(List<string> args)
        {
            if(Client.CurrentSessionState == DebugClient.SessionState.Stopped) {
                NotifyUnrecoverableError(Configuration.GetLocalized("The debugger is not connected. Nothing to detach from."));
            } else {
                if(Client.CurrentPetriState != DebugClient.PetriState.Stopped) {
                    NotifyUnrecoverableError(Configuration.GetLocalized("The petri net is running."));
                    if(_inputManager.PromptYesNo(Configuration.GetLocalized("Do you want to stop it and detach anyway?"))) {
                        Stop(new List<string>());
                    }
                }

                if(Client.CurrentPetriState == DebugClient.PetriState.Stopped) {
                    Client.Detach();
                    WaitFor(() => {
                        return Client.CurrentSessionState == DebugClient.SessionState.Stopped;
                    });
                }
            }

            return true;
        }

        /// <summary>
        /// Stops the debug server and lets the process run freely.
        /// </summary>
        /// <param name="args">Arguments.</param>
        bool Release(List<string> args)
        {
            if(Client.CurrentSessionState == DebugClient.SessionState.Stopped) {
                NotifyUnrecoverableError(Configuration.GetLocalized("The debugger is not connected. Nothing to detach from."));
            } else {
                if(Client.CurrentPetriState != DebugClient.PetriState.Stopped) {
                    NotifyUnrecoverableError(Configuration.GetLocalized("The petri net is running."));
                    if(_inputManager.PromptYesNo(Configuration.GetLocalized("Do you want to stop it and detach anyway?"))) {
                        Stop(new List<string>());
                    }
                }

                if(Client.CurrentPetriState == DebugClient.PetriState.Stopped) {
                    Client.StopSession();
                    WaitFor(() => {
                        return Client.CurrentSessionState == DebugClient.SessionState.Stopped;
                    });
                }
            }

            return true;
        }

        /// <summary>
        /// Runs the petri net, optionnaly attaching to the debug server
        /// </summary>
        /// <param name="args">The command's arguments.</param>
        bool Run(List<string> args)
        {
            if(Client.CurrentSessionState != DebugClient.SessionState.Started) {
                NotifyUnrecoverableError(Configuration.GetLocalized("The debugger must be attached to the debug server to be able to run the petri net."));
                if(_inputManager.PromptYesNo(Configuration.GetLocalized("Do you want to attach?"))) {
                    Attach(new List<string>());
                }
            }

            if(Client.CurrentSessionState == DebugClient.SessionState.Started) {
                if(Client.CurrentPetriState != DebugClient.PetriState.Stopped) {
                    NotifyUnrecoverableError(Configuration.GetLocalized("The petri net is already running."));
                    if(_inputManager.PromptYesNo(Configuration.GetLocalized("Do you want to stop it and start it again?"))) {
                        Stop(new List<string>());
                    }
                }

                if(Client.CurrentPetriState == DebugClient.PetriState.Stopped) {
                    Runtime.PetriNet.LogVerbosity? verbosity = null;
                    if(args.Contains("--log-nothing")) {
                        verbosity = Runtime.PetriNet.LogVerbosity.Nothing;
                    } else {
                        if(args.Contains("--log-states")) {
                            if(!verbosity.HasValue) {
                                verbosity = Runtime.PetriNet.LogVerbosity.Nothing;
                            }
                            verbosity |= Runtime.PetriNet.LogVerbosity.States;
                        }
                        if(args.Contains("--log-transitions")) {
                            if(!verbosity.HasValue) {
                                verbosity = Runtime.PetriNet.LogVerbosity.Nothing;
                            }
                            verbosity |= Runtime.PetriNet.LogVerbosity.Transitions;
                        }
                        if(args.Contains("--log-return-values")) {
                            if(!verbosity.HasValue) {
                                verbosity = Runtime.PetriNet.LogVerbosity.Nothing;
                            }
                            verbosity |= Runtime.PetriNet.LogVerbosity.ReturnValues;
                        }
                    }

                    var concatArgs = new List<string>();
                    for(int i = 0; i < args.Count; ++i) {
                        var arg = args[i];
                        if(arg.EndsWithInv(":") && i != args.Count - 1) {
                            arg += args[++i];
                            concatArgs.Add(arg);
                        }
                    }

                    Dictionary<UInt32, Int64> param = null;
                    try {
                        param = CLIApplication.ParsePetriNetArguments(concatArgs, PetriNet);
                    } catch(KeyNotFoundException e) {
                        WriteErrorLine(Configuration.GetLocalized("Invalid petri net argument's name '{0}'!", e.Message));
                    } catch(ArgumentException e) {
                        WriteErrorLine(Configuration.GetLocalized("Invalid petri net argument's value '{0}'!", e.ParamName));
                    }

                    if(param != null) {
                        Client.StartPetri(param, verbosity);
                        WaitForPausedOrStopped();
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Stops the petri net.
        /// </summary>
        /// <param name="args">The command's arguments.</param>
        bool Stop(List<string> args)
        {
            if(Client.CurrentPetriState == DebugClient.PetriState.Stopped) {
                NotifyUnrecoverableError(Configuration.GetLocalized("The petri net is not running. Nothing to stop."));
            } else {
                Client.StopPetri();
                WaitFor(() => {
                    return Client.CurrentPetriState == DebugClient.PetriState.Stopped;
                });
            }

            return true;
        }

        /// <summary>
        /// Resumes the petri net's execution, if it is paused.
        /// </summary>
        /// <param name="args">The command's arguments.</param>
        bool Continue(List<string> args)
        {
            if(Client.CurrentPetriState != DebugClient.PetriState.Paused) {
                NotifyUnrecoverableError(Configuration.GetLocalized("The petri net is not paused. Nothing to resume."));
            } else {
                Client.SetPause(false);
                WaitForPausedOrStopped();
            }

            return true;
        }

        /// <summary>
        /// Evaluates and prints an expression.
        /// </summary>
        /// <param name="args">The command's arguments.</param>
        bool Print(List<string> args)
        {
            var result = Eval(args);
            if(result.Item1 != null) {
                WriteLine("{0}", result.Item1);
            }

            return result.Item2;
        }

        /// <summary>
        /// Evaluates a code expression and returns the result of the evaluation.
        /// </summary>
        /// <param name="args">The command's arguments.</param>
        /// <returns>The evaluation result (or null if none), and <c>false</c> if the invocation was malformed (<c>true otherwise</c>)</returns>
        Tuple<string, bool> Eval(List<string> args)
        {
            if(Client.CurrentSessionState != DebugClient.SessionState.Started) {
                NotifyUnrecoverableError(Configuration.GetLocalized("The debugger must be attached to the debug server to evaluate an expression.\nTry 'attach'."));
                return Tuple.Create((string)null, true);
            } else {
                object[] userData = null;
                string expr = null;
                if(_documentSettings.Language == Code.Language.C) {
                    if(args.Count != 3 && args.IndexOf("-f") != 0) {
                        return Tuple.Create((string)null, false);
                    }

                    userData = new object[] { args[1] };
                    expr = args[2];
                } else if(args.Count == 1) {
                    expr = args[0];
                } else {
                    return Tuple.Create((string)null, false);
                }

                _evalResult = null;
                // TODO: allow to evaluate expressions from within external petri nets
                Client.Evaluate(
                    PetriNet,
                    Code.Expression.CreateFromString(expr, _documentSettings.Language),
                    userData
                );
                WaitFor(() => _evalResult != null);

                var evalResult = _evalResult;
                _evalResult = null;

                var message = evalResult.Item1;
                if(!string.IsNullOrEmpty(evalResult.Item2)) {
                    message = Configuration.GetLocalized("{0}nnAdditional message: '{1}'", message, evalResult.Item2);
                }

                return Tuple.Create(message, true);
            }
        }

        /// <summary>
        /// Sets the evaluation result.
        /// </summary>
        /// <value>The eval result, with the value as its first item, and some additional user info as its second item.</value>
        public Tuple<string, string> EvalResult {
            set {
                _evalResult = value;
            }
        }

        /// <summary>
        /// Sets the variables' values.
        /// </summary>
        /// <value>The variables.</value>
        public List<Tuple<RootPetriNet, Code.VariableExpression, string>> Vars {
            set {
                _vars = value;
            }
        }

        /// <summary>
        /// Prints the debugger's status.
        /// </summary>
        /// <param name="args">The command's arguments.</param>
        bool PrintStatus(List<string> args)
        {
            if(_documentPath == "") {
                WriteLine("{0}", Configuration.GetLocalized("No document loaded."));
            } else {
                WriteLine("{0}", Configuration.GetLocalized("Loaded document: {0} ({1}).", _documentSettings.Name, _documentPath));

                switch(Client.CurrentSessionState) {
                case DebugClient.SessionState.Started:
                    WriteLine("{0}", Configuration.GetLocalized("The debugger is attached to the server."));
                    break;
                case DebugClient.SessionState.Stopped:
                    WriteLine("{0}", Configuration.GetLocalized("The debugger is detached from the server."));
                    break;
                case DebugClient.SessionState.Starting:
                    WriteLine("{0}", Configuration.GetLocalized("The debugger is attempting to attach to the server."));
                    break;
                }

                switch(Client.CurrentPetriState) {
                case DebugClient.PetriState.Paused:
                    WriteLine("{0}", Configuration.GetLocalized("The petri net's execution is paused."));
                    break;
                case DebugClient.PetriState.Pausing:
                    WriteLine("{0}", Configuration.GetLocalized("The petri net is pausing."));
                    break;
                case DebugClient.PetriState.Started:
                    WriteLine("{0}", Configuration.GetLocalized("The petri net is running."));
                    break;
                case DebugClient.PetriState.Starting:
                    WriteLine("{0}", Configuration.GetLocalized("The petri net is starting."));
                    break;
                case DebugClient.PetriState.Stopped:
                    WriteLine("{0}", Configuration.GetLocalized("The petri net is not running."));
                    break;
                case DebugClient.PetriState.Stopping:
                    WriteLine("{0}", Configuration.GetLocalized("The petri net is stopping."));
                    break;
                }
            }

            return true;
        }

        bool SearchEntities(List<string> args)
        {
            FindWhich which = 0;
            FindWhere where = 0;
            string what = null;
            bool regex = false;

            bool isArg = true;

            for(int i = 0; i < args.Count; ++i) {
                if(isArg) {
                    if(args[i] == "--state" || args[i] == "-s") {
                        which |= FindWhich.State;
                    } else if(args[i] == "--transition" || args[i] == "-t") {
                        which |= FindWhich.State;
                    } else if(args[i] == "--comment" || args[i] == "-c") {
                        which |= FindWhich.State;
                    } else if(args[i] == "--id") {
                        where |= FindWhere.ID;
                    } else if(args[i] == "--name") {
                        where |= FindWhere.Name;
                    } else if(args[i] == "--code") {
                        where |= FindWhere.Code;
                    } else if(args[i] == "--regex") {
                        regex = true;
                    } else {
                        isArg = false;
                    }
                }
                if(!isArg) {
                    if(what != null) {
                        // Excess argument
                        return false;
                    } else if(args[i] != "--") {
                        what = args[i];
                    }
                }
            }

            if(which == 0) {
                which = FindWhich.All;
            }
            if(where == 0) {
                where = FindWhere.Everywhere;
            }

            if(what == null) {
                return false;
            }

            var result = new List<Tuple<string, string, string, string>>();
            var resultSize = Tuple.Create(int.MinValue, int.MinValue, int.MinValue, int.MinValue);

            try {
                var list = Find(which, where, what, regex);

                WriteLine(Configuration.GetLocalized("{0} occurrences found.", list.Count));
                if(list.Count > 0) {
                    WriteLine();
                }

                foreach(var ee in list) {
                    if(ee is State) {
                        var e = (State)ee;
                        var tup = Tuple.Create(e.GlobalID.ToString(),
                                               Configuration.GetLocalized("State"),
                                               e.FullPath,
                                               (e is Action) ? ((Action)e).Invocation.MakeUserReadable() : "-");
                        result.Add(tup);
                        resultSize = Tuple.Create(Math.Max(resultSize.Item1, tup.Item1.Length),
                                                  Math.Max(resultSize.Item2, tup.Item2.Length),
                                                  Math.Max(resultSize.Item3, tup.Item3.Length),
                                                  Math.Max(resultSize.Item4, tup.Item4.Length));
                    } else if(ee is Transition) {
                        var e = (Transition)ee;
                        var tup = Tuple.Create(e.GlobalID.ToString(),
                                               Configuration.GetLocalized("Transition"),
                                               e.FullPath,
                                               e.Condition.MakeUserReadable());
                        result.Add(tup);
                        resultSize = Tuple.Create(Math.Max(resultSize.Item1, tup.Item1.Length),
                                                  Math.Max(resultSize.Item2, tup.Item2.Length),
                                                  Math.Max(resultSize.Item3, tup.Item3.Length),
                                                  Math.Max(resultSize.Item4, tup.Item4.Length));
                    } else if(ee is Comment) {
                        var e = (Comment)ee;
                        var tup = Tuple.Create(e.GlobalID.ToString(),
                                               Configuration.GetLocalized("Comment"),
                                               "-",
                                               e.Name);
                        result.Add(tup);
                        resultSize = Tuple.Create(Math.Max(resultSize.Item1, tup.Item1.Length),
                                                  Math.Max(resultSize.Item2, tup.Item2.Length),
                                                  Math.Max(resultSize.Item3, tup.Item3.Length),
                                                  Math.Max(resultSize.Item4, tup.Item4.Length));
                    }
                }

                // TODO: pretty table
                foreach(var tup in result) {
                    Console.WriteLine("{0}\t{1}\t{2}\t{3}",
                                  tup.Item1.PadRight(resultSize.Item1),
                                  tup.Item2.PadRight(resultSize.Item2),
                                  tup.Item3.PadRight(resultSize.Item3),
                                  tup.Item4.PadRight(resultSize.Item4));
                }
            } catch(ArgumentException exc) {
                NotifyUnrecoverableError(Configuration.GetLocalized("The given regular expression is invalid ({0})!", exc.Message));
            }


            return true;
        }

        /// <summary>
        /// Adds some breakpoints by the id of the actions.
        /// </summary>
        /// <param name="args">The command's arguments.</param>
        bool AddBreakpoints(List<string> args)
        {
            int count = 0;

            foreach(string arg in args) {
                UInt64 id;
                try {
                    id = UInt64.Parse(arg);
                } catch {
                    WriteLine("{0}", Configuration.GetLocalized("The argument '{0}' should be a valid positive integer.",
                                                                arg));
                    continue;
                }

                var action = PetriNet.EntityFromID(id);
                if(action == null) {
                    WriteLine("{0}", Configuration.GetLocalized("No entity with the specified identifier '{0}' exist in the petri net.",
                                                                id));
                } else if(!(action is Model.Action)) {
                    WriteLine("{0}", Configuration.GetLocalized("The entity with the specified identifier '{0}' is not a state.",
                                                                id));
                } else {
                    if(AddBreakpoint(action as Action)) {
                        ++count;
                    }
                }
            }

            WriteLine("{0}", Configuration.GetLocalized("{0} breakpoints have been added.", count));

            return true;
        }

        /// <summary>
        /// Removes some breakpoints by the id of their states.
        /// </summary>
        /// <param name="args">The command's arguments.</param>
        bool RemoveBreakpoints(List<string> args)
        {
            int count = 0;

            foreach(string arg in args) {
                UInt64 id;
                try {
                    id = UInt64.Parse(arg);
                } catch {
                    WriteLine("{0}", Configuration.GetLocalized("The argument '{0}' should be a valid positive integer.",
                                                                arg));
                    continue;
                }

                var action = PetriNet.EntityFromID(id);
                if(action == null) {
                    WriteLine("{0}", Configuration.GetLocalized("No entity with the specified identifier '{0}' exist in the petri net.",
                                                                id));
                } else if(!(action is Action)) {
                    WriteLine("{0}", Configuration.GetLocalized("The entity with the specified identifier '{0}' is not a state.",
                                                               id));
                } else {
                    if(RemoveBreakpoint(action as Action)) {
                        ++count;
                    }
                }
            }

            WriteLine("{0}", Configuration.GetLocalized("{0} breakpoints have been removed.", count));

            return true;
        }

        /// <summary>
        /// Clears all of the breakpoints.
        /// </summary>
        /// <param name="args">Arguments.</param>
        bool ClearBreakpoints(List<string> args)
        {
            ClearBreakpoints();
            WriteLine("{0}", Configuration.GetLocalized("Breakpoints cleared."));

            return true;
        }

        /// <summary>
        /// Prints a list of the breakpoints currently installed.
        /// </summary>
        /// <param name="args">The command's arguments.</param>
        bool ListBreakpoints(List<string> args)
        {
            if(Breakpoints.Count == 0) {
                WriteLine("{0}", Configuration.GetLocalized("No breakpoints are installed."));
            } else {
                WriteLine("{0}", Configuration.GetLocalized("The following breakpoints are installed:"));
                var list = new List<string>(Breakpoints.Select((Action a) => {
                    return string.Format(Configuration.GetLocalized("Action '{0}' with id {1}",
                                                                    a.FullPath,
                                                                    a.GlobalID));
                }));
                list.Sort();
                foreach(var s in list) {
                    WriteLine("  {0}", s);
                }
            }

            return true;
        }

        /// <summary>
        /// Prints a list of the current petri net's variables with their values.
        /// </summary>
        /// <param name="args">The command's arguments.</param>
        bool ListVariables(List<string> args)
        {
            if(Client.CurrentPetriState == DebugClient.PetriState.Paused) {
                WriteLine("{0}", Configuration.GetLocalized("Current variables' values:"));
            } else {
                WriteLine("{0}", Configuration.GetLocalized("Variables of the petri net (launch the petri net to see their values):"));
            }
            _vars = null;
            Client.RequestVariables();
            WaitFor(() => _vars != null);

            int maxLength = _vars.Aggregate(0, (length, tup) => {
                return Math.Max(length, tup.Item1.VariableFullName(tup.Item2).Length);
            });

            for(int i = 0; i < _vars.Count; ++i) {
                WriteLine("  {0} {1}",
                          _vars[i].Item1.VariableFullName(_vars[i].Item2).PadRight(maxLength + 1),
                          _vars[i].Item3);
            }

            _vars = null;

            return true;
        }

        /// <summary>
        /// Prints a list of the current petri net's parameters with their default values.
        /// </summary>
        /// <param name="args">The command's arguments.</param>
        bool ListParameters(List<string> args)
        {
            int maxLength = PetriNet.Parameters.Aggregate(0, (length, param) => {
                return Math.Max(length, param.MakeUserReadable().Length);
            });

            foreach(var param in PetriNet.Parameters) {
                WriteLine("  {0} {1}",
                          param.MakeUserReadable().PadRight(maxLength + 1),
                          PetriNet.Variables[param]);
            }

            _vars = null;

            return true;
        }
        /// <summary>
        /// List the currently active states, if any.
        /// </summary>
        /// <param name="args">The command's arguments.</param>

        bool ListActiveStates(List<string> args)
        {
            if(Client.CurrentPetriState != DebugClient.PetriState.Paused) {
                WriteLine("{0}", Configuration.GetLocalized("The petri net is not running, there is no active state."));
            } else {
                if(StatesInformation.Count == 0) {
                    // Should never happen. Just in case…
                    WriteLine("{0}", Configuration.GetLocalized("No states are active right now."));
                } else {
                    WriteLine("{0}", Configuration.GetLocalized("Currently active states:"));

                    var states = new List<State>(StatesInformation.Keys);
                    states.Sort((s1, s2) => string.Compare(s1.FullPath, s2.FullPath, StringComparison.InvariantCulture));

                    var maxLength1 = states.Aggregate(0,
                                                      (length, state) => Math.Max(length,
                                                                                  state.FullPath.Length));
                    var maxLength2 = states.Aggregate(0,
                                                      (length, state) => Math.Max(length,
                                                                                  StatesInformation[state].ToString().Length));

                    foreach(var s in states) {
                        WriteLine("  {0}{1}",
                                  s.FullPath.PadRight(maxLength1),
                                  StatesInformation[s].InvocationsCount > 1 ? string.Format(Configuration.GetLocalized(", active {0} times"),
                                                                                            StatesInformation[s].InvocationsCount).PadLeft(maxLength2) : "");
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Prints the application's version.
        /// </summary>
        /// <param name="args">The command's arguments.</param>
        bool PrintVersion(List<string> args)
        {
            WriteLine("{0}", Application.Version);
            return true;
        }

        /// <summary>
        /// Creates an external host to run the petri net, passing it the specified command-line arguments.
        /// </summary>
        /// <param name="args">The command's arguments.</param>
        bool CreateHost(List<string> args)
        {
            string name = args[0];
            string arguments = "";
            int firstArg = 1 + (args.IndexOf("--") == 1 ? 1 : 0);

            // FIXME: quote args
            arguments = string.Join(" ", args.GetRange(firstArg, args.Count - firstArg));

            name = ResolvePath(name);
            if(name.Length == 0) {
                NotifyUnrecoverableError(Configuration.GetLocalized("An executable name must be specified."));
                return false;
            }

            WriteLine("{0}", Configuration.GetLocalized("Creating host process '{0}' with arguments '{1}'…",
                                                        name,
                                                        arguments));

            Client.CreateHostAndAttach(name, arguments).Wait();

            return true;
        }

        /// <summary>
        /// Clears the debugger's commands' history.
        /// </summary>
        /// <param name="args">The command's arguments.</param>
        bool ClearHistory(List<string> args)
        {
            _inputManager.ClearHistory();
            return true;
        }

        /// <summary>
        /// Changes the current directory.
        /// </summary>
        /// <param name="args">The command's arguments.</param>
        bool ChangeDirectory(List<string> args)
        {
            string path;
            if(args.Count == 0) {
                path = System.IO.Directory.GetParent(_documentPath).FullName;
            } else {
                path = args[0];
            }

            Environment.CurrentDirectory = ResolvePath(path);

            return true;
        }

        /// <summary>
        /// Prints the current working directory.
        /// </summary>
        /// <param name="args">The command's arguments.</param>
        bool PrintDirectory(List<string> args)
        {
            WriteLine("{0}", Environment.CurrentDirectory);

            return true;
        }

        /// <summary>
        /// Prints the current profile's name.
        /// </summary>
        /// <param name="args">The command's arguments.</param>
        bool GetCurrentProfile(List<string> args)
        {
            WriteLine("{0}", _documentSettings.CurrentProfile.Name);
            return true;
        }

        /// <summary>
        /// Sets the current profile.
        /// </summary>
        /// <param name="args">The command's arguments.</param>
        bool SetCurrentProfile(List<string> args)
        {
            if(args.Count != 1) {
                return false;
            }

            var profile = _documentSettings.Profiles.Find(prof => prof.Name == args[0]);
            if(profile == null) {
                WriteLine("{0}", Configuration.GetLocalized("The given profile name does not exist."));
                return true;
            }

            if(Client.CurrentSessionState != DebugClient.SessionState.Stopped) {
                NotifyUnrecoverableError(Configuration.GetLocalized("The debugger is attached to the server."));
                if(_inputManager.PromptYesNo(Configuration.GetLocalized("Do you want to stop the session and change profile anyway?"))) {
                    Detach(new List<string>());
                }
            }
            if(Client.CurrentSessionState == DebugClient.SessionState.Stopped) {
                _documentSettings.CurrentProfile = profile;
            }

            return true;
        }

        /// <summary>
        /// Lists the available profiles.
        /// </summary>
        /// <param name="args">The command's arguments.</param>
        bool ListProfiles(List<string> args)
        {
            foreach(var profile in _documentSettings.Profiles) {
                if(profile == _documentSettings.CurrentProfile) {
                    WriteLine("* {0}", profile.Name);
                } else {
                    WriteLine("  {0}", profile.Name);
                }
            }
            return true;
        }

        /// <summary>
        /// Shows the states with their tokens.
        /// </summary>
        /// <param name="args">The command's arguments.</param>
        bool ShowTokens(List<string> args)
        {
            var states = new List<Action>(PetriNet.BuildAllEntitiesList().Where((e) => {
                return e is Action && ((Action)e).RequiredTokens > 0;
            }).Cast<Action>());
            states.Sort((s1, s2) => s1.FullPath.CompareTo(s2.FullPath));

            var tokensHeader = Configuration.GetLocalized("Tokens");
            int maxLength = tokensHeader.Length;

            var tokensAndState = states.Select((s) => {
                var currentTokens = Client.PetriAlive ? StatesInformation[s].TokensCount.ToString() : "-";
                currentTokens += "/" + s.RequiredTokens.ToString();
                maxLength = Math.Max(maxLength, currentTokens.Length);
                return Tuple.Create(currentTokens, s.FullPath);
            });

            ++maxLength;

            Console.WriteLine("{0} {1}", tokensHeader.PadRight(maxLength), Configuration.GetLocalized("State"));

            foreach(var tup in tokensAndState) {
                Console.WriteLine("{0} {1}", tup.Item1.PadRight(maxLength), tup.Item2);
            }

            return true;
        }

        /// <summary>
        /// Waits for the petri net's execution to stop or for it to be paused.
        /// </summary>
        void WaitForPausedOrStopped()
        {
            WaitFor(() => {
                if(Client.CurrentPetriState == DebugClient.PetriState.Started && _inputManager.Interrupt) {
                    Client.SetPause(true);
                    WaitFor(() => {
                        return Client.CurrentPetriState == DebugClient.PetriState.Paused || Client.CurrentPetriState == DebugClient.PetriState.Stopped;
                    });
                }
                return Client.CurrentPetriState == DebugClient.PetriState.Stopped || Client.CurrentPetriState == DebugClient.PetriState.Paused;
            });
        }

        /// <summary>
        /// Resolves the path by changing the ~ character into the user's home directory.
        /// </summary>
        /// <returns>The path.</returns>
        /// <param name="path">Path.</param>
        string ResolvePath(string path)
        {
            // Resolve the ~ character as the home directory
            string home = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
            if(path.StartsWithInv("~")) {
                if(path.Length >= 2) {
                    path = System.IO.Path.Combine(home, path.Substring(2));
                } else {
                    path = home;
                }
            }

            if(!path.StartsWithInv("\"") || !path.EndsWithInv("\"")) {
                path = path.Replace("\\ ", " ");
            }

            return path;
        }

        /// <summary>
        /// Waits for the specified predicate to become true.
        /// </summary>
        /// <param name="pred">Pred.</param>
        static void WaitFor(Func<bool> pred)
        {
            while(!pred.Invoke()) {
                Thread.Sleep(50);
            }
        }

        DebugMode _oldDebugMode;

        int _returnCode = 0;
        string _nextDocument = null;
        volatile bool _alive = true;
        volatile Tuple<string, string> _evalResult;
        volatile List<Tuple<RootPetriNet, Code.VariableExpression, string>> _vars;

        InputManager _inputManager;
        RootDebuggerAction _actions;
        string _documentPath;
        DocumentSettings _documentSettings;
    }
}
