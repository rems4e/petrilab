//
//  DebuggerAlias.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-03-11.
//

using System;
using System.Collections.Generic;

namespace Petri.Application.CLI.Debugger
{
    /// <summary>
    /// A debugger action that aliases to another one, usually to shorten an invocation, or to avoid typing some arguments.
    /// This wraps around another DebuggerAction and some potential arguments.
    /// </summary>
    public class DebuggerAlias : DebuggerAction
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Petri.Application.CLI.Debugger.DebuggerAlias"/> class.
        /// </summary>
        /// <param name="output">The object in charge of outputting text to the console.</param>
        /// <param name="invocation">The string that will invoke the alias.</param>
        /// <param name="action">The debugger action that will be invoked when the alias is called</param>
        /// <param name="description">Maps to <c>DebuggerAction.Description</c>.</param>
        /// <param name="args">The arguments the wrapped debugger action will be called with upon alias call.</param>
        public DebuggerAlias(IOutput output, string invocation,
                             DebuggerAction action,
                             string description,
                             string args) : base(output)
        {
            _action = action;
            _args = Application.SpaceSplit(args);
            _strargs = args;
            Description = description;
            Invocation = invocation;
            NeedsDocumentFirst = action.NeedsDocumentFirst;
        }

        /// <summary>
        /// Executes the specified action with the specified args.
        /// This is this method's responsibility to block if the underlying action is asynchronous but the side effects
        /// should be synchronous.
        /// </summary>
        /// <param name="args">Arguments.</param>
        public override bool Execute(List<string> args)
        {
            var actualArgs = new List<string>(_args);
            actualArgs.AddRange(args);
            return _action.Execute(actualArgs);
        }

        /// <summary>
        /// Prints the help text of the action.
        /// </summary>
        /// <param name="args">Arguments.</param>
        public override void PrintHelp(List<string> args)
        {
            var actualArgs = new List<string>(_args);
            actualArgs.AddRange(args);
            _action.PrintHelp(actualArgs);

            if(!args.Contains("--usage-only")) {
                Output.WriteLine("\n{0}", Configuration.GetLocalized("'{0}' is an alias for '{1}'.",
                                                                     Invocation,
                                                                     Aliased));
            }
        }

        /// <summary>
        /// Returns the invocation that would perform identically as this alias does, including its arguments.
        /// For instance, if an alias "hq" wraps around "help quit", then calling this method on it would return "help quit".
        /// </summary>
        /// <value>The aliased.</value>
        public string Aliased {
            get {
                return _action.FullInvocation + (_strargs.Length > 0 ? " " : "") + _strargs;
            }
        }

        /// <summary>
        /// Returns a list of autocompletion values for the current user input. The first item of the tuple is the base string on which the autocompletion was based.
        /// </summary>
        /// <param name="args">The string on which the autocompletion is based.</param>
        public override Tuple<string, List<string>> Autocomplete(string args)
        {
            return _action.Autocomplete(args);
        }

        /// <summary>
        /// Gets the values available for completion.
        /// </summary>
        /// <returns>The completion values.</returns>
        /// <param name="prefix">Prefix.</param>
        public override IEnumerable<string> GetCompletionValues(string prefix)
        {
            return _action.GetCompletionValues(prefix);
        }

        /// <summary>
        /// Gets the index of the first parameter that is in raw form, or <c>int.MaxValue</c> if none exist.
        /// </summary>
        /// <value>The first raw input.</value>
        public override int FirstRawInput {
            get {
                return _action.FirstRawInput;
            }
        }

        DebuggerAction _action;
        List<string> _args;
        string _strargs;
    }
}

