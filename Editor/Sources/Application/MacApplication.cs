//
//  MacApplication.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2014-12-09.
//

using System;
using System.Collections.Generic;

namespace Petri.Application
{
    /// <summary>
    /// The main class for the OS X wrapper of the petri net editor.
    /// </summary>
    public class MacApplication
    {
        /// <summary>
        /// The entry point of the program, where the program control starts and ends.
        /// Delegates to the <see cref="T:Petri.Application.Application"/> class.
        /// </summary>
        /// <param name="args">The command-line arguments.</param>
        /// <returns>The exit code that is given to the operating system after the program ends.</returns>
        public static int Main(string[] args)
        {
            int index = Array.FindIndex(args, (s) => {
                return System.Text.RegularExpressions.Regex.IsMatch(s, "-psn_0_\\d+");
            });

            if(index != -1) {
                var list = new List<string>(args);
                list.RemoveAt(index);
                args = list.ToArray();
            }
            return Application.Main(args);
        }
    }
}
