﻿//
//  TestFindEntitiesText.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2014-12-09.
//

using NUnit.Framework;

namespace Petri.Test.Application.Controller
{
    [TestFixture]
    public class TestFindEntitiesText : TestFindEntitiesBase
    {
        public TestFindEntitiesText() : base(false)
        {
        }
    }
}