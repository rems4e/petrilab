﻿//
//  TestFindEntitiesRegex.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2017-04-02.
//

using NUnit.Framework;

using C = Petri.Application.Controller;
using M = Petri.Model;

namespace Petri.Test.Application.Controller
{
    [TestFixture]
    public class TestFindEntitiesRegex : TestFindEntitiesBase
    {
        public TestFindEntitiesRegex() : base(true)
        {
        }

        [Test]
        public void TestCountFindAllRegex1()
        {
            var which = C.FindWhich.All;
            var where = C.FindWhere.Everywhere;

            var result = _controller.Find(which, where, ".*", _regex);

            // RootPetriNet + ExitPoint + actions + transitions + comments 
            Assert.AreEqual(2 + (_actionsCount + 1) * 3 + _transitionsCount * 3 + _commentsCount * 2, result.Count);
        }

        [Test]
        public void TestCountFindAllRegex2()
        {
            var which = C.FindWhich.All;
            var where = C.FindWhere.ID;

            var result = _controller.Find(which, where, "[0-9]+", _regex);

            // RootPetriNet + ExitPoint + actions + transitions + comments 
            Assert.AreEqual(1 + (_actionsCount + 1) + _transitionsCount + _commentsCount, result.Count);
        }

        [TestCase("[a-z")]
        [TestCase("{3}")]
        [TestCase("*")]
        public void TestInvalidRegex(string regex)
        {
            Assert.Throws<System.ArgumentException>(() => {
                _controller.Find(C.FindWhich.All, C.FindWhere.Everywhere, regex, true);
            });
        }

        [Test]
        public void TestFindActionsByNameRegex()
        {
            var which = C.FindWhich.State;
            var where = C.FindWhere.Name;

            var result = _controller.Find(which, where, "Test \\w+ action[0-9]+", _regex);

            Assert.AreEqual(_actionsCount, result.Count);
            CollectionAssert.AllItemsAreInstancesOfType(result, typeof(M.Action));
        }

        [Test]
        public void TestFindActionsByIDRegex()
        {
            var which = C.FindWhich.State;
            var where = C.FindWhere.ID;

            var result = _controller.Find(which, where, "1[0-9]{2}", _regex);

            Assert.AreEqual(_actionsCount, result.Count);
            CollectionAssert.AllItemsAreInstancesOfType(result, typeof(M.Action));
        }

        [Test]
        public void TestFindActionsByCodeRegex()
        {
            var which = C.FindWhich.State;
            var where = C.FindWhere.Code;

            var result = _controller.Find(which, where, "invocation.*", _regex);

            Assert.AreEqual(_actionsCount, result.Count);
            CollectionAssert.AllItemsAreInstancesOfType(result, typeof(M.Action));
        }

        [Test]
        public void TestFindTransitionsByNameRegex()
        {
            var which = C.FindWhich.Transition;
            var where = C.FindWhere.Name;

            var result = _controller.Find(which, where, "\\w+ text transition[0-9]+", _regex);

            Assert.AreEqual(_transitionsCount, result.Count);
            CollectionAssert.AllItemsAreInstancesOfType(result, typeof(M.Transition));
        }

        [Test]
        public void TestFindTransitionsByIDRegex()
        {
            var which = C.FindWhich.Transition;
            var where = C.FindWhere.ID;

            var result = _controller.Find(which, where, "2[0-9]{2}", _regex);

            Assert.AreEqual(_transitionsCount, result.Count);
            CollectionAssert.AllItemsAreInstancesOfType(result, typeof(M.Transition));
        }

        [Test]
        public void TestFindTransitionsByCodeRegex()
        {
            var which = C.FindWhich.Transition;
            var where = C.FindWhere.Code;

            var result = _controller.Find(which, where, "condition.*", _regex);

            Assert.AreEqual(_transitionsCount, result.Count);
            CollectionAssert.AllItemsAreInstancesOfType(result, typeof(M.Transition));
        }

        [Test]
        public void TestFindCommentsByNameRegex()
        {
            var which = C.FindWhich.Comment;
            var where = C.FindWhere.Name;

            var result = _controller.Find(which, where, "\\w+ text comment[0-9]+", _regex);

            Assert.AreEqual(_commentsCount, result.Count);
            CollectionAssert.AllItemsAreInstancesOfType(result, typeof(M.Comment));
        }

        [Test]
        public void TestFindCommentsByIDRegex()
        {
            var which = C.FindWhich.Comment;
            var where = C.FindWhere.ID;

            var result = _controller.Find(which, where, "3[0-9]{2}", _regex);

            Assert.AreEqual(_commentsCount, result.Count);
            CollectionAssert.AllItemsAreInstancesOfType(result, typeof(M.Comment));
        }

        [Test]
        public void TestDoesNotFindCommentsByCodeRegex()
        {
            var result = _controller.Find(C.FindWhich.Comment, C.FindWhere.Code, ".*", true);

            Assert.AreEqual(0, result.Count);
        }
    }
}