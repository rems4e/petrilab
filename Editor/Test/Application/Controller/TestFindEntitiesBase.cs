﻿//
//  TestFindEntitiesBase.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2017-04-02.
//

using NUnit.Framework;

using C = Petri.Application.Controller;
using M = Petri.Model;

namespace Petri.Test.Application.Controller
{
    public abstract class TestFindEntitiesBase
    {
        protected C _controller;
        protected M.PetriNet _petriNet;
        protected int _actionsCount;
        protected int _transitionsCount;
        protected int _commentsCount;
        protected bool _regex;

        protected TestFindEntitiesBase(bool regex)
        {
            _regex = regex;

            var lang = Petri.Code.Language.Cpp;

            var doc = new Debugger.DebuggableTestDocument(lang, new DelegateOutput());
            _controller = doc.DebugController;

            var pn = doc.PetriNet;

            pn.ExitPoint.Name = "Test text exitpoint";

            var a1 = new M.Action(doc.EntityFactory, pn, false);
            a1.Name = "Test text action1";
            a1.Invocation = new Petri.Code.WrapperFunctionInvocation(doc.Settings.Enum.Type,
                                                                     Petri.Code.Expression.CreateFromString("\"Test text invocation code1\"", lang));
            a1.ID = 101;
            pn.AddState(a1);
            ++_actionsCount;

            var a2 = new M.Action(doc.EntityFactory, pn, false);
            a2.Name = "Test text action2";
            a2.Invocation = new Petri.Code.WrapperFunctionInvocation(doc.Settings.Enum.Type,
                                                                     Petri.Code.Expression.CreateFromString("\"Test text invocation code2\"", lang));
            a2.ID = 102;
            pn.AddState(a2);
            ++_actionsCount;

            var a3 = new M.Action(doc.EntityFactory, pn, false);
            a3.Name = "Test text action3";
            a3.Invocation = new Petri.Code.WrapperFunctionInvocation(doc.Settings.Enum.Type,
                                                                     Petri.Code.Expression.CreateFromString("\"Test text invocation code3\"", lang));
            a3.ID = 103;
            pn.AddState(a3);
            ++_actionsCount;

            var t1 = new M.Transition(doc.EntityFactory, pn, a1, a2);
            t1.Name = "Test text transition1";
            t1.Condition = new Petri.Code.WrapperFunctionInvocation(new Petri.Code.Type(lang, "bool"),
                                                                    Petri.Code.Expression.CreateFromString("\"Test text condition code1\"", lang));
            t1.ID = 201;
            pn.AddTransition(t1);
            t1.Before.AddTransitionAfter(t1);
            t1.After.AddTransitionBefore(t1);
            ++_transitionsCount;

            var t2 = new M.Transition(doc.EntityFactory, pn, a3, a3);
            t2.Name = "Test text transition2";
            t2.Condition = new Petri.Code.WrapperFunctionInvocation(new Petri.Code.Type(lang, "bool"),
                                                                    Petri.Code.Expression.CreateFromString("\"Test text condition code2\"", lang));
            t2.ID = 202;
            pn.AddTransition(t2);
            t2.Before.AddTransitionAfter(t2);
            t2.After.AddTransitionBefore(t2);
            ++_transitionsCount;

            var c1 = new M.Comment(doc.EntityFactory, pn, "");
            c1.Name = "Test text comment1";
            c1.ID = 301;
            pn.AddComment(c1);
            ++_commentsCount;

            _petriNet = pn;
        }

        [Test]
        public void TestWellFormed()
        {
            Model.ModelUtility.AssertWellFormed(_petriNet);
        }

        [Test]
        public void TestEquivalentAllWhich()
        {
            var which1 = C.FindWhich.All;
            var where1 = C.FindWhere.Everywhere;
            var result1 = _controller.Find(which1, where1, "", _regex);

            var which2 = C.FindWhich.State | C.FindWhich.Transition | C.FindWhich.Comment;
            var where2 = C.FindWhere.Everywhere;

            var result2 = _controller.Find(which2, where2, "", _regex);

            CollectionAssert.AreEquivalent(result1, result2);
            CollectionAssert.IsNotEmpty(result1);
        }

        [Test]
        public void TestEquivalentAllWhere()
        {
            var which1 = C.FindWhich.All;
            var where1 = C.FindWhere.Everywhere;
            var result1 = _controller.Find(which1, where1, "", _regex);

            var which2 = C.FindWhich.All;
            var where2 = C.FindWhere.ID | C.FindWhere.Name | C.FindWhere.Code;

            var result2 = _controller.Find(which2, where2, "", _regex);

            CollectionAssert.AreEquivalent(result1, result2);
            CollectionAssert.IsNotEmpty(result1);
        }

        [Test]
        public void TestEquivalentAllWhichWhere()
        {
            var which1 = C.FindWhich.All;
            var where1 = C.FindWhere.Everywhere;
            var result1 = _controller.Find(which1, where1, "", _regex);

            var which2 = C.FindWhich.State | C.FindWhich.Transition | C.FindWhich.Comment;
            var where2 = C.FindWhere.ID | C.FindWhere.Name | C.FindWhere.Code;

            var result2 = _controller.Find(which2, where2, "", _regex);

            CollectionAssert.AreEquivalent(result1, result2);
            CollectionAssert.IsNotEmpty(result1);
        }

        [Test]
        public void TestCountFindAll()
        {
            var which = C.FindWhich.All;
            var where = C.FindWhere.Everywhere;

            var result = _controller.Find(which, where, "", _regex);

            // RootPetriNet + ExitPoint + actions + transitions + comments 
            Assert.AreEqual(2 + (_actionsCount + 1) * 3 + _transitionsCount * 3 + _commentsCount * 2, result.Count);
        }

        [Test]
        public void TestFindActionsByName([Values(C.FindWhich.All, C.FindWhich.State)] C.FindWhich which,
                                          [Values(C.FindWhere.Everywhere, C.FindWhere.Name)] C.FindWhere where)
        {
            var result = _controller.Find(which, where, "action", _regex);

            Assert.AreEqual(_actionsCount, result.Count);
            CollectionAssert.AllItemsAreInstancesOfType(result, typeof(M.Action));
            CollectionAssert.AllItemsAreUnique(result);
        }

        [Test]
        public void TestFindActionsByCode([Values(C.FindWhich.All, C.FindWhich.State)] C.FindWhich which,
                                          [Values(C.FindWhere.Everywhere, C.FindWhere.Code)] C.FindWhere where)
        {
            var result = _controller.Find(which, where, "invocation code", _regex);

            Assert.AreEqual(_actionsCount, result.Count);
            CollectionAssert.AllItemsAreInstancesOfType(result, typeof(M.Action));
            CollectionAssert.AllItemsAreUnique(result);
        }

        [Test]
        public void TestFindActionsByID([Values(C.FindWhich.All, C.FindWhich.State)] C.FindWhich which,
                                        [Values(C.FindWhere.Everywhere, C.FindWhere.ID)] C.FindWhere where)
        {
            var result = _controller.Find(which, where, "10", _regex);

            Assert.AreEqual(_actionsCount, result.Count);
            CollectionAssert.AllItemsAreInstancesOfType(result, typeof(M.Action));
            CollectionAssert.AllItemsAreUnique(result);
        }

        [Test]
        public void TestFindTransitionsByName([Values(C.FindWhich.All, C.FindWhich.Transition)] C.FindWhich which,
                                              [Values(C.FindWhere.Everywhere, C.FindWhere.Name)] C.FindWhere where)
        {
            var result = _controller.Find(which, where, "transition", _regex);

            Assert.AreEqual(_transitionsCount, result.Count);
            CollectionAssert.AllItemsAreInstancesOfType(result, typeof(M.Transition));
            CollectionAssert.AllItemsAreUnique(result);
        }

        [Test]
        public void TestFindTransitionsByID([Values(C.FindWhich.All, C.FindWhich.Transition)] C.FindWhich which,
                                            [Values(C.FindWhere.Everywhere, C.FindWhere.ID)] C.FindWhere where)
        {
            var result = _controller.Find(which, where, "20", _regex);

            Assert.AreEqual(_transitionsCount, result.Count);
            CollectionAssert.AllItemsAreInstancesOfType(result, typeof(M.Transition));
            CollectionAssert.AllItemsAreUnique(result);
        }

        [Test]
        public void TestFindTransitionByCode([Values(C.FindWhich.All, C.FindWhich.Transition)] C.FindWhich which,
                                             [Values(C.FindWhere.Everywhere, C.FindWhere.Code)] C.FindWhere where)
        {
            var result = _controller.Find(which, where, "condition code", _regex);

            Assert.AreEqual(_transitionsCount, result.Count);
            CollectionAssert.AllItemsAreInstancesOfType(result, typeof(M.Transition));
            CollectionAssert.AllItemsAreUnique(result);
        }

        [Test]
        public void TestFindCommentsByName([Values(C.FindWhich.All, C.FindWhich.Comment)] C.FindWhich which,
                                           [Values(C.FindWhere.Everywhere, C.FindWhere.Name)] C.FindWhere where)
        {
            var result = _controller.Find(which, where, "comment", _regex);

            Assert.AreEqual(_commentsCount, result.Count);
            CollectionAssert.AllItemsAreInstancesOfType(result, typeof(M.Comment));
            CollectionAssert.AllItemsAreUnique(result);
        }

        [Test]
        public void TestDoesNotFindCommentsByCode()
        {
            var result = _controller.Find(C.FindWhich.Comment, C.FindWhere.Code, "", _regex);

            Assert.AreEqual(0, result.Count);
        }

        [Test]
        public void TestFindCommentsByID([Values(C.FindWhich.All, C.FindWhich.Comment)] C.FindWhich which,
                                         [Values(C.FindWhere.Everywhere, C.FindWhere.ID)] C.FindWhere where)
        {
            var result = _controller.Find(which, where, "30", _regex);

            Assert.AreEqual(_commentsCount, result.Count);
            CollectionAssert.AllItemsAreInstancesOfType(result, typeof(M.Comment));
            CollectionAssert.AllItemsAreUnique(result);
        }

        [Test]
        public void TestDoesNotFindActionWrongText()
        {
            var result = _controller.Find(C.FindWhich.State, C.FindWhere.Everywhere, "something", _regex);
            CollectionAssert.IsEmpty(result);
        }
        [Test]
        public void TestDoesNotFindTransitionWrongText()
        {
            var result = _controller.Find(C.FindWhich.Transition, C.FindWhere.Everywhere, "something", _regex);
            CollectionAssert.IsEmpty(result);
        }
        [Test]
        public void TestDoesNotFindCommentWrongText()
        {
            var result = _controller.Find(C.FindWhich.Comment, C.FindWhere.Everywhere, "something", _regex);
            CollectionAssert.IsEmpty(result);
        }

        [Test]
        public void TestDoesNotFindAnythingWrongText()
        {
            var result = _controller.Find(C.FindWhich.All, C.FindWhere.Everywhere, "something", _regex);
            CollectionAssert.IsEmpty(result);
        }
    }
}