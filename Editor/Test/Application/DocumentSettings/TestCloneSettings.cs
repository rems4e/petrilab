﻿//
//  TestCloneSettings.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-02-22.
//

using System;
using System.Linq;

using NUnit.Framework;

using HeadlessDocument = Petri.Application.CLI.HeadlessDocument;

namespace Petri.Test.Application.DocumentSettings
{
    [TestFixture]
    public class TestCloneSettings
    {
        HeadlessDocument _document;

        [OneTimeSetUp]
        public void FixtureSetUp()
        {
            _document = new HeadlessDocument(CodeUtility.RandomLanguage());
        }

        static void AssertEquivalent(Petri.Application.DocumentSettings s1, Petri.Application.DocumentSettings s2)
        {
            Assert.NotNull(s1);
            Assert.NotNull(s2);

            TestingUtility.AssertInstancePropertiesEqual(
                s1,
                s2,
                "UniqueDocumentsPaths",
                "AllFunctions",
                "Profiles",
                "CurrentProfile",
                "CurrentProfileWrapper",
                "MatchedProfile",
                "StandardSourcePath",
                "StandardLibPath",
                "StandardIntermediatePath",
                "Headers",
                "PreprocessorMacros",
                "XMLBackup"
            );

            Assert.AreEqual(s1.Profiles.Count, s2.Profiles.Count);
            for(int i = 0; i < s1.Profiles.Count; ++i) {
                AssertEquivalent(s1.Profiles[i], s2.Profiles[i]);
            }
            AssertEquivalent(s1.CurrentProfile, s2.CurrentProfile);
            CollectionAssert.AreEquivalent(s1.Headers, s2.Headers);
            CollectionAssert.AreEquivalent(s1.PreprocessorMacros, s2.PreprocessorMacros);
        }

        static void AssertEquivalent(Petri.Application.DocumentSettingsProfile p1, Petri.Application.DocumentSettingsProfile p2)
        {
            Assert.NotNull(p1);
            Assert.NotNull(p2);

            TestingUtility.AssertInstancePropertiesEqual(
                p1,
                p2,
                "Profiles",
                "IncludePaths",
                "LibPaths",
                "Libs",
                "CompilerInfo"
            );
            CollectionAssert.AreEquivalent(p1.CompilerInfo.Compiler, p2.CompilerInfo.Compiler);
            CollectionAssert.AreEquivalent(p1.CompilerInfo.CustomCompilerFlags, p2.CompilerInfo.CustomCompilerFlags);
            CollectionAssert.AreEquivalent(p1.CompilerInfo.CustomLinkerFlags, p2.CompilerInfo.CustomLinkerFlags);
            CollectionAssert.AreEquivalent(p1.CompilerInfo.IncludePaths, p2.CompilerInfo.IncludePaths);
            CollectionAssert.AreEquivalent(p1.CompilerInfo.LibPaths, p2.CompilerInfo.LibPaths);
            CollectionAssert.AreEquivalent(p1.CompilerInfo.Libs, p2.CompilerInfo.Libs);
        }

        [Test]
        public void TestDefaultSettings()
        {
            // GIVEN a fresh document
            // WHEN we get the default settings for this document
            var defaultSettings = Petri.Application.DocumentSettings.GetDefaultSettings(_document, _document.Settings.Language);

            // THEN the settings of the document are the same as the default settings.
            AssertEquivalent(_document.Settings, defaultSettings);
        }

        Petri.Application.DocumentSettings GetRandomSettings()
        {
            var settings = Petri.Application.DocumentSettings.GetDefaultSettings(_document, CodeUtility.RandomLanguage());

            var random = new Random();
            settings.CurrentProfile.CompilerInfo.Compiler = CodeUtility.RandomIdentifier();
            int flagsCount = random.Next(10);
            for(int i = 0; i < flagsCount; ++i) {
                settings.CurrentProfile.CompilerInfo.CustomCompilerFlags.Add("-" + CodeUtility.RandomIdentifier());
                settings.CurrentProfile.CompilerInfo.CustomLinkerFlags.Add("-" + CodeUtility.RandomIdentifier());
            }

            bool defaultEnum = random.Next(2) != 0;
            if(defaultEnum) {
                settings.Enum = Petri.Application.DocumentSettings.GetDefaultEnumForLanguage(settings.Language);
            } else {
                int enumCount = random.Next(1, 10) + 1;
                var enumValues = string.Join(
                    ",",
                    Enumerable.Repeat((object)null, enumCount).Select(o => CodeUtility.RandomIdentifier())
                );

                settings.Enum = new Petri.Code.Enum(settings.Language, enumValues);
            }

            settings.CurrentProfile.Hostname = "localhost";
            settings.CurrentProfile.DebugPort = (UInt16)random.Next(65536);

            settings.Name = CodeUtility.RandomIdentifier();

            settings.CurrentProfile.DebugMode = (Petri.Application.DebugMode)random.Next(3);

            settings.CurrentProfile.RelativeSourceOutputPath = TestingUtility.RandomRelativePath();
            settings.CurrentProfile.RelativeLibOutputPath = TestingUtility.RandomRelativePath();

            int includePathsCount = random.Next(10);
            for(int i = 0; i < includePathsCount; ++i) {
                settings.CurrentProfile.CompilerInfo.IncludePaths.Add(Tuple.Create(TestingUtility.RandomRelativePath(), random.Next(2) == 0));
            }

            int libPathsCount = random.Next(10);
            for(int i = 0; i < libPathsCount; ++i) {
                settings.CurrentProfile.CompilerInfo.LibPaths.Add(Tuple.Create(TestingUtility.RandomRelativePath(), random.Next(2) == 0));
            }

            int libsCount = random.Next(10);
            for(int i = 0; i < libsCount; ++i) {
                settings.CurrentProfile.CompilerInfo.Libs.Add(CodeUtility.RandomIdentifier());
            }

            return settings;
        }

        [Test, Repeat(10)]
        public void TestClone()
        {
            // GIVEN a fresh document and settings for this document with random values
            var randomSettings = GetRandomSettings();

            // WHEN we clone the document's settings
            var cloned = randomSettings.Clone();

            // THEN the cloned settings are the same as the document's.
            AssertEquivalent(randomSettings, cloned);
        }
    }
}

