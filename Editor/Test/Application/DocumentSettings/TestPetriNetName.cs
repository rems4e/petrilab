﻿//
//  TestPetriNetName.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2017-06-12.
//

using System.Text.RegularExpressions;

using NUnit.Framework;

using HeadlessDocument = Petri.Application.CLI.HeadlessDocument;

namespace Petri.Test.Application.DocumentSettings
{
    [TestFixture]
    public class TestPetriNetName
    {
        HeadlessDocument _document;

        [OneTimeSetUp]
        public void FixtureSetUp()
        {
            _document = new HeadlessDocument(CodeUtility.RandomLanguage());
        }

        [Test, Repeat(100)]
        public void RandomNameIsMadeValid()
        {
            // GIVEN a random string
            var val = CodeUtility.RandomLiteral(CodeUtility.LiteralType.String, Petri.Code.Language.C).Item1;
            val = CodeUtility.LiteralValue(System.Tuple.Create(val, CodeUtility.LiteralType.String));

            // WHEN we try to set the document's name with it
            _document.Settings.Name = val;

            // THEN the resulting name is a valid conde identifier
            Regex name = new Regex(Petri.Code.Parser.GetNamePattern(true));
            Match nameMatch = name.Match(_document.Settings.Name);
            Assert.True(nameMatch.Success);
        }

        [TestCase("éà@&~./-")]
        [TestCase(";")]
        [TestCase(" ")]
        public void TestAllInvalidChars(string val)
        {
            // GIVEN a string containing only invalid characters

            // WHEN we try to set the document's name with it
            _document.Settings.Name = val;

            // THEN the resulting name is a default value
            Assert.AreEqual("MyPetriNet", _document.Settings.Name);
        }

        [Test]
        public void TestEmpty()
        {
            // GIVEN an empty string
            string val = "";

            // WHEN we try to set the document's name with it
            _document.Settings.Name = val;

            // THEN the resulting name is a default value
            Assert.AreEqual("MyPetriNet", _document.Settings.Name);
        }

        [Test, Repeat(100)]
        public void RandomIdentifier()
        {
            // GIVEN a random identifier
            var val = CodeUtility.RandomIdentifier();

            // WHEN we try to set the document's name with it
            _document.Settings.Name = val;

            // THEN the resulting name is the same as the input
            Assert.AreEqual(val, _document.Settings.Name);
        }

        [Test]
        public void TestSpace()
        {
            // GIVEN a string with space
            var val = "Sans titre 1";

            // WHEN we try to set the document's name with it
            _document.Settings.Name = val;

            // THEN the resulting name is the same as the input
            Assert.AreEqual("Sanstitre1", _document.Settings.Name);
        }

        [Test, Repeat(20)]
        public void RandomIdentifierStartingWithDigit()
        {
            // GIVEN a random identifier starting with a digit
            var val = TestingUtility.RandomBetween(0, 9).ToString() + CodeUtility.RandomIdentifier();

            // WHEN we try to set the document's name with it
            _document.Settings.Name = val;

            // THEN the resulting name is the same as the input, but starts with an underscores
            Assert.AreEqual("_" + val, _document.Settings.Name);
        }
    }
}
