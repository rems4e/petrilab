﻿//
//  TestInvocation.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2015-12-30.
//

using NUnit.Framework;

namespace Petri.Test.Application
{
    [TestFixture]
    public class TestInvocation
    {
        string _help;

        [OneTimeSetUp]
        public void SetUp()
        {
            string help_stderr;
            TestingUtility.InvokeAndRedirectOutput(() => Petri.Application.CLI.CLIApplication.PrintHelp(new Petri.Application.ConsoleOutput(false)),
                                                    out _help,
                                                    out help_stderr);
        }

        [TestCase("--help")]
        [TestCase("-h")]
        public void TestHelp(string arg)
        {
            // GIVEN launch arguments requesting help
            string[] args = { arg };
            string stdout, stderr;

            // WHEN the invocation is made
            int result = CompilerUtility.InvokeCompiler(args, out stdout, out stderr);

            // THEN no error is returned and the expected string is output
            Assert.AreEqual(0, result);
            Assert.AreEqual(_help, stdout);
            StringAssert.StartsWith(Petri.Application.CLI.CLIApplication.UsageString, _help);
            Assert.IsEmpty(stderr);
        }

        [Test]
        public void TestGenerationWithoutDocument()
        {
            // GIVEN launch arguments requesting the code generation of a not provided document
            string[] args = { "--generate" };
            string stdout, stderr;

            // WHEN the invocation is made
            // THEN an usage exception is thrown
            Assert.Throws<Petri.Application.CLI.UsageException>(() => {
                CompilerUtility.InvokeCompiler(args, out stdout, out stderr);
            });
        }

        [Test]
        public void TestCompilationWithoutDocument()
        {
            // GIVEN launch arguments requesting the compilation of a not provided document
            string[] args = { "--compile" };
            string stdout, stderr;

            // WHEN the invocation is made
            // THEN an usage exception is thrown
            Assert.Throws<Petri.Application.CLI.UsageException>(() => {
                CompilerUtility.InvokeCompiler(args, out stdout, out stderr);
            });
        }

        [Test]
        public void TestGenerationAndCompilationWithoutDocument()
        {
            // GIVEN launch arguments requesting the code generation and compilation of a not provided document
            string[] args = { "--generate", "--compile" };
            string stdout, stderr;

            // WHEN the invocation is made
            // THEN an usage exception is thrown
            Assert.Throws<Petri.Application.CLI.UsageException>(() => {
                CompilerUtility.InvokeCompiler(args, out stdout, out stderr);
            });
        }

        [Test]
        public void TestGenerationWithInvalidDocument()
        {
            // GIVEN launch arguments requesting the code generation with an invalid document path
            string[] args = { "--generate", "/" };
            string stdout, stderr;

            // WHEN the invocation is made
            // THEN a document load exception is thrown
            Assert.Throws<Petri.Application.CLI.DocumentLoadException>(() => {
                CompilerUtility.InvokeCompiler(args, out stdout, out stderr);
            });
        }

        [Test]
        public void TestCompilationWithInvalidDocument()
        {
            // GIVEN launch arguments requesting the code generation with an invalid document path
            string[] args = { "--compile", "/" };
            string stdout, stderr;

            // WHEN the invocation is made
            // THEN a document load exception is thrown
            Assert.Throws<Petri.Application.CLI.DocumentLoadException>(() => {
                CompilerUtility.InvokeCompiler(args, out stdout, out stderr);
            });
        }

        [Test]
        public void TestGenerationAndCompilationWithInvalidDocument()
        {
            // GIVEN launch arguments requesting the code generation with an invalid document path
            string[] args = { "--generate", "--compile", "/" };
            string stdout, stderr;

            // WHEN the invocation is made
            // THEN a document load exception is thrown
            Assert.Throws<Petri.Application.CLI.DocumentLoadException>(() => {
                CompilerUtility.InvokeCompiler(args, out stdout, out stderr);
            });
        }
    }
}
