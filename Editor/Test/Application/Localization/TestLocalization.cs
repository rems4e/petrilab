﻿//
//  TestLocalization.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-07-17.
//

using System.Collections.Generic;
using System.Linq;
using System.Reflection;

using NUnit.Framework;

using Petri.Application;

namespace Petri.Test.Application
{
    [TestFixture]
    public class TestLocalization
    {
        Dictionary<string, Dictionary<string, string>> _langList = new Dictionary<string, Dictionary<string, string>>();
        Dictionary<string, string> _output = new Dictionary<string, string>();

        [OneTimeSetUp]
        public void FixtureSetUp()
        {
            var assembly = Assembly.GetAssembly(typeof(Petri.Application.Application));

            var resources = assembly.GetManifestResourceNames();
            var langResources = from r in resources where r.EndsWithInv(".lang") select r;

            foreach(var l in langResources) {
                string stdout, stderr;
                var localizations = TestingUtility.InvokeAndRedirectOutput(() => {
                    return Petri.Application.Configuration.GetLocalizationsForLanguage(l);
                }, out stdout, out stderr);

                _langList.Add(l, localizations);
                _output[l] = stdout + (stdout == "" ? "" : "\n") + stderr;
            }
        }

        [Test]
        public void TestHasLanguages()
        {
            Assert.That(_langList, Is.Not.Empty);
        }

        [Test]
        public void TestHasEnglish()
        {
            Assert.That(_langList.Keys, Does.Contain("en.lang"));
        }

        [Test]
        public void TestHasFrench()
        {
            Assert.That(_langList.Keys, Does.Contain("fr.lang"));
        }

        [Test]
        public void TestNoLocalizationAvailable()
        {
            string stdout, stderr;

            // GIVEN a non existent localization key
            var key = "hfuihiruheguihuihgheu";

            // WHEN we retrieve the localization of a nonexistent key
            var loc = TestingUtility.InvokeAndRedirectOutput(() => {
                return Petri.Application.Configuration.GetLocalized(key);
            }, out stdout, out stderr);

            // THEN the key is returned and an error message is output
            Assert.That(loc, Is.EqualTo(key));
            Assert.That(stderr, Is.EqualTo(string.Format(Petri.Application.Configuration.NoLocalizationFormat, Petri.Application.Configuration.Language, key) + "\n"));
            Assert.That(stdout, Is.EqualTo(""));
        }

        [Test]
        public void TestNoEmptyLocalizationKey()
        {
            foreach(var l in _langList) {
                Assert.That(l.Value.Keys, Does.Not.Contain(""));
            }
        }

        [Test]
        public void TestNoEmptyLocalizationValue()
        {
            foreach(var l in _langList) {
                Assert.That(l.Value.Keys, Does.Not.Contain(null));
                Assert.That(l.Value.Keys, Does.Not.Contain(""));
            }
        }


        [Test]
        public void TestAllLanguagesHaveSameLocalizationsCount()
        {
            var count = _langList["en.lang"].Count;
            foreach(var l in _langList) {
                Assert.That(l.Value, Has.Count.EqualTo(count));
            }
        }

        [Test]
        public void TestLocalizationKeys()
        {
            var keys = _langList["en.lang"].Keys;

            foreach(var l in _langList) {
                foreach(var k in keys) {
                    Assert.That(l.Value.Keys, Has.Exactly(1).EqualTo(k));
                }
            }
        }

        [Test]
        public void TestAllLanguagesHaveSameKeys()
        {
            // GIVEN the English localization
            var keys = _langList["en.lang"].Keys;

            // WHEN we retrieve the others localizations
            foreach(var l in _langList) {
                // THEN they have the same localization keys
                Assert.That(l.Value.Keys, Is.EquivalentTo(keys));
            }
        }

        /// <summary>
        /// Tries to retrieve all of the GetLocalized(…) calls in all the source files in Source.
        /// </summary>
        /// <returns>The localization instances.</returns>
        HashSet<string> GetLocalizationInstances()
        {
            // The check as currently installed depends on a sane scripting environment being available.
            if(Petri.Application.Configuration.RunningPlatform != Platform.Linux && Petri.Application.Configuration.RunningPlatform != Platform.Mac) {
                Assert.Inconclusive();
            }

            var process = Petri.Application.Application.CreateProcess();

            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.FileName = "../get_localizations.sh";
            process.StartInfo.WorkingDirectory = "../../Sources";

            var stdout = new System.Text.StringBuilder();

            process.OutputDataReceived += (sender, ev) => {
                if(ev.Data != null) {
                    stdout.AppendLine(ev.Data);
                }
            };

            process.Start();
            process.BeginOutputReadLine();
            process.WaitForExit();

            var array = stdout.ToString().Split(new string[] { "\n@@@@\n" }, System.StringSplitOptions.None);

            // We check that we have only one empty string, and that it is the result of the stray separator at the end of the output.
            Assert.That(array, Has.Exactly(1).EqualTo(""));
            Assert.That(array[array.Length - 1], Is.EqualTo(""));

            var instances = new HashSet<string>(array);
            instances.Remove("");

            return instances;
        }

        [Test]
        public void TestRightLocalizationsCountInSourceCode()
        {
            // WHEN we get all the GetLocalized count in the project's source files
            var instances = GetLocalizationInstances();

            foreach(var l in _langList) {
                // THEN for all the languages, we have the same available localizations keys as we have GetLocalized calls with different key.
                Assert.That(l.Value.Keys, Has.Count.EqualTo(instances.Count));
            }
        }

        [Test]
        public void TestHaveAllNeededLocalizations()
        {
            // WHEN we get all the GetLocalized count in the project's source files
            foreach(var key in GetLocalizationInstances()) {
                foreach(var l in _langList) {
                    // THEN for all the languages, we have all the localizations keys that are requested by the number of different keys GetLocalized is called with.
                    Assert.That(l.Value.Keys, Does.Contain(key.Replace("\\n", "\n").Replace("\\\"", "\"")));
                }
            }
        }

        [Test]
        public void TestNoUnusedLocalizations()
        {
            // WHEN we get all the GetLocalized count in the project's source files
            var instances = GetLocalizationInstances();

            foreach(var l in _langList) {
                foreach(var key in l.Value.Keys) {
                    // THEN for all the languages, each available localization is required by a GetLocalized call.
                    Assert.That(instances, Does.Contain(key.Replace("\n", "\\n").Replace("\"", "\\\"")));
                }
            }
        }

        [Test]
        public void TestNoDuplicateLocalizationsOrError()
        {
            foreach(var l in _langList) {
                Assert.IsEmpty(_output[l.Key]);
            }
        }
    }
}
