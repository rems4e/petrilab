﻿//
//  TestPythonRuntimeInfo.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2014-12-09.
//

using NUnit.Framework;

namespace Petri.Test.Application.Configuration
{
    [TestFixture]
    public class TestGetPythonRuntimeInfo
    {
        [TestCase("a")]
        [TestCase("")]
        [TestCase(null)]
        [TestCase("0")]
        [TestCase("25")]
        public void TestGetInvalidPythonVersion(string version)
        {
            var info = Petri.Application.Configuration.GetPythonRuntimeInfo(version);

            Assert.NotNull(info);
            Assert.IsTrue(info.Item1);
            StringAssert.StartsWith("3.", info.Item2);
            Assert.NotNull(info.Item3);
        }

        [Test]
        public void TestGetPython2Version()
        {
            var info = Petri.Application.Configuration.GetPythonRuntimeInfo("2");

            Assert.NotNull(info);
            Assert.IsTrue(info.Item1);
            StringAssert.StartsWith("2.", info.Item2);
            Assert.IsEmpty(info.Item3);
        }

        [Test]
        public void TestGetPython3Version()
        {
            var info = Petri.Application.Configuration.GetPythonRuntimeInfo("3");

            Assert.NotNull(info);
            Assert.IsTrue(info.Item1);
            StringAssert.StartsWith("3.", info.Item2);
            Assert.NotNull(info.Item3);
        }
    }
}

