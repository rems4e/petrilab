﻿//
//  TestPathUtility.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-12-27.
//

using NUnit.Framework;

namespace Petri.Test.Application
{
    [TestFixture]
    public class TestPathUtility
    {
        static void CheckParamsAndResult(string file, string folder, string result, string expectedResult)
        {
            Assert.True(System.IO.Path.IsPathRooted(folder));
            Assert.True(System.IO.Path.IsPathRooted(file));
            Assert.AreEqual(expectedResult, result);

            var recombined = System.IO.Path.GetFullPath(System.IO.Path.Combine(folder, result));

            Assert.AreEqual(recombined, file);
        }

        [TestCase("/file_or_folder_that_should_not_exist_or_at_least_not_be_a_symlink")]
        [TestCase("/file_or_folder_that_should_not_exist/hey/hey")]
        [TestCase("/usr")]
        [TestCase("/usr/bin")]
        public void TestRealPath(string entry)
        {
            // GIVEN a filesystem entry, which does not exist

            // WHEN we resolve it with realpath(3)
            var result = Petri.Application.PathUtility.RealPath(entry);

            // THEN we get no segfault, null ref, and get back the original value
            Assert.AreEqual(entry, result);
        }

        [Test]
        public void TestSimple()
        {
            var fileName = "file";
            var folder = "/folder";
            var file = folder + "/" + fileName;

            var result = Petri.Application.PathUtility.GetRelativePath(file, folder);

            CheckParamsAndResult(file, folder, result, fileName);
        }

        [Test]
        public void TestSimpleWithTrailingSeparator()
        {
            var fileName = "file";
            var folder = "/folder/";
            var file = folder + fileName;

            var result = Petri.Application.PathUtility.GetRelativePath(file, folder);

            CheckParamsAndResult(file, folder, result, fileName);
        }

        [Test]
        public void TestDeeperSimple()
        {
            var fileName = "file";
            var folder = "/folder/subfolder/";
            var file = folder + fileName;

            var result = Petri.Application.PathUtility.GetRelativePath(file, folder);

            CheckParamsAndResult(file, folder, result, fileName);
        }

        [Test]
        public void TestSimpleWithFileSpace()
        {
            var fileName = "file with space";
            var folder = "/folder";
            var file = folder + "/" + fileName;

            var result = Petri.Application.PathUtility.GetRelativePath(file, folder);

            CheckParamsAndResult(file, folder, result, fileName);
        }

        [Test]
        public void TestSimpleWithDirectorySpace()
        {
            var fileName = "file";
            var folder = "/folder with space";
            var file = folder + "/" + fileName;

            var result = Petri.Application.PathUtility.GetRelativePath(file, folder);

            CheckParamsAndResult(file, folder, result, fileName);
        }

        [Test]
        public void TestSimpleWithUnicode()
        {
            var fileName = "file with some unicode chars éà➳";
            var folder = "/folder with some others unicode chars ⠉⠵";
            var file = folder + "/" + fileName;

            var result = Petri.Application.PathUtility.GetRelativePath(file, folder);

            CheckParamsAndResult(file, folder, result, fileName);
        }

        [TestCase("/folder", "/file")]
        [TestCase("/folder/subdirectory", "/folder/file")]
        public void TestDifferentDirectory1(string folder, string file)
        {
            var result = Petri.Application.PathUtility.GetRelativePath(file, folder);

            CheckParamsAndResult(file, folder, result, "../file");
        }

        [TestCase("/folder", "/another folder/file")]
        [TestCase("/folder/subdirectory", "/folder/another folder/file")]
        public void TestDifferentDirectory2(string folder, string file)
        {
            var result = Petri.Application.PathUtility.GetRelativePath(file, folder);

            CheckParamsAndResult(file, folder, result, "../another folder/file");
        }

        [TestCase("/folder/subfolder", "/file")]
        [TestCase("/folder/subfolder/again", "/folder/file")]
        public void TestDifferentDirectory3(string folder, string file)
        {
            var result = Petri.Application.PathUtility.GetRelativePath(file, folder);

            CheckParamsAndResult(file, folder, result, "../../file");
        }

        [TestCase("/", "/file")]
        [TestCase("/", "/folder/file")]
        [TestCase("/", "/folder/another subdirectory/file")]
        public void TestDifferentDirectory4(string folder, string file)
        {
            var result = Petri.Application.PathUtility.GetRelativePath(file, folder);

            CheckParamsAndResult(file, folder, result, file.Substring(1));
        }

        [Test, Repeat(20)]
        public void TestRandomFileNameInSameDirectory()
        {
            var fileName = CodeUtility.RandomIdentifier();
            var folder = TestingUtility.RandomAbsolutePath();
            var file = folder + fileName;

            var result = Petri.Application.PathUtility.GetRelativePath(file, folder);

            CheckParamsAndResult(file, folder, result, fileName);
        }
    }
}

