﻿//
//  TestBreakpoints.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2017-04-02.
//

using System;
using System.Collections.Generic;

using NUnit.Framework;

using Petri.Application.Debugger;

using M = Petri.Model;

namespace Petri.Test.Application.Debugger
{
    [TestFixture("CCountTo10", 10)]
    [TestFixture("CppCountTo10", 10)]
    [TestFixture("CSharpCountTo10", 10)]
    [TestFixture("PythonCountTo10", 10)]
    [TestFixture("CCountTo10plusN", 12)]
    [TestFixture("CppCountTo10plusN", 12)]
    [TestFixture("PythonCountTo10plusN", 12)]
    public class TestBreakpoints
    {
        string _path;
        protected DebuggableTestDocument _document;
        protected DebugClient _client;
        DelegateOutput _output;
        protected M.Action _action;
        protected int _iterCount;

        protected Dictionary<UInt32, Int64> _petriArgs = new Dictionary<UInt32, Int64>();

        public TestBreakpoints(string path, int iterCount)
        {
            _path = "../../../Examples/" + path + ".petri";
            _iterCount = iterCount;
        }

        [OneTimeSetUp]
        public void FixtureSetUp()
        {
            _output = new DelegateOutput();
            _document = new DebuggableTestDocument(_path, _output);
            _action = (M.Action)_document.DebugController.Find(Petri.Application.Controller.FindWhich.State,
                                                               Petri.Application.Controller.FindWhere.Name,
                                                               "print",
                                                               false)[0];
            _document.GenerateCode();
            _document.Compile().Wait();
            _client = _document.DebugController.Client;
        }

        [OneTimeTearDown]
        public void FixtureTearDown()
        {
            if(_client.CurrentSessionState != DebugClient.SessionState.Stopped) {
                _client.Detach();
            }
        }

        [Test, Order(1)]
        public virtual void TestSetup()
        {
            _document.AttachWait();
            Assert.NotNull(_document);
            Assert.NotNull(_client);
            Assert.AreEqual(Petri.Application.DebugMode.RunInEditor, _document.Settings.CurrentProfile.DebugMode);
            Assert.AreEqual(DebugClient.SessionState.Started, _client.CurrentSessionState);
        }

        [Test, Order(20)]
        public void TestAddBreakpoint()
        {
            CollectionAssert.IsEmpty(_document.DebugController.Breakpoints);
            _document.DebugController.AddBreakpoint(_action);
            Assert.AreEqual(1, _document.DebugController.Breakpoints.Count);
        }

        [Test, Order(50)]
        public virtual void TestRunUntilBreakpoint()
        {
            Assert.AreEqual(DebugClient.SessionState.Started, _client.CurrentSessionState);
            Assert.AreEqual(DebugClient.PetriState.Stopped, _client.CurrentPetriState);

            _document.RunWait(DebugClient.PetriState.Paused, _petriArgs);

            Assert.AreEqual(DebugClient.PetriState.Paused, _client.CurrentPetriState);
        }

        [Test, Order(55)]
        public void TestResume()
        {
            for(int i = 0; i < _iterCount; ++i) {
                // 1 eval out of 3 resumes for better performance
                ResumeImpl(true, i, TestingUtility.RandomBetween(0, 100) <= 33);
            }
        }

        /// <summary>
        /// Resumes the petri net execution if requested, and performs eval() tests before that
        /// </summary>
        /// <param name="resume">If set to <c>true</c>, we have to resume execution ourselves.</param>
        /// <param name="i">The current iteration number.</param>
        /// <param name="shouldEval"><c>true</c> if some eval can be performed, <c>false</c> otherwise</param>
        protected virtual void ResumeImpl(bool resume, int i, bool shouldEval)
        {
            Assert.AreEqual(DebugClient.PetriState.Paused, _client.CurrentPetriState);
            Assert.AreEqual(DebugClient.SessionState.Started, _client.CurrentSessionState);

            if(resume) {
                _document.ResumeWait();
            }
        }

        [Test, Order(60)]
        public virtual void TestFinishExecution()
        {
            TestingUtility.WaitFor(() => {
                return _client.CurrentPetriState == DebugClient.PetriState.Stopped;
            });

            Assert.AreEqual(DebugClient.PetriState.Stopped, _client.CurrentPetriState);
            Assert.AreEqual(DebugClient.SessionState.Started, _client.CurrentSessionState);
        }
    }
}
