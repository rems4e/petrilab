﻿//
//  DebuggableTestDocument.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-05-28.
//

using System;
using System.Collections.Generic;

namespace Petri.Test.Application.Debugger
{
    public delegate bool BoolDel();
    public delegate void VoidDel();
    public delegate void VoidStrDel(string value);
    public delegate void VoidListOfTuplePNVarStringDel(List<Tuple<Petri.Model.RootPetriNet, Petri.Code.VariableExpression, string>> list);

    public class DebuggableTestDocument : TestingDocument, Petri.Application.Debugger.IDebuggable
    {
        public DebuggableTestDocument(string path, Petri.Application.IOutput output) : base(path,
                                                                                            output)
        {
            Init();
        }

        public DebuggableTestDocument(Petri.Code.Language language, Petri.Application.IOutput output) : base(language, output)
        {
            Init();
        }

        void Init()
        {
            var provider = new Petri.Application.PetriNetProvider(this);
            DebugController = new Petri.Application.Debugger.DebugController(
                provider,
                new Petri.Application.Debugger.DebugClient(this, this)
            );
        }

        public Petri.Application.Debugger.DebugController BaseDebugController {
            get {
                return DebugController;
            }
        }

        public Petri.Application.Debugger.DebugController DebugController {
            get;
            private set;
        }

        public VoidDel NotifyStateChangedDel {
            get;
            set;
        }

        /// <summary>
        /// Called when the state of the petri net or the session is changed.
        /// </summary>
        public void NotifyStateChanged()
        {
            if(NotifyStateChangedDel != null) {
                NotifyStateChangedDel();
            }
        }

        /// <summary>
        /// Asks the user if regenerating the dynamic library should be attempted.
        /// </summary>
        /// <returns><c>true</c>, if we want to reload the lib, <c>false</c> otherwise.</returns>
        public System.Threading.Tasks.Task<bool> ShouldAttemptDylibReload()
        {
            return System.Threading.Tasks.Task.FromResult(true);
        }

        /// <summary>
        /// Invoked when an attempt will be made to attach to a debug host.
        /// </summary>
        public void OnStartAttachAttempt()
        {
            _lastDate = DateTime.Now;
        }

        /// <summary>
        /// Whether to interrupt a debugger attaching procedure
        /// </summary>
        /// <returns><c>true</c>, if attach was interrupted, <c>false</c> otherwise.</returns>
        public bool InterruptAttach()
        {
            return DateTime.Now > _lastDate.AddSeconds(10);
        }

        /// <summary>
        /// Invoked when a previously start attempt to attach to a debug host comes to and end.
        /// </summary>
        /// <param name="success">If set to <c>true</c> then the attempt was successful.</param>
        public void OnAttachAttemptEnd(bool success)
        {
        }

        public VoidDel NotifyStatusMessageDel {
            get;
            set;
        }

        /// <summary>
        /// Notifies a new status message.
        /// </summary>
        /// <param name="message">Message.</param>
        public void NotifyStatusMessage(string message)
        {
            if(NotifyStatusMessageDel != null) {
                NotifyStatusMessageDel();
            }
        }

        public VoidStrDel NotifyEvaluatedDel {
            get;
            set;
        }

        /// <summary>
        /// Notifies that a code expression was successfully evaluated.
        /// </summary>
        /// <param name="value">The evaluation result.</param>
        /// <param name="userInfo">Additional user info.</param>
        public void NotifyEvaluated(string value, string userInfo)
        {
            if(NotifyEvaluatedDel != null) {
                NotifyEvaluatedDel(value + userInfo);
            }
        }

        public VoidListOfTuplePNVarStringDel NotifyVariablesDel {
            get;
            set;
        }

        /// <summary>
        /// Notifies that the value of the petri net's variables have just been retrieved.
        /// </summary>
        /// <param name="variables">The list of key-value pairs.</param>
        public void NotifyVariables(List<Tuple<Petri.Model.RootPetriNet, Petri.Code.VariableExpression, string>> variables)
        {
            if(NotifyVariablesDel != null) {
                NotifyVariablesDel(variables);
            }
        }

        public VoidDel NotifyServerErrorDel {
            get;
            set;
        }

        /// <summary>
        /// Notifies an error in the debug server.
        /// </summary>
        /// <param name="message">Message.</param>
        public void NotifyServerError(string message)
        {
            if(NotifyServerErrorDel != null) {
                NotifyServerErrorDel();
            }
        }

        public VoidDel NotifyActiveStatesChangedDel {
            get;
            set;
        }

        /// <summary>
        /// Notifies a new state have been activated in the petri net, or a state have been deactivated.
        /// </summary>
        public void NotifyActiveStatesChanged()
        {
            if(NotifyActiveStatesChangedDel != null) {
                NotifyActiveStatesChangedDel();
            }
        }

        public BoolDel ShouldOrphanProcessDel {
            get;
            set;
        }

        /// <summary>
        /// Notifies the user that the current action will cause the debug server's process to be orphaned, and aks him whether to continue or not.
        /// </summary>
        /// <returns><c>true</c>, if the process should be orphaned, <c>false</c> if the current action should be cancelled.</returns>
        public bool ShouldOrphanProcess()
        {
            if(ShouldOrphanProcessDel != null) {
                return ShouldOrphanProcessDel();
            }
            return true;
        }

        /// <summary>
        /// Notifies the user from an unrecoverable error.
        /// </summary>
        /// <param name="message">The error message.</param>
        public void NotifyUnrecoverableError(string message)
        {
            if(NotifyErrorDel != null) {
                NotifyErrorDel(message);
            }
        }

        public void AttachWait(string externalHostPath = null, string hostArgs = null)
        {
            if(externalHostPath == null) {
                DebugController.Client.Attach().Wait();
            } else {
                DebugController.Client.CreateHostAndAttach(externalHostPath, hostArgs).Wait();
            }
        }

        public void DetachWait(bool endSession = false)
        {
            var doneEvent = new System.Threading.ManualResetEvent(false);

            VoidDel monitor = () => {
                if(DebugController.Client.CurrentSessionState == Petri.Application.Debugger.DebugClient.SessionState.Stopped) {
                    doneEvent.Set();
                }
            };

            NotifyStateChangedDel += monitor;

            if(endSession) {
                DebugController.Client.StopSession();
            } else {
                DebugController.Client.Detach();
            }
            doneEvent.WaitOne();

            NotifyStateChangedDel -= monitor;
        }

        public void RunWait(Petri.Application.Debugger.DebugClient.PetriState expectedStatus = Petri.Application.Debugger.DebugClient.PetriState.Started,
                            Dictionary<UInt32, Int64> arguments = null)
        {
            var doneEvent = new System.Threading.ManualResetEvent(false);

            VoidDel monitor = () => {
                if(DebugController.Client.CurrentPetriState == expectedStatus) {
                    doneEvent.Set();
                }
            };

            NotifyStateChangedDel += monitor;

            DebugController.Client.StartPetri(arguments);
            doneEvent.WaitOne();

            NotifyStateChangedDel -= monitor;
        }

        /// <summary>
        /// Resumes the execution of the petri net, and waits for it to be paused again, or stopped.
        /// </summary>
        public void ResumeWait()
        {
            var doneEvent = new System.Threading.ManualResetEvent(false);

            VoidDel monitor = () => {
                if(DebugController.Client.CurrentPetriState == Petri.Application.Debugger.DebugClient.PetriState.Paused
                   || DebugController.Client.CurrentPetriState == Petri.Application.Debugger.DebugClient.PetriState.Stopped) {
                    doneEvent.Set();
                }
            };

            NotifyStateChangedDel += monitor;

            DebugController.Client.SetPause(false);
            doneEvent.WaitOne();

            NotifyStateChangedDel -= monitor;
        }

        public string Eval(string expression, string format = "%d")
        {
            _evalResult = null;
            VoidStrDel eval = (result) => {
                _evalResult = result;
            };
            NotifyEvaluatedDel += eval;

            var args = new List<string>();
            if(Settings.Language == Petri.Code.Language.C) {
                args.Add(format);
            }

            DebugController.Client.Evaluate(
                PetriNet,
                Petri.Code.Expression.CreateFromString(expression, Settings.Language),
                args.ToArray()
            );

            TestingUtility.WaitFor(() => {
                return _evalResult != null;
            });

            return _evalResult;
        }

        DateTime _lastDate = DateTime.Now;
        volatile string _evalResult = null;
    }
}

