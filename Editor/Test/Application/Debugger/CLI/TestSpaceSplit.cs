﻿//
//  TestSpaceSplit.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-06-07.
//

using System;

using NUnit.Framework;

namespace Petri.Test.Application.Debugger.CLI
{
    [TestFixture(1)]
    [TestFixture(2)]
    [TestFixture(3)]
    [TestFixture(int.MaxValue)]
    public class TestSpaceSplit
    {
        int _howMuch;

        public TestSpaceSplit(int howMuch)
        {
            _howMuch = howMuch;
        }

        [Test, Repeat(200)]
        public void TestRandomWithZeroNonRaw()
        {
            string toSplit = CodeUtility.RandomLiteral(CodeUtility.LiteralType.String, CodeUtility.RandomLanguage()).Item1;

            var result = Petri.Application.Application.SpaceSplit(toSplit, 0);

            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(toSplit, result[0]);
        }

        [Test]
        public void TestNullString()
        {
            // GIVEN a null string
            string toSplit = null;

            // WHEN we split it
            Assert.Throws<NullReferenceException>(() => {
                Petri.Application.Application.SpaceSplit(toSplit, _howMuch);
            });
        }

        [TestCase("\"\"")]
        [TestCase("''")]
        public void TestEmptyString(string toSplit)
        {
            // GIVEN a quoted or unquoted empty string

            // WHEN we split it
            var result = Petri.Application.Application.SpaceSplit(toSplit, _howMuch);

            // THEN the resulting list is empty
            CollectionAssert.IsEmpty(result);
        }

        [TestCase("\" \"")]
        [TestCase("' '")]
        public void TestQuotedSpaceString(string toSplit)
        {
            // GIVEN a quoted string made of one space

            // WHEN we split it
            var result = Petri.Application.Application.SpaceSplit(toSplit, _howMuch);

            // THEN the resulting list contains one string made of one space
            CollectionAssert.Contains(result, " ");
            Assert.AreEqual(1, result.Count);
        }

        [TestCase("\"  \"")]
        [TestCase("'  '")]
        public void TestQuotedSpacesString(string toSplit)
        {
            // GIVEN a quoted string made of 2 spaces

            // WHEN we split it
            var result = Petri.Application.Application.SpaceSplit(toSplit, _howMuch);

            // THEN the resulting list contains one string made of 2 spaces
            CollectionAssert.Contains(result, "  ");
            Assert.AreEqual(1, result.Count);
        }

        [TestCase(" ")]
        [TestCase("  ")]
        [TestCase("\t")]
        [TestCase("\t\t")]
        [TestCase(" \t")]
        [TestCase("\t ")]
        public void TestSpaceString(string toSplit)
        {
            // GIVEN a string made of only whitespaces

            // WHEN we split it
            var result = Petri.Application.Application.SpaceSplit(toSplit, _howMuch);

            // THEN the resulting list is empty
            CollectionAssert.IsEmpty(result);
        }

        [TestCase("a")]
        [TestCase("abcdef")]
        public void TestSimpleString(string toSplit)
        {
            // GIVEN a simple string

            // WHEN we split it
            var result = Petri.Application.Application.SpaceSplit(toSplit, _howMuch);

            // THEN the resulting list contains one element, and this element is the simple string
            CollectionAssert.Contains(result, toSplit);
            Assert.AreEqual(1, result.Count);
        }

        [TestCase("abcdef ")]
        [TestCase("abcdef  ")]
        [TestCase("abcdef\t")]
        [TestCase("abcdef\t\t")]
        [TestCase("abcdef\t ")]
        [TestCase("abcdef \t")]
        public void TestSimpleStringEndingWithSpaces(string toSplit)
        {
            // GIVEN a simple string ending with some spaces

            // WHEN we split it
            var result = Petri.Application.Application.SpaceSplit(toSplit, _howMuch);

            // THEN the resulting list contains one element (2 when first only), and this element is the simple string without the trailing spaces
            Assert.AreEqual(1 + (_howMuch == 1 ? 1 : 0), result.Count);
            StringAssert.DoesNotEndWith(" ", result[0]);
            StringAssert.DoesNotEndWith("\t", result[0]);
            if(_howMuch == 1 && !System.Text.RegularExpressions.Regex.IsMatch(toSplit, "^.*\\s{2}$")) {
                Assert.IsEmpty(result[1]);
            }
            Assert.AreEqual(toSplit.TrimEnd(new char[] { ' ', '\t' }), result[0]);
        }

        [TestCase(" abcdef")]
        [TestCase("  abcdef")]
        [TestCase("\tabcdef")]
        [TestCase("\t\tabcdef")]
        [TestCase("\t abcdef")]
        [TestCase(" \tabcdef")]
        public void TestSimpleStringStartingWithSpaces(string toSplit)
        {
            // GIVEN a simple string starting with some spaces

            // WHEN we split it
            var result = Petri.Application.Application.SpaceSplit(toSplit, _howMuch);

            // THEN the resulting list contains one element, and this element is the simple string without the leading spaces
            Assert.AreEqual(1, result.Count);
            StringAssert.DoesNotStartWith(" ", result[0]);
            StringAssert.DoesNotStartWith("\t", result[0]);
            Assert.AreEqual(toSplit.TrimStart(new char[] { ' ', '\t' }), result[0]);
        }

        [TestCase("abc def")]
        [TestCase("abc\tdef")]
        public void TestTwoStrings1(string toSplit)
        {
            // GIVEN 2 string separated by some spaces

            // WHEN we split it
            var result = Petri.Application.Application.SpaceSplit(toSplit, _howMuch);

            // THEN the resulting list contains 2 elements, "abc" and "def"
            Assert.AreEqual(2, result.Count);
            CollectionAssert.Contains(result, "abc");
            CollectionAssert.Contains(result, "def");
        }

        [TestCase("abc  def")]
        [TestCase("abc\t def")]
        public void TestTwoStrings2(string toSplit)
        {
            // GIVEN 2 string separated by some spaces

            // WHEN we split it
            var result = Petri.Application.Application.SpaceSplit(toSplit, _howMuch);

            // THEN the resulting list contains 2 elements, "abc" and "def" (or " def" if first only)
            Assert.AreEqual(2, result.Count);
            CollectionAssert.Contains(result, "abc");
            if(_howMuch == 1) {
                CollectionAssert.Contains(result, " def");
            } else {
                CollectionAssert.Contains(result, "def");
            }
        }

        [TestCase("abc\t\tdef")]
        [TestCase("abc \tdef")]
        public void TestTwoStrings3(string toSplit)
        {
            // GIVEN 2 string separated by some spaces

            // WHEN we split it
            var result = Petri.Application.Application.SpaceSplit(toSplit, _howMuch);

            // THEN the resulting list contains 2 elements, "abc" and "def" (or "\tdef" if first only)
            Assert.AreEqual(2, result.Count);
            CollectionAssert.Contains(result, "abc");
            if(_howMuch == 1) {
                CollectionAssert.Contains(result, "\tdef");
            } else {
                CollectionAssert.Contains(result, "def");
            }
        }

        public void TestFourStrings([Values(0, 1, 2)] int offset)
        {
            string toSplit = "ab \"c def\" ghi jkl ";
            int howMuch = _howMuch + offset;
            // GIVEN 2 string separated by some spaces

            // WHEN we split it
            var result = Petri.Application.Application.SpaceSplit(toSplit, howMuch);

            // THEN the resulting list contains 2 elements, "abc" and "def"
            int count = howMuch + 1;
            if(howMuch == int.MaxValue) {
                count = 4;
            }
            Assert.AreEqual(count, result.Count);
            CollectionAssert.Contains(result, "ab");
            if(howMuch > 1) {
                CollectionAssert.Contains(result, "c def");
                if(howMuch > 2) {
                    CollectionAssert.Contains(result, "ghi");
                    if(howMuch > 3) {
                        CollectionAssert.Contains(result, "jkl");
                    } else {
                        CollectionAssert.Contains(result, "jkl ");
                    }
                } else {
                    CollectionAssert.Contains(result, " ghi jkl ");
                }
            } else {
                CollectionAssert.Contains(result, "\"c def\" ghi jkl ");
            }
        }

        [TestCase("a b")]
        [TestCase("a\tb")]
        public void TestTwoStrings4(string toSplit)
        {
            // GIVEN 2 string separated by some spaces

            // WHEN we split it
            var result = Petri.Application.Application.SpaceSplit(toSplit, _howMuch);

            // THEN the resulting list contains 2 elements, "a" and "b"
            Assert.AreEqual(2, result.Count);
            CollectionAssert.Contains(result, "a");
            CollectionAssert.Contains(result, "b");
        }

        [TestCase(" abc def")]
        public void TestTwoStringsWithLeadingAndTrailingSpaces1(string toSplit)
        {
            // GIVEN 2 string separated by some spaces with some leading and trailing spaces

            // WHEN we split it
            var result = Petri.Application.Application.SpaceSplit(toSplit, _howMuch);

            // THEN the resulting list contains 2 elements, "abc" and "def"
            Assert.AreEqual(2, result.Count);
            CollectionAssert.Contains(result, "abc");
            CollectionAssert.Contains(result, "def");
        }

        [TestCase("abc def ")]
        [TestCase(" abc def ")]
        public void TestTwoStringsWithLeadingAndTrailingSpaces2(string toSplit)
        {
            // GIVEN 2 string separated by some spaces with some leading and trailing spaces

            // WHEN we split it
            var result = Petri.Application.Application.SpaceSplit(toSplit, _howMuch);

            // THEN the resulting list contains 2 elements, "abc" and "def" ("def " if first only)
            Assert.AreEqual(2 + (_howMuch != 2 ? 0 : 1), result.Count);
            CollectionAssert.Contains(result, "abc");
            if(_howMuch < 2) {
                CollectionAssert.Contains(result, "def ");
            } else {
                CollectionAssert.Contains(result, "def");
            }
        }

        [TestCase(" abc  def ")]
        public void TestTwoStringsWithLeadingAndTrailingSpaces3(string toSplit)
        {
            // GIVEN 2 string separated by some spaces with some leading and trailing spaces

            // WHEN we split it
            var result = Petri.Application.Application.SpaceSplit(toSplit, _howMuch);

            // THEN the resulting list contains 2 elements, "abc" and "def" (" def " if first only)
            Assert.AreEqual(2 + (_howMuch != 2 ? 0 : 1), result.Count);
            CollectionAssert.Contains(result, "abc");
            if(_howMuch < 2) {
                CollectionAssert.Contains(result, " def ");
            } else {
                CollectionAssert.Contains(result, "def");
            }
        }

        [TestCase("abc\\ def")]
        [TestCase("\\ abc\\ def")]
        [TestCase("abc\\ def\\ ")]
        [TestCase("\\ abc\\ def\\ ")]
        public void TestUnquotedWithEscapedSpace(string toSplit)
        {
            // GIVEN an unquoted string containing escaped spaces in it

            // WHEN we split it
            var result = Petri.Application.Application.SpaceSplit(toSplit, _howMuch);

            // THEN the resulting list contains 1 element, which is "abc def" with or without a leading or trailing space.
            Assert.AreEqual(1 + (_howMuch < 2 && System.Text.RegularExpressions.Regex.IsMatch(toSplit,
                                                                                              "^.*\\s$") ? 1 : 0),
                            result.Count);
            StringAssert.IsMatch("^ ?abc ?def ?$", result[0]);
        }

        [TestCase("\"abc def\"")]
        [TestCase("\"abc def\"")]
        [TestCase("\"abc def \"")]
        [TestCase("\" abc def \"")]
        public void TestDoublyQuotedWithUnescapedSpace(string toSplit)
        {
            // GIVEN a doubly quoted string containing unescaped spaces in it

            // WHEN we split it
            var result = Petri.Application.Application.SpaceSplit(toSplit, _howMuch);

            // THEN the resulting list contains 1 element, which is "abc def" with or without a leading or trailing space.
            Assert.AreEqual(1, result.Count);
            StringAssert.DoesNotStartWith("\"", result[0]);
            StringAssert.DoesNotEndWith("\"", result[0]);
            StringAssert.IsMatch("^ ?abc ?def ?$", result[0]);
        }

        [TestCase("'abc def'")]
        [TestCase("' abc def'")]
        [TestCase("'abc def '")]
        [TestCase("' abc def '")]
        public void TestSinglyQuotedWithUnescapedSpace(string toSplit)
        {
            // GIVEN a singly quoted string containing unescaped spaces in it

            // WHEN we split it
            var result = Petri.Application.Application.SpaceSplit(toSplit, _howMuch);

            // THEN the resulting list contains 1 element, which is "abc def" with or without a leading or trailing space.
            Assert.AreEqual(1, result.Count);
            StringAssert.DoesNotStartWith("'", result[0]);
            StringAssert.DoesNotEndWith("'", result[0]);
            StringAssert.IsMatch("^ ?abc ?def ?$", result[0]);
        }

        [Test]
        public void TestDoublyQuotedWithEscapedDoubleQuote()
        {
            // GIVEN a doubly quoted string containing an escaped double quote
            string toSplit = "\"abc\\\"def\"";

            // WHEN we split it
            var result = Petri.Application.Application.SpaceSplit(toSplit, _howMuch);

            // THEN the resulting list contains 1 element, which is abc"def.
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("abc\"def", result[0]);
        }

        [Test]
        public void TestDoublyQuotedWithEscapedSingleQuote()
        {
            // GIVEN a doubly quoted string containing an escaped single quote
            string toSplit = "\"abc\\'def\"";

            // WHEN we split it
            var result = Petri.Application.Application.SpaceSplit(toSplit, _howMuch);

            // THEN the resulting list contains 1 element, which is abc\'def.
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("abc\\'def", result[0]);
        }

        [Test]
        public void TestDoublyQuotedWithUnescapedSingleQuote()
        {
            // GIVEN a doubly quoted string containing an unescaped single quote
            string toSplit = "\"abc'def\"";

            // WHEN we split it
            var result = Petri.Application.Application.SpaceSplit(toSplit, _howMuch);

            // THEN the resulting list contains 1 element, which is abc'def.
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("abc'def", result[0]);
        }

        [Test]
        public void TestSinglyQuotedWithEscapedSingleQuote()
        {
            // GIVEN a singly quoted string containing an escaped single quote
            string toSplit = "'abc\\'def'";

            // WHEN we split it
            var result = Petri.Application.Application.SpaceSplit(toSplit, _howMuch);

            // THEN the resulting list contains 1 element, which is abc'def.
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("abc'def", result[0]);
        }

        [Test]
        public void TestSinglyQuotedWithEscapedDoubleQuote()
        {
            // GIVEN a singly quoted string containing an escaped double quote
            string toSplit = "'abc\\\"def'";

            // WHEN we split it
            var result = Petri.Application.Application.SpaceSplit(toSplit, _howMuch);

            // THEN the resulting list contains 1 element, which is abc\"def.
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("abc\\\"def", result[0]);
        }

        [Test]
        public void TestSinglyQuotedWithUnescapedDoubleQuote()
        {
            // GIVEN a singly quoted string containing an unescaped double quote
            string toSplit = "'abc\"def'";

            // WHEN we split it
            var result = Petri.Application.Application.SpaceSplit(toSplit, _howMuch);

            // THEN the resulting list contains 1 element, which is abc"def.
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("abc\"def", result[0]);
        }
    }
}

