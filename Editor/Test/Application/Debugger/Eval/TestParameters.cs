﻿//
//  TestParameters.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2017-06-11.
//

using NUnit.Framework;

namespace Petri.Test.Application.Debugger.Eval
{
    [TestFixture("CCountTo10plusN")]
    [TestFixture("CppCountTo10plusN")]
    [TestFixture("CSharpCountTo10plusN")]
    [TestFixture("PythonCountTo10plusN")]
    public class TestParameters : TestBasic
    {
        public TestParameters(string path) : base(path, TestingUtility.RandomBetween(2, 30))
        {
            _petriArgs[0] = _iterCount - 10;
        }

        /// <summary>
        /// Resumes the petri net execution if requested, and performs eval() tests before that
        /// </summary>
        /// <param name="resume">If set to <c>true</c>, we have to resume execution ourselves.</param>
        /// <param name="i">The current iteration number.</param>
        /// <param name="shouldEval"><c>true</c> if some eval can be performed, <c>false</c> otherwise</param>
        protected override void ResumeImpl(bool resume, int i, bool shouldEval)
        {
            base.ResumeImpl(false, i, shouldEval);

            if(shouldEval) {
                var result = _document.Eval("$deltaCount");
                Assert.AreEqual(_petriArgs[0].ToString(), result);
            }

            if(resume) {
                _document.ResumeWait();
            }
        }
    }
}
