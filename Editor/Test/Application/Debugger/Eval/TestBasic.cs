﻿//
//  TestBasic.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2017-06-11.
//

using System;

using NUnit.Framework;

namespace Petri.Test.Application.Debugger.Eval
{
    [TestFixture("CCountTo10", 10)]
    [TestFixture("CppCountTo10", 10)]
    [TestFixture("CSharpCountTo10", 10)]
    [TestFixture("PythonCountTo10", 10)]
    public class TestBasic : TestBreakpoints
    {
        public TestBasic(string path, int iterCount) : base(path, iterCount)
        {
        }

        [Test, Order(5)]
        public void TestEvalWhenNotAttached1()
        {
            Assert.Throws<InvalidOperationException>(() => {
                _document.Eval("3");
            });
        }

        [Test, Order(6)]
        public void TestEvalWhenNotAttached2()
        {
            Assert.Throws<InvalidOperationException>(() => {
                _document.Eval("$count");
            });
        }

        [Test, Order(20)]
        public override void TestSetup()
        {
            base.TestSetup();
        }


        [Test, Order(25)]
        public void TestEvalWhenStopped1()
        {
            Assert.AreEqual("3", _document.Eval("3", "%d"));
        }

        [Test, Order(26)]
        public void TestEvalVarWhenStopped1()
        {
            Assert.Throws<NotSupportedException>(() => {
                _document.Eval("$count", "%d");
            });
        }

        [Test, Order(27), Repeat(10)]
        public void TestRandomEvalWhenStopped1()
        {
            var expression = CodeUtility.RandomLiteral(_document.Settings.Language);
            var result = _document.Eval(expression.Item1, CodeUtility.CFormatterForType(expression.Item2));

            if(expression.Item2 == CodeUtility.LiteralType.Double
               || expression.Item2 == CodeUtility.LiteralType.Float
               || expression.Item2 == CodeUtility.LiteralType.LongDouble) {
                var val = double.Parse(CodeUtility.LiteralValue(expression), System.Globalization.CultureInfo.InvariantCulture);
                var res = double.Parse(result.Replace(",", "."), System.Globalization.CultureInfo.InvariantCulture);
                Assert.True(Math.Abs(val - res) < val * 1e-5);
            } else {
                Assert.AreEqual(CodeUtility.LiteralValue(expression), result);
            }
        }

        [Test, Order(27), Repeat(3)]
        public void TestRandomEvalWhenStopped2()
        {
            var random = new Random();
            var n1 = random.Next(99999);
            var n2 = random.Next(99999);

            Assert.AreEqual((n1 + n2).ToString(), _document.Eval(string.Format("{0} + {1}", n1, n2)));
        }

        /// <summary>
        /// Resumes the petri net execution if requested, and performs eval() tests before that
        /// </summary>
        /// <param name="resume">If set to <c>true</c>, we have to resume execution ourselves.</param>
        /// <param name="i">The current iteration number.</param>
        /// <param name="shouldEval"><c>true</c> if some eval can be performed, <c>false</c> otherwise</param>
        protected override void ResumeImpl(bool resume, int i, bool shouldEval)
        {
            base.ResumeImpl(false, i, shouldEval);

            if(shouldEval) {
                var result = _document.Eval("- 1+2* $var");
                Assert.AreEqual((i * 2 - 1).ToString(), result);

                result = _document.Eval("$var + $count");
                Assert.AreEqual((i + 10).ToString(), result);
            }

            if(resume) {
                _document.ResumeWait();
            }
        }
    }
}
