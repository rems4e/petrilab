﻿//
//  TestDebugger.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-05-28.
//

using NUnit.Framework;

using Petri.Application.Debugger;

namespace Petri.Test.Application.Debugger
{
    [TestFixture("CBasic")]
    [TestFixture("CppBasic")]
    [TestFixture("CSharpBasic")]
    [TestFixture("PythonBasic")]
    [TestFixture("CCountTo10")]
    [TestFixture("CppCountTo10")]
    [TestFixture("CSharpCountTo10")]
    [TestFixture("PythonCountTo10")]
    [TestFixture("CCountTo10plusN")]
    [TestFixture("CppCountTo10plusN")]
    [TestFixture("CSharpCountTo10plusN")]
    [TestFixture("PythonCountTo10plusN")]
    [TestFixture("Python3CountTo10plusN")]
    [TestFixture("Python2CountTo10plusN")]
    [TestFixture("CCountTo10", "out")]
    [TestFixture("CppCountTo10", "out")]
    [TestFixture("CSharpCountTo10", "exe")]
    [TestFixture("PythonCountTo10", "py")]
    [TestFixture("CCountTo10plusN", "out")]
    [TestFixture("CppCountTo10plusN", "out")]
    [TestFixture("CSharpCountTo10plusN", "exe")]
    [TestFixture("Python3CountTo10plusN", "py")]
    [TestFixture("Python2CountTo10plusN", "py")]
    [TestFixture("CSync")]
    [TestFixture("CppSync")]
    [TestFixture("CSharpSync")]
    [TestFixture("PythonSync")]
    [TestFixture("InnerPetriNet/CInnerPetriNet")]
    [TestFixture("InnerPetriNet/CppInnerPetriNet")]
    [TestFixture("InnerPetriNet/CSharpInnerPetriNet")]
    [TestFixture("InnerPetriNet/PythonInnerPetriNet")]
    [TestFixture("ExternalPetriNet/CExternal")]
    [TestFixture("ExternalPetriNet/CppExternal")]
    [TestFixture("ExternalPetriNet/CSharpExternal")]
    [TestFixture("ExternalPetriNet/PythonExternal")]
    public class TestDebugger
    {
        string _path;
        string _externalHost;
        string _externalHostArguments;
        DebuggableTestDocument _document;
        DebugClient _client;
        DelegateOutput _output;

        public TestDebugger(string path) : this(path, null)
        {
        }

        public TestDebugger(string path, string externalHostExtension)
        {
            _path = "../../../Examples/" + path + ".petri";
            if(externalHostExtension != null) {
                path = path.Replace("PythonC", "Python3C");
                var wd = System.IO.Path.Combine(System.Environment.CurrentDirectory, "../../../Examples/Integration");

                var process = Petri.Application.Application.CreateProcess();
                process.StartInfo.WorkingDirectory = wd;
                process.StartInfo.FileName = "make";
                process.StartInfo.Arguments = path.Substring(0, path.IndexOfInv("Count")).ToLowerInvariant() + "10" + (path.EndsWithInv("plusN") ? "plusN" : "");

                process.Start();
                process.WaitForExit();

                _externalHost = "Integration/run" + path + "." + externalHostExtension;
                _externalHostArguments = "--debug";
                if(externalHostExtension.EndsWithInv("py")) {
                    _externalHostArguments = string.Format("-c 'cd {0}; PYTHONPATH=../.. LD_LIBRARY_PATH=. {1} {2}'", wd, _externalHost.Replace("Integration", "."), _externalHostArguments);
                    _externalHost = "/bin/sh";
                }
            }
        }

        [OneTimeSetUp]
        public void FixtureSetUp()
        {
            _output = new DelegateOutput();
            _document = new DebuggableTestDocument(_path, _output);
            _document.GenerateCode();
            _document.Compile().Wait();
            _client = _document.DebugController.Client;
        }

        [OneTimeTearDown]
        public void FixtureTearDown()
        {
            if(_client.CurrentSessionState != DebugClient.SessionState.Stopped) {
                _client.Detach();
            }
        }

        [Test, Order(1)]
        public void TestDocumentLoad()
        {
            Assert.NotNull(_document);
            Assert.NotNull(_client);
            Assert.AreEqual(Petri.Application.DebugMode.RunInEditor, _document.Settings.CurrentProfile.DebugMode);
        }

        [Test, Order(2), Repeat(5)]
        public void TestAttachDetach()
        {
            // GIVEN a stopped debugger session
            Assert.AreEqual(DebugClient.SessionState.Stopped, _client.CurrentSessionState);

            // WHEN we start the debugger session
            _document.AttachWait(_externalHost, _externalHostArguments);

            // THEN the client is correctly attached
            Assert.AreEqual(DebugClient.SessionState.Started, _client.CurrentSessionState);

            // WHEN we stop the debugger session
            _document.DetachWait();

            // THEN the client is correctly stopped
            Assert.AreEqual(DebugClient.SessionState.Stopped, _client.CurrentSessionState);

            System.Threading.Thread.Sleep(100);
        }

        [Test, Order(3)]
        public void TestAttach1()
        {
            // GIVEN a stopped debugger session
            Assert.AreEqual(DebugClient.SessionState.Stopped, _client.CurrentSessionState);

            // WHEN we start the debugger session
            _document.AttachWait(_externalHost, _externalHostArguments);

            // THEN the client is correctly attached
            Assert.AreEqual(DebugClient.SessionState.Started, _client.CurrentSessionState);
        }

        [TestCase, Order(5), Repeat(1)]
        public void TestRun1()
        {
            // GIVEN a started debugger session and a stopped petri net
            Assert.AreEqual(DebugClient.SessionState.Started, _client.CurrentSessionState);
            Assert.AreEqual(DebugClient.PetriState.Stopped, _client.CurrentPetriState);


            // WHEN we start the petri net execution and wait for it to stop
            _document.RunWait();

            TestingUtility.WaitFor(() => {
                return _client.CurrentPetriState == DebugClient.PetriState.Stopped;
            });

            // THEN the petri net is stopped and the client is still correctly attached
            Assert.AreEqual(DebugClient.PetriState.Stopped, _client.CurrentPetriState);
            Assert.AreEqual(DebugClient.SessionState.Started, _client.CurrentSessionState);
        }

        [Test, Order(10)]
        public void TestDetach1()
        {
            // GIVEN a started debugger session
            Assert.AreEqual(DebugClient.SessionState.Started, _client.CurrentSessionState);

            // WHEN we stop the debugger session
            _document.DetachWait();

            // THEN the client is correctly detached
            Assert.AreEqual(DebugClient.SessionState.Stopped, _client.CurrentSessionState);
        }

        [Test, Order(20)]
        public void TestAttach2()
        {
            // GIVEN a stopped debugger session
            Assert.AreEqual(DebugClient.SessionState.Stopped, _client.CurrentSessionState);

            // WHEN we start the debugger session
            _document.AttachWait(_externalHost, _externalHostArguments);

            // THEN the client is correctly attached
            Assert.AreEqual(DebugClient.SessionState.Started, _client.CurrentSessionState);
        }

        [TestCase, Repeat(2), Order(40)]
        public void TestRun2()
        {
            // GIVEN a started debugger session and a stopped petri net
            Assert.AreEqual(DebugClient.SessionState.Started, _client.CurrentSessionState);
            Assert.AreEqual(DebugClient.PetriState.Stopped, _client.CurrentPetriState);


            // WHEN we start the petri net execution and wait for it to stop
            _document.RunWait();

            TestingUtility.WaitFor(() => {
                return _client.CurrentPetriState == DebugClient.PetriState.Stopped;
            });

            // THEN the petri net is stopped and the client is still correctly attached
            Assert.AreEqual(DebugClient.PetriState.Stopped, _client.CurrentPetriState);
            Assert.AreEqual(DebugClient.SessionState.Started, _client.CurrentSessionState);
        }

        [Test, Order(999)]
        public void TestDetachLast()
        {
            // GIVEN a started debugger session
            Assert.AreEqual(DebugClient.SessionState.Started, _client.CurrentSessionState);

            // WHEN we stop the debugger session
            _document.DetachWait();

            // THEN the client is correctly detached
            Assert.AreEqual(DebugClient.SessionState.Stopped, _client.CurrentSessionState);
        }
    }
}
