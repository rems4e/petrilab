﻿//
//  TestCompiler.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2017-11-06.
//

using System.Collections.Generic;
using System.Linq;

using NUnit.Framework;

using Petri.Application.CLI;

namespace Petri.Test.Application.Compiler
{
    [TestFixture("CBasic")]
    [TestFixture("CppBasic")]
    [TestFixture("CSharpBasic")]
    [TestFixture("PythonBasic")]
    [TestFixture("CSync")]
    [TestFixture("CppSync")]
    [TestFixture("CSharpSync")]
    [TestFixture("PythonSync")]
    [TestFixture("CCountTo10")]
    [TestFixture("CppCountTo10")]
    [TestFixture("CSharpCountTo10")]
    [TestFixture("PythonCountTo10")]
    [TestFixture("CCountTo10plusN")]
    [TestFixture("CppCountTo10plusN")]
    [TestFixture("CSharpCountTo10plusN")]
    [TestFixture("PythonCountTo10plusN")]
    [TestFixture("InnerPetriNet/CInnerPetriNet")]
    [TestFixture("InnerPetriNet/CppInnerPetriNet")]
    [TestFixture("InnerPetriNet/CSharpInnerPetriNet")]
    [TestFixture("InnerPetriNet/PythonInnerPetriNet")]
    [TestFixture("ExternalPetriNet/CExternal")]
    [TestFixture("ExternalPetriNet/CppExternal")]
    [TestFixture("ExternalPetriNet/CSharpExternal")]
    [TestFixture("ExternalPetriNet/PythonExternal")]
    [TestFixture("ExternalPetriNet/CLocalVariables")]
    [TestFixture("ExternalPetriNet/CppLocalVariables")]
    [TestFixture("ExternalPetriNet/CSharpLocalVariables")]
    [TestFixture("ExternalPetriNet/PythonLocalVariables")]
    [TestFixture("ExternalPetriNet/CConcurrent")]
    [TestFixture("ExternalPetriNet/CConcurrentInvocation")]
    [TestFixture("ExternalPetriNet/CppConcurrent")]
    [TestFixture("ExternalPetriNet/CppConcurrentInvocation")]
    [TestFixture("ExternalPetriNet/CSharpConcurrent")]
    [TestFixture("ExternalPetriNet/CSharpConcurrentInvocation")]
    [TestFixture("ExternalPetriNet/PythonConcurrent")]
    [TestFixture("ExternalPetriNet/PythonConcurrentInvocation")]
    public class TestCompiler
    {
        HeadlessDocument _doc;

        public TestCompiler(string path)
        {
            _doc = new TestingDocument("../../../Examples/" + path + ".petri");
            _doc.Compiler = new TestingCompiler(_doc, false);
            foreach(var doc in _doc.AllExternalDocuments) {
                doc.Compiler = new TestingCompiler(doc, false);
            }
        }

        [Test]
        public void TestCompilerInvocations()
        {
            CollectionAssert.IsEmpty(((TestingCompiler)_doc.Compiler).Invocations);

            var result = _doc.Compile().Result;

            Assert.AreEqual(Petri.Application.Compiler.CompilationStatus.Success, result.Item1);
            Assert.AreEqual("", result.Item2);

            var pathSet = new HashSet<string>();

            foreach(var doc in new List<Petri.Application.Document> { _doc }.Union(_doc.AllExternalDocuments)) {
                if(!pathSet.Add(doc.Path)) {
                    // Ignore already compiled paths that are not recompiled.
                    continue;
                }

                var compiler = (TestingCompiler)doc.Compiler;

                var expectedCount = 1;
                if(doc.Settings.Language != Petri.Code.Language.CSharp) {
                    ++expectedCount;
                }

                if(doc.Settings.Language != Petri.Code.Language.CSharp) {
                    Assert.AreEqual(expectedCount--, compiler.Invocations.Count);

                    var archiverInvocation = compiler.Invocations.Pop();
                    Assert.AreEqual(doc.Settings.Archiver, archiverInvocation.Item1);
                    Assert.AreEqual(doc.Settings.GetArchiverArguments(doc.Settings.StandardIntermediatePath, doc.Settings.StandardLibPath, compiler.DetectToolchain()), archiverInvocation.Item2);
                }

                Assert.AreEqual(expectedCount--, compiler.Invocations.Count);

                var compilerInvocation = compiler.Invocations.Pop();
                Assert.AreEqual(doc.Settings.CurrentProfile.CompilerInfo.Compiler, compilerInvocation.Item1);
                Assert.AreEqual(doc.Settings.GetCompilerArguments(doc.Sources, doc.Settings.StandardIntermediatePath, compiler.DetectToolchain()), compilerInvocation.Item2);

                Assert.AreEqual(0, compiler.Invocations.Count);

                compiler.Invocations.Clear();
            }
        }
    }
}
