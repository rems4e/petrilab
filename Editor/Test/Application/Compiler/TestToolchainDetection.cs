﻿//
//  TestToolchainDetection.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2018-01-28.
//

using NUnit.Framework;

namespace Petri.Test.Application.Compiler
{
    [TestFixture]
    public class TestToolchainDetection
    {
        [TestCase("CBasic")]
        [TestCase("CppBasic")]
        [TestCase("CSharpBasic")]
        [TestCase("PythonBasic")]
        [TestCase("CSync")]
        [TestCase("CppSync")]
        [TestCase("CSharpSync")]
        [TestCase("PythonSync")]
        [TestCase("CCountTo10")]
        [TestCase("CppCountTo10")]
        [TestCase("CSharpCountTo10")]
        [TestCase("PythonCountTo10")]
        [TestCase("CCountTo10plusN")]
        [TestCase("CppCountTo10plusN")]
        [TestCase("CSharpCountTo10plusN")]
        [TestCase("PythonCountTo10plusN")]
        [TestCase("InnerPetriNet/CInnerPetriNet")]
        [TestCase("InnerPetriNet/CppInnerPetriNet")]
        [TestCase("InnerPetriNet/CSharpInnerPetriNet")]
        [TestCase("InnerPetriNet/PythonInnerPetriNet")]
        [TestCase("ExternalPetriNet/CExternal")]
        [TestCase("ExternalPetriNet/CppExternal")]
        [TestCase("ExternalPetriNet/CSharpExternal")]
        [TestCase("ExternalPetriNet/PythonExternal")]
        [TestCase("ExternalPetriNet/CLocalVariables")]
        [TestCase("ExternalPetriNet/CppLocalVariables")]
        [TestCase("ExternalPetriNet/CSharpLocalVariables")]
        [TestCase("ExternalPetriNet/PythonLocalVariables")]
        [TestCase("ExternalPetriNet/CConcurrent")]
        [TestCase("ExternalPetriNet/CppConcurrent")]
        [TestCase("ExternalPetriNet/CSharpConcurrent")]
        [TestCase("ExternalPetriNet/PythonConcurrent")]
        public void TestToolchainIsNotUnknown(string path)
        {
            var doc = new TestingDocument("../../../Examples/" + path + ".petri");

            Assert.AreNotEqual(Petri.Application.Compiler.Toolchain.Unknown, doc.Compiler.DetectToolchain());
        }

        [TestCase("CBasic", "cc")]
        [TestCase("CBasic", "clang")]
        [TestCase("CBasic", "gcc")]
        [TestCase("PythonBasic", "cc")]
        [TestCase("PythonBasic", "clang")]
        [TestCase("PythonBasic", "gcc")]
        public void TestCAndPythonToolchain(string path, string compiler)
        {
            var doc = new TestingDocument("../../../Examples/" + path + ".petri");

            doc.Settings.CurrentProfile.CompilerInfo.Compiler = compiler;
            var toolchain = doc.Compiler.DetectToolchain();
            if(compiler == "cc") {
                Assert.AreNotEqual(Petri.Application.Compiler.Toolchain.Unknown, toolchain);
            } else if(compiler == "gcc") {
                Assert.AreEqual(Petri.Application.Compiler.Toolchain.GCC, toolchain);
            } else if(compiler == "clang") {
                Assert.AreEqual(Petri.Application.Compiler.Toolchain.Clang, toolchain);
            } else {
                Assert.Fail("Should not get there!");
            }
        }

        [TestCase("CppBasic", "c++")]
        [TestCase("CppBasic", "clang++")]
        [TestCase("CppBasic", "g++")]
        public void TestCppToolchain(string path, string compiler)
        {
            var doc = new TestingDocument("../../../Examples/" + path + ".petri");

            doc.Settings.CurrentProfile.CompilerInfo.Compiler = compiler;
            var toolchain = doc.Compiler.DetectToolchain();
            if(compiler == "c++") {
                Assert.AreNotEqual(Petri.Application.Compiler.Toolchain.Unknown, toolchain);
            } else if(compiler == "g++") {
                Assert.AreEqual(Petri.Application.Compiler.Toolchain.GCC, toolchain);
            } else if(compiler == "clang++") {
                Assert.AreEqual(Petri.Application.Compiler.Toolchain.Clang, toolchain);
            } else {
                Assert.Fail("Should not get there!");
            }
        }

        [TestCase("CSharpBasic", "csc")]
        [TestCase("CSharpBasic", "mcs")]
        public void TestCSharpToolchain(string path, string compiler)
        {
            var doc = new TestingDocument("../../../Examples/" + path + ".petri");

            doc.Settings.CurrentProfile.CompilerInfo.Compiler = compiler;
            var toolchain = doc.Compiler.DetectToolchain();
            if(compiler == "csc") {
                Assert.AreEqual(Petri.Application.Compiler.Toolchain.MSVisualCS, toolchain);
            } else if(compiler == "mcs") {
                Assert.AreEqual(Petri.Application.Compiler.Toolchain.MonoCS, toolchain);
            } else {
                Assert.Fail("Should not get there!");
            }
        }
    }
}
