﻿//
//  TestProfilesOverriding.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2017-11-06.
//

using NUnit.Framework;

using Petri.Application.CLI;

namespace Petri.Test.Application.Compiler
{
    [TestFixture("CInstantiateN")]
    [TestFixture("CppInstantiateN")]
    [TestFixture("CSharpInstantiateN")]
    public class TestProfilesOverriding
    {
        HeadlessDocument _doc;
        Petri.Application.Document _external;

        Petri.Application.DocumentSettingsProfile _altProfile;
        Petri.Application.DocumentSettingsProfile _altProfile2;

        static readonly string AltCompiler = "This is Sparta.exe";
        static readonly string AltArg1 = "nananana";
        static readonly string AltArg2 = "-std=cpp42";

        static readonly string AltCompiler2 = "a.out";
        static readonly string AltArg3 = "--blah";

        public TestProfilesOverriding(string path)
        {
            _doc = new TestingDocument("../../../Examples/ExternalPetriNet/" + path + ".petri");
            _doc.Compiler = new TestingCompiler(_doc, false);
            foreach(var doc in _doc.AllExternalDocuments) {
                doc.Compiler = new TestingCompiler(doc, false);
                _external = doc;
            }

            _altProfile = Petri.Application.DocumentSettingsProfile.GetDefaultProfile(_doc.Settings.Language);
            _altProfile.Name = "Alt";
            _altProfile.CompilerInfo.Compiler = AltCompiler;
            _altProfile.CompilerInfo.CustomCompilerFlags.Add(AltArg1);
            _altProfile.CompilerInfo.CustomCompilerFlags.Add(AltArg2);
            _doc.Settings.Profiles.Add(_altProfile);

            _altProfile2 = Petri.Application.DocumentSettingsProfile.GetDefaultProfile(_doc.Settings.Language);
            _altProfile2.Name = "Alt";
            _altProfile2.CompilerInfo.Compiler = AltCompiler2;
            _altProfile2.CompilerInfo.CustomCompilerFlags.Add(AltArg3);
            // Purposely not added to the document's profiles
        }

        [TearDown]
        public void TearDown()
        {
            string[] args = { "--clean", _doc.Path };

            CompilerUtility.InvokeCompiler(args);
        }

        [Test]
        public void TestAltProfileNoOverride()
        {
            _doc.Settings.OverrideChildCompilationSettings = false;

            var oldProfile = _doc.Settings.CurrentProfile;
            var oldProfile2 = _external.Settings.CurrentProfile;
            _doc.Settings.CurrentProfile = _altProfile;

            // GIVEN a freshly loaded document with a custom profile
            var compiler = (TestingCompiler)_doc.Compiler;

            // WHEN it is compiled
            var result = _doc.Compile().Result;

            if(compiler.Invocations.Count > 1) {
                compiler.Invocations.Pop();
            }

            // THEN we find the correct value of the compiler and arguments in the invocation
            Assert.AreEqual(_doc.Settings.CurrentProfile.CompilerInfo.Compiler, compiler.Invocations.Peek().Item1);
            Assert.AreEqual(_doc.Settings.GetCompilerArguments(_doc.Sources, _doc.Settings.StandardIntermediatePath, compiler.DetectToolchain()), compiler.Invocations.Peek().Item2);
            Assert.AreEqual(AltCompiler, compiler.Invocations.Peek().Item1);
            StringAssert.Contains(AltArg1, compiler.Invocations.Peek().Item2);
            StringAssert.Contains(AltArg2, compiler.Invocations.Peek().Item2);
            StringAssert.DoesNotContain(AltArg3, compiler.Invocations.Peek().Item2);

            compiler.Invocations.Clear();
            compiler = (TestingCompiler)_external.Compiler;

            if(compiler.Invocations.Count > 1) {
                compiler.Invocations.Pop();
            }

            // AND we find the correct value for the child document
            Assert.AreEqual(_external.Settings.CurrentProfile.CompilerInfo.Compiler, compiler.Invocations.Peek().Item1);
            Assert.AreEqual(_external.Settings.GetCompilerArguments(_external.Sources, _external.Settings.StandardIntermediatePath, compiler.DetectToolchain()), compiler.Invocations.Peek().Item2);
            Assert.AreNotEqual(AltCompiler, compiler.Invocations.Peek().Item1);
            StringAssert.DoesNotContain(AltArg1, compiler.Invocations.Peek().Item2);
            StringAssert.DoesNotContain(AltArg2, compiler.Invocations.Peek().Item2);
            StringAssert.DoesNotContain(AltArg3, compiler.Invocations.Peek().Item2);

            compiler.Invocations.Clear();

            _doc.Settings.CurrentProfile = oldProfile;
            _external.Settings.CurrentProfile = oldProfile2;
            if(_external.Settings.Profiles.Contains(_altProfile2)) {
                _external.Settings.Profiles.Remove(_altProfile2);
            }
        }

        [Test]
        public void TestAltProfileChildTooNoOverride()
        {
            _doc.Settings.OverrideChildCompilationSettings = false;

            var oldProfile = _doc.Settings.CurrentProfile;
            var oldProfile2 = _external.Settings.CurrentProfile;
            _doc.Settings.CurrentProfile = _altProfile;
            _external.Settings.CurrentProfile = _altProfile2;
            _external.Settings.Profiles.Add(_altProfile2);

            // GIVEN a freshly loaded document with a custom profile
            var compiler = (TestingCompiler)_doc.Compiler;

            // WHEN it is compiled
            var result = _doc.Compile().Result;

            if(compiler.Invocations.Count > 1) {
                compiler.Invocations.Pop();
            }

            // THEN we find the correct value of the compiler and arguments in the invocation
            Assert.AreEqual(_doc.Settings.CurrentProfile.CompilerInfo.Compiler, compiler.Invocations.Peek().Item1);
            Assert.AreEqual(_doc.Settings.GetCompilerArguments(_doc.Sources, _doc.Settings.StandardIntermediatePath, compiler.DetectToolchain()), compiler.Invocations.Peek().Item2);
            Assert.AreEqual(AltCompiler, compiler.Invocations.Peek().Item1);
            StringAssert.Contains(AltArg1, compiler.Invocations.Peek().Item2);
            StringAssert.Contains(AltArg2, compiler.Invocations.Peek().Item2);
            StringAssert.DoesNotContain(AltArg3, compiler.Invocations.Peek().Item2);

            compiler.Invocations.Clear();
            compiler = (TestingCompiler)_external.Compiler;

            if(compiler.Invocations.Count > 1) {
                compiler.Invocations.Pop();
            }

            // AND we find the correct value for the child document
            Assert.AreEqual(_external.Settings.CurrentProfile.CompilerInfo.Compiler, compiler.Invocations.Peek().Item1);
            Assert.AreEqual(_external.Settings.GetCompilerArguments(_external.Sources, _external.Settings.StandardIntermediatePath, compiler.DetectToolchain()), compiler.Invocations.Peek().Item2);
            Assert.AreEqual(AltCompiler2, compiler.Invocations.Peek().Item1);
            StringAssert.DoesNotContain(AltArg1, compiler.Invocations.Peek().Item2);
            StringAssert.DoesNotContain(AltArg2, compiler.Invocations.Peek().Item2);
            StringAssert.Contains(AltArg3, compiler.Invocations.Peek().Item2);

            compiler.Invocations.Clear();

            _doc.Settings.CurrentProfile = oldProfile;
            _external.Settings.CurrentProfile = oldProfile2;
            if(_external.Settings.Profiles.Contains(_altProfile2)) {
                _external.Settings.Profiles.Remove(_altProfile2);
            }
        }

        [Test]
        public void TestAltProfileChildOnlyNoOverride()
        {
            _doc.Settings.OverrideChildCompilationSettings = false;

            var oldProfile = _doc.Settings.CurrentProfile;
            var oldProfile2 = _external.Settings.CurrentProfile;
            _external.Settings.CurrentProfile = _altProfile2;
            _external.Settings.Profiles.Add(_altProfile2);

            // GIVEN a freshly loaded document with a custom profile
            var compiler = (TestingCompiler)_doc.Compiler;

            // WHEN it is compiled
            var result = _doc.Compile().Result;

            if(compiler.Invocations.Count > 1) {
                compiler.Invocations.Pop();
            }

            // THEN we find the correct value of the compiler and arguments in the invocation
            Assert.AreEqual(_doc.Settings.CurrentProfile.CompilerInfo.Compiler, compiler.Invocations.Peek().Item1);
            Assert.AreEqual(_doc.Settings.GetCompilerArguments(_doc.Sources, _doc.Settings.StandardIntermediatePath, compiler.DetectToolchain()), compiler.Invocations.Peek().Item2);
            Assert.AreNotEqual(AltCompiler, compiler.Invocations.Peek().Item1);
            StringAssert.DoesNotContain(AltArg1, compiler.Invocations.Peek().Item2);
            StringAssert.DoesNotContain(AltArg2, compiler.Invocations.Peek().Item2);
            StringAssert.DoesNotContain(AltArg3, compiler.Invocations.Peek().Item2);

            compiler.Invocations.Clear();
            compiler = (TestingCompiler)_external.Compiler;

            if(compiler.Invocations.Count > 1) {
                compiler.Invocations.Pop();
            }

            // AND we find the correct value for the child document
            Assert.AreNotEqual(_external.Settings.CurrentProfile.CompilerInfo.Compiler, compiler.Invocations.Peek().Item1);
            Assert.AreEqual(_external.Settings.GetCompilerArguments(_external.Sources, _external.Settings.StandardIntermediatePath, compiler.DetectToolchain()), compiler.Invocations.Peek().Item2);
            Assert.AreNotEqual(AltCompiler2, compiler.Invocations.Peek().Item1);
            StringAssert.DoesNotContain(AltArg1, compiler.Invocations.Peek().Item2);
            StringAssert.DoesNotContain(AltArg2, compiler.Invocations.Peek().Item2);
            StringAssert.DoesNotContain(AltArg3, compiler.Invocations.Peek().Item2);

            compiler.Invocations.Clear();

            _doc.Settings.CurrentProfile = oldProfile;
            _external.Settings.CurrentProfile = oldProfile2;
            if(_external.Settings.Profiles.Contains(_altProfile2)) {
                _external.Settings.Profiles.Remove(_altProfile2);
            }
        }

        [Test]
        public void TestAltProfileOverride()
        {
            _doc.Settings.OverrideChildCompilationSettings = true;

            var oldProfile = _doc.Settings.CurrentProfile;
            var oldProfile2 = _external.Settings.CurrentProfile;
            _doc.Settings.CurrentProfile = _altProfile;

            // GIVEN a freshly loaded document with a custom profile
            var compiler = (TestingCompiler)_doc.Compiler;

            // WHEN it is compiled
            var result = _doc.Compile().Result;

            if(compiler.Invocations.Count > 1) {
                compiler.Invocations.Pop();
            }

            // THEN we find the correct value of the compiler and arguments in the invocation
            Assert.AreEqual(_doc.Settings.CurrentProfile.CompilerInfo.Compiler, compiler.Invocations.Peek().Item1);
            Assert.AreEqual(_doc.Settings.GetCompilerArguments(_doc.Sources, _doc.Settings.StandardIntermediatePath, compiler.DetectToolchain()), compiler.Invocations.Peek().Item2);
            Assert.AreEqual(AltCompiler, compiler.Invocations.Peek().Item1);
            StringAssert.Contains(AltArg1, compiler.Invocations.Peek().Item2);
            StringAssert.Contains(AltArg2, compiler.Invocations.Peek().Item2);
            StringAssert.DoesNotContain(AltArg3, compiler.Invocations.Peek().Item2);

            compiler.Invocations.Clear();
            compiler = (TestingCompiler)_external.Compiler;

            if(compiler.Invocations.Count > 1) {
                compiler.Invocations.Pop();
            }

            // AND we find the correct value for the child document
            Assert.AreNotEqual(_external.Settings.CurrentProfile.CompilerInfo.Compiler, compiler.Invocations.Peek().Item1);
            Assert.AreEqual(_doc.Settings.CurrentProfile.CompilerInfo.Compiler, compiler.Invocations.Peek().Item1);
            Assert.AreEqual(_external.Settings.GetCompilerArguments(_external.Sources, _external.Settings.StandardIntermediatePath, compiler.DetectToolchain()), compiler.Invocations.Peek().Item2);
            Assert.AreEqual(AltCompiler, compiler.Invocations.Peek().Item1);
            StringAssert.Contains(AltArg1, compiler.Invocations.Peek().Item2);
            StringAssert.Contains(AltArg2, compiler.Invocations.Peek().Item2);
            StringAssert.DoesNotContain(AltArg3, compiler.Invocations.Peek().Item2);

            compiler.Invocations.Clear();

            _doc.Settings.CurrentProfile = oldProfile;
            _external.Settings.CurrentProfile = oldProfile2;
            if(_external.Settings.Profiles.Contains(_altProfile2)) {
                _external.Settings.Profiles.Remove(_altProfile2);
            }
        }

        [Test]
        public void TestAltProfileChildTooOverride()
        {
            _doc.Settings.OverrideChildCompilationSettings = true;

            var oldProfile = _doc.Settings.CurrentProfile;
            var oldProfile2 = _external.Settings.CurrentProfile;
            _doc.Settings.CurrentProfile = _altProfile;
            _external.Settings.CurrentProfile = _altProfile2;
            _external.Settings.Profiles.Add(_altProfile2);

            // GIVEN a freshly loaded document with a custom profile
            var compiler = (TestingCompiler)_doc.Compiler;

            // WHEN it is compiled
            var result = _doc.Compile().Result;

            if(compiler.Invocations.Count > 1) {
                compiler.Invocations.Pop();
            }

            // THEN we find the correct value of the compiler and arguments in the invocation
            Assert.AreEqual(_doc.Settings.CurrentProfile.CompilerInfo.Compiler, compiler.Invocations.Peek().Item1);
            Assert.AreEqual(_doc.Settings.GetCompilerArguments(_doc.Sources, _doc.Settings.StandardIntermediatePath, compiler.DetectToolchain()), compiler.Invocations.Peek().Item2);
            Assert.AreEqual(AltCompiler, compiler.Invocations.Peek().Item1);
            StringAssert.Contains(AltArg1, compiler.Invocations.Peek().Item2);
            StringAssert.Contains(AltArg2, compiler.Invocations.Peek().Item2);
            StringAssert.DoesNotContain(AltArg3, compiler.Invocations.Peek().Item2);

            compiler.Invocations.Clear();
            compiler = (TestingCompiler)_external.Compiler;

            if(compiler.Invocations.Count > 1) {
                compiler.Invocations.Pop();
            }

            // AND we find the correct value for the child document
            Assert.AreNotEqual(_external.Settings.CurrentProfile.CompilerInfo.Compiler, compiler.Invocations.Peek().Item1);
            Assert.AreEqual(_doc.Settings.CurrentProfile.CompilerInfo.Compiler, compiler.Invocations.Peek().Item1);
            Assert.AreEqual(_external.Settings.GetCompilerArguments(_external.Sources, _external.Settings.StandardIntermediatePath, compiler.DetectToolchain()), compiler.Invocations.Peek().Item2);
            Assert.AreEqual(AltCompiler, compiler.Invocations.Peek().Item1);
            StringAssert.Contains(AltArg1, compiler.Invocations.Peek().Item2);
            StringAssert.Contains(AltArg2, compiler.Invocations.Peek().Item2);
            StringAssert.DoesNotContain(AltArg3, compiler.Invocations.Peek().Item2);

            compiler.Invocations.Clear();

            _doc.Settings.CurrentProfile = oldProfile;
            _external.Settings.CurrentProfile = oldProfile2;
            if(_external.Settings.Profiles.Contains(_altProfile2)) {
                _external.Settings.Profiles.Remove(_altProfile2);
            }
        }

        [Test]
        public void TestAltProfileChildOnlyOverride()
        {
            _doc.Settings.OverrideChildCompilationSettings = true;

            var oldProfile = _doc.Settings.CurrentProfile;
            var oldProfile2 = _external.Settings.CurrentProfile;
            _external.Settings.CurrentProfile = _altProfile2;
            _external.Settings.Profiles.Add(_altProfile2);

            // GIVEN a freshly loaded document with a custom profile
            var compiler = (TestingCompiler)_doc.Compiler;

            // WHEN it is compiled
            var result = _doc.Compile().Result;

            if(compiler.Invocations.Count > 1) {
                compiler.Invocations.Pop();
            }

            // THEN we find the correct value of the compiler and arguments in the invocation
            Assert.AreEqual(_doc.Settings.CurrentProfile.CompilerInfo.Compiler, compiler.Invocations.Peek().Item1);
            Assert.AreEqual(_doc.Settings.GetCompilerArguments(_doc.Sources, _doc.Settings.StandardIntermediatePath, compiler.DetectToolchain()), compiler.Invocations.Peek().Item2);
            Assert.AreNotEqual(AltCompiler, compiler.Invocations.Peek().Item1);
            StringAssert.DoesNotContain(AltArg1, compiler.Invocations.Peek().Item2);
            StringAssert.DoesNotContain(AltArg2, compiler.Invocations.Peek().Item2);
            StringAssert.DoesNotContain(AltArg3, compiler.Invocations.Peek().Item2);

            compiler.Invocations.Clear();
            compiler = (TestingCompiler)_external.Compiler;

            if(compiler.Invocations.Count > 1) {
                compiler.Invocations.Pop();
            }

            // AND we find the correct value for the child document
            Assert.AreNotEqual(_external.Settings.CurrentProfile.CompilerInfo.Compiler, compiler.Invocations.Peek().Item1);
            Assert.AreEqual(_external.Settings.GetCompilerArguments(_external.Sources, _external.Settings.StandardIntermediatePath, compiler.DetectToolchain()), compiler.Invocations.Peek().Item2);
            Assert.AreNotEqual(AltCompiler, compiler.Invocations.Peek().Item1);
            StringAssert.DoesNotContain(AltArg1, compiler.Invocations.Peek().Item2);
            StringAssert.DoesNotContain(AltArg2, compiler.Invocations.Peek().Item2);
            StringAssert.DoesNotContain(AltArg3, compiler.Invocations.Peek().Item2);

            compiler.Invocations.Clear();

            _doc.Settings.CurrentProfile = oldProfile;
            _external.Settings.CurrentProfile = oldProfile2;
            if(_external.Settings.Profiles.Contains(_altProfile2)) {
                _external.Settings.Profiles.Remove(_altProfile2);
            }
        }
    }
}
