﻿//
//  TestProfiles.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2017-11-06.
//

using NUnit.Framework;

using Petri.Application.CLI;

namespace Petri.Test.Application.Compiler
{
    [TestFixture("CBasic")]
    [TestFixture("CppBasic")]
    [TestFixture("CSharpBasic")]
    [TestFixture("PythonBasic")]
    public class TestProfiles
    {
        HeadlessDocument _doc;
        Petri.Application.DocumentSettingsProfile _altProfile;
        static readonly string AltCompiler = "This is Sparta.exe";
        static readonly string AltArg1 = "nananana";
        static readonly string AltArg2 = "-std=cpp42";

        public TestProfiles(string path)
        {
            _doc = new TestingDocument("../../../Examples/" + path + ".petri");
            _doc.Compiler = new TestingCompiler(_doc, false);
            foreach(var doc in _doc.AllExternalDocuments) {
                doc.Compiler = new TestingCompiler(doc, false);
            }

            _altProfile = Petri.Application.DocumentSettingsProfile.GetDefaultProfile(_doc.Settings.Language);
            _altProfile.Name = "Alt";
            _altProfile.CompilerInfo.Compiler = AltCompiler;
            _altProfile.CompilerInfo.CustomCompilerFlags.Add(AltArg1);
            _altProfile.CompilerInfo.CustomCompilerFlags.Add(AltArg2);
            _altProfile.CompilerInfo.CustomLinkerFlags.Add(AltArg1 + "bla");
            _altProfile.CompilerInfo.CustomLinkerFlags.Add(AltArg2 + "bla");
            _doc.Settings.Profiles.Add(_altProfile);
        }

        [TearDown]
        public void TearDown()
        {
            string[] args = { "--clean", _doc.Path };

            CompilerUtility.InvokeCompiler(args);
        }

        [Test]
        public void TestDefaultCompilerArguments()
        {
            // GIVEN a freshly loaded document
            var compiler = (TestingCompiler)_doc.Compiler;

            // WHEN it is compiled
            var result = _doc.Compile().Result;

            if(compiler.Invocations.Count > 1) {
                // THEN we find the correct value of the archiver and arguments in the invocation
                Assert.AreEqual(_doc.Settings.Archiver, compiler.Invocations.Peek().Item1);
                Assert.AreEqual(_doc.Settings.GetArchiverArguments(_doc.Settings.StandardIntermediatePath, _doc.Settings.StandardLibPath, compiler.DetectToolchain()), compiler.Invocations.Peek().Item2);
                compiler.Invocations.Pop();
            }

            // THEN we find the correct value of the compiler and arguments in the invocation
            Assert.AreEqual(_doc.Settings.CurrentProfile.CompilerInfo.Compiler, compiler.Invocations.Peek().Item1);
            Assert.AreEqual(_doc.Settings.GetCompilerArguments(_doc.Sources, _doc.Settings.StandardIntermediatePath, compiler.DetectToolchain()), compiler.Invocations.Peek().Item2);

            compiler.Invocations.Clear();
        }

        [TestCase("gcc")]
        [TestCase("toto.jpg")]
        [TestCase("Bla bla.exe")]
        public void TestCustomCompiler(string cc)
        {
            // GIVEN a freshly loaded document with a custom compiler
            var compiler = (TestingCompiler)_doc.Compiler;
            _doc.Settings.CurrentProfile.CompilerInfo.Compiler = cc;

            // WHEN it is compiled
            var result = _doc.Compile().Result;

            if(compiler.Invocations.Count > 1) {
                // THEN we find the correct value of the archiver in the invocation
                Assert.AreEqual(_doc.Settings.Archiver, compiler.Invocations.Peek().Item1);
                StringAssert.Contains(cc, compiler.Invocations.Peek().Item1);
                compiler.Invocations.Pop();
            }

            // THEN we find the correct value of the compiler in the invocation
            Assert.AreEqual(_doc.Settings.CurrentProfile.CompilerInfo.Compiler, compiler.Invocations.Peek().Item1);
            StringAssert.Contains(cc, compiler.Invocations.Peek().Item1);

            compiler.Invocations.Clear();
        }

        [TestCase("--this is -a --test")]
        [TestCase("-abcdefghijklmnopqrstuvwxyz")]
        [TestCase("012345678")]
        public void TestCustomCompilerArguments(string args)
        {
            // GIVEN a freshly loaded document with custom compiler arguments
            var compiler = (TestingCompiler)_doc.Compiler;
            _doc.Settings.CurrentProfile.CompilerInfo.CustomCompilerFlags.Clear();
            _doc.Settings.CurrentProfile.CompilerInfo.CustomCompilerFlags.AddRange(args.Split(' '));

            // WHEN it is compiled
            var result = _doc.Compile().Result;

            if(compiler.Invocations.Count > 1) {
                compiler.Invocations.Pop();
            }

            // THEN we find the correct value of the compiler and arguments in the invocation
            Assert.AreEqual(_doc.Settings.CurrentProfile.CompilerInfo.Compiler, compiler.Invocations.Peek().Item1);
            Assert.AreEqual(_doc.Settings.GetCompilerArguments(_doc.Sources, _doc.Settings.StandardIntermediatePath, compiler.DetectToolchain()), compiler.Invocations.Peek().Item2);
            StringAssert.Contains(args, compiler.Invocations.Peek().Item2);

            compiler.Invocations.Clear();
        }

        [TestCase("--this is -a --test")]
        [TestCase("-abcdefghijklmnopqrstuvwxyz")]
        [TestCase("012345678")]
        public void TestCustomLinkerArguments(string args)
        {
            // GIVEN a freshly loaded document with custom compiler arguments
            var compiler = (TestingCompiler)_doc.Compiler;
            _doc.Settings.CurrentProfile.CompilerInfo.CustomLinkerFlags.Clear();
            _doc.Settings.CurrentProfile.CompilerInfo.CustomLinkerFlags.AddRange(args.Split(' '));

            // WHEN it is compiled
            var result = _doc.Compile().Result;

            if(compiler.Invocations.Count > 1) {
                // THEN we find the correct value of the compiler and arguments in the invocation
                Assert.AreEqual(_doc.Settings.Archiver, compiler.Invocations.Peek().Item1);
                Assert.AreEqual(_doc.Settings.GetArchiverArguments(_doc.Settings.StandardIntermediatePath, _doc.Settings.StandardLibPath, compiler.DetectToolchain()), compiler.Invocations.Peek().Item2);
                StringAssert.Contains(args, compiler.Invocations.Peek().Item2);
                compiler.Invocations.Pop();
            }

            compiler.Invocations.Clear();
        }

        [Test]
        public void TestAltProfile()
        {
            var oldProfile = _doc.Settings.CurrentProfile;
            _doc.Settings.CurrentProfile = _altProfile;

            // GIVEN a freshly loaded document with a custom profile
            var compiler = (TestingCompiler)_doc.Compiler;

            // WHEN it is compiled
            var result = _doc.Compile().Result;

            if(compiler.Invocations.Count > 1) {
                // THEN we find the correct value of the compiler and arguments in the invocation
                Assert.AreEqual(_doc.Settings.Archiver, compiler.Invocations.Peek().Item1);
                Assert.AreEqual(_doc.Settings.GetArchiverArguments(_doc.Settings.StandardIntermediatePath, _doc.Settings.StandardLibPath, compiler.DetectToolchain()), compiler.Invocations.Peek().Item2);
                Assert.AreEqual(AltCompiler, compiler.Invocations.Peek().Item1);
                StringAssert.Contains(AltArg1 + "bla", compiler.Invocations.Peek().Item2);
                StringAssert.Contains(AltArg2 + "bla", compiler.Invocations.Peek().Item2);
                compiler.Invocations.Pop();
            }

            // THEN we find the correct value of the compiler and arguments in the invocation
            Assert.AreEqual(_doc.Settings.CurrentProfile.CompilerInfo.Compiler, compiler.Invocations.Peek().Item1);
            Assert.AreEqual(_doc.Settings.GetCompilerArguments(_doc.Sources, _doc.Settings.StandardIntermediatePath, compiler.DetectToolchain()), compiler.Invocations.Peek().Item2);
            Assert.AreEqual(AltCompiler, compiler.Invocations.Peek().Item1);
            StringAssert.Contains(AltArg1, compiler.Invocations.Peek().Item2);
            StringAssert.Contains(AltArg2, compiler.Invocations.Peek().Item2);

            compiler.Invocations.Clear();

            _doc.Settings.CurrentProfile = oldProfile;
        }
    }
}
