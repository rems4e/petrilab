﻿//
//  TestingCompiler.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-01.
//

using System;
using System.Collections.Generic;

namespace Petri.Test
{
    public class TestingCompiler : Petri.Application.Compiler
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Petri.Test.TestingCompiler"/> class.
        /// </summary>
        /// <param name="doc">Document.</param>
        /// <param name="actualInvocation">If set to <c>true</c> the program invocations will be made.</param>
        public TestingCompiler(Petri.Application.Document doc, bool actualInvocation) : base(doc)
        {
            _actualInvocation = actualInvocation;
        }

        protected override Tuple<bool, string> InvokeExternal(string program, string args, string wd, Petri.Application.IOutput verboseOutput)
        {
            Invocations.Push(Tuple.Create(program, args));

            if(_actualInvocation) {
                return base.InvokeExternal(program, args, wd, verboseOutput);
            }

            return Tuple.Create(true, "");
        }

        public override Toolchain DetectToolchain()
        {
            var result = base.DetectToolchain();
            Invocations.Pop();
            return result;
        }


        /// <summary>
        /// Gets the invocations made by the compiler, last invocation at the stop of the stack.
        /// The first member of the tuple is the program, the second is the program's arguments.
        /// </summary>
        /// <value>The last invocations.</value>
        public Stack<Tuple<string, string>> Invocations {
            get;
        } = new Stack<Tuple<string, string>>();

        bool _actualInvocation;
    }
}
