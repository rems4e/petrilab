//
//  CompilerUtility.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-02.
//
namespace Petri.Test
{
    public static class CompilerUtility
    {
        public static int InvokeCompiler(string[] args)
        {
            return InvokeCompiler(args, new Petri.Application.ConsoleOutput(false));
        }

        public static int InvokeCompiler(string[] args, Petri.Application.IOutput output)
        {
            return Petri.Application.CLI.CLIApplication.RawMain(output, args);
        }

        public static int InvokeCompiler(string[] args, out string stdout, out string stderr)
        {
            return TestingUtility.InvokeAndRedirectOutput(() => {
                return InvokeCompiler(args);
            }, out stdout, out stderr);
        }
    }
}

