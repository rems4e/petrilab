﻿//
//  TestingDocument.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-01.
//

using System;

using NUnit.Framework;

namespace Petri.Test
{
    public delegate void VoidStringDel(string arg);

    public class TestingDocument : Petri.Application.CLI.HeadlessDocument
    {
        public TestingDocument(string path,
                               Petri.Application.IOutput output = null) : base(path)
        {
            Init(output);
        }

        public TestingDocument(Petri.Code.Language language,
                               Petri.Application.IOutput output = null) : base(language)
        {
            Init(output);
        }

        void Init(Petri.Application.IOutput output)
        {
            if(output != null) {
                Output = output;
            }

            NotifyErrorDel = (string message) => {
                Console.Error.WriteLine("{0}", message);
                Assert.Fail("Error: " + message);
            };
        }

        public VoidStringDel NotifyErrorDel {
            get;
            set;
        }

        public override void NotifyError(string error)
        {
            NotifyErrorDel?.Invoke(error);
        }
    }
}
