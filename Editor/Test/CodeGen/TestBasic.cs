﻿//
//  TestBasic.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-10-18.
//

using NUnit.Framework;

using HeadlessDocument = Petri.Application.CLI.HeadlessDocument;

namespace Petri.Test.CodeGen
{
    [TestFixture("Embedded/EmbeddedC")]
    [TestFixture("Embedded/EmbeddedCLocalVariables")]
    [TestFixture("CBasic")]
    [TestFixture("CppBasic")]
    [TestFixture("CSharpBasic")]
    [TestFixture("PythonBasic")]
    public class TestBasic
    {
        protected HeadlessDocument _document;
        protected Petri.CodeGen.PetriGen _generator;
        protected string _subpath;

        public TestBasic(string subpath)
        {
            _subpath = subpath;
        }

        protected void ResetFlatPetriNet()
        {
            _document.InvalidateFlatPetriNet();
        }

        protected void ResetGenerator()
        {
            _generator = Petri.CodeGen.PetriGen.PetriGenForDoc(_document);
        }

        [SetUp]
        public virtual void SetUp()
        {
            _document = new HeadlessDocument("../../../Examples/" + _subpath + ".petri");
            ResetGenerator();
        }

        [Test]
        public void TestHasVersionNumber()
        {
            var generated = _generator.GetGeneratedCode();

            StringAssert.Contains(Petri.Application.Application.Version, generated);
        }

        [Test]
        public void TestHasWebSite()
        {
            var generated = _generator.GetGeneratedCode();

            StringAssert.Contains(Petri.Application.Application.WebSiteInfo, generated);
        }
    }
}
