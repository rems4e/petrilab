﻿//
//  TestPattern.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-10-18.
//

using NUnit.Framework;

using Petri.Code;
using Petri.Model;

using HeadlessDocument = Petri.Application.CLI.HeadlessDocument;

namespace Petri.Test.CodeGen
{
    [TestFixture(Language.C)]
    [TestFixture(Language.Cpp)]
    [TestFixture(Language.CSharp)]
    [TestFixture(Language.EmbeddedC)]
    [TestFixture(Language.Python)]
    public class TestPattern
    {
        HeadlessDocument _document;
        readonly string _pattern = "\"abcdefghijklmnopqrstuvwxyz\"";
        Language _language;

        public TestPattern(Language l)
        {
            _language = l;
        }

        [SetUp]
        public void FixtureSetUp()
        {
            _document = new HeadlessDocument(_language);
        }

        [Test]
        public void TestFindPatternInGeneratedCodeOfReachableState()
        {
            // GIVEN a petri net with a single state that is reachable and whose action contains a recognizable code pattern
            var state = new Action(_document.EntityFactory, _document.PetriNet, true);
            _document.PetriNet.AddState(state);
            state.Invocation = new WrapperFunctionInvocation(_document.Settings.Enum.Type, _document.EntityFactory.CreateExpressionFromString(_pattern));
            Assert.True(state.IsReachable);

            // WHEN we generate the source code for this petri net
            var generator = Petri.CodeGen.PetriGen.PetriGenForDoc(_document);
            var generated = generator.GetGeneratedCode();

            string pattern = _pattern;
            if(_language == Language.Python) {
                pattern = _pattern.Escape().Escape();
            }

            // THEN the generated source code contains the pattern
            StringAssert.Contains(pattern, generated);
        }

        [Test]
        public void TestDoesNotFindPatternInGeneratedCodeOfNotReachableState()
        {
            // GIVEN a petri net with a single state that is not reachable and whose action contains a recognizable code pattern
            var state = new Action(_document.EntityFactory, _document.PetriNet, false);
            _document.PetriNet.AddState(state);
            state.Invocation = new WrapperFunctionInvocation(_document.Settings.Enum.Type, _document.EntityFactory.CreateExpressionFromString(_pattern));
            Assert.False(state.IsReachable);

            // WHEN we generate the source code for this petri net
            var generator = Petri.CodeGen.PetriGen.PetriGenForDoc(_document);
            var generated = generator.GetGeneratedCode();

            string pattern = _pattern;
            if(_language == Language.Python) {
                pattern = _pattern.Escape().Escape();
            }

            // THEN the generated source code does not contains the pattern
            StringAssert.DoesNotContain(pattern, generated);
        }
    }
}
