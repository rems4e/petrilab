﻿//
//  TestConcurrentPetriGen.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2017-07-03.
//

using System;
using System.Collections.Generic;
using System.Linq;

using NUnit.Framework;

namespace Petri.Test.CodeGen
{
    [TestFixture("CCountTo10plusN")]
    [TestFixture("CppCountTo10plusN")]
    [TestFixture("CSharpCountTo10plusN")]
    [TestFixture("PythonCountTo10plusN")]
    [TestFixture("InnerPetriNet/CInnerPetriNet")]
    [TestFixture("InnerPetriNet/CppInnerPetriNet")]
    [TestFixture("InnerPetriNet/CSharpInnerPetriNet")]
    [TestFixture("InnerPetriNet/PythonInnerPetriNet")]
    [TestFixture("ExternalPetriNet/CExternal")]
    [TestFixture("ExternalPetriNet/CppExternal")]
    [TestFixture("ExternalPetriNet/CSharpExternal")]
    [TestFixture("ExternalPetriNet/PythonExternal")]
    public class TestConcurrentPetriGen : TestBasic
    {
        new Petri.CodeGen.CLikePetriGen _generator;

        public TestConcurrentPetriGen(string subpath) : base(subpath)
        {
        }

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();
            _generator = (Petri.CodeGen.CLikePetriGen)base._generator;
        }

        [Test]
        public void TestHeaderHasVersionNumber()
        {
            // GIVEN the petri net's generated code's header
            var generated = _generator.GetGeneratedHeader();

            // THEN it contains the application's version
            StringAssert.Contains(Petri.Application.Application.Version, generated);
        }

        [Test]
        public void TestHeaderHasWebSite()
        {
            // GIVEN the petri net's generated code's header
            var generated = _generator.GetGeneratedHeader();

            // Then it contains the application's version
            StringAssert.Contains(Petri.Application.Application.WebSiteInfo, generated);
        }

        [Test]
        public void TestHasHash()
        {
            // GIVEN the petri net's generated code
            var generated = _generator.GetGeneratedCode();

            // Then it contains the petri net's hash code
            StringAssert.Contains(_document.Hash, generated);
            Assert.AreEqual(_document.Hash, _generator.GetHash());
        }

        [Test]
        public void TestHashUsesName()
        {
            // GIVEN the petri net's generated code's current hash
            string lastHash = _document.Hash;
            ResetGenerator();

            var newName = CodeUtility.RandomIdentifier();

            if(newName == _document.Settings.Name) {
                Assert.Inconclusive("Not lucky today…");
            }

            // WHEN we change the petri net's name
            _document.Settings.Name = newName;
            _document.InvalidateHash();

            var generated = _generator.GetGeneratedCode();

            // THEN the hash changes
            Assert.AreNotEqual(lastHash, _document.Hash);

            StringAssert.Contains(_document.Hash, generated);
            StringAssert.DoesNotContain(lastHash, generated);
        }

        [Test]
        public void TestHashDoesntUsePort()
        {
            // GIVEN the petri net's generated code's current hash
            string lastHash = _document.Hash;
            ResetGenerator();

            var newPort = TestingUtility.RandomBetween(0, 65535);

            if(newPort == _document.Settings.CurrentProfile.DebugPort) {
                Assert.Inconclusive("Not lucky today…");
            }

            // WHEN we change the petri net's debug port
            _document.Settings.CurrentProfile.DebugPort = (UInt16)newPort;

            var generated = _generator.GetGeneratedCode();

            // THEN the hash changes
            Assert.AreEqual(lastHash, _document.Hash);

            StringAssert.Contains(_document.Hash, generated);
            StringAssert.Contains(lastHash, generated);
        }

        [Test, Repeat(10)]
        public void TestChangePropertyMeansHashChange()
        {
            // GIVEN the petri net's generated code's current hash
            string lastHash = _document.Hash;
            _document.InvalidateHash();

            var entities = _document.PetriNet.BuildLocalEntitiesList();
            entities.RemoveAll((e) => !_document.IsReachable(e) || !(e.GetType() == typeof(Petri.Model.Action) || e is Petri.Model.Transition));

            var toChangeCount = TestingUtility.RandomBetween(1, entities.Count);

            var toChange = new List<Petri.Model.Entity>();
            while(toChangeCount > 0) {
                --toChangeCount;
                var i = TestingUtility.RandomBetween(0, entities.Count);
                toChange.Add(entities[i]);
                entities.RemoveAt(i);
            }

            CollectionAssert.IsNotEmpty(toChange);

            // WHEN we change random properties on random entities
            foreach(var e in toChange) {
                var toDo = TestingUtility.RandomBetween(0, 1);
                switch(toDo) {
                case 0:
                    e.Name = CodeUtility.RandomIdentifier();
                    break;
                case 1:
                    var newCode = CodeUtility.RandomLiteral(CodeUtility.LiteralType.String, _document.Settings.Language).Item1;
                    if(e is Petri.Model.Action) {
                        ((Petri.Model.Action)e).Invocation = new Petri.Code.WrapperFunctionInvocation(
                            new Petri.Code.Type(_document.Settings.Language, "void"),
                            Petri.Code.LiteralExpression.CreateFromString(newCode, _document.Settings.Language)
                        );
                    } else if(e is Petri.Model.Transition) {
                        ((Petri.Model.Transition)e).Condition = new Petri.Code.WrapperFunctionInvocation(
                            new Petri.Code.Type(_document.Settings.Language, "void"),
                            Petri.Code.LiteralExpression.CreateFromString(newCode, _document.Settings.Language)
                        );
                    }
                    break;
                }
            }

            Model.ModelUtility.AssertWellFormed(_document.PetriNet);

            ResetFlatPetriNet();
            ResetGenerator();
            var generated = _generator.GetGeneratedCode();

            // THEN the hash changes
            Assert.AreNotEqual(lastHash, _document.Hash);

            StringAssert.Contains(_document.Hash, generated);
            StringAssert.DoesNotContain(lastHash, generated);
        }

        [Test]
        public void TestHashTwiceIsSame([Values(true, false)] bool reset)
        {
            // GIVEN the petri net's generated code's current hash
            string lastHash = _document.Hash;
            for(int i = 0; i < 2; ++i) {
                // WHEN we clear the generated code and generate it again
                if(reset) {
                    ResetGenerator();
                }
                var generated = _generator.GetGeneratedCode();

                Assert.AreEqual(lastHash, _document.Hash);
                Assert.AreEqual(lastHash, _generator.GetHash());

                // THEN the next generation's hash is the same as the first and the previous
                StringAssert.Contains(_document.Hash, generated);
                StringAssert.Contains(lastHash, generated);
            }
        }

        [Test]
        public void TestNamespaceHeader()
        {
            // GIVEN the petri net's generated code's header
            var generated = _generator.GetGeneratedHeader();

            // THEN it contains the petri net's name
            switch(_document.Settings.Language) {
            case Petri.Code.Language.C:
            case Petri.Code.Language.Python:
                StringAssert.Contains(_document.Settings.Name + "_", generated);
                break;
            case Petri.Code.Language.Cpp:
                StringAssert.Contains("namespace " + _document.Settings.Name, generated);
                break;
            case Petri.Code.Language.CSharp:
                StringAssert.Contains("class " + _document.Settings.Name, generated);
                break;
            default:
                Assert.Fail(string.Format("Update the test to handle language {0}", _document.Settings.Language));
                break;
            }
        }

        [Test]
        public void TestWithoutMaxConcurrency()
        {
            // GIVEN a petri net with no max concurrency level set
            if(_document.Settings.CurrentProfile.MaxConcurrency.HasValue) {
                Assert.Inconclusive("This test is not intended to handle max concurrency values.");
            }

            // WHEN we generate its code
            var generated = _generator.GetGeneratedCode();

            // THEN we find no trace of max concurrency setting in the code
            StringAssert.DoesNotContain("axConcurrency", generated);
        }

        [Test]
        public void TestWithMaxConcurrency()
        {
            // GIVEN a petri net with a max concurrency level set
            int max = TestingUtility.RandomBetween(0, 1000);
            _document.Settings.CurrentProfile.MaxConcurrency = (UInt64)max;

            // WHEN we generate its code
            var generated = _generator.GetGeneratedCode();

            // THEN it contains mentions of the max concurrency setting
            StringAssert.Contains("axConcurrency", generated); // just to fool-proof the test with no max concurrency
            switch(_document.Settings.Language) {
            case Petri.Code.Language.C:
            case Petri.Code.Language.Python:
                StringAssert.Contains("PetriNet_setMaxConcurrency(petriNet, " + max + ")", generated);
                break;
            case Petri.Code.Language.Cpp:
                StringAssert.Contains("petriNet->setMaxConcurrency(" + max + ")", generated);
                break;
            case Petri.Code.Language.CSharp:
                StringAssert.Contains("petriNet.MaxConcurrency = " + max, generated);
                break;
            default:
                Assert.Fail(string.Format("Update the test to handle language {0}", _document.Settings.Language));
                break;
            }
        }

        [Test]
        public void TestLogVerbosity()
        {
            // GIVEN a petri net with a log level set
            int max = (int)Enum.GetValues(typeof(Petri.Runtime.PetriNet.LogVerbosity)).Cast<Petri.Runtime.PetriNet.LogVerbosity>().Last();
            var verbosity = (Petri.Runtime.PetriNet.LogVerbosity)TestingUtility.RandomBetween(0, max);
            _document.Settings.DefaultExecutionVerbosity = verbosity;

            // WHEN we generate its code
            var generated = _generator.GetGeneratedCode();

            // THEN it contains mentions of the verbosity setting
            StringAssert.Contains("LogVerbosity", generated);
            foreach(Petri.Runtime.PetriNet.LogVerbosity v in Enum.GetValues(typeof(Petri.Runtime.PetriNet.LogVerbosity))) {
                if(verbosity.HasFlag(v)) {
                    StringAssert.Contains(v.ToString(), generated);
                }
            }
        }

        [Test]
        public void TestDebugPort()
        {
            int port = TestingUtility.RandomBetween(0, 65535);

            // GIVEN a petri net with a random debugging TCP port
            _document.Settings.CurrentProfile.DebugPort = (UInt16)port;

            // WHEN we generate the code
            var generated = _generator.GetGeneratedCode();

            // THEN it mentions the TCP port
            StringAssert.Contains(port.ToString(), generated);
        }

        [Test]
        public void TestHasVariables()
        {
            // GIVEN the petri net's generated code
            var generated = _generator.GetGeneratedCode();
            var variables = _document.PetriNet.Variables;

            // WHEN we check if it contains the variables
            foreach(var var in variables) {
                // THEN we find their name and code
                StringAssert.Contains(var.Key.MakeUserReadable(), generated);
                StringAssert.Contains(var.Key.MakeCode(), generated);
            }
        }

        [Test]
        public void TestHeaderHasParameters()
        {
            // GIVEN the petri net's generated code's header
            var generated = _generator.GetGeneratedHeader();

            // WHEN we check if it contains the parameters
            foreach(var param in _document.PetriNet.Parameters) {
                // THEN we find them
                StringAssert.Contains(param.Expression, generated);
            }
        }

        [Test]
        public void TestHasParameters()
        {
            // GIVEN the petri net's generated code
            var generated = _generator.GetGeneratedCode();

            // WHEN we check if it contains the parameters
            foreach(var param in _document.PetriNet.Parameters) {
                // THEN we find their name, code enumerator and default value
                StringAssert.Contains(param.MakeUserReadable(), generated);
                StringAssert.Contains(param.MakeCode(), generated);
                StringAssert.Contains(param.EnumeratorName, generated);

                StringAssert.Contains(_document.PetriNet.Variables[param].ToString(), generated);
            }
        }

        [Test]
        public void TestHasVariablesEnumInRightOrder()
        {
            // GIVEN the petri net's generated code
            var generated = _generator.GetGeneratedCode();
            int i = 0;


            // WHEN we check the order the variables are declared in the code
            foreach(var var in _document.PetriNet.Parameters) {
                // THEN it's the same as expected
                StringAssert.Contains(var.Expression + " = " + i++, generated);
            }

            foreach(var var in from v in _document.PetriNet.Variables where !_document.PetriNet.Parameters.Contains(v.Key) select v) {
                // THEN it's the same as expected
                StringAssert.Contains(var.Key.Expression + " = " + i++, generated);
            }
        }

        [Test]
        public void TestHasParametersInVariablesEnumInRightOrder()
        {
            // GIVEN the petri net's generated code
            var generated = _generator.GetGeneratedCode();

            int i = 0;

            // WHEN we check the order the parameters are declared in the code
            foreach(var param in _document.PetriNet.Parameters) {
                // THEN it's the same as expected, and before the other variables
                StringAssert.Contains(param.Expression + " = " + i++, generated);
            }
        }

        [Test]
        public void TestHasExternalPetriNets()
        {
            // GIVEN the petri net's generated code
            var generated = _generator.GetGeneratedCode();

            // WHEN we check the presence of external petri nets' fill code
            foreach(var pn in _document.FirstLevelExternalPetriNets) {
                // THEN we find it
                StringAssert.IsMatch(string.Format(@"{0}[_.][Ff]ill\(", pn.Name), generated);
            }
        }

        [Test]
        public void TestEntities()
        {
            // GIVEN the petri net's generated code and entities list
            var generated = _generator.GetGeneratedCode();
            var entities = _document.PetriNet.BuildLocalEntitiesList();

            foreach(var e in entities) {
                // WHEN we check for presence of reachable entities' properties in the generated code
                if(_document.IsReachable(e)) {
                    // THEN we find them
                    StringAssert.Contains(e.ID.ToString(), generated);
                    StringAssert.Contains(e.Name, generated);
                    if(e is Petri.Model.Action) {
                        var invocation = ((Petri.Model.Action)e).Invocation.MakeCode();
                        if(_document.Settings.Language == Petri.Code.Language.Python) {
                            invocation = invocation.Escape();
                        }
                        StringAssert.Contains(invocation, generated);
                    } else if(e is Petri.Model.Transition) {
                        var condition = ((Petri.Model.Transition)e).Condition.MakeCode();
                        if(_document.Settings.Language == Petri.Code.Language.Python) {
                            condition = condition.Escape();
                        }
                        StringAssert.Contains(condition, generated);
                    }
                }
            }
        }
    }
}
