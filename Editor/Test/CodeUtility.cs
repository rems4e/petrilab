﻿//
//  CodeUtility.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2015-12-25.
//

using System;
using System.Linq;

using Petri.Code;

namespace Petri.Test
{
    public static class CodeUtility
    {
        static Random _random = new Random();

        public enum LiteralType
        {
            Char,
            String,
            Int,
            UInt,
            ULong,
            ULongLong,
            Float,
            Double,
            LongDouble
        }

        /// <summary>
        /// Gets a random ASCII char.
        /// If the computed character needs to be escaped to be a valid C literal char, it is esccaped by a backslash,
        /// like in " -> \".
        /// </summary>
        /// <returns>The char.</returns>
        /// <param name="inString">If set to <c>true</c> in string.</param>
        public static string RandomChar(bool inString)
        {
            string c = new string((char)_random.Next(32, 128), 1);
            if(c == @"\" || ((inString && c == "\"") || (!inString && c == "'"))) {
                c = @"\" + c;
            }
            return c;
        }

        /// <summary>
        /// Gets a random literal of the provided type in the given language.
        /// A string or a char with content that needs to be escaped to be a valid C string is escapaed apropriately with
        /// backslashes.
        /// </summary>
        /// <returns>The literal.</returns>
        /// <param name="type">Type.</param>
        /// <param name="lang">Lang.</param>
        public static Tuple<string, LiteralType> RandomLiteral(LiteralType type, Language lang)
        {
            if(lang == Language.Python) {
                if(type == LiteralType.Float || type == LiteralType.LongDouble) {
                    type = LiteralType.Double;
                } else if(type == LiteralType.UInt || type == LiteralType.ULong || type == LiteralType.ULongLong) {
                    type = LiteralType.Int;
                }
            } else if(lang == Language.CSharp) {
                if(type == LiteralType.LongDouble) {
                    type = LiteralType.Double;
                } else if(type == LiteralType.ULongLong) {
                    type = LiteralType.ULong;
                }
            }
            switch(type) {
            case LiteralType.Char:
                return Tuple.Create("'" + RandomChar(false) + "'", type);
            case LiteralType.String:
                int count = _random.Next(50);
                var result = new System.Text.StringBuilder();
                for(int i = 0; i < count; ++i) {
                    result.Append(RandomChar(true));
                }

                return Tuple.Create('"' + result.ToString() + '"', type);

            case LiteralType.UInt:
                return Tuple.Create(_random.Next(1 << 16).ToString() + "U", type);
            case LiteralType.ULong:
                return Tuple.Create(_random.Next(1 << 16).ToString() + "UL", type);
            case LiteralType.ULongLong:
                return Tuple.Create(_random.Next(1 << 16).ToString() + "ULL", type);

            case LiteralType.Float:
                return Tuple.Create((_random.NextDouble() * _random.Next(1, 30)).ToString(System.Globalization.CultureInfo.InvariantCulture) + "F", type);
            case LiteralType.Double:
                return Tuple.Create((_random.NextDouble() * _random.Next(1, 30)).ToString(System.Globalization.CultureInfo.InvariantCulture), type);
            case LiteralType.LongDouble:
                return Tuple.Create((_random.NextDouble() * _random.Next(1, 30)).ToString(System.Globalization.CultureInfo.InvariantCulture) + "L", type);
            }

            return Tuple.Create(_random.Next().ToString(), type);
        }

        /// <summary>
        /// Gets a random literal in the given language. <see cref="Petri.Test.CodeUtility.RandomLiteral(LiteralType, Petri.Code.Language)"/>
        /// </summary>
        /// <returns>The literal.</returns>
        /// <param name="lang">Lang.</param>
        public static Tuple<string, LiteralType> RandomLiteral(Language lang)
        {
            var types = System.Enum.GetValues(typeof(LiteralType));
            var type = (LiteralType)types.GetValue(_random.Next(types.Length));
            var lit = RandomLiteral(type, lang);

            return lit;
        }

        /// <summary>
        /// Interprets a literal and returns a string containing its value.
        /// e.g 3U will be 3, 3.054f will be 3.054, 'test with \"a\" quote' will be 'test with "a" quote'
        /// (depends on the literal type, of course).
        /// </summary>
        /// <returns>The value.</returns>
        /// <param name="tup">Tup.</param>
        public static string LiteralValue(Tuple<string, LiteralType> tup)
        {
            switch(tup.Item2) {
            case LiteralType.Char:
            case LiteralType.String: {
                    string val = tup.Item1.Substring(1, tup.Item1.Length - 2).Replace("\\\\", "\\");
                    if(tup.Item2 == LiteralType.String) {
                        return val.Replace("\\\"", "\"");
                    } else {
                        return val.Replace("\\'", "'");
                    }
                }
            case LiteralType.Int:
                return tup.Item1;
            case LiteralType.UInt:
                return tup.Item1.Substring(0, tup.Item1.Length - 1);
            case LiteralType.Float:
                return tup.Item1.Substring(0, tup.Item1.Length - 1);
            case LiteralType.LongDouble:
                return tup.Item1.Substring(0, tup.Item1.Length - 1);
            case LiteralType.Double:
                return tup.Item1.Substring(0, tup.Item1.Length - 0);
            case LiteralType.ULong:
                return tup.Item1.Substring(0, tup.Item1.Length - 2);
            case LiteralType.ULongLong:
                return tup.Item1.Substring(0, tup.Item1.Length - 3);
            }

            throw new Exception("CodeUtility.LiteralValue: Should not get there!");
        }

        /// <summary>
        /// Gets the printf-like formatter for a given type.
        /// </summary>
        /// <returns>The formatter for type.</returns>
        /// <param name="type">Type.</param>
        public static string CFormatterForType(LiteralType type)
        {
            switch(type) {
            case LiteralType.Char:
                return "%c";
            case LiteralType.String:
                return "%s";
            case LiteralType.Int:
                return "%d";
            case LiteralType.UInt:
                return "%u";
            case LiteralType.Double:
            case LiteralType.Float:
                return "%f";
            case LiteralType.LongDouble:
                return "%Lf";
            case LiteralType.ULong:
                return "%lu";
            case LiteralType.ULongLong:
                return "%llu";
            }

            throw new Exception("CodeUtility.CFormatterForType: Should not get there!");
        }

        /// <summary>
        /// Gets a random identifier that can be used as a variable name, for instance.
        /// </summary>
        /// <returns>The identifier.</returns>
        public static string RandomIdentifier()
        {
            int length = _random.Next(1, 50);
            string domain = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_";
            var result = new string(Enumerable.Repeat(domain, length).Select(s => s[_random.Next(s.Length)]).ToArray());

            if(Char.IsDigit(result[0])) {
                result = domain[_random.Next(52)] + result.Substring(1);
            }

            return result;
        }

        /// <summary>
        /// Gets a random language among the supported set.
        /// </summary>
        /// <returns>The language.</returns>
        public static Language RandomLanguage()
        {
            var languages = System.Enum.GetValues(typeof(Language));
            return (Language)languages.GetValue(_random.Next(languages.Length));
        }
    }
}

