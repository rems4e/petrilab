﻿//
//  TestFunctions.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2015-12-25.
//

using System.Collections.Generic;

using NUnit.Framework;

using Petri.Code;

namespace Petri.Test.Code
{
    [TestFixture]
    public class TestFunctions
    {
        [Test]
        public void TestFreeFunction1([Values(Language.Cpp, Language.C, Language.CSharp, Language.Python)] Language lang)
        {
            // GIVEN a function invocation string
            // WHEN an invocation is created from the string
            var e = Expression.CreateFromString<FunctionInvocation>("f()", lang);

            // THEN the function is created with the right number of parameters, and the number of arguments passed to it is recognized.
            Assert.IsInstanceOf<FunctionInvocation>(e);
            Assert.AreEqual("f", e.Function.Name);
            Assert.AreEqual(0, e.Function.Parameters.Count);
            Assert.AreEqual(0, e.Arguments.Count);
        }

        [Test]
        public void TestFreeFunction2([Values(Language.Cpp, Language.C, Language.CSharp, Language.Python)] Language lang)
        {
            // GIVEN a function invocation string
            // WHEN an invocation is created from the string
            var e = Expression.CreateFromString<FunctionInvocation>("f(3)", lang);

            // THEN the function is created with the right number of parameters, and the number of arguments passed to it is recognized.
            Assert.IsInstanceOf<FunctionInvocation>(e);
            Assert.AreEqual("f", e.Function.Name);
            Assert.AreEqual(1, e.Function.Parameters.Count);
            Assert.AreEqual(1, e.Arguments.Count);

            Assert.AreEqual("3", e.Arguments[0].MakeCode());
        }

        [Test]
        public void TestFreeFunction3([Values(Language.Cpp, Language.C, Language.CSharp, Language.Python)] Language lang)
        {
            // GIVEN a function invocation string
            // WHEN an invocation is created from the string
            var e = Expression.CreateFromString<FunctionInvocation>("f(   3 )", lang);

            // THEN the function is created with the right number of parameters, and the number of arguments passed to it is recognized.
            Assert.IsInstanceOf<FunctionInvocation>(e);
            Assert.AreEqual("f", e.Function.Name);
            Assert.AreEqual(1, e.Function.Parameters.Count);
            Assert.AreEqual(1, e.Arguments.Count);

            Assert.AreEqual("3", e.Arguments[0].MakeCode());
        }

        [Test]
        public void TestFreeFunction4([Values(Language.Cpp, Language.C, Language.CSharp, Language.Python)] Language lang)
        {
            // GIVEN a function invocation string
            // WHEN an invocation is created from the string
            var e = Expression.CreateFromString<FunctionInvocation>("f(1 ,  2  )", lang);

            // THEN the function is created with the right number of parameters, and the number of arguments passed to it is recognized.
            Assert.IsInstanceOf<FunctionInvocation>(e);
            Assert.AreEqual("f", e.Function.Name);
            Assert.AreEqual(2, e.Function.Parameters.Count);
            Assert.AreEqual(2, e.Arguments.Count);

            Assert.AreEqual("1", e.Arguments[0].MakeCode());
            Assert.AreEqual("2", e.Arguments[1].MakeCode());
        }

        [Test]
        public void TestFreeFunction5([Values(Language.Cpp, Language.C, Language.CSharp, Language.Python)] Language lang)
        {
            // GIVEN a function invocation string
            // WHEN an invocation is created from the string
            var e = Expression.CreateFromString<FunctionInvocation>("f ()", lang);

            // THEN the function is created with the right number of parameters, and the number of arguments passed to it is recognized.
            Assert.IsInstanceOf<FunctionInvocation>(e);
            Assert.AreEqual("f", e.Function.Name);
            Assert.AreEqual(0, e.Function.Parameters.Count);
        }

        [Test]
        public void TestFreeFunction6([Values(Language.Cpp, Language.C, Language.CSharp, Language.Python)] Language lang)
        {
            // GIVEN a function invocation string
            // WHEN an invocation is created from the string
            var e = Expression.CreateFromString<UnaryExpression>("!f()", lang);

            // THEN the function is created with the right number of parameters, and the number of arguments passed to it is recognized.

            Assert.IsInstanceOf<UnaryExpression>(e);
            Assert.AreEqual(Operator.Name.LogicalNot, e.Operator);

            var invocation = e.Expression as FunctionInvocation;
            Assert.AreEqual("f", invocation.Function.Name);
            Assert.AreEqual(0, invocation.Function.Parameters.Count);
        }

        [Test]
        public void TestFreeFunction7([Values(Language.Cpp, Language.C, Language.CSharp, Language.Python)] Language lang)
        {

            // GIVEN a function list
            var functions = new List<Function>();
            var f = new Function(new Petri.Code.Type(lang,
                                                                "void"),
                                            null,
                                            "f");
            functions.Add(f);

            // AND a function invocation string
            // WHEN an invocation is created from the string and uses the functions list
            var e = Expression.CreateFromString<FunctionInvocation>("f()",
                                                                    lang,
                                                                    functions);

            // THEN the function is created with the right number of parameters, and the number of arguments passed to it is recognized.
            Assert.IsInstanceOf<FunctionInvocation>(e);
            Assert.AreEqual("f", e.Function.Name);
            Assert.AreEqual(0, e.Function.Parameters.Count);
            Assert.AreEqual(0, e.Arguments.Count);
        }

        [Test]
        public void TestFreeFunctionWithParens([Values(Language.Cpp, Language.C, Language.CSharp, Language.Python)] Language lang)
        {
            // GIVEN a function invocation string whose function name is parenthesized
            // WHEN an invocation is created from the string
            var e = Expression.CreateFromString<FunctionInvocation>("(f)(1)", lang);

            // THEN the function is created with the right number of parameters, and the right function name and arguments are recognized.
            Assert.IsInstanceOf<FunctionInvocation>(e);
            Assert.AreEqual("(f)", e.Function.Name);
            Assert.AreEqual(1, e.Function.Parameters.Count);
            Assert.AreEqual(1, e.Arguments.Count);
            Assert.AreEqual("1", e.Arguments[0].MakeCode());
        }

        [Test]
        public void TestMethod1([Values(Language.Cpp, Language.CSharp, Language.Python)] Language lang)
        {
            var expected = "object(3).is_door_open(42)";
            var e = Expression.CreateFromString<FunctionInvocation>(expected, lang);

            Assert.AreEqual(expected, e.MakeUserReadable());
        }

        [Test]
        public void TestMethod2([Values(Language.Cpp, Language.CSharp, Language.Python)] Language lang)
        {
            var expected = "object(3).bla.is_door_open(42)";
            var e = Expression.CreateFromString<FunctionInvocation>(expected, lang);

            Assert.AreEqual(expected, e.MakeUserReadable());
        }
    }
}

