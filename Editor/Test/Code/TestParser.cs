﻿//
//  TestParser.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-02-22.
//

using NUnit.Framework;

using Petri.Code;

namespace Petri.Test.Code
{
    [TestFixture]
    public class TestParser
    {
        static void AssertIdentifier(string id)
        {
            var match = System.Text.RegularExpressions.Regex.Match(id, Parser.GetNamePattern(true));

            Assert.True(match.Success);
            Assert.AreEqual(id, match.Value);
        }

        [Test, Repeat(20)]
        public void TestRandomIdentifier()
        {
            // GIVEN a random identifier
            string identifier = CodeUtility.RandomIdentifier();

            // WHEN we check if it is actually an identifier
            // THEN the identifier is matched and its value is kept as a whole
            AssertIdentifier(identifier);
        }

        [Test]
        public void TestIdentifier1()
        {
            // GIVEN a given identifier
            string identifier = "a";

            // WHEN we check if it is actually an identifier
            // THEN the identifier is matched and its value is kept as a whole
            AssertIdentifier(identifier);
        }

        [Test]
        public void TestIdentifier2()
        {
            // GIVEN a given identifier
            string identifier = "_a";

            // WHEN we check if it is actually an identifier
            // THEN the identifier is matched and its value is kept as a whole
            AssertIdentifier(identifier);
        }

        [Test]
        public void TestIdentifier3()
        {
            // GIVEN a given identifier
            string identifier = "a3";

            // WHEN we check if it is actually an identifier
            // THEN the identifier is matched and its value is kept as a whole
            AssertIdentifier(identifier);
        }

        [Test]
        public void TestIdentifier4()
        {
            // GIVEN a given identifier
            string identifier = "_a_A4246_bfuzokze_";

            // WHEN we check if it is actually an identifier
            // THEN the identifier is matched and its value is kept as a whole
            AssertIdentifier(identifier);
        }

        [Test]
        public void TestNotIdentifier1()
        {
            // GIVEN a given string that is not an identifier
            string identifier = "1";

            // WHEN we check if it is actually an identifier
            var match = System.Text.RegularExpressions.Regex.Match(identifier, Parser.GetNamePattern(true));

            // THEN it is recognized as not being an identifier
            Assert.False(match.Success);
        }

        [Test]
        public void TestNotIdentifier2()
        {
            // GIVEN a given string that is not an identifier
            string identifier = "1a";

            // WHEN we check if it is actually an identifier
            var match = System.Text.RegularExpressions.Regex.Match(identifier, Parser.GetNamePattern(true));

            // THEN it is recognized as not being an identifier
            Assert.False(match.Success);
        }

        [Test]
        public void TestNotIdentifier3()
        {
            // GIVEN a given string that is not an identifier
            string identifier = "a:";

            // WHEN we check if it is actually an identifier
            var match = System.Text.RegularExpressions.Regex.Match(identifier, Parser.GetNamePattern(true));

            // THEN it is recognized as not being an identifier
            Assert.False(match.Success);
        }

        [Test]
        public void TestNotIdentifier4()
        {
            // GIVEN a given string that is not an identifier
            string identifier = "a a";

            // WHEN we check if it is actually an identifier
            var match = System.Text.RegularExpressions.Regex.Match(identifier, Parser.GetNamePattern(true));

            // THEN it is recognized as not being an identifier
            Assert.False(match.Success);
        }

        [Test]
        [Ignore("Implement #97 first")]
        public void TestSpace()
        {
            string identifier = "a a";

            var exp = Expression.CreateFromString(identifier, Language.Cpp);

            Assert.AreEqual(identifier, exp.ToString());
        }
    }
}

