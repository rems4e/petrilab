﻿//
//  TestParser.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2018-03-05.
//

using System;
using System.Collections.Generic;
using System.Linq;

using NUnit.Framework;

using Petri.Code;

namespace Petri.Test.Code
{
    [TestFixture]
    public class TestHeaderParser
    {
        static string _builtins = Petri.Application.Configuration.GetLocalized("Built-in functions");

        [Test]
        public void TestCHeaderSections()
        {
            var doc = new Petri.Application.CLI.HeadlessDocument("../../../Examples/CBasic.petri");

            var functions = doc.AllFunctions;
            Assert.AreEqual(4, functions.Count);

            CollectionAssert.AreEquivalent(new List<string> { "Action2&3", "Conditions", _builtins, "" }, functions.Keys);
        }

        [Test]
        public void TestCHeaderFunctions()
        {
            var doc = new Petri.Application.CLI.HeadlessDocument("../../../Examples/CBasic.petri");

            var functions = doc.AllFunctions;
            var flattened = from kvp in functions where kvp.Key != _builtins from f in kvp.Value select Tuple.Create(kvp.Key, f);
            Assert.AreEqual(6, flattened.Count());

            CollectionAssert.AreEquivalent(new List<string> { "action1", "action2", "action3", "action4", "condition1", "condition2" }, from tup in flattened select tup.Item2.Name);

            Assert.AreEqual("action1()", flattened.First(tup => tup.Item2.Name == "action1").Item2.Signature);
            Assert.AreEqual("ActionResult", flattened.First(tup => tup.Item2.Name == "action1").Item2.ReturnType.ToString());

            Assert.AreEqual("action2()", flattened.First(tup => tup.Item2.Name == "action2").Item2.Signature);
            Assert.AreEqual("ActionResult", flattened.First(tup => tup.Item2.Name == "action2").Item2.ReturnType.ToString());

            Assert.AreEqual("action3()", flattened.First(tup => tup.Item2.Name == "action3").Item2.Signature);
            Assert.AreEqual("double", flattened.First(tup => tup.Item2.Name == "action3").Item2.ReturnType.ToString());

            Assert.AreEqual("action4(int param, char const* param2)", flattened.First(tup => tup.Item2.Name == "action4").Item2.Signature);
            Assert.AreEqual("ActionResult", flattened.First(tup => tup.Item2.Name == "action4").Item2.ReturnType.ToString());

            Assert.AreEqual("condition1(Petri_actionResult_t result)", flattened.First(tup => tup.Item2.Name == "condition1").Item2.Signature);
            Assert.AreEqual("bool", flattened.First(tup => tup.Item2.Name == "condition1").Item2.ReturnType.ToString());

            Assert.AreEqual("condition2(Petri_actionResult_t result)", flattened.First(tup => tup.Item2.Name == "condition2").Item2.Signature);
            Assert.AreEqual("bool", flattened.First(tup => tup.Item2.Name == "condition2").Item2.ReturnType.ToString());
        }

        [Test]
        public void TestCppHeaderSections()
        {
            var doc = new Petri.Application.CLI.HeadlessDocument("../../../Examples/CppBasic.petri");

            var functions = doc.AllFunctions;
            Assert.AreEqual(4, functions.Count);

            CollectionAssert.AreEquivalent(new List<string> { "action2", "class", _builtins, "" }, functions.Keys);
        }

        [Test]
        public void TestCppHeaderFunctions()
        {
            var doc = new Petri.Application.CLI.HeadlessDocument("../../../Examples/CppBasic.petri");

            var functions = doc.AllFunctions;
            var flattened = from kvp in functions where kvp.Key != _builtins from f in kvp.Value select Tuple.Create(kvp.Key, f);
            Assert.AreEqual(10, flattened.Count());

            CollectionAssert.AreEquivalent(new List<string> { "action1", "action2", "condition1", "action4", "action5", "condition2", "action7", "action8", "action9", "action10" }, from tup in flattened select tup.Item2.Name);

            Assert.AreEqual("TestNS::action1()", flattened.First(tup => tup.Item2.Name == "action1").Item2.Signature);
            Assert.AreEqual("ActionResult", flattened.First(tup => tup.Item2.Name == "action1").Item2.ReturnType.ToString());
            Assert.AreEqual(typeof(Function), flattened.First(tup => tup.Item2.Name == "action1").Item2.GetType());

            Assert.AreEqual("TestNS::action2()", flattened.First(tup => tup.Item2.Name == "action2").Item2.Signature);
            Assert.AreEqual("ActionResult", flattened.First(tup => tup.Item2.Name == "action2").Item2.ReturnType.ToString());
            Assert.AreEqual(typeof(Function), flattened.First(tup => tup.Item2.Name == "action2").Item2.GetType());

            Assert.AreEqual("TestNS::condition1(ActionResult result)", flattened.First(tup => tup.Item2.Name == "condition1").Item2.Signature);
            Assert.AreEqual("bool", flattened.First(tup => tup.Item2.Name == "condition1").Item2.ReturnType.ToString());
            Assert.AreEqual(typeof(Function), flattened.First(tup => tup.Item2.Name == "condition1").Item2.GetType());

            Assert.AreEqual("TestClass::action4(std::vector<std::list<int >> const&&)", flattened.First(tup => tup.Item2.Name == "action4").Item2.Signature);
            Assert.AreEqual("std::size_t", flattened.First(tup => tup.Item2.Name == "action4").Item2.ReturnType.ToString());
            Assert.AreEqual(typeof(Function), flattened.First(tup => tup.Item2.Name == "action4").Item2.GetType());

            Assert.AreEqual("TestClass::action5(std::vector<std::list<int >> const&&)", flattened.First(tup => tup.Item2.Name == "action5").Item2.Signature);
            Assert.AreEqual("ActionResult", flattened.First(tup => tup.Item2.Name == "action5").Item2.ReturnType.ToString());
            Assert.AreEqual(typeof(Function), flattened.First(tup => tup.Item2.Name == "action5").Item2.GetType());

            Assert.AreEqual("TestClass::condition2(ActionResult result, int param)", flattened.First(tup => tup.Item2.Name == "condition2").Item2.Signature);
            Assert.AreEqual("bool", flattened.First(tup => tup.Item2.Name == "condition2").Item2.ReturnType.ToString());
            Assert.AreEqual(typeof(Function), flattened.First(tup => tup.Item2.Name == "condition2").Item2.GetType());

            Assert.AreEqual("action7(std::vector<std::list<int>> const& param)", flattened.First(tup => tup.Item2.Name == "action7").Item2.Signature);
            Assert.AreEqual("ActionResult", flattened.First(tup => tup.Item2.Name == "action7").Item2.ReturnType.ToString());
            Assert.AreEqual(typeof(Function), flattened.First(tup => tup.Item2.Name == "action7").Item2.GetType());

            Assert.AreEqual("NS2::MyClass::action8(double)", flattened.First(tup => tup.Item2.Name == "action8").Item2.Signature);
            Assert.AreEqual("int", flattened.First(tup => tup.Item2.Name == "action8").Item2.ReturnType.ToString());
            Assert.AreEqual(typeof(Function), flattened.First(tup => tup.Item2.Name == "action8").Item2.GetType());

            Assert.AreEqual("NS2::MyClass::action9(double)", flattened.First(tup => tup.Item2.Name == "action9").Item2.Signature);
            Assert.AreEqual("int", flattened.First(tup => tup.Item2.Name == "action9").Item2.ReturnType.ToString());
            Assert.AreEqual(typeof(Method), flattened.First(tup => tup.Item2.Name == "action9").Item2.GetType());

            Assert.AreEqual("NS2::MyClass::action10(double)", flattened.First(tup => tup.Item2.Name == "action10").Item2.Signature);
            Assert.AreEqual("ActionResult", flattened.First(tup => tup.Item2.Name == "action10").Item2.ReturnType.ToString());
            Assert.AreEqual(typeof(Method), flattened.First(tup => tup.Item2.Name == "action10").Item2.GetType());
        }

        [Test]
        public void TestCSharpHeaderSections()
        {
            var doc = new Petri.Application.CLI.HeadlessDocument("../../../Examples/CSharpBasic.petri");

            var functions = doc.AllFunctions;
            Assert.AreEqual(4, functions.Count);

            CollectionAssert.AreEquivalent(new List<string> { "class + NS", "class", _builtins, "" }, functions.Keys);
        }

        [Test]
        public void TestCSharpHeaderFunctions()
        {
            var doc = new Petri.Application.CLI.HeadlessDocument("../../../Examples/CSharpBasic.petri");

            var functions = doc.AllFunctions;
            var flattened = from kvp in functions where kvp.Key != _builtins from f in kvp.Value select Tuple.Create(kvp.Key, f);
            Assert.AreEqual(5, flattened.Count());

            CollectionAssert.AreEquivalent(new List<string> { "Action1", "Action2", "Condition1", "Action3", "Condition2" }, from tup in flattened select tup.Item2.Name);

            Assert.AreEqual("TestNS.Action1()", flattened.First(tup => tup.Item2.Name == "Action1").Item2.Signature);
            Assert.AreEqual("ActionResult", flattened.First(tup => tup.Item2.Name == "Action1").Item2.ReturnType.ToString());

            Assert.AreEqual("TestNS.Action2()", flattened.First(tup => tup.Item2.Name == "Action2").Item2.Signature);
            Assert.AreEqual("ActionResult", flattened.First(tup => tup.Item2.Name == "Action2").Item2.ReturnType.ToString());

            Assert.AreEqual("TestNS.Condition1(Int32 result)", flattened.First(tup => tup.Item2.Name == "Condition1").Item2.Signature);
            Assert.AreEqual("bool", flattened.First(tup => tup.Item2.Name == "Condition1").Item2.ReturnType.ToString());

            Assert.AreEqual("TestNS2.TestClass.Action3(string value)", flattened.First(tup => tup.Item2.Name == "Action3").Item2.Signature);
            Assert.AreEqual("List<Tuple<string, char>>", flattened.First(tup => tup.Item2.Name == "Action3").Item2.ReturnType.ToString());

            Assert.AreEqual("TestNS2.TestClass.Condition2(Int32 result)", flattened.First(tup => tup.Item2.Name == "Condition2").Item2.Signature);
            Assert.AreEqual("bool", flattened.First(tup => tup.Item2.Name == "Condition2").Item2.ReturnType.ToString());
        }

        [Test]
        public void TestPythonHeaderSections()
        {
            var doc = new Petri.Application.CLI.HeadlessDocument("../../../Examples/PythonBasic.petri");

            var functions = doc.AllFunctions;
            Assert.AreEqual(4, functions.Count);

            CollectionAssert.AreEquivalent(new List<string> { "Test section", "Test section 2", _builtins, "" }, functions.Keys);
        }

        [Test]
        public void TestPythonHeaderFunctions()
        {
            var doc = new Petri.Application.CLI.HeadlessDocument("../../../Examples/PythonBasic.petri");

            var functions = doc.AllFunctions;
            var flattened = from kvp in functions where kvp.Key != _builtins from f in kvp.Value select Tuple.Create(kvp.Key, f);
            Assert.AreEqual(8, flattened.Count());

            CollectionAssert.AreEquivalent(new List<string> { "action1", "action2", "action3", "condition1", "my_method1", "my_method2", "my_method3", "my_method4" }, from tup in flattened select tup.Item2.Name);

            Assert.AreEqual("TestPython.action1()", flattened.First(tup => tup.Item2.Name == "action1").Item2.Signature);
            Assert.AreEqual(typeof(Function), flattened.First(tup => tup.Item2.Name == "action1").Item2.GetType());

            Assert.AreEqual("TestPython.action2()", flattened.First(tup => tup.Item2.Name == "action2").Item2.Signature);
            Assert.AreEqual(typeof(Function), flattened.First(tup => tup.Item2.Name == "action2").Item2.GetType());

            Assert.AreEqual("TestPython.action3(arg1, arg2)", flattened.First(tup => tup.Item2.Name == "action3").Item2.Signature);
            Assert.AreEqual(typeof(Function), flattened.First(tup => tup.Item2.Name == "action3").Item2.GetType());

            Assert.AreEqual("TestPython.condition1(action_result)", flattened.First(tup => tup.Item2.Name == "condition1").Item2.Signature);
            Assert.AreEqual(typeof(Function), flattened.First(tup => tup.Item2.Name == "condition1").Item2.GetType());

            Assert.AreEqual("TestPython.MyClass2.my_method1(self)", flattened.First(tup => tup.Item2.Name == "my_method1").Item2.Signature);
            Assert.AreEqual(typeof(Function), flattened.First(tup => tup.Item2.Name == "my_method1").Item2.GetType());

            Assert.AreEqual("TestPython.MyClass2.my_method2()", flattened.First(tup => tup.Item2.Name == "my_method2").Item2.Signature);
            Assert.AreEqual(typeof(Function), flattened.First(tup => tup.Item2.Name == "my_method2").Item2.GetType());

            Assert.AreEqual("TestPython.MyClass2.MyClass3.my_method3(myself, arg)", flattened.First(tup => tup.Item2.Name == "my_method3").Item2.Signature);
            Assert.AreEqual(typeof(Function), flattened.First(tup => tup.Item2.Name == "my_method3").Item2.GetType());

            Assert.AreEqual("TestPython.MyClass2.my_method4()", flattened.First(tup => tup.Item2.Name == "my_method4").Item2.Signature);
            Assert.AreEqual(typeof(Function), flattened.First(tup => tup.Item2.Name == "my_method4").Item2.GetType());
        }
    }
}

