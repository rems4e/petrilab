﻿//
//  TestExpressionToString.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2015-12-25.
//

using NUnit.Framework;

using Petri.Code;

namespace Petri.Test.Code
{
    [TestFixture]
    public class TestExpressionToString
    {
        [Test]
        public void TestAdditionMakeCode([Values(Language.Cpp, Language.C, Language.CSharp, Language.Python)] Language lang)
        {
            // GIVEN a simple addition expression
            // WHEN we create an expression from it and convert it to a code string
            var e = Expression.CreateFromString("3+4", lang);

            // THEN the string representations of the expression is as expected
            Assert.AreEqual("3 + 4", e.MakeCode());
            Assert.AreEqual("3 + 4", e.MakeUserReadable());
        }

        [Test]
        public void TestMultiplicationToCode([Values(Language.Cpp, Language.C, Language.CSharp, Language.Python)] Language lang)
        {
            // GIVEN a composition of an addition and multiplication
            // WHEN we create an expression from it and convert it to a code string
            var e = Expression.CreateFromString("3+4*5", lang);

            // THEN the string representations of the expression is as expected
            Assert.AreEqual("3 + 4 * 5", e.MakeCode());
            Assert.AreEqual("3 + 4 * 5", e.MakeUserReadable());
        }

        [Test]
        public void TestFunctionInvocationToString1([Values(Language.Cpp, Language.C, Language.CSharp, Language.Python)] Language lang)
        {
            // GIVEN a function invocation string
            string invocation = "f()";

            // WHEN we create an expression from it
            var e = Expression.CreateFromString<FunctionInvocation>(invocation, lang);

            // THEN the string representations of the expression is as expected
            Assert.AreEqual(invocation, e.MakeCode());
            Assert.AreEqual(invocation, e.MakeUserReadable());
        }

        [Test]
        public void TestFunctionInvocationToString2([Values(Language.Cpp, Language.C, Language.CSharp, Language.Python)] Language lang)
        {
            // GIVEN a function invocation string
            string invocation = "f(a)";

            // WHEN we create an expression from it
            var e = Expression.CreateFromString<FunctionInvocation>(invocation, lang);

            // THEN the string representations of the expression is as expected
            Assert.AreEqual("f(a)", e.MakeCode());
            Assert.AreEqual(invocation, e.MakeUserReadable());
        }

        [Test]
        public void TestFunctionInvocationToString3([Values(Language.Cpp, Language.C, Language.CSharp, Language.Python)] Language lang)
        {
            // GIVEN a function invocation string
            string invocation = "f( a             )";

            // WHEN we create an expression from it
            var e = Expression.CreateFromString<FunctionInvocation>(invocation, lang);

            // THEN the string representations of the expression is as expected
            Assert.AreEqual("f(a)", e.MakeCode());
            Assert.AreEqual("f(a)", e.MakeUserReadable());
        }

        [Test]
        public void TestFunctionInvocationToString4([Values(Language.Cpp, Language.C, Language.CSharp, Language.Python)] Language lang)
        {
            // GIVEN a function invocation string
            string invocation = "f( a     ,b, c        )";

            // WHEN we create an expression from it
            var e = Expression.CreateFromString<FunctionInvocation>(invocation, lang);

            // THEN the string representations of the expression is as expected
            Assert.AreEqual("f(a, b, c)", e.MakeCode());
            Assert.AreEqual("f(a, b, c)", e.MakeUserReadable());
        }
    }
}

