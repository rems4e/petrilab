﻿//
//  TestSelectionOperator.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-03-13.
//

using NUnit.Framework;

using Petri.Code;

namespace Petri.Test.Code
{
    [TestFixture]
    public class TestSelectionOperator
    {
        [Test]
        public void TestMemberSelectionRef()
        {
            // GIVEN a selection by reference expression
            // WHEN we create an expression from it
            var e = Expression.CreateFromString("a.b", Language.Cpp);

            // THEN the expression is binary and has the right operator
            Assert.IsInstanceOf<BinaryExpression>(e);
            Assert.AreEqual(Operator.Name.SelectionRef, e.Operator);
        }

        [Test]
        public void TestMemberSelectionPtr()
        {
            // GIVEN a selection by pointer expression
            // WHEN we create an expression from it
            var e = Expression.CreateFromString("a->b", Language.Cpp);

            // THEN the expression is binary and has the right operator
            Assert.IsInstanceOf<BinaryExpression>(e);
            Assert.AreEqual(Operator.Name.SelectionPtr, e.Operator);
        }

        [Test]
        public void TestMethodCallRef()
        {
            // GIVEN a method call on a reference object
            // WHEN we create an expression from it
            var e = Expression.CreateFromString("a.b()", Language.Cpp);

            // THEN the expression is a method invocation
            Assert.IsInstanceOf<MethodInvocation>(e);
        }

        [Test]
        public void TestMethodCallPtr()
        {
            // GIVEN a method call on a pointer object
            // WHEN we create an expression from it
            var e = Expression.CreateFromString("a->b()", Language.Cpp);

            Assert.IsInstanceOf<MethodInvocation>(e);
            // THEN the expression is a method invocation
        }
    }
}

