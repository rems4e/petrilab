﻿//
//  TestLiterals.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2015-12-25.
//

using NUnit.Framework;

using Petri.Code;

namespace Petri.Test.Code
{
    [TestFixture]
    public class TestLiterals
    {
        [Test, Repeat(200)]
        public void TestLiteral([Values(Language.Cpp, Language.C, Language.CSharp, Language.Python)] Language lang)
        {
            // GIVEN a random literal string
            var literal = CodeUtility.RandomLiteral(lang);
            // WHEN we create an expression from it
            var e = Expression.CreateFromString(literal.Item1, lang);

            // THEN it is a LiteralExpression
            Assert.IsInstanceOf<LiteralExpression>(e);

            var lit = e as LiteralExpression;

            // AND the textual content of the expression is the same as the initial literal
            Assert.AreEqual(literal.Item1, lit.Expression);
        }

        [Test]
        public void TestLiteralWithAt([Values(Language.Cpp, Language.C, Language.CSharp, Language.Python)] Language lang)
        {
            // GIVEN a literal string with an @ symbol inside
            var literal = "\"a string with a @ symbol…\"";

            // WHEN we create an expression from it
            var e = Expression.CreateFromString(literal, lang);

            // THEN it is a LiteralExpression
            Assert.IsInstanceOf<LiteralExpression>(e);

            var lit = e as LiteralExpression;

            // AND the textual content of the expression is the same as the initial literal
            Assert.AreEqual(literal, lit.Expression);
        }

        [Test]
        public void TestLiteralWithQuote1([Values(Language.Cpp, Language.C, Language.CSharp, Language.Python)] Language lang)
        {
            // GIVEN a literal string with a singly quoted character inside
            var literal = "'a'";

            // WHEN we create an expression from it
            var e = Expression.CreateFromString(literal, lang);

            // THEN it is a LiteralExpression
            Assert.IsInstanceOf<LiteralExpression>(e);

            var lit = e as LiteralExpression;

            // AND the textual content of the expression is the same as the initial literal
            Assert.AreEqual(literal, lit.Expression);
        }

        [Test]
        public void TestLiteralWithQuote2([Values(Language.Cpp, Language.C, Language.CSharp, Language.Python)] Language lang)
        {
            // GIVEN a literal string with a singly quoted quote character inside
            var literal = "'\\''";

            // WHEN we create an expression from it
            var e = Expression.CreateFromString(literal, lang);

            // THEN it is a LiteralExpression
            Assert.IsInstanceOf<LiteralExpression>(e);

            var lit = e as LiteralExpression;

            // AND the textual content of the expression is the same as the initial literal
            Assert.AreEqual(literal, lit.Expression);
        }

        [Test]
        public void TestLiteralWithQuote3([Values(Language.Cpp, Language.C, Language.CSharp, Language.Python)] Language lang)
        {
            // GIVEN a literal string with a singly quoted double quote character inside
            var literal = "'\"'";

            // WHEN we create an expression from it
            var e = Expression.CreateFromString(literal, lang);

            // THEN it is a LiteralExpression
            Assert.IsInstanceOf<LiteralExpression>(e);

            var lit = e as LiteralExpression;

            // AND the textual content of the expression is the same as the initial literal
            Assert.AreEqual(literal, lit.Expression);
        }

        [Test]
        public void TestLiteralWithQuote4([Values(Language.Cpp, Language.C, Language.CSharp, Language.Python)] Language lang)
        {
            // GIVEN a literal string with a singly quoted space character inside
            var literal = "' '";

            // WHEN we create an expression from it
            var e = Expression.CreateFromString(literal, lang);

            // THEN it is a LiteralExpression
            Assert.IsInstanceOf<LiteralExpression>(e);

            var lit = e as LiteralExpression;

            // AND the textual content of the expression is the same as the initial literal
            Assert.AreEqual(literal, lit.Expression);
        }

        [Test]
        public void TestLiteralWithDoubleQuote1([Values(Language.Cpp, Language.C, Language.CSharp, Language.Python)] Language lang)
        {
            // GIVEN a literal string with a doubly quoted character inside
            var literal = "\"a\"";

            // WHEN we create an expression from it
            var e = Expression.CreateFromString(literal, lang);

            // THEN it is a LiteralExpression
            Assert.IsInstanceOf<LiteralExpression>(e);

            var lit = e as LiteralExpression;

            // AND the textual content of the expression is the same as the initial literal
            Assert.AreEqual(literal, lit.Expression);
        }

        [Test]
        public void TestLiteralWithDoubleQuote2([Values(Language.Cpp, Language.C, Language.CSharp, Language.Python)] Language lang)
        {
            // GIVEN a literal string with a doubly quoted backslash character inside
            var literal = "\"\\\"";

            // WHEN we create an expression from it
            var e = Expression.CreateFromString(literal, lang);

            // THEN it is a LiteralExpression
            Assert.IsInstanceOf<LiteralExpression>(e);

            var lit = e as LiteralExpression;

            // AND the textual content of the expression is the same as the initial literal
            Assert.AreEqual(literal, lit.Expression);
        }

        [Test]
        public void TestLiteralWithDoubleQuote3([Values(Language.Cpp, Language.C, Language.CSharp, Language.Python)] Language lang)
        {
            // GIVEN a literal string with a doubly quoted quote character inside
            var literal = "\"'\"";

            // WHEN we create an expression from it
            var e = Expression.CreateFromString(literal, lang);

            // THEN it is a LiteralExpression
            Assert.IsInstanceOf<LiteralExpression>(e);

            var lit = e as LiteralExpression;

            // AND the textual content of the expression is the same as the initial literal
            Assert.AreEqual(literal, lit.Expression);
        }

        [Test]
        public void TestLiteralWithDoubleQuote4([Values(Language.Cpp, Language.C, Language.CSharp, Language.Python)] Language lang)
        {
            // GIVEN a literal string with a doubly quoted double quote character inside
            var literal = "\"\\\"\"";

            // WHEN we create an expression from it
            var e = Expression.CreateFromString(literal, lang);

            // THEN it is a LiteralExpression
            Assert.IsInstanceOf<LiteralExpression>(e);

            var lit = e as LiteralExpression;

            // AND the textual content of the expression is the same as the initial literal
            Assert.AreEqual(literal, lit.Expression);
        }

        [Test]
        public void TestLiteralWithDoubleQuote5([Values(Language.Cpp, Language.C, Language.CSharp, Language.Python)] Language lang)
        {
            // GIVEN a literal string with a doubly quoted space character inside
            var literal = "\" \"";

            // WHEN we create an expression from it
            var e = Expression.CreateFromString(literal, lang);

            // THEN it is a LiteralExpression
            Assert.IsInstanceOf<LiteralExpression>(e);

            var lit = e as LiteralExpression;

            // AND the textual content of the expression is the same as the initial literal
            Assert.AreEqual(literal, lit.Expression);
        }

        [Test]
        public void TestLiteralWithDoubleQuoteAfterBackSlash([Values(Language.Cpp, Language.C, Language.CSharp, Language.Python)] Language lang)
        {
            // GIVEN a literal string with a double quote following a backslash
            var literal = "\"\\\"\"";

            // WHEN we create an expression from it
            var e = Expression.CreateFromString(literal, lang);

            // THEN it is a LiteralExpression
            Assert.IsInstanceOf<LiteralExpression>(e);

            var lit = e as LiteralExpression;

            // AND the textual content of the expression is the same as the initial literal
            Assert.AreEqual(literal, lit.Expression);
        }

        [Test]
        public void TestLiteralsLiterals([Values(Language.Cpp, Language.C, Language.CSharp, Language.Python)] Language lang)
        {
            // GIVEN a random literal string
            var literal = CodeUtility.RandomLiteral(lang);

            // WHEN we create an expression from it
            var e = Expression.CreateFromString(literal.Item1, lang);

            var literals = e.GetLiterals();

            // THEN the set of literals in the whole expression is only {initialLiteral}
            Assert.AreEqual(1, literals.Count);
            Assert.Contains(e, literals);
        }

        [Test]
        public void TestAddition([Values(Language.Cpp, Language.C, Language.CSharp, Language.Python)] Language lang)
        {
            // GIVEN a simple addition expression
            var addition = "3+4";

            // WHEN we create an expression from it
            var e = Expression.CreateFromString(addition, lang);

            // THEN the resulting expression is a BinaryExpression, consisting of the right operator and the right literal subexpressions.
            Assert.IsInstanceOf<BinaryExpression>(e);
            var bin = e as BinaryExpression;
            Assert.AreEqual(Operator.Name.Plus, bin.Operator);

            Assert.IsInstanceOf<LiteralExpression>(bin.Expression1);
            Assert.IsInstanceOf<LiteralExpression>(bin.Expression1);

            Assert.AreEqual(((LiteralExpression)bin.Expression1).Expression, "3");
            Assert.AreEqual(((LiteralExpression)bin.Expression2).Expression, "4");
        }
    }
}

