//
//  TestCompilation.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-01.
//

using NUnit.Framework;

namespace Petri.Test.Examples.External
{
    [TestFixture]
    public class TestCompilation
    {
        [TestCase("CExternal.petri")]
        [TestCase("CppExternal.petri")]
        [TestCase("CSharpExternal.petri")]
        [TestCase("PythonExternal.petri")]
        [TestCase("CLocalVariables.petri")]
        [TestCase("CppLocalVariables.petri")]
        [TestCase("CSharpLocalVariables.petri")]
        [TestCase("PythonLocalVariables.petri")]
        [TestCase("CConcurrent.petri")]
        [TestCase("CConcurrentInvocation.petri")]
        [TestCase("CppConcurrent.petri")]
        [TestCase("CppConcurrentInvocation.petri")]
        [TestCase("CSharpConcurrent.petri")]
        [TestCase("CSharpConcurrentInvocation.petri")]
        [TestCase("PythonConcurrent.petri")]
        [TestCase("PythonConcurrentInvocation.petri")]
        [TestCase("CVarAsArg.petri")]
        [TestCase("CppVarAsArg.petri")]
        [TestCase("CSharpVarAsArg.petri")]
        [TestCase("PythonVarAsArg.petri")]
        [TestCase("CInstantiateN.petri")]
        [TestCase("CppInstantiateN.petri")]
        [TestCase("CSharpInstantiateN.petri")]
        [TestCase("PythonInstantiateN.petri")]
        [TestCase("CParamToArg.petri")]
        [TestCase("CppParamToArg.petri")]
        [TestCase("CSharpParamToArg.petri")]
        [TestCase("PythonParamToArg.petri")]
        public void TestExampleCompilation(string path)
        {
            // GIVEN launch arguments requesting code generation and compilation of the petri net example
            string[] args = { "-k", "-g", "-c", "../../../Examples/ExternalPetriNet/" + path };
            string stdout, stderr;

            // WHEN the invocation is made
            int result = CompilerUtility.InvokeCompiler(args, out stdout, out stderr);

            // THEN no error is returned.
            Assert.IsEmpty(stderr);
            Assert.IsEmpty(stdout);
            Assert.AreEqual(0, result);
        }
    }
}

