//
//  TestExecution.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-01.
//

using System;
using System.IO;

using NUnit.Framework;

namespace Petri.Test.Examples.External
{
    [TestFixture]
    public class TestExecution
    {
        [TestCase("../../../Examples/ExternalPetriNet/CExternal.petri")]
        [TestCase("../../../Examples/ExternalPetriNet/CppExternal.petri")]
        [TestCase("../../../Examples/ExternalPetriNet/CSharpExternal.petri")]
        [TestCase("../../../Examples/ExternalPetriNet/PythonExternal.petri")]
        public void TestSimpleExternalExecution(string path)
        {
            var name = Path.GetFileNameWithoutExtension(path);
            var expected = "Start " + name + "\n";
            for(int i = 0; i < 10; ++i) {
                expected += "$var: " + i + "\n";
            }
            expected += "Intermediate " + name + "\n";
            expected += "End " + name + "\n";

            // GIVEN launch arguments requesting execution of the petri net example
            string[] args = { "-r", path };
            string stdout, stderr;

            // WHEN the invocation is made
            int result = CompilerUtility.InvokeCompiler(args, out stdout, out stderr);

            // THEN no error is returned.
            Assert.AreEqual(0, result);
            Assert.AreEqual(expected, stdout);
            Assert.IsEmpty(stderr);
        }

        [TestCase("../../../Examples/ExternalPetriNet/CLocalVariables.petri")]
        [TestCase("../../../Examples/ExternalPetriNet/CppLocalVariables.petri")]
        [TestCase("../../../Examples/ExternalPetriNet/CSharpLocalVariables.petri")]
        [TestCase("../../../Examples/ExternalPetriNet/PythonLocalVariables.petri")]
        [TestCase("../../../Examples/Embedded/EmbeddedCLocalVariables.petri")]
        public void TestLocalVariablesExternalExecution(string path)
        {
            var expected = "";
            for(int i = 0; i < 3; ++i) {
                expected += "$var: " + (i + 1) * 21 + "\n";
                for(int j = 0; j < 10; ++j) {
                    expected += "$var: " + j + "\n";
                }
                expected += "$var: " + (i + 1) * 21 + "\n";
            }
            expected += "$count: -5\n";

            // GIVEN launch arguments requesting execution of the petri net example
            string[] args = { "-r", path };
            string stdout, stderr;

            // WHEN the invocation is made
            int result = CompilerUtility.InvokeCompiler(args, out stdout, out stderr);

            // THEN no error is returned.
            Assert.AreEqual(0, result);
            Assert.AreEqual(expected, stdout);
            Assert.IsEmpty(stderr);
        }

        [TestCase("../../../Examples/ExternalPetriNet/CConcurrent.petri")]
        [TestCase("../../../Examples/ExternalPetriNet/CConcurrentInvocation.petri")]
        [TestCase("../../../Examples/ExternalPetriNet/CppConcurrent.petri")]
        [TestCase("../../../Examples/ExternalPetriNet/CppConcurrentInvocation.petri")]
        [TestCase("../../../Examples/ExternalPetriNet/CSharpConcurrent.petri")]
        [TestCase("../../../Examples/ExternalPetriNet/CSharpConcurrentInvocation.petri")]
        [TestCase("../../../Examples/ExternalPetriNet/PythonConcurrent.petri")]
        [TestCase("../../../Examples/ExternalPetriNet/PythonConcurrentInvocation.petri")]
        public void TestConcurrentExternalExecution(string path)
        {
            // GIVEN launch arguments requesting execution of the petri net example
            string[] args = { "-r", path };
            string stdout, stderr;

            // WHEN the invocation is made
            int result = CompilerUtility.InvokeCompiler(args, out stdout, out stderr);

            var lines = stdout.Split(new char[] { '\n' });

            Assert.AreEqual(lines.Length, 10 * 4 * 16 + 2);

            Assert.AreEqual("$iter: 16", lines[lines.Length - 2]);

            Array.Sort(lines);
            for(int i = 0; i < 10 * 4 * 16; ++i) {
                Assert.AreEqual(string.Format("$var: {0}", i / (16 * 4)), lines[i + 2]);
            }

            // THEN no error is returned.
            Assert.AreEqual(0, result);
            Assert.IsEmpty(stderr);
        }

        [TestCase("../../../Examples/ExternalPetriNet/CVarAsArg.petri")]
        [TestCase("../../../Examples/ExternalPetriNet/CppVarAsArg.petri")]
        [TestCase("../../../Examples/ExternalPetriNet/CSharpVarAsArg.petri")]
        [TestCase("../../../Examples/ExternalPetriNet/PythonVarAsArg.petri")]
        public void TestVarAsArgExecution(string path)
        {
            var name = Path.GetFileNameWithoutExtension(path);
            var expected = "";
            for(int i = 0; i < 17; ++i) {
                expected += "$var: " + i + "\n";
            }

            // GIVEN launch arguments requesting execution of the petri net example
            string[] args = { "-r", path };
            string stdout, stderr;

            // WHEN the invocation is made
            int result = CompilerUtility.InvokeCompiler(args, out stdout, out stderr);

            // THEN no error is returned.
            Assert.AreEqual(0, result);
            Assert.AreEqual(expected, stdout);
            Assert.IsEmpty(stderr);
        }

        void AssertInstantiateN(int count, string stdout)
        {
            var lines = stdout.Split(new char[] { '\n' });

            Assert.AreEqual(count * 10 + count * (count + 1) / 2 + 1, lines.Length);

            if(count > 0) {
                Assert.AreEqual(string.Format("$var: {0}", 10 + count - 1), lines[lines.Length - 2]);
            }

            Array.Sort(lines, (a, b) => {
                if(a == "" || b == "") {
                    return string.Compare(a, b, StringComparison.InvariantCulture);
                }
                return int.Parse(a.Substring("$var: ".Length)).CompareTo(int.Parse(b.Substring("$var: ".Length)));
            });
            var line = 0;
            for(int j = 0; j < 10; ++j) {
                for(int i = 0; i < count; ++i) {
                    Assert.AreEqual(string.Format("$var: {0}", j), lines[line + 1]);
                    ++line;
                }
            }
            for(int i = 0; i < count; ++i) {
                for(int j = count - i - 1; j >= 0; --j) {
                    Assert.AreEqual(string.Format("$var: {0}", 10 + i), lines[line + 1]);
                    ++line;
                }
            }

        }

        [TestCase("../../../Examples/ExternalPetriNet/CInstantiateN.petri")]
        [TestCase("../../../Examples/ExternalPetriNet/CppInstantiateN.petri")]
        [TestCase("../../../Examples/ExternalPetriNet/CSharpInstantiateN.petri")]
        [TestCase("../../../Examples/ExternalPetriNet/PythonInstantiateN.petri")]
        public void TestInstantiateNExecutionDefaultValue(string path)
        {
            // GIVEN launch arguments requesting execution of the petri net example
            string[] args = { "-r", path };
            string stdout, stderr;

            // WHEN the invocation is made
            int result = CompilerUtility.InvokeCompiler(args, out stdout, out stderr);

            // THEN no error is returned.
            AssertInstantiateN(3, stdout);
            Assert.AreEqual(0, result);
            Assert.IsEmpty(stderr);
        }

        [TestCase("../../../Examples/ExternalPetriNet/CParamToArg.petri")]
        [TestCase("../../../Examples/ExternalPetriNet/CppParamToArg.petri")]
        [TestCase("../../../Examples/ExternalPetriNet/CSharpParamToArg.petri")]
        [TestCase("../../../Examples/ExternalPetriNet/PythonParamToArg.petri")]
        public void TestParamToArgExecutionDefaultValue(string path)
        {
            // GIVEN launch arguments requesting execution of the petri net example
            string[] args = { "-r", path };
            string stdout, stderr;

            // WHEN the invocation is made
            int result = CompilerUtility.InvokeCompiler(args, out stdout, out stderr);

            // THEN no error is returned.
            AssertInstantiateN(0, stdout);
            Assert.AreEqual(0, result);
            Assert.IsEmpty(stderr);
        }

        [Test]
        public void TestInstantiateNExecutionCustomValue([Values(
            "CInstantiateN",
            "CppInstantiateN",
            "CSharpInstantiateN",
            "PythonInstantiateN",
            "CParamToArg",
            "CppParamToArg",
            "CSharpParamToArg",
            "PythonParamToArg")] string lang, [Values(0, 3, 10, 40)] int count)
        {
            string path = string.Format("../../../Examples/ExternalPetriNet/{0}.petri", lang);

            // GIVEN launch arguments requesting execution of the petri net example
            string[] args = { "-r", "--args", string.Format("$n:{0}", count), path };
            string stdout, stderr;

            // WHEN the invocation is made
            int result = CompilerUtility.InvokeCompiler(args, out stdout, out stderr);

            // THEN no error is returned.
            AssertInstantiateN(count, stdout);
            Assert.AreEqual(0, result);
            Assert.IsEmpty(stderr);
        }
    }
}

