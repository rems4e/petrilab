//
//  TestUpgrade.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2017-01-31.
//

using NUnit.Framework;

using Petri.Application;

using HeadlessDocument = Petri.Application.CLI.HeadlessDocument;

namespace Petri.Test.Examples.Upgrade
{
    [TestFixture]
    public class TestUpgradeFromOldAPI
    {
        [TestCase("CCountTo10", 99)]
        [TestCase("CSharpCountTo10", 99)]
        [TestCase("CppCountTo10", 99)]
        [TestCase("CCountTo10", 100)]
        [TestCase("CSharpCountTo10", 100)]
        [TestCase("CppCountTo10", 100)]
        [TestCase("CCountTo10", 101)]
        [TestCase("CSharpCountTo10", 101)]
        [TestCase("CppCountTo10", 101)]
        [TestCase("CCountTo10plusN", 102)]
        [TestCase("CSharpCountTo10plusN", 102)]
        [TestCase("CppCountTo10plusN", 102)]
        [TestCase("CCountTo10plusN", 103)]
        [TestCase("CSharpCountTo10plusN", 103)]
        [TestCase("CppCountTo10plusN", 103)]
        public void TestLoad(string example, int oldAPI)
        {
            string stdout, stderr;
            TestingUtility.InvokeAndRedirectOutput(() => {
                return new HeadlessDocument(string.Format("../../../Examples/old_api/{0}_api{1}.petri", example, oldAPI));
            }, out stdout, out stderr);

            Assert.IsEmpty(stderr);
            Assert.AreEqual(string.Format(Document.UpgradingOldNewFormat, oldAPI, Document.DocumentAPIVersion)
                                + "\n"
                                + Document.UpgradeCompleteString
                                + "\n",
                            stdout);
        }

        [TestCase("CCountTo10", 99)]
        [TestCase("CSharpCountTo10", 99)]
        [TestCase("CppCountTo10", 99)]
        [TestCase("CCountTo10", 100)]
        [TestCase("CSharpCountTo10", 100)]
        [TestCase("CppCountTo10", 100)]
        [TestCase("CCountTo10", 101)]
        [TestCase("CSharpCountTo10", 101)]
        [TestCase("CppCountTo10", 101)]
        [TestCase("CCountTo10plusN", 102)]
        [TestCase("CSharpCountTo10plusN", 102)]
        [TestCase("CppCountTo10plusN", 102)]
        [TestCase("CCountTo10plusN", 103)]
        [TestCase("CSharpCountTo10plusN", 103)]
        [TestCase("CppCountTo10plusN", 103)]
        public void TestCompile(string example, int oldAPI)
        {
            string stdout, stderr;
            var result = TestingUtility.InvokeAndRedirectOutput(() => {
                var doc = new HeadlessDocument(string.Format("../../../Examples/old_api/{0}_api{1}.petri", example, oldAPI));
                doc.GenerateCode();
                return doc.Compile().Result;
            }, out stdout, out stderr);

            Assert.IsEmpty(stderr);
            Assert.AreEqual(string.Format(Document.UpgradingOldNewFormat, oldAPI, Document.DocumentAPIVersion)
                                + "\n"
                                + Document.UpgradeCompleteString
                                + "\n",
                            stdout);
            Assert.IsEmpty(result.Item2.Trim());
            Assert.AreEqual(Compiler.CompilationStatus.Success, result.Item1);
        }

        [TestCase("CCountTo10", 99)]
        [TestCase("CSharpCountTo10", 99)]
        [TestCase("CppCountTo10", 99)]
        [TestCase("CCountTo10", 100)]
        [TestCase("CSharpCountTo10", 100)]
        [TestCase("CppCountTo10", 100)]
        [TestCase("CCountTo10", 101)]
        [TestCase("CSharpCountTo10", 101)]
        [TestCase("CppCountTo10", 101)]
        [TestCase("CCountTo10plusN", 102)]
        [TestCase("CSharpCountTo10plusN", 102)]
        [TestCase("CppCountTo10plusN", 102)]
        [TestCase("CCountTo10plusN", 103)]
        [TestCase("CSharpCountTo10plusN", 103)]
        [TestCase("CppCountTo10plusN", 103)]
        public void TestRun(string example, int oldAPI)
        {
            string stdout, stderr;
            var result = CompilerUtility.InvokeCompiler(new string[] {"-r",
                                                                      "--log-return-values",
                                                                      string.Format("../../../Examples/old_api/{0}_api{1}.petri", example, oldAPI)},
                                                        out stdout,
                                                        out stderr);

            var expected = string.Format(Document.UpgradingOldNewFormat, oldAPI, Document.DocumentAPIVersion)
                                + "\n"
                                + Document.UpgradeCompleteString
                                + "\n";

            expected += expected; // The run server loads the document again.

            string suffix = "";
            int total = 10;
            if(example.Contains("plusN")) {
                total += 2;
                suffix = "Return values:" + string.Format("\n\ttotal: {0}\n", total);
            }

            for(int i = 0; i < total; ++i) {
                expected += "$var: " + i + "\n";
            }

            expected += suffix;

            Assert.IsEmpty(stderr);
            Assert.AreEqual(0, result);
            Assert.AreEqual(expected, stdout);
        }
    }
}
