﻿//
//  TestWellFormed.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-01.
//

using NUnit.Framework;

using HeadlessDocument = Petri.Application.CLI.HeadlessDocument;

namespace Petri.Test.Examples
{
    [TestFixture]
    public class TestExamplesWellFormed
    {
        [TestCase("Embedded/EmbeddedC")]
        [TestCase("Embedded/Concurrency")]
        [TestCase("Embedded/EmbeddedCCountTo10")]
        [TestCase("Embedded/EmbeddedCLocalVariables")]
        [TestCase("CBasic")]
        [TestCase("CppBasic")]
        [TestCase("CSharpBasic")]
        [TestCase("PythonBasic")]
        [TestCase("CSync")]
        [TestCase("CppSync")]
        [TestCase("CSharpSync")]
        [TestCase("PythonSync")]
        [TestCase("CSyncWithTransitions")]
        [TestCase("CppSyncWithTransitions")]
        [TestCase("CSharpSyncWithTransitions")]
        [TestCase("PythonSyncWithTransitions")]
        [TestCase("CCountTo10")]
        [TestCase("CppCountTo10")]
        [TestCase("CSharpCountTo10")]
        [TestCase("PythonCountTo10")]
        [TestCase("CCountTo10plusN")]
        [TestCase("CppCountTo10plusN")]
        [TestCase("CSharpCountTo10plusN")]
        [TestCase("PythonCountTo10plusN")]
        [TestCase("InnerPetriNet/CInnerPetriNet")]
        [TestCase("InnerPetriNet/CppInnerPetriNet")]
        [TestCase("InnerPetriNet/CSharpInnerPetriNet")]
        [TestCase("InnerPetriNet/PythonInnerPetriNet")]
        [TestCase("ExternalPetriNet/CExternal")]
        [TestCase("ExternalPetriNet/CppExternal")]
        [TestCase("ExternalPetriNet/CSharpExternal")]
        [TestCase("ExternalPetriNet/PythonExternal")]
        [TestCase("ExternalPetriNet/CLocalVariables")]
        [TestCase("ExternalPetriNet/CppLocalVariables")]
        [TestCase("ExternalPetriNet/CSharpLocalVariables")]
        [TestCase("ExternalPetriNet/PythonLocalVariables")]
        [TestCase("ExternalPetriNet/CConcurrent")]
        [TestCase("ExternalPetriNet/CConcurrentInvocation")]
        [TestCase("ExternalPetriNet/CppConcurrent")]
        [TestCase("ExternalPetriNet/CppConcurrentInvocation")]
        [TestCase("ExternalPetriNet/CSharpConcurrent")]
        [TestCase("ExternalPetriNet/CSharpConcurrentInvocation")]
        [TestCase("ExternalPetriNet/PythonConcurrent")]
        [TestCase("ExternalPetriNet/PythonConcurrentInvocation")]
        public void TestWellFormed(string subpath)
        {
            // GIVEN a petri net loaded from an example
            var doc = new HeadlessDocument("../../../Examples/" + subpath + ".petri");

            // THEN it is a well formed petri net
            Model.ModelUtility.AssertWellFormed(doc.PetriNet);
        }
    }
}
