﻿//
//  TestCompilation.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-01.
//

using System;

using NUnit.Framework;

namespace Petri.Test.Examples.Integration
{
    [TestFixture]
    public class TestCompilation
    {
        [TestCase("clean")]
        [TestCase("all")]
        [TestCase("clean")]
        [TestCase("c10")]
        [TestCase("cpp10")]
        [TestCase("csharp10")]
        [TestCase("python310")]
        [TestCase("c10plusN")]
        [TestCase("cpp10plusN")]
        [TestCase("csharp10plusN")]
        [TestCase("python310plusN")]
        [TestCase("python210plusN")]
        public void TestExampleCompilation(string target)
        {
            var process = Petri.Application.Application.CreateProcess();
            process.StartInfo.WorkingDirectory = System.IO.Path.Combine(Environment.CurrentDirectory, "../../../Examples/Integration");
            process.StartInfo.FileName = "make";
            process.StartInfo.Arguments = target;

            process.Start();

            process.WaitForExit();

            Assert.AreEqual(0, process.ExitCode);
        }
    }
}

