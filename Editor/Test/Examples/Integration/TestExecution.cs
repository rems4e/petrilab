﻿//
//  TestExecution.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2017-05-03.
//

using System;

using NUnit.Framework;

namespace Petri.Test.Examples.Integration
{
    [TestFixture]
    public class TestExecution
    {
        [TestCase("runCCountTo10.out", 0)]
        [TestCase("runCppCountTo10.out", 0)]
        [TestCase("runCSharpCountTo10.exe", 0)]
        [TestCase("runPython3CountTo10.py", 0)]
        [TestCase("runCCountTo10plusN.out", 2)]
        [TestCase("runCppCountTo10plusN.out", 2)]
        [TestCase("runCSharpCountTo10plusN.exe", 2)]
        [TestCase("runPython3CountTo10plusN.py", 2)]
        [TestCase("runPython2CountTo10plusN.py", 2)]
        public void TestExampleExecutionDefault(string name, int delta)
        {
            var count = Variables.TestVariables.IterCount + delta;
            string expected = Variables.TestVariables.VariablesResult(count);
            if(name.Contains("plus")) {
                expected += string.Format("Petri net return values:\n\ttotal: {0}\n", count);
            }

            var process = Petri.Application.Application.CreateProcess();
            if(name.EndsWithInv("py")) {
                process.StartInfo.EnvironmentVariables["PYTHONPATH"] = "../..";
                process.StartInfo.EnvironmentVariables["LD_LIBRARY_PATH"] = ".";
            }
            process.StartInfo.WorkingDirectory = System.IO.Path.Combine(Environment.CurrentDirectory, "../../../Examples/Integration");
            process.StartInfo.FileName = System.IO.Path.Combine(process.StartInfo.WorkingDirectory, name);
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.RedirectStandardError = true;

            string output = "";
            string error = "";

            process.OutputDataReceived += (sender, ev) => {
                if(ev.Data != null) {
                    output += ev.Data + "\n";
                }
            };
            process.ErrorDataReceived += (sender, ev) => {
                if(ev.Data != null) {
                    error += ev.Data + "\n";
                }
            };

            process.Start();

            process.BeginOutputReadLine();
            process.BeginErrorReadLine();

            process.WaitForExit();

            Assert.AreEqual(0, process.ExitCode);
            Assert.AreEqual("", error);
            Assert.AreEqual(expected, output);
        }

        [Test]
        public void TestExampleExecutionParam([Values("runCCountTo10plusN.out", "runCppCountTo10plusN.out", "runCSharpCountTo10plusN.exe", "runPython3CountTo10plusN.py", "runPython2CountTo10plusN.py")]string name,
                                              [Values(-8, -4, -1, 0, 5, 10, 1132)] int delta)
        {
            var count = Variables.TestVariables.IterCount + delta;
            string expected = Variables.TestVariables.VariablesResult(count);
            expected += string.Format("Petri net return values:\n\ttotal: {0}\n", count);

            var process = Petri.Application.Application.CreateProcess();
            if(name.EndsWithInv("py")) {
                process.StartInfo.EnvironmentVariables["PYTHONPATH"] = "../..";
                process.StartInfo.EnvironmentVariables["LD_LIBRARY_PATH"] = ".";
            }
            process.StartInfo.WorkingDirectory = System.IO.Path.Combine(Environment.CurrentDirectory, "../../../Examples/Integration");
            process.StartInfo.FileName = System.IO.Path.Combine(process.StartInfo.WorkingDirectory, name);
            process.StartInfo.Arguments = delta.ToString();
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.RedirectStandardError = true;

            string output = "";
            string error = "";

            process.OutputDataReceived += (sender, ev) => {
                if(ev.Data != null) {
                    output += ev.Data + "\n";
                }
            };
            process.ErrorDataReceived += (sender, ev) => {
                if(ev.Data != null) {
                    error += ev.Data + "\n";
                }
            };

            process.Start();

            process.BeginOutputReadLine();
            process.BeginErrorReadLine();

            process.WaitForExit();

            Assert.AreEqual(0, process.ExitCode);
            Assert.AreEqual("", error);
            Assert.AreEqual(expected, output);
        }
    }
}
