//
//  TestCompilation.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-01.
//

using NUnit.Framework;

namespace Petri.Test.Examples.Sync
{
    [TestFixture]
    public class TestCompilation
    {
        [TestCase("../../../Examples/CSync.petri")]
        [TestCase("../../../Examples/CppSync.petri")]
        [TestCase("../../../Examples/CSharpSync.petri")]
        [TestCase("../../../Examples/PythonSync.petri")]
        public void TestSyncCompilation(string path)
        {
            // GIVEN launch arguments requesting code generation and compilation of the Sync.petri example
            string[] args = { "-kc", path };
            string stdout, stderr;

            // WHEN the invocation is made
            int result = CompilerUtility.InvokeCompiler(args, out stdout, out stderr);

            // THEN no error is returned.
            Assert.IsEmpty(stderr);
            Assert.IsEmpty(stdout);
            Assert.AreEqual(0, result);
        }

        [TestCase("../../../Examples/CTokens.petri")]
        [TestCase("../../../Examples/CppTokens.petri")]
        [TestCase("../../../Examples/CSharpTokens.petri")]
        [TestCase("../../../Examples/PythonTokens.petri")]
        public void TestTokensCompilation(string path)
        {
            // GIVEN launch arguments requesting code generation and compilation of the Tokens.petri example
            string[] args = { "-kc", path };
            string stdout, stderr;

            // WHEN the invocation is made
            int result = CompilerUtility.InvokeCompiler(args, out stdout, out stderr);

            // THEN no error is returned.
            Assert.IsEmpty(stderr);
            Assert.IsEmpty(stdout);
            Assert.AreEqual(0, result);
        }

        [TestCase("../../../Examples/InnerPetriNet/CInnerSync.petri")]
        [TestCase("../../../Examples/InnerPetriNet/CppInnerSync.petri")]
        [TestCase("../../../Examples/InnerPetriNet/CSharpInnerSync.petri")]
        [TestCase("../../../Examples/InnerPetriNet/PythonInnerSync.petri")]
        public void TestInnerSyncCompilation(string path)
        {
            // GIVEN launch arguments requesting code generation and compilation of the InnerSync.petri example
            string[] args = { "-kc", path };
            string stdout, stderr;

            // WHEN the invocation is made
            int result = CompilerUtility.InvokeCompiler(args, out stdout, out stderr);

            // THEN no error is returned.
            Assert.IsEmpty(stderr);
            Assert.IsEmpty(stdout);
            Assert.AreEqual(0, result);
        }

        [TestCase("../../../Examples/CSyncWithTransitions.petri")]
        [TestCase("../../../Examples/CppSyncWithTransitions.petri")]
        [TestCase("../../../Examples/CSharpSyncWithTransitions.petri")]
        [TestCase("../../../Examples/PythonSyncWithTransitions.petri")]
        public void TestSyncWithTransitionsCompilation(string path)
        {
            // GIVEN launch arguments requesting code generation and compilation of the SyncWithTransitions.petri example
            string[] args = { "-kc", path };
            string stdout, stderr;

            // WHEN the invocation is made
            int result = CompilerUtility.InvokeCompiler(args, out stdout, out stderr);

            // THEN no error is returned.
            Assert.IsEmpty(stderr);
            Assert.IsEmpty(stdout);
            Assert.AreEqual(0, result);
        }

    }
}

