//
//  TestExecution.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-01.
//

using System.Collections.Generic;
using System.Linq;

using NUnit.Framework;

namespace Petri.Test.Examples.Sync
{
    [TestFixture]
    public class TestExecution
    {
        [TestCase("../../../Examples/CSync.petri")]
        [TestCase("../../../Examples/CppSync.petri")]
        [TestCase("../../../Examples/CSharpSync.petri")]
        [TestCase("../../../Examples/PythonSync.petri")]
        public void TestSyncExecution(string path)
        {
            // GIVEN launch arguments requesting execution of the Sync.petri example
            string[] args = { "-r", path };
            string stdout, stderr;

            // WHEN the invocation is made
            int result = CompilerUtility.InvokeCompiler(args, out stdout, out stderr);

            // THEN no error is returned.
            Assert.IsEmpty(stderr);
            Assert.AreEqual("$i: 5000\n", stdout);
            Assert.AreEqual(0, result);
        }

        [TestCase("../../../Examples/CTokens.petri")]
        [TestCase("../../../Examples/CppTokens.petri")]
        [TestCase("../../../Examples/CSharpTokens.petri")]
        [TestCase("../../../Examples/PythonTokens.petri")]
        public void TestTokensExecution(string path)
        {
            // GIVEN launch arguments requesting execution of the Tokens.petri example
            string[] args = { "-r", path };
            string stdout, stderr;

            // WHEN the invocation is made
            int result = CompilerUtility.InvokeCompiler(args, out stdout, out stderr);

            // THEN no error is returned.
            Assert.IsEmpty(stderr);
            StringAssert.StartsWith("This is text\nSome string\n", stdout);
            StringAssert.EndsWith("Another value\n", stdout);
            var lines = stdout.Split(new char[] { '\n' }, System.StringSplitOptions.RemoveEmptyEntries);
            Assert.AreEqual(6, lines.Count());
            Assert.AreEqual(2, lines.Count((str) => str == "Some string"));
            Assert.AreEqual(3, lines.Count((str) => str == "Another value"));
            Assert.AreEqual(0, result);
        }

        [TestCase("../../../Examples/InnerPetriNet/CInnerSync.petri")]
        [TestCase("../../../Examples/InnerPetriNet/CppInnerSync.petri")]
        [TestCase("../../../Examples/InnerPetriNet/CSharpInnerSync.petri")]
        [TestCase("../../../Examples/InnerPetriNet/PythonInnerSync.petri")]
        public void TestInnerSyncExecution(string path)
        {
            // GIVEN launch arguments requesting execution of the InnerSync.petri example
            string[] args = { "-r", path };
            string stdout, stderr;

            // WHEN the invocation is made
            int result = CompilerUtility.InvokeCompiler(args, out stdout, out stderr);

            // THEN no error is returned.
            Assert.IsEmpty(stderr);
            Assert.AreEqual("This is text\nSome string\nSome string\nSome string\n", stdout);
            Assert.AreEqual(0, result);
        }

        [TestCase("../../../Examples/CSyncWithTransitions.petri")]
        [TestCase("../../../Examples/CppSyncWithTransitions.petri")]
        [TestCase("../../../Examples/CSharpSyncWithTransitions.petri")]
        [TestCase("../../../Examples/PythonSyncWithTransitions.petri")]
        public void TestSyncWithTransitionsExecution(string path)
        {
            // GIVEN launch arguments requesting execution of the Sync.petri example
            string[] args = { "-r", path };
            string stdout, stderr;

            // WHEN the invocation is made
            int result = CompilerUtility.InvokeCompiler(args, out stdout, out stderr);

            var split = new List<string>(stdout.Split(new char[] { '\n' }, System.StringSplitOptions.RemoveEmptyEntries));

            Assert.AreEqual(20, split.Count);

            split.Sort();

            var joined = string.Join("\n", split);

            // THEN no error is returned.
            Assert.IsEmpty(stderr);
            Assert.AreEqual(
                Variables.TestVariables.VariablesResult(10, "var") + Variables.TestVariables.VariablesResult(10, "var2"),
                joined + "\n"
            );
            Assert.AreEqual(0, result);
        }
    }
}

