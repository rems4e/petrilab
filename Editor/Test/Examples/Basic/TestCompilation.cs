//
//  TestCompilation.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-01.
//

using NUnit.Framework;

namespace Petri.Test.Examples.Basic
{
    [TestFixture]
    public class TestCompilation
    {
        [TestCase("../../../Examples/CBasic.petri")]
        [TestCase("../../../Examples/CppBasic.petri")]
        [TestCase("../../../Examples/CSharpBasic.petri")]
        [TestCase("../../../Examples/PythonBasic.petri")]
        [TestCase("../../../Examples/Python2Basic.petri")]
        [TestCase("../../../Examples/Python3Basic.petri")]
        [TestCase("../../../Examples/InnerPetriNet/CInnerPetriNet.petri")]
        [TestCase("../../../Examples/InnerPetriNet/CppInnerPetriNet.petri")]
        [TestCase("../../../Examples/InnerPetriNet/CSharpInnerPetriNet.petri")]
        [TestCase("../../../Examples/InnerPetriNet/PythonInnerPetriNet.petri")]
        public void TestExampleCompilation(string path)
        {
            // GIVEN launch arguments requesting code generation and compilation of the petri net example
            string[] args = { "-k", "-g", "-c", path };
            string stdout, stderr;

            // WHEN the invocation is made
            int result = CompilerUtility.InvokeCompiler(args, out stdout, out stderr);

            // THEN no error is returned.
            Assert.IsEmpty(stderr);
            Assert.IsEmpty(stdout);
            Assert.AreEqual(0, result);
        }
    }
}

