//
//  TestExecution.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-01.
//

using NUnit.Framework;

namespace Petri.Test.Examples.Basic
{
    [TestFixture]
    public class TestExecution
    {
        [TestCase("../../../Examples/CBasic.petri")]
        [TestCase("../../../Examples/CppBasic.petri")]
        [TestCase("../../../Examples/CSharpBasic.petri")]
        [TestCase("../../../Examples/PythonBasic.petri")]
        public void TestExampleExecution(string path)
        {
            // GIVEN launch arguments requesting execution of the petri net example
            string[] args = { "-r", path };
            string stdout, stderr;

            // WHEN the invocation is made
            int result = CompilerUtility.InvokeCompiler(args, out stdout, out stderr);

            // THEN no error is returned.
            Assert.AreEqual(0, result);
            Assert.AreEqual("Action1!\nCondition1!\nAction1!\nCondition1!\nAction2!\n", stdout);
            Assert.IsEmpty(stderr);
        }

        [TestCase("../../../Examples/Python2Basic.petri")]
        public void TestPython2ExampleExecution(string path)
        {
            // GIVEN launch arguments requesting execution of the petri net example
            string[] args = { "-r", path };
            string stdout, stderr;

            // WHEN the invocation is made
            int result = CompilerUtility.InvokeCompiler(args, out stdout, out stderr);

            // THEN no error is returned.
            Assert.AreEqual(0, result);
            Assert.AreEqual("0 == 0\n2 == 2\n", stdout);
            Assert.IsEmpty(stderr);
        }

        [TestCase("../../../Examples/Python3Basic.petri")]
        public void TestPython3ExampleExecution(string path)
        {
            // GIVEN launch arguments requesting execution of the petri net example
            string[] args = { "-r", path };
            string stdout, stderr;

            // WHEN the invocation is made
            int result = CompilerUtility.InvokeCompiler(args, out stdout, out stderr);

            // THEN no error is returned.
            Assert.AreEqual(0, result);
            Assert.AreEqual("0.5 == 0.5\n3 == 3\n", stdout);
            Assert.IsEmpty(stderr);
        }
    }
}

