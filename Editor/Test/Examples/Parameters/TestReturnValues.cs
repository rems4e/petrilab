//
//  TestReturnValues.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-30.
//

using NUnit.Framework;

namespace Petri.Test.Examples.Parameters
{
    [TestFixture]
    public class TestReturnValues
    {
        public static readonly int DefaultDelta = 2;

        [Test, Order(1)]
        [TestCase("CReturnValue")]
        [TestCase("CppReturnValue")]
        [TestCase("CSharpReturnValue")]
        [TestCase("PythonReturnValue")]
        public void TestExampleCompilation(string path)
        {
            // GIVEN launch arguments requesting code generation and compilation of the example
            string[] args = { "-kcg", string.Format("../../../Examples/ExternalPetriNet/{0}.petri", path) };
            string stdout, stderr;

            // WHEN the invocation is made
            int result = CompilerUtility.InvokeCompiler(args, out stdout, out stderr);

            // THEN no error is returned.
            Assert.IsEmpty(stderr);
            Assert.IsEmpty(stdout);
            Assert.AreEqual(0, result);
        }

        [Test, Order(2)]
        public void TestExampleExecutionReturnValues([Values("CReturnValue", "CppReturnValue", "CSharpReturnValue", "PythonReturnValue")] string path,
                                                     [Values(-20, -5674, 496)] int offset)
        {
            // GIVEN launch arguments requesting execution of the example
            string[] args = { "-r", "--log-return-values", "--args", string.Format("$offset:{0}", offset), string.Format("../../../Examples/ExternalPetriNet/{0}.petri", path) };
            string stdout, stderr;

            // WHEN the invocation is made
            int result = CompilerUtility.InvokeCompiler(args, out stdout, out stderr);

            // THEN no error is returned.
            Assert.IsEmpty(stderr);
            Assert.AreEqual(string.Format("{0}\n\tresult: {1}\n", "Return values:", offset + 60), stdout);
            Assert.AreEqual(0, result);
        }

        [Test, Order(2)]
        public void TestExampleExecutionNoReturnValues([Values("CReturnValue", "CppReturnValue", "CSharpReturnValue", "PythonReturnValue")] string path,
                                                     [Values(-20, -5674, 496)] int offset)
        {
            // GIVEN launch arguments requesting execution of the example
            string[] args = { "-r", "--log-nothing", "--args", string.Format("$offset:{0}", offset), string.Format("../../../Examples/ExternalPetriNet/{0}.petri", path) };
            string stdout, stderr;

            // WHEN the invocation is made
            int result = CompilerUtility.InvokeCompiler(args, out stdout, out stderr);

            // THEN no error is returned.
            Assert.IsEmpty(stderr);
            Assert.IsEmpty(stdout);
            Assert.AreEqual(0, result);
        }

        [Test, Order(2)]
        public void TestDirectExecution([Values("CReturnValue", "CppReturnValue", "CSharpReturnValue", "PythonReturnValue")] string path,
                                        [Values(-20, -5674, 496)] int offset)
        {
            var doc = new Petri.Application.CLI.HeadlessDocument(string.Format("../../../Examples/ExternalPetriNet/{0}.petri", path));
            var dir = System.Environment.CurrentDirectory;
            try {
                System.Environment.CurrentDirectory = System.IO.Directory.GetParent(doc.Path).FullName;

                var variables = Petri.Application.CLI.CLIApplication.RunDocument(
                    new Petri.Application.ConsoleOutput(false),
                    doc,
                    new System.Collections.Generic.Dictionary<System.UInt32, System.Int64> { { 0, offset } },
                    null,
                    false,
                    false
                );

                Assert.AreEqual(1, variables.Count);
                CollectionAssert.Contains(variables.Keys, "result");
                Assert.AreEqual(variables["result"], 60 + offset);
            } finally {
                System.Environment.CurrentDirectory = dir;
            }
        }
    }
}
