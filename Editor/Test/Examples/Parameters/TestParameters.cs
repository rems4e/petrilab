﻿//
//  TestParameters.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2017-06-08.
//

using NUnit.Framework;

namespace Petri.Test.Examples.Parameters
{
    [TestFixture]
    public class TestParameters
    {
        public static readonly int DefaultDelta = 2;

        [TestCase("CCountTo10plusN")]
        [TestCase("CppCountTo10plusN")]
        [TestCase("CSharpCountTo10plusN")]
        [TestCase("PythonCountTo10plusN")]
        [TestCase("Python2CountTo10plusN")]
        [TestCase("Python3CountTo10plusN")]
        [TestCase("CMultipleParam")]
        [TestCase("CppMultipleParam")]
        [TestCase("CSharpMultipleParam")]
        [TestCase("PythonMultipleParam")]
        [TestCase("ExternalPetriNet/CMultipleArg")]
        [TestCase("ExternalPetriNet/CppMultipleArg")]
        [TestCase("ExternalPetriNet/CSharpMultipleArg")]
        [TestCase("ExternalPetriNet/PythonMultipleArg")]
        public void TestExampleCompilation(string path)
        {
            // GIVEN launch arguments requesting code generation and compilation of the example
            string[] args = { "-kcg", string.Format("../../../Examples/{0}.petri", path) };
            string stdout, stderr;

            // WHEN the invocation is made
            int result = CompilerUtility.InvokeCompiler(args, out stdout, out stderr);

            // THEN no error is returned.
            Assert.IsEmpty(stderr);
            Assert.IsEmpty(stdout);
            Assert.AreEqual(0, result);
        }

        [TestCase("CCountTo10plusN")]
        [TestCase("CppCountTo10plusN")]
        [TestCase("CSharpCountTo10plusN")]
        [TestCase("PythonCountTo10plusN")]
        [TestCase("Python2CountTo10plusN")]
        [TestCase("Python3CountTo10plusN")]
        public void TestExampleExecutionDefaultValueNoReturnValues(string path)
        {
            // GIVEN launch arguments requesting code generation and compilation of the example
            string[] args = { "-r", "--log-nothing", string.Format("../../../Examples/{0}.petri", path) };
            string stdout, stderr;

            // WHEN the invocation is made
            int result = CompilerUtility.InvokeCompiler(args, out stdout, out stderr);

            // THEN no error is returned.
            Assert.IsEmpty(stderr);
            Assert.AreEqual(0, result);
            Assert.AreEqual(Variables.TestVariables.VariablesResult(Variables.TestVariables.IterCount + DefaultDelta), stdout);
        }

        [TestCase("CCountTo10plusN")]
        [TestCase("CppCountTo10plusN")]
        [TestCase("CSharpCountTo10plusN")]
        [TestCase("PythonCountTo10plusN")]
        [TestCase("Python2CountTo10plusN")]
        [TestCase("Python3CountTo10plusN")]
        public void TestExampleExecutionDefaultValueWithReturnValues(string path)
        {
            // GIVEN launch arguments requesting code generation and compilation of the example
            string[] args = { "-r", "--log-return-values", string.Format("../../../Examples/{0}.petri", path) };
            string stdout, stderr;

            // WHEN the invocation is made
            int result = CompilerUtility.InvokeCompiler(args, out stdout, out stderr);

            // THEN no error is returned.
            Assert.IsEmpty(stderr);
            Assert.AreEqual(0, result);
            Assert.AreEqual(string.Format("{0}{1}\n\ttotal: {2}\n", Variables.TestVariables.VariablesResult(Variables.TestVariables.IterCount + DefaultDelta), "Return values:", Variables.TestVariables.IterCount + DefaultDelta), stdout);
        }

        [Test]
        public void TestExampleExecutionCustomValueNoReturnValues([Values("CCountTo10plusN", "CppCountTo10plusN", "CSharpCountTo10plusN", "PythonCountTo10plusN", "Python2CountTo10plusN", "Python3CountTo10plusN")] string path,
                                                                  [Values(-7, -5, -2, 0, 8, 9, 997)] int delta)
        {
            // GIVEN launch arguments requesting code generation and compilation of the example
            string[] args = { "-r", "--args", string.Format("$deltaCount:{0}", delta), "--log-nothing", string.Format("../../../Examples/{0}.petri", path) };
            string stdout, stderr;

            // WHEN the invocation is made
            int result = CompilerUtility.InvokeCompiler(args, out stdout, out stderr);

            // THEN no error is returned.
            Assert.IsEmpty(stderr);
            Assert.AreEqual(0, result);
            Assert.AreEqual(Variables.TestVariables.VariablesResult(Variables.TestVariables.IterCount + delta), stdout);
        }

        [Test]
        public void TestExampleExecutionCustomValueWithReturnValues([Values("CCountTo10plusN", "CppCountTo10plusN", "CSharpCountTo10plusN", "PythonCountTo10plusN", "Python2CountTo10plusN", "Python3CountTo10plusN")] string path,
                                                                    [Values(-7, -5, -2, 0, 8, 9, 997)] int delta)
        {
            // GIVEN launch arguments requesting code generation and compilation of the example
            string[] args = { "-r", "--log-return-values", "--args", string.Format("$deltaCount:{0}", delta), string.Format("../../../Examples/{0}.petri", path) };
            string stdout, stderr;

            // WHEN the invocation is made
            int result = CompilerUtility.InvokeCompiler(args, out stdout, out stderr);

            // THEN no error is returned.
            Assert.IsEmpty(stderr);
            Assert.AreEqual(0, result);
            Assert.AreEqual(string.Format("{0}{1}\n\ttotal: {2}\n", Variables.TestVariables.VariablesResult(Variables.TestVariables.IterCount + delta), "Return values:", Variables.TestVariables.IterCount + delta), stdout);
        }

        public string MultipleArgResult(int first, int second, int third)
        {
            return string.Format("$param0: {0} $param1: {1} $param2: {2}\n", first, second, third);
        }

        [Test]
        public void TestMultipleParam([Values("CMultipleParam", "CppMultipleParam", "CSharpMultipleParam", "PythonMultipleParam")] string path)
        {
            // GIVEN launch arguments requesting code generation and compilation of the example
            string[] args = { "-r", string.Format("../../../Examples/{0}.petri", path) };
            string stdout, stderr;

            // WHEN the invocation is made
            int result = CompilerUtility.InvokeCompiler(args, out stdout, out stderr);

            // THEN no error is returned.
            Assert.IsEmpty(stderr);
            Assert.AreEqual(0, result);
            Assert.AreEqual(MultipleArgResult(2, 3, 4), stdout);
        }

        [Test]
        public void TestMultipleArg([Values("CMultipleArg", "CppMultipleArg", "CSharpMultipleArg", "PythonMultipleArg")] string path)
        {
            // GIVEN launch arguments requesting code generation and compilation of the example
            string[] args = { "-r", string.Format("../../../Examples/ExternalPetriNet/{0}.petri", path) };
            string stdout, stderr;

            // WHEN the invocation is made
            int result = CompilerUtility.InvokeCompiler(args, out stdout, out stderr);

            // THEN no error is returned.
            Assert.IsEmpty(stderr);
            Assert.AreEqual(0, result);
            Assert.AreEqual(MultipleArgResult(0, 0, 0), stdout);
        }

        [Test]
        public void TestMultipleParamAndArgCustom(
            [Values("CMultipleParam", "CppMultipleParam", "CSharpMultipleParam", "PythonMultipleParam",
                    "ExternalPetriNet/CMultipleArg", "ExternalPetriNet/CppMultipleArg", "ExternalPetriNet/CSharpMultipleArg", "ExternalPetriNet/PythonMultipleArg")] string path,
            [Values(3, 7)] int first,
            [Values(-3, -12)] int second,
            [Values(-456, 0)] int third,
            [Values(0, 1, 2, 3, 4, 5)] int permutation
        )
        {
            string format = null;
            switch(permutation) {
            case 0:
                format = "$param0:{0} $param1:{1} $param2:{2}";
                break;
            case 1:
                format = "$param0:{0} $param2:{2} $param1:{1}";
                break;
            case 2:
                format = "$param1:{1} $param0:{0} $param2:{2}";
                break;
            case 3:
                format = "$param1:{1} $param2:{2} $param0:{0}";
                break;
            case 4:
                format = "$param2:{2} $param0:{0} $param1:{1}";
                break;
            case 5:
                format = "$param2:{2} $param1:{1} $param0:{0}";
                break;
            }

            // GIVEN launch arguments requesting code generation and compilation of the example
            string[] args = { "-r", "--args", string.Format(format, first, second, third), string.Format("../../../Examples/{0}.petri", path) };
            string stdout, stderr;

            // WHEN the invocation is made
            int result = CompilerUtility.InvokeCompiler(args, out stdout, out stderr);

            // THEN no error is returned.
            Assert.IsEmpty(stderr);
            Assert.AreEqual(0, result);
            Assert.AreEqual(MultipleArgResult(first, second, third), stdout);
        }
    }
}
