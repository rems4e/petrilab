﻿//
//  TestEmbedded.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-01.
//

using System.Linq;

using NUnit.Framework;

namespace Petri.Test.Examples.Embedded
{
    [TestFixture]
    public class TestEmbedded
    {
        [Test]
        public void TestEmbeddedExecution()
        {
            // GIVEN launch arguments requesting execution of the EmbC.petri example
            string[] args = { "-kr", "../../../Examples/Embedded/EmbeddedC.petri" };
            string stdout, stderr;

            // WHEN the invocation is made
            int result = CompilerUtility.InvokeCompiler(args, out stdout, out stderr);

            // THEN no error is returned and the petri net execution gives the expected result.
            Assert.IsEmpty(stderr);
            Assert.AreEqual(string.Join("",
                                        Enumerable.Repeat("Action1!\nCondition1!\nCondition2!\nAction2!\n",
                                                          4)),
                            stdout);
            Assert.AreEqual(0, result);
        }

        [Test]
        public void TestConcurrency()
        {
            // GIVEN launch arguments requesting execution of the Concurrency.petri example
            string[] args = { "-kr", "../../../Examples/Embedded/Concurrency.petri" };

            string stdout = "";
            string stderr = "";
            int result = -1;
            // WHEN the invocation is made
            var task = System.Threading.Tasks.Task.Run(() => {
                result = CompilerUtility.InvokeCompiler(args, out stdout, out stderr);
            });

            task.Wait(5000);
            if(!task.IsCompleted) {
                Assert.Fail("The petri net is deadlocked!");
            }

            // THEN no error is returned and the petri net execution gives the expected result without blocking.
            Assert.IsEmpty(stderr);
            StringAssert.EndsWith("Finished!\n", stdout);
            Assert.AreEqual(0, result);
        }
    }
}

