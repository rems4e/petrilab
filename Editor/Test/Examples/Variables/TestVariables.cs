﻿//
//  TestVariables.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-30.
//

using NUnit.Framework;

namespace Petri.Test.Examples.Variables
{
    [TestFixture]
    public class TestVariables
    {
        public static readonly int IterCount = 10;

        public static string VariablesResult(int upTo, string name = "var")
        {
            var result = "";
            for(int i = 0; i < upTo; ++i) {
                result += string.Format("${0}: {1}\n", name, i);
            }

            return result;
        }

        [TestCase("CCountTo10")]
        [TestCase("CppCountTo10")]
        [TestCase("CSharpCountTo10")]
        [TestCase("PythonCountTo10")]
        public void TestExampleCompilation(string path)
        {
            // GIVEN launch arguments requesting code generation and compilation of the example
            string[] args = { "-kcg", string.Format("../../../Examples/{0}.petri", path) };
            string stdout, stderr;

            // WHEN the invocation is made
            int result = CompilerUtility.InvokeCompiler(args, out stdout, out stderr);

            // THEN no error is returned.
            Assert.IsEmpty(stderr);
            Assert.IsEmpty(stdout);
            Assert.AreEqual(0, result);
        }

        [TestCase("CCountTo10")]
        [TestCase("CppCountTo10")]
        [TestCase("CSharpCountTo10")]
        [TestCase("PythonCountTo10")]
        public void TestExampleExecution(string path)
        {
            // GIVEN launch arguments requesting code generation and compilation of the example
            string[] args = { "-r", string.Format("../../../Examples/{0}.petri", path) };
            string stdout, stderr;

            // WHEN the invocation is made
            int result = CompilerUtility.InvokeCompiler(args, out stdout, out stderr);

            // THEN no error is returned.
            Assert.IsEmpty(stderr);
            Assert.AreEqual(0, result);
            Assert.AreEqual(VariablesResult(IterCount), stdout);
        }

        [Test]
        public void TestEmbeddedCExampleExecution()
        {
            // GIVEN launch arguments requesting code generation and compilation of the CCountTo10.petri example
            string[] args = { "-gr", "../../../Examples/Embedded/EmbeddedCCountTo10.petri" };
            string stdout, stderr;

            // WHEN the invocation is made
            int result = CompilerUtility.InvokeCompiler(args, out stdout, out stderr);

            // THEN no error is returned.
            Assert.IsEmpty(stderr);
            Assert.AreEqual(0, result);
            Assert.AreEqual(VariablesResult(IterCount), stdout);
        }
    }
}
