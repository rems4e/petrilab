//
//  ModelUtility.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-10-18.
//

using System;
using System.Collections.Generic;

using NUnit.Framework;

using Petri.Model;

namespace Petri.Test.Model
{
    /// <summary>
    /// Model utility.
    /// </summary>
    public static class ModelUtility
    {
        /// <summary>
        /// Asserts that the passed in petri net is well formed, i.e. it has all of its relations etc.
        /// </summary>
        /// <param name="petriNet">Petri net.</param>
        public static void AssertWellFormed(Petri.Model.PetriNet petriNet)
        {
            Assert.NotNull(petriNet);
            AssertWellFormedHelper(petriNet, new HashSet<UInt64>(), false);
        }

        static void AssertWellFormedHelper(Petri.Model.PetriNet petriNet, HashSet<UInt64> idSet, bool flat)
        {
            if(!flat) {
                idSet.Add(petriNet.GlobalID);
            }
            foreach(var s in petriNet.States) {
                CollectionAssert.DoesNotContain(idSet, s.GlobalID);
                if(s is InnerPetriNet) {
                    AssertWellFormedHelper(s as InnerPetriNet, idSet, false);
                } else {
                    idSet.Add(s.GlobalID);
                }
                if(petriNet is ExternalInnerPetriNet) {
                    Assert.AreEqual(((ExternalInnerPetriNet)petriNet).ExternalDocument.PetriNet, s.Parent);
                } else {
                    Assert.AreEqual(petriNet, s.Parent);
                }
            }

            foreach(var t in petriNet.Transitions) {
                CollectionAssert.DoesNotContain(idSet, t.GlobalID);
                idSet.Add(t.GlobalID);
                if(petriNet is ExternalInnerPetriNet) {
                    Assert.AreEqual(((ExternalInnerPetriNet)petriNet).ExternalDocument.PetriNet, t.Parent);
                } else {
                    Assert.AreEqual(petriNet, t.Parent);
                }
                CollectionAssert.Contains(petriNet.States, t.Before);
                CollectionAssert.Contains(petriNet.States, t.After);
                CollectionAssert.Contains(t.Before.TransitionsAfter, t);
                CollectionAssert.Contains(t.After.TransitionsBefore, t);
            }

            foreach(var c in petriNet.Comments) {
                CollectionAssert.DoesNotContain(idSet, c.GlobalID);
                idSet.Add(c.GlobalID);
                if(petriNet is ExternalInnerPetriNet) {
                    Assert.AreEqual(((ExternalInnerPetriNet)petriNet).ExternalDocument.PetriNet, c.Parent);
                } else {
                    Assert.AreEqual(petriNet, c.Parent);
                }
            }
        }

        /// <summary>
        /// Asserts that the passed in petri net is well formed, i.e. it has all of its relations etc.
        /// It also must not contain any inner petri net (must be flat).
        /// </summary>
        /// <param name="petriNet">Petri net.</param>
        public static void AssertWellFormedFlat(Petri.Model.PetriNet petriNet)
        {
            Assert.NotNull(petriNet);
            AssertWellFormedHelper(petriNet, new HashSet<UInt64>(), true);

            foreach(var s in petriNet.States) {
                Assert.True(s is Petri.Model.Action || s is ExitPoint || s is ExternalInnerPetriNetProxy);
                Assert.AreEqual(petriNet, s.Parent);
            }

            foreach(var t in petriNet.Transitions) {
                Assert.AreEqual(petriNet, t.Parent);
                CollectionAssert.Contains(petriNet.States, t.Before);
                CollectionAssert.Contains(petriNet.States, t.After);
                CollectionAssert.Contains(t.Before.TransitionsAfter, t);
                CollectionAssert.Contains(t.After.TransitionsBefore, t);
            }
        }
    }
}
