//
//  TestFlattenReachable.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-01.
//

using System;
using System.Collections.Generic;

using NUnit.Framework;

using Petri.Application;
using Petri.Model;

using HeadlessDocument = Petri.Application.CLI.HeadlessDocument;

namespace Petri.Test.Model.PetriNet
{
    [TestFixture]
    public class TestFlattenReachable
    {
        [Test]
        public void TestFlattenReachability([Values(
            "InnerPetriNet/CInnerPetriNet",
            "InnerPetriNet/CppInnerPetriNet",
            "InnerPetriNet/CSharpInnerPetriNet",
            "InnerPetriNet/PythonInnerPetriNet",
            "Embedded/EmbeddedC",
            "Embedded/Concurrency",
            "Embedded/EmbeddedCCountTo10",
            //"Embedded/EmbeddedCLocalVariables",
            "CBasic",
            "CppBasic",
            "CSharpBasic",
            "PythonBasic",
            "CSync",
            "CppSync",
            "CSharpSync",
            "PythonSync",
            "CCountTo10",
            "CppCountTo10",
            "CSharpCountTo10",
            "PythonCountTo10",
            "CCountTo10plusN",
            "CppCountTo10plusN",
            "CSharpCountTo10plusN",
            "PythonCountTo10plusN",
            "ExternalPetriNet/CExternal",
            "ExternalPetriNet/CLocalVariables",
            "ExternalPetriNet/CppExternal",
            "ExternalPetriNet/CppLocalVariables",
            "ExternalPetriNet/CSharpExternal",
            "ExternalPetriNet/CSharpLocalVariables")] string subpath)
        {
            var doc = new HeadlessDocument("../../../Examples/" + subpath + ".petri");
            var reachable = new Dictionary<UInt64, bool>();
            var entities = doc.PetriNet.BuildAllEntitiesList();
            UInt64 offset = 0;
            foreach(var e in entities) {
                if(e is State) {
                    if(e.Document is ExternalDocument) {
                        offset += e.Document.Parent.ExternalDocument.FlatPetriNet.NextID;
                    }
                    reachable.Add(e.ID + offset, e.Document.IsReachable((State)e));
                }
            }

            var flattened = doc.FlatPetriNet;
            foreach(var s in flattened.States) {
                if(s != flattened.ExitPoint) {
                    Assert.AreEqual(reachable[s.ID], flattened.IsReachable(s));
                }
            }

            Assert.True(flattened.IsReachable(flattened.ExitPoint));
        }
    }
}
