//
//  TestFlattenWellFormed.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-01.
//

using NUnit.Framework;

using Petri.Model;

using HeadlessDocument = Petri.Application.CLI.HeadlessDocument;

namespace Petri.Test.Model.PetriNet
{
    [TestFixture]
    public class TestFlattenWellFormed
    {
        [Test]
        public void TestWellFormedFlat([Values(
            "InnerPetriNet/CInnerPetriNet",
            "InnerPetriNet/CppInnerPetriNet",
            "InnerPetriNet/CSharpInnerPetriNet",
            "InnerPetriNet/PythonInnerPetriNet",
            "Embedded/EmbeddedC",
            "Embedded/Concurrency",
            "Embedded/EmbeddedCCountTo10",
            "Embedded/EmbeddedCLocalVariables",
            "CBasic",
            "CppBasic",
            "CSharpBasic",
            "PythonBasic",
            "CSync",
            "CppSync",
            "CSharpSync",
            "PythonSync",
            "CCountTo10",
            "CppCountTo10",
            "CSharpCountTo10",
            "PythonCountTo10",
            "CCountTo10plusN",
            "CppCountTo10plusN",
            "CSharpCountTo10plusN",
            "PythonCountTo10plusN",
            "ExternalPetriNet/CExternal",
            "ExternalPetriNet/CLocalVariables",
            "ExternalPetriNet/CppExternal",
            "ExternalPetriNet/CppLocalVariables",
            "ExternalPetriNet/CSharpExternal",
            "ExternalPetriNet/CSharpLocalVariables",
            "ExternalPetriNet/PythonExternal",
            "ExternalPetriNet/PythonLocalVariables")] string subpath)
        {
            // GIVEN a petri net loaded from an example
            var doc = new HeadlessDocument("../../../Examples/" + subpath + ".petri");

            // WHEN it is flattened
            var petriNet = FlatPetriNet.Flatten(doc.PetriNet).Item1;

            // THEN it is a well formed petri net
            ModelUtility.AssertWellFormedFlat(petriNet);
        }
    }
}
