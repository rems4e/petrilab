//
//  TestFlatten.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-04-24.
//

using System.Linq;
using System.Collections.Generic;

using NUnit.Framework;

using Petri.Code;
using Petri.Model;

using HeadlessDocument = Petri.Application.CLI.HeadlessDocument;

namespace Petri.Test.Model.PetriNet
{
    public class TestFlatten
    {
        [TestCase(Language.C)]
        [TestCase(Language.Cpp)]
        [TestCase(Language.CSharp)]
        [TestCase(Language.EmbeddedC)]
        [TestCase(Language.Python)]
        public void TestEmptyPetriNet(Language language)
        {
            var doc = new HeadlessDocument(language);

            // GIVEN an empty petri net
            var petriNet = new RootPetriNet(doc, null);
            doc.PetriNet = petriNet;

            // WHEN it is flattened
            var flattened = doc.FlatPetriNet;

            // THEN the new petri net contains only the exit point
            Assert.IsInstanceOf<FlatPetriNet>(flattened);
            Assert.AreEqual(2, flattened.States.Count);
            Assert.AreEqual("Root_Entry", flattened.States[0].Name);
            Assert.AreEqual("Root_End", flattened.States[1].Name);
            CollectionAssert.IsEmpty(flattened.Transitions);
        }

        [TestCase(Language.C)]
        [TestCase(Language.Cpp)]
        [TestCase(Language.CSharp)]
        [TestCase(Language.EmbeddedC)]
        [TestCase(Language.Python)]
        public void TestSimplePetriNet(Language language)
        {
            var doc = new HeadlessDocument(language);

            // GIVEN a simple petri net
            var petriNet = new RootPetriNet(doc, null);
            doc.PetriNet = petriNet;
            var a1 = new Action(doc.EntityFactory, petriNet, true);
            var a2 = new Action(doc.EntityFactory, petriNet, false);

            var t1 = new Transition(doc.EntityFactory, petriNet, a1, a2);

            petriNet.States.Add(a1);
            petriNet.States.Add(a2);
            petriNet.Transitions.Add(t1);

            // WHEN it is flattened
            var flattened = doc.FlatPetriNet;

            // THEN the new petri net is of the same configuration as the original petri net
            Assert.IsInstanceOf<FlatPetriNet>(flattened);
            Assert.AreEqual(4, flattened.States.Count);
            Assert.AreEqual(2, flattened.Transitions.Count);
            CollectionAssert.AllItemsAreInstancesOfType(flattened.States, typeof(Action));
        }

        [TestCase(Language.C)]
        [TestCase(Language.Cpp)]
        [TestCase(Language.CSharp)]
        [TestCase(Language.EmbeddedC)]
        [TestCase(Language.Python)]
        public void TestSimpleInnerPetriNet(Language language)
        {
            var doc = new HeadlessDocument(language);

            // GIVEN a petri net containing one InnerPetriNet
            var petriNet = new RootPetriNet(doc, null);
            doc.PetriNet = petriNet;
            var inner1 = new LocalInnerPetriNet(doc.EntityFactory, petriNet, true);
            var a1 = new Action(doc.EntityFactory, inner1, true);
            var a2 = new Action(doc.EntityFactory, inner1, false);

            var t1 = new Transition(doc.EntityFactory, inner1, a1, a2);
            var t2 = new Transition(doc.EntityFactory, inner1, a2, inner1.ExitPoint);
            a1.AddTransitionAfter(t1);
            a1.AddTransitionAfter(t2);
            a2.AddTransitionBefore(t1);
            inner1.ExitPoint.AddTransitionBefore(t1);

            inner1.States.Add(a1);
            inner1.States.Add(a2);
            inner1.Transitions.Add(t1);
            inner1.Transitions.Add(t2);

            petriNet.States.Add(inner1);

            // WHEN it is flattened
            var flattened = doc.FlatPetriNet;

            // THEN the new petri net is of the same configuration as the original petri net
            Assert.IsInstanceOf<FlatPetriNet>(flattened);
            Assert.AreEqual(6, flattened.States.Count);
            Assert.AreEqual(4, flattened.Transitions.Count);
            CollectionAssert.AllItemsAreInstancesOfType(flattened.States, typeof(Action));
        }

        [TestCase("../../../Examples/InnerPetriNet/CInnerPetriNet.petri")]
        [TestCase("../../../Examples/InnerPetriNet/CppInnerPetriNet.petri")]
        [TestCase("../../../Examples/InnerPetriNet/CSharpInnerPetriNet.petri")]
        [TestCase("../../../Examples/InnerPetriNet/PythonInnerPetriNet.petri")]
        public void TestInnerPetriNetFromFile(string path)
        {
            // GIVEN a petri net loaded from the InnerPetriNet example
            var doc = new HeadlessDocument(path);

            // WHEN it is flattened
            var flattened = doc.FlatPetriNet;

            // THEN it contains only one starting state
            Assert.AreEqual(1, flattened.States.Count((State s) => s.IsStartState));
        }

        [TestCase("../../../Examples/InnerPetriNet/CInnerPetriNet.petri")]
        [TestCase("../../../Examples/InnerPetriNet/CppInnerPetriNet.petri")]
        [TestCase("../../../Examples/InnerPetriNet/CSharpInnerPetriNet.petri")]
        [TestCase("../../../Examples/InnerPetriNet/PythonInnerPetriNet.petri")]
        public void TestRuntime(string path)
        {
            // GIVEN launch arguments requesting execution of the InnerPetriNet.petri example
            string[] args = { "-kr", path };
            string stdout, stderr;

            // WHEN the invocation is made
            int result = CompilerUtility.InvokeCompiler(args, out stdout, out stderr);
            var output = stdout.Split(new char[] { '\n' }, System.StringSplitOptions.RemoveEmptyEntries);

            // THEN no error is returned andd the expected output is met.
            Assert.AreEqual(0, result);
            var expected = new HashSet<string> {
                "Action I1_A2, ID 10 completed.",
                "Action I1_I3_I1_A1, ID 22 completed.",
                "Action I1_I5_I1_A1, ID 34 completed."
            };

            Assert.AreEqual(expected.Count, output.Length);
            CollectionAssert.AreEquivalent(expected, new HashSet<string>(output));
            Assert.IsEmpty(stderr);
        }
    }
}
