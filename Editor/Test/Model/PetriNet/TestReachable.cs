//
//  TestReachable.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-10-09.
//

using NUnit.Framework;

using Petri.Model;

using HeadlessDocument = Petri.Application.CLI.HeadlessDocument;

namespace Petri.Test.Model.PetriNet
{
    [TestFixture]
    public class TestReachable
    {
        readonly HeadlessDocument _document = new HeadlessDocument(Petri.Code.Language.Cpp);

        [Test]
        public void TestEmptyPetriNet()
        {
            // GIVEN an empty petri net
            var root = new RootPetriNet(_document, null);

            // WHEN we test if it is reachable
            var reachable = root.IsReachable;

            // THEN it is reachable
            Assert.IsTrue(reachable);
        }

        [Test]
        public void TestOneInactiveState()
        {
            // GIVEN a petri net with an inactive state
            var root = new RootPetriNet(_document, null);
            var state = new Action(_document.EntityFactory, root, false);

            // WHEN we test if the state is reachable
            var reachable = state.IsReachable;

            // THEN it is not reachable
            Assert.IsFalse(reachable);
        }

        [Test]
        public void TestOneActiveState()
        {
            // GIVEN a petri net with an active state
            var root = new RootPetriNet(_document, null);
            var state = new Action(_document.EntityFactory, root, true);

            // WHEN we test if the state is reachable
            var reachable = state.IsReachable;

            // THEN it is reachable
            Assert.IsTrue(reachable);
        }

        [Test]
        public void TestActiveThenInactive()
        {
            // GIVEN a petri net with an active state that leads to an inactive state
            var root = new RootPetriNet(_document, null);
            var state1 = new Action(_document.EntityFactory, root, true);
            var state2 = new Action(_document.EntityFactory, root, false);
            var t12 = new Transition(_document.EntityFactory, root, state1, state2);
            state1.AddTransitionAfter(t12);
            state2.AddTransitionBefore(t12);

            // WHEN we test if the states are reachable
            var reachable1 = state1.IsReachable;
            var reachable2 = state2.IsReachable;

            // THEN they are reachable
            Assert.IsTrue(reachable1);
            Assert.IsTrue(reachable2);
        }

        [Test]
        public void TestActiveAndInactive()
        {
            // GIVEN a petri net with an active state and an inactive state
            var root = new RootPetriNet(_document, null);
            var state1 = new Action(_document.EntityFactory, root, true);
            var state2 = new Action(_document.EntityFactory, root, false);

            // WHEN we test if the states are reachable
            var reachable1 = state1.IsReachable;
            var reachable2 = state2.IsReachable;

            // THEN only the first is reachable
            Assert.IsTrue(reachable1);
            Assert.IsFalse(reachable2);
        }

        [Test]
        public void TestActiveThenActive()
        {
            // GIVEN a petri net with an active state that leads to an active state
            var root = new RootPetriNet(_document, null);
            var state1 = new Action(_document.EntityFactory, root, true);
            var state2 = new Action(_document.EntityFactory, root, true);
            var t12 = new Transition(_document.EntityFactory, root, state1, state2);
            state1.AddTransitionAfter(t12);
            state2.AddTransitionBefore(t12);

            // WHEN we test if the states are reachable
            var reachable1 = state1.IsReachable;
            var reachable2 = state2.IsReachable;

            // THEN they are reachable
            Assert.IsTrue(reachable1);
            Assert.IsTrue(reachable2);
        }

        [Test]
        public void TestActiveAndActive()
        {
            // GIVEN a petri net with 2 active states
            var root = new RootPetriNet(_document, null);
            var state1 = new Action(_document.EntityFactory, root, true);
            var state2 = new Action(_document.EntityFactory, root, true);

            // WHEN we test if the states are reachable
            var reachable1 = state1.IsReachable;
            var reachable2 = state2.IsReachable;

            // THEN they are reachable
            Assert.IsTrue(reachable1);
            Assert.IsTrue(reachable2);
        }

        [Test]
        public void TestInactiveThenActive()
        {
            // GIVEN a petri net with an active state that leads to an inactive state
            var root = new RootPetriNet(_document, null);
            var state1 = new Action(_document.EntityFactory, root, false);
            var state2 = new Action(_document.EntityFactory, root, true);
            var t12 = new Transition(_document.EntityFactory, root, state1, state2);
            state1.AddTransitionAfter(t12);
            state2.AddTransitionBefore(t12);

            // WHEN we test if the states are reachable
            var reachable1 = state1.IsReachable;
            var reachable2 = state2.IsReachable;

            // THEN only the second state is active
            Assert.IsFalse(reachable1);
            Assert.IsTrue(reachable2);
        }

        [Test]
        public void TestInactiveThenInactive()
        {
            // GIVEN a petri net with an inactive state that leads to an inactive state
            var root = new RootPetriNet(_document, null);
            var state1 = new Action(_document.EntityFactory, root, false);
            var state2 = new Action(_document.EntityFactory, root, false);
            var t12 = new Transition(_document.EntityFactory, root, state1, state2);
            state1.AddTransitionAfter(t12);
            state2.AddTransitionBefore(t12);

            // WHEN we test if the states are reachable
            var reachable1 = state1.IsReachable;
            var reachable2 = state2.IsReachable;

            // THEN they are not reachable
            Assert.IsFalse(reachable1);
            Assert.IsFalse(reachable2);
        }

        [Test]
        public void TestInactiveAndInactive()
        {
            // GIVEN a petri net with 2 inactive states
            var root = new RootPetriNet(_document, null);
            var state1 = new Action(_document.EntityFactory, root, false);
            var state2 = new Action(_document.EntityFactory, root, false);

            // WHEN we test if the states are reachable
            var reachable1 = state1.IsReachable;
            var reachable2 = state2.IsReachable;

            // THEN they are not reachable
            Assert.IsFalse(reachable1);
            Assert.IsFalse(reachable2);
        }

        [Test]
        public void TestSelfLoopInactive()
        {
            // GIVEN a petri net with an inactive state that leads to itself
            var root = new RootPetriNet(_document, null);
            var state = new Action(_document.EntityFactory, root, false);
            var t12 = new Transition(_document.EntityFactory, root, state, state);
            state.AddTransitionAfter(t12);
            state.AddTransitionBefore(t12);

            // WHEN we test if the state is reachable
            var reachable = state.IsReachable;

            // THEN is not reachable
            Assert.IsFalse(reachable);
        }

        [Test]
        public void TestSelfLoopActive()
        {
            // GIVEN a petri net with an active state that leads to itself
            var root = new RootPetriNet(_document, null);
            var state = new Action(_document.EntityFactory, root, true);
            var t12 = new Transition(_document.EntityFactory, root, state, state);
            state.AddTransitionAfter(t12);
            state.AddTransitionBefore(t12);

            // WHEN we test if the state is reachable
            var reachable = state.IsReachable;

            // THEN is reachable
            Assert.IsTrue(reachable);
        }

        [Test]
        public void TestActiveThenSelfLoopInactive()
        {
            // GIVEN a petri net with an active state that leads to an inactive state
            var root = new RootPetriNet(_document, null);
            var state1 = new Action(_document.EntityFactory, root, true);
            var state2 = new Action(_document.EntityFactory, root, false);
            var t12 = new Transition(_document.EntityFactory, root, state1, state2);
            state1.AddTransitionAfter(t12);
            state2.AddTransitionBefore(t12);
            var t23 = new Transition(_document.EntityFactory, root, state2, state2);
            state2.AddTransitionAfter(t23);
            state2.AddTransitionBefore(t23);

            // WHEN we test if the states are reachable
            var reachable1 = state1.IsReachable;
            var reachable2 = state2.IsReachable;

            // THEN they are reachable
            Assert.IsTrue(reachable1);
            Assert.IsTrue(reachable2);
        }

        [Test]
        public void TestLoopInactiveAndInactive()
        {
            // GIVEN a petri net with an active state that leads to an inactive state
            var root = new RootPetriNet(_document, null);
            var state1 = new Action(_document.EntityFactory, root, false);
            var state2 = new Action(_document.EntityFactory, root, false);
            var t12 = new Transition(_document.EntityFactory, root, state1, state2);
            state1.AddTransitionAfter(t12);
            state2.AddTransitionBefore(t12);
            var t23 = new Transition(_document.EntityFactory, root, state2, state1);
            state2.AddTransitionAfter(t23);
            state1.AddTransitionBefore(t23);

            // WHEN we test if the states are reachable
            var reachable1 = state1.IsReachable;
            var reachable2 = state2.IsReachable;

            // THEN they are reachable
            Assert.IsFalse(reachable1);
            Assert.IsFalse(reachable2);
        }

        [Test]
        public void TestLoopInactiveAndActive()
        {
            // GIVEN a petri net with an active state that leads to an inactive state
            var root = new RootPetriNet(_document, null);
            var state1 = new Action(_document.EntityFactory, root, false);
            var state2 = new Action(_document.EntityFactory, root, true);
            var t12 = new Transition(_document.EntityFactory, root, state1, state2);
            state1.AddTransitionAfter(t12);
            state2.AddTransitionBefore(t12);
            var t23 = new Transition(_document.EntityFactory, root, state2, state1);
            state2.AddTransitionAfter(t23);
            state1.AddTransitionBefore(t23);

            // WHEN we test if the states are reachable
            var reachable1 = state1.IsReachable;
            var reachable2 = state2.IsReachable;

            // THEN they are reachable
            Assert.IsTrue(reachable1);
            Assert.IsTrue(reachable2);
        }

        [Test]
        public void TestLoopActiveAndActive()
        {
            // GIVEN a petri net with an active state that leads to an inactive state
            var root = new RootPetriNet(_document, null);
            var state1 = new Action(_document.EntityFactory, root, true);
            var state2 = new Action(_document.EntityFactory, root, true);
            var t12 = new Transition(_document.EntityFactory, root, state1, state2);
            state1.AddTransitionAfter(t12);
            state2.AddTransitionBefore(t12);
            var t23 = new Transition(_document.EntityFactory, root, state2, state1);
            state2.AddTransitionAfter(t23);
            state1.AddTransitionBefore(t23);

            // WHEN we test if the states are reachable
            var reachable1 = state1.IsReachable;
            var reachable2 = state2.IsReachable;

            // THEN they are reachable
            Assert.IsTrue(reachable1);
            Assert.IsTrue(reachable2);
        }

        [Test]
        public void TestActiveInnerPetriNet()
        {
            // GIVEN an active inner petri net
            var root = new RootPetriNet(_document, null);
            var inner = new LocalInnerPetriNet(_document.EntityFactory, root, true);

            // WHEN we test if it is reachable
            var reachable = inner.IsReachable;

            // THEN it is reachable
            Assert.IsTrue(reachable);
        }

        [Test]
        public void TestInactiveInnerPetriNet()
        {
            // GIVEN an inactive inner petri net
            var root = new RootPetriNet(_document, null);
            var inner = new LocalInnerPetriNet(_document.EntityFactory, root, false);

            // WHEN we test if it is reachable
            var reachable = inner.IsReachable;

            // THEN it is not reachable
            Assert.IsFalse(reachable);
        }

        [Test]
        public void TestActiveStateInActiveInnerPetriNet()
        {
            // GIVEN an active inner petri net containing an active state
            var root = new RootPetriNet(_document, null);
            var inner = new LocalInnerPetriNet(_document.EntityFactory, root, true);
            var state = new Action(_document.EntityFactory, inner, true);

            // WHEN we test if the  is reachable
            var reachable = state.IsReachable;

            // THEN it is reachable
            Assert.IsTrue(reachable);
        }

        [Test]
        public void TestActiveStateInInactiveInnerPetriNet()
        {
            // GIVEN an inactive inner petri net containing an active state
            var root = new RootPetriNet(_document, null);
            var inner = new LocalInnerPetriNet(_document.EntityFactory, root, false);
            var state = new Action(_document.EntityFactory, inner, true);

            // WHEN we test if the state is reachable
            var reachable = state.IsReachable;

            // THEN it is not reachable
            Assert.IsFalse(reachable);
        }

        [Test]
        public void TestInactiveStateInActiveInnerPetriNet()
        {
            // GIVEN an active inner petri net containing an inactive state
            var root = new RootPetriNet(_document, null);
            var inner = new LocalInnerPetriNet(_document.EntityFactory, root, true);
            var state = new Action(_document.EntityFactory, inner, false);

            // WHEN we test if the state is reachable
            var reachable = state.IsReachable;

            // THEN it is not reachable
            Assert.IsFalse(reachable);
        }

        [Test]
        public void TestInactiveStateInInactiveInnerPetriNet()
        {
            // GIVEN an inactive inner petri net containing an inactive state
            var root = new RootPetriNet(_document, null);
            var inner = new LocalInnerPetriNet(_document.EntityFactory, root, false);
            var state = new Action(_document.EntityFactory, inner, false);

            // WHEN we test if the state is reachable
            var reachable = state.IsReachable;

            // THEN it is not reachable
            Assert.IsFalse(reachable);
        }

        [Test]
        public void TestActiveStateThenInactiveStateInActiveInnerPetriNet()
        {
            // GIVEN an active inner petri net containing an active state then an inactive state
            var root = new RootPetriNet(_document, null);
            var inner = new LocalInnerPetriNet(_document.EntityFactory, root, true);
            var state1 = new Action(_document.EntityFactory, inner, true);
            var state2 = new Action(_document.EntityFactory, inner, false);
            var t12 = new Transition(_document.EntityFactory, inner, state1, state2);
            state1.AddTransitionAfter(t12);
            state2.AddTransitionBefore(t12);

            // WHEN we test if the states are reachable
            var reachable1 = state1.IsReachable;
            var reachable2 = state2.IsReachable;

            // THEN they are reachable
            Assert.IsTrue(reachable1);
            Assert.IsTrue(reachable2);
        }

        [Test]
        public void TestActiveStateThenInnerPetriNet()
        {
            // GIVEN an active state then an inactive inner petri net
            var root = new RootPetriNet(_document, null);
            var inner = new LocalInnerPetriNet(_document.EntityFactory, root, false);
            var state1 = new Action(_document.EntityFactory, root, true);
            var t12 = new Transition(_document.EntityFactory, root, state1, inner);
            state1.AddTransitionAfter(t12);
            inner.AddTransitionBefore(t12);

            // WHEN we test if the states are reachable
            var reachable1 = state1.IsReachable;
            var reachable2 = inner.IsReachable;

            // THEN they are reachable
            Assert.IsTrue(reachable1);
            Assert.IsTrue(reachable2);
        }

        [Test]
        public void TestActiveStateThenActiveStateInInnerPetriNet()
        {
            // GIVEN an active state then an inactive inner petri net containing an active state
            var root = new RootPetriNet(_document, null);
            var inner = new LocalInnerPetriNet(_document.EntityFactory, root, false);
            var state1 = new Action(_document.EntityFactory, root, true);
            var state2 = new Action(_document.EntityFactory, inner, true);
            var t12 = new Transition(_document.EntityFactory, root, state1, inner);
            state1.AddTransitionAfter(t12);
            inner.AddTransitionBefore(t12);

            // WHEN we test if the states are reachable
            var reachable1 = state1.IsReachable;
            var reachable2 = inner.IsReachable;
            var reachable3 = state2.IsReachable;

            // THEN they are reachable
            Assert.IsTrue(reachable1);
            Assert.IsTrue(reachable2);
            Assert.IsTrue(reachable3);
        }

        [Test]
        public void TestActiveInnerPetriNetThenActiveState()
        {
            // GIVEN an active inner petri net then an active state
            var root = new RootPetriNet(_document, null);
            var inner = new LocalInnerPetriNet(_document.EntityFactory, root, true);
            var state1 = new Action(_document.EntityFactory, root, true);
            var t12 = new Transition(_document.EntityFactory, root, inner, state1);
            state1.AddTransitionBefore(t12);
            inner.AddTransitionAfter(t12);

            // WHEN we test if the states are reachable
            var reachable1 = state1.IsReachable;
            var reachable2 = inner.IsReachable;

            // THEN they are reachable
            Assert.IsTrue(reachable1);
            Assert.IsTrue(reachable2);
        }

        [Test]
        public void TestActiveInnerPetriNetThenInactiveState()
        {
            // GIVEN an active inner petri net then an active state
            var root = new RootPetriNet(_document, null);
            var inner = new LocalInnerPetriNet(_document.EntityFactory, root, true);
            var state1 = new Action(_document.EntityFactory, root, false);
            var t12 = new Transition(_document.EntityFactory, root, inner, state1);
            state1.AddTransitionBefore(t12);
            inner.AddTransitionAfter(t12);

            // WHEN we test if the states are reachable
            var reachable1 = inner.IsReachable;
            var reachable2 = state1.IsReachable;

            // THEN only the first state is reachable
            Assert.IsTrue(reachable1);
            Assert.IsFalse(reachable2);
        }

        [Test]
        public void TestActiveStateInActiveInnerPetriNetThenInactiveState()
        {
            // GIVEN an active state in an active inner petri net then an active state
            var root = new RootPetriNet(_document, null);
            var inner = new LocalInnerPetriNet(_document.EntityFactory, root, true);
            var state1 = new Action(_document.EntityFactory, root, false);
            var t12 = new Transition(_document.EntityFactory, root, inner, state1);
            state1.AddTransitionBefore(t12);
            inner.AddTransitionAfter(t12);
            var state2 = new Action(_document.EntityFactory, inner, true);

            // WHEN we test if the states are reachable
            var reachable1 = inner.IsReachable;
            var reachable2 = state1.IsReachable;
            var reachable3 = state2.IsReachable;

            // THEN they are reachable
            Assert.IsTrue(reachable1);
            Assert.IsFalse(reachable2);
            Assert.IsTrue(reachable3);
        }

        [Test]
        public void TestActiveStateWithTransitionToExitPointInActiveInnerPetriNetThenInactiveState()
        {
            // GIVEN an active state leading to the exit point of an active inner petri net then an active state
            var root = new RootPetriNet(_document, null);
            var inner = new LocalInnerPetriNet(_document.EntityFactory, root, true);
            var state1 = new Action(_document.EntityFactory, root, false);
            var t12 = new Transition(_document.EntityFactory, root, inner, state1);
            state1.AddTransitionBefore(t12);
            inner.AddTransitionAfter(t12);
            var state2 = new Action(_document.EntityFactory, inner, true);
            var t23 = new Transition(_document.EntityFactory, inner, state2, inner.ExitPoint);
            inner.ExitPoint.AddTransitionBefore(t23);
            state2.AddTransitionAfter(t23);

            // WHEN we test if the states are reachable
            var reachable1 = inner.IsReachable;
            var reachable2 = state1.IsReachable;
            var reachable3 = state2.IsReachable;

            // THEN they are reachable
            Assert.IsTrue(reachable1);
            Assert.IsTrue(reachable2);
            Assert.IsTrue(reachable3);
        }
    }
}

