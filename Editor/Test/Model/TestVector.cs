//
//  TestVector.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-10-02.
//

using System;

using NUnit.Framework;

using Petri.Model;

namespace Petri.Test.Model
{
    [TestFixture]
    public class TestVector
    {
        Random _random = new Random();

        [Test]
        public void TestDefaultVector()
        {
            // GIVEN a defautl vector
            var v = new Vector();

            // THEN it is the null vector
            Assert.AreEqual(v.X, 0.0);
            Assert.AreEqual(v.Y, 0.0);
        }

        [Test, Repeat(10)]
        public void TestAssign()
        {
            // GIVEN random coordinates
            var x = _random.NextDouble() * _random.Next();
            var y = _random.NextDouble() * _random.Next();

            // WHEN we create a vector with them
            var v = new Vector(x, y);

            // THEN it is the right value
            Assert.AreEqual(v.X, x);
            Assert.AreEqual(v.Y, y);
        }

        [Test, Repeat(10)]
        public void TestCopy()
        {
            // GIVEN a vector with random coordinates
            var x = _random.NextDouble() * _random.Next();
            var y = _random.NextDouble() * _random.Next();
            var v1 = new Vector(x, y);

            // WHEN we copy it
            var v2 = new Vector(v1);

            // THEN it is equal to the first
            Assert.AreEqual(v1.X, v2.X);
            Assert.AreEqual(v1.Y, v2.Y);
        }

        [Test]
        public void TestZero()
        {
            // GIVEN a vector with zero coordinates
            var v = new Vector();

            // THEN operations on it are coherent
            Assert.AreEqual(v.Norm, 0.0);
            Assert.AreEqual(v.SquaredNorm, 0.0);
            Assert.AreEqual(5 * v, new Vector());
            Assert.AreEqual(5 * v, new Vector(0, 0));
        }

        [Test]
        public void TestNorm([Values(true, false)] bool xOrY, [Random(-100.0, 100.0, 3)] double coord)
        {
            // GIVEN a vector with one non-zero coordinate
            var v = new Vector();
            if(xOrY) {
                v.X = coord;
            } else {
                v.Y = coord;
            }

            // WHEN we take its norm
            var norm = v.Norm;

            // THEN it is the correct value
            Assert.AreEqual(norm, Math.Sqrt(coord * coord));
        }

        [Test]
        public void TestSquaredNorm([Values(true, false)] bool xOrY, [Random(-100.0, 100.0, 3)] double coord)
        {
            // GIVEN a vector with one non-zero coordinate
            var v = new Vector();
            if(xOrY) {
                v.X = coord;
            } else {
                v.Y = coord;
            }

            // WHEN we take its squared norm
            var sqnorm = v.SquaredNorm;

            // THEN it is the correct value
            Assert.AreEqual(sqnorm, coord * coord);
        }

        [Test]
        public void TestNormalized1([Values(true, false)] bool xOrY, [Random(-100.0, 100.0, 3)] double coord)
        {
            // GIVEN a vector with one non-zero coordinate
            var v = new Vector();
            if(xOrY) {
                v.X = coord;
            } else {
                v.Y = coord;
            }

            if(v.Norm < 1.0e-10) {
                Assert.Inconclusive();
                return;
            }

            // WHEN we normalize it
            v = v.Normalized;

            // THEN it has the correct value
            Assert.Less(Math.Abs(v.Norm - 1.0), 1.0e-10);
            if(xOrY) {
                Assert.Less(Math.Abs(Math.Abs(v.X) - 1.0), 1.0e-10);
                Assert.AreEqual(0, v.Y);
            } else {
                Assert.Less(Math.Abs(Math.Abs(v.Y) - 1.0), 1.0e-10);
                Assert.AreEqual(0, v.X);
            }
        }

        [Test]
        public void TestNormalized2([Random(-100.0, 100.0, 3)] double x, [Random(-100.0, 100.0, 3)] double y)
        {
            // GIVEN a vector with one non-zero coordinate
            var v = new Vector(x, y);

            if(v.Norm < 1.0e-10) {
                Assert.Inconclusive();
                return;
            }

            // WHEN we normalize it
            v = v.Normalized;

            // THEN it has the correct value
            Assert.Less(Math.Abs(v.Norm - 1.0), 1.0e-10);
        }

        [Test]
        public void TestAdd([Random(-100.0, 100.0, 2)] double x1, [Random(-100.0, 100.0, 2)] double y1, [Random(-100.0, 100.0, 2)] double x2, [Random(-100.0, 100.0, 2)] double y2)
        {
            // GIVEN two random vectors
            var v1 = new Vector(x1, y1);
            var v2 = new Vector(x2, y2);

            // WHEN we sum them
            var sum = v1 + v2;

            // THEN the resulting vector's coordinates have the correct value
            Assert.AreEqual(sum.X, v1.X + v2.X);
            Assert.AreEqual(sum.Y, v1.Y + v2.Y);
            Assert.IsInstanceOf<Vector>(sum);
        }

        [Test]
        public void TestSubtract([Random(-100.0, 100.0, 2)] double x1, [Random(-100.0, 100.0, 2)] double y1, [Random(-100.0, 100.0, 2)] double x2, [Random(-100.0, 100.0, 2)] double y2)
        {
            // GIVEN two random vectors
            var v1 = new Vector(x1, y1);
            var v2 = new Vector(x2, y2);

            // WHEN we subtract the second to the first
            var result = v1 - v2;

            // THEN the resulting vector's coordinates have the correct value
            Assert.AreEqual(result.X, v1.X - v2.X);
            Assert.AreEqual(result.Y, v1.Y - v2.Y);
            Assert.IsInstanceOf<Vector>(result);
        }

        [Test]
        public void TestPrefixMult([Random(-100.0, 100.0, 2)] double x, [Random(-100.0, 100.0, 2)] double y, [Random(-100.0, 100.0, 4)] double a)
        {
            // GIVEN one random vector
            var v = new Vector(x, y);

            // WHEN we multiply it by a real number
            var product = a * v;

            // THEN the resulting vector's coordinates have the correct value
            Assert.AreEqual(a * v.X, product.X);
            Assert.AreEqual(a * v.Y, product.Y);
            Assert.IsInstanceOf<Vector>(product);
        }

        [Test]
        public void TestPostfixMult([Random(-100.0, 100.0, 2)] double x, [Random(-100.0, 100.0, 2)] double y, [Random(-100.0, 100.0, 4)] double a)
        {
            // GIVEN one random vector
            var v = new Vector(x, y);

            // WHEN we multiply it by a real number
            var product = v * a;

            // THEN the resulting vector's coordinates have the correct value
            Assert.AreEqual(v.X * a, product.X);
            Assert.AreEqual(v.Y * a, product.Y);
            Assert.IsInstanceOf<Vector>(product);
        }

        [Test]
        public void TestDivide([Random(-100.0, 100.0, 2)] double x, [Random(-100.0, 100.0, 2)] double y, [Random(-100.0, 100.0, 4)] double a)
        {
            if(Math.Abs(a) < 1.0e-12) {
                Assert.Inconclusive("Do not divide by 0!");
            }
            // GIVEN one random vector
            var v = new Vector(x, y);

            // WHEN we divide it by a real number
            var result = v / a;

            // THEN the resulting vector's coordinates have the correct value
            Assert.AreEqual(v.X / a, result.X);
            Assert.AreEqual(v.Y / a, result.Y);
            Assert.IsInstanceOf<Vector>(result);
        }

        [Test, Sequential]
        public void TestToString([Values(-10.5, -1, -0.05, 0, 0.05, 2, 10.5)] double x, [Values(8.5, 4, 1.05, 0, -1.05, -3, -8.5)] double y, [Values("{-10.5, 8.5}", "{-1, 4}", "{-0.05, 1.05}", "{0, 0}", "{0.05, -1.05}", "{2, -3}", "{10.5, -8.5}")] string result)
        {
            // GIVEN a random vector
            var v = new Vector(x, y);

            // WHEN we take its string representation
            var s = v.ToString();

            // THEN it is equal to the expected value
            Assert.AreEqual(result.Replace(".", System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator), s);
        }
    }
}

