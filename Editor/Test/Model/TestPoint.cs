//
//  TestPoint.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-10-03.
//

using System;

using NUnit.Framework;

using Petri.Model;

namespace Petri.Test.Model
{
    [TestFixture]
    public class TestPoint
    {
        Random _random = new Random();

        [Test]
        public void TestDefaultPoint()
        {
            // GIVEN a default point
            var v = new Point();

            // THEN it has zero coordinates
            Assert.AreEqual(v.X, 0.0);
            Assert.AreEqual(v.Y, 0.0);
        }

        [Test, Repeat(10)]
        public void TestAssign()
        {
            // GIVEN random coordinates
            var x = _random.NextDouble() * _random.Next();
            var y = _random.NextDouble() * _random.Next();

            // WHEN we create a point with them
            var v = new Point(x, y);

            // THEN it has the right value
            Assert.AreEqual(v.X, x);
            Assert.AreEqual(v.Y, y);
        }

        [Test, Repeat(10)]
        public void TestCopy()
        {
            // GIVEN a point with random coordinates
            var x = _random.NextDouble() * _random.Next();
            var y = _random.NextDouble() * _random.Next();
            var p1 = new Point(x, y);

            // WHEN we copy it
            var p2 = new Point(p1);

            // THEN it has the right values
            Assert.AreEqual(p1.X, p2.X);
            Assert.AreEqual(p1.Y, p2.Y);
        }

        [Test]
        public void TestAddWithVector1([Random(-100.0, 100.0, 2)] double x1, [Random(-100.0, 100.0, 2)] double y1, [Random(-100.0, 100.0, 2)] double x2, [Random(-100.0, 100.0, 2)] double y2)
        {
            // GIVEN a random vector and a random point
            var v = new Vector(x1, y1);
            var p = new Point(x2, y2);

            // WHEN we sum them
            var result = v + p;

            // THEN the resulting point's coordinates have the correct value
            Assert.AreEqual(result.X, v.X + p.X);
            Assert.AreEqual(result.Y, v.Y + p.Y);
            Assert.IsInstanceOf<Point>(result);
        }

        [Test]
        public void TestAddWithVector2([Random(-100.0, 100.0, 2)] double x1, [Random(-100.0, 100.0, 2)] double y1, [Random(-100.0, 100.0, 2)] double x2, [Random(-100.0, 100.0, 2)] double y2)
        {
            // GIVEN a random vector and a random point
            var v = new Vector(x1, y1);
            var p = new Point(x2, y2);

            // WHEN we sum them
            var result = p + v;

            // THEN the resulting point's coordinates have the correct value
            Assert.AreEqual(result.X, p.X + v.X);
            Assert.AreEqual(result.Y, p.Y + v.Y);
            Assert.IsInstanceOf<Point>(result);
        }

        [Test]
        public void TestSubtract([Random(-100.0, 100.0, 2)] double x1, [Random(-100.0, 100.0, 2)] double y1, [Random(-100.0, 100.0, 2)] double x2, [Random(-100.0, 100.0, 2)] double y2)
        {
            // GIVEN two random points
            var p1 = new Point(x1, y1);
            var p2 = new Point(x2, y2);

            // WHEN we subtract the second to the first
            var result = p1 - p2;

            // THEN the resulting vector's coordinates have the correct value
            Assert.AreEqual(result.X, p1.X - p2.X);
            Assert.AreEqual(result.Y, p1.Y - p2.Y);
            Assert.IsInstanceOf<Vector>(result);
        }

        [Test]
        public void TestSubtractWithVector([Random(-100.0, 100.0, 2)] double x1, [Random(-100.0, 100.0, 2)] double y1, [Random(-100.0, 100.0, 2)] double x2, [Random(-100.0, 100.0, 2)] double y2)
        {
            // GIVEN a random vector and a random point
            var v = new Vector(x1, y1);
            var p = new Point(x2, y2);

            // WHEN we subtract them
            var result = p - v;

            // THEN the resulting point's coordinates have the correct value
            Assert.AreEqual(result.X, p.X - v.X);
            Assert.AreEqual(result.Y, p.Y - v.Y);
            Assert.IsInstanceOf<Point>(result);
        }

        [Test]
        public void TestToCairoPoint([Random(-100.0, 100.0, 2)] double x, [Random(-100.0, 100.0, 2)] double y)
        {
            // GIVEN a random vector
            var p = new Point(x, y);

            // WHEN we convert it to a Cairo.PointD instance
            Cairo.PointD cairoPoint = p;

            // THEN the resulting point's coordinates have the correct value
            Assert.AreEqual(cairoPoint.X, p.X);
            Assert.AreEqual(cairoPoint.Y, p.Y);
        }

        [Test, Sequential]
        public void TestToString([Values(-10.5, -1, -0.05, 0, 0.05, 2, 10.5)] double x, [Values(8.5, 4, 1.05, 0, -1.05, -3, -8.5)] double y, [Values("{-10.5, 8.5}", "{-1, 4}", "{-0.05, 1.05}", "{0, 0}", "{0.05, -1.05}", "{2, -3}", "{10.5, -8.5}")] string result)
        {
            // GIVEN a random point
            var p = new Point(x, y);

            // WHEN we take its string representation
            var s = p.ToString();

            // THEN it is equal to the expected value
            Assert.AreEqual(result.Replace(".", System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator), s);
        }
    }
}

