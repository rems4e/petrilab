﻿//
//  TestRuntime.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-01-01.
//

using NUnit.Framework;

using Petri.Runtime;

using Random = System.Random;
using UInt32 = System.UInt32;
using UInt64 = System.UInt64;

namespace Petri.Test.Runtime
{
    [TestFixture]
    public class TestRuntime
    {
        Random _random = new Random();

        [Test, Repeat(10)]
        public void TestRuntimePetriNetProperties()
        {
            var name = CodeUtility.RandomLiteral(CodeUtility.LiteralType.String, CodeUtility.RandomLanguage()).Item1;
            var hasVariables = TestingUtility.RandomBool();
            var variablesCount = hasVariables ? TestingUtility.RandomBetween(0, 1000) : 0;

            var hasMaxConcurrency = TestingUtility.RandomBool();
            var maxConcurrency = hasMaxConcurrency ? TestingUtility.RandomBetween(1, 1000) : 0;


            PetriNet pn;

            // GIVEN a petri net created with a custom name
            if(hasVariables) {
                pn = new PetriNet(name, variablesCount);
            } else {
                pn = new PetriNet(name);
            }

            // WHEN we read the properties of the action
            // THEN we get an equivalent value
            Assert.AreNotSame(name, pn.Name);
            Assert.AreEqual(name, pn.Name);
            Assert.AreEqual(variablesCount, pn.Variables.Size);

            Assert.Greater(pn.MaxConcurrency, 1_000_000);

            if(hasMaxConcurrency) {
                pn.MaxConcurrency = (UInt64)maxConcurrency;

                Assert.AreEqual(maxConcurrency, pn.MaxConcurrency);
            }
        }

        [Test, Repeat(10)]
        public void TestInvalidMaxConcurrency()
        {
            var pn = new PetriNet("");
            pn.MaxConcurrency = 0;
            Assert.AreEqual(1, pn.MaxConcurrency);
        }

        public static System.Int32 Action1()
        {
            System.Console.WriteLine("Action1!");
            return 0;
        }

        public static System.Int32 Action2()
        {
            System.Console.WriteLine("Action2!");
            return 0;
        }

        public static System.Int32 Action3()
        {
            System.Console.WriteLine("Action3!");
            return 0;
        }

        public static bool Transition1(System.Int32 result)
        {
            --counter;
            return counter > 0;
        }

        public static bool Transition3(System.Int32 result)
        {
            return counter == 0;
        }

        public static bool Transition2(System.Int32 result)
        {
            return true;
        }

        [Test]
        public void TestRuntime1()
        {
            PetriNet pn = new PetriNet("Test");

            Action a1 = new Action(1, "action1", Action1, 1);
            Action a2 = new Action(2, "action2", Action2, 1);
            Action a3 = new Action(3, "action3", Action3, 1);

            a1.AddTransition(4, "transition1", a2, Transition1);
            a2.AddTransition(5, "transition2", a1, Transition2);
            a1.AddTransition(6, "transition3", a3, Transition3);

            pn.AddAction(a1, true);
            pn.AddAction(a2, false);
            pn.AddAction(a3, false);

            counter = 2;

            string stdout, stderr;
            Petri.Test.TestingUtility.InvokeAndRedirectOutput(() => {
                pn.Run();
                pn.Join();
            }, out stdout, out stderr);

            Assert.AreEqual("Action1!\nAction2!\nAction1!\nAction3!\n", stdout);
            Assert.IsEmpty(stderr);
        }

        [Test, Repeat(10)]
        public void TestRuntimeActionProperties()
        {
            var name = CodeUtility.RandomLiteral(CodeUtility.LiteralType.String, CodeUtility.RandomLanguage()).Item1;
            var id = (UInt64)_random.Next();
            var requiredTokens = (UInt32)_random.Next();

            // GIVEN an action created with a custom name, id and required tokens count
            Action a = new Action(id, name, Action1, requiredTokens);

            // WHEN we read the properties of the action
            // THEN we get an equivalent value
            Assert.AreNotSame(name, a.Name);
            Assert.AreEqual(name, a.Name);
            Assert.AreEqual(id, a.ID);
            Assert.AreEqual(requiredTokens, a.RequiredTokens);
            Assert.AreEqual(0, a.CurrentTokens);
        }

        [Test]
        public void TestRuntimeTransitionProperties()
        {
            var name = CodeUtility.RandomLiteral(CodeUtility.LiteralType.String, CodeUtility.RandomLanguage()).Item1;
            var id = (UInt64)_random.Next();
            // GIVEN a transition created with a custom name and id
            Action a = new Action(3, "", Action1, 1);
            Transition t = a.AddTransition(id, name, a, Transition1);

            // WHEN we read the name of the transition
            // THEN we get an equivalent value
            Assert.AreNotSame(name, t.Name);
            Assert.AreEqual(name, t.Name);
            Assert.AreEqual(id, t.ID);
        }

        static volatile int counter;
    }
}
