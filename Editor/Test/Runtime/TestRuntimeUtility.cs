﻿//
//  TestRuntimeUtility.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-03-12.
//

using System;

using NUnit.Framework;

namespace Petri.Test.Runtime
{
    [TestFixture]
    public class TestRuntimeUtility
    {
        Random _random = new Random();

        [Test]
        public void TestSleep()
        {
            // GIVEN a random delay set between 0.5 and 2.5 seconds and a margin of 10%
            double delay = (_random.NextDouble() * 2) + 0.5;
            double margin = 0.2;

            var watch = new System.Diagnostics.Stopwatch();

            // WHEN we wait for that delay
            watch.Start();
            Petri.Runtime.Utility.Pause(delay);
            watch.Stop();

            // THEN the elapsed delay is within the expected margin.
            Assert.Greater(watch.ElapsedMilliseconds / 1000.0, delay * (1 - margin));
            Assert.Less(watch.ElapsedMilliseconds / 1000.0, delay * (1 + margin));
        }

        [Test, Repeat(20)]
        public void TestRandom()
        {
            // GIVEN two bounds forming a closed integral interval
            Int64 random1 = _random.Next();
            Int64 random2 = _random.Next();

            if(random2 < random1) {
                var tmp = random1;
                random1 = random2;
                random2 = tmp;
            }

            // WHEN we generate a random number with the specified bounds
            var value = Petri.Runtime.Utility.Random(random1, random2);

            // THEN the returned value is within the specified bounds
            Assert.GreaterOrEqual(value, random1);
            Assert.LessOrEqual(value, random2);
        }

        [Test, Repeat(5)]
        public void TestRandomOneInterval()
        {
            // GIVEN a number forming a closed integral interval
            Int64 random1 = _random.Next();

            // WHEN we generate a random number with the specified bounds
            var value = Petri.Runtime.Utility.Random(random1, random1);

            // THEN the returned value is within the specified bounds
            Assert.AreEqual(value, random1);
        }
    }
}
