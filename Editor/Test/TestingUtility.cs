﻿//
//  TestingUtility.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-02-22.
//

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using NUnit.Framework;

namespace Petri.Test
{
    [SetUpFixture]
    public class TestInit
    {
        [OneTimeSetUp]
        public void RunBeforeAnyTests()
        {
            Environment.CurrentDirectory = Path.GetDirectoryName(typeof(TestInit).Assembly.Location);
        }
    }

    public static class TestingUtility
    {
        /// <summary>
        /// Checks if 2 instances have their public properties one-to-one equal, without considering the fields which names are in <paramref name="ignore"/>.
        /// </summary>
        /// <returns><c>false</c>, if any non-ignored public field of one object is different in the other object.</returns>
        /// <param name="first">The first object.</param>
        /// <param name="second">The second object.</param>
        /// <param name="ignore">The list of fields to not compare.</param>
        /// <typeparam name="T">The type of the objects to compare.</typeparam>
        public static void AssertPublicInstancePropertiesEqual<T>(T first, T second, params string[] ignore) where T : class
        {
            AssertInstancePropertiesEqualFlags(first,
                                               second,
                                               System.Reflection.BindingFlags.Public,
                                               ignore);
        }

        /// <summary>
        /// Checks if 2 instances have their properties one-to-one equal, without considering the fields which names are in <paramref name="ignore"/>.
        /// </summary>
        /// <returns><c>false</c>, if any non-ignored field of one object is different in the other object.</returns>
        /// <param name="first">The first object.</param>
        /// <param name="second">The second object.</param>
        /// <param name="ignore">The list of fields to not compare.</param>
        /// <typeparam name="T">The type of the objects to compare.</typeparam>
        public static void AssertInstancePropertiesEqual<T>(T first, T second, params string[] ignore) where T : class
        {
            AssertInstancePropertiesEqualFlags(first,
                                               second,
                                               System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.NonPublic,
                                               ignore);
        }

        /*
         * Adapted from code from "Big T", available at https://stackoverflow.com/a/844855/1276072 (version of 2016-02-19)
         */
        static void AssertInstancePropertiesEqualFlags<T>(T first,
                                                          T second,
                                                          System.Reflection.BindingFlags flags,
                                                          params string[] ignore) where T : class
        {
            if(first != null && second != null) {
                Type type = typeof(T);
                var ignoreList = new HashSet<string>(ignore);
                foreach(System.Reflection.PropertyInfo pi in type.GetProperties(flags | System.Reflection.BindingFlags.Instance)) {
                    if(!ignoreList.Contains(pi.Name)) {
                        object selfValue = type.GetProperty(pi.Name).GetValue(first, null);
                        object toValue = type.GetProperty(pi.Name).GetValue(second, null);

                        if(selfValue != toValue && (selfValue == null || !selfValue.Equals(toValue))) {
                            Assert.Fail(pi.Name);
                        }
                    }
                }
            } else {
                Assert.AreEqual(first, second);
            }
        }

        /// <summary>
        /// Path root type.
        /// </summary>
        public enum PathRootType
        {
            /// <summary>
            /// Absolute path.
            /// </summary>
            Rooted,

            /// <summary>
            /// Path relative to the current directory, i.e. something like './rest/of/the/path'.
            /// </summary>
            RelativeToCurrentDir,

            /// <summary>
            /// A relative path, i.e. not rooted 'this/is/a/relative/path'
            /// </summary>
            Relative
        }

        /// <summary>
        /// Gets a random filesystem path
        /// </summary>
        /// <returns>The path.</returns>
        /// <param name="maxDepth">The max number of subdirectories composing the path.</param>
        /// <param name="rootType">The type of path.</param>
        /// <param name="file">The filename to append to the end of the returned path. Nothing is appended if the parameter is empty, meaning that the returned path would be a directory path.</param>
        public static string RandomPath(int maxDepth, PathRootType rootType, string file = "")
        {
            var random = new Random();

            int depth = random.Next(0, maxDepth);
            string path;

            if(rootType == PathRootType.Rooted) {
                path = "" + Path.DirectorySeparatorChar;
            } else if(rootType == PathRootType.RelativeToCurrentDir) {
                path = "." + Path.DirectorySeparatorChar;
            } else {
                path = "";
            }

            string domain = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789éàç@&_$-+=*,;& ";
            for(int i = 0; i < depth; ++i) {
                int length = random.Next(1, 25);

                var component = new string(Enumerable.Repeat(domain, length).Select(s => s[random.Next(s.Length)]).ToArray());
                path += component + Path.DirectorySeparatorChar;
            }

            path += file;

            return path;
        }

        /// <summary>
        /// Returns a random relative path.
        /// </summary>
        /// <returns>The relative path.</returns>
        /// <param name="file">The filename to append to the end of the returned path. Nothing is appended if the parameter is empty, meaning that the returned path would be a directory path.</param>
        public static string RandomRelativePath(string file = "")
        {
            return RandomPath(10, PathRootType.Relative, file);
        }

        /// <summary>
        /// Returns a random absolute path.
        /// </summary>
        /// <returns>The absolute path.</returns>
        /// <param name="file">The filename to append to the end of the returned path. Nothing is appended if the parameter is empty, meaning that the returned path would be a directory path.</param>
        public static string RandomAbsolutePath(string file = "")
        {
            return RandomPath(10, PathRootType.Rooted, file);
        }

        /// <summary>
        /// Gets a random number between the given min and max.
        /// </summary>
        /// <returns>The random number.</returns>
        /// <param name="min">Minimum.</param>
        /// <param name="max">Max.</param>
        public static int RandomBetween(int min, int max)
        {
            return _random.Next(min, max);
        }

        /// <summary>
        /// Gets a random boolean.
        /// </summary>
        /// <returns>The random boolean.</returns>
        public static bool RandomBool()
        {
            return RandomBetween(0, 100) > 49;
        }

        /// <summary>
        /// Waits for a condition to become true.
        /// </summary>
        /// <param name="pred">The condition to test.</param>
        /// <param name="delayMS">The time to wait between 2 tests, in milliseconds.</param>
        public static void WaitFor(Func<bool> pred, int delayMS = 50)
        {
            while(!pred.Invoke()) {
                System.Threading.Thread.Sleep(delayMS);
            }
        }

        public class ConsoleStringWriter : StringWriter
        {
            public override System.Text.Encoding Encoding {
                get {
                    return System.Text.Encoding.UTF8;
                }
            }
        }

        public static TResult InvokeAndRedirectOutput<TResult>(Func<TResult> function,
                                                               out string stdout,
                                                               out string stderr)
        {
            TResult result;

            var previousOut = Console.Out;
            var previousErr = Console.Error;

            using(StringWriter sout = new ConsoleStringWriter(), serr = new ConsoleStringWriter()) {
                Console.SetOut(sout);
                Console.SetError(serr);
                result = function();

                stdout = sout.ToString();
                stderr = serr.ToString();
            }

            Console.SetOut(previousOut);
            Console.SetError(previousErr);

            return result;
        }

        public static void InvokeAndRedirectOutput(Action function,
                                                   out string stdout,
                                                   out string stderr)
        {
            InvokeAndRedirectOutput(() => {
                function();
                return 0;
            }, out stdout, out stderr);
        }

        static Random _random = new Random();
    }
}

