//
//  DelegateOutput.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2016-05-29.
//

using System;

using NUnit.Framework;

namespace Petri.Test
{
    public class DelegateOutput : Petri.Application.IOutput
    {
        public DelegateOutput()
        {
            WriteDel = (string s) => {
                Console.Write("{0}", s);
            };
            WriteErrDel = (string s) => {
                Console.Error.Write("{0}", s);
                Assert.Fail(s);
            };
        }

        public Action<string> WriteDel {
            get;
            set;
        }

        public Action<string> WriteErrDel {
            get;
            set;
        }

        public void Write(string value, params object[] args)
        {
            if(WriteDel != null) {
                WriteDel.Invoke(string.Format(value, args));
            }
        }

        public void WriteLine(string value, params object[] args)
        {
            if(WriteDel != null) {
                WriteDel.Invoke(string.Format(value, args) + "\n");
            }
        }

        public void WriteLine()
        {
            if(WriteDel != null) {
                WriteDel.Invoke("\n");
            }
        }

        public void WriteWarningLine(string value, params object[] args)
        {
            if(WriteDel != null) {
                WriteDel.Invoke(string.Format(value, args) + "\n");
            }
        }

        public void WriteError(string value, params object[] args)
        {
            if(WriteErrDel != null) {
                WriteErrDel.Invoke(string.Format(value, args));
            }
        }

        public void WriteErrorLine(string value, params object[] args)
        {
            if(WriteErrDel != null) {
                WriteErrDel.Invoke(string.Format(value, args) + "\n");
            }
        }

        public void WriteErrorLine()
        {
            if(WriteErrDel != null) {
                WriteErrDel.Invoke("\n");
            }
        }
    }
}

