﻿//
//  VoidOutput.cs
//  PetriLab
//
//  Created by Rémi Saurel on 2014-12-09.
//

using System;

using NUnit.Framework;

namespace Petri.Test
{
    public class VoidOutput : DelegateOutput
    {
        public VoidOutput()
        {
            WriteDel = (string s) => {
            };
            WriteErrDel = (string s) => {
                Assert.Fail(s);
            };
        }
    }
}

