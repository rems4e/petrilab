#!/bin/sh

#
#  get_localizations.sh
#  PetriLab
#
#  Created by Rémi Saurel on 2016-07-17.
#


find ../Sources -name "*.cs" -exec perl -0777 -ne '@m = /Localized\(\n*\s*"((?:[^"\\]|(?:(?:\\")|(?:\\n)|(?:\\t)))*)"/g; if(@m) { print join("\n@@@@\n", @m), "\n@@@@\n" }' {} \;
