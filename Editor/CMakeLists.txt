#
#  CMakeLists.txt
#  PetriLab
#
#  Created by Rémi Saurel on 2016-04-29.
#

set(CS_CONF "Debug")

if(CMAKE_BUILD_TYPE MATCHES Release)
	set(CS_CONF "Release")
endif()

### execProxy + Editor ###

add_executable(_Petri_execProxy ../Runtime/execProxy.c)
target_compile_options(_Petri_execProxy PUBLIC -O2)
set_target_properties(_Petri_execProxy PROPERTIES OUTPUT_NAME execProxy)

if(FORCE_32_BITS)
	set(XBUILD_ARCH_OPTION /p:PlatformTarget=x86)
endif()

add_custom_target(PetriEditor ALL
	COMMAND ${CMAKE_CURRENT_SOURCE_DIR}/../Runtime/CSharp/C2CS.sh
	COMMAND msbuild /nologo /verbosity:quiet /property:Configuration=${CS_CONF} ${XBUILD_ARCH_OPTION} ${CMAKE_CURRENT_SOURCE_DIR}/Projects/Petri.csproj
	COMMAND ln -sfT Runtime ${Petri_SOURCE_DIR}/petrilab
)
add_dependencies(PetriEditor LinkExecProxy)
add_custom_target(LinkExecProxy ALL
	COMMAND mkdir -p ${Petri_SOURCE_DIR}/Editor/bin
	COMMAND mkdir -p ${Petri_SOURCE_DIR}/Editor/Test/bin
	COMMAND ln -sf $<TARGET_FILE:_Petri_execProxy> ${Petri_SOURCE_DIR}/
	COMMAND ln -sf $<TARGET_FILE:_Petri_execProxy> ${Petri_SOURCE_DIR}/Editor/bin/execProxy
	COMMAND ln -sf $<TARGET_FILE:_Petri_execProxy> ${Petri_SOURCE_DIR}/Editor/Test/bin/execProxy
)
if(APPLE)
	add_custom_target(_Petri_execProxyMac ALL
		COMMAND mkdir -p ${Petri_SOURCE_DIR}/Editor/PetriMac.app/Contents/MonoBundle
		COMMAND cp $<TARGET_FILE:_Petri_execProxy> ${Petri_SOURCE_DIR}/Editor/PetriMac.app/Contents/MonoBundle
	)
	add_dependencies(LinkExecProxy _Petri_execProxyMac)
endif()

add_custom_target(examples ALL
	COMMAND ${Petri_SOURCE_DIR}/Examples/petri_cpp_to_c_cs_python.sh >/dev/null
)
add_custom_target(examples_build ALL
	COMMAND find ${Petri_SOURCE_DIR}/Examples -name "*.petri" -exec ${Petri_SOURCE_DIR}/petri -gcv {} "\;"
)
add_dependencies(examples_build PetriEditor examples)
set_target_properties(examples_build PROPERTIES EXCLUDE_FROM_ALL TRUE)

add_custom_target(clean_examples ALL
	COMMAND find ${Petri_SOURCE_DIR}/Examples -name "*.petri" -exec ${Petri_SOURCE_DIR}/petri -kv {} "+"
	COMMAND find ${Petri_SOURCE_DIR}/Examples -name "__pycache__" -exec rm -rf {} "\;" 2>/dev/null || true
)
add_dependencies(clean_examples PetriEditor)
set_target_properties(clean_examples PROPERTIES EXCLUDE_FROM_ALL TRUE)

### Clean ###

set_property(DIRECTORY APPEND PROPERTY ADDITIONAL_MAKE_CLEAN_FILES ${CMAKE_CURRENT_SOURCE_DIR}/bin ${CMAKE_CURRENT_SOURCE_DIR}/Test/bin ${CMAKE_CURRENT_SOURCE_DIR}/Projects/obj ${CMAKE_CURRENT_SOURCE_DIR}/Test/obj)

### Testing ###

set(NUNIT_ARGS "--result=TestResult.xml")
add_custom_target(check ALL
	COMMAND msbuild /nologo /verbosity:quiet /property:Configuration=${CS_CONF} ${XBUILD_ARCH_OPTION} ${CMAKE_CURRENT_SOURCE_DIR}/Test/Test.csproj
	COMMAND ln -sf ${Petri_SOURCE_DIR}/Editor/bin/PetriRuntime.dll ${Petri_SOURCE_DIR}/Examples/ExternalPetriNet
	COMMAND nunit3-console --noheader --timeout=180000 --agents=1 "${NUNIT_ARGS}" ${CMAKE_CURRENT_SOURCE_DIR}/Test/Test.csproj
)
add_dependencies(check PetriEditor PetriRuntime LinkLib examples)
set_target_properties(check PROPERTIES EXCLUDE_FROM_ALL TRUE)
